/*
*	图片轮播控件
*/
UCML.Slides = function (id) {
    this.imgList = [];
    this.srcDataTable;
    this.imgField;
    this.addEvents("slidesClick");
    UCML.Slides.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Slides, UCML.Input, {
    ctype: "UCML.Slides",
    //轮播相关属性
    width: 500,
    height: 300,
    interval: 2000,
    active: false,
    auto: true,
    srcBCName: null,
    setProperties: function () {
        UCML.Slides.superclass.setProperties.call(this);
        this.width = this.el.attr("imgWidth") || this.width;
        this.height = this.el.attr("imgHeight") || this.height;
        this.interval = this.el.attr("interval") || this.interval;
        this.auto = this.el.attr("auto") || this.auto;
        this.active = (this.el.attr("active") == "true") ? true : this.active;

        if (this.el.attr("imgList")) {
            this.imgList = this.el.attr("imgList").split(";");
        }

        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("BCName")) || this.dataTable || undefined;
        this.imgField = this.el.attr("dataFld") || this.imgField;
    },
    setValue: function () {
        UCML.Slides.superclass.setValue.call(this);
    },
    bindEvents: function () {
        UCML.Slides.superclass.bindEvents.call(this);

        if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.imgField)) {
            this.srcDataTable.on("onLoad", this.bindData, this);
        } else {
            this.renderImgList();
        }

        var opts = this;
        $(document).on("click", "#" + this.id, function (e) {

            if (e.target.nodeName.toLowerCase() == "img") {
                var el = $(e.target);
                var sender = {};
                sender.ctl = opts;
                sender.el = el;
                sender.oid = el.attr("oid");
                if (!UCML.isEmpty(opts.srcDataTable)) opts.srcDataTable.LocateOID(sender.oid);
                opts.fireEvent("slidesClick", sender);
            }

        });
    }
});

UCML.Slides.prototype.getSysImgSrc = function () {
    var imgURL = "File/Images/" + this.srcDataTable.TableName + "_" + this.imgField + "/" + UCML.UserInfo.getUserOID() + "/" + this.srcDataTable.getOID() + this.srcDataTable.getFieldValue(this.imgField);
    return imgURL;
}

UCML.Slides.prototype.bindData = function () {
    var recordCount = this.srcDataTable.getRecordCount();
    if (recordCount > 0) {
        this.imgList = [];
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            var imgUrl = this.srcDataTable.getFieldValue(this.imgField);
            if (imgUrl == ".jpg" || imgUrl == ".png" || imgUrl == ".gif") imgUrl = this.getSysImgSrc();
            if (this.getImgSrc) imgUrl = this.getImgSrc();
            var obj = {};
            obj.url = imgUrl;
            obj.oid = this.srcDataTable.getOID();
            this.imgList.add(obj);
        }
        this.srcDataTable.SetIndexNoEvent(0);
        this.renderImgList();
    }
}

UCML.Slides.prototype.formatUrl = function (url) {
    if ((url.indexOf("http://") > -1) || (url.indexOf("https://") > -1)) { //网络资源
        return url;
    } else { //服务器资源
        url = UCMLLocalResourcePath + url;
        return url;
    }
}

UCML.Slides.prototype.renderImgList = function () {
    if (this.imgList.length > 0) {
        var imgStr = '';
        for (var i = 0; i < this.imgList.length; i++) {
            if (typeof this.imgList[i] == "object") {
                imgStr += '<img app="' + this.formatUrl(this.imgList[i].url) + '" oid="' + this.imgList[i].oid + '"/>';
            } else {
                imgStr += '<img app="' + this.formatUrl(this.imgList[i]) + '"/>';
            }
        }

        this.el.append(imgStr);
        this.el.slidesjs({
            play: { //播放设置
                active: false,
                auto: this.auto,
                interval: this.interval,
                effect: "slide", //"fade"
                swap: false
            },
            navigation: { //分页栏设置
                active: false
            },
            width: this.width,
            height: this.height
        });
    }
}

UCML.reg("UCML.Slides", UCML.Slides);

/**
*   九宫格菜单控件
*/
UCML.ComplexMenu = function (id) {
    this.menuUrl;
    this.menuLink;
    this.menuText;
    this.menuDivide;
    this.menuDivideList = [];

    this.addEvents("complexMenuClick");
    UCML.ComplexMenu.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ComplexMenu, UCML.Input, {
    ctype: "UCML.ComplexMenu",
    cols: 4,
    setValue: function (val, trigger) {
        UCML.ComplexMenu.superclass.setValue.call(this);
    },
    onRender: function () {
        UCML.ComplexMenu.superclass.onRender.call(this);

        this.el.addClass("nav-carousel-box");
    },
    setProperties: function () {
        UCML.ComplexMenu.superclass.setProperties.call(this);

        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("BCName")) || this.dataTable || undefined;
        this.menuUrl = this.el.attr("menuUrlField") || "";
        this.menuLink = this.el.attr("menuLinkField") || "";
        this.menuText = this.el.attr("menuTextField") || "";
        this.menuDivide = this.el.attr("menuTypeField") || undefined;
        this.cols = parseInt(this.el.attr("cols")) || this.cols;
    },
    bindEvents: function () {
        UCML.ComplexMenu.superclass.bindEvents.call(this);

        if (!UCML.isEmpty(this.srcDataTable)) {
            this.srcDataTable.on("onLoad", this.bindData, this);
        }

        var opts = this;

        $(document).on("click", "#" + this.id, function (e) {
            if (e.target.nodeName.toLowerCase() == "img") {
                var sender = {};
                sender.ctl = opts;
                sender.el = el;
                sender.oid = $(e.target).attr("oid");
                if (!UCML.isEmpty(opts.srcDataTable)) opts.srcDataTable.LocateOID(sender.oid);
                opts.fireEvent("complexMenuClick", sender);
            }
        });
    }
});

UCML.ComplexMenu.prototype.getSysImgSrc = function () {
    var imgURL = "File/Images/" + this.srcDataTable.TableName + "_" + this.menuUrl + "/" + UCML.UserInfo.getUserOID() + "/" + this.srcDataTable.getOID() + this.srcDataTable.getFieldValue(this.menuUrl);
    return imgURL;
}

UCML.ComplexMenu.prototype.bindData = function (data) {
    var recordCount = this.srcDataTable.getRecordCount();

    if (recordCount > 0) {
        this.getMenuDivideList();
        var menuStr = "";
        if (this.menuDivideList.length > 0) { //有分组的情况
            var menuTypeLength = this.menuDivideList.length;
            for (var m = 0; m < menuTypeLength; m++) {
                menuStr += '<ul class="slide_ul_header"><li>' + this.menuDivideList[m].text + '</li></ul>';
                menuStr += '<ul class="slide_ul"><li>';
                for (var i = 0; i < recordCount; i++) {
                    this.srcDataTable.SetIndexNoEvent(i);
                    if (this.menuDivideList[m].value == this.srcDataTable.getFieldValue(this.menuDivide)) {
                        var imgsrc = this.srcDataTable.getFieldValue(this.menuUrl);
                        if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
                        if (this.getImgSrc) imgsrc = this.getImgSrc();
                        //menuStr += '<a rel="external" href="' + this.srcDataTable.getFieldValue(this.menuLink) + '">' +
                        if (imgsrc == "") imgsrc = "Images/WeiXin/wx_default.jpg";
                        menuStr += '<a href="javascript:void(0)" >' +
                        '<img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/>' +
                            '<span>' + this.srcDataTable.getFieldValue(this.menuText) + '</span>' +
                            '</a>';
                    }
                }
                this.srcDataTable.SetIndexNoEvent(0);

                menuStr += '</li></ul>';
            }

        } else {
            menuStr += '<ul class="slide_ul"><li>';
            for (var i = 0; i < recordCount; i++) {
                this.srcDataTable.SetIndexNoEvent(i);
                var imgsrc = this.srcDataTable.getFieldValue(this.menuUrl);
                if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
                if (this.getImgSrc) imgsrc = this.getImgSrc();
                if (imgsrc == "") imgsrc = "Images/WeiXin/wx_default.jpg";
                //menuStr += '<a href="' + this.srcDataTable.getFieldValue(this.menuLink) + '">' +
                menuStr += '<a href="javascript:void(0)" >' +
                    '<img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/>' +
                    '<span>' + this.srcDataTable.getFieldValue(this.menuText) + '</span>' +
                    '</a>';
            }
            this.srcDataTable.SetIndexNoEvent(0);

            menuStr += '</li></ul>';
        }

        this.el.append(menuStr);

        var ratioWidth = (100 / this.cols) + "%";
        this.el.find("ul.slide_ul").find("a").css({
            "width": ratioWidth
        });
    }
}

UCML.ComplexMenu.prototype.formatUrl = function (url) {
    if ((url.indexOf("http://") > -1) || (url.indexOf("https://") > -1)) { //网络资源
        return url;
    } else { //服务器资源
        url = UCMLLocalResourcePath + url;
        return url;
    }
}

UCML.ComplexMenu.prototype.getMenuDivideList = function () {
    if (this.menuDivide) {
        var col = this.srcDataTable.getColumn(this.menuDivide);
        var recordCount = this.srcDataTable.getRecordCount();
        var menuTextArr = [];
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);

            var text = "";
            if (col.isCodeTable && col.codeTable) {
                text = BusinessObject.GetCodeCaption(col.codeTable, this.srcDataTable.getFieldValue(this.menuDivide));
            } else {
                text = this.srcDataTable.getFieldValue(this.menuDivide);
            }

            if (menuTextArr.indexOf(text) == -1) {
                menuTextArr.add(text);
                this.menuDivideList.add({
                    value: this.srcDataTable.getFieldValue(this.menuDivide),
                    text: text
                });
            }
        }
        this.srcDataTable.SetIndexNoEvent(0);
    }
}

UCML.reg("UCML.ComplexMenu", UCML.ComplexMenu);

/**
*   图片列表控件
*/
UCML.MImageList = function (id) {
    this.imgSrcField;
    this.descField;
    this.contentField;
    this.priceField;
    this.addEvents("imageListClick");

    //有加载更多
    this.hasMore = false;

    //高度设置
    this.hasSameHeight = false;
    this.sameHeight = null;

    UCML.MImageList.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MImageList, UCML.Input, {
    ctype: "UCML.MImageList",
    cols: 3,
    setValue: function (val, trigger) {
        UCML.MImageList.superclass.setValue.call(this);
    },
    onRender: function () {
        UCML.MImageList.superclass.onRender.call(this);

        this.el.addClass("nav-imagelist-box");
    },
    init: function () {
        UCML.MImageList.superclass.init.call(this);
        if (this.hasMore) {
            this.el.after(this.clickLoadMore);
        }
    },
    setProperties: function () {
        UCML.MImageList.superclass.setProperties.call(this);

        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("BCName")) || this.dataTable || undefined;
        this.imgSrcField = this.el.attr("imgSrcField") || this.imgSrc;
        this.descField = this.el.attr("descField") || this.descField;
        this.contentField = this.el.attr("contentField") || this.contentField;
        this.priceField = this.el.attr("priceField") || this.priceField;
        this.cols = parseInt(this.el.attr("cols")) || this.cols;
        this.imgList = this.el.attr("imgList") || "";
        this.imgDescList = this.el.attr("imgDescList") || "";

        this.listviewEl = $('<ul class="imglist_ul"></ul>');
        //this.moreEl = $('<div class="imagelist-navbar">点击加载更多</div>');

        //是否有滚动条
        this.hasMore = this.getAttribute("hasMore") == "true" ? true : this.hasMore;
        this.clickLoadMore = $('<div class="click-load-more"><a href="javascript:void(0);">点击加载更多</a></div>');

        //高度设置
        this.hasSameHeight = this.getAttribute("hasSameHeight") == "true" ? true : this.hasSameHeight;
        this.sameHeight = parseInt(this.getAttribute("sameHeight")) || null;
    },
    bindEvents: function () {
        UCML.MImageList.superclass.bindEvents.call(this);

        if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.imgSrcField)) {
            this.srcDataTable.isLoadHistoryData = true;
            this.srcDataTable.on("onLoad", this.bindData, this);
            this.srcDataTable.on("beforeGetData", this.clearData, this);
        } else {
            this.bindCustomData();
        }

        var opts = this;
        $(document).on("click", "#" + this.id, function (e) {
            var el = $(e.target);

            if (e.target.nodeName.toLowerCase() == "img") {
                var sender = {};
                sender.ctl = opts;
                sender.el = el;
                sender.oid = el.attr("oid");
                if (!UCML.isEmpty(opts.srcDataTable)) opts.srcDataTable.LocateOID(sender.oid);
                opts.fireEvent("imageListClick", sender);
            }
        });

        this.clickLoadMore.on("click", function (e) {
            opts.reserveData = true;
            opts.srcDataTable.LoadMoreData();
            opts.reserveData = false;
        });
    }
});

UCML.MImageList.prototype.clearData = function () {
    if (!this.reserveData) {
        this.srcDataTable.ClearData();
    }
}

UCML.MImageList.prototype.getSysImgSrc = function () {
    var imgURL = "File/Images/" + this.srcDataTable.TableName + "_" + this.imgSrcField + "/" + UCML.UserInfo.getUserOID() + "/" + this.srcDataTable.getOID() + this.srcDataTable.getFieldValue(this.imgSrcField);
    return imgURL;
}

UCML.MImageList.prototype.formatUrl = function (url) {
    if ((url.indexOf("http://") > -1) || (url.indexOf("https://") > -1)) { //网络资源
        return url;
    } else { //服务器资源
        url = UCMLLocalResourcePath + url;
        return url;
    }
}

UCML.MImageList.prototype.bindCustomData = function () {
    var imgStr = '';
    var imgArr = this.imgList.split(";");
    var imgDescArr = [];
    if (!UCML.isEmpty(this.imgDescList))
        imgDescArr = this.imgDescList.split(";");
    for (var i = 0; i < imgArr.length; i++) {
        imgStr += '<li><a href="javascript:void(0)">' +
            '<img alt oid app="' + this.formatUrl(imgArr[i]) + '"/>' +
            (!UCML.isEmpty(imgDescArr[i]) ? ('<span class="img-tip">' + imgDescArr[i] + '</span>') : '') +
            '</a></li>';
    }

    this.listviewEl.html("").append(imgStr);

    this.el.append(this.listviewEl);

    var ratioWidth = (100 / this.cols) + "%";
    this.el.find("ul.imglist_ul").find("li").css({
        "width": ratioWidth
    });
}

UCML.MImageList.prototype.bindData = function (data) {
    var recordCount = this.srcDataTable.getRecordCount();
    var imgStr = '';
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        var imgsrc = this.srcDataTable.getFieldValue(this.imgSrcField);
        if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
        if (this.getImgSrc) imgsrc = this.getImgSrc();

        imgStr += '<li><a href="javascript:void(0)">';
        imgStr += '<div class="pro-img-box"><img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/>';
        imgStr += (!UCML.isEmpty(this.descField) && !UCML.isEmpty(this.srcDataTable.getFieldValue(this.descField))) ? ('<span class="img-tip">' + this.srcDataTable.getFieldValue(this.descField) + '</span>') : '';
        imgStr += '</div>';
        if (this.contentField) {
            var content = this.srcDataTable.getFieldValue(this.contentField);
            imgStr += '<p class="pro-box"><span>' + content + '<span></p>';
        }
        if (this.priceField) {
            var price = this.srcDataTable.getFieldValue(this.priceField);
            imgStr += '<div class="pro-price"><span><em>￥' + price + '</em></span></div>';
        }
        imgStr += '</a></li>';
    }

    this.listviewEl.html("").append(imgStr);

    this.el.append(this.listviewEl);

    var ratioWidth = (100 / this.cols) + "%";
    this.el.find("ul.imglist_ul").find("li").css({
        "width": ratioWidth
    });

    //设置高度
    if (this.hasSameHeight) {
        var height = (this.sameHeight != null && !UCML.isEmpty(this.sameHeight)) ? this.sameHeight : this.el.find("img[oid]:first").height();
        this.el.find("img[oid]").height(height);
    }
}

UCML.reg("UCML.MImageList", UCML.MImageList);

UCML.MImageCheckList = function (id) {
    this.type = "check";
    this.align = "vertical"; //horizontal
    this.checkOID = [];
    UCML.MImageCheckList.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MImageCheckList, UCML.MImageList, {
    ctype: "UCML.MImageCheckList",
    setProperties: function () {
        UCML.MImageCheckList.superclass.setProperties.call(this);
        this.type = this.getAttribute("type") || this.type;
        this.align = this.getAttribute("align") || this.align;
    },
    onRender: function () {
        UCML.MImageList.superclass.onRender.call(this);
    },
    bindEvents: function () {
        UCML.MImageCheckList.superclass.bindEvents.call(this);

        var opts = this;
        this.el.on("click", ".imgcheck_item", function (e) {
            var el = $(this); //.imgcheck_item元素
            var oid = el.attr("itemoid");
            if (opts.srcDataTable) opts.srcDataTable.LocateOID(oid); //定位数据

            var thisEl = $(e.target); //当前点击的元素
            if (e.target.nodeName.toLowerCase() != "input") {
                var chkEl = el.find("input");
                if (chkEl.attr("type") == "checkbox") {
                    if (chkEl.attr("checked") == "checked") {
                        chkEl.removeAttr("checked");
                        opts.checkOID.remove(oid);
                    } else {
                        chkEl.attr("checked", "checked");
                        opts.checkOID.remove(oid);
                        opts.checkOID.add(oid);
                    }
                } else {
                    if (chkEl.attr("checked") == "checked") {

                    } else {
                        chkEl.attr("checked", "checked");
                    }
                }
            } else {
                if (thisEl.attr("type") == "checkbox") {
                    if (thisEl.attr("checked") == "checked") {
                        opts.checkOID.remove(oid);
                        opts.checkOID.add(oid);
                    }
                    else {
                        opts.checkOID.remove(oid);
                    }
                }
            }
        });
    }
});

UCML.MImageCheckList.prototype.getCheckOID = function () {
    return this.checkOID;
}

UCML.MImageCheckList.prototype.bindData = function (data) {
    var recordCount = this.srcDataTable.getRecordCount();
    var imgStr = '<div class="imgcheck_list">';
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        var imgsrc = this.srcDataTable.getFieldValue(this.imgSrcField);
        if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
        if (this.getImgSrc) imgsrc = this.getImgSrc();

        var checkname = this.id + "_check";
        imgStr += '<div class="imgcheck_item" itemoid="' + this.srcDataTable.getOID() + '">';
        if (this.align == "vertical") {
            if (this.type == "check") {
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_input"><input type="checkbox" name="' + checkname + '"></div>';
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_img"><img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/></div>';
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_span"><span>' + this.srcDataTable.getFieldValue(this.descField) + '</span></div>';
            }
            else if (this.type == "radio") {
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_input"><input type="radio" name="' + checkname + '"></div>';
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_img"><img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/></div>';
                imgStr += '<div class="imgcheck_item_cell imgcheck_item_span"><span>' + this.srcDataTable.getFieldValue(this.descField) + '</span></div>';
            }
        } else {
            if (this.type == "check") {
                imgStr += '<div class="imgcheck_item_img_outer"><img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/></div>';
                imgStr += '<div class="imgcheck_item_text_outer"><input type="checkbox" name="' + checkname + '">';
                imgStr += '<span>' + this.srcDataTable.getFieldValue(this.descField) + '</span></div>';
            }
            else if (this.type == "radio") {
                imgStr += '<div><img alt oid="' + this.srcDataTable.getOID() + '" app="' + this.formatUrl(imgsrc) + '"/></div>';
                imgStr += '<div><input type="radio" name="' + checkname + '">';
                imgStr += '<span>' + this.srcDataTable.getFieldValue(this.descField) + '</span></div>';
            }
        }
        imgStr += '</div>';
    }
    imgStr += '</div>';

    this.el.append(imgStr);

    var ratioWidth = (100 / this.cols) + "%";
    var css = {
        "width": ratioWidth
    };
    if (this.align == "vertical") { css["display"] = "table"; }
    this.el.find("div.imgcheck_list").find("div.imgcheck_item").css(css);

    //设置高度
    if (this.hasSameHeight) {
        var height = (this.sameHeight != null && !UCML.isEmpty(this.sameHeight)) ? this.sameHeight : this.el.find("img[oid]:first").height();
        this.el.find("img[oid]").height(height);
    }
}

UCML.reg("UCML.MImageCheckList", UCML.MImageCheckList);

UCML.VideoPlayHTML5 = function (id) {
    this.srcDataTable;
    UCML.VideoPlayHTML5.superclass.constructor.call(this, id);
}

UCML.extend(UCML.VideoPlayHTML5, UCML.Input, {
    ctype: "UCML.VideoPlayHTML5",
    autoPlay: false,
    controls: true,
    playerWidth: 320,
    playerHeight: 240,
    setValue: function (val, trigger) {
        UCML.VideoPlayHTML5.superclass.setValue.call(this);
    },
    onRender: function () {
        UCML.VideoPlayHTML5.superclass.onRender.call(this);
    },
    setProperties: function () {
        UCML.VideoPlayHTML5.superclass.setProperties.call(this);
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("BCName")) || this.dataTable || undefined;
        this.videoHref = this.getAttribute("videoLinkField") || this.videoHref || "";
        this.videoTitle = this.getAttribute("videoTitleField") || this.videoTitle || "";

        this.customVideoHref = this.getAttribute("customVideoHref") || this.customVideoHref || "";
        this.customVideoTitle = this.getAttribute("customVideoTitle") || this.customVideoTitle || "";

        this.autoPlay = (this.getAttribute("autoPlay") == "true") ? true : this.autoPlay;
        this.controls = (this.getAttribute("controls") == "false") ? false : this.controls;
        this.playerWidth = this.getAttribute("playerWidth") || this.playerWidth;
        this.playerHeight = this.getAttribute("playerHeight") || this.playerHeight;

        this.titleEl = $('<p></p>');
        this.videoEl = $('<video>您的浏览器不支持HTML5！</video>');
    },
    bindEvents: function () {
        UCML.VideoPlayHTML5.superclass.bindEvents.call(this);

        if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.videoHref)) {
            this.srcDataTable.on("onLoad", this.bindData, this);
        } else {
            this.bindCustomData();
        }
    }
});

UCML.VideoPlayHTML5.prototype.bindData = function (data) {
    var title = this.srcDataTable.getFieldValue(this.videoTitle);
    if (!UCML.isEmpty(title)) {
        this.titleEl.text(title);
        this.el.append(this.titleEl);
    }
    var linkHref = this.srcDataTable.getFieldValue(this.videoHref);
    if (!UCML.isEmpty(linkHref)) {
        this.videoEl.attr("src", this.formatUrl(linkHref));
        this.videoEl.attr("width", this.playerWidth + "px");
        this.videoEl.attr("height", this.playerHeight + "px");
        if (this.autoPlay) this.videoEl.attr("autoplay", "autoplay");
        if (this.controls) this.videoEl.attr("controls", "controls");
    } else {
        this.videoEl.text("视频地址为空！");
    }
    this.el.append(this.videoEl);
}

UCML.VideoPlayHTML5.prototype.bindCustomData = function () {
    if (!UCML.isEmpty(this.customVideoTitle)) {
        this.titleEl.text(this.customVideoTitle);
        this.el.append(this.titleEl);
    }
    var linkHref = this.customVideoHref;
    if (!UCML.isEmpty(linkHref)) {
        this.videoEl.attr("src", this.formatUrl(linkHref));
        this.videoEl.attr("width", this.playerWidth + "px");
        this.videoEl.attr("height", this.playerHeight + "px");
        if (this.autoPlay) this.videoEl.attr("autoplay", "autoplay");
        if (this.controls) this.videoEl.attr("controls", "controls");
    } else {
        this.videoEl.text("视频地址为空！");
    }
    this.el.append(this.videoEl);
}

UCML.VideoPlayHTML5.prototype.formatUrl = function (url) {
    if ((url.indexOf("http://") > -1) || (url.indexOf("https://") > -1)) { //网络资源
        return url;
    } else { //服务器资源
        url = UCMLLocalResourcePath + url;
        return url;
    }
}

UCML.reg("UCML.VideoPlayHTML5", UCML.VideoPlayHTML5);