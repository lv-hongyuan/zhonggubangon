﻿/**
* @class UCML.Input
* @extends UCML.Commponent
* @description 表单控件基类
* @param {String} id 容器控件id
*/
UCML.Input = function (id) {
    this.value = "";

    this.addEvents("valuechange", "setValue");

    UCML.Input.superclass.constructor.call(this, id);
};

UCML.extend(UCML.Input, UCML.Commponent, {
    ctype: "UCML.Input",
    setProperties: function () {
        UCML.Input.superclass.setProperties.call(this);
        this.disabled = (this.el.attr('disabled') ? true : this.disabled);
    }, select: function () {
        if (this.dom.select) {
            this.dom.select();
        }
    }
});

UCML.Input.prototype.init = function () {
    UCML.Input.superclass.init.call(this);
    //私有  
    if (this.disabled) {
        this.disable();
    }
}


UCML.Input.prototype.setValue = function (val, trigger) {

    this.value = val;
    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }
    if (this.tagName == "input" || this.tagName == "textarea") {
        this.el.val(val);
    }
    else {
        this.el.html(val);
    }
}

UCML.Input.prototype.setValueBack = function (val) {
    this.setValue(val, false);
}

UCML.Input.prototype.setLabel = function () {
    var p = this.el.parent();
    var value = this.getValue();
    if (this.getText) {
        value = this.getText();
    }
    this.destroy();
    p.append('<div class="">' + value + '</div>');
}


UCML.Input.prototype.resize = function (width, height) {
    this.setSize(width, height);
}

UCML.Input.prototype.setSize = function (width, height) {
    if ($.boxModel == true) {
        this.setWidth(width - (this.getOuterWidth(true) - this.getWidth()));
        this.setHeight(height - (this.getOuterHeight(true) - this.getHeight()));
    } else {
        this.setWidth(width - (this.getOuterWidth(true) - this.getInnerWidth()));
        this.setHeight(height - (this.dom.offsetHeight - this.dom.clientHeight));
    }
}


/**
*获得匹配元素的当前值
*select 如果多选，将返回一个数组，其包含所选的值
*/
UCML.Input.prototype.getValue = function () {
    return this.value;
}

/**   
* @method focus 
* @description 获得焦点     
*/
UCML.Input.prototype.focus = function () {
    this.dom.focus();
}

UCML.Input.prototype.blur = function () {
    this.dom.blur();
}

UCML.reg("UCML.Input", UCML.Input);

/*-----------TextBox-----------------*/

UCML.TextBox = function (id) {
    UCML.TextBox.superclass.constructor.call(this, id);
    return this;
}
UCML.extend(UCML.TextBox, UCML.Input, {
    ctype: "UCML.TextBox", autoEl: 'input'
});

UCML.TextBox.prototype.init = function () {
    UCML.TextBox.superclass.init.call(this);
    this.on("blur", function () {
        this.setValue(this.dom.value);
    }, this);
}

UCML.TextBox.prototype.setMaxLength = function (val) {
    this.el.attr("maxLength", val);
}

UCML.TextBox.prototype.select = function () {
    if (this.dom.select) {
        this.dom.select();
    }
}

UCML.reg("UCML.TextBox", UCML.TextBox);

/*-----------Password-----------------*/

UCML.Password = function (id) {
    UCML.Password.superclass.constructor.call(this, id);
}
UCML.extend(UCML.Password, UCML.TextBox, {
    ctype: "UCML.Password"
});

UCML.reg("UCML.Password", UCML.Password);

/*-----------textarea-----------------*/

UCML.TextArea = function (id) {
    UCML.TextArea.superclass.constructor.call(this, id);
}
UCML.extend(UCML.TextArea, UCML.TextBox, {
    ctype: "UCML.TextArea",
    autoEl: 'textarea'
});

UCML.reg("UCML.TextArea", UCML.TextArea);

/*-----------CheckBox-----------------*/

UCML.CheckBox = function (id) {
    UCML.CheckBox.superclass.constructor.call(this, id);
}
UCML.extend(UCML.CheckBox, UCML.Input, {
    ctype: "UCML.CheckBox"
});

UCML.CheckBox.prototype.init = function () {
    UCML.CheckBox.superclass.init.call(this);
    this.on("change", function () {
        this.setValue(this.dom.checked ? "true" : "false");
    }, this);
}

UCML.CheckBox.prototype.setValue = function (val, trigger) {
    this.value = val;
    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }
    if (val == "true" || val == true) {
        this.dom.checked = true;
    }
    else {
        this.dom.checked = false;
    }
}
UCML.reg("UCML.Checkbox", UCML.CheckBox);
UCML.reg("UCML.CheckBox", UCML.CheckBox);

/*-----------Radio-----------------*/

UCML.Radio = function (id) {
    UCML.Radio.superclass.constructor.call(this, id);
}
UCML.extend(UCML.Radio, UCML.CheckBox, {
    ctype: "UCML.Radio"
});

UCML.reg("UCML.Radio", UCML.Radio);

/*-----------Hidden-----------------*/

UCML.Hidden = function (id) {
    UCML.Hidden.superclass.constructor.call(this, id);
}
UCML.extend(UCML.Hidden, UCML.TextBox, {
    ctype: "UCML.Hidden"
});

UCML.reg("UCML.Hidden", UCML.Hidden);

/*-----------Image-----------------*/
UCML.Image = function (id) {
    UCML.Image.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Image, UCML.Input, {
    ctype: "UCML.Image"
});

UCML.Image.prototype.setValue = function (val, trigger) {
    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }
    this.value = val;
    this.dom.src = UCMLLocalResourcePath + "File/Images/" + this.dataTable.TableName + "_" + this.fieldName + "/" + this.dataTable.getUserOID() + "/" + this.dataTable.getOID() + val;
}

UCML.reg("UCML.Image", UCML.Image);

/*-----------UploadImage-----------------*/
UCML.UploadImage = function (id) {
    UCML.UploadImage.superclass.constructor.call(this, id);
}

UCML.extend(UCML.UploadImage, UCML.Input, {
    ctype: "UCML.UploadImage"
});

UCML.UploadImage.prototype.setValue = function (val, trigger) {
    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }
    this.value = val;
    this.dom.src = "upload/" + val;
}
UCML.reg("UCML.UploadImage", UCML.UploadImage);

/*-----------Article-----------------*/
UCML.Article = function (id) {
    UCML.Article.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Article, UCML.Input, {
    ctype: "UCML.Article",
    onRender: function () {
        UCML.Article.superclass.onRender.call(this);
        var p = $('<p></p>');
        this.el.append(p);
        this.el.css("text-indent", "2em");
    },
    setValue: function (val, trigger) {
        this.value = val;
        if (trigger !== false) {
            this.fireEvent("setValue", val);
        }
        this.el.find("p").text(val);
    }
});

UCML.reg("UCML.Article", UCML.Article);

/*-----------Label-----------------*/
UCML.Label = function (id) {
    UCML.Label.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Label, UCML.Input, {
    ctype: "UCML.Label"
});

UCML.Label.prototype.setValue = function (val, trigger) {
    var valData = val;
    if (this.isCodeTable) {
        val = this.BusinessObject.GetCodeCaption(this.codeTable, val) || "";
    }
    this.value = val;
    this.el.html(val);
    var urlkind = this.el.attr("urlkind");
    if (urlkind == "phone") {
        if (this.el.attr("href") != undefined)
            this.el.attr("href", "tel:" + val);
    }
    else if (urlkind == "sms") {
        if (this.el.attr("href") != undefined)
            this.el.attr("href", "sms:" + val);
    }

    var iconSrc = this.getAttribute("icon") || "";
    if (!UCML.isEmpty(iconSrc)) {
        var icon = '<img class="ui-li-icon" app="' + iconSrc + '">';
        //this.el.before(icon);
        this.el.html(icon + '<span>' + val + '</span>');
        this.el.parents("ul[data-role='listview']").eq(0).listview('refresh');
    }

    var align = this.getAttribute("align") || "left";
    if (align != "left") {
        this.el.find("span").addClass("ui-text-" + align);
    }

    if (trigger !== false) {
        this.fireEvent("setValue", valData);
    }
}

UCML.reg("UCML.Label", UCML.Label);

UCML.Link = function (id) {
    UCML.Link.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Link, UCML.Input, {
    ctype: "UCML.Link",
    autoEl: 'a'
});

UCML.reg("UCML.Link", UCML.Link);

UCML.BCLink = function (id) {

    UCML.BCLink.superclass.constructor.call(this, id);
}

UCML.extend(UCML.BCLink, UCML.Link, {
    ctype: "UCML.BCLink",
    linkState: 0, //连接状态 0，未初始化，1，下载中，2装载完毕
    bindEvents: function () {
        UCML.BCLink.superclass.bindEvents.call(this);

        this.on("click", this.click, this);
    },
    click: function () {
        startBCLink(this.dom);
    },
    setAppletObject: function (val) {
        //  this.onQuickQuery();
        this.AppletObject = val;
    }
});

UCML.reg("UCML.BCLink", UCML.BCLink);

UCML.BCLink.prototype.setValue = function (val, trigger) {

    if (val == "") {
        val = "请选择";
    }
    this.value = val;
    this.dom.innerHTML = val;
    UCML.BCLink.superclass.setValue.call(this, val, trigger);
}

UCML.HTMLEdit = function (id) {
    UCML.HTMLEdit.superclass.constructor.call(this, id);
}

UCML.extend(UCML.HTMLEdit, UCML.Input, {
    ctype: "UCML.HTMLEdit",
    autoEl: 'div',
    init: function () {
        UCML.HTMLEdit.superclass.init.call(this);

    },
    onRender: function () {
        if (this.tagName == "textarea") {
            this.editor = $(this.dom).xheditor({ skin: 'o2007silver' });
        }
        else {
            this.el.append("<textarea></textarea>");
            this.editor = this.el.find("textarea").xheditor({ skin: 'vista' });
        }
    },
    bindEvents: function () {
        UCML.HTMLEdit.superclass.bindEvents.call(this);
        var opts = this;
        this.editor.settings.blur = function () { opts.setValue(opts.editor.getSource(), true); };
    },
    setValue: function (val, trigger) {
        if (trigger) {
            this.fireEvent("setValue", val);
        }
        else {
            this.editor.setSource(val);
        }
    }
});

UCML.reg("UCML.HTMLEdit", UCML.HTMLEdit);

UCML.reg("UCML.Html", UCML.HTMLEdit);
/**
* @class UCML.InputList
* @extends UCML.Input
* @description 列表表单控件基类
* @param {String} id 容器控件id
*/
UCML.InputList = function (id) {

    /**   
    * @property dataValueField 
    * @description 值绑定字段
    * @type String
    */
    this.dataValueField;

    /**   
    * @property dataTextField 
    * @description 文本绑定字段
    * @type String
    */
    this.dataTextField;

    /**   
    * @property private srcDataTable 
    * @description 数据源BC
    * @type UCML.DataTable
    */
    this.srcDataTable;

    this.codeTable;

    this.isCodeTable = false;

    /**
    * 自定义的文本，逗号隔开
    */
    this.Captions;

    /**
    * 自定义的值，逗号隔开
    */
    this.Values;

    this.text;

    //定义事件
    this.addEvents("setData", "setreadonly", "onitem", "databind");

    UCML.InputList.superclass.constructor.call(this, id);
};

UCML.extend(UCML.InputList, UCML.Input, {
    ctype: "UCML.InputList",
    setProperties: function () {
        UCML.InputList.superclass.setProperties.call(this);
        this.codeTable = this.getAttribute("codeTable") || this.codeTable;
        this.isCodeTable = (this.el.attr('isCodeTable') ? this.getAttribute('isCodeTable')
== 'true' : false) || this.isCodeTable;
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute
("srcBCName")) || undefined;
        this.dataValueField = this.getAttribute("dataValueField") || this.dataValueField;
        this.dataTextField = this.getAttribute("dataTextField") || this.dataTextField;
        this.Captions = this.getAttribute("Captions") || this.Captions;
        this.Values = this.getAttribute("Values") || this.Values;

    }, getText: function () {
        return this.text;
    }
});

UCML.InputList.prototype.init = function () {
    UCML.InputList.superclass.init.call(this);
    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.srcDataTable.on("onLoad", this.dataBind, this);
        //    this.srcDataTable.on("OnFieldChange", this.SrcfieldChange, this);
    }
    else if (this.isCodeTable == true && !UCML.isEmpty(this.codeTable)) {
        if (window.BusinessObject.dataLoading) {
            this.bindCodeTable();
        }
        else {
            window.BusinessObject.on("onLoad", this.bindCodeTable, this);
        }

    } else if (!UCML.isEmpty(this.Captions) && !UCML.isEmpty(this.Values)) {
        //自定义接口
        this.bindCustomData(this.Captions, this.Values);
    }
};

/**
* @method bindCustomData
* @description 绑定自定义数据
* @在各个实例中实现
*/
UCML.InputList.prototype.bindCustomData = function (texts, values) {

}

/**   
* @method getSrcDataTable 
* @description 获取数据源BC      
* @return {UCML.DataTable} 
*/
UCML.InputList.prototype.getSrcDataTable = function () {
    return this.srcDataTable;
};

/**   
* @method setSrcDataTable 
* @description 设置数据源BC
* @param {UCML.DataTable} dataTable 数据源BC        
*/
UCML.InputList.prototype.setSrcDataTable = function (dataTable) {
    this.srcDataTable = dataTable;
    //  this.srcDataTable.AddConnectControls(this);
}


/**   
* @method dataBind 
* @description 绑定控件   
*/
UCML.InputList.prototype.dataBind = function () {
    //    this.input.disabled = this.disabled;
    //    if (this.hidden) {
    //        this.hide();
    //    }

    this.fireEvent("databind", this, this.input);
}

UCML.InputList.prototype.bindCodeTable = function () {
}



