﻿/*!
*UCML.Report 报表显示控件
*Copyright 2013,UCML Co.Ltd
*History: 
*2013-02-25	完成基本接口定义
*2013-03-04	修复报表导出功能
*/

UCML.Report = function (id) {
    UCML.Report.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Report, UCML.Applet, {
    ReportID: null,
    ReportObject: null,
    DataPacketXML: null,
    /*
    *@Attribute:ShowPrintDialog
    *@Description:打印时是否显示打印设置对话框
    */
    ShowPrintDialog: true,
    /*
    *@Method: createReportObject
    *@Description: 创建报表控件对象
    *@Param: null
    */
    createReportObject: function () {
        var url = window.location.toString();
        var path = url.substring(0, url.lastIndexOf('/') + 1);
        var rootPath = path + window.UCMLLocalResourcePath;
        var sHTML = '<OBJECT id="UCMLREPORT" classid="clsid:77439687-CD41-4D51-A48B-2CE97239A2F9" codeBase="' + rootPath + 'plugin/UCMLReport2.ocx#version=2012,12,25,1"></OBJECT>';
        this.ReportObject = this.el.append(sHTML).find("#UCMLREPORT").width("100%").height("100%").get(0);
        if (!this.ReportObject) alert("创建报表控件失败！");
        else {
            var opts = this;
            if (this.ReportObject.attachEvent) {
                this.ReportObject.attachEvent("OnSaveReport", function (data) {
                    opts.saveReport(data);
                });
                this.ReportObject.attachEvent("OnAfterPrint", function () {
                    opts.afterPrint();
                });
            }
            else {
		var handler=document.createElement('script');
                handler.setAttribute("for", "UCMLREPORT");
                handler.event = "OnSaveReport(data)";
                handler.appendChild(document.createTextNode("" + opts.id + ".saveReport(data);"));
                document.body.appendChild(handler);

                //this.ReportObject.addEventListener("OnAfterPrint", function () {
                //    opts.afterPrint();
                //});
            }
        }
    },
    /*
    *@Method: show
    *@Description: 加载报表控件数据并显示报表
    *@Param: null
    */
    show: function () {
        if (!this.ReportObject) this.createReportObject();
        this.BusinessObject.ShowMessage('正在加载报表...');
        this.loadData();
        this.showReport();
    },

    /*
    *@Method: showReport
    *@Description: 显示报表控件
    *@Param: null
    */
    showReport: function () {
        var url = window.location.toString();
        var path = url.substring(0, url.lastIndexOf('/') + 1);
        var rootPath = path + window.UCMLLocalResourcePath;

        var rootTable = this.BusinessObject.getRootTable();

        this.ReportObject.GetReportRes(rootPath);

        var theMainDataPacket = this.createXMLObject();
        theMainDataPacket.loadXML(this.DataPacketXML);

        this.ReportObject.GetReportData(this.BusinessObject.BPOName, rootPath, this.DataPacketXML, this.dataTable.BCName);

        var BCList = this.BusinessObject.getBCList();
        for (var i = 0; i < BCList.length; i++) {
            this.ReportObject.SetBCData(theMainDataPacket, BCList[i].BCName);
        }
        //	this.setMainIndex(rootTable);
        this.ReportObject.SetBCIndex(rootTable.BCName, rootTable.getOID());
        this.setBCIndex(rootTable.getDetailTables());

        //根据报表ID号加载报表定义数据
        this.ReportObject.GetReportShape(rootPath + "OtherSource/UCMLReport2.aspx?ReportNo=" + this.ReportID);
        this.BusinessObject.HideMessage();
        this.ReportObject.ShowReport();
    },

    /*
    *@Method: loadData
    *@Description: 加载报表数据
    *@Param: null
    */
    loadData: function () {
        /*
        this.BusinessObject.invoke(
        "",
        "InitBusinessEnv",
        false,
        {},
        function(o,xml){
        alert(xml);
        this.DataPacketXML=xml;
        },
        function(o,text){
        alert(text);
        },
        this,
        false);*/
        this.DataPacketXML = this.BusinessObject.getXMLData();
    },

    /*
    *@Method: createXMLObject
    *@Description: 创建XML对象(报表控件参数需要)
    *@Param: null
    */
    createXMLObject: function () {
        try {
            var xmlObject = new ActiveXObject("Msxml2.DOMDocument");
            return xmlObject;
        }
        catch (e) {
            try {
                xmlObject = new ActiveXObject("Msxml.DOMDocument");
                return xmlObject;
            }
            catch (e) {
                try {
                    xmlObject = new ActiveXObject("Microsoft.XMLDOM");
                    return xmlObject;
                }
                catch (e) {
                    errorHandler(e, "createXMLObject");
                }
            }
        }
    },

    /*
    *@Method: setBCIndex
    *@Description: 递归设置子表BC信息
    *@Param: detailTables
    * detailTables: 子表BC数组
    */
    setBCIndex: function (detailTables) {
        if (detailTables == null) return;
        for (var i = 0; i < detailTables.length; i++) {
            if (detailTables[i].getRecordCount() > 0) {
                this.ReportObject.SetBCIndex(detailTables[i].BCName, detailTables[i].getOID());
                this.setBCIndex(detailTables[i].getDetailTables());
            }
        }
    },

    setMainIndex: function (mainTables) {
        if (mainTables == null) return;
        for (var i = 0; i < mainTables.getRecordCount(); i++) {
            mainTables.setRecordIndex(i);
            this.ReportObject.SetBCIndex(mainTables.BCName, mainTables.getOID());
            this.setBCIndex(mainTables.getDetailTables());
        }
    },
    /*
    *@Method: designReport
    *@Description: 在客户端设计报表样式
    *@Param: null
    *@Comment: 该方法暂时不支持
    */
    designReport: function () {
        // this.el.hide();
        this.show();
        this.ReportObject.DesignReport();
    },
    designReportNet: function (data) {
        if (!this.ReportObject) this.createReportObject();
        this.BusinessObject.ShowMessage('正在加载报表...');
        this.ReportObject.DesignReportNet(data);
        this.BusinessObject.HideMessage();
    },
    /*
    *@Method: saveReport
    *@Description: 客户端保存报表设计时的回调函数
    *@Param: data
    * data: 报表定义的数据
    *@Comment: 该方法暂时不支持
    */
    saveReport: function (data) {
        this.BusinessObject.invoke(
        "",
        "SaveReportDefine",
        false,
        { ReportNo: this.ReportID, ReportData: data },
        function (o, xml) {
            //    alert("报表定义保存成功");
            if (this.onAfterPrint && typeof this.onAfterPrint == "function") {
                this.onAfterPrint.call();
            }
        },
        function (o, text) {
            //    alert("报表定义保存失败：" + text);
            if (this.onAfterPrint && typeof this.onAfterPrint == "function") {
                this.onAfterPrint.call();
            }
        },
        this,
        true);
    },
    onSaveReport: null,
    afterPrint: function () {
        if (this.onAfterPrint && typeof this.onAfterPrint == "function") {
            this.onAfterPrint.call();
        }
    },
    onAfterPrint: null,
    /*
    *@Method: printReport
    *@Description: 打印报表
    *@Param: null
    */
    print: function () {
        this.ReportObject.ShowPrintDialog = this.ShowPrintDialog;
        this.ReportObject.Print();
    },

    /*
    *@Method: exportPDF
    *@Description: 导出报表为PDF文件
    *@Param: fileName(导出的文件名)
    */
    exportPDF: function (fileName) {
        this.ReportObject.ExportPDF(fileName);
        this.showReport();
    },

    /*
    *@Method: exportHTML
    *@Description: 导出报表为HTML文件
    *@Param: fileName(导出的文件名)
    */
    exportHTML: function (fileName) {
        this.ReportObject.ExportHTML(fileName);
        this.showReport();
    },

    /*功能有问题
    *@Method: exportJPEG
    *@Description: 导出报表为JPEG文件
    *@Param: fileName(导出的文件名)
    */
    exportJPEG: function (fileName) {
        this.ReportObject.ExportJPG(fileName);
        this.showReport();
    },

    /*功能有问题
    *@Method: exportBMP
    *@Description: 导出报表为BMP文件
    *@Param: fileName(导出的文件名)
    */
    exportBMP: function (fileName) {
        this.ReportObject.ExportBMP(fileName);
        this.showReport();
    },

    /*功能有问题
    *@Method: exportExcel
    *@Description: 导出报表为Excel文件
    *@Param: fileName(导出的文件名)
    */
    exportExcel: function (fileName) {
        this.ReportObject.ExportExcel(fileName);
        this.showReport();
    },

    /*
    *@Method: exportRTF
    *@Description: 导出报表为RTF文件
    *@Param: fileName(导出的文件名)
    */
    exportRTF: function (fileName) {
        this.ReportObject.ExportRTF(fileName);
        this.showReport();
    },

    /*
    *@Method: showToolbar
    *@Description: 设置报表控件是否显示工具栏
    *@Param: value
    * value: 要设置的值
    *@return: 一个bool值表示当前控件是否显示工具栏
    */
    showToolbar: function (value) {
        if (typeof value == "boolean") this.ReportObject.DisplayToolbar = value.toString();
    },

    /*
    *@Method: hasToolbar
    *@Description: 获取当前报表控件是否显示工具栏
    *@Param: null
    *@return: 一个bool值表示当前控件是否显示工具栏
    */
    hasToolbar: function () {
        return this.ReportObject.DisplayToolbar;
    },
    /*
    *@Method: reportHtmlWebService
    *@Description: 通过报表ID用HTML方式显示报表
    *@Param: ReportID 报表id
    *@Param: StartPos 起始行
    *@Param: Records 读取记录数
    *@Param: ReportParam 查询条件
    *@return: 
    */
    reportHtmlWebService: function (ReportID, StartPos, Records, ReportParam) {
        GetReportURL(ReportID, StartPos, Records, ReportParam, this.succeeded_ReportHtmlWebService, this.failed_ReportHtmlWebService, this);
    },
    succeeded_ReportHtmlWebService: function (result, text) {
        if (!this.ReportHTMLFrame) {
            this.ReportHTMLFrame = $("<iframe  scrolling=yes width=100% height=100% frameborder=0 id=UCMLBPO_View" + this.id + " ></IFRAME>");
            this.el.append(this.ReportHTMLFrame);
        }
        this.ReportHTMLFrame.attr("src", (UCMLLocalResourcePath + text));
    },
    failed_ReportHtmlWebService: function (result, text) {
        alert("报表显示错误：" + text);
    },
    reportSrvWebService: function (ReportID, StartPos, Records, ReportParam) {
        if (!this.ReportObject) this.createReportObject();
        GetSrvReportResult(ReportID, StartPos, Records, ReportParam, this.succeeded_ReportSrvWebService, this.failed_ReportHtmlWebService, this);
    },
    succeeded_ReportSrvWebService: function (result, text) {
        this.ReportObject.SetPrepareData(text);
    }
});