﻿UCML.namespace("UCML.Portal");

/**
* @class UCML.Portal.PortalList
* @extends UCML.Applet
* @description 门户列表
* @param {object} id 容器控件id
*/
UCML.Portal.PortalList = function (id) {
    UCML.Portal.PortalList.superclass.constructor.call(this, id);
};
/**
* @class UCML.Edit
* @extends UCML.Applet
* @description 编辑控件,继承自UCML.Applet
*/
UCML.extend(UCML.Portal.PortalList, UCML.Applet, {
    ctype: "UCML.Portal.PortalList",
    captionField: null,
    dateField: null,
    onItemClick: null,
    captionWidth: "63%",
    dateWidth: "32%",
    onRender: function () {
        UCML.Portal.PortalList.superclass.onRender.call(this);
        this.bodyTable = $("<TABLE style=width:100%;border:0px; cellSpacing=1 cellPadding=3 width=100%><TBODY></TBODY></TABLE>");
        this.el.append(this.bodyTable);
        for (var i = 0; i < this.columns.length; i++) {
            if (this.columns[i].fieldName == this.captionField && this.columns[i].objBCLinkColl != null) {
                this.captionColumn = this.columns[i];
                break;
            }
        }
    },
    bindEvents: function () {
  
        UCML.Portal.PortalList.superclass.bindEvents.call(this);
        if (!UCML.isEmpty(this.dataTable)) {
            this.dataTable.on("onLoad", this.loadData, this);
        }
    },
    subStr: function (str, length) {
        try {
            if (str != null) {
                var a = str.match(/[^\x00-\xff]|\w{1,2}/g);
                return a.length < length ? str : a.slice(0, length).join("") + "…";
            } else {
                return str;
            }
        } catch (e) {
            return str;
        }
    }
    ,
    renderCaption: function () {
        var captionValue = this.dataTable.getFieldValue(this.captionField);
        captionValue = this.subStr(captionValue, 17);
        return captionValue;
    }
    ,
    loadData: function () {
        this.bodyTable.empty();
        if (this.captionField && this.dateField) {
            var rowCount = this.dataTable.getRecordCount();
            for (var i = 0; i < rowCount; i++) {
                this.dataTable.SetIndexNoEvent(i);
                var captionValue = this.renderCaption();
                
                var dataValue = this.dataTable.getFieldValue(this.dateField);

                var OID = this.dataTable.getOID();
                var rowStr = "<tr OID=" + OID + "><td class=portalList-caption width=" + this.captionWidth + "><a href=### OID=" + OID + ">" + captionValue + "</a></td><td class=portalList-data width=" + this.dateWidth + ">" + dataValue + "</td></tr>";
                this.bodyTable.append(rowStr);
            }
        }

        var opts = this;
        this.bodyTable.find("td.portalList-caption a").bind("click", function () {
            if (opts.onItemClick) {
                var OID = $(this).attr("OID");
                opts.dataTable.LocateOID(OID, opts);
                opts.onItemClick(opts.dataTable.getCurrentData(), opts.dataTable);
            }
        });
    }
});


UCML.namespace("UCML.Portal");

/**
* @class UCML.Portal.Slide 
* @extends UCML.Applet
* @description 门户列表
* @param {object} id 容器控件id
*/
UCML.Portal.Slide = function (id) {
    UCML.Portal.Slide.superclass.constructor.call(this, id);
};
/**
* @class UCML.Edit
* @extends UCML.Applet
* @description 编辑控件,继承自UCML.Applet
*/
UCML.extend(UCML.Portal.Slide, UCML.Applet, {
    ctype: "UCML.Portal.Slide ",
    showType: 'fieldImg', //img:图片
    imgSrcField: null,
    paginationAlign: 'center',
    itemContentCls: 'slide-itemContent',
    slidesConfig: null,
    onRender: function () {
        this.slidesConfig = {
            preload: true,
            play: 4000
        };
        UCML.Portal.Slide.superclass.onRender.call(this);
    },
    bindEvents: function () {
        UCML.Portal.Slide.superclass.bindEvents.call(this);
        if (!UCML.isEmpty(this.dataTable)) {
            this.dataTable.on("onLoad", this.loadData, this);
        }
    },
    renderItemContent: function (OID, fieldValues) {
        var value = "";

        if (this.showType == "fieldImg" && this.imgSrcField) {
            value += " <IMG style=\"FILTER: progid:DXImageTransform.Microsoft.RadialWipe(duration=2); BACKGROUND-COLOR: gold; WIDTH: 100%; HEIGHT: 300px\" border=0 align=absMiddle app="
            value += "File/upload/DOCData/" + OID + "/" + this.dataTable.getFieldValue(this.imgSrcField);
            value += " >";
        }
        return value;
    }
    ,
    loadData: function () {

        if (!this.slidesContainer) {
            this.el.empty();
            this.slidesContainer = $("<div class=\"slides_container\"></div>");
            this.el.append(this.slidesContainer);

            if (this.renderItemContent) {
                var rowCount = this.dataTable.getRecordCount();
                for (var i = 0; i < rowCount; i++) {
                    this.dataTable.SetIndexNoEvent(i);
                    var OID = this.dataTable.getOID();
                    var itemContent = this.renderItemContent(OID, this.dataTable.getCurrentData());
                    this.slidesContainer.append("<div OID=" + OID + " class=" + this.itemContentCls + ">" + itemContent + "</div>");
                }

                this.el.slides(this.slidesConfig);

                if (this.paginationAlign == 'center') {
                    this.el.find(".pagination").width(this.el.find(" > .pagination li").outerWidth(true) * rowCount)
                }
            }

        }
    }
});

UCML.Portal.Custom = function(id) {
       UCML.Portal.Custom.superclass.constructor.call(this, id);
};

UCML.extend(UCML.Portal.Custom, UCML.Applet, {
    ctype: "UCML.Portal.Custom",
    imgSrcField: null,
    paginationAlign: 'center',
    itemContentCls: 'slide-itemContent',
    slidesConfig: null,
    imgUrl: 'File/',
    imageurl: '',
    showType: '',
    customType: 'true',
    hrefurl: '',
    nameTitle: '',
    customString: '',
    isYScroll: 'false',
    sColumn: '',
    footurl: '',
    onRender: function() {
        this.slidesConfig = {
            preload: true,
            play: 4000
        };
        UCML.Portal.Slide.superclass.onRender.call(this);
    },
    bindEvents: function() {
        UCML.Portal.Slide.superclass.bindEvents.call(this);
        if (!UCML.isEmpty(this.dataTable)) {
            this.dataTable.on("onLoad", this.loadData, this);
        }
    },
    subStr: function(str, length) {
        try {
            if (str != null) {
                var a = str.match(/[^\x00-\xff]|\w{1,2}/g);
                return a.length < length ? str : a.slice(0, length).join("") + "…";
            } else {
                return str;
            }
        } catch (e) {
            return str;
        }
    },
    renderItemContent: function(OID, fieldValues) {
        var value = "";
        var customTypeEx = this.dataTable.getFieldValue(this.customType);
        var showTypeEx = this.dataTable.getFieldValue(this.showType);

        var Atitle = this.dataTable.getFieldValue(this.nameTitle)
        var AtitleEx = this.subStr(Atitle, 12);
        if (customTypeEx == 'false') {//非自定义
            if (showTypeEx == '0') {//只显示标题
                value += "<a href='" + this.dataTable.getFieldValue(this.footurl) + this.dataTable.getFieldValue(this.hrefurl) + "' target='_blank'   style='text-decoration: none; color:#494b4c'><div style='height:100%;width:100%;' title='" + Atitle + "'>";
                value += AtitleEx + "</div></a><p/>";
            } else if (showTypeEx == '1') {//只显示图片
                value += "<a href='" + this.dataTable.getFieldValue(this.footurl) + this.dataTable.getFieldValue(this.hrefurl) + "' target='_blank' style='text-decoration: none; color:#494b4c'><div style='height:100%;width:100%;'>";
                value += "<img app='" + this.imgUrl + this.dataTable.getFieldValue(this.imageurl) +
                         "/" + this.dataTable.getFieldValue(this.imgSrcField) + "' style='border:0' /></div></a><p/>";
            } else if (showTypeEx == '2') { //标题图片都显示
                value += "<a href='" + this.dataTable.getFieldValue(this.footurl) + this.dataTable.getFieldValue(this.hrefurl) + "' target='_blank' style='text-decoration: none; color:#494b4c'><div style='height:100%;width:100%;'>";
                value += "<img app='" + this.imgUrl + this.dataTable.getFieldValue(this.imageurl) +
                         "/" + this.dataTable.getFieldValue(this.imgSrcField) + "' style='border:0'/>" +
                          this.dataTable.getFieldValue(this.nameTitle) +
                         "</div></a><p/>";
            }

        } else if (customTypeEx == 'true') { //自定义
            value = this.dataTable.getFieldValue(this.customString);
        }
        return value;
    }
    ,

    loadData: function() {

        if (!this.slidesContainer) {
            this.el.empty();

            this.slidesContainer = $("<div style='overflow:hidden;height:100%;width:100%'></div>");
            var ISYS = this.dataTable.getFieldValue(this.sColumn);
            var marqueestring = "";

            if (ISYS == "true") {//有滚动
                marqueestring = "<marquee behavior='scroll' direction=up width='200px' height='250px' scrollamount='5' " +
                                "onmouseout='this.start()' onmouseover='this.stop()' ></marquee>";
            } else {
            marqueestring = "<table style=width:100%;border:0px; cellSpacing=1 cellPadding=3 width=100%></table>";
            }


            var marquee = $(marqueestring);
            this.el.append(this.slidesContainer);
            this.slidesContainer.append(marquee);
            var columns = this.dataTable.getFieldValue(this.sColumn) / 1; //分栏数
            if (this.renderItemContent) {
                var itemtrstr = "";
                var rowCount = this.dataTable.getRecordCount();
                for (var i = 0; i < rowCount; i++) {
                    this.dataTable.SetIndexNoEvent(i);
                    var OID = this.dataTable.getOID();
                    var itemContent = this.renderItemContent(OID, this.dataTable.getCurrentData());
                    if (ISYS == "true") {//有滚动
                    } else {
                        if (columns > 1) {
                            var x = (i + 1) % (columns / 1);
                            if (x == 0 || (i + 1) == (columns / 1)) {
                                itemContent = "<td class=portalList-caption >" + itemContent + "</td></tr>";
                            } else if (x == 1 || i == 0) {
                            itemContent = "<tr><td class=portalList-caption >" + itemContent + "</td>";
                            } else {
                            itemContent = "<td class=portalList-caption >" + itemContent + "</td>";
                            }


                        } else {//无分栏
                        itemContent = "<tr><td class=portalList-caption >" + itemContent + "</td></tr>";
                        }
                        itemtrstr = itemtrstr + itemContent;

                    }

                }
                marquee.append(itemtrstr);
                var CMS_Section_FK = this.dataTable.getFieldValue("CMS_Section_FK");
                rowmoreStr = "<tr><td colspan='" + columns + "' style='text-align:right'> <a href=### onclick=window.showModalDialog('CMS/aegis/"+                                                 "BPO_PortalSectionQuery.aspx?CMS_Section_FK=" + CMS_Section_FK + "',window,'dialogWidth:710px;dialogHeight:400px') >>>更多</a>"+
                            "</td></tr>";
                marquee.append(rowmoreStr);
            }

        }
    }
});