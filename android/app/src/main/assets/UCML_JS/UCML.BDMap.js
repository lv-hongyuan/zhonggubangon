UCML.BDMap = function (id) {

    this.id = id;
    this.map = {};

    UCML.BDMap.superclass.constructor.call(this, id);
}

UCML.extend(UCML.BDMap, UCML.Applet, {
    mapkey: "",//百度地图开发者账号
    initLongitude: 116.404, //初始化时经度
    initLatitude: 39.915, //初始化时纬度
    initLevel: 11,//初始化级别
    isInitName: false,//是否地名初始化位置
    initName: "北京",

    isNavigation: true, //是否添加平移缩放控件
    isScale: true, //是否添加比例尺控件
    isOverview: false, //是否添加缩略图控件
    isMapType: false,//是否添加地图类型控件
    isMarker: false,//当中心为点时可设置是否设置标签

    onRender: function () {

        UCML.BDMap.superclass.onRender.call(this);  
    },
    setProperties: function () {

        UCML.BDMap.superclass.setProperties.call(this);
    },
    open: function () { 

        UCML.BDMap.superclass.open.call(this);

        /** 异步加载
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.app = "http://api.map.baidu.com/api?v=2.0&ak="+this.mapkey+"&callback=mapinit";
        document.body.appendChild(script);
        */
    },
    init: function () {
        UCML.BDMap.superclass.init.call(this);

        this.map = new BMap.Map(this.id);

        var initPoint;
        if(this.isInitName) {
            initPoint = this.initName;
        } else {
            initPoint = new BMap.Point(this.initLongitude, this.initLatitude);
            if(this.isMarker) {
                var marker = this.createMarker(initPoint);
                this.addOverlay(marker);
            }
        }

        this.map.centerAndZoom(initPoint, this.initLevel);

        if(this.isNavigation) {
            this.map.addControl(new BMap.NavigationControl());
        }
        if(this.isScale) {
            this.map.addControl(new BMap.ScaleControl());
        }
        if(this.isOverview) {
            this.map.addControl(new BMap.OverviewMapControl());
        }
        if(this.isMapType) {
            this.map.addControl(new BMap.MapTypeControl());
        }
    },
    bindEvents: function () {
        UCML.BDMap.superclass.bindEvents.call(this);
        /**
        * map tilesloaded 图块加载完
        * map click 地图单击
        * overlay click 覆盖物点击
        */
    }
});

UCML.BDMap.prototype.clean = function () {//关闭地图

}

UCML.BDMap.prototype.hide = function () {//隐藏地图
    this.el.hide();
}

UCML.BDMap.prototype.show = function () {//显示地图
    this.el.show();
}

/**
* @description 返回地图控件dom
*/
UCML.BDMap.prototype.getMapContainer = function () {
    return this.map.getContainer();
}

/**
* @description 设置中心点
*/
UCML.BDMap.prototype.setCenter = function (point) {
    if(typeof point === "object") {
        var p = new BMap.Point(point.longitude, point.latitude);
        this.map.setCenter(p);
    }else {
        this.map.setCenter(point);
    }
}

/**
* @description 移动中心点
*/
UCML.BDMap.prototype.panCenter = function (point, isMarker) {

    this.map.panTo(point);
    if(isMarker) {
        this.addOverlay(this.createMarker(point));
    }
}

/**
* @description 恢复初始化设置
*/
UCML.BDMap.prototype.restore = function () {
    this.map.reset();
}

/**
* @description 设置类型
* 可选参数: BMAP_NORMAL_MAP 普通街道视图
            BMAP_PERSPECTIVE_MAP 透视图像视图
            BMAP_SATELLITE_MAP 展示卫星视图
            BMAP_HYBRID_MAP 卫星和路网的混合视图
*/
UCML.BDMap.prototype.setType = function (type) {
    this.map.setMapType(type);
}

/**
* @description 放大一个级别
*/
UCML.BDMap.prototype.zoomIn = function () {
    this.map.zoomIn();
}

/**
* @description 缩小一个级别
*/
UCML.BDMap.prototype.zoomOut = function () {
    this.map.zoomOut();
}

/**
* @description 设置地图级别
*/
UCML.BDMap.prototype.setZoom = function (zoom) {
    this.map.setZoom(zoom);
}

UCML.BDMap.prototype.getCurrentLocation = function () {

}

UCML.BDMap.prototype.showUserLocation = function () {

}

/**
* @description 设置地图显示的城市
*/
UCML.BDMap.prototype.setCurrentCity = function (cityName) {
    this.map.setCurrentCity(cityName);
}

/**
* @description 设置滚轮放大缩小,默认禁用
*/
UCML.BDMap.prototype.setScrollWheelZoom = function (disable) {
    if(disable==undefined || disable === true) {
        this.map.enableScrollWheelZoom();
    }else {
        this.map.disableScrollWheelZoom();
    }
}

/**
* @description 启用地图拖拽，默认启用
*/
UCML.BDMap.prototype.setDragging = function (disable) {
    if(disable == undefined || disable === true) {
        this.map.enableDragging();
    }else {
        this.map.disableDragging();
    }
}

/**
* @description 启用双击放大，默认启用
*/
UCML.BDMap.prototype.setDoubleClickZoom = function (disable) {
    if(disable == undefined || disable === true) {
        this.map.enableDoubleClickZoom();
    }else {
        this.map.disableDoubleClickZoom();
    }
}

/**
* @description 启用双指操作缩放，默认启用
*/
UCML.BDMap.prototype.setPinchToZoom = function (disable) {
    if(disable == undefined || disable === true) {
        this.map.enablePinchToZoom();
    }else {
        this.map.disablePinchToZoom();
    }
}

/**
* @description 启用自动适应容器尺寸变化，默认启用
*/
UCML.BDMap.prototype.setAutoResize = function (disable) {
    if(disable == undefined || disable === true) {
        this.map.enableAutoResize();
    }else {
        this.map.disableAutoResize();
    }
}

/**
* @description 设置地图允许的最小、最大级别
*/
UCML.BDMap.prototype.setMinMaxZoom = function (min, max) {
    if(min != null) {
        this.map.setMinZoom(min);
    }
    if(max != null) {
        this.map.setMaxZoom(max);
    }
}

//--- 地图状态 ---//
/**
* @description 返回地图当前中心点, return BMap.Point
*/
UCML.BDMap.prototype.getCenter = function () {
    return this.map.getCenter();
}

/**
* @description 返回两点之间的距离，单位是米, return number
*/
UCML.BDMap.prototype.getDistance = function (startP, endP) {

    return this.map.getDistance(startP, endP);
}

/**
* @description 返回地图类型
*/
UCML.BDMap.prototype.getMapType = function () {
    return this.map.getMapType();
}

/**
* @description 返回地图当前缩放级别
*/
UCML.BDMap.prototype.getZoom = function () {
    return this.map.getZoom();
}

//---添加控件  移除控件---//
/**
* @description 添加地图类型控件
*/
UCML.BDMap.prototype.addMapTypeControl = function (position) {
    //top-left,top-right,bottom-left,bottom-right
    var option = {anchor: BMAP_ANCHOR_TOP_LEFT};
    if(position == "top-left") {

    }else if(position == "top-right") {
        option.anchor = BMAP_ANCHOR_TOP_RIGHT;
    }else if(position == "bottom-left") {
        option.anchor = BMAP_ANCHOR_BOTTOM_LEFT;
    }else if(position == "bottom-right") {
        option.anchor = BMAP_ANCHOR_BOTTOM_RIGHT;
    }

    this.map.addControl(new BMap.MapTypeControl(option));
}

UCML.BDMap.prototype.removeMapTypeControl = function () {
    this.map.removeControl(new BMap.MapTypeControl());
}

/**
* @description 添加比例尺控件
*/
UCML.BDMap.prototype.addScaleControl = function (position) {
    //top-left,top-right,bottom-left,bottom-right
    var option = {anchor: BMAP_ANCHOR_TOP_LEFT};
    if(position == "top-left") {

    }else if(position == "top-right") {
        option.anchor = BMAP_ANCHOR_TOP_RIGHT;
    }else if(position == "bottom-left") {
        option.anchor = BMAP_ANCHOR_BOTTOM_LEFT;
    }else if(position == "bottom-right") {
        option.anchor = BMAP_ANCHOR_BOTTOM_RIGHT;
    }

    this.map.addControl(new BMap.ScaleControl(option));
}

UCML.BDMap.prototype.removeScaleControl = function () {
    this.map.removeControl(new BMap.ScaleControl());
}

/**
* @description 添加平移缩放控件
*/
UCML.BDMap.prototype.addNavigationControl = function (position, type) {
    //top-left,top-right,bottom-left,bottom-right
    //normal--正常,small--仅包含平移和缩放按钮,pan--仅包含平移按钮,zoom--仅包含缩放按钮
    var option = {anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_LARGE};
    if(position == "top-left") {

    }else if(position == "top-right") {
        option.anchor = BMAP_ANCHOR_TOP_RIGHT;
    }else if(position == "bottom-left") {
        option.anchor = BMAP_ANCHOR_BOTTOM_LEFT;
    }else if(position == "bottom-right") {
        option.anchor = BMAP_ANCHOR_BOTTOM_RIGHT;
    }

    if(type == "normal") {

    }else if(type == "small") {
        option.type = BMAP_NAVIGATION_CONTROL_SMALL;
    }else if(type == "pan") {
        option.type = BMAP_NAVIGATION_CONTROL_PAN;
    }else if(type == "zoom") {
        option.type = BMAP_NAVIGATION_CONTROL_ZOOM;
    }

    this.map.addControl(new BMap.NavigationControl(option));
}

UCML.BDMap.prototype.removeNavigationControl = function () {
    this.map.removeControl(new BMap.NavigationControl());
}

/**
* @description 添加缩略图控件
* @param top-left,top-right,bottom-left,bottom-right
*        normal--正常,small--仅包含平移和缩放按钮,pan--仅包含平移按钮,zoom--仅包含缩放按钮
*/
UCML.BDMap.prototype.addOverviewControl = function (position) {
    
    var option = {anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_LARGE};
    if(position == "top-left") {

    }else if(position == "top-right") {
        option.anchor = BMAP_ANCHOR_TOP_RIGHT;
    }else if(position == "bottom-left") {
        option.anchor = BMAP_ANCHOR_BOTTOM_LEFT;
    }else if(position == "bottom-right") {
        option.anchor = BMAP_ANCHOR_BOTTOM_RIGHT;
    }

    this.map.addControl(new BMap.OverviewMapControl(option));
}

UCML.BDMap.prototype.removeOverviewControl = function () {
    this.map.removeControl(new BMap.OverviewMapControl());
}

/**
* @description 添加自定义控件示例
*/
UCML.BDMap.prototype.addCustomControl = function() {
    //先定义一个控件类即function
    function ZoomControl() {
        this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
        this.defaultOffset = new BMap.Size(10, 10);
    }

    //通过prototype属性进行继承控件类
    ZoomControl.prototype = new BMap.Control();
    //必须实现自己的initialize方法，并返回控件的DOM元素
    //创建自己的控件容器，并添加到map中
    ZoomControl.prototype.initialize = function (map) {
        var div = document.createElement('div');
        div.appendChild(document.createTextNode('放大2级'));
        div.style.cursor = 'pointer';
        div.style.border = '1px solid gray';
        div.style.backgroundColor = 'white';
        div.onclick = function (e) {
            map.setZoom(map.getZoom() + 2);
        }
        map.getContainer().appendChild(div);
        return div;
    }

    //创建控件实例
    var myZoomCtl = new ZoomControl();
    this.map.addControl(myZoomCtl);
}

/**
* @description 添加全景控件
* @param offset 偏移量
*/
UCML.BDMap.prototype.addPanoramaControl = function (offset) {
    var stCtl = new BMap.PanoramaControl();
    stCtl.setOffset(new BMap.Size(offset.top, offset.left));

    this.map.addControl(stCtl);
}

//--- 覆盖物 ---//

/**
* @description 创建坐标点对象
* @param longitude 经度 latitude 维度
*/
UCML.BDMap.prototype.createPoint = function(longitude, latitude) {
    return new BMap.Point(longitude, latitude);
}

/**
* @description 创建标注
* @param point：点对象
* @return 标注对象
*/
UCML.BDMap.prototype.createMarker = function (point) {//创建点
    
    return new BMap.Marker(point);
}

/**
* @description 标注是否可拖拽,默认不可拖拽
* @param marker 标注对象  dragging 为true时可拖拽，否则不可
*/
UCML.BDMap.prototype.markerDragging = function (marker, dragging) {
    if(dragging) {
        marker.enableDragging();
    }else {
        marker.disableDragging();
    }
}

/**
* @description 创建折线
* @param ptArr:点数组,通过createPoint创建
* @param config:配置属性，对象有属性strokeColor、strokeWeight、strokeOpacity 例如{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5}
* @return 折线对象
*/
UCML.BDMap.prototype.createPolyline = function (ptArr, config) {

    return new BMap.Polyline(ptArr, config);
}

/**
* @description 创建圆
* @param point 点对象,通过createPoint创建
* @param radius 半径
* @param config:配置属性，对象有属性strokeColor、strokeWeight、strokeOpacity 例如{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5}
* @return 圆对象
*/
UCML.BDMap.prototype.createCircle = function (point, radius, config) {

    return new BMap.Circle(point, radius, config);
}

/**
* @description 创建多边形
* @param ptArr:点数组
* @param config:配置属性，对象有属性strokeColor、strokeWeight、strokeOpacity 例如{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5}
* @return 折线对象
*/
UCML.BDMap.prototype.createPolygon = function (ptArr, config) {

    return new BMap.Polygon(ptArr, config);
}

/**
* @description 创建文字标签
* @param point 文本所在点位置
* @param text 文本
* @param config 偏移量 offset,对象有属性top left
* @param css css属性对象格式
*/
UCML.BDMap.prototype.createLabel = function (point, text, config, css) {
    var opts = {
        position: point,
        offset: new BMap.Size(config.top, config.left)
    }
    var label = new BMap.Label(text, opts);
    label.setStyle(css);

    return label;
}

/**
* @description 创建弧线
* @description 创建多边形
* @param ptArr:点数组
* @param config:配置属性，对象有属性strokeColor、strokeWeight、strokeOpacity 例如{strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5}
* @return 弧线对象
*/
UCML.BDMap.prototype.createCurveLine = function (ptArr, config) {

    return new BMapLib.CurveLine(ptArr, config);
}

/**
* @description 添加信息窗口
* @param text 信息窗口显示的文本
* @param config 信息窗口的属性对象,无则使用默认,例如opts={width:200,height:100,title:"xxx",enableMessage:true,message:"xxx"}
* @return 返回信息窗口对象
*/
UCML.BDMap.prototype.createInfoWindow = function (text, config) {

    return new BMap.InfoWindow(text, config);
}

/**
* @description 打开信息窗口
* @param infoWindow 信息窗口对象
* @param point 信息窗口显示点
*/
UCML.BDMap.prototype.openInfoWindow = function (infoWindow, point) {

    this.map.openInfoWindow(infoWindow, point);
}

UCML.BDMap.prototype.getInfoWindowContent = function (infoWindow) {

    return infoWindow.getContent();
}

/**
* @description 线、面可编辑控制
* @param ploy Polygon或Ployline对象
*/
UCML.BDMap.prototype.ployEditing = function (ploy, edited) {
    if(edited) {
        ploy.enableEditing();
    }else {
        ploy.disableEditing();
    }
}

/**
* @description 添加覆盖物
* @param overlay 以上几种创建的对象(点、线、多边形等)
*/
UCML.BDMap.prototype.addOverlay = function (overlay) {

    this.map.addOverlay(overlay);
}

/**
* @description 清除覆盖物
*/
UCML.BDMap.prototype.removeOverlay = function () {

    this.map.clearOverlays();
}

/**
* @description 隐藏显示覆盖物
* @param overlay 覆盖物对象 如marker、ployline、circle等
* @param visible 是否显示 true显示、false隐藏
*/
UCML.BDMap.prototype.setOverlayVisible = function (overlay, visible) {
    if(visible) {
        overlay.show();
    }else {
        overlay.hide();
    }
}

/**
* @description 设置点覆盖物并配置其效果
* @param point 覆盖物点坐标
* @param action 动作
             1: MAP_ANIMATION_BOUNCE(跳动) 
             2:图片 需配置config,属性有url、top、left
             3: 添加Label 则config为label对象
*/
UCML.BDMap.prototype.setMarkerAndAction = function (point, action, config) {
    var marker;
    
    if(action == 1) {//跳动
        marker = new BMap.Marker(point);
        marker.setAnimation(MAP_ANIMATION_BOUNCE);
    }else if(action == 2) {//图片
        var myIcon = new BMap.Icon(config.url, new BMap.Size(config.top, config.left));
        marker = new BMap.Marker(point, {icon: myIcon});
    }else if(action == 3) {//label
        marker = new BMap.Marker(point);
        marker.setLabel(config);
    }

    this.map.addOverlay(marker);
}

/**
* @description 浏览器定位
*/
UCML.BDMap.prototype.getPositionByBrowse = function () {
    var geolocation = new BMap.Geolocation();

    geolocation.getCurrentPosition(function(r){
        if(this.getStatus() == BMAP_STATUS_SUCCESS) {
            var point = r.point;
        }else {
            alert('failed'+this.getStatus);
        }
    }, {enableHighAccuracy: true});
    //关于状态码
    //BMAP_STATUS_SUCCESS   检索成功。对应数值“0”。
    //BMAP_STATUS_CITY_LIST 城市列表。对应数值“1”。
    //BMAP_STATUS_UNKNOWN_LOCATION  位置结果未知。对应数值“2”。
    //BMAP_STATUS_UNKNOWN_ROUTE 导航结果未知。对应数值“3”。
    //BMAP_STATUS_INVALID_KEY   非法密钥。对应数值“4”。
    //BMAP_STATUS_INVALID_REQUEST   非法请求。对应数值“5”。
    //BMAP_STATUS_PERMISSION_DENIED 没有权限。对应数值“6”。(自 1.1 新增)
    //BMAP_STATUS_SERVICE_UNAVAILABLE   服务不可用。对应数值“7”。(自 1.1 新增)
    //BMAP_STATUS_TIMEOUT   超时。对应数值“8”。(自 1.1 新增)
}

/**
* @description 根据关键字检索
* @param keys 单个关键字字符串 或者 多个关键字组成的数组
* 若需显示查询结果，则在dom中需有显示结果的div，例如id为r-result，则在renderOptions配置中添加panel:"r-result"
    即renderOptions:{map: map, panel:"r-result"}
*/
UCML.BDMap.prototype.searchByKeys = function (keys) {
    var local = new BMap.LocalSearch(this.map, {
        renderOptions: {map: this.map}
    });
    local.search(keys);
}