﻿UCML.Messager = function (id) {
    UCML.Messager.superclass.constructor.call(this, id);
}

UCML.Messager.moveShow = function (win, type, speed, timeout) {
    if (!win) return;

    switch (type) {
        case null:
            win.show();
            break;
        case 'slide':
            win.slideDown(speed);
            break;
        case 'fade':
            win.fadeIn(speed);
            break;
        case 'show':
            win.show(speed);
            break;
    }

    var timer = null;
    if (timeout > 0) {
        timer = setTimeout(function () {
            UCML.Messager.moveHide(win, type, speed);
        }, timeout);
    }
    win.hover(
				function () {
				    if (timer) {
				        clearTimeout(timer);
				    }
				},
				function () {
				    if (timeout > 0) {
				        timer = setTimeout(function () {
				            UCML.Messager.moveHide(win, type, speed);
				        }, timeout);
				    }
				}
		)

}

UCML.Messager.moveHide = function (win, type, speed) {
    if (!win) return;

    switch (type) {
        case null:
            win.hide();
            break;
        case 'slide':
            win.slideUp(speed);
            break;
        case 'fade':
            win.fadeOut(speed);
            break;
        case 'show':
            win.hide(speed);
            break;
    }

    setTimeout(function () {
        win.remove();
    }, speed);
}

UCML.extend(UCML.Messager, UCML.util.Observable, {
    ok: '确定',
    cancel: '取消',
    bodyCls: "messager-body",
    show: function (options) {
        var opts = $.extend({
            showType: 'slide',
            showSpeed: 600,
            width: 250,
            height: 100,
            msg: '',
            title: '',
            timeout: 4000
        }, options || {});

        var body = $('<div class="messager-body"></div>').html(opts.msg);
        var win = new UCML.Window({
            title: opts.title,
            width: opts.width,
            height: opts.height,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            closable: true,
            body: body,
            rendeTo: window.document.body,
            shadow: false,
            draggable: false,
            resizable: false,
            closed: true,
            onBeforeOpen: function () {
                UCML.Messager.moveShow(win.el, opts.showType, opts.showSpeed, opts.timeout);
                return false;
            },
            onBeforeClose: function () {
                UCML.Messager.moveHide(win.el, opts.showType, opts.showSpeed);
                return false;
            }
        });

        // set the message window to the right bottom position
        win.el.css({
            left: null,
            top: null,
            right: 0,
            bottom: -document.body.scrollTop - document.documentElement.scrollTop
        });
        win.open();
    },
    alert: function (title, msg, icon, fn) {
        var content = '<div>' + (msg || "") + '</div>';
        switch (icon) {
            case 'error':
                content = '<div class="messager-icon messager-error"></div>' + content;
                break;
            case 'info':
                content = '<div class="messager-icon messager-info"></div>' + content;
                break;
            case 'question':
                content = '<div class="messager-icon messager-question"></div>' + content;
                break;
            case 'warning':
                content = '<div class="messager-icon messager-warning"></div>' + content;
                break;
        }
        content += '<div style="clear:both;"/>';

        var buttons = {};

        buttons[this.ok] = function () {
            win.dialog({ closed: true });
            if (fn) {
                fn();
                return false;
            }
        };

        var opts = this;

        buttons[this.ok] = function () {
            opts.window.close();
            if (fn) {
                fn();
                return false;
            }
        };

        this.window = this.createDialog(title, content, buttons);

    },
    confirm: function (title, msg, fn) {
        var content = '<div class="messager-icon messager-question"></div>'
					+ '<div>' + msg + '</div>'
					+ '<div style="clear:both;"/>';
        var buttons = {};
        var opts = this;
        buttons[this.ok] = function () {
            opts.window.close();
            if (fn) {
                fn(true);
                return false;
            }
        };
        buttons[this.cancel] = function () {
            opts.window.close();
            if (fn) {
                fn(false);
                return false;
            }
        };
        this.window = this.createDialog(title, content, buttons);
    },

    prompt: function (title, msg, fn) {
        var content = '<div class="messager-icon messager-question"></div>'
						+ '<div>' + msg + '</div>'
						+ '<br/>'
						+ '<input class="messager-input" type="text"/>'
						+ '<div style="clear:both;"/>';
        var buttons = {};
        var opts = this;
        buttons[this.ok] = function () {
            win.window('close');
            if (fn) {
                fn($('.messager-input', win).val());
                return false;
            }
        };
        buttons[this.cancel] = function () {
            win.window('close');
            if (fn) {
                fn();
                return false;
            }
        };
        this.window = this.createDialog(title, content, buttons);
    },
    createDialog: function (title, content, buttons) {
        var body = $('<div class="messager-body">' + content + '</div>');
        if (buttons) {
            var tb = $('<div class="messager-button"></div>').appendTo(body);
            for (var label in buttons) {
                //                $('<a></a>').attr('href', 'javascript:void(0)').text(label)
                //							.css('margin-left', 10)
                //							.bind('click', eval(buttons[label])).appendTo(tb);
                new UCML.Linkbutton({ text: label, rendeTo: tb, onClick: buttons[label] });
                //	.appendTo(tb).linkbutton();
            }
        }
        var win = new UCML.Window({
            title: title,
            width: 300,
            height: 'auto',
            modal: true,
            body: body,
            rendeTo: window.document.body,
            collapsible: false,
            minimizable: false,
            maximizable: false,
            resizable: false,
            closable: true,
            onClose: function () {
                setTimeout(function () {
                    win.destroy();
                }, 100);
            }
        });
        return win;
    }
});


UCML.alert = function (title, msg, icon, fn) {
    return new UCML.Messager().alert(title, msg, icon, fn);
}

UCML.confirm = function (title, msg, icon, fn) {
    return new UCML.Messager().confirm(title, msg, fn);
}



//window.alert = function (msg) {
//    UCML.alert("消息提示", msg);
//};

//window.confirm = function (msg) {
//    UCML.confirm("消息提示", msg);
//};

