/*
 *流程意见控件
 *date:2013/06/06
 */

UCML.SuggestView_Mobile = function (id) {
    //意见的BC字段名称
	this.ActivityIDField="ActivityID";					//分组字段,以活动编号分组
    this.ActivityNameField="ActivityName";				//分组标签字段,显示活动名称
    this.PersonNameField = "PersonName"; 				//审批人
    this.DateField = "SYS_Created"; 					//创建日期
    this.OpinionTypeField = "BusiField1"; 				//审批结果
    this.OpinionField = "Idea"; 						//审批意见
    this.ActivityChineseName = "ActivityChineseName"; 	//任务名称
    this.FinishDate = "FinishDate"; 					//完成时间
		this.FileNameField = "FileName";					//文件名
		this.HaveFiles=true;
		var OpinionCodeArray = [["0", "已阅"], ["1", "同意"], ["2", "不同意"]];
	 
    this.HaveHead = true;
    
    UCML.SuggestView_Mobile.superclass.constructor.call(this, id); //必须调用父类初始化
}

UCML.extend(UCML.SuggestView_Mobile, UCML.Applet, {
	SuggestItems:[],
	FileBCName:"BC_WF_AssignTaskFiles",
    FileM_FK :"AssignTaskOID",
	IsAllowViewIdea:true, 		//是否过滤意见内容，对应工作流活动节点'禁止浏览意见的节点'功能。默认为过滤。
	IsShowEmpty:false,			//是否显示意见为空的数据，默认为不显示
	bindEvents:function(){
		UCML.Applet.superclass.bindEvents.call(this);
        this.dataTable.on("onLoad", this.loadData, this);
        this.fileDataTable=eval(this.FileBCName + "Base");
        this.fileDataTable.on("onLoad", this.loadData, this);
	},
	/*
	 *从BC中加载数据
	 */
	loadData:function(){
		if(!this.dataTable.dataStore||!this.fileDataTable.dataStore){
			return false;	
		}
        this.SuggestItems=[];
        for (var i = 0; i < this.dataTable.getRecordCount(); i++) {
            this.dataTable.SetIndexNoEvent(i);
            if (this.IsShowEmpty == false && this.dataTable.getFieldValue(this.OpinionField) == '') continue;
            if (this.IsAllowViewIdea) {
                var NotViewIdea = this.dataTable.getFieldValue('NotAllowViewIdeaNodes');
               
                if (NotViewIdea.indexOf(this.dataTable.getFieldValue(this.ActivityIDField)) != -1) {
                    continue;
                }
            }
			
            var suggest = new UCML.SuggestView_Mobile.WFSuggest_Mobile();
            suggest.OID = this.dataTable.getFieldValue(this.dataTable.TableName + "OID");
            suggest.PersonName = this.dataTable.getFieldValue(this.PersonNameField);
            suggest.Date = this.dataTable.dateFilterValue(this.dataTable.getFieldValue(this.DateField),'y-m-d h:mi:s');
            suggest.OpinionType = this.dataTable.getFieldValue(this.OpinionTypeField);
            suggest.Opinion = this.dataTable.getFieldValue(this.OpinionField);
            suggest.ActivityChineseName = this.dataTable.getFieldValue(this.ActivityChineseName);
            suggest.FinishDate = this.dataTable.getFieldValue(this.FinishDate);
            if (this.HaveFiles == true || this.HaveFiles == "true") {
                suggest.Files = this.GetFilesByMOID(suggest.OID);
            }
            this.SuggestItems.push(suggest);
        }
		//渲染内容
		this.box.children().remove();
		if(this.SuggestItems.length!=0){
			var thisControl=this;
			$.each(this.SuggestItems,function(index,el){
				var $table=$(String.format(thisControl.ItemHTML,el.PersonName,el.OpinionType,el.FinishDate,el.Opinion,el.Files,el.Date,index,0,el.ActivityChineseName));
				$table.find("img").click(thisControl.toggle).attr("src","../images/SuggestView/flex1.gif");
				$table.find('a[oid]').click(thisControl.downloadFile);
				thisControl.box.append($table);
				//thisControl.box.append($table).find('table a[oid]').unbind("click").click(thisControl.downloadFile);	
			});
			
		}
		
		$.each($("table"), function(index, el) {
			var opts = $(this);
        	var $tr = opts.find("tr").first();
        	$tr.bind("tap", function(event) {
        		var $img = $(this).find("img");
				var index=parseInt($img.attr("index"));
				var state=parseInt($img.attr("state"));
				if(state == 0) {
					$img.attr("src","../images/SuggestView/flex.gif").attr("state",1);;
				}else {
					$img.attr("src","../images/SuggestView/flex1.gif").attr("state",0);
				}
				opts.find("tr>td").toggle();
				event.stopPropagation();
				return false;
        	});
        });
	},
	
	ItemHTML:'<table cellpadding="0" cellspacing="0" bordercolor="#c5c4c2"><tr><th class="form_th" colspan=2><span>任务名称：<b>{8}</b></span><span class="flex"><a href="javascript:void(0)" ><img width="10" height="10" index="{6}" state="{7}"/></a></span></th></tr><tr><td class="td1" width="20%">执行人</td><td class="td2">{0}</td></tr><tr><td class="td1">执行结果</td><td class="td2">{1}</td></tr><tr><td class="td1">完成时间</td><td class="td2">{2}</td></tr><tr><td class="td1">执行意见</td><td class="td2">{3}</td></tr><tr><td class="td3">文件信息</td><td class="td4">{4}&nbsp;</td></tr><tr><td class="td1">创建日期</td><td class="td2">{5}</td></tr></table>',

	/*
	 *显示控件
	 */
	onRender:function(){
		this.box=$('<div class="table_box"><div class="upload_process_bar"><div class="upload_current_process"></div></div><div class="process_info"></div></div>');
		this.el.append(this.box);
	},

	showItem:function(index){
	},
	toggle:function(event){
		var index=parseInt($(this).attr("index"));
		var state=parseInt($(this).attr("state"));
		if(state==0) {
			$(this).attr("src","../images/SuggestView/flex.gif").attr("state",1);;
		}
		else {
			$(this).attr("src","../images/SuggestView/flex1.gif").attr("state",0);
		}
		$("table").eq(index).find("tr>td").toggle();
		event.stopPropagation();
		return false;
	},
	
	downloadFile:function (event)
	{
		var opts = this;
		var OID = $(opts).attr("oid");
		if(OID==null || OID=="") return false;

		//var opts = this;
		var url = getServicePath()+"ucml_mobile/getFile.ashx?RECORDOID="+OID;
		$.ajax({
			url: url,
			dataType: "jsonp",
			jsonpCallback:"jsonpcallback",
			success: function(data) {
				window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
						fileSystem.root.getDirectory("Download", {create:true}, 
							function(fileEntry) {

								var filePath = fileEntry.fullPath + "/" + data.filePath.substr(data.filePath.lastIndexOf("\/")+1);
								var uri = encodeURI(getServicePath() + data.filePath);
								try {
									var fileTransfer = new FileTransfer();

									//下载
									fileTransfer.download(uri, filePath,
										function(entry){
											alert("下载成功");
										},
										function(error){
											alert("下载失败");
										}
										);

									//获取下载进度
									fileTransfer.onprogress = function(progressEvent) {
										if (progressEvent.lengthComputable) {
									        //已经上传
									        var loaded = progressEvent.loaded;
									        //文件总长度
									        var total = progressEvent.total;
									        //计算百分比，用于显示进度条
									        var percent = parseInt((loaded / total) * 50);
									        //换算成MB
									        loaded = (loaded / 1024 / 1024).toFixed(2);
									        total = (total / 1024 / 1024).toFixed(2);

									        alert(percent);
									    }
									}

								}catch(e) {
									alert("下载异常:");
								}
							},
							function(){
								alert("创建download目录失败");
							});
					},
					function(evt) {
						alert("加载文件系统失败");
					}
					);
			},
			error: function(data) {
				alert("服务端数据异常");
			}
		});
	},

	GetFilesByMOID:function (OID) {
		var BCBase = eval(this.FileBCName + "Base");
		var files = new Array();
		var RecordObject;
		var count = BCBase.getRecordCount();
		for (var i = 0; i < count; i++) {
			BCBase.SetIndexNoEvent(i);
			if (BCBase.getFieldValue(this.FileM_FK) == OID) {
				RecordObject = new Object();
				RecordObject.OID = BCBase.getFieldValue(BCBase.TableName+"OID");
				RecordObject.FileName = BCBase.getFieldValue(this.FileNameField);
				files.push(RecordObject);
			}
		}
		return $.map(files,function(file,index){
			var link='<a href="#" title="点击下载" oid="{0}">{1}</a>';
			return String.format(link,file.OID,file.FileName);
		}).join("&nbsp;&nbsp;");
	}
});

//注册控件
UCML.reg("UCML.SuggestView_Mobile", UCML.SuggestView_Mobile);

/*
 *意见对象
 */
UCML.SuggestView_Mobile.WFSuggest_Mobile=function (){
	//意见属性定义
	this.OID=null;
    this.PersonName = null;
    this.Date =null;
    this.OpinionType = null;
    this.Opinion =null;
    this.ActivityChineseName = null;
    this.FinishDate =null;
	this.HaveFiles = true;
    this.Files ="";		
}