package com.wsights.platform.autoupdate;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
//import android.provider.Settings;
//import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.Log;
//import android.widget.Toast;

import java.io.File;
import java.io.IOException;

/**
 * @author feicien (ithcheng@gmail.com)
 * @since 2018-04-07 16:49
 */
public class ApkUtils {


    public static void installAPk(Context context, File apkFile) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            boolean hasInstallPermission = context.getPackageManager().canRequestPackageInstalls();
//            if (!hasInstallPermission) {
//                Toast.makeText(context, "请允许本程序安装未知应用", Toast.LENGTH_LONG).show();
//                startInstallPermissionSettingActivity(context);
//                return;
//            }
//        }
        Intent installAPKIntent = getApkInStallIntent(context, apkFile);
        context.startActivity(installAPKIntent);

    }


    public static Intent getApkInStallIntent(Context context, File apkFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".autoupdate.provider", apkFile);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        } else {
            Uri uri = getApkUri(apkFile);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }
        return intent;
    }


    private static Uri getApkUri(File apkFile) {
        Log.d(Constants.TAG, apkFile.toString());
        //如果没有设置 SDCard 写权限，或者没有 SDCard,apk 文件保存在内存中，需要授予权限才能安装
        try {
            String[] command = {"chmod", "777", apkFile.toString()};
            ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
        } catch (IOException ignored) {
        }
        Uri uri = Uri.fromFile(apkFile);
        Log.d(Constants.TAG, uri.toString());
        return uri;
    }

//    /**
//     * 跳转到设置-允许安装未知来源-页面
//     */
//    @RequiresApi(api = Build.VERSION_CODES.O)
//    private static void startInstallPermissionSettingActivity(Context context) {
//        //注意这个是8.0新API
//        Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(intent);
//    }
}
