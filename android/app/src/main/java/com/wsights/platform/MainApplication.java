package com.wsights.platform;

import android.support.multidex.MultiDexApplication;

import com.facebook.react.ReactApplication;
import com.burnweb.rnwebview.RNWebViewPackage;
import com.mehcode.reactnative.splashscreen.SplashScreenPackage;
import com.zyu.ReactNativeWheelPickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactlibrary.RNSyanImagePickerPackage;
import com.horcrux.svg.SvgPackage;
import com.lmy.smartrefreshlayout.SmartRefreshLayoutPackage;
import com.github.yamill.orientation.OrientationPackage;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.rnfs.RNFSPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.psykar.cookiemanager.CookieManagerPackage;
//import com.reactlibrary.RNSyanImagePickerPackage;
//import com.dylanvann.fastimage.FastImageViewPackage;
//import com.rnfs.RNFSPackage;
//import com.oblador.vectoricons.VectorIconsPackage;
//
//import fr.bamlab.rnimageresizer.ImageResizerPackage;

//import com.lmy.smartrefreshlayout.SmartRefreshLayoutPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.tencent.bugly.crashreport.CrashReport;
//import com.zyu.ReactNativeWheelPickerPackage;
//import com.horcrux.svg.SvgPackage;
//import com.github.yamill.orientation.OrientationPackage;
//import com.psykar.cookiemanager.CookieManagerPackage;

import cn.jpush.reactnativejpush.JPushPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.mehcode.reactnative.splashscreen.SplashScreenPackage;
import com.wsights.platform.autoupdate.UpgradePackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends MultiDexApplication implements ReactApplication {

    // 设置为 true 将不会弹出 toast
    private boolean SHUTDOWN_TOAST = true;
    // 设置为 true 将不会打印 log
    private boolean SHUTDOWN_LOG = true;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new RNWebViewPackage(),
            new SplashScreenPackage(),
            new ReactNativeWheelPickerPackage(),
            new VectorIconsPackage(),
            new RNSyanImagePickerPackage(),
            new SvgPackage(),
            new SmartRefreshLayoutPackage(),
            new OrientationPackage(),
            new ImageResizerPackage(),
            new RNFSPackage(),
//            new FastImageViewPackage(),
            new RNDeviceInfo(),
            new CookieManagerPackage(),
//                    new RNSyanImagePickerPackage(),
                    new FastImageViewPackage(),
//                    new RNFSPackage(),
//                    new VectorIconsPackage(),
//                    new ImageResizerPackage(),
//                    new SmartRefreshLayoutPackage(),
                    new JPushPackage(SHUTDOWN_TOAST, SHUTDOWN_LOG),
//                    new RNDeviceInfo(),
//                    new ReactNativeWheelPickerPackage(),
//                    new SvgPackage(),
//                    new OrientationPackage(),
//                    new CookieManagerPackage(),
//                    new SplashScreenPackage(),
                    new UpgradePackage()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
        // CrashReport.initCrashReport(getApplicationContext(), "65985b6992", false);
        CrashReport.initCrashReport(getApplicationContext(), "e4b640e43a", false);
    }
}
