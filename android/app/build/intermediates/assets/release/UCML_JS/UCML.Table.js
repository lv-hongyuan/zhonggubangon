﻿/**
* @file UCML.Table.js
* @description 前端BC处理类
* @company UCML
* @author 创造人
* @version 1.0
* @date 日期
*/


/**
* @class UCML.BusinessComponent
* @extends UCML.util.Observable
* @description UCML.BusinessComponent类描述
* @param {object} config   类定义初始化参数
*/
UCML.BusinessComponent = function () {
    this.BusinessObject = null;
    this.TableName = "";
    this.BCName = "";
    this.TableType = "M";
    this.LinkKeyName = "";
    this.PK_COLUMN_NAME = "";
    this.dateFormat = "";
    this.recordIndex = -1;

    this.dataStore = null;
    this.ChangeOnlyOwnerBy = false;

    this.CodeTableXMLData = null;
    this.OIDSeed = 10000;
    this.SeedOffset = 0;
    this.columns = null;
    this.MasterTable = null;
    this.theDetailTables = new Array();
    this.theBPOName = null;
    this.theDeltaData = null;
    this.theRecordOwnerType = 0;
    this.theFieldList = "";
    this.theValueList = "";
    this.theCondiIndentList = "";

    this.theDefaultFieldList = ""
    this.theDefaultValueList = ""
    this.theDefaultCondiIndentList = "";
    this.theDefaultSQLCondi = "";
    this.theDefaultSQLCondiType = 0;
    this.nTotalRecordCount = 0;
    this.StartPos = 0;
    this.ReadCount = 0;
    this.DefaultPageCount = 10;
    this.EnabledEdit = true;
    this.EnabledAppend = true;
    this.EnabledDelete = true;
    this.PositionObject = new Array();
    this.sortSQL = "";   //排序语句
    this.theinsertOtherDefaultValue = null;
    this.sender = "";
    this.theLoadChildData = true;
    this.allChildIndex = null;
    this.theSQLCondi = "";
    this.theSQLCondiType = 0;
    this.enabledSort = true;
    this.PrimaryKey = null;
    this.ForeignKey = null;
    this.fCustomRela = false;
    this.thefCustomPrimaryKey = false;
    this.subBC_SQLCondi = "";
    this.theUCMLlanguge = null;
    this.InnerRuleList = new Array();

    //BC加载数据时是否保存历史加载的数据
    this.isLoadHistoryData = false;

    this.childDatas = {};
    UCML.BusinessComponent.superclass.constructor.call(this);
    //事件
    this.addEvents("onInit",
    "onLoad",
    "OnGetCodeValue",
     "OnAfterInsert",
     "OnRecordChange",
      "BeforeFieldChange",
       "OnFieldChange",
       "OnCalculate",
        "onAfterFieldChange",
         "OnAfterDelete",
          "OnBeforeDelete",
           "OnValiate",
            "beforeGetData",
            "afterGetData",
            "loadChildData"
           );
}

UCML.extend(UCML.BusinessComponent, UCML.util.Observable,
{
    setinsertOtherDefaultValue: function (value) {
        this.theinsertOtherDefaultValue = value;
    },
    getinsertOtherDefaultValue: function () {
        return this.theinsertOtherDefaultValue;
    },
    loaded: undefined, //数据初始化状态 为了兼容老版本特意设置为未定义
    isChangeData: true, //是否提交变化数据

    /**    
    * @method public open 
    * @description BC初始化包括给Columns赋值 PrimaryKey赋值
    */
    open: function () {
        if (this.BusinessObject) {
            window[this.BCName + 'base'] = this;
            this.BusinessObject.AddUseTable(this);
            //    this.BusinessObject.on("onLoadData", this.loadData, this);

            if (this.PrimaryKey == null) {
                this.PrimaryKey = this.TableName + "OID";
            }
            if (this.MasterTable != null) {
                this.MasterTable.AddDetailTable(this);

            }

            this.fireEvent("onInit");

            this.on("OnAfterInsert", function () {
                this.claerChildData();
            }, this);
        }
    },
    loadData: function (data) {

        this.SetDataPacket(data);
        if (this.loaded == undefined) {
            this.loaded = false;
        }
        this.fireEvent("onLoad", data);
    },
    setChildData: function (BCName, val) {
        var data = [];
        if (val) {
            data = val.slice(0);
        }
        if (BCName) {
            this.childDatas = this.childDatas || {};
            var MOID = this.OID_GUID();
            this.childDatas[MOID] = this.childDatas[MOID] || {};
            this.childDatas[MOID][BCName] = data;
        }
    }
    ,
    removeChildData: function (BCName, OID) {
        var data = this.getChildData(BCName);
        if (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i][0] == OID) {
                    data.remove(data[i]);
                }
            }
        }
    }
    ,
    getChildData: function (BCName) {
        return this.getChildDataByOID(BCName);
    },
    getChildDataByOID: function (BCName, OID) {
        if (BCName) {
            this.childDatas = this.childDatas || {};
            var MOID = OID || this.OID_GUID();
            this.childDatas[MOID] = this.childDatas[MOID] || {};
            this.childDatas[MOID][BCName] = this.childDatas[MOID][BCName] || [];
            return this.childDatas[MOID][BCName];
        }
        return [];
    }
    ,
    isChildData: function (BCName) {
        return this.isChildDataByOID(BCName);
    }
    ,
    isChildDataByOID: function (BCName, OID) {
        if (!this.childDatas) {
            return false;
        }
        var MOID = OID || this.OID_GUID();
        if (!this.childDatas[MOID]) {
            return false;
        }
        if (BCName) {
            return this.childDatas[MOID][BCName] ? true : false;
        }
        return true;
    }
    ,
    getSequenceNo: function () {
        return GetSequenceNo(this.TableName) - 9999;
    },
    getOIDSeed: function () {
        var ar = BusinessData['OIDData'];
        for (var i = 0; i < ar.length; i++) {
            if (ar[i][0] == this.BCName) {
                return ar[i][1];
            }
        }
        return 0;
    },
    getSeedOffset: function () {
        var ar = BusinessData['OIDData'];
        for (var i = 0; i < ar.length; i++) {
            if (ar[i][0] == this.BCName) {
                return ar[i][2];
            }
        }
        return -1;
    },

    /**    
    * @method private SetDataPacket 
    * @description 将数据传递到BC 
    * @param {Object} dataPacket     
    */
    SetDataPacket: function (data) {
        var seed = this.getOIDSeed();
        var offset = this.getSeedOffset();
        if (seed != -1) {	//重入问题
            this.OIDSeed = seed;
            this.SeedOffset = offset;
        }

        this.nTotalRecordCount = RecordCountInfo[this.BCName].RecordCount;
        this.StartPos = RecordCountInfo[this.BCName].StartPos;
        this.ReadCount = RecordCountInfo[this.BCName].ReadCount;
        this.DefaultPageCount = RecordCountInfo[this.BCName].PageCount;

        var obj = new Object();
        if (this.MasterTable != null) {
            obj.key = this.MasterTable.getOID();
            obj.StartPos = this.StartPos;
            obj.nTotalRecordCount = this.nTotalRecordCount;
            obj.ReadCount = this.ReadCount;
            this.AddPosition(obj);
        }
        else {
            obj.key = '0';
            obj.StartPos = this.StartPos;
            obj.nTotalRecordCount = this.nTotalRecordCount;
            obj.ReadCount = this.ReadCount;
            this.AddPosition(obj);
        }
        if (this.dataStore == null) {
            this.dataStore = new UCML.Data.Table();
        }
        this.dataStore.startPos = this.StartPos;
        this.dataStore.BCName = this.BCName;
        this.dataStore.tableName = this.TableName;

        if (this.isLoadHistoryData) {
            this.dataStore.loadNoClear(data, this.getColumns());
        } else {
            this.dataStore.load(data, this.getColumns());
        }
        this.recordIndex = 0;

        if (this.theDetailTables.length > 0) {
            if (this.isChildData() == false) {
                for (var i = 0; i < this.theDetailTables.length; i++) {
                    if (this.theDetailTables[i].loaded !== true) {//设置首次加载的数据集合
                        this.setChildData(this.theDetailTables[i].BCName, BusinessData[this.theDetailTables[i].BCName]);
                    }
                }
            }
        }

    }
    ,

    /**    
    * @method private SetDataPacketAndChild 
    * @description 装载BC数据递归装载子BC数据
    * @param {Object} data  
    */
    SetDataPacketAndChild: function (data, sender) {
        this.loaded = false; //解决初始不读数据，但通过接口读取数据，这个时候需要考虑加载子表数据问题
        sender || this;
        this.SetDataPacket(data);
        for (var i = 0; i < this.theDetailTables.length; i++) {
            this.theDetailTables[i].SetDataPacketAndChild(BusinessData[this.theDetailTables[i].BCName]);
        }
        //this.childDatas = {}; //清除子表数据,解决切换新加记录丢失问题
        this.fireEvent("onLoad", data);
    }
    ,

    /**    
    * @method public claerChildData 
    * @description 清除多级子表数据
    */
    claerChildData: function () {
        if (this.theDetailTables.length > 0) {
            for (var i = 0; i < this.theDetailTables.length; i++) {
                this.setChildData(this.theDetailTables[i].BCName, []);
                this.theDetailTables[i].SetDataPacketAndChild([]);
                this.theDetailTables[i].claerChildData();
            }
        }
    },
    loadChildData: function () {
        if (this.theLoadChildData == false || this.loaded == true) {
            return;
        }
        if (this.theDetailTables.length > 0) {
            var cur = this.getCurrentRecord();
            if (cur == null) {

                this.claerChildData();
            }
            else {
                var flag = this.isChildData();
                //   var flag = null;
                //从服务端请求子表数据
                if (!flag && !this.IsInsert()) {
                    this.ShowMessage(UCML.Languge.BCfromSeivicesChild);
                    this.BusinessObject.loadChildDataEx(this.BCName, this.getOID(), this.turnArrayDataToXml(cur), this.subBC_SQLCondi, this.LoadChildResults, this.OnFailureCall, this);
                }
                else {
                    for (var i = 0; i < this.theDetailTables.length; i++) {

                        var data = this.getChildData(this.theDetailTables[i].BCName);

                        //重置数据记录
                        RecordCountInfo[this.theDetailTables[i].BCName].RecordCount = this.theDetailTables[i].getTotalRecordCount();

                        RecordCountInfo[this.theDetailTables[i].BCName].StartPos = this.theDetailTables[i].getStartPos();

                        RecordCountInfo[this.theDetailTables[i].BCName].ReadCount = this.theDetailTables[i].getReadCount();

                        BusinessData[this.theDetailTables[i].BCName] = data;
                        this.setChildData(this.theDetailTables[i].BCName, BusinessData[this.theDetailTables[i].BCName]);

                        this.theDetailTables[i].SetDataPacketAndChild(data);

                        this.theDetailTables[i].loadChildData();
                    }
                }
            }
        }
    }
    ,
    /**    
    * @method public getTotalRecordCount 
    * @description 得到记录总数 
    * @return {int} 
    */
    getTotalRecordCount: function () {
        if (this.MasterTable == null) {
            return this.nTotalRecordCount;
        }
        else {
            for (var i = 0; this.PositionObject.length; i++) {
                var obj = this.PositionObject[i];
                if (obj == undefined || obj == null) {	//说明有新的主表
                    var obj = new Object();
                    if (this.MasterTable != null) {
                        obj.key = this.MasterTable.getOID();
                        obj.StartPos = 0;
                        obj.nTotalRecordCount = 1;
                        obj.ReadCount = 0;
                        this.AddPosition(obj);
                        this.nTotalRecordCount = 0;
                        this.StartPos = 0;
                        this.ReadCount = 0;
                    }
                    return this.nTotalRecordCount;
                }
                if (obj.key == '0') {
                    return this.nTotalRecordCount;
                }
                if (obj.key == this.MasterTable.getOID()) {
                    return obj.nTotalRecordCount;
                }
            }
        }
        return this.nTotalRecordCount;
    },

    /**    
    * @method public getStartPos 
    * @description 得到起始位置
    * @return {int} 
    */
    getStartPos: function () {
        if (this.MasterTable == null) {
            return this.StartPos;
        }
        else {
            for (var i = 0; this.PositionObject.length; i++) {
                var obj = this.PositionObject[i];
                if (obj == undefined || obj == null) {	//说明有新的主表
                    var obj = new Object();
                    if (this.MasterTable != null) {
                        obj.key = this.MasterTable.getOID();
                        obj.StartPos = 0;
                        obj.nTotalRecordCount = 1;
                        obj.ReadCount = 0;
                        this.AddPosition(obj);
                        this.nTotalRecordCount = 0;
                        this.StartPos = 0;
                        this.ReadCount = 0;
                    }
                    return this.StartPos;
                }

                if (obj.key == '0') return this.StartPos;
                if (obj.key == this.MasterTable.getOID()) {
                    return obj.StartPos;
                }
            }
        }
        return this.StartPos;
    },

    /**    
    * @method public getReadCount 
    * @description 得到读取的记录总数 
    * @return {int} 
    */
    getReadCount: function () {

        if (this.MasterTable == null) {
            return this.ReadCount;
        }
        else {
            for (var i = 0; this.PositionObject.length; i++) {
                var obj = this.PositionObject[i];
                if (obj == undefined || obj == null) {	//说明有新的主表
                    var obj = new Object();
                    if (this.MasterTable != null) {
                        obj.key = this.MasterTable.getOID();
                        obj.StartPos = 0;
                        obj.nTotalRecordCount = 1;
                        obj.ReadCount = 0;
                        this.AddPosition(obj);
                        this.nTotalRecordCount = 0;
                        this.StartPos = 0;
                        this.ReadCount = 0;
                    }
                    return this.ReadCount;
                }
                if (obj.key == '0') return this.ReadCount;
                if (obj.key == this.MasterTable.getOID()) {
                    return obj.ReadCount;
                }
            }
        }
        return this.ReadCount;
    },
    getAppletList: function () {
        return this.theConnectControls;
    },
    getDefaultPageCount: function () {
        return this.DefaultPageCount;
    },
    setDefaultPageCount: function (value) {
        this.DefaultPageCount = value;
    },
    getMasterTable: function () {
        return this.MasterTable;
    },
    setMasterTable: function (value) {
        this.MasterTable = value;
    },
    setDeltaData: function (value) {
        this.theDeltaData = value;
    },
    setColumns: function (value) {
        this.columns = value;
    },
    getColumns: function () {
        return this.columns;
    },
    getColumn: function (name) {
        return this.getColumnByName(name);
    },
    getColumnByName: function (name) {
        if (this.columns && name) {
            for (var i = 0; i < this.columns.length; i++) {
                if (this.columns[i].fieldName == name) {
                    return this.columns[i];
                }
            }
        }
        return false;
    },
    getLocalDataPacket: function () {
        return this.dataStore.xmldoc;
    },
    getCurrentData: function () {
        var aryT = [];
        for (var i = 0; i < this.columns.length; i++) {
            var columnName = this.columns[i].fieldName;
            var strF = this.getFieldValue(columnName);
            aryT[columnName] = strF;
        }
        return aryT;
    }
    ,

    /**    
    * @method public getCurrentRecord 
    * @description 得到当前的记录 
    * @return {Array}
    */
    getCurrentRecord: function () {
        try {
            if (this.dataStore != null) {
                if (this.recordIndex >= this.dataStore.getLength()) {
                    return null;
                }
                if (this.dataStore.getLength() == 0) {
                    return null;
                }
                if (this.recordIndex < 0) {
                    return null;
                }
                return this.dataStore.getRecordData(this.recordIndex);
            }
            return null;
        }
        catch (e) {
            alert("getCurrentRecord:" + e.description);
        }
        return null;
    },
    getRecordCount: function () {
        return this.dataStore.getLength();
    },
    addRecord: function (recData) {
        recData = recData || [];
        recData[0] = recData[0] || this.TimeToGuid();
        for (var i = 0; i < this.columns.length; i++) {
            recData[i] = recData[i] || "";
        }
        this.dataStore.addRecord(recData);
        var evObj = this.createEventObject();
        this.nTotalRecordCount = this.getRecordCount();
    },

    /**    
    * @method public find 
    * @description 根据字段名称和值判断此记录 是否存在
    * @param {Object} fieldName     
    * @param {Object} value
    * @return {bool}
    */
    find: function (fieldName, value) {
        if (this.dataStore == null) {
            return false;
        }
        var index = this.dataStore.findByName(fieldName, value);
        if (index == -1) {
            return false;
        }
        else {
            return true;
        }
    },

    /**    
    * @method public Locate 
    * @description 定位数据
    * @param {Object} fieldName     
    * @param {Object} value
    * @return {bool}
    */
    Locate: function (fieldName, value, sender) {
        if (this.dataStore == null) {
            return false;
        }
        var index = this.dataStore.findByName(fieldName, value);
        if (index == -1) {
            return false;
        }
        else {
            this.setRecordIndex(index, sender);
            return true;
        }
    },

    /**    
    * @method public GetOIDIndex 
    * @description 定位主键所在的行数
    * @param {OID} OID     
    * @return {int}
    */
    GetOIDIndex: function (OID) {
        if (this.dataStore == null) {
            return -1;
        }
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return this.dataStore.findLocateOID("", OID);
        }
        else {
            return this.dataStore.findLocateOID(this.PrimaryKey, OID);
        }
    },
    //是否OID为主键
    GetIsOIDKey: function () {
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return true;
        }
        else {
            return false;
        }
    }
     ,
    GetGuidBy_Index: function (OID) {
        if (this.GetIsOIDKey()) {
            return this.GetOIDIndex(OID);
        }
        else {
            return this.dataStore.findLocateOID("", OID);
        }
    }
    ,
    GetOIDBy_GUID: function (OID) {
        var rowIndex = -1;
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return OID;
        }
        else {
            rowIndex = this.dataStore.findLocateOID("", OID);
        }
        if (rowIndex == -1) {
            return this.getEmptyGuid();
        }
        else {
            return this.dataStore.getOID(rowIndex, this.PrimaryKey);


        }
    },
    GetGUIDBy_OID: function (OID) {//通过主键找GUIDID
        var rowIndex = -1;
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return OID;
        }
        else {
            rowIndex = this.dataStore.findLocateOID(this.PrimaryKey, OID);
        }
        if (rowIndex == -1) {
            return this.getEmptyGuid();
        }
        else {
            return this.dataStore.getRecordData(rowIndex)[0];
        }
    },

    /**    
    * @method public LocateOID_GUID 
    * @description 定位主键所在的行数并返回该行数据
    * @param {OID} OID     
    * @return {Object}
    */
    LocateOID_GUID: function (OID) {
        if (this.dataStore == null) {
            return null;
        }
        var rowIndex = this.dataStore.findLocateOID("", OID);
        if (rowIndex == -1) {
            this.setRecordIndex(-1);
            return null;
        }
        else {
            this.setRecordIndex(rowIndex);
            return this.dataStore.getRecordData(rowIndex);
        }
    },

    /**    
    * @method public LocateOID 
    * @description 定位主键(包含系统主键及自定义主键)所在的记录并返回该行数据
    * @param {OID} OID     
    * @return {Object}
    */
    LocateOID: function (OID, sender) {
        if (this.dataStore == null) {
            return null;
        }
        var rowIndex = -1;
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            rowIndex = this.dataStore.findLocateOID("", OID);
        }
        else {
            rowIndex = this.dataStore.findLocateOID(this.PrimaryKey, OID);
        }
        if (rowIndex == -1) {
            this.setRecordIndex(-1);
            return null;
        }
        else {
            this.setRecordIndex(rowIndex, sender);
            return this.dataStore.getRecordData(rowIndex);
        }
    }
});

/**    
* @method public LookupOID 
* @description 根据OID判断是否有该记录
* @param {OID} OID     
* @return {bool}
*/
UCML.BusinessComponent.prototype.LookupOID = function (OID) {
    try {
        var rowIndex = -1;
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            rowIndex = this.dataStore.findLocateOID("", OID);
        }
        else {
            rowIndex = this.dataStore.findLocateOID(this.PrimaryKey, OID);
        }
        if (rowIndex == -1) {
            return false;
        }
        else {
            return true;
        }
    }
    catch (e) {
        alert("LookupOID:" + e.description);
    }
}

/**    
* @method public Lookup 
* @description 根据OID和字段名称得到该字段的值
* @param {OID} OID    
* @param {string} fieldName   
* @return {Object}
*/
UCML.BusinessComponent.prototype.Lookup = function (OID, fieldName) {
    try {
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return this.dataStore.LookupByName("", OID, fieldName);
        }
        else {
            return this.dataStore.LookupByName(this.PrimaryKey, OID, fieldName);
        }
    }
    catch (e) {
        alert("Lookup:" + e.description);
    }
}


/**    
* @method public addActorData 
* @description 函数未知
* @param {Object} actorTable      
*/
UCML.BusinessComponent.prototype.addActorData = function (actorTable) {
    if (this.EnabledAppend == false) {
        return;
    }
    if (this.EnabledEdit == false) {
        return;
    }
    var xmldoc = this.dataStore.xmldoc;
    if (xmldoc == null) {
        return;
    }
    this.Insert();
    this.setFieldValue(actorTable.TableName + "OID", actorTable.getOID());
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].lookupDataSet != "") {
            this.setFieldValue(this.columns[i].fieldName, actorTable.getFieldValue(this.columns[i].fieldName));
        }
    }
}


/**    
* @method public getEmptyGuid 
* @description 得到空OID串
* @return {Object} "00000000-0000-0000-0000-000000000000" 
*/
UCML.BusinessComponent.prototype.getEmptyGuid = function () {
    return "00000000-0000-0000-0000-000000000000";
}

/**    
* @method public setActorData 
* @description Json参与者赋值调用函数 
* @param {Object} actorTable    
* @param {string} srcFieldName 
* @param {string} destFieldName   
*/

UCML.BusinessComponent.prototype.setActorData = function (actorTable, srcFieldName, destFieldName) {

    if (this.EnabledEdit == false) {
        return;
    }
    var srcArray = srcFieldName.split(';')
    var destArray = destFieldName.split(';')
    if (srcArray.length == 0) {
        return;
    }
    for (var i = 0; i < srcArray.length; i++) {
        var index = this.recordIndex;
        this.setFieldValue(destArray[i], actorTable.getFieldValue(srcArray[i]));
        this.recordIndex = index;
    }


    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldKind == 2 && this.columns[i].lookupDataSet == actorTable.TableName &&
			this.columns[i].foreignKeyField == destArray[0]) {
            var index = this.recordIndex;
            this.setFieldValue(this.columns[i].fieldName, actorTable.getFieldValue(this.columns[i].lookupResultField));
            this.recordIndex = index;
        }
    }

    var evObj = this.createEventObject()
    evObj.destFieldName = destFieldName;
    evObj.srcFieldName = srcFieldName;
    this.fireEvent("OnActorFieldChange", evObj)
}




UCML.BusinessComponent.prototype.createEventObject = function () {
    return new Object();
}


UCML.BusinessComponent.prototype.setBPOName = function (value) {
    this.theBPOName = value;
}

UCML.BusinessComponent.prototype.getBPOName = function () {
    return this.theBPOName;
}


/**    
* @method public LoadResults 
* @description ajax的回调函数，BC的数据刷新等都是调用此函数 完成任务也是此函数
* @param {Object} result          
*/
UCML.BusinessComponent.prototype.LoadResults = function (data) {
    if (data) {
        if (this.MasterTable) {
            this.MasterTable.setChildData(this.BCName, data);
        }
        this.SetDataPacketAndChild(data);
    }
}

/**    
* @method public onAfterGetData 
* @description ajax调用数据后函数
* @param {Object} result          
*/
UCML.BusinessComponent.prototype.onAfterGetData = function (result, text) {
    this.HideMessage();
    if (text != undefined) {
        if (text != null) {
            eval(text);
        }
    }
    this.LoadResults(BusinessData[this.BCName]);
    this.fireEvent("afterGetData");
    return;
}



/**    
* @method private AddPosition 
* @description 增加分页对象 
* @param {Object} PObject      
*/
UCML.BusinessComponent.prototype.AddPosition = function (PObject) {
    for (var i = 0; i < this.PositionObject.length; i++) {
        var obj = this.PositionObject[i];
        if (obj.key == PObject.key) {
            this.PositionObject[i] = PObject;
            return;
        }
    }
    this.PositionObject[this.PositionObject.length] = PObject;
}


/**    
* @method private ClearData 
* @description 清空数据    
*/
UCML.BusinessComponent.prototype.ClearData = function () {
    if (this.dataStore != null) {
        BusinessData[this.BCName] = null;
        this.dataStore.clearData();
    }
}


/**    
* @method public getUserOID 
* @description 得到登陆者的UserOID
* @return {Object}
*/
UCML.BusinessComponent.prototype.getUserOID = function () {
    return UCML.UserInfo.getUserOID();
}

/**    
* @method public getPostnName 
* @description 得到登陆者得到主岗位名称
* @return {string}
*/
UCML.BusinessComponent.prototype.getPostnName = function () {
    return UCML.UserInfo.getPostnName();
}

/**    
* @method public getPostnName 
* @description 得到登陆者人员名称
* @return {string}
*/
UCML.BusinessComponent.prototype.getUserName = function () {
    return UCML.UserInfo.getUserId();
}

/**    
* @method public getPostnName 
* @description 得到登陆者部门OID
* @return {Object}
*/
UCML.BusinessComponent.prototype.getDivisionOID = function () {
    return UCML.UserInfo.getDivisionOID();
}

/**    
* @method public getOrgOID 
* @description 得到登陆者组织OID
* @return {Object}
*/
UCML.BusinessComponent.prototype.getOrgOID = function () {
    return UCML.UserInfo.getOrgOID();
}


/**    
* @method public getPostPrimaryOID 
* @description 得到登陆者主岗位负责人OID
* @return {Object}
*/
UCML.BusinessComponent.prototype.getPostPrimaryOID = function () {
    return UCML.UserInfo.getPostPrimaryOID();
}

/**    
* @method public getMasterPostOID 
* @description 得到登陆者主岗位OID
* @return {Object}
*/
UCML.BusinessComponent.prototype.getMasterPostOID = function () {
    return UCML.UserInfo.getMasterPostOID();
}

UCML.BusinessComponent.prototype.getPersonName = function () {
    return UCML.UserInfo.getPersonName();
}
UCML.BusinessComponent.prototype.getDepartName = function () {
    return UCML.UserInfo.getDivisionName();
}
UCML.BusinessComponent.prototype.getDivisionName = function () {
    return UCML.UserInfo.getDivisionName();
}
UCML.BusinessComponent.prototype.getOrgName = function () {
    return UCML.UserInfo.getOrgName();
}

/**    
* @method public getLoginTime 
* @description 得到登陆时间 
* @return {string}
*/
UCML.BusinessComponent.prototype.getLoginTime = function () {
    return UCML.UserInfo.getLoginTime();
}

/**    
* @method public getLoginState 
* @description 得到登陆状态 1为登陆
* @return {string}
*/
UCML.BusinessComponent.prototype.getLoginState = function () {
    return UCML.UserInfo.getLoginState();
}


/**
*通过单元测试 
*/
/**    
* @method public getUserID 
* @description 得到登陆者的UserOID
* @return {string}
*/
UCML.BusinessComponent.prototype.getUserID = function () {
    return UCML.UserInfo.getUserId();
}
/**
*未经过单元测试 
*系统未返回此数据
*/
/**    
* @method public getSSID 
* @description 得到客户SSID 未实现
* @return {string}
*/
UCML.BusinessComponent.prototype.getSSID = function () {
    return UCML.UserInfo.getSSID();
}

/**
*通过单元测试 
*/
/**    
* @method public getfMutiLangugeSupport 
* @description 得到是否使用多语言 
* @return {string}
*/
UCML.BusinessComponent.prototype.getfMutiLangugeSupport = function () {
    var thefMutiLanguge = "false";
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        if (BusinessData.UCMLUserSystemInfo.length == 0) return null;
        thefMutiLanguge = BusinessData.UCMLUserSystemInfo[0][20];
    }
    return thefMutiLanguge;
}


/**    
* @method public getPersonOID 
* @description 得到人员PersonOID
* @return {string}
*/
UCML.BusinessComponent.prototype.getPersonOID = function () {
    return UCML.UserInfo.getPersonOID();
}
/**    
* @method public getPersonOID 
* @description 得到人员RESPOID
* @return {string}
*/
UCML.BusinessComponent.prototype.getRESPOID = function () {
    return UCML.UserInfo.getRESPOID();
}

//-----------------END------------------------------------

/**    
* @method public TimeToGuid 
* @description 根据时间生成Guid
* @return {string}
*/
UCML.BusinessComponent.prototype.TimeToGuid = function () {
    var val0;
    var val1;
    var i;
    hexascii = "0123456789ABCDEF";
    var cArr = new Array('', '', '', '', '', '', '-', '', '-', '', '-', '', '-', '', '', '');
    mystr = "";
    var valArr = new Array(4);

    var mydate = new Date();
    value1 = mydate.getTime();

    sec = mydate.getSeconds();
    min = mydate.getMinutes();
    hou = mydate.getHours();
    ran = Math.random() * 10;

    valArr[0] = value1;
    //其他有本地ip组成
    valArr[1] = 0; //Math.pow(ran,sec);
    valArr[2] = this.SeedOffset; //Math.pow(ran,min);
    this.SeedOffset++;
    valArr[3] = this.OIDSeed; //Math.pow(ran,hou);
    for (i = 0; i < 32; i++) {
        val0 = Math.floor(valArr[Math.floor(i / 8)] / 16);
        val1 = valArr[Math.floor(i / 8)] - (val0 * 16);
        c = hexascii.charAt(val1);
        if (i % 2 == 0)
            mystr = cArr[i / 2] + mystr;
        mystr = c + mystr;
        valArr[Math.floor(i / 8)] = val0;
    }
    return mystr.toLowerCase();
}

UCML.BusinessComponent.prototype.getColumnIndex = function (fieldName) {
    return this.dataStore.columnIndex(fieldName);
}


/**    
* @method public sort 
* @description 排序
* @param {string} fieldName   
* @param {int} Order 
*/
UCML.BusinessComponent.prototype.sort = function (fieldName, sortMode) {
    if (this.enabledSort == false) return;

    var bcColumn = this.getColumn(fieldName);
    if (!bcColumn) {
        return;
    }

    var index = this.columns.indexOf(bcColumn);

    var fieldKind = bcColumn.fieldKind;
    var fieldType = bcColumn.fieldType;

    if (fieldType == "Text" || fieldType == "Blob" || fieldType == "Image") {
        return; //不进行排序
    }

    if (fieldKind == 0) {//实字段
        var sortFieldName = this.TableName + "." + fieldName + "";
        this.RefreshSort(sortFieldName, sortMode);
    }
    else if (fieldKind == 2) {//连接虚字段
        var sortFieldName = "al" + bcColumn.foreignKeyField + "." + bcColumn.lookupResultField + "";
        this.RefreshSort(sortFieldName, sortMode);
    }
    else if (fieldKind == 10) {//多值字段
        var sortFieldName = "al" + bcColumn.foreignKeyField + "." + fieldName + "";
        this.RefreshSort(sortFieldName, sortMode);
    }
    else if (fieldKind == 5 && bcColumn.QueryRefColumn) {//SQL语句列
        this.RefreshSort(bcColumn.QueryRefColumn, sortMode);
    }
    else if (fieldKind == 3) {//纯虚字段
        this.dataStore.sort(index, sortMode == 1 ? "desc" : "asc");
        this.fireEvent("onLoad");
    }
    else {
        this.dataStore.sort(index, sortMode == 1 ? "desc" : "asc");
        this.fireEvent("onLoad");
    }
}


/**    
* @method public CopyRecord 
* @description 复制当前记录
* @return {string}
*/
UCML.BusinessComponent.prototype.CopyRecord = function () {
    if (this.dataStore == null) {
        return;
    }

    var newOID = null;
    var currec = this.getCurrentRecord();
    var newrec;

    if (currec == null) {
        this.Insert();
        return;
    }

    this.fireEvent("OnBeforeInsert");

    newOID = this.TimeToGuid();
    if (newOID != null) {
        newrec = this.dataStore.Insert(newOID);
        var cols = this.getColumns();
        for (var i = 1; i < cols.length; i++) {
            newrec[i] = currec[i];
        }
    }

    this.dataStore.DeltaInsert(newrec);
    this.recordIndex = parseInt(this.getRecordCount()) - 1;
    this.dataStore.setRecordIndex(this.recordIndex);
    this.nTotalRecordCount++;

    var evObj = this.createEventObject();
    this.fireEvent("OnAfterInsert", evObj);

    var evObj = this.createEventObject();
    this.fireEvent("OnRecordChange", evObj);

    return newrec;
}



/**    
* @method public Insert 
* @description 插入一条记录
* @param {string} mode 
* @return {string}
*/
UCML.BusinessComponent.prototype.Insert = function (mode) {

    if (this.dataStore == null) {
        return;
    }

    if (this.EnabledAppend == false) {
        return null;
    }
    if (this.EnabledEdit == false) {
        return null;
    }

    if (this.TableType == 'S') {
        if (this.MasterTable.recordCount == 0) {
            alert(UCML.Languge.BCnotMasterTable);
            return null;
        }
    }
    this.fireEvent("OnBeforeInsert");

    var OID = this.TimeToGuid();
    var rec = this.dataStore.Insert(OID);
    for (var i = 1; i < this.columns.length; i++) {
        var colIndex = i;

        var objColumn = this.columns[i];
        if (this.columns[i].defaultValue != null) { //默认值
            rec[colIndex] = this.columns[i].defaultValue;

            /*代码表暂未处理
            if (objColumn.isCodeTable == true) {
            var CodeList = GetCodeValue(objColumn.codeTable);
            if (CodeList != null) {
            for (var j = 0; j < CodeList.length; j++) {
            if (rec[colIndex] == CodeList[j].value) {
            rec[colIndex] = CodeList[j].caption;
            break;
            }
            }
            }
            }*/
        }

        if (objColumn.isFunctionInitValue == true && objColumn.initValueFunc != "") {//函数赋默认值
            rec[colIndex] = eval(objColumn.initValueFunc + "()");
        }

        if (this.MasterTable != null && this.TableType == 'S') {

            if (this.columns[i].isForeignKey == true && this.columns[i].lookupDataSet == this.MasterTable.TableName) {
                rec[colIndex] = this.MasterTable.getOID();
            }
            //2006-07-11 加入,保证任意字段和主键关联
            if (this.columns[i].fieldName == this.LinkKeyName) {
                rec[colIndex] = this.MasterTable.getFieldValue(this.PK_COLUMN_NAME);
            }
            if (this.getCustomRela()) {//是否自定义主键
                for (var m = 0; m < this.ForeignKey.ChildColumns.length; m++) {
                    if (this.columns[i].fieldName == this.ForeignKey.ChildColumns[m]) {
                        rec[colIndex] = this.MasterTable.getFieldValue(this.ForeignKey.ParentColumns[m]);
                        break;
                    }
                }
            }
            if (this.columns[i].isForeignKey == false && this.columns[i].lookupDataSet == this.MasterTable.TableName) {
                if (this.columns[i].isCodeTable == false) {
                    rec[colIndex] = this.MasterTable.getFieldValue(this.columns[i].lookupResultField);
                }
                else {
                    //关联表如果是主表 并且配置一个参与者代码表字段则读取主表代码值
                    //       rec[colIndex] = this.MasterTable.getFieldDisplayText(this.columns[i].lookupResultField);
                }
            }
        }
        if (this.columns[i].fieldName == "SYS_Created") {
            rec[colIndex] = this.today('y-m-d h:mi:s');
        }
        else if (this.columns[i].fieldName == "SYS_CreatedBy") {
            rec[colIndex] = UCML.UserInfo.getUserOID();
        }
        else if (this.columns[i].fieldName == "SYS_POSTN") {
            rec[colIndex] = UCML.UserInfo.getMasterPostOID();
        }
        else if (this.columns[i].fieldName == "SYS_DIVISION") {
            rec[colIndex] = UCML.UserInfo.getDivisionOID();
        }
        else if (this.columns[i].fieldName == "SYS_ORG") {
            rec[colIndex] = UCML.UserInfo.getOrgOID();
        }
        else if (this.columns[i].fieldName == "SYS_LAST_UPD") {
            rec[colIndex] = this.today('y-m-dTh:mi:s');
        }
        else if (this.columns[i].fieldName == "SYS_LAST_UPD_BY") {
            rec[colIndex] = UCML.UserInfo.getUserOID();

        }
        //20081103
        if (this.theinsertOtherDefaultValue != null) {
            if (this.theinsertOtherDefaultValue[columns[i].fieldName] != null) {
                rec[colIndex] = this.theinsertOtherDefaultValue[columns[i].fieldName];
            }
        }
    }


    this.dataStore.DeltaInsert(rec);
    this.recordIndex = parseInt(this.getRecordCount()) - 1;
    this.dataStore.setRecordIndex(this.recordIndex);
    this.nTotalRecordCount++;

    var obj = new Object();
    if (this.MasterTable != null) {
        obj.key = this.MasterTable.getOID();
        obj.StartPos = this.StartPos;
        obj.nTotalRecordCount = this.nTotalRecordCount;
        obj.ReadCount = this.ReadCount;
        this.AddPosition(obj);
    }
    else {
        obj.key = '0';
        obj.StartPos = this.StartPos;
        obj.nTotalRecordCount = this.nTotalRecordCount;
        obj.ReadCount = this.ReadCount;
        this.AddPosition(obj);
    }

    var evObj = this.createEventObject();
    this.fireEvent("OnAfterInsert", evObj);

    var evObj = this.createEventObject();

    this.fireEvent("OnRecordChange", evObj);

    return rec;
}


/**    
* @method public Delete 
* @description 删除一条记录
* @param {string} mode 
* @return {string}
*/
UCML.BusinessComponent.prototype.Delete = function (mode) {

    if (this.dataStore == null) {
        return;
    }

    if (this.EnabledDelete == false) return;
    var row = this.getCurrentRecord();
    if (row == null) {
        return;
    }

    if (this.IsRecordOwner() == false) {
        alert(UCML.Languge.BCnotRightEdit);
        return;
    }
    var DeleteOID = row[0];

    this.fireEvent("OnBeforeDelete", DeleteOID);
    this.dataStore.Delete(DeleteOID);


    this.recordIndex--;
    this.nTotalRecordCount--;
    if (this.recordIndex <= 0) this.recordIndex = 0;
    if (this.getRecordCount() == 0) this.recordIndex = -1;
    this.dataStore.setRecordIndex(this.recordIndex);

    var obj = new Object();
    if (this.MasterTable != null) {
        obj.key = this.MasterTable.getOID();
        obj.StartPos = this.StartPos;
        obj.nTotalRecordCount = this.nTotalRecordCount;
        obj.ReadCount = this.ReadCount;
        this.AddPosition(obj);
    }
    else {
        obj.key = '0';
        obj.StartPos = this.StartPos;
        obj.nTotalRecordCount = this.nTotalRecordCount;
        obj.ReadCount = this.ReadCount;
        this.AddPosition(obj);
    }

    this.fireEvent("OnAfterDelete", DeleteOID);

    this.NotifyDetailTable("D");

    var evObj = this.createEventObject()


    this.NotifyDetailTable("R");

    this.fireEvent("OnRecordChange", evObj);

}


/**    
* @method private IsInsert 
* @description 判断是否是增加记录
* @return {bool}
*/
UCML.BusinessComponent.prototype.IsInsert = function () {
    if (this.dataStore == null) {
        return;
    }
    var id = this.OID_GUID();
    return this.dataStore.IsInsert(id);
}


/**    
* @method public ChangeCountBC 
* @description 判断BC是否有变化数据
* @return {int}
*/
UCML.BusinessComponent.prototype.ChangeCountBC = function () {
    return this.ChangeCount();
}

/**
*通过单元测试 
*/
/**    
* @method public MergeChange 
* @description 清空当前BC的变化数据
*/
UCML.BusinessComponent.prototype.MergeChange = function () {
    if (this.dataStore == null) {
        return;
    }
    this.dataStore.clearChangData();
}


/**    
* @method private getInnerPacket 
* @description 得到底层数据集合 
*/
UCML.BusinessComponent.prototype.getInnerPacket = function () {
    return this.dataStore;
}

/**
*通过单元测试 
*/
/**    
* @method public ChangeCount 
* @description 判断是否有变化数据 
* @return {bool} 
*/
UCML.BusinessComponent.prototype.ChangeCount = function () {
    if (this.dataStore.getChangeCount() > 0) {
        return true;
    } else {
        return false;
    }
}
/**
*未经过单元测试 
*底层已经实现，但全局数据未实现 
*/
/**    
* @method public RejectChanges 
* @description 触发数据改变，底层已经实现，但全局数据未实现 
*/
UCML.BusinessComponent.prototype.RejectChanges = function () {
    this.RejectChangesInner();
    this.fireEvent("onLoad");
    //  this.fireEvent("OnRecordChange"); 性能优化
}

/**
*未经过单元测试 
*底层已经实现，但全局数据未实现 
*/
/**    
* @method public RejectChangesInner 
* @description 触发数据改变，底层已经实现，但全局数据未实现 
*/
UCML.BusinessComponent.prototype.RejectChangesInner = function () {
    var srcNode = null;
    var changeNode = null;
    var changeData = this.dataStore.changeData;
    var bcData = this.dataStore.bcData;
    if (changeData) {
        for (var i = 0; i < changeData.getCount(); i++) {
            var item = changeData.getItemAt(i);
            var state = item[item.length - 1];
            if (state != "null") {
                srcNode = item;
                if (state == "ukModify") {
                    bcData.replace(srcNode[0], srcNode);
                }
                if (state == "ukInsert") {
                    this.Delete(srcNode[0]);
                }
                if (state == "ukDelete") {
                    this.addRecord(srcNode);
                }
            }
        }
    }
    changeData.clear();
}



/**    
* @method public Valiate 
* @description 验证数据有效性
* @return {bool} 
*/

UCML.BusinessComponent.prototype.showValiateMsg = function (msg) {
    alert(msg);
}

/**    
* @method public Valiate 
* @description 验证数据有效性
* @return {bool} 
*/
UCML.BusinessComponent.prototype.Valiate = function () {
    var fieldType = null;
    var columnInfo = null;
    var index;

    var oldIndex = this.getRecordIndex();

    for (var j = 0; j < this.getRecordCount(); j++) {
        this.SetIndexNoEvent(j);
        for (var i = 0; i < this.columns.length; i++) {
            columnInfo = this.columns[i];
            value = this.getFieldValue(columnInfo.fieldName);


            if (columnInfo.allowNull == false) {//去掉主键判断
                if (this.isEmpty(value)) {
                    error = UCML.Languge.BCfieldLanguge + " " + columnInfo.caption + " " + UCML.Languge.BCnotForNull;
                    if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                        this.showValiateMsg(error);
                    }
                    return false;
                }
                else if (this.isWhitespace(value)) {
                    error = UCML.Languge.BCfieldLanguge + " " + columnInfo.caption + " " + UCML.Languge.BCincludeLegality;
                    if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                        this.showValiateMsg(error);
                    }
                    return false;
                }
            }

            if (value != null && value.length != 0) {

                if (columnInfo.fieldType == "Short" || columnInfo.fieldType == "Int" || columnInfo.fieldType == "Long" || columnInfo.fieldType == "Byte") {
                    if (/^-?\d+$/.test(value) == false) {
                        error = columnInfo.caption + UCML.Languge.BCneedNumber;
                        if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                            this.showValiateMsg(error);
                        }
                        return false;
                    }
                }
                if ((columnInfo.fieldType == "VarChar" || columnInfo.fieldType == "Char") && columnInfo.fieldKind != 3) {
                    if ((value+"").replace(/[\u0391-\uFFE5]/g,"aa").length > columnInfo.length) {
                        error = columnInfo.caption + UCML.Languge.BCOutOfLength + columnInfo.length + "!";
                        if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                            this.showValiateMsg(error);
                        }
                        return false;
                    }
                }
                if (columnInfo.fieldType == "Float" || columnInfo.fieldType == "Double" || columnInfo.fieldType == "Money" || columnInfo.fieldType == "Numeric")
                    if (isNaN(value)) {
                        error = columnInfo.caption + UCML.Languge.BCneedNumberDecimal;
                        if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                            this.showValiateMsg(error);
                        }
                        return false;
                    }
            }

            //调用规则注入的规则
            var objColumn = columnInfo;
            if (typeof (ACColumnInputRule) != "undefined") {
                for (var m = 0; m < ACColumnInputRule.length; m++) {
                    if (columnInfo.fieldName == ACColumnInputRule[m].FieldName && this.BCName == ACColumnInputRule[m].BCName) {
                        var inputParams = new Array();

                        inputParams[inputParams.length] = value;
                        if (ACColumnInputRule[m].ExParam1 != "") {
                            var strsrl = new Array(); //定义一数组 
                            var strrlist = ACColumnInputRule[m].ExParam1;
                            strsrl = strrlist.split("["); //字符分割
                            if (strsrl[0] == "") {
                                var thisvalue = strsrl[1].split("]")[0];
                                inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                            }
                            else {
                                inputParams[inputParams.length] = strrlist;
                            }
                        }
                        if (ACColumnInputRule[m].ExParam2 != "") {
                            var strsrl = new Array(); //定义一数组 
                            var strrlist = ACColumnInputRule[m].ExParam2;
                            strsrl = strrlist.split("["); //字符分割
                            if (strsrl[0] == "") {
                                var thisvalue = strsrl[1].split("]")[0];
                                inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                            }
                            else {
                                inputParams[inputParams.length] = strrlist;
                            }
                        }
                        if (ACColumnInputRule[m].ExParam3 != "") {
                            var strsrl = new Array(); //定义一数组 
                            var strrlist = ACColumnInputRule[m].ExParam3;
                            strsrl = strrlist.split("["); //字符分割
                            if (strsrl[0] == "") {
                                var thisvalue = strsrl[1].split("]")[0];
                                inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                            }
                            else {
                                inputParams[inputParams.length] = strrlist;
                            }
                        }
                        if (ACColumnInputRule[m].ExParam4 != "") {
                            var strsrl = new Array(); //定义一数组 
                            var strrlist = ACColumnInputRule[m].ExParam4;
                            strsrl = strrlist.split("["); //字符分割
                            if (strsrl[0] == "") {
                                var thisvalue = strsrl[1].split("]")[0];
                                inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                            }
                            else {
                                inputParams[inputParams.length] = strrlist;
                            }
                        }
                        var result = ExecuteRule(ACColumnInputRule[m].RuleMethod, inputParams);
                        if (result.ReturnCode != "OK") {
                            var error = result.ReturnDescText;
                            if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                                this.showValiateMsg(error);
                            }
                            return false;
                        }
                    }
                }
            }
            if ((columnInfo.allowNull == true && !UCML.isEmpty(value)) && (objColumn.RuleList != null && objColumn.RuleList.length > 0)) {
                for (var m = 0; m < objColumn.RuleList.length; m++) {
                    var inputParams = new Array();

                    inputParams[inputParams.length] = value;
                    if (objColumn.RuleList[m].ExParam1 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = objColumn.RuleList[m].ExParam1;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (objColumn.RuleList[m].ExParam2 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = objColumn.RuleList[m].ExParam2;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (objColumn.RuleList[m].ExParam3 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = objColumn.RuleList[m].ExParam3;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (objColumn.RuleList[m].ExParam4 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = objColumn.RuleList[m].ExParam4;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    var result = ExecuteRule(objColumn.RuleList[m].RuleMethod, inputParams);
                    if (result.ReturnCode != "OK") {
                        var error = result.ReturnDescText;
                        if (this.fireEvent('OnValiate', { OID: this.OID_GUID(), msg: error, caption: columnInfo.caption, value: value, fieldName: columnInfo.fieldName, columnInfo: columnInfo }) !== false) {
                            this.showValiateMsg(error);
                        }
                        return false;
                    }
                }
            }

        }
        //规则验证
        //        for (var m = 0; m < this.InnerRuleList.length; m++) {
        //            for (var n = 0; n < record.childNodes.length; n++) {
        //                if (this.InnerRuleList[m].fieldName == record.childNodes[n].tagName) {
        //                    value = record.childNodes[n].text;
        //                    if (ExecuteRule(this.InnerRuleList[m].RuleID, value) == false) return false;
        //                }
        //            }
        //        }

    }

    this.SetIndexNoEvent(oldIndex);

    return true;
}
//---------------------判断数据-----------------------------
/**
*通过单元测试 
*可改为Tool函数
*/
/**    
* @method public isEmpty 
* @description 判断数据是否为空
* @return {bool} 
*/
UCML.BusinessComponent.prototype.isEmpty = function (s) {
    var bol = false;
    if (s == null || s.length == 0) {
        bol = true;
    }
    return bol;
}

/*
* @method columnIsNumber
* @description 列是否为数字类型
* @param obj列对象 或 string字段名
* @return {bool}
*/
UCML.BusinessComponent.prototype.columnIsNumber = function (val) {

    var col;
    if (UCML.isObject(val)) {
        col = val;
    }
    else {
        col = this.getColumn(val);
    }
    if (col && (col.fieldType == "Short" || col.fieldType == "Int" || col.fieldType == "Long" || col.fieldType == "Byte" || col.fieldType == "Float" || col.fieldType == "Double" || col.fieldType == "Money" || col.fieldType == "Numeric")) {
        return true;
    }
    else {
        return false;
    }
}
/**
*通过单元测试 
*可改为Tool函数
*/
/**    
* @method public isWhitespace 
* @description 判断char是否存在空格
* @return {bool} 
*/
UCML.BusinessComponent.prototype.isWhitespace = function (s) {
    var i;
    s = s + "";
    var whitespace = " \t\n\r";
    if (this.isEmpty(s)) {
        return true;
    }

    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (whitespace.indexOf(c) == -1) {
            return false;
        }
    }
    return true;
}


//---------------------end-----------------------------

/**
*未经过单元测试 
*需要环境测试
*/
/**    
* @method public UseRule 
* @description 应用规则定义
* @param {string} fieldName 
* @param {string} RuleID 
*/
UCML.BusinessComponent.prototype.UseRule = function (fieldName, RuleID) {
    for (var i = 0; this.InnerRuleList.length; i++) {
        var obj = this.InnerRuleList[i];
        if (obj.RuleID == RuleID) {
            return;
        }
    }
    var obj = new Object();
    obj.fieldName = fieldName;
    obj.RuleID = RuleID;
    this.InnerRuleList[this.InnerRuleList.length] = obj;
}

/**    
* @method public setFieldValue 
* @description 设置某个字段的值 增加了事件
* @param {string} fieldName 
* @param {string} value 
*/
UCML.BusinessComponent.prototype.setFieldValue = function (fieldName, value, sender) {

    if (this.EnabledEdit == false) {
        return;
    }
    var columnInfo = null;
    var index;
    if (this.IsRecordOwner() == false) {
        alert(UCML.Languge.BCnotRightEdit);
        return;
    }

    var columnInfo = null;
    index = this.dataStore.columnIndex(fieldName);
    columnInfo = this.columns[index];
    if (index == null) {
        return;
    }

    var oldvalue = this.getFieldValue(fieldName);

    var evObj = this.createEventObject()
    evObj.result = true;
    evObj.fieldName = fieldName;
    evObj.value = value;
    evObj.oldvalue = oldvalue;

    if (this.fireEvent("BeforeFieldChange", evObj) === false) {
        return;
    }

    this.sender = sender;
    var fieldType = columnInfo.fieldType;
    if (columnInfo.fieldType == "Boolean") {
        if (value == true) {
            value = "true";
        }
        if (value == false) {
            value = "false";
        }
    }
    else if (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Int' ||
        fieldType == 'Byte' ||
        fieldType == 'Short'
        ) {
        if (columnInfo.isCodeTable != true) {
            if (value != 'null') {

                if (value != null && value != "" && (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Short')
        ) {
                    value = parseFloat(value);

                    if (columnInfo.decLength != 0) {
                        value = this.DotRound(value, columnInfo.decLength);
                    }
                }

            }
        }
    } else if (columnInfo.fieldType == "DateTime" || columnInfo.fieldType == "Date") {
        var valueTemp = value;
        if (columnInfo.dateTimeFormat) {
            value = this.dateFilterValue(value, columnInfo.dateTimeFormat);
        }
        valueTemp = this.dateFilterValue(valueTemp, 'y-m-d');
        if (valueTemp == "1900-01-01" || valueTemp == "1900-1-1" || valueTemp == "1899-12-31") {
            value = "";
        }
    }

    var record = this.getCurrentRecord();
    if (record == null && this.getRecordCount() == 0) {
        this.Insert();
        record = this.getCurrentRecord();
    }
    if (value == null) {
        value = "";
    }
    if (record != null) {
        if (record[index] + "" == value) {
            return;
        }
        this.dataStore.setFieldValueByIndexID(this.recordIndex, index, value);
        var evObj = this.createEventObject();
        evObj.TableName = this.TableName;
        evObj.fieldName = fieldName;
        evObj.value = value;
        evObj.newvalue = value;
        evObj.oldvalue = oldvalue;
        evObj.dataTable = this;
        evObj.OID = record[0];
        this.fireEvent("OnFieldChange", evObj, sender);

        var evObj = this.createEventObject();
        evObj.fieldName = fieldName;
        this.fireEvent("OnCalculate", evObj);

        var evObj = this.createEventObject();
        evObj.fieldName = fieldName;
        evObj.value = value;
        this.fireEvent("onAfterFieldChange", evObj);
    }
}


/**    
* @method public setAllFieldValue 
* @description 设置所有BC的数据根据某个字段的值
* @param {string} fieldName 
* @param {string} value 
*/
UCML.BusinessComponent.prototype.setAllFieldValue = function (fieldName, value, sender) {
    var sRecordIndex = this.recordIndex;
    for (var i = 0; i < this.getRecordCount(); i++) {
        this.SetIndexNoEvent(i);
        this.setFieldValueEx(fieldName, value, sender);
    }

    this.recordIndex = sRecordIndex;
}

/**    
* @method public setFieldValueEx 
* @description 设置某个字段的值 无事件
* @param {string} fieldName 
* @param {string} value 
*/
UCML.BusinessComponent.prototype.setFieldValueEx = function (fieldName, value, sender) {

    if (this.EnabledEdit == false) {
        return;
    }
    if (this.IsRecordOwner() == false) {
        alert("您现在无权修改这条记录!");
        return;
    }
    var flag = 0;
    var index = null;
    var columnInfo = null;
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            flag = 1;
            columnInfo = this.columns[i];
            break;
        }
    }
    this.sender = sender;

    if (flag == 0) {
        //alert(this.TableName+"非法字段名称:"+fieldName);
        return;
    }
    index = this.dataStore.columnIndex(fieldName);
    columnInfo = this.columns[index];
    if (index == null) {
        return;
    }
    var fieldType = columnInfo.fieldType;
    if (columnInfo.fieldType == "Boolean") {
        if (value == true) {
            value = "true";
        }
        if (value == false) {
            value = "false";
        }
    } else if (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Int' ||
        fieldType == 'Byte' ||
        fieldType == 'Short'
        ) {
        if (columnInfo.isCodeTable != true) {
            if (value != 'null') {

                if (value != null && value != "" && (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Short')
        ) {
                    value = parseFloat(value);

                    if (columnInfo.decLength != 0) {
                        value = this.DotRound(value, columnInfo.decLength);
                    }
                }

            }
        }
    }



    var evObj = this.createEventObject();
    evObj.result = true;
    evObj.fieldName = fieldName;
    evObj.Value = value;

    if (this.fireEvent("BeforeFieldChange", evObj) === false) {
        return;
    }
    var oldvalue = this.getFieldValue(fieldName);

    var record = this.getCurrentRecord();
    if (record != null) {
        this.dataStore.setFieldValueByIndexID(this.recordIndex, index, value);

        var evObj = this.createEventObject();
        evObj.TableName = this.TableName;
        evObj.fieldName = fieldName;
        evObj.value = value;
        evObj.newvalue = value;
        evObj.oldvalue = oldvalue;
        evObj.dataTable = this;
        evObj.OID = record[0];
        this.fireEvent("OnFieldChange", evObj, sender);

        var evObj = this.createEventObject();
        evObj.fieldName = fieldName;
        evObj.Value = value;
        this.fireEvent("onAfterFieldChange", evObj);
    }
}

/**    
* @method public setFieldValueToControl 
* @description 给控件设置某个字段的值 通知某些控件
* @param {string} fieldName 
* @param {string} value 
*/
UCML.BusinessComponent.prototype.setFieldValueToControl = function (fieldName, value) {
    var objectValue = new Object();
    objectValue.fieldName = fieldName;
    objectValue.value = value;
    this.NotifyControls("QueryValue", objectValue);
}
/**
*通过单元测试 
*/
/**    
* @method public setFieldValueBack 
* @description 设置某个字段的值 不触发任何事件
* @param {string} fieldName 
* @param {string} value 
*/
UCML.BusinessComponent.prototype.setFieldValueBack = function (fieldName, value, sender) {

    if (this.EnabledEdit == false) {
        return;
    }
    if (this.IsRecordOwner() == false) {
        alert(UCML.Languge.BCnotRightEdit);
        return;
    }
    this.sender = sender;
    var index = this.dataStore.columnIndex(fieldName);
    if (index == null) {
        return;
    }
    var record = this.getCurrentRecord();
    if (record != null) {
        this.dataStore.setFieldValueByIndexID(this.recordIndex, index, value);
    }
}

/**
*通过单元测试 
*/
/**    
* @method public ShowMessage 
* @description 业务操作时的提示消息显示
* @param {string} str 
*/
UCML.BusinessComponent.prototype.ShowMessage = function (str) {

    var top = window.document.body.scrollTop;
    //    var left = window.document.body.scrollLeft;
    //    var MsgPanel = window.document.getElementById("MsgPanel");
    //    MsgPanel.style.left = left + 200;
    //    MsgPanel.style.top = top + 100;
    //    MsgPanel.style.visibility = "visible";
    //    MsgPanel.innerHTML = str;
}
/**
*通过单元测试 
*/
/**    
* @method public HideMessage 
* @description 业务操作时的提示消息隐藏
*/
UCML.BusinessComponent.prototype.HideMessage = function () {
    //    var MsgPanel = window.document.getElementById("MsgPanel");
    //    MsgPanel.style.visibility = "hidden";
}
/**
*通过单元测试 
*/
/**    
* @method public getLoadChildData 
* @description 判断是否有子表数据
* @return {bool}
*/
UCML.BusinessComponent.prototype.getLoadChildData = function () {
    return this.theLoadChildData;
}
/**
*通过单元测试 
*/
/**    
* @method public setLoadChildData 
* @description 设置是否有子表数据
* @param {bool} value
*/
UCML.BusinessComponent.prototype.setLoadChildData = function (value) {
    this.theLoadChildData = value;
}

/**
*通过单元测试 
*/
/**    
* @method public turnArrayDataToXml 
* @description 将当前数组记录转换成xml数据
* @return {Object}
*/
UCML.BusinessComponent.prototype.turnArrayDataToXml = function () {

    var xml = "<" + this.BCName + ">";
    if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
        xml = xml + "<" + this.TableName + "OID" + ">";
        xml = xml + this.getOID();
        xml = xml + "</" + this.TableName + "OID" + ">";
    }
    for (var i = 0; i < this.columns.length; i++) {
        xml = xml + "<" + this.columns[i].fieldName + ">";
        xml = xml + "<![CDATA[" + this.getFieldValue(this.columns[i].fieldName) + "]]>";
        xml = xml + "</" + this.columns[i].fieldName + ">";
    }
    xml = xml + "</" + this.BCName + ">";
    return xml;
}

/**
*通过单元测试 
*/
/**    
* @method public LoadChildResults 
* @description 读取多级子表数据回调函数
* @param {Object} result 
*/
UCML.BusinessComponent.prototype.LoadChildResults = function (result, text, methodName) {
    this.HideMessage();
    eval(text);
    for (var i = 0; i < this.theDetailTables.length; i++) {
        this.theDetailTables[i].loaded = false;
        this.setChildData(this.theDetailTables[i].BCName, BusinessData[this.theDetailTables[i].BCName]);
        this.theDetailTables[i].SetDataPacketAndChild(BusinessData[this.theDetailTables[i].BCName]);
    }

    this.fireEvent("loadChildData", {});

}
/**
*通过单元测试 
*/
/**    
* @method public NotifyDetailTable 
* @description 触发子表
* @param {string} msg
*/
UCML.BusinessComponent.prototype.NotifyDetailTable = function (msg) {

    if (msg == "R") {
        this.loadChildData();
    }
    else if (msg == "I") //Insert
    {
    }
    else if (msg == "D") {
        for (var i = 0; i < this.theDetailTables.length; i++) {
            //      this.theDetailTables[i].DeleteAll();
        }
    }

}

/**    
* @method public DeleteAll 
* @description 删除所有数据
*/
UCML.BusinessComponent.prototype.DeleteAll = function () {
    var i = 0;
    while (true) {
        this.recordIndex = 0;
        if (this.getRecordCount() > 0) {
            this.Delete();
        }
        else {
            break;
        }
    }
}


/**    
* @method public DeleteAll 
* @description 删除所有子表数据
* @param {string} ParentFieldName 
* @param {string} ParentOID 
*/
UCML.BusinessComponent.prototype.DeleteAllChild = function (ParentFieldName, ParentOID) {
    this.allChildIndex = new Array();
    this.LocateOID(ParentOID);
    var old = this.recordIndex;
    this.InernalGetAllChildIndex(ParentFieldName, ParentOID);
    for (var i = 0; i < this.allChildIndex.length; i++) {
        this.recordIndex = this.allChildIndex[i];
        this.Delete();
    }
    this.recordIndex = old;
    this.Delete();
    //必须配合树一起用
    this.recordIndex = -1;
}

/**    
* @method public InernalGetAllChildIndex 
* @description 递归定位数据
* @param {string} ParentFieldName 
* @param {string} ParentOID 
*/
UCML.BusinessComponent.prototype.InernalGetAllChildIndex = function (ParentFieldName, ParentOID) {
    var n = this.getRecordCount();
    for (var i = 0; i < n; i++) {
        this.recordIndex = i;
        if (this.getFieldValue(ParentFieldName) == ParentOID) {
            this.allChildIndex[this.allChildIndex.length] = i;
            this.InernalGetAllChildIndex(ParentFieldName, this.getOID());
        }
    }
}


/**    
* @method private AddDetailTable 
* @description 添加数据表  
* @param {object} Table  
*/
UCML.BusinessComponent.prototype.AddDetailTable = function (Table) {
    this.theDetailTables[this.theDetailTables.length] = Table;

    //删除时清除缓存数据
    this.on("OnBeforeDelete", function (OID) {
        if (!this.childDatas) {
            return
        }
        if (!this.childDatas[OID]) {
            return
        }
        this.childDatas[OID] = null;
    }, this);

    Table.on("OnAfterInsert", function (OID) {
        if (this.getChildData(Table.BCName).indexOf(Table.dataStore.bcData.last()) != -1)
            return;
        this.getChildData(Table.BCName).add(Table.dataStore.bcData.last());
    }, this);

    Table.on("OnBeforeDelete", function (OID) {
        this.removeChildData(Table.BCName, OID);
    }, this);

}

/**    
* @method private getDateTimeFormatType 
* @description 得到时间类型  
* @param {string} dateTimeFormat  
* @return {string} 
*/
UCML.BusinessComponent.prototype.getDateTimeFormatType = function (dateTimeFormat) {
    if (dateTimeFormat.split(' ').length == 2) {
        return 'datetime'
    }
    else if (dateTimeFormat.indexOf('y') > -1) {
        return 'date'
    }
    else {
        return 'time'
    }
}


/**    
* @method private dateFilterValue 
* @description 格式化时间
* @param {string} dateTimeValue  
* @param {string} dateTimeFormat  
* @return {string} 
*/
UCML.BusinessComponent.prototype.dateTimeValue = function (dateTimeValue, dateTimeFormat) {
    var localTimeFormat = ''
    var localDateFormat = ''
    var timeValue = ''
    var dateValue = ''

    var dateTimeFormatType = this.getDateTimeFormatType(dateTimeFormat)

    switch (dateTimeFormatType) {
        case 'datetime':
            localDateFormat = dateTimeFormat.split(' ')[0]
            localTimeFormat = dateTimeFormat.split(' ')[1]
            if (dateTimeValue.split('T').length == 2) {
                dateValue = dateTimeValue.split('T')[0]
                timeValue = dateTimeValue.split('T')[1]
            }
            else if (dateTimeValue.split(' ').length == 2) {
                dateValue = dateTimeValue.split(' ')[0]
                timeValue = dateTimeValue.split(' ')[1]
            }
            else if (dateTimeValue.split(' ').length == 0 || dateTimeValue.split('T').length == 0) {
                dateValue = dateTimeValue.split(' ')[0]
                timeValue = '';
            }
            break

        case 'date':
            localDateFormat = dateTimeFormat
            dateValue = dateTimeValue
            break;
        case 'time':
            localDateFormat = dateTimeFormat.split(' ')[0]
            localTimeFormat = dateTimeFormat.split(' ')[1]
            if (dateTimeValue.split('T').length == 2) {
                timeValue = dateTimeValue.split('T')[1]
            }
            else if (dateTimeValue.split(' ').length == 2) {
                timeValue = dateTimeValue.split(' ')[1]
            }
            else if (dateTimeValue.split(' ').length == 1) {
                timeValue = dateTimeValue.split(' ')[0]
            }
            else if (dateTimeValue.split(' ').length == 0 || dateTimeValue.split('T').length == 0) {
                timeValue = '';
            }
            dateValue = '';
            break
    }

    var year = 1900
    var month = 1
    var day = 1
    if (dateValue != '') {
        dateValue = dateValue.replace(/[^0-9]/g, '/')
        var dateSequence = localDateFormat.toLowerCase().replace(/[^ymd]/g, '')
        var dateArray = dateValue.split('/')

        year = dateArray[dateSequence.indexOf('y')]
        month = dateArray[dateSequence.indexOf('m')]
        if (dateSequence.indexOf('d') != -1) {
            day = dateArray[dateSequence.indexOf('d')]
        }
    }

    var d = new Date(year, parseInt(month, 10) - parseInt(1), day)
    if (timeValue != '') {
        timeValue = timeValue.replace(/[^0-9]/g, ':')
        timeArray = timeValue.split(':')

        d = new Date(year, parseInt(month, 10) - 1, day)
        d.setHours(parseInt(timeArray[0], 10))
        if (timeArray.length > 1) {
            d.setMinutes(parseInt(timeArray[1], 10))
            if (timeArray.length > 2)
                d.setSeconds(parseInt(timeArray[2], 10))
        }
    }
    return d
}

/**    
* @method private dateFilterValue 
* @description 小于10补0
* @param {string} value  
* @return {string} 
*/
UCML.BusinessComponent.prototype.leadingZero = function (value) {
    if (value < 10) {
        return '0' + value.toString()
    }
    else {
        return value.toString()
    }
}

/**    
* @method private dateFilterValue 
* @description 时间格式化
* @param {string} value  
* @param {string} dateTimeFormat  
* @return {string} 
*/
UCML.BusinessComponent.prototype.dateFilterValue = function (value, dateTimeFormat) {
    if (dateTimeFormat == '') {
        dateTimeFormat = this.dateFormat
    }
    var d = this.dateTimeValue(value, dateTimeFormat)

    var day = this.leadingZero(d.getDate())
    var month = this.leadingZero(d.getMonth() + 1)
    var year = d.getFullYear()
    var hours = this.leadingZero(d.getHours())
    var minutes = this.leadingZero(d.getMinutes())
    var seconds = this.leadingZero(d.getSeconds())

    var dateTimeFormatType = this.getDateTimeFormatType(dateTimeFormat)
    var DBMSName = 'Microsoft SQL Server';
    switch (DBMSName) {
        case 'Microsoft SQL Server':
        case "Adaptive Server Anywhere":
            switch (dateTimeFormatType) {
                case 'datetime':
                    //value = "y-m-d h:mi:s"
                    value = dateTimeFormat
                    break;
                case 'date':
                    //value = "y-m-d"
                    value = dateTimeFormat
                    break;
                case 'time':
                    value = "h:mi:s"
                    break
            }
            break
        case 'Oracle':
            switch (dateTimeFormatType) {
                case 'datetime':
                    value = "TO_DATE('m/d/y h:mi:s','MM/DD/YYYY HH24:MI:SS')"
                    break;
                case 'date':
                    value = "TO_DATE('m/d/y','MM/DD/YYYY')"
                    break;
                case 'time':
                    value = "TO_DATE('h:mi:s','HH24:MI:SS')"
                    break
            }
            break
        case 'Sybase SQL Server':
            switch (dateTimeFormatType) {
                case 'datetime':
                    value = "'m/d/y h:mi:s'"
                    break;
                case 'date':
                    value = "'m/d/y'"
                    break;
                case 'time':
                    value = "'h:mi'"
                    break
            }
            break
        case 'DB2/NT':
            value = "'m/d/y'"
            break
        case 'InterSystems Cache':
            value = "TO_DATE('m/d/y','MM/DD/YYYY')"
            break
        case 'MySQL':
            value = "'y-m-d'"
            break
        default:
            switch (dateTimeFormatType) {
                case 'time':
                    value = "'h:mi'"
                    break
                default:
                    value = "#m/d/y#"
                    break;
            }
            break
    }
    value = value.replace(/mi/, minutes).replace(/h/, hours).replace(/s/, seconds);
    value = value.replace(/m/, month).replace(/d/, day).replace(/y/, year);

    return value;
}


/**    
* @method public today 
* @description 得到当前的时间
* @param {string} dateTimeFormat  
* @return {string} 
*/
UCML.BusinessComponent.prototype.today = function (dateTimeFormat) {
    var d = new Date();

    var day = this.leadingZero(d.getDate())
    var month = this.leadingZero(d.getMonth() + 1)
    var year = d.getFullYear()
    var hours = this.leadingZero(d.getHours())
    var minutes = this.leadingZero(d.getMinutes())
    var seconds = this.leadingZero(d.getSeconds())

    return year + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + seconds;
}


/**    
* @method private IsRecordOwner 
* @description 判断当前用户记录是否有权限修改此记录
* @return {bool} 
*/
UCML.BusinessComponent.prototype.IsRecordOwner = function () {
    var evObj = this.createEventObject()
    evObj.result = true;
    this.fireEvent("OnRecordOwner", evObj)
    if (evObj.result == false) {
        return false;
    }

    //如果不存在创建人字段，所有控制不起作用
    if (this.haveField("SYS_CreatedBy") == false || this.ChangeOnlyOwnerBy==false) {
        return true;
    }
    var d = new UCML.BusinessComponent.ModifyPerm(this);
    if (d != null) {
        if (d["OnlyAllowsSelf"] == "true" || d["OnlyAllowsSelf"] == "1") {
            var value = this.getFieldValue("SYS_CreatedBy");
            if (value == this.getUserOID() || this.getUserOID() == '00000000-0000-0000-0000-000000000001') {
                return true;
            }
            else {
                return false;
            }
        }
        if (d["AllowsSelf"] == "true" || d["AllowsSelf"] == "1") {
            var value = this.getFieldValue("SYS_CreatedBy");
            if (value == this.getUserOID() || this.getUserOID() == '00000000-0000-0000-0000-000000000001') {
                return true;
            }
        }
        if (d["NotAllowsSelf"] == "true" || d["NotAllowsSelf"] == "1") {
            var value = this.getFieldValue("SYS_CreatedBy");
            if (value == this.getUserOID()) {
                return false;
            }
        }
        if (d["AllowsDesignee"] == "true" || d["AllowsDesignee"] == "1") {
            if (UCML.BusinessComponent.ModifyPerm.IsAllowsResp(this, this.getRESPOID()) || UCML.BusinessComponent.ModifyPerm.IsAllowsUser(this, this.getUserOID())) {
                return true;
            }
            else {
                return false;
            }
        }

    }
}

/**    
* @method public haveField 
* @description 判断某个字段是否存在
* @param {string} fieldName  
* @return {bool} 
*/
UCML.BusinessComponent.prototype.haveField = function (fieldName) {
    if (fieldName == this.TableName + 'OID') {
        return true;
    }
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return true;
        }
    }
    return false;
}

/**    
* @method public getRowFieldValue 
* @description 给小数格式化，小数点不够位的，补0
* @param {Numeric} Dight 
* @param {int} How 
* @return {object} 
*/
UCML.BusinessComponent.prototype.DotRound = function (Dight, How) {
    Dight = Math.round(Dight * Math.pow(10, How)) / Math.pow(10, How);
    var ar = Dight.toString().split(".");
    if (ar.length > 1) {
        var str = ar[1];
        for (var i = 0; i < parseInt(How) - parseInt(ar[1].length); i++) {
            str = str + "0";
        }
        Dight = ar[0] + "." + str;
    }
    else {
        var str = "";
        for (var i = 0; i < How; i++) {
            str = str + "0";
        }
        Dight = ar[0] + "." + str;
    }
    return Dight;
}

/**    
* @method public getRowFieldValue 
* @description 得到某条记录的字段值 根据记录索引值
* @param {int} rowIndex 
* @param {string} fieldName 
* @return {object} 
*/
UCML.BusinessComponent.prototype.getRowFieldValue = function (rowIndex, fieldName) {
    var old = this.recordIndex;
    this.recordIndex = rowIndex;
    var value = this.getFieldValue(fieldName);
    this.recordIndex = old;
    return value;

}

/**    
* @method public getRowFieldValue 
* @description 设置某条记录的字段值 根据记录索引值
* @param {int} rowIndex 
* @param {string} fieldName 
* @param {object} value 
* @return {object} 
*/
UCML.BusinessComponent.prototype.setRowFieldValue = function (rowIndex, fieldName, value) {
    var old = this.recordIndex;
    this.recordIndex = rowIndex;
    var value = this.setFieldValueEx(fieldName, value);
    this.recordIndex = old;
}


/**    
* @method public getFieldValue 
* @description 得到当前记录某字段值
* @param {string} fieldName 
* @return {object} 
*/
UCML.BusinessComponent.prototype.getFieldValue = function (fieldName) {

    if (this.dataStore == null) {
        return;
    }

    if (fieldName == "" || fieldName == null) {
        return "";
    }
    var fieldType = null;
    var columnInfo = null;
    var index;

    if (this.GetIsOIDKey() && fieldName == this.TableName + 'OID') {
        return this.getOID();
    }
    index = this.dataStore.columnIndex(fieldName);
    if (index == null) {
        return;
    }
    columnInfo = this.columns[index];
    fieldType = columnInfo.fieldType;
    if (columnInfo == null) {
        alert(this.TableName + UCML.Languge.BClawlessFieldLanguge + fieldName);
        return null;
    }
    var value = null;
    var v;
    var fieldNode = null;
    var record = this.getCurrentRecord();
    if (record != null) {
        //        i = index + 1;
        //        value = record[i]; 串列
        value = record[index];
        //????
        //   value = this.dataStore.getFieldValue(fieldName);


        if (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Int' ||
        fieldType == 'Byte' ||
        fieldType == 'Short'
        ) {
            if (columnInfo.isCodeTable != true) {
                if (value != 'null') {

                    if (value != null && value != "" && (fieldType == 'Float' ||
        fieldType == 'Double' ||
        fieldType == 'Money' ||
        fieldType == 'Number' ||
        fieldType == 'Long' ||
        fieldType == 'Numeric' ||
        fieldType == 'Short')
        ) {
                        value = parseFloat(value);

                        if (columnInfo.decLength != 0) {
                            value = this.DotRound(value, columnInfo.decLength);
                        }
                    }

                }
            }
        }
        if (fieldType == "Date") {
            var valueTemp = value;
            if (columnInfo.dateTimeFormat) {
                value = this.dateFilterValue(value, columnInfo.dateTimeFormat);
            }

            else {
                value = this.dateFilterValue(value, 'y-m-d');
            }

            valueTemp = this.dateFilterValue(valueTemp, 'y-m-d');
            if (valueTemp == "1900-01-01" || valueTemp == "1900-1-1" || valueTemp == "1899-12-31") {
                value = "";
            }
        }
        else if (fieldType == "DateTime") {
            var valueTemp = value;
            if (value.length <= 10) {
                value += ' 00:00:00';
            }
            //value = this.dateFilterValue(value, 'y-m-d h:mi:s');
            if (columnInfo.dateTimeFormat) {
                value = this.dateFilterValue(value, columnInfo.dateTimeFormat);
            }
            else {
                value = this.dateFilterValue(value, 'y-m-d h:mi:s');
            }
            if (value == '1900-01-01 00:00:00') {
                value = "";
            }
            valueTemp = this.dateFilterValue(valueTemp, 'y-m-d');
            if (valueTemp == "1900-01-01" || valueTemp == "1900-1-1" || valueTemp == "1899-12-31") {
                value = "";
            }
        }
        else if (fieldType == "Time") {
            value = this.dateFilterValue(value, 'h:mi:s');
        }
    }
    else {
        //        alert(fieldName + BCinexistenceLanguge)
    }

    if (value == null) {
        value = "";
    }
    if (value == "null") {
        value = "";
    }
    return value;
}


/**    
* @method public turnFieldValue 
* @description 根据字段的属性改变显示情况
* @param {Object} value  
* @param {Object} columnInfo  
*/
UCML.BusinessComponent.prototype.turnFieldValue = function (value, columnInfo) {
    var fieldType = columnInfo.fieldType;

    if (fieldType == 'Float' ||
		fieldType == 'Double' ||
		fieldType == 'Money' ||
		fieldType == 'Numeric' ||
		fieldType == 'Long' ||
		fieldType == 'Numeric' ||
		fieldType == 'Int' ||
		fieldType == 'Byte' ||
		fieldType == 'Short'
	) {
        if (columnInfo.isCodeTable != true) {
            if (value != 'null') {
                if (columnInfo.decLength != 0) {
                    value = this.DotRound(value, columnInfo.decLength);
                }
            }
            else {
                value = "";
            }
        }
        else {
            if (value == 'null') {
                value = "";
            }
        }
    }

    if (fieldType == "Date") {
        if (value == "") {
            value = "1900-01-01";
        }
        if (value == "null") {
            value = "1900-01-01";
        }
        if (columnInfo.dateTimeFormat) {
            value = this.dateFilterValue(value, columnInfo.dateTimeFormat);
        }
        else {
            value = this.dateFilterValue(value, 'y-m-d');
        }
        //value = this.dateFilterValue(value, 'y-m-d');
        if (value == '1900-1-1') {
            value = "";
        }
        if (value == '1900-01-01') {
            value = "";
        }
    }
    else if (fieldType == "DateTime") {
        if (value == "") {
            value = "1900-01-01 00:00:00";
        }
        if (value == "null") {
            value = "1900-01-01 00:00:00";
        }
        if (columnInfo.dateTimeFormat) {
            value = this.dateFilterValue(value, columnInfo.dateTimeFormat);
        }
        else {
            value = this.dateFilterValue(value, 'y-m-d h:mi:s');
        }
        //value = this.dateFilterValue(value, "y-m-d h:mi:s");
        if (value == '1900-01-01 00:00:00') {
            value = "";
        }
    }
    else if (fieldType == "Time") {
        if (value == "") {
            value = "1900-01-01 00:00:00";
        }
        if (value == "null") {
            value = "1900-01-01 00:00:00";
        }
        value = this.dateFilterValue(value, 'h:mi:s');
        if (value == '00:00:00') {
            value = "";
        }
    }
    if (value == 'null') {
        value = "";
    }
    return value;
}


/**    
* @method public OID_GUID 
* @description 得到记录主键
* @return {string} 
*/
UCML.BusinessComponent.prototype.OID_GUID = function () {
    var record = this.getCurrentRecord();
    if (record != null) {
        return record[0];
    }
    return '00000000-0000-0000-0000-000000000000';
}

/**
*通过单元测试
*/
/**    
* @method public getOID 
* @description 得到记录主键
* @return {string} 
*/
UCML.BusinessComponent.prototype.getOID = function () {
    var record = this.getCurrentRecord();
    try {
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            return record[0];
        }
        else {
            var key = this.PrimaryKey.split(";")[0];
            return this.getFieldValue(key);
        }
    }
    catch (e) { }
    return '00000000-0000-0000-0000-000000000000';
}


/**    
* @method public setOID 
* @description 给当前记录赋OID        
* @param {Object} value  
* @return {string} 
*/
UCML.BusinessComponent.prototype.setOID = function (value) {
    var record = this.getCurrentRecord();
    try {
        if (this.PrimaryKey == null || this.PrimaryKey == "" || this.PrimaryKey == this.TableName + "OID") {
            this.dataStore.setOID(this.recordIndex, "", value);
        }
        else {
            this.dataStore.setOID(this.recordIndex, this.PrimaryKey, value);
        }
    }
    catch (e) { }
}

/**    
* @method public GetKeyValue 
* @description 根据一条记录得到他的Key        
* @param {Object} value  
* @return {string} 
*/
UCML.BusinessComponent.prototype.GetKeyValue = function (value) {
    if (value != null) {
        if (value.length > 0) {
            return value[0];
        }
    }
    return '00000000-0000-0000-0000-000000000000';
}


/**    
* @method public getFieldValueByID 
* @description 根据列的索引设置当前记录的字段值     慎用   
* @param {int} index    
* @param {Object} value  
* @return {Object} 
*/
UCML.BusinessComponent.prototype.setFieldValueByID = function (index, value) {

    if (this.EnabledEdit == false) {
        return;
    }
    this.dataStore.setFieldValueByID(index, value);
}


/**    
* @method public getFieldValueByID 
* @description 根据列的索引获取当前记录的字段值       
* @param {Object} index    
* @return {Object} 
*/
UCML.BusinessComponent.prototype.getFieldValueByID = function (index) {
    this.dataStore.getFieldValueByID(index);
}

UCML.BusinessComponent.prototype.Post = function () {
    this.NotifyControls("Post")
}

/*
* @description 上一条记录
*/
UCML.BusinessComponent.prototype.Prev = function () {
    var recIndex = this.recordIndex - 1;
    this.setRecordIndex(recIndex);
}

/*
* @descritpion 下一条记录
*/
UCML.BusinessComponent.prototype.Next = function () {
    var recIndex = this.recordIndex + 1;
    this.setRecordIndex(recIndex);
}


/*
* @descritpion 第一条记录
*/
UCML.BusinessComponent.prototype.First = function () {
    var recIndex = 0;
    this.setRecordIndex(recIndex);

}

/**     
* @method public Last 
* @description 最后一条记录
*/
UCML.BusinessComponent.prototype.Last = function () {
    var recIndex = this.getRecordCount() - 1;
    this.setRecordIndex(recIndex);
}


/**     
* @method public Eof 
* @description 判断是否是最后一条记录
*/
UCML.BusinessComponent.prototype.Eof = function () {
    if (this.recordIndex < 0) {
        return true;
    }
    if (this.recordIndex == this.getRecordCount() - 1) {
        return true;
    }
    return false;
}


/**     
* @method public Bof 
* @description 判断是否是第一条记录
*/
UCML.BusinessComponent.prototype.Bof = function () {
    if (this.recordIndex <= 0) {
        return true;
    } else {
        return false;
    }
}

/*
* @method public getRecordIndex
* @descritpion 获取当前记录的行索引
*/
UCML.BusinessComponent.prototype.getRecordIndex = function () {
    return this.recordIndex;
}

/**     
* @method public setRecordIndex 
* @description 设置当前记录触发事件
* @param {int} value recordIndex
*/
UCML.BusinessComponent.prototype.setRecordIndex = function (value, sender) {
    if (value < 0) {
        return;
    }

    if (value >= this.getRecordCount()) {
        return;
    }
    if (this.recordIndex != value) {
        this.recordIndex = value;
        this.dataStore.setRecordIndex(this.recordIndex);
        this.NotifyDetailTable("R");

        this.fireEvent("OnRecordChange", {}, sender);
    }
}

/**     
* @method private SetIndexNoEvent 
* @description 设置当前记录但不触发任何事件
* @param {int} value recordIndex
*/
UCML.BusinessComponent.prototype.SetIndexNoEvent = function (value, sender) {
    if (value < 0) {
        return;
    }
    if (value >= this.getRecordCount()) {
        return;
    }
    if (this.recordIndex != value) {
        this.recordIndex = value;
        this.dataStore.setRecordIndex(this.recordIndex, sender);
    }
}
UCML.BusinessComponent.prototype.setIndexNoEvent = function (value, sender) {
    if (value < 0) {
        return;
    }
    if (value >= this.getRecordCount()) {
        return;
    }
    if (this.recordIndex != value) {
        this.recordIndex = value;
        this.dataStore.setRecordIndex(this.recordIndex, sender);
    }
}


UCML.BusinessComponent.prototype.getRecordOwnerType = function () {
    return this.recordOwnerType;
}

UCML.BusinessComponent.prototype.setRecordOwnerType = function (value) {
    this.recordOwnerType = value;
}


/**     
* @method private LoadFirstPage 
* @description 转到第一页
*/
UCML.BusinessComponent.prototype.LoadFirstPage = function () {
    if (this.getStartPos() == 0) {
        return;
    }

    this.ShowMessage(UCML.Languge.BCLoadFirstPageLanguge);

    var nStartPoseEx = 0;
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}

/**
*通过单元测试
*/

/**     
* @method private LoadPrevPage 
* @description 转到上一页
*/
UCML.BusinessComponent.prototype.LoadPrevPage = function () {
    if (this.getStartPos() == 0) return;

    this.ShowMessage(UCML.Languge.BCLoadPrevPageLanguge);

    var nStartPoseEx = parseInt(this.getStartPos()) - parseInt(this.getDefaultPageCount());
    if (nStartPoseEx < 0) {
        nStartPoseEx = 0;
    }
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}


/**     
* @method private LoadMoreData 
* @description 转到下一页
*/
UCML.BusinessComponent.prototype.LoadMoreData = function () {

    if (parseInt(this.getStartPos()) + parseInt(this.getReadCount()) >= this.getTotalRecordCount()) {
        return;
    }

    this.ShowMessage(UCML.Languge.BCLoadMoreDataLanguge);

    var nStartPoseEx = parseInt(this.getStartPos()) + parseInt(this.getReadCount());
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}

/**     
* @method private LoadLastPage 
* @description 转到末尾页
*/
UCML.BusinessComponent.prototype.LoadLastPage = function () {

    this.ShowMessage(UCML.Languge.BCLoadLastPageLanguge);

    var pageNum = parseInt(parseInt(this.getTotalRecordCount()) / parseInt(this.getDefaultPageCount()));
    if (parseInt(parseInt(this.getTotalRecordCount()) % parseInt(this.getDefaultPageCount())) != 0) {
        pageNum++;
    }
    var nStartPoseEx = (pageNum - 1) * parseInt(this.getDefaultPageCount());
    if (nStartPoseEx < 0) {
        nStartPoseEx = 0;
    }
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}


/**     
* @method private GotoPage 
* @description 转到第几页
* @param {int} pageNo   
*/
UCML.BusinessComponent.prototype.GotoPage = function (pageNo) {

    this.ShowMessage(UCML.Languge.BCRefreshLanguge);

    nStartPoseEx = (parseInt(pageNo) - 1) * parseInt(this.getDefaultPageCount());
    if (nStartPoseEx < 0) {
        nStartPoseEx = 0;
    }
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}

/*
* @description 是否自定义主键
* @return {bool} true:自定义主键 false:不是自定义主键
*/
UCML.BusinessComponent.prototype.getCustomRela = function () {
    if (this.fCustomRela == true && this.ForeignKey != undefined && this.ForeignKey != null) {
        return true;
    }
    return false;
}

/**     
* @method getData 
* @description 查询获得数据，onAfterGetData，失败时调用onFailureCall
* @param {int} nStartPoseEx
* @param {int} DefaultPageCount     
*/
UCML.BusinessComponent.prototype.getData = function (nStartPoseEx, DefaultPageCount, async) {

    nStartPoseEx = nStartPoseEx == undefined ? 0 : nStartPoseEx;
    DefaultPageCount = DefaultPageCount == undefined ? this.getDefaultPageCount() : DefaultPageCount;

    this.fireEvent("beforeGetData");
    
    if (this.TableType == "M") {
        this.BusinessObject.getCondiActorDataBC(this.BCName, nStartPoseEx, DefaultPageCount, this.theFieldList, this.theValueList, this.theCondiIndentList, this.theSQLCondi, this.theSQLCondiType, this.sortSQL,
                    this.onAfterGetData, this.onFailureCall, this, async);
    }
    else if (this.TableType == "A") {
        this.BusinessObject.getCondiActorDataBC(this.BCName, nStartPoseEx, DefaultPageCount, this.theFieldList, this.theValueList, this.theCondiIndentList, this.theSQLCondi, this.theSQLCondiType, this.sortSQL,
                    this.onAfterGetData, this.onFailureCall, this, async);
    }
    else if (this.TableType == "S") {
        var cond = this.getForeignCondition();
        this.BusinessObject.getCondiActorDataBC(this.BCName, nStartPoseEx, DefaultPageCount, cond.fieldList, cond.valueList, cond.condiIndentList, this.theSQLCondi, this.theSQLCondiType, this.sortSQL,
                    this.onAfterGetData, this.onFailureCall, this, async);
    }
}


/**     
* @method getForeignCondition 
* @description 获得子BC外键条件    
*/
UCML.BusinessComponent.prototype.getForeignCondition = function () {
    var ForeignKey = this.ForeignKey;

    var theCondiIndentList1 = "";
    var theFieldList1 = '';
    var theValueList1 = '';
    if (this.TableType == "S") {
        //如果是自定义主外键时
        if (this.getCustomRela()) {

            for (var m = 0; m < ForeignKey.ChildColumns.length; m++) {
                parentOID = this.MasterTable.getFieldValue(ForeignKey.ParentColumns[m]);
                pkField = ForeignKey.ChildColumns[m];
                theFieldList1 = theFieldList1 + this.theFieldList + this.TableName + '.' + pkField + ';';
                theValueList1 = theValueList1 + this.theValueList + parentOID + ';';
                theCondiIndentList1 = theCondiIndentList1 + this.theCondiIndentList + ' = ;'
            }
        }
        else {
            theFieldList1 = this.theFieldList + this.TableName + '.' + this.LinkKeyName + ';';
            if (this.PK_COLUMN_NAME != "") {
                theValueList1 = this.theValueList + this.MasterTable.getFieldValue(this.PK_COLUMN_NAME) + ';';
            }
            else {
                theValueList1 = this.theValueList + this.MasterTable.getOID() + ';';
            }
            theCondiIndentList1 = this.theCondiIndentList + ' = ;'
        }
    }
    return { condiIndentList: theCondiIndentList1, fieldList: theFieldList1, valueList: theValueList1 };
}


/**     
* @method private OnSuccessCall 
* @description 当查询返回正确时的回调函数
* @param {string} result
* @param {string} userContext   
* @param {string} methodName      
*/
UCML.BusinessComponent.prototype.OnSuccessCall = function (result, userContext, methodName) {
    BusinessObject.HideMessage();
    var xmldoc = XMLJS.parseXML2(result.responseText);
    var text = xmldoc.text
    alert(text);
}

/**
*通过单元测试
*/

/**     
* @method private onFailureCall 
* @description 当查询返回错误时的回调函数
* @param {string} result
* @param {string} userContext   
* @param {string} methodName      
*/
UCML.BusinessComponent.prototype.onFailureCall = function (result, text, methodName) {
    this.fireEvent("afterGetData");
    alert(text);
}

/**
*通过单元测试
*但未处理恢复变化数据
*/

/**     
* @method public Refresh 
* @description 刷新数据同时取消修改 
*/
UCML.BusinessComponent.prototype.Refresh = function () {
    this.ShowMessage(UCML.Languge.BCRefreshLanguge);

    if (this.MasterTable) {
        this.MasterTable.setChildData(this.BCName, []);
    }
    this.claerChildData();
    this.childDatas = {}; //刷新之后清除childDatas

    this.ClearData();
    //恢复变化数据
    this.RejectChangesInner();
    //this.StartPos = 0;
    //var nStartPoseEx = 0;
    var nStartPoseEx = this.StartPos;
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}


/**     
* @method public RefreshSort 
* @description 数据排序
* @param {string} fieldName    
* @param {string} Order   
*/
UCML.BusinessComponent.prototype.RefreshSort = function (fieldName, Order) {
   
    //恢复变化数据
    this.RejectChangesInner();
    this.StartPos = 0;
    var nStartPoseEx = 0;
    if (Order == 0) {
        SQLFix = " ORDER BY " + fieldName;
    }
    else if (Order == 1) {
        SQLFix = " ORDER BY " + fieldName + " DESC";
    }
    else {
        SQLFix = "";
    }
    this.sortSQL = SQLFix;
  
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}

/*
* @description 设置自定义排序sql语句
* @param {string}
*/
UCML.BusinessComponent.prototype.setSortSQL = function (val) {
    //恢复变化数据
    this.RejectChangesInner();
    this.StartPos = 0;
    var nStartPoseEx = 0;
    this.sortSQL = val;
    this.getData(nStartPoseEx, this.getDefaultPageCount());
}


/**     
* @method public SetCondiList 
* @description 设置查询条件 为condiQuery提供支持
* @param {string} fieldList    
* @param {string} valueList 
* @param {string} condiIndentList 
* @param {string} SQLCondi 
* @param {string} SQLCondiType   
*/
UCML.BusinessComponent.prototype.SetCondiList = function (fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType) {
    this.theFieldList = fieldList;
    this.theValueList = valueList;
    this.theCondiIndentList = condiIndentList;
    //这样可以保留以前的条件
    if (SQLCondi != undefined) {
        this.theSQLCondi = SQLCondi;
    }
    if (SQLCondiType == undefined) {
        this.theSQLCondiType = 0;
    } else {
        this.theSQLCondiType = SQLCondiType;
    }
}

UCML.BusinessComponent.prototype.getSQLCondi = function () {
    return this.theSQLCondi;
}

UCML.BusinessComponent.prototype.getFieldList = function () {
    return this.theFieldList;
}

UCML.BusinessComponent.prototype.getValueList = function () {
    return this.theValueList;
}

UCML.BusinessComponent.prototype.getCondiIndentList = function () {
    return this.theCondiIndentList;
}


UCML.BusinessComponent.prototype.setSQLCondi = function (value) {
    this.theSQLCondi = value;
}

UCML.BusinessComponent.prototype.setFieldList = function (value) {
    this.theFieldList = value;
}

UCML.BusinessComponent.prototype.setValueList = function (value) {
    this.theValueList = value;
}

UCML.BusinessComponent.prototype.setCondiIndentList = function (value) {
    this.theCondiIndentList = value;
}
/**
*未经过单元测试
*没有任何地方调用过
*/

/**     
* @method private SetDefaultCondiList 
* @description 设置默认的查询条件
* @param {string} fieldList    
* @param {string} valueList 
* @param {string} condiIndentList 
* @param {string} SQLCondi 
* @param {string} SQLCondiType   
*/
UCML.BusinessComponent.prototype.SetDefaultCondiList = function (fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType) {
    this.theDefaultFieldList = fieldList;
    this.theDefaultValueList = valueList;
    this.theDefaultCondiIndentList = condiIndentList;
    this.theDefaultSQLCondi = SQLCondi;
    this.theDefaultSQLCondiType = SQLCondiType;
}

UCML.BusinessComponent.prototype.getEnabledEdit = function () {
    return this.EnabledEdit;
}

UCML.BusinessComponent.prototype.setEnabledEdit = function (value) {
    this.EnabledEdit = value;
}

UCML.BusinessComponent.prototype.getEnabledAppend = function () {
    return this.EnabledAppend;
}

UCML.BusinessComponent.prototype.setEnabledAppend = function (value) {
    this.EnabledAppend = value;
}

UCML.BusinessComponent.prototype.getEnabledDelete = function () {
    return this.EnabledDelete;
}

UCML.BusinessComponent.prototype.setEnabledDelete = function (value) {
    this.EnabledDelete = value;
}

UCML.BusinessComponent.prototype.getEnabledSort = function () {
    return this.enabledSort;
}

UCML.BusinessComponent.prototype.setEnabledSort = function (value) {
    this.enabledSort = value;
}


UCML.BusinessComponent.prototype.getDetailTables = function () {
    return this.theDetailTables;
}


UCML.BusinessComponent.prototype.getPrimaryKey = function () {
    return this.PrimaryKey;
}

UCML.BusinessComponent.prototype.setPrimaryKey = function (value) {
    this.PrimaryKey = value;
}

UCML.BusinessComponent.prototype.getForeignKey = function () {
    return this.ForeignKey;
}

UCML.BusinessComponent.prototype.setForeignKey = function (value) {
    this.ForeignKey = value;
}

UCML.BusinessComponent.prototype.setCustomRela = function (value) {
    this.fCustomRela = value;
}

UCML.BusinessComponent.prototype.getfCustomPrimaryKey = function () {
    return this.thefCustomPrimaryKey;
}

UCML.BusinessComponent.prototype.setfCustomPrimaryKey = function (value) {
    this.thefCustomPrimaryKey = value;
}

UCML.BusinessComponent.prototype.getCustomKeyValue = function () {
}


UCML.BusinessComponent.prototype.setNotifyControl = function (value) {
    this.theNotifyControl = value;
}

UCML.BusinessComponent.prototype.setSubBC_SQLCondi = function () {
    this.subBC_SQLCondi = value;
}



/**     
* @method public getFieldType 
* @description 根据BC字段名称得到BC字段的类型  
* @param {string} fieldName      
* @return {string} 
*/
UCML.BusinessComponent.prototype.getFieldType = function (fieldName) {
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return this.columns[i].fieldType;
        }
    }
    return 46;
}

/**
*通过单元测试
*/

/**     
* @method public getFieldCaption 
* @description 根据BC字段名称得到BC字段的标题   
* @param {string} fieldName      
* @return {string} 
*/
UCML.BusinessComponent.prototype.getFieldCaption = function (fieldName) {
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return this.columns[i].caption;
        }
    }
    return fieldName;
}
/**
*通过单元测试
*/

/**     
* @method public clearAllBCLink 
* @description 清空所有的链接业务组件      
*/
UCML.BusinessComponent.prototype.clearAllBCLink = function () {
    for (var i = 0; i < this.columns.length; i++) {
        this.columns[i].objBCLinkColl = null;
    }
}
/**
*通过单元测试
*/

/**     
* @method public clearBCLink 
* @description 清空某个字段的链接业务组件 
* @param {string} fieldName         
*/
UCML.BusinessComponent.prototype.clearBCLink = function (fieldName) {
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            this.columns[i].objBCLinkColl = null;
            break;
        }
    }
}

/**     
* @method public GetBCLink 
* @description 得到某个字段的链接业务组件 
* @param {string} fieldName   
* @return {Columns.objBCLinkColl}       
*/
UCML.BusinessComponent.prototype.GetBCLink = function (fieldName) {
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return this.columns[i].objBCLinkColl;
        }
    }
    return null;
}
UCML.BusinessComponent.prototype.getBCLink = function (fieldName) {
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return this.columns[i].objBCLinkColl;
        }
    }
    return null;
}
/**
*未通过单元测试
*服务器端没有生成UCMLlanguge.innerHTML需要底层配合
*/

/**     
* @method private getUCMLlanguge
* @description 得到UCML语言包 
* @param {string} LableText   
* @return {string}       
*/
UCML.BusinessComponent.prototype.getUCMLlanguge = function (LableText) {
    //    if (this.theUCMLlanguge == null) {
    //        this.theUCMLlanguge = this.createXMLObject();
    //        this.theUCMLlanguge.loadXML(UCMLlanguge.innerHTML);
    //    }
    //    var node = this.theUCMLlanguge.selectSingleNode("//" + LableText);
    //    if (node != null) return node.text;
    return "";
}


/**     
* @method private getChangeXML
* @description 将变化数据转成XML   
* @return {string} 
*/
UCML.BusinessComponent.prototype.getChangeXML = function () {
    return this.dataStore.getChangeXML();
}

UCML.override(UCML.BusinessComponent, {
    sum: function (fieldName) {
        //修改，获取字段小数位显示长度
        var columnInfo = null;
        var cindex = this.dataStore.columnIndex(fieldName);
        columnInfo = this.columns[cindex];
        var oldIndex = this.recordIndex;
        var recordCount = this.getRecordCount();
        var value = 0;
        for (var i = 0; i < recordCount; i++) {
            this.SetIndexNoEvent(i);
            var tempValue = this.getFieldValue(fieldName);
            if (isNaN(parseFloat(tempValue)) == false)
                value = value + parseFloat(tempValue);
        }
        this.SetIndexNoEvent(oldIndex);
        if (columnInfo.decLength != 0) {
            value = this.DotRound(value, columnInfo.decLength);
        }
        return value;
    }
    ,
    avg: function (fieldName) {
        //修改，获取字段小数位显示长度
        var columnInfo = null;
        var cindex = this.dataStore.columnIndex(fieldName);
        columnInfo = this.columns[cindex];

        var oldIndex = this.recordIndex;
        var recordCount = this.getRecordCount();
        var value = 0;
        for (var i = 0; i < recordCount; i++) {
            this.SetIndexNoEvent(i);
            var tempValue = this.getFieldValue(fieldName);
            if (isNaN(parseFloat(tempValue)) == false)
                value = value + parseFloat(tempValue);
        }
        this.SetIndexNoEvent(oldIndex);
        if (recordCount > 0) {
            if (columnInfo.decLength != 0) {
                return this.DotRound(value / recordCount, columnInfo.decLength);
            }
            else
                return value / recordCount;
        }
        else return 0;
    },
    count: function (fieldName) {
        return this.getRecordCount();
    }
    , max: function (fieldName) {
        var oldIndex = this.recordIndex;
        var recordCount = this.getRecordCount();
        var value;
        for (var i = 0; i < recordCount; i++) {
            this.SetIndexNoEvent(i);
            var fval = parseFloat(this.getFieldValue(fieldName));
            if (!value) {
                value = fval;
            }
            else if (value < fval) {
                value = fval;
            }
        }
        this.SetIndexNoEvent(oldIndex);
        return UCML.isEmpty(value) ? "" : value;
    },
    min: function (fieldName) {
        var oldIndex = this.recordIndex;
        var recordCount = this.getRecordCount();
        var value;
        for (var i = 0; i < recordCount; i++) {
            this.SetIndexNoEvent(i);
            var fval = parseFloat(this.getFieldValue(fieldName));
            if (!value) {
                value = fval;
            }
            else if (value > fval) {
                value = fval;
            }
        }
        this.SetIndexNoEvent(oldIndex);
        return UCML.isEmpty(value) ? "" : value;
    }
});


UCML.BusinessComponent.ModifyPerm = function (BCBase) {
    try {
        this.data = BusinessData[BCBase.BPOName + 'ModifyPerm'];
        if (this.data) {
            if (this.data.length > 0) {
                var obj = {};
                obj["BPOName"] = this.data[0][1];
                obj["OnlyAllowsSelf"] = this.data[0][5];
                obj["AllowsSelf"] = this.data[0][6];
                obj["NotAllowsSelf"] = this.data[0][7];
                obj["AllowsDesignee"] = this.data[0][8];
                return obj;
            }
        }
    }
    catch (e) {
        this.data = null;
    }
    return null;
}

UCML.BusinessComponent.ModifyPerm.IsAllowsResp = function (BCBase, RespStr) {
    this.data = BusinessData[BCBase.BPOName + 'AllowModifyResp'];
    var respList = RespStr.split(";");
    var userResp, respKey;
    var isModeify = false;
    if (this.data) {
        for (var r = 0; r < respList.length; r++) {
            userResp = respList[r];
            if (UCML.isEmpty(userResp)) { continue; }
            for (var i = 0; i < this.data.length; i++) {
                var respKey = this.data[i][4];
                if (userResp == respKey) {
                    isModeify = true;
                    break;
                }
            }
            if (isModeify == true) {
                break;
            }
        }
    }
    return isModeify;
}
UCML.BusinessComponent.ModifyPerm.IsAllowsUser = function (BCBase, userOID) {
    this.data = BusinessData[BCBase.BPOName + 'AllowModifyUser'];
    var userKey;
    var isModeify = false;
    if (this.data) {
        for (var i = 0; i < this.data.length; i++) {
            var respKey = this.data[i][5];
            if (userOID == respKey) {
                isModeify = true;
                break;
            }
        }
    }
    return isModeify;
}
