﻿UCML.Numberbox = function (id) {
    this.min = null;
    this.max = null;
    this.precision = 0;
    this.mark = "";
    this.markAlign = "right";
    UCML.Numberbox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Numberbox, UCML.Input, {
    autoEl: 'input'
});

UCML.Numberbox.prototype.init = function () {

    UCML.Numberbox.superclass.init.call(this);

    this.el.css({ imeMode: "disabled" });
}

UCML.Numberbox.prototype.setProperties = function () {
    UCML.Numberbox.superclass.setProperties.call(this);
    this.min = (this.el.attr('min') == '0' ? 0 : parseFloat(this.el.attr('min')) || this.min);
    this.max = (this.el.attr('max') == '0' ? 0 : parseFloat(this.el.attr('max')) || this.max);
    this.precision = (parseInt(this.el.attr('precision')) || this.precision);
    this.mark = (parseInt(this.el.attr('mark')) || this.mark);
    this.markAlign = (parseInt(this.el.attr('markAlign')) || this.markAlign);
}

UCML.Numberbox.prototype.select = function () {

    if (this.dom.select) {
        this.dom.select();
    }
}

UCML.Numberbox.prototype.setValue = function (v, trigger) {
    var val = parseFloat(v).toFixed(this.precision);

    if (isNaN(val)) {
        this.el.val('');
        this.value = "";
        return;
    }
  
    if (this.min != null && this.min != undefined && val < this.min) {
        val = this.min.toFixed(this.precision);
        if (this.markAlign == "left") {
           
            this.el.val(this.mark + this.min.toFixed(this.precision));
        }
        else {
            this.el.val(this.min.toFixed(this.precision) + this.mark);
        }
    } else if (this.max != null && this.max != undefined && val > this.max) {
        val = this.max.toFixed(this.precision);
        if (this.markAlign == "left") {
            this.el.val(this.mark + this.max.toFixed(this.precision));
        }
        else {
            this.el.val(this.max.toFixed(this.precision) + this.mark);
        }
    } else {
        if (this.markAlign == "left") {
            this.el.val(this.mark + val);
        }
        else {
            this.el.val(val + this.mark);
        }
    }

    this.value = val;

    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }
}

UCML.Numberbox.prototype.bindEvents = function () {

    var opts = this;
    this.el.unbind('.numberbox');
    this.el.bind('keypress.numberbox', function (e) {
        if (e.which == 45) {	//-
            return true;
        } if (e.which == 46) {	//.
            return true;
        }
        else if ((e.which >= 48 && e.which <= 57 && e.ctrlKey == false && e.shiftKey == false) || e.which == 0 || e.which == 8) {
            return true;
        } else if (e.ctrlKey == true && (e.which == 99 || e.which == 118)) {
            return true;
        } else {
            return false;
        }
    }).bind('paste.numberbox', function () {
        if (window.clipboardData) {
            var s = clipboardData.getData('text');
            if (!/\D/.test(s)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }).bind('dragenter.numberbox', function () {
        return false;
    }).bind('blur.numberbox', function () {
        opts.setValue(this.value.replace(opts.mark, ''));
    }).bind('focus.numberbox', function () {
        this.value = this.value.replace(opts.mark, '');
        this.select();
    });
}


UCML.reg("UCML.Numberbox", UCML.Numberbox);
