UCML.Container = function (id) {

    UCML.Container.superclass.constructor.call(this, id);

    this.addEvents("setsize");
}

UCML.extend(UCML.Container, UCML.Commponent, {
    ctype: "UCML.Container",
    doSize: true, // true to set size and do layout
    href: null,
    cache: true,
    sizeCount: 0,
    alignWidth: false, //适应宽
    alignHeight: false, //适应高
    onResize: function (width, height) { },
    ratioWidth: null, //百分比宽
    ratioHeight: null, //百分比高
    resizeTo: true,
    beforeRender: function () {
        UCML.Container.superclass.beforeRender.call(this);
        if (this.alignWidth == true || this.alignWidth == "true") {//自适应宽
            this.alignWidth = true;
            this.width = "100%";
        }
        else {
            this.alignWidth = false;
        }

        if (this.alignHeight == true || this.alignHeight == "true") {//自适应高
            this.alignHeight = true;
            this.height = "100%";
        }
        else {
            this.alignHeight = false;
        }

        this.el.attr("layout", this.ctype);
    },
    onRender: function () {
        UCML.Container.superclass.onRender.call(this);
        this.el.css(this.style);
        this.el.addClass(this.cls);
    },
    afterRender: function () {
        UCML.Container.superclass.afterRender.call(this);
        if (this.doSize == true) {
            this.el.css('display', 'block');
            this.setSize(null, "afterRender");
        }
    }
    ,
    getHasLayout: function () {
        return this.el.parent().attr("layout") ? true : false;
    }
    , resize: function (param) {
        this.setSize(param, "resize");
    }
    ,
    setProperties: function () {

        UCML.Container.superclass.setProperties.call(this);

        //    this.top = (parseInt(this.el.css('top')) || this.top);
        //    this.left = (parseInt(this.el.css('left')) || this.left);

        //    this.cls = this.el.attr('cls') || this.cls;

        //  var width = this.el.css('width');
        //  var height = this.el.css('height');
        var width = this.dom.style.width;
        var height = this.dom.style.height;
        //  alert(this.id + ":" + height+":"+this.dom.style.height);
        //        if (width) {
        //            this.width = width.replace("px", "") || this.width;
        //        }
        //        if (height) {
        //            this.height = height.replace("px", "") || this.height;
        //       }


        this.alignWidth = (this.el.attr('alignwidth') ? this.el.attr('alignwidth') == 'true' : this.alignWidth); //自适应宽
        this.alignHeight = (this.el.attr('alignheight') ? this.el.attr('alignheight') == 'true' : this.alignHeight); //自适应高

        var elWidth = width;
        if (isNaN(elWidth) && elWidth.indexOf('%') != -1) {//如果是百分比
            this.ratioWidth = parseInt(elWidth);
        }
        else if (!isNaN(parseInt(elWidth))) {
            this.width = parseInt(elWidth);
        }

        var elHeight = height;
        if (isNaN(elHeight) && elHeight.indexOf('%') != -1) {//如果是百分比
            this.ratioHeight = parseInt(elHeight);
        }
        else if (!isNaN(parseInt(elHeight))) {
            this.height = parseInt(elHeight);
        }
    },
    bindEvents: function () {
        UCML.Container.superclass.bindEvents.call(this);
        var opt = this;


        opt.el.parent().bind("_resize", function () {
            opt.resize(null, opt.el.attr("id") + ":opt.el.parent().bind(_resize");

        });
        if (opt.alignHeight == true || opt.alignWidth == true || UCML.isEmpty(opt.ratioHeight) == false || UCML.isEmpty(opt.ratioWidth) == false) {
          //  if (opt.getHasLayout()) {
          //  }
          //  else {
            $(window).bind('resize', function () {
                    opt.setSize(null, "$(window).bind('resize'");
                });
          //  }
        }
    }
    ,
    resize: function (param) {
        this.setSize(param);
        this.onResize.apply(this, [this.width, this.height]);
        this.el.triggerHandler('_resize');
        // alert(this.id + "triggerHandler_resize");
    }
    , setSize: function (param, a) {
     //   alert(this.id + " -- " + a);
          this.sizeCount = this.sizeCount + 1;
      //   alert(this.id + "-" + this.sizeCount);

        var oldWidth = this.width;
        var oldHeight = this.height;
        if (param) {
            if (param.width) {
                this.width = param.width
            }
            if (param.height) {

                this.height = param.height;
            }
            if (param.left != null) {
                this.left = param.left;
            }
            if (param.top != null) {
                this.top = param.top;
            }
        } else {
            var p = this.getParentEl(); //父对象

            if (this.alignWidth == true || this.alignWidth == "true") {//自适应宽
                this.alignWidth = true;
                this.width = p.width();
            }
            else if (this.ratioWidth) {
                this.width = parseInt(p.width() * this.ratioWidth / 100);

            }

            if (this.alignHeight) {//自适应高
                this.height = p.height();
            }
            else if (this.ratioHeight) {

                this.height = parseInt(p.height() * this.ratioHeight / 100); //防止百分比计算四舍五入

            }
        }

        this.el.css({
            left: this.left,
            top: this.top
        });

        if (!isNaN(this.width)) {
            if ($.boxModel == true ) {
                this.el.width(this.width - (this.el.outerWidth(true) - this.el.width()));
            }
            else {
                this.el.width(this.width - (this.el.outerWidth(true) - this.el.width()));
            }
        }
        else {
            this.el.width(this.width);
        }
        if (!isNaN(this.height)) {
            if ($.boxModel == true) {
                this.el.height(this.height - (this.el.outerHeight(true) - this.el.height()));
            }
            else {
                this.el.height(this.height - (this.el.outerHeight(true) - this.el.height()));
            }
        }
        else {
            this.el.height(this.height);
        }



        this.fireEvent("setsize");


    }
})


UCML.Container.prototype.init = function () {
    UCML.Container.superclass.init.call(this);
}



