﻿
/** 
* @fileOverview  
* @author tanglongbin 
* @version 0.1 
*/

/** 
* @constructor UCML.ComponentMgr 
* @description 控件集合管理类 
*/
UCML.ComponentMgr = function () {
    var types = {};
    return {
        /**  
        *@function
        *@description 控件类注册
        *@param {String} ctype 控件类ctype类型
        *@param {Object} cls 控件类
        */
        registerType: function (ctype, cls) {
            types[ctype] = cls;
            cls.ctype = ctype;
        },
        /** 
        *@function 
        * @description 控件动态创建
        * @param {Json} config 控件初始化配置信息
        * @param {String} defaultType 控件ctype类型
        */
        create: function (config, defaultType) {
            if (types[config.ctype || defaultType]) {
                return new types[config.ctype || defaultType](config);
            }
            else {
                return null;
            }
        },
        getCtype: function (ctype) {
            return types[ctype];
        }
    };
} ();

/**  
* @description 控件类注册
*@static
@function
*@param {String} ctype 控件类ctype类型
*@param {Object} cls 控件类
*/
UCML.reg = UCML.ComponentMgr.registerType;

/**  
* @description 控件动态创建
*@static
* @param {Json} config 控件初始化配置信息
* @param {String} defaultType 控件ctype类型
*/
UCML.create = UCML.ComponentMgr.create;


/**  
* @description 根据字符串获得类
*@static
* @param {String} ctype类型
*/
UCML.getCtype = UCML.ComponentMgr.getCtype;


/**  
* @description 获得控件
*@static
* @param {String} id 控件id
* @return {Object} 返回控件实例对象
*/
UCML.get = function (id) {
    return window[id];
}

/** 
* @constructor UCML.Commponent 
* @description 控件基类
* @extends UCML.util.Observable
* @param {String} config dom的id或者json对象 
*/
UCML.Commponent = function (config) {
    this.config = config;

    this.isInit = this.isInit == undefined ? true : this.isInit;
    UCML.Commponent.superclass.constructor.call(this);

    this.addEvents(
    /**
    *初始化时
    *@fileOverview
    *@event
    */
    "init", "beforerender", "render", "afterrender");
    if (this.isInit) {
        if (UCML.isObject(this.config)) {
            //类对象
            UCML.apply(this, config);
        }
        this.init();
    }
}


UCML.extend(UCML.Commponent, UCML.util.Observable, {
    autoEl: 'div',
    ctype: "UCML.Commponent",
    rendeTo: null,
    /**
    *渲染状态 true为已经渲染，false为未渲染
    *@field
    */
    rendered: false,
    initState: false,
    disabled: false,
    cls: "",
    left: null,
    top: null,
    width: 'auto',
    height: 'auto',
    style: {},
    isSetProperties: true,
    /**
    *@description 控件销毁前
    *@event
    */
    onBeforeDestroy: UCML.emptyFn,
    /**
    *@description 控件销毁时
    *@function
    */
    onDestroy: UCML.emptyFn
});

/**
*@description 控件初始化
*@inner
*/
UCML.Commponent.prototype.init = function (config) {
    config = config || this.config || {};
    if (UCML.isString(config)) {//判断是否是ID
        config = config.replace("ToolBar", "Toolbar"); //生成错误 临时改变 需要王总改代码
        this.id = config;
        this.el = $("#" + this.id); //jquery对象
        this.dom = this.el[0]; //dom对象
    }
    else if (UCML.isElement(config)) {//判读是对象
        this.dom = config;
        this.id = this.id || this.dom.id || UCML.id();
        this.el = $(this.dom);
    }
    else if (!UCML.isEmpty(config[0])) { //jquery对象
        this.id = config[0].id || UCML.id();
        this.dom = config[0];
        this.el = config;
    }
    else {
        this.window = this.window || window;

        if (!UCML.isEmpty(this.el)) {
            this.dom = this.el[0];
        }
        else if (UCML.isElement(this.dom)) {
            this.el = $(this.dom);
        }
        else if (this.autoEl) {
            if (UCML.isString(this.autoEl)) {
                this.dom = this.window.document.createElement(this.autoEl);
                this.el = $(this.dom);
            } else {
                var div = document.createElement('div');
                $(this.autoEl).appendTo(div);
                this.dom = div.firstChild;
                this.el = $(this.dom);
            }
        }
        if (!this.dom.id) {
            this.id = config.id || UCML.id();
            //  this.dom.id = this.id;
        }
        else {
            this.id = this.id || this.dom.id;
        }
        if (this.rendeTo) {
            this.el.appendTo(this.rendeTo);
            delete this.rendeTo;
        }
    }

    if (!this.dom) {
        alert("页面中缺少" + this.id + "元素，或者页面中HTML标记有错");
    }
    this.tagName = this.dom.tagName.toLowerCase();
    window[this.id] = this;

    this.initState = true;
    this.fireEvent("onInit");
    if (this.isSetProperties) {
        this.setProperties(); //设置属性
    }
    this.render(); //渲染

    this.bindEvents(); //绑定事件

}

/**
*@description 控件渲染
*@function
*@inner
*/
UCML.Commponent.prototype.onRender = UCML.emptyFn;

/**
*@function
*@inner
*/
UCML.Commponent.prototype.render = function () {
    if (!this.rendered && this.fireEvent('beforerender') !== false) {
        this.rendered = true;
        this.beforeRender();
        this.onRender();
        this.fireEvent('render');
        this.afterRender();
        this.fireEvent('afterrender');
    }
    return this;
}

/**
*@description 移除控件
*@function
*/
UCML.Commponent.prototype.destroy = function () {
    this.el.remove();

    delete UCML.get(this.id);
}

/**
*@description 控件渲染前
*@event
*/
UCML.Commponent.prototype.beforeRender = UCML.emptyFn;

/**
*@description 控件渲染后
*@event
*/
UCML.Commponent.prototype.afterRender = UCML.emptyFn;

/**
*@description 控件事件绑定
*@function
*@inner
*/
UCML.Commponent.prototype.bindEvents = UCML.emptyFn;

/**
*@description 控件初始化设置dom属性
*@inner
*/
UCML.Commponent.prototype.setProperties = UCML.emptyFn;

/**
*@description 添加事件监听
*@function
*@param {String} eventName 事件名称
*@param {function} fn 事件调用函数 
*@param {Object} scope 作用于||上下文变量
*@param {Object} data 事件传递数据
*@inner 
*/
UCML.Commponent.prototype.addListener = function (eventName, fn, scope, data) {
    if (this.events && this.events[eventName]) {
        UCML.Commponent.superclass.addListener.call(this, eventName, fn, scope, data);
    }
    else {
        UCML.EventManager.addListener(this.dom, eventName, fn, scope, data);
    }
}

/**
*@description 添加事件监听
*@function
*@param {String} eventName 事件名称
*@param {function} fn 事件调用函数 
*@param {Object} scope 作用于||上下文变量
*@param {Object} data 事件传递数据
*/
UCML.Commponent.prototype.on = UCML.Commponent.prototype.addListener;

UCML.Commponent.prototype.removeListener = function (eventName, fn, scope, data) {
    if (this.events && this.events[eventName]) {
        UCML.Commponent.superclass.removeListener.call(this, eventName, fn, scope);
    }
    else {
        UCML.EventManager.removeListener(this.dom, eventName, data || scope);
    }
}

UCML.Commponent.prototype.un = UCML.Commponent.prototype.removeListener;


/**
*获得当前对象的父元素
*/
UCML.Commponent.prototype.getParentEl = function () {
    return this.el.parent();
}


/**
*访问匹配元素的样式属性
*/
UCML.Commponent.prototype.getCssValue = function (cssname) {
    return this.el.css(cssname);
}
/**
*设置一个样式属性的值。
*/
UCML.Commponent.prototype.setCssValue = function (v1, v2) {
    if (arguments.length == 2) {
        this.el.css(v1, v2); //css(name,value)
    } else {
        this.el.css(v1); //把一个“名/值对”对象设置为所有匹配元素的样式属性
    }
}

/**
*获取匹配元素在当前视口的相对偏移。
*返回的对象包含两个整形属性：top 和 left。此方法只对可见元素有效。
*/
UCML.Commponent.prototype.getOffset = function () {
    return this.el.offset();
}

/**
*设置匹配元素相对于document对象的坐标。
*.offset()方法可以让我们重新设置元素的位置。这个元素的位置是相对于document对象的。
*如果对象原先的position样式属性是static的话，会被改成relative来实现重定位。
*/
UCML.Commponent.prototype.setOffset = function (_top, _left) {
    this.el.offset({ top: _top, left: _left });
}

/**
*获取匹配元素相对父元素的偏移。
*返回的对象包含两个整形属性：top 和 left。为精确计算结果，请在补白、边框和填充属性上使用像素单位。
*此方法只对可见元素有效。
*/
UCML.Commponent.prototype.getPosition = function () {
    return this.el.position();
}

/**
*获取匹配元素相对滚动条顶部的偏移。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getScrollTop = function () {
    return this.el.scrollTop();
}

/**
*传递参数值时，设置滚动条顶部偏移为该值。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.setScrollTop = function (val) {
    this.el.scrollTop(val);
}

/**
*获取匹配元素相对滚动条左侧的偏移。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getScrollLeft = function () {
    return this.el.scrollLeft();
}

/**
*传递参数值时，设置滚动条左侧偏移为该值。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.setScrollLeft = function (val) {
    this.el.scrollLeft(val);
}

/**
*取得匹配元素当前计算的高度值(px)
*/
UCML.Commponent.prototype.getHeight = function () {
    return this.el.height();
}



/**
*为匹配的元素设置CSS高度(hidth)属性的值。
*如果没有明确指定单位（如：em或%），使用px。
*/
UCML.Commponent.prototype.setHeight = function (val) {
    this.el.height(val);
}

/**
*取得匹配元素当前计算的宽度值(px)
*/
UCML.Commponent.prototype.getWidth = function () {
    return this.el.width();
}

/**
*为匹配的元素设置CSS宽度(hidth)属性的值。
*如果没有明确指定单位（如：em或%），使用px。
*/
UCML.Commponent.prototype.setWidth = function (val) {
    this.el.width(val);
}

/**
*获取匹配元素内部区域高度（包括补白、不包括边框）。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getInnerHeight = function () {
    return this.el.innerHeight();
}

/**
*获取匹配元素内部区域宽度（包括补白、不包括边框）。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getInnerWidth = function () {
    return this.el.innerWidth();
}
/**
*获取匹配元素外部高度（默认包括补白和边框）。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getOuterHeight = function (options) {
    return this.el.outerHeight(options); //options(Boolean) : (false) 设置为 true 时，计算边距在内
}

/**
*获取匹配元素外部宽度（默认包括补白和边框）。
*此方法对可见和隐藏元素均有效。
*/
UCML.Commponent.prototype.getOuterWidth = function (options) {
    return this.el.outerWidth(options); //options(Boolean) : (false) 设置为 true 时，计算边距在内
}

/**
*控制组件显示/隐藏,true可见，flase不可见
*/
UCML.Commponent.prototype.setVisible = function (options) {
    options ? this.show() : this.hide();
}

/**
*控件显示
*/
UCML.Commponent.prototype.show = function () {
    this.el.show();
}

/**
*控件隐藏
*/
UCML.Commponent.prototype.hide = function () {
    this.el.hide();
}

/**
*该组件可见时返回true
*/
UCML.Commponent.prototype.isVisible = function () {
    if (this.el.css("display") != "" && this.el.css("display") != "none") {
        return true;
    } else {
        return false;
    }
}
/**
*切换元素的可见状态。
*如果元素是可见的，切换为隐藏的；如果元素是隐藏的，切换为可见的。
*/
UCML.Commponent.prototype.toggle = function () {
    this.el.toggle();
}
/**
*为匹配的元素加上 css 类,支持多个
*@param {String} cn 一个或多个要添加到元素中的CSS类名，请用空格分开
*/
UCML.Commponent.prototype.addClass = function (cn) {
    this.el.addClass(cn); //cn (String) : 
}

/**
*从所有匹配的元素中删除全部或者指定的类。
*@param {String} cn 一个或多个要添加到元素中的CSS类名，请用空格分开
*/
UCML.Commponent.prototype.removeClass = function (cn) {
    if (arguments.length > 0)
        this.el.removeClass(cn); //cn (String) : (可选) 一个或多个要删除的CSS类名，请用空格分开
    else
        this.el.removeClass();
}
/**
*如果存在（不存在）就删除（添加）一个类。
*@param {String} cn 元素中的CSS类名，只支持一个
*/
UCML.Commponent.prototype.toggleClass = function (cn) {
    this.el.toggleClass(cn);
}

/**
*取得匹配元素的属性值。通过这个方法可以方便地从匹配元素中获取一个属性的值。
*如果元素没有相应属性，则返回 undefined 。
*@param {String} name 属性名称
*/
UCML.Commponent.prototype.getAttribute = function (name) {
    return this.el.attr(name); //name (String) : 
}

/**
*为匹配元素设置属性值
*@param {String} v1 属性名称
*@param {String} v2 属性值
*/
UCML.Commponent.prototype.setAttribute = function (v1, v2) {
    if (arguments.length == 2) {
        this.el.attr(v1, v2); //attr(name,value)
    } else {
        this.el.attr(v1); //把一个“名/值对”对象设置为匹配元素的属性
    }
}

/**
*从匹配的元素中删除一个属性
*@param {String} name 属性名称
*/
UCML.Commponent.prototype.removeAttribute = function (name) {
    this.el.removeAttr(name);
}

/**
*取得匹配元素的html内容。这个函数不能用于XML文档。但可以用于XHTML文档。
* @return {String} 返回XHTML文档字符串
*/
UCML.Commponent.prototype.getHtml = function () {
    return this.el.html();
}

/**
*设置匹配元素的html内容。这个函数不能用于XML文档。但可以用于XHTML文档。
*@param {String} val XHTML文档字符串
*/
UCML.Commponent.prototype.setHtml = function (val) {
    this.el.html(val); //val (String) : 用于设定HTML内容的值
}

/**
*取得匹配元素的内容。
*结果是由所有匹配元素包含的文本内容组合起来的文本。这个方法对HTML和XML文档都有效。
* @return {String} 返回文本内容
*/
UCML.Commponent.prototype.getText = function () {
    return this.el.text();
}
/**
*设置匹配元素的文本内容
*与 setHtml() 类似, 但将编码 HTML (将 "<" 和 ">" 替换成相应的HTML实体).
*@param {String} val 文本内容
*/
UCML.Commponent.prototype.setText = function (val) {
    this.el.text(val); //val (String) : 用于设置元素内容的文本
}

/**
*获得匹配元素的当前值（仅支持input控件）
*select 如果多选，将返回一个数组，其包含所选的值
* @return {String} 返回值内容
*/
UCML.Commponent.prototype.getValue = function () {
    return this.el.val();
}
/**
*设置匹配元素的值
*check,select,radio等都能使用为之赋值,array (Array<String>) : 用于 check/select 的值
*@param {String} val 值内容
*/
UCML.Commponent.prototype.setValue = function (val) {
    this.el.val(val);
}

/**   
* 设置元素状态v true启用,ch是否递归子集 
*@param {Boolean} v true启用，false为不启用  
*/
UCML.Commponent.prototype.setState = function (v, ch) {
    this.disabled = v ? false : true;
    if (!this.disabled) {
        this.el.removeAttr("disabled");
    }
    else {
        this.el.attr("disabled", "disabled");
    }
}

/**   
* @description 启用控件      
*/
UCML.Commponent.prototype.enable = function (ch) {
    this.setState(true, ch);
}

/**   
* @description 禁用控件      
*/
UCML.Commponent.prototype.disable = function (ch) {
    this.setState(false, ch);
}

/**   
* 控件状态是否禁用
* @return {Boolean} 返回true为禁用，返回false为启用     
*/
UCML.Commponent.prototype.isEnable = function () {
    return this.el.attr("disabled") == "";
}

/**   
* 销毁控件
*@param {Boolean} forceDestroy 是否启用销毁前事件，true启用，false不启用    
*/
UCML.Commponent.prototype.destroy = function (forceDestroy) {
    if (forceDestroy != true && this.onBeforeDestroy) {
        if (this.onBeforeDestroy.call(this) == false) return;
    }
    this.removeNode();
    if (this.onDestroy) {
        this.onDestroy.call(this);
    }
}

/**   
* 销毁控件
*@inner  
*/
UCML.Commponent.prototype.removeNode = function (node) {
    var tnode = node || this.el;
    tnode.each(function () {
        $(this).remove();
        try {
            if ($.browser.msie) {
                this.outerHTML = '';
            }
        } catch (e) { 
        }
    });
}


UCML.reg("UCML.Commponent", UCML.Commponent);













































/*
* UCML前端BC数据及其变化数据集合类
* 主要实现以下功能：
*   1、存储BC数据(同时存储OID和记录)
*   2、存储变化数据(只存储变化数据，无OID)
*   3、增加一条记录(同时插入OID和记录或只有记录)
*   
* 
* 
*/
UCML.namespace("UCML.Common");
UCML.Common.Collection = function () {
    this.items = [];
    //this.map = {};
    this.keys = [];
    //this.length = 0;


    this.addEvents(
    "clear",
    "add",
    "replace",
    "remove",
    "sort"
    );

    UCML.Common.Collection.superclass.constructor.call(this);
};

UCML.extend(UCML.Common.Collection, UCML.util.Observable, {

    /*
    * 添加一条记录 
    * 这里判断主键
    */
    add: function (obj) {

        this.items.push(obj);

        return obj;
    },
    /*
    * 将某记录更新
    * 这里判断主键
    */
    replace: function (key, obj) {
        if (typeof key != 'undefined' && key !== null) {
            var index = this.indexOfKey(key);
            if (index != -1) {
                this.items[index] = obj;
            }
            return obj;
        }

    },
    /*
    * 删除某个元素(索引)
    */
    remove: function (o) {
        return this.removeAt(this.indexOf(o));
    },
    /*
    * 删除某个元素(索引)
    */
    removeAt: function (index) {
        if (index < this.getLength() && index >= 0) {
            var obj = this.items[index];
            this.items.splice(index, 1);

            return obj;
        }
        return false;
    },
    /*
    * 删除某个元素(OID)
    */
    removeKey: function (key) {
        return this.removeAt(this.indexOfKey(key));
    },
    /*
    * 返回某个元素的索引
    * 存在返回所在索引
    * 不存在返回-1
    */
    indexOf: function (obj) {
        return this.items.indexOf(obj);
    },
    /*
    * 返回某个主键的索引
    * 存在返回所在索引
    * 不存在返回-1
    */
    indexOfKey: function (key) {

        var index = -1;
        for (var i = 0; i < this.getItems().length; i++) {
            if (this.getItems()[i][0] == key) {
                return i;
            }
        }


        return index;
    },
    /*
    * 返回所有记录
    */
    getItems: function () {
        return this.items;
    },
    /*
    * 根据主键返回某条记录
    * 不存在返回null
    */
    item: function (key) {
        var index = this.indexOfKey(key);
        if (index == -1) {
            return null;
        }
        return this.itemAt(index);
        //        var mk = this.map[key],
        //            item = mk !== undefined ? mk : (typeof key == 'number') ? this.items[key] : undefined;
        //        return !Ext.isFunction(item) || this.allowFunctions ? item : null; // for prototype!
    },
    /*
    * 根据索引返回某条记录
    * 不存在返回null
    */
    itemAt: function (index) {
        return this.getItems()[index];
    },
    /*
    * 判断某条记录是否存在(数据在未修改的状态下可以使用)
    * 不存在返回false
    */
    contains: function (o) {
        return this.indexOf(o) != -1;
    },
    /*
    * 判断主键是否存在(仅仅判断主键)
    * 不存在返回false
    */
    containsKey: function (key) {
        return this.indexOfKey(o) != -1;
    },
    /*
    * 清除所有数据(包括Key和数据)
    */
    clear: function () {
        this.items = [];

    },
    /*
    * 定位到第一条记录
    */
    first: function () {
        return this.getItems()[0];
    },
    /*
    * 定位到最后一条记录
    */
    last: function () {
        return this.getItems()[this.getLength() - 1];
    },
    /*
    * 查找某条记录
    * 待补充
    */
    findIndex: function (property, value, start, anyMatch, caseSensitive) {
        if (UCML.isEmpty(value, false)) {
            return -1;
        }
        value = this.createValueMatcher(value, anyMatch, caseSensitive);
        return this.findIndexBy(function (o) {
            return o && value.test(o[property]);
        }, null, start);
    },
    /*
    * 查找某条记录
    * 待补充
    */
    findIndexBy: function (fn, scope, start) {
        var it = this.getItems();
        for (var i = (start || 0), len = it.length; i < len; i++) {
            if (fn.call(scope || this, it[i], it[i][0])) {
                return i;
            }
        }
        return -1;
    },
    // private
    createValueMatcher: function (value, anyMatch, caseSensitive) {
        if (!value.exec) { // not a regex
            value = String(value);
            value = new RegExp((anyMatch === true ? '' : '^') + UCML.escapeRe(value), caseSensitive ? '' : 'i');
        }
        return value;
    },
    /*
    * 复制记录
    * 参数:要拷贝的数据索引,新生成的OID
    */
    cloneByIndex: function (index, newkey) {
        var obj = this.itemAt(index);
        var newobj = [obj.length];
        for (var i = 1; i < obj.length; i++) {
            newobj[i] = obj[i];
        }
        newobj[0] = newkey;
        return this.add(newobj);
    },
    /*
    * 设置记录主键
    */
    setKey: function (rowIndex, fieldIndex, oid) {
        if (rowIndex >= 0 && rowIndex < this.getLength()) {
            if (fieldIndex == 0) {

                this.items[rowIndex][fieldIndex] = oid;
            }
            else {
                this.items[rowIndex][fieldIndex] = oid;
            }
        }
    },
    /*
    * 获取记录主键
    */
    getKey: function (rowIndex, fieldIndex) {
        if (rowIndex >= 0 && rowIndex < this.getLength()) {
            return this.items[rowIndex][fieldIndex];
        }
    },
    /*
    * 记录的长度
    */
    getLength: function () {
        return this.getItems().length;
    },
    /*
    * 记录的长度
    */
    getCount: function () {
        return this.getItems().length;
    },

    _sort: function (property, dir, fn) {
        var i,
            len,
            dsc = String(dir).toUpperCase() == 'DESC' ? -1 : 1,
            c = [], k = this.keys, items = this.items;

        fn = fn || function (a, b) {
            return a - b;
        };
        for (i = 0, len = items.length; i < len; i++) {
            c[c.length] = { key: k[i], value: items[i], index: i };
        }
        c.sort(function (a, b) {
            var v = fn(a[property], b[property]) * dsc;
            if (v === 0) {
                v = (a.index < b.index ? -1 : 1);
            }
            return v;
        });
        for (i = 0, len = c.length; i < len; i++) {
            items[i] = c[i].value;
            k[i] = c[i].key;
        }
        //this.fireEvent('sort', this);
    }

    //    /**
    //     * Sorts this collection by <b>item</b> value with the passed comparison function.
    //     * @param {String} direction (optional) 'ASC' or 'DESC'. Defaults to 'ASC'.
    //     * @param {Function} fn (optional) Comparison function that defines the sort order.
    //     * Defaults to sorting by numeric value.
    //     */
    //    sort : function(dir, fn){
    //        this._sort('value', dir, fn);
    //    }
});

UCML.Common.Collection.prototype.get = UCML.Common.Collection.prototype.item;

UCML.Common.HasTable = function () {
    UCML.Common.HasTable.superclass.constructor.call(this);
}

UCML.extend(UCML.Common.HasTable, UCML.Common.Collection, {
    add: function (key, obj) {
        if (typeof key != 'undefined' && key !== null) {
            var index = this.indexOfKey(key);
            if (index >= 0) {
                return this.replace(key, obj);
            }
        }
        this.items.push(obj);
        this.keys.push(key);
        return obj;
    },
    replace: function (key, obj) {
        if (typeof key != 'undefined' && key !== null) {
            var index = this.indexOfKey(key);
            if (index == -1) {
                return this.add(key, obj);
            }
            this.items[index] = obj;
            return obj;
        }

    },
    removeAt: function (index) {
        if (index < this.getLength() && index >= 0) {
            var obj = this.items[index];
            this.items.splice(index, 1);
            this.keys.splice(index, 1);
            return obj;
        }
        return false;
    },
    indexOfKey: function (key) {
        return this.keys.indexOf(key);
    },
    itemAt: function (index) {
        return this.items[index];
    },
    clear: function () {
        this.items = [];
        this.keys = [];
    },
    findIndexBy: function (fn, scope, start) {
        var k = this.keys, it = this.items;
        for (var i = (start || 0), len = it.length; i < len; i++) {
            if (fn.call(scope || this, it[i], k[i])) {
                return i;
            }
        }
        return -1;
    },
    cloneByIndex: function (index, newkey) {
        var obj = this.itemAt(index);
        var newobj = [obj.length];
        for (var i = 1; i < obj.length; i++) {
            newobj[i] = obj[i];
        }
        newobj[0] = newkey;
        return this.add(newkey, newobj);
    },
    /*
    * 设置记录主键
    */
    setKey: function (rowIndex, fieldIndex, oid) {
        if (rowIndex >= 0 && rowIndex < this.getLength()) {
            if (fieldIndex == 0) {
                this.keys[rowIndex] = oid;
                this.items[rowIndex][fieldIndex] = oid;
            }
            else {
                this.items[rowIndex][fieldIndex] = oid;
            }
        }
    },
    getKey: function (rowIndex, fieldIndex) {
        if (rowIndex >= 0 && rowIndex < this.getLength()) {
            if (fieldIndex == 0)
                return this.keys[rowIndex];
            else
                return this.items[rowIndex][fieldIndex];
        }
    }
});
/*
* 变化数据存储集合类
* 主要用来存储变化数据
*/
UCML.Common.ChangeDataCollection = function () {
    this.items = [];    //以索引方式存储数据集合
    UCML.Common.ChangeDataCollection.superclass.constructor.call(this);
};

UCML.extend(UCML.Common.ChangeDataCollection, UCML.util.Observable, {
    /*
    * 以关键字的方式存储增加一条记录(位置是最后)
    */
    add: function (obj) {
        this.items.push(obj);
    },
    /*
    * 在指定位置插入一个对象
    * 插入时会导致该位置的数据后移,长度+1
    */
    insert: function (index, obj) {
        if (index >= this.length) {
            return this.add(obj);
        }
        for (var i = this.items.length; i > index; i--) {
            this.items[i] = this.items[i - 1];
        }
        this.items[index] = obj;
    },
    removeAt: function (index) {
        this.items.splice(index, 1);
    },
    getCount: function () {
        return this.items.length;
    },
    getItems: function () {
        return this.items;
    },
    getItemAt: function (index) {
        return this.items[index];
    },
    clear: function () {
        this.items = [];
    }
});

/*
* UCML前端BC数据操作类
* 主要实现以下功能：
*   1、赋值、取值
*   2、增加、修改、删除、复制记录(记录变化数据)
*   3、查找某一记录、返回索引(多种检索方式OID、FiledName+Value等)
*   4、对变化数据操作(转换XML、清除变化数据等)
* 
* 
*/
UCML.namespace("UCML.Data");
UCML.Data.Table = function () {
    this.OLD = '_old';                              //标示原始数据
    this.records = [];    //BC列对象数据集(使用字段名称可检索)
    this.record = null;                             //BC列对象
    this.bcData = new UCML.Common.Collection();       //当前BC的数据集
    //this.changeData = new UCML.Common.Collection(); //变化数据集
    this.changeData = new UCML.Common.ChangeDataCollection();
    this.addEvents("load");
    //this.theDeltaData = createXMLObject();          //XML变化数据
    //this.theDeltaData.loadXML("<root/>");           //初始化XML
    this.theRecordIndex = -1;                       //当前记录指针
    this.startPos = 0;                                //开始读取记录数

    this.tableName = "";
    this.BCName = "bc";                             //BC名称
    UCML.Data.Table.superclass.constructor.call(this);
};
UCML.extend(UCML.Data.Table, UCML.util.Observable, {
    /*
    * 加载数据
    * 参数:
    *       data:数据数组
    *       records:BC的列对象
    *       这里有点问题,翻页加载数据也是调用此函数,导致BC的列重建,是否有点多余????
    */
    load: function (data, records) {
        var beginTime = new Date();
        //  if (this.record == null || this.records.getLength() == 0) {
        //debugger;
        this.record = records;
        this.records = [];


        for (var i = 0; i < records.length; i++) {
            this.records.add(records[i].fieldName);
        }

        //  }
        this.clearData();
        if (data == null || data.length == 0) {
            return;
        }

        this.bcData.items = data;

        if (this.bcData.getLength() > 0)
            this.theRecordIndex = 0;

        var endTime = new Date();
        //  alert(endTime - beginTime);
    }
, loadNoClear: function (data, records) {
    var beginTime = new Date();

    this.record = records;
    this.records = [];


    for (var i = 0; i < records.length; i++) {
        this.records.add(records[i].fieldName);
    }

    if (data == null || data.length == 0) {
        return;
    }

    var opts = this;
    data.forEach(function (item) {
        if (opts.bcData.indexOf(item)) {
            opts.bcData.add(item);
        }
    });

    if (this.bcData.getLength() > 0)
        this.theRecordIndex = 0;

    var endTime = new Date();
},
    addRecord: function (recData) {
        this.bcData.add(recData);
    },
    sort: function (index, dir) {
        sortFns = [];
        sortFns.push(this.createSortFunction(index, dir));
        var directionModifier = dir.toUpperCase() == "DESC" ? -1 : 1;
        var fn = function (r1, r2) {
            var result = sortFns[0].call(this, r1, r2);


            if (sortFns.length > 1) {
                for (var i = 1, j = sortFns.length; i < j; i++) {
                    result = result || sortFns[i].call(this, r1, r2);
                }
            }

            return directionModifier * result;
        };
        this.bcData._sort('value', dir, fn);
    }
    ,
    createSortFunction: function (index, direction) {
        direction = direction || "ASC";
        var directionModifier = direction.toUpperCase() == "DESC" ? -1 : 1;

        return function (r1, r2) {
            var v1 = r1[index],
                v2 = r2[index];
            return directionModifier * (v1 > v2 ? 1 : (v1 < v2 ? -1 : 0));
        };
    },
    cloneByIndex: function (index, newkey) {
        this.bcData.cloneByIndex(index, newkey);
    },
    /*
    * 清空数据
    */
    clearData: function () {
        this.bcData.clear();
    },
    //数组转换 追加数据状态
    //在数据的最后一列增加一列，标明是增加、或修改、或删除的数据
    DataConversion: function (obj, state) {
        var tempobj = [obj.length + 1];
        //   tempobj = obj;
        for (var i = 0; i < obj.length; i++) {
            if (obj[i] != null && obj[i] != "")
                tempobj[i] = obj[i];
            else
                tempobj[i] = "null";
        }
        tempobj[obj.length] = state;
        return tempobj;
    },
    /*
    * 添加数据
    * 参数:记录主键
    */
    Insert: function (key) {
        if (!key)
            return;
        var c = this.createBlankRec(key);

        this.bcData.add(c);
        return c;

    },
    /*
    * 删除数据
    * 参数:记录主键
    */
    Delete: function (key) {
        var row = this.bcData.item(key);
        if (row != null) {
            this.DeltaDelete(row);
            this.bcData.removeKey(key);
        }
    },
    /*
    * 根据列名称返回列的索引
    * 参数:字段名称
    */
    columnIndex: function (fieldName) {
        var index = this.records.indexOf(fieldName);
        if (index >= 0) {
            return index;
        }
        else {
            return null;
        }
    },
    /*
    * 获取某条记录的Key
    * 参数:行索引
    */
    getKey: function (index) {
        if (!index) {
            index = this.theRecordIndex;

        }
        var row = this.bcData.getItems()[index];
        if (row) {
            row[0];
        }
        else {
            return null;
        }
    },
    /*
    * 获取当前记录指定字段的值
    * 得到字段值
    */
    getFieldValue: function (fieldName) {

        var fieldIndex = this.columnIndex(fieldName);
        if (this.theRecordIndex < 0 || fieldIndex < 0) {
            return null;
        }
        return this.getFieldValueByID(fieldIndex);
    },
    /*
    * 根据列的索引获取当前记录的字段值
    */
    getFieldValueByID: function (fieldIndex) {
        if (this.theRecordIndex < 0 || fieldIndex < 0)
            return null;
        var row = this.bcData.itemAt(this.theRecordIndex);
        return row[fieldIndex];
    },
    /*
    * 根据行和列的索引获取当前记录的字段值
    */
    getFieldValueByIndexID: function (rowIndex, fieldIndex) {
        this.theRecordIndex = rowIndex;
        if (this.theRecordIndex < 0 || fieldIndex < 0)
            return null;
        var row = this.bcData.itemAt(this.theRecordIndex);
        return row[fieldIndex];
    },
    /*
    * 设置当前记录指定字段的值
    */
    setFieldValue: function (fieldName, value) {
        var fieldIndex = this.columnIndex(fieldName);
        if (this.theRecordIndex < 0 || fieldIndex < 0)
            return null;
        return this.setFieldValueByID(this.theRecordIndex, fieldIndex, value);
    },
    /*
    * 根据列的索引设置当前记录的字段值
    */
    setFieldValueByID: function (fieldIndex, value) {
        if (this.theRecordIndex < 0 || fieldIndex < 0)
            return null;
        var row = this.bcData.itemAt(this.theRecordIndex);
        if (!row || row[fieldIndex] + "" == value)
            return;

        this.bcData.getItems()[this.theRecordIndex] = row;
        this.DeltaUpdate(row, fieldIndex, value);
        row[fieldIndex] = value;
    },
    /*
    * 根据列的索引设置当前记录的字段值
    */
    setFieldValueByIndexID: function (rowIndex, fieldIndex, value) {

        this.theRecordIndex = rowIndex;
        if (this.theRecordIndex < 0 || fieldIndex < 0)
            return null;
        var row = this.bcData.itemAt(this.theRecordIndex);
        if (!row || row[fieldIndex] + "" == value)
            return;

        this.bcData.getItems()[this.theRecordIndex] = row;
        this.DeltaUpdate(row, fieldIndex, value);
        row[fieldIndex] = value;
    },
    /*
    * 根据列的索引设置当前记录的字段值
    */
    setRecordIndex: function (rowIndex) {
        this.theRecordIndex = rowIndex;
    },
    getLength: function () {
        return this.bcData.getLength();
    },
    getRecordData: function (index) {
        this.theRecordIndex = index;
        return this.bcData.itemAt(index);
    },
    //--------------------------------记录的更新操作--------------------------------
    /*
    * 修改变化数据集
    * 采用传统的方式处理
    * 处理过程：
    *   首先查找变化数据是否存在该记录
    *       如果存在，并且状态为增加的话那么直接修改该记录
    *       如果存在，并且状态为修改的话那么修改该记录下面的一条(也就是记录+1)
    *       如果不存在，将原记录保存一份，然后增加一个新记录
    */
    DeltaUpdate: function (recordData, fieldIndex, value) {
        var srcNode = null;
        var changeNode = null;
        var index = -1;
        if (recordData == null) return;
        if (value == undefined || value == null)
            value = "null";
        //判断变化数据中是否存在
        for (var i = 0; i < this.changeData.getCount(); i++) {
            var indexNode = this.changeData.getItemAt(i);
            if (indexNode[this.record.length + 1] != null && (indexNode[0] == recordData[0])) {
                srcNode = indexNode;
                index = i;

                if (srcNode[this.record.length + 1] == "ukModify") {
                    changeNode = this.changeData.getItemAt(i + 1);
                    index++;
                }
                else {
                    changeNode = srcNode;
                }
                break;
            }
        }
        if (srcNode == null)    //如果不存在
        {

            //数据转换
            srcNode = this.DataConversion(recordData, "ukModify");
            changeNode = [srcNode.length];
            //下面的循环可以考虑去掉
            for (var i = 0; i < srcNode.length; i++) {
                if (i != fieldIndex)
                    changeNode[i] = "null";
            }
            //   changeNode[0] = srcNode[0];
            changeNode[fieldIndex] = value;

            this.changeData.add(srcNode);
            this.changeData.add(changeNode);
            index = this.changeData.getCount() - 1;
        }
        else {
            //如果修改的记录和原始记录值相同时
            if (srcNode[fieldIndex] + "" == value) {
                changeNode[fieldIndex] = "null";
            }
            else {
                changeNode[fieldIndex] = value;
            }
            var nullCount = 0;
            for (var i = 0; i < changeNode.length; i++) {
                if (changeNode[i] == "null") {
                    nullCount++;
                }
            }
            //如果都是空值则清除变化记录
            if (nullCount == changeNode.length) {
                this.changeData.removeAt(index);
                this.changeData.removeAt(index - 1);
            }
            //this.changeData.itemAt(index)[fieldIndex]=value;
        }
    },
    DeltaInsert: function (recordData) {
        if (recordData == null) return;
        //   if (recordData.length == this.record.length + 1) {
        recordData[recordData.length] = "0";
        //      debugger;
        //  }
        var newNode = this.DataConversion(recordData, "ukInsert");
        this.changeData.add(newNode);
    },
    DeltaDelete: function (recordData) {
        var srcNode = null;
        var changeNode = null;
        var index = -1;
        if (recordData == null) return;
        //判断变化数据中是否存在
        for (var i = 0; i < this.changeData.getCount(); i++) {
            var indexNode = this.changeData.getItemAt(i);
            if (indexNode[0] == recordData[0] && indexNode[this.record.length + 1] != null) {
                if (indexNode[this.record.length + 1] == "ukInsert") {
                    this.changeData.removeAt(i);
                    return;
                }
                else if (indexNode[this.record.length + 1] == "ukModify") {
                    this.changeData.removeAt(i + 1);
                    this.changeData.removeAt(i);
                }
            }
        }
        srcNode = this.DataConversion(recordData, "ukDelete");
        this.changeData.add(srcNode);
    },
    IsInsert: function (id) {
        //判断变化数据中是否存在
        for (var i = 0; i < this.changeData.getCount(); i++) {
            var indexNode = this.changeData.getItemAt(i);
            if (indexNode[0] == id) {
                var upKind = indexNode[this.record.length + 1];
                if (upKind == "ukInsert") {
                    return true;
                }
                break;
            }
        }
        return false;
    },
    clearChangData: function (id) {
        //判断变化数据中是否存在
        if (this.changeData != null) {
            this.changeData.clear();
        }
    },
    //--------------------------------------End-------------------------------------
    //--------------------------记录的查询、检索、定位操作--------------------------
    /*
    * 根据字段名称和值检索数据
    * 没有找到返回-1
    */
    findByName: function (fieldName, value) {
        var fieldIndex = this.columnIndex(fieldName);
        if (fieldIndex < 0 || this.bcData.getLength() <= 0) {
            return -1;
        }
        return this.findByIndex(fieldIndex, value);
    },
    /*
    * 根据字段索引和值检索数据
    * 没有找到返回-1
    */
    findByIndex: function (fieldIndex, value) {
        if (fieldIndex < 0 || this.bcData.getLength() <= 0)
            return -1;
        for (var i = 0; i < this.bcData.getLength(); i++) {
            var row = this.bcData.itemAt(i);
            if (row[fieldIndex] + "" == value)
                return i;
        }
        return -1;
    },
    /*
    * 主键查找
    * 没有找到返回-1
    */
    findLocateOID: function (primaryKey, oid) {
        var fieldIndex = 0;
        if ((primaryKey == null) || (primaryKey == "") || (primaryKey == (this.tableName + "OID")))
        // fieldIndex = 0; 汤龙斌修改 优化主键查找速度
            return this.bcData.indexOfKey(oid);
        else {
            fieldIndex = this.columnIndex(primaryKey);
        }
        return this.findByIndex(fieldIndex, oid);
    },
    /*
    * 查找某条记录的某字段值(字段索引)
    * 没有找到返回null
    */
    LookupByIndex: function (primaryKey, oid, fieldIndex) {
        var rowIndex = this.findLocateOID(primaryKey, oid);
        if (rowIndex > -1) {
            var row = this.bcData.itemAt(rowIndex);
            return row[fieldIndex];
        }
        else
            return null;

    },
    /*
    * 查找某条记录的某字段值(字段名称)
    * 没有找到返回null
    */
    LookupByName: function (primaryKey, oid, fieldName) {
        var fieldIndex = this.columnIndex(fieldName);
        if (fieldIndex < 0 || this.bcData.getLength() <= 0)
            return null;
        return this.LookupByIndex(primaryKey, oid, fieldIndex);
    },
    getOID: function (rowIndex, primaryKey) {
        var keyIndex = this.getKeyIndex(primaryKey);
        return this.bcData.getKey(rowIndex, keyIndex);
    },
    setOID: function (rowIndex, primaryKey, oid) {
        var keyIndex = this.getKeyIndex(primaryKey);
        return this.bcData.setKey(rowIndex, keyIndex, oid);
    },
    getKeyIndex: function (primaryKey) {
        var keyIndex = 0;
        if ((primaryKey == null) || (primaryKey == "") || (primaryKey == (this.tableName + "OID")))
            keyIndex == 0;
        else
            keyIndex = this.columnIndex(primaryKey);

        return keyIndex;
    },
    //--------------------------------------End-------------------------------------

    //----------------------------------变化数据操作--------------------------------
    /*
    * 数组转换成XML数据
    */
    getChangeXML: function () {

        //var strXML='<![CDATA[<root>';
        var strXML = '<root>';
        for (var i = 0; i < this.changeData.items.length; i++) {
            //var rNode = this.theDeltaData.createNode(1, this.BCName, "");
            strXML += '<' + this.BCName + ' UpdateKind="'; //this.changeData.items[i][this.record.length+1];
            if (this.changeData.items[i][this.record.length + 1] != "" && this.changeData.items[i][this.record.length + 1] != "null") {
                strXML += this.changeData.items[i][this.record.length + 1];
            }
            strXML += '">';
            for (var j = 0; j < this.record.length; j++) {
                //    if (j == 0) {
                //        strXML += '<' + this.tableName + 'OID>' + this.changeData.items[i][j] + '</' + this.tableName + 'OID>';
                //    }
                //    else if (j < this.record.length+1) {
                strXML += '<' + this.record[j].fieldName + '>' + this.changeData.items[i][j] + '</' + this.record[j].fieldName + '>';
                //    }
            }
            strXML += '</' + this.BCName + '>';
        }
        strXML += '</root>';
        return strXML;
        /*
        var strXML='<![CDATA[<root>';
        strXML+='<'+this.BCName + 'UpdateKind="'+ this.changeData.items[i][this.record.length+1] +'">';
        strXML+='<'+ this.tableName + 'OID>' + this.changeData.items[i][j]+'</'+ this.tableName + 'OID>';
        strXML+='<'+ this.record[j - 1].fieldName + '>' + this.changeData.items[i][j]+'</'+ this.record[j - 1].FieldName+ '>';
        strXML+='</'+this.BCName +'>';
        strXML+='</root>]]>';    
        */
    },
    /*
    * 获取当前BC的变化数据记录总数
    */
    getChangeCount: function () {
        return this.changeData.getCount();
    },
    //--------------------------------------End-------------------------------------    
    createBlankRec: function (key) {
        //+2 OID 和 是否已读标记
        var row = [this.record.length + 1];
        row[0] = key;
        return row;
    }
});

function createXMLObject() {
    try {
        var l_objActiveXObject = new ActiveXObject("Msxml2.DOMDocument");
        return l_objActiveXObject;
    }
    catch (e) {
        try {
            l_objActiveXObject = new ActiveXObject("Msxml.DOMDocument");
            return l_objActiveXObject;
        }
        catch (e) {
            try {
                l_objActiveXObject = new ActiveXObject("Microsoft.XMLDOM");
                return l_objActiveXObject;
            }
            catch (e) {
                // errorHandler(e, "createXMLObject");
            }
        }
    }
}



var prefixes = ["MSXML2.DomDocument", "Microsoft.XMLDOM", "MSXML.DomDocument", "MSXML3.DomDocument"];

var isIE = navigator.userAgent.indexOf("MSIE") > 0;
var XMLJS = function () { };

XMLJS.apply = function (o, c, defaults) {
    if (defaults) {
        // no "this" reference for friendly out of scope calls
        XMLJS.apply(o, defaults);
    }
    if (o && c && typeof c == 'object') {
        for (var p in c) {
            o[p] = c[p];
        }
    }
    return o;
};

XMLJS.apply(XMLJS, {
    docObjs: new Array,
    /**
    * 初始化xml对象文件连接池
    */
    initDocs: function (count) {
        if (count == null)
            count = 5;

        for (var i = 0; i < count; i++) {
            XMLJS.docObjs[i] = new Object;
            if (isIE) {
                XMLJS.docObjs[i].doc = new ActiveXObject('Microsoft.XMLDOM');
                XMLJS.docObjs[i].doc.async = false;
                XMLJS.docObjs[i].free = true;
            }
            else {
                XMLJS.docObjs[i].doc = new DOMParser();
                XMLJS.docObjs[i].doc.async = false;
                XMLJS.docObjs[i].free = true;
            }

        }
    },

    /**
    * 获得空闲xml文件docObj对象
    */
    getFreeDocObj: function () {
        var docObj = null;

        for (var i = 0; i < XMLJS.docObjs.length; i++) {
            if (XMLJS.docObjs[i].free) {
                XMLJS.docObjs[i].free = false;
                docObj = XMLJS.docObjs[i];
                break;
            }
        }

        return docObj;
    },

    /**
    * 解析xml
    * 使用xml对象文件连接池中的空闲对象
    */
    parseXML: function (xmlContent) {
        var docObj = XMLJS.getFreeDocObj();
        if (isIE) {
            docObj.doc.loadXML(xmlContent); //解析字符串不能用load必须用loadXML.(load解析载入文档的)
            var doc = docObj.doc;
            docObj.free = true;
            return doc;
        }
        else {
            var XmlDom = docObj.doc.parseFromString(xmlContent, "text/xml");
            docObj.free = true;
            return XmlDom;
        }
    },

    /**
    * 解析xml
    * 新建Xml对象
    */
    parseXML2: function (xmlContent) {
        var xmlDoc;
        if (isIE) {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(xmlContent);
            return xmlDoc;
        }
        else {
            xmlDoc = new DOMParser();
            var XmlDom = xmlDoc.parseFromString(xmlContent, "text/xml");
            return XmlDom;
        }
    },

    /**
    * 获取xml子节点
    * @param{domNode}dom节点
    * @param{subNodeName}子节点名称
    */
    getXMLSubNodes: function (domNode, subNodeName) {
        var subNodes = [];

        var currentNode = domNode;
        var childNodes = null;

        var paths = subNodeName.split("/");

        for (var i = 0; paths != null && i < paths.length - 1; i++) {
            if (paths[i] == "")
                continue;

            childNodes = currentNode.childNodes;

            for (var j = 0; childNodes != null && j < childNodes.length; j++) {
                if (childNodes[j].nodeName.toLowerCase() == paths[i].toLowerCase()) {
                    currentNode = childNodes[j];
                    break;
                }
            }
        }

        childNodes = currentNode.childNodes;

        for (var i = 0; i < childNodes.length; i++) {
            if (childNodes[i].nodeName.toLowerCase() == paths[paths.length - 1].toLowerCase())
                subNodes.push(childNodes[i]);
        }

        return subNodes;
    },

    /**
    * 获取xml根节点
    * @param{doc}xml对象
    */
    getXMLRootNode: function (doc) {
        if (doc == null)
            return null;

        return doc.documentElement;
    },

    getXMLChildNodes: function (root) {
        var itemNodes = [];
        var nodes = root.childNodes;

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].nodeType == 1) {
                itemNodes.push(nodes[i]);
                //alert(nodes[i].nodeName + nodes[i].nodeType);
            }
        }

        return itemNodes;
    },

    getXMLChildNode: function (doc_el, nodeName) {
        var element = doc_el.getElementsByTagName(nodeName);
        return element.documentElement;
    },

    /**
    * 获取xml节点的值
    * @param{doc}xml对象
    * @param{NodeName}节点名称
    */
    getXMLNodeValue: function (doc, nodeName) {
        if (doc == null) {
            return null;
        }
        return XMLJS.getXMLSingleNodeValue(doc.documentElement, nodeName);
    },

    /**
    * 简化 DOM 访问的函数
    * @param{doc_el} req.responseXML.documentElement
    * @param{name} getElementsByTagName("name"), element name
    * @param{idx} element index,exp:elements[0].firstChild.data
    * @return nodevalue
    * 
    */
    getXMLSingleNodeValue: function (doc_el, nodeName) {
        var element = doc_el.getElementsByTagName(nodeName);
        var nodevalue = "";
        if (element[0].firstChild != null) {
            nodevalue = element[0].firstChild.nodeValue;
        }
        return nodevalue;
        //return element[0].firstChild.nodeValue;
    },

    /**
    * 获取xml节点的值
    * @param{doc}xml对象
    * @param{NodeName}节点名称
    */
    getAttributeNodeValues: function (doc_el, nodeName, attributeName) {
        if (doc_el == null) {
            return null;
        }
        var values = [];
        var len = doc_el.length;
        var value = "";
        for (var i = 0; i < len; i++) {
            if (doc_el[i].nodeName == nodeName) {
                value = doc_el[i].getAttribute(attributeName);
                values.push(value);
            }
        }
        return values;
    },

    /**
    * 获取节点属性值的方法
    * @param{doc_el} req.responseXML.documentElement
    * @param{name} getElementsByTagName("name"), element name
    * @param{nodeName} node name
    * @param{attributeName} node Attribute name
    * @return nodevalue
    * 
    */
    getAttributeNodeValue: function (doc_el, nodeName, attributeName) {
        var element = doc_el.getElementsByTagName(nodeName);
        var nodevalue = "";
        if (element[0] != null) {
            nodevalue = element[0].getAttribute(attributeName);
        }
        return nodevalue;
    }

});
/**
* @deprecated 调用初始化xml文件对象方法,必须在定义后才能调用
* 
*/
XMLJS.initDocs();

