﻿
/**
* @class UCML.Panel
* @extends UCML.Commponent
* @description 显示容器
* @param {String} id 容器控件id
*/

UCML.Panel = function (id) {
    this.addEvents("load", "beforeOpen", "open", "beforeClose", "close", "move", "maximize", "restore",
    "minimize", "beforeCollapse", "beforeExpand", "collapse", "expand");
    UCML.Panel.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Panel, UCML.Container, {
    ctype: "UCML.Panel",
    //标题
    title: null,
    //图标样式
    iconCls: null,
    //表头样式
    headerCls: "",
    //容器样式
    bodyCls: "",
    href: null,
    cache: true,
    //是否有边框 
    border: true,
    //是否无头（没有按钮，标题部分）
    noheader: false,
    content: null, // the body content if specified
    //是否允许折叠操作
    collapsible: false,
    //是否最小化操作
    minimizable: false,
    //是否最大化操作
    maximizable: false,
    closable: false,
    //初始化时 是否显示折叠状态
    collapsed: false,
    //初始化时 是否最小化
    minimized: false,
    //初始化时 是否最大化
    maximized: false,
    //是否关闭状态
    closed: false,
    tools: [],
    isLoaded: false,
    href: null,
    //是否支持动画
    animate: true,
    loadingMessage: '数据加载中....',
    onLoad: UCML.emptyFn,
    onBeforeOpen: UCML.emptyFn,
    onOpen: UCML.emptyFn,
    onBeforeClose: UCML.emptyFn,
    onClose: UCML.emptyFn,
    onMove: UCML.emptyFn,
    onMaximize: UCML.emptyFn,
    onRestore: UCML.emptyFn,
    onMinimize: UCML.emptyFn,
    onBeforeCollapse: UCML.emptyFn,
    onBeforeExpand: UCML.emptyFn,
    onCollapse: UCML.emptyFn,
    onExpand: UCML.emptyFn,
    afterRender: function () {
        UCML.Panel.superclass.afterRender.call(this);
        if (this.closed == true) {
            this.el.hide();
        } else {
            this.open();
        }
    },
    loadData: function () {
        var opts = this;
        if (this.href && (!this.isLoaded || !this.cache)) {
            this.isLoaded = false;
            var pbody = this.el.find('>div.panel-body');
            pbody.html($('<div class="panel-loading"></div>').html(this.loadingMessage));
            pbody.load(this.href, null, function () {
                //  if ($.parser) {
                //      $.parser.parse(pbody);
                //  }
                opts.fireEvent('load');
                opts.onLoad.apply(this, arguments);
                opts.isLoaded = true;
            });
        }
    },
    onRender: function () {

        this.cls = "panel " + this.cls;
        UCML.Panel.superclass.onRender.call(this);

        //  this.el.addClass("panel");
        //   var html = this.el.html();
        //    this.el.empty();

        var opts = this;

        if (!this.body) {
            $('<div class="panel-body"></div>', this.window.document).appendTo(this.el);
            this.body = this.el.children("div.panel-body");

            var elCildren = this.el.children();
            for (var i = elCildren.length - 1; i >= 0; i--) {
                if (elCildren[i] != opts.body[0]) {
                    $(elCildren[i]).prependTo(opts.body);
                }
            }
        }
        else {
            $(this.body).addClass("panel-body").appendTo(this.el);

        }

        this.body.attr("layout", this.ctype);

        if (!this.header) {
            this.createHeader();
            this.header = this.el.children("div.panel-header");
        }
        else {
            $(this.header).addClass("panel-header").appendTo(this.el);
        }

        this.header.addClass(this.headerCls);
        this.body.addClass(this.bodyCls);
        this.setBorder();

    },
    setProperties: function () {
        UCML.Panel.superclass.setProperties.call(this);

        this.title = this.el.attr('title') || this.title; // 标题名称
        this.el.attr('title', '');
        this.iconCls = this.el.attr('icon') || this.iconCls;

        this.headerCls = this.el.attr('headerCls') || this.headerCls;
        this.bodyCls = this.el.attr('bodyCls') || this.bodyCls;
        this.href = this.el.attr('href') || this.href;
        this.cache = (this.el.attr('cache') ? this.el.attr('cache') == 'true' : undefined);

        this.border = (this.el.attr('border') ? this.el.attr('border') == 'true' : this.border);
        this.noheader = (this.el.attr('noheader') ? this.el.attr('noheader') == 'true' : this.noheader); //不显示头
        this.collapsible = (this.el.attr('collapsible') ? this.el.attr('collapsible') == 'true' : this.collapsible);
        this.minimizable = (this.el.attr('minimizable') ? this.el.attr('minimizable') == 'true' : this.minimizable);
        this.maximizable = (this.el.attr('maximizable') ? this.el.attr('maximizable') == 'true' : this.maximizable);
        this.closable = (this.el.attr('closable') ? this.el.attr('closable') == 'true' : this.closable);
        this.collapsed = (this.el.attr('collapsed') ? this.el.attr('collapsed') == 'true' : this.collapsed);
        this.minimized = (this.el.attr('minimized') ? this.el.attr('minimized') == 'true' : this.minimized);
        this.maximized = (this.el.attr('maximized') ? this.el.attr('maximized') == 'true' : this.maximized);
        this.closed = (this.el.attr('closed') ? this.el.attr('closed') == 'true' : this.closed);
    },
    createHeader: function () {
        var opt = this;
        //构建头
        if (this.title && !this.noheader) {

            var header = $('<div class="panel-header"><div class="panel-title">' + this.title + '</div></div>', this.window.document).prependTo(this.el);
            if (this.iconCls) {
                header.find('.panel-title').addClass('panel-with-icon');
                $('<div class="panel-icon"></div>', this.window.document).addClass(this.iconCls).appendTo(header);
            }
            //添加工具栏
            var tool = $('<div class="panel-tool"></div>', this.window.document).appendTo(header);

            if (this.closable) {
                $('<div class="panel-tool-close"></div>', this.window.document).appendTo(tool).bind('click', function () { opt.close(); });
            }
            if (this.maximizable) {
                $('<div class="panel-tool-max"></div>', this.window.document).appendTo(tool).bind('click', function () { opt.onMax.call(opt, this); });
            }
            if (this.minimizable) {
                $('<div class="panel-tool-min"></div>', this.window.document).appendTo(tool).bind('click', function () { opt.onMin.call(opt, this); });
            }
            if (this.collapsible) {
                $('<div class="panel-tool-collapse"></div>', this.window.document).appendTo(tool).bind('click', function () { opt.onToggle.call(opt, this); });
            }
            //自定义工具栏
            if (this.tools) {
                for (var i = this.tools.length - 1; i >= 0; i--) {
                    var t = $('<div></div>').addClass(this.tools[i].iconCls).appendTo(tool);
                    if (this.tools[i].handler) {
                        t.bind('click', eval(this.tools[i].handler));
                    }
                }
            }
            tool.find('div').hover(
				function () { $(this).addClass('panel-tool-over'); },
				function () { $(this).removeClass('panel-tool-over'); }
			);
            this.el.find('>div.panel-body').removeClass('panel-body-noheader');
        } else {
            this.el.find('>div.panel-body').addClass('panel-body-noheader');
        }
    },
    close: function (forceClose) {
        if (forceClose != true) {
            if (this.onBeforeClose.call(this) == false || this.fireEvent('beforeClose') == false) return;
        }
        this.el.hide();
        this.closed = true;

        this.fireEvent('close');
        this.onClose.call(this);
    },
    open: function (forceOpen) {
        if (forceOpen != true) {
            if (this.onBeforeOpen.call(this) == false || this.fireEvent('beforeOpen') == false) return;
        }
        this.el.show();
        this.closed = false;

        this.fireEvent('open');
        this.onOpen.call(this);

        if (this.maximized == true) {
            this.maximize();
        }
        if (this.minimized == true) {
            this.minimize();
        }
        if (this.collapsed == true) {
            this.collapse();
        }

        if (!this.collapsed) {
            this.loadData();
        }
    },
    move: function (param) {
        if (param) {
            if (param.left != null) this.left = param.left;
            if (param.top != null) this.top = param.top;
        }
        this.el.css({
            left: this.left,
            top: this.top
        });
        this.fireEvent('move', [this.left, this.top]);
        this.onMove.apply(this, [this.left, this.top]);
    },
    maximize: function () {
        var tool = this.el.find('>div.panel-header .panel-tool-max');

        if (tool.hasClass('panel-tool-restore')) return;


        tool.addClass('panel-tool-restore');

        this.original = {
            width: this.width,
            height: this.height,
            left: this.left,
            top: this.top,
            alignHeight: this.alignHeight,
            alignWidth: this.alignWidth,
            ratioWidth: this.ratioWidth, //百分比宽
            ratioHeight: this.ratioHeight //百分比高
        };
        this.left = 0;
        this.top = 0;
        this.alignHeight = true;
        this.alignWidth = true;
        this.setSize();
        this.minimized = false;
        this.maximized = true;

        this.fireEvent('maximize');
        this.onMaximize.call(this);
    },
    restore: function () {
        var tool = this.el.find('>div.panel-header .panel-tool-max');

        if (!tool.hasClass('panel-tool-restore')) return;


        this.el.show();
        tool.removeClass('panel-tool-restore');
        var original = this.original;
        this.width = original.width;
        this.height = original.height;
        this.left = original.left;
        this.top = original.top;
        this.alignWidth = original.alignWidth;
        this.alignHeight = original.alignHeight;
        this.ratioWidth = original.ratioWidth;
        this.ratioHeight = original.ratioHeight;
        this.setSize();
        this.minimized = false;
        this.maximized = false;

        this.fireEvent('restore');
        this.onRestore.call(this);
    }
    , minimize: function () {
        this.el.hide();
        this.minimized = true;
        this.maximized = false;

        this.fireEvent('minimize');
        this.onMinimize.call(this);
    }
    ,
    collapse: function (param) {
        var opt = this;
        var body = this.el.find('>div.panel-body');
        var tool = this.el.find('>div.panel-header .panel-tool-collapse');

        if (tool.hasClass('panel-tool-expand')) return;

        body.stop(true, true); // stop animation
        if (this.onBeforeCollapse.call(this) == false || this.fireEvent('beforeCollapse') == false) return;

        tool.addClass('panel-tool-expand');
        if (this.animate == true) {
            body.slideUp('normal', function () {
                opt.collapsed = true;

                opt.fireEvent('collapse');
                opt.onCollapse.call(opt);
            });
        } else {
            body.hide();
            this.collapsed = true;

            this.fireEvent('collapse');
            this.onCollapse.call(this);
        }
    }
    , expand: function (param) {
        var opt = this;
        var body = this.el.find('>div.panel-body');
        var tool = this.el.find('>div.panel-header .panel-tool-collapse');

        if (!tool.hasClass('panel-tool-expand')) return;

        body.stop(true, true); // stop animation
        if (this.onBeforeExpand.call(this) == false || this.fireEvent('beforeExpand') == false) return;

        tool.removeClass('panel-tool-expand');
        if (this.animate == true) {
            body.slideDown('normal', function () {
                opt.collapsed = false;
                opt.fireEvent('expand');
                opt.onExpand.call(this);
                opt.loadData();
            });
        } else {
            body.show();

            this.collapsed = false;

            this.fireEvent('expand');
            this.onExpand.call(this);
            this.loadData();
        }
        this.setSize();
    }
    , refresh: function () {
        this.isLoaded = false;
        this.loadData();
    }
    , setBorder: function () {
        if (this.border == true) {
            this.el.find('>div.panel-header').removeClass('panel-header-noborder');
            this.el.find('>div.panel-body').removeClass('panel-body-noborder');
        } else {
            this.el.find('>div.panel-header').addClass('panel-header-noborder');
            this.el.find('>div.panel-body').addClass('panel-body-noborder');
        }
    }
    , setTitle: function (title) {
        this.title = title;
        this.header.find('div.panel-title').html(title);
    }
    , getTitle: function () {
        return this.title;
    },
     resize: function (param) {
        this.setSize(param);
        this.onResize.apply(this, [this.width, this.height]);
      //  this.el.triggerHandler('_resize');
      //  this.body.triggerHandler('_resize');
    }
    , setSize: function (param) {
        UCML.Panel.superclass.setSize.call(this, param);
        if (!isNaN(this.width)) {
            var width = this.el.width();
            if ($.boxModel == true) {
                this.header.width(width - (this.header.outerWidth(true) - this.header.width()));
                this.body.width(width - (this.body.outerWidth(true) - this.body.width()));
            } else {
                this.header.width(width - (this.header.outerWidth(true) - this.header.width()));
                this.body.width(width - (this.body.outerWidth(true) - this.body.width()));
            }
        } else {
            this.body.width('auto');
        }
        if (!isNaN(this.height)) {
            var height = this.el.height();
            if ($.boxModel == true) {
                this.body.height(height - this.header.outerHeight(true) - (this.body.outerHeight(true) - this.body.height()));
            } else {
                this.body.height(height - this.header.outerHeight(true) - (this.body.outerHeight(true) - this.body.height()));
            }
        } else {
            this.body.height('auto');
        }
        this.el.css('height', 'auto');
        this.body.triggerHandler('_resize');
    }
    , onToggle: function (el) {
        if ($(el).hasClass('panel-tool-expand')) {
            this.expand(true);
        } else {
            this.collapse(true);
        }
        return false;
    }
    , onMin: function () {
        this.minimize();
        return false;
    }
    , onMax: function () {
        var tool = this.el.find('>div.panel-header .panel-tool-max');
        if (tool.hasClass("panel-tool-restore")) {
            this.restore();
        } else {
            this.maximize();
        }
        return false;
    }
});


UCML.Panel.prototype.init = function () {
    UCML.Panel.superclass.init.call(this);
}






