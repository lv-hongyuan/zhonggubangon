﻿UCML.CheckBoxGroupBox = function (id) {
    /**   
    * @property dataValueField 
    * @description 值绑定字段
    * @type String
    */
    this.dataValueField;

    /**   
    * @property dataTextField 
    * @description 文本绑定字段
    * @type String
    */
    this.dataTextField;

    /**   
    * @property private dataValueField 
    * @description 数据源BC
    * @type UCML.DataTable
    */
    this.srcDataTable;

    this.codeTable;

    this.isCodeTable = false;

    this.Captions;
    this.Values;
    this.columns = 1;
    UCML.CheckBoxGroupBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.CheckBoxGroupBox, UCML.Combo, {
    ctype: "UCML.CheckBoxGroupBox",
    autoEl: 'input',
    panelWidth: 180,
    panelHeight: 'auto',
    setProperties: function () {
        UCML.Combo.superclass.setProperties.call(this);
        this.codeTable = this.getAttribute("codeTable") || this.codeTable;
        this.isCodeTable = (this.el.attr('isCodeTable') ? this.getAttribute('isCodeTable')
== 'true' : false) || this.isCodeTable;
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute
("srcBCName")) || undefined;
        this.dataValueField = this.getAttribute("dataValueField") || this.dataValueField;
        this.dataTextField = this.getAttribute("dataTextField") || this.dataTextField;
        this.columns = this.getAttribute("repeatColumns") || this.columns;
        this.Values = this.getAttribute("Values") || this.Values;
        this.Captions = this.getAttribute("Captions") || this.Captions;
    },
    onRender: function () {
        this.el.attr('readonly', "true");
        UCML.CheckBoxGroupBox.superclass.onRender.call(this);
		
		UCML.un(this.el, "blur");
        if (!this.CheckBoxGroup) {
            this.CheckBoxGroup = new UCML.CheckBoxGroup({ columns: this.columns, isSetProperties: false,
                codeTable: this.codeTable, isCodeTable: this.isCodeTable, dataTable: this.dataTable
        , srcDataTable: this.srcDataTable, dataValueField: this.dataValueField, dataTextField: this.dataTextField
        , fieldName: this.fieldName
        , rendeTo: this.panel.body
        , Values: this.Values
        , Captions: this.Captions,BusinessObject:this.BusinessObject
            });
            this.CheckBoxGroup.on("valuechange", this.change, this);
        }
        UCML.on(this.el, "blur", function () {//离焦时将复选框值赋给输入框，并隐藏panel
            this.setValue(this.value, false);
          //  this.hidePanel();  解决IE8下的问题
        }, this);
		
    },
    showPanel: function () {
        UCML.CheckBoxGroupBox.superclass.showPanel.call(this);
    },
    hidePanel: function () {
        UCML.CheckBoxGroupBox.superclass.hidePanel.call(this);
    },
    bindEvents: function () {
        UCML.CheckBoxGroupBox.superclass.bindEvents.call(this);
        this.CheckBoxGroup.on("databind", function () {
            // this.loadData();
        }, this);
    },
    change: function (val, text) {
        this.value = val;
        this.text = text;
        this.setText(this.text);
        this.setValue(val);
    },
    setValue: function (value, trigger) {
        if (trigger !== false) {
            this.fireEvent("setValue", value);
        }
        this.value = value;
        if (this.CheckBoxGroup) {
            this.CheckBoxGroup.setValue(this.value);
            this.setText(this.CheckBoxGroup.getText());
        }
        this.setValues([value]);

    }
});

/**
* @method renderedValue
* @description 重写该函数，通过value显示text
*/
UCML.CheckBoxGroupBox.prototype.renderedValue = function (val) {
    if (this.CheckBoxGroup && !UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.CheckBoxGroup.dataBind();
    }
    this.setValue(val);
    return this.getText();
}

/**
* @method bindCodeTable
* @description 绑定代码表
*/
UCML.CheckBoxGroupBox.prototype.bindCodeTable = function () {

}

UCML.reg("UCML.CheckBoxGroupBox", UCML.CheckBoxGroupBox);
