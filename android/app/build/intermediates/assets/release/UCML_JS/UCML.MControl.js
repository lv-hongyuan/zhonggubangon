/*
手机控件扩展
*/

//按钮
UCML.MButton = function(id) {
    UCML.MButton.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MButton, UCML.Input, {
    ctype: "UCML.MButton"
});

UCML.reg("UCML.MButton", UCML.MButton);

//滑动条
UCML.MSlider = function(id) {
    UCML.MSlider.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MSlider, UCML.Input, {
    ctype: "UCML.MSlider"
});

UCML.MSlider.prototype.init = function() {
    UCML.MSlider.superclass.init.call(this);
    this.on("blur", function() {
        this.setValue(this.dom.value);
    }, this);
    this.on("slidestop", function() {
        this.setValue(this.dom.value);
    }, this);
}

//重写setValue函数，用于重绘slider的效果
UCML.MSlider.prototype.setValue = function(val, trigger) {
    this.value = val;
    if (trigger !== false) {
        this.fireEvent("setValue", val);
    }

    if (this.tagName == "input" || this.tagName == "textarea") {
        this.el.val(val);
    } else {
        this.el.html(val);
    }

    this.el.slider("refresh");
}

UCML.reg("UCML.MSlider", UCML.MSlider);

//开关
UCML.MToggle = function(id) {
    this.ValueChecked = "1";
    this.ValueUnchecked = "0";
    UCML.MToggle.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MToggle, UCML.InputList, {
    ctype: "UCML.MToggle",
    autoEl: "select",
    setProperties: function() {
        UCML.MToggle.superclass.setProperties.call(this);
        this.ValueChecked = this.el.attr("ValueChecked") || this.ValueChecked;
        this.ValueUnchecked = this.el.attr("ValueUnchecked") || this.ValueUnchecked;
    },
    bindEvents: function() {
        UCML.MToggle.superclass.bindEvents.call(this);
        var opts = this;
        this.el.bind("change", function() {
            var value = opts.el.val();
            opts.setValue(value);
        });
    }
});

UCML.MToggle.prototype.setValue = function(val, trigger) {
    var value = val || "";
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }

    if (value == this.ValueChecked) {
        this.el[0].selectedIndex = 0;
    } else if (value == this.ValueUnchecked) {
        this.el[0].selectedIndex = 1;
    } else {
        this.el[0].selectedIndex = -1;
    }
    this.el.slider("refresh");

    this.value = value;
}

UCML.reg("UCML.MToggle", UCML.MToggle);

//复选框组
UCML.MCheckBoxGroup = function(id) {
    this.dataRole = "none";
    UCML.MCheckBoxGroup.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MCheckBoxGroup, UCML.CheckBoxGroup, {
    ctype: "UCML.MCheckBoxGroup",
    setProperties: function() {
        UCML.MCheckBoxGroup.superclass.setProperties.call(this);
        this.columns = this.getAttribute("repeatColumns") || this.columns;
        this.dataRole = this.getAttribute("data-role") || this.dataRole;
    }
});

UCML.MCheckBoxGroup.prototype.setValue = function(value, trigger) {
    value = value || "";
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    var opt = this;
    var spv = value.split(';');

    this.el.find(":checkbox").each(function() {
        this.checked = false;
        $(this).attr("checked", false);
        if (this.dataRole != "none")
            $(this).checkboxradio("refresh");
    });

    for (var i = 0; i < spv.length; i++) {
        this.el.find(":checkbox").each(function() {
            if (spv[i] == this.value) {
                this.checked = true;
            }
            $(this).attr("checked", this.checked);
            if (this.dataRole != "none")
                $(this).checkboxradio("refresh");
        });
    }
    this.value = value;
    this.text = this.getText();
}

/**   
 * @method dataBind
 * @description 绑定控件
 */
UCML.MCheckBoxGroup.prototype.dataBind = function() {
    this.el.children("table").children("tbody").empty();
    var recordCount = this.srcDataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        this.add(this.srcDataTable.getFieldValue(this.dataTextField), this.srcDataTable.getFieldValue(this.dataValueField));
    }
    this.srcDataTable.SetIndexNoEvent(0);

    this.fireEvent("databind", this, this.input);

    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.MCheckBoxGroup.prototype.bindCodeTable = function() {
    this.el.children("table").children("tbody").empty();
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    if (codeList && codeList.length > 0) {
        for (var i = 0; i < codeList.length; i++) {
            this.add(codeList[i].caption, codeList[i].value);
        }
    }
    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.MCheckBoxGroup.prototype.bindCustomData = function(texts, values) {
    this.el.children("table").children("tbody").empty();
    var valueList = values.split(";");
    var textList = texts.split(";");
    if (valueList && textList && valueList.length > 0) {
        for (var i = 0; i < valueList.length; i++) {
            this.add(textList[i], valueList[i]);
        }
    }

    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.reg("UCML.MCheckBoxGroup", UCML.MCheckBoxGroup);

//单选框组
UCML.MRadioGroup = function(id) {
    this.dataRole = "none";
    UCML.MRadioGroup.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MRadioGroup, UCML.RadioGroup, {
    ctype: "UCML.MRadioGroup",
    setProperties: function() {
        UCML.MRadioGroup.superclass.setProperties.call(this);
        this.columns = this.getAttribute("columns") || this.columns;
        this.dataRole = this.getAttribute("data-role") || this.dataRole;
    }
});

/**   
 * @method setValue
 * @description 设置控件值
 */
UCML.MRadioGroup.prototype.setValue = function(value, trigger) {
    //value = value || "";
    if (value == undefined) {
        value = "";
    }

    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    this.value = value;
    this.text = "";
    var opt = this;

    this.el.find(":radio").each(function() {
        this.checked = false;
        $(this).attr("checked", false);
        if (this.dataRole != "none")
            $(this).checkboxradio("refresh");
    });

    this.el.find(":radio[value='" + value + "']").each(function() {
        this.checked = true;
        var el = $(this);
        opt.text = el.attr("text");
        el.attr("checked", this.checked);
        if (this.dataRole != "none")
            $(this).checkboxradio("refresh");
    });
}

/**   
 * @method dataBind
 * @description 绑定控件
 */
UCML.MRadioGroup.prototype.dataBind = function() {
    this.el.children("table").children("tbody").empty();
    var recordCount = this.srcDataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        this.add(this.srcDataTable.getFieldValue(this.dataTextField), this.srcDataTable.getFieldValue(this.dataValueField));
    }
    this.srcDataTable.SetIndexNoEvent(0);

    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.MRadioGroup.prototype.bindCodeTable = function() {
    this.el.children("table").children("tbody").empty();
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    if (codeList && codeList.length > 0) {
        for (var i = 0; i < codeList.length; i++) {
            this.add(codeList[i].caption, codeList[i].value);
        }
    }

    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.MRadioGroup.prototype.bindCustomData = function(texts, values) {
    this.el.children("table").children("tbody").empty();
    var textList = texts.split(";");
    var valueList = values.split(";");
    if (textList && valueList && valueList.length > 0) {
        for (var i = 0; i < valueList.length; i++) {
            this.add(textList[i], valueList[i]);
        }
    }

    if (this.dataRole != "none")
        this.el.trigger("create");
}

UCML.reg("UCML.MRadioGroup", UCML.MRadioGroup);

UCML.MComboBox = function(id) {
    this.dataRole = "mSelect";
    UCML.MComboBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MComboBox, UCML.DropDownList, {
    ctype: "UCML.MComboBox",
    setProperties: function() {
        UCML.MComboBox.superclass.setProperties.call(this);
        this.dataRole = this.el.attr('data-role') || this.dataRole;
    }
});

UCML.MComboBox.prototype.setValue = function(value, trigger) {
    this.value = value;
    this.text = "";
    for (var i = 0; i < this.dom.options.length; i++) {

        if (this.value == this.dom.options[i].value) {
            this.text = this.dom.options[i].text;

            var opt = this.dom.options[i];

            try {
                this.dom.options[i].selected = true;

            } catch (ex) {}
        } else {
            this.dom.options[i].selected = false;
        }
    }

    if (this.JIONDataField) {
        this.dataTable.setFieldValue(this.JIONDataField, this.text);
    }

    if (this.srcDataTable && value != "") {
        this.srcDataTable.Locate(this.dataValueField, value);
    }
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }

    if (this.dataRole != "none")
        this.el.selectmenu('refresh');
}

UCML.reg("UCML.MComboBox", UCML.MComboBox);

UCML.JQMDate = function(id) {
    UCML.JQMDate.superclass.constructor.call(this, id);
}

UCML.extend(UCML.JQMDate, UCML.Input, {
    ctype: "UCML.JQMDate",
    hasDay: true,
    setProperties: function() {
        UCML.JQMDate.superclass.setProperties.call(this);
        this.hasDay = (this.el.attr('hasDay') ? this.el.attr('hasDay') == 'false' : this.hasDay);
    },
    setValue: function(val, trigger) {
        this.value = val;
        if (!this.hasDay) {
            val = val.substr(0, 7);
        }

        if (trigger !== false) {
            this.fireEvent("setValue", this.value);
        }
        this.el.val(val);
    },
    bindEvents: function() {
        UCML.JQMDate.superclass.bindEvents.call(this);
        var opts = this;
        this.el.bind("blur", function(e) {
            var value = $(this).val();
            if (!opts.hasDay) {
                value += "-01";
            }
            opts.setValue(value);
        });
    }
});

UCML.reg("UCML.JQMDate", UCML.JQMDate);

UCML.JQMMonth = function(id) {
    UCML.JQMMonth.superclass.constructor.call(this, id);
}

UCML.extend(UCML.JQMMonth, UCML.JQMDate, {
    ctype: "UCML.JQMMonth",
    hasDay: false,
    setValue: function(val, trigger) {
        UCML.JQMMonth.superclass.setValue.call(this, val, trigger);
    },
    bindEvents: function() {
        UCML.JQMMonth.superclass.bindEvents.call(this);
    }
});

UCML.reg("UCML.JQMMonth", UCML.JQMMonth);

UCML.JQMDateTime = function(id) {
    UCML.JQMDateTime.superclass.constructor.call(this, id);
}

UCML.extend(UCML.JQMDateTime, UCML.Input, {
    ctype: "UCML.JQMDateTime",
    setValue: function (val, trigger) {
        var jqmFormatDt = val.substr(0, val.length - 3).replace(" ", "T");

        this.value = val;
        if (trigger !== false) {
            this.fireEvent("setValue", val);
        }
        this.el.val(jqmFormatDt);
    },
    bindEvents: function () {
        var opts = this;
        this.el.bind("blur", function (e) {
            var value = $(this).val();
            value = value.replace("T", " ");
            if(value.length == 16) {
                value = value + ":00";
            }
            else if (value.length == 17) {
                var curr = new Date();
                value = value + "" + curr.getSeconds();
            }
            else if (value.length >= 19) {
                value = value.substr(0, 19);
            }
            opts.setValue(value);
        });
    }
});

UCML.reg("UCML.JQMDateTime", UCML.JQMDateTime);

UCML.JQMTime = function(id) {
    UCML.JQMTime.superclass.constructor.call(this, id);
}

UCML.extend(UCML.JQMTime, UCML.Input, {
    ctype: "UCML.JQMTime",
    setValue: function(val, trigger) {
        this.value = val;
        val = val.substr(0, 5);
        if (trigger !== false) {
            this.fireEvent("setValue", this.value);
        }
        this.el.val(val);
    },
    bindEvents: function() {
        var opts = this;
        this.el.bind("blur", function(e) {
            var value = $(this).val();
            value += ":00";
            opts.setValue(value);
        });
    }
});

UCML.reg("UCML.JQMTime", UCML.JQMTime);

UCML.JQMWeek = function(id) {
    UCML.JQMWeek.superclass.constructor.call(this, id);
}

UCML.extend(UCML.JQMWeek, UCML.Input, {
    ctype: "UCML.JQMWeek",
    setValue: function(val, trigger) {
        this.value = val;

        if (trigger !== false) {
            this.fireEvent("setValue", this.value);
        }
        this.el.val(val);
    },
    bindEvents: function() {
        var opts = this;
        this.el.bind("blur", function(e) {
            var value = $(this).val();
            opts.setValue(value);
        });
    }
})

UCML.reg("UCML.JQMWeek", UCML.JQMWeek);

/**
*   手机版选人控件
    1.多选人员
    2.单选人员
    3.多选岗位
    4.单选岗位
    5.多选部门
    6.单选部门
    7.多选群组
    8.单选群组
    9.多选相对岗位
    10.单选相对岗位
*/
UCML.MSelectManBox = function (id) {
    this.addEvents("manSelected");
    UCML.MSelectManBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MSelectManBox, UCML.Input, {
    ctype: "UCML.MSelectManBox",
    readOnly: true,
    otype: "1",
    destFieldName: "",
    dataValueField: "",
    BCName: "",
    setProperties: function () {
        UCML.MSelectManBox.superclass.setProperties.call(this);

        this.otype = this.getAttribute("otype") || this.otype; //选人类型字段
        this.dataFld = this.getAttribute("dataFld") || this.dataFld; //选人存名称的字段
        this.dataValueField = this.getAttribute("DataValueField") || this.dataValueField; //选人存值的字段
        if (!this.dataValueField) {
            this.dataValueField = this.dataFld;
        }
        this.BCName = this.getAttribute("BCName") || this.BCName;
        this.bc = BusinessObject.getDataTableByBCName(this.BCName);

        this.mainPageId = $.mobile.activePage.attr("id");

        this.selectPage = $('<div data-role="page" data-theme="' + $.mobile.activePage.attr("data-theme") + '" id="' + this.id + '_selectPage">' +
            '<div data-role="header">' +
            '<a id="' + this.id + '_confirmBtn" data-role="button">确定</a>' +
            '<h1 class="selectManTitle">人员选择</h1>' +
            '<a data-rel="back" data-role="button">返回</a>' +
            '</div><div id="' + this.id + '_selectPage_content" ></div></div>');

        this.searchEl = $('<input id="' + this.id + '_searchInput" placeholder="请输入关键字..." data-type="search" class="ui-input-text ui-body-c" />');
        this.selectedListEl = $('<div id="' + this.id + '_selectedList" class="selectMan_nav"></div>');

        this.listviewOuter = $('<div id="' + this.id + '_listview_outer" class="wrapper"></div>');
        this.listviewEl = $('<ul id="' + this.id + '_listview" data-role="listview">');

        this.scrollEl = $('<div class="scroller"><div id="' + this.id + '_listview_outer_pullDown" class="pullDown"><span class="pullDownIcon"></span><span class="pullDownLabel">准备刷新...</span></div><div id="' + this.id + '_listview_outer_pullUp" class="pullUp"><span class="pullUpIcon"></span><span class="pullUpLabel">上拉加载更多...</span></div></div>');

        this.UCMLIScroll = new UCML.IScroll({
            pEl: this.listviewOuter,
            ctl: this,
            pullUpAction: this.loadMoreMan
        });

        this.currData = null;
        this.reqCount = 1; //请求服务端次数
    },
    onRender: function () {
        UCML.MSelectManBox.superclass.onRender.call(this);

        if (this.readOnly) {
            this.setAttribute("readOnly", true);
        }
        var iconEl = $('<img alt app="' + UCMLLocalResourcePath + 'Images/selectMan/U.gif"/>');
        this.el.after(iconEl);
        iconEl.parent().css({
            "position": "relative",
            "padding-right": "30px"
        });
        iconEl.css({
            "position": "absolute",
            "top": "30%",
            "right": "7px"
        });

        this.setScrollTop();
    },
    bindEvents: function () {
        UCML.MSelectManBox.superclass.bindEvents.call(this);

        var opts = this;
        UCML.on(this.el, 'click', this.showSelectPage, this);

        //查询事件
        $(document).on("keyup change input", "#" + this.id + "_searchInput", function (e) {
            var searchKey = $(this).val();
            if (searchKey) {
                $.ajax({
                    url: getServicePath() + "ucml_mobile/getUsersBySearch.ashx?searchName=" + escape(searchKey) + "&type=" + opts.getSelectType(),
                    dataType: "jsonp",
                    type: "post",
                    jsonpCallback: "onManLoadBySearch",
                    success: function (dd) {
                        var data = eval(dd);

                        opts.listviewEl.html("").append(opts.getListStr(data));
                        opts.listviewEl.listview("refresh");
                    },
                    error: function () {
                        alert("fail to load");
                    }
                });
            } else {
                opts.listviewEl.html("").append(opts.getListStr(opts.currData));
                opts.listviewEl.listview("refresh");
            }
        });

        $(document).on("pageshow", "#" + this.id + "_selectPage", function () {
            opts.setScrollTop();
        });

        //确定按钮选择事件
        $(document).on("click", "#" + this.id + "_confirmBtn", function (e) {
            var checked = opts.listviewEl.find('input:checked');
            var oidStr = "",
                nameStr = "";

            var selectedData = [];
            checked.each(function (index, el) {
                if (oidStr != "") oidStr += ",";
                if (nameStr != "") nameStr += ",";
                var oid = $(this).attr("oid");
                var name = $(this).parent().text();
                if (opts.getSelectType() == "U") name = name.split("/")[0];
                oidStr += oid;
                nameStr += name;

                for (var i = 0; i < opts.currData.length; i++) {
                    if (oid.indexOf(opts.currData[i].oid) != -1) {
                        selectedData.add(opts.currData[i]);
                        break;
                    }
                }
            });

            opts.bc.setFieldValue(opts.dataFld, nameStr);
            opts.bc.setFieldValue(opts.dataValueField, oidStr);

            opts.fireEvent("manSelected", selectedData);

            //$.mobile.changePage("#" + opts.mainPageId);
            history.back();
        });

        //数据列表点击事件
        this.listviewEl.on("click", function (e) {
            if (e.target.nodeName.toLowerCase() == "li") {
                var el = $(e.target);
                var chkEl = el.find('input');
                if (chkEl.length > 0) {
                    if (chkEl.attr("type") == "checkbox") {
                        var oid = chkEl.attr('oid');
                        var text = el.text();
                        if (opts.getSelectType() == "U") text = text.split("/")[0];
                        if (chkEl.attr("checked") == "checked") {
                            var selectedItem = opts.selectedListEl.find('div[sid="' + oid + '"]');
                            if (selectedItem.length > 0) {
                                selectedItem.remove();
                            }
                            var deleteItem = opts.selectedListEl.find('div[deleteid="' + oid + '"]');
                            if (deleteItem.length > 0) {
                                deleteItem.remove();
                            }
                            chkEl.removeAttr("checked");
                        } else {
                            var src = UCMLLocalResourcePath + "Images/WebIM/chat_boy.gif";
                            var str = '<div class="selectedItem" sid="' + oid + '"><img app="' + src + '"><p>' + text + '</p></div>';
                            opts.selectedListEl.append(str);
                            chkEl.attr("checked", "checked");
                        }

                        opts.setScrollTop();
                    } else if (chkEl.attr("type") == "radio") {
                        chkEl.attr("checked", "checked");
                    }
                }
            } else if (e.target.nodeName.toLowerCase() == "input") {
                var el = $(e.target);
                if (el.attr("type") == "checkbox") {
                    var oid = el.attr('oid');
                    var text = el.parent().text();
                    if (opts.getSelectType() == "U") text = text.split("/")[0];
                    if (el.attr("checked") == "checked") {
                        var src = UCMLLocalResourcePath + "Images/WebIM/chat_boy.gif";
                        var str = '<div class="selectedItem" sid="' + oid + '"><img app="' + src + '"><p>' + text + '</p></div>';
                        opts.selectedListEl.append(str);
                        el.attr("checked", "checked");
                    } else {
                        var selectedItem = opts.selectedListEl.find('div[sid="' + oid + '"]');
                        if (selectedItem.length > 0) {
                            selectedItem.remove();
                        }
                        var deleteItem = opts.selectedListEl.find('div[deleteid="' + oid + '"]');
                        if (deleteItem.length > 0) {
                            deleteItem.remove();
                        }
                        el.removeAttr("checked");
                    }

                    opts.setScrollTop();
                }
            }
        });

        //选择列表点击事件
        this.selectedListEl.on("click", function (e) {
            if (e.target.nodeName.toLowerCase() == "img" || e.target.nodeName.toLowerCase() == "p") {
                var el = $(e.target);
                var pEl = el.parent();
                if (pEl.hasClass("selectedItem") && !UCML.isEmpty(pEl.attr("sid"))) {
                    if ($(this).find('div[deleteid="' + pEl.attr("sid") + '"]').length > 0) return;

                    var src = UCMLLocalResourcePath + "Images/Upload/minus_alt_32x32.png";
                    var str = '<div class="selectedItem" deleteid="' + pEl.attr("sid") + '"><img app="' + src + '"><p>' + pEl.text() + '</p></div>';
                    opts.selectedListEl.append(str);
                } else if (pEl.hasClass("selectedItem") && !UCML.isEmpty(pEl.attr("deleteid"))) {
                    //删除selected元素，删除自己，去checkbox的勾
                    var selectedEl = $(this).find('div[sid="' + pEl.attr("deleteid") + '"]');
                    if (selectedEl.length > 0) selectedEl.remove();

                    pEl.remove();

                    var checkboxEl = opts.listviewEl.find('li[lid="' + pEl.attr("deleteid") + '"]');
                    if (checkboxEl.length > 0) checkboxEl.find("input[type=checkbox]").removeAttr("checked");
                }

                opts.setScrollTop();
            }
        });
    }
});

UCML.MSelectManBox.prototype.setScrollTop = function() {
    var selectedListHeight = this.selectedListEl.outerHeight(true);
    this.listviewOuter.css({
        "top": (100 + selectedListHeight) + "px"
    });
}

UCML.MSelectManBox.prototype.showSelectPage = function() {
    if ($("#" + this.id + "_selectPage").length < 1) {
        this.renderPage();
    } else {
        $.mobile.changePage("#" + this.id + "_selectPage");
    }
}

UCML.MSelectManBox.prototype.getSelectType = function() {
    var type = "U";
    if (this.otype == "1" || this.otype == "2") {
        type = "U";
        this.selectPage.find(".selectManTitle").text("人员选择");
    } else if (this.otype == "3" || this.otype == "4") {
        type = "P";
        this.selectPage.find(".selectManTitle").text("岗位选择");
    } else if (this.otype == "5" || this.otype == "6") {
        type = "O";
        this.selectPage.find(".selectManTitle").text("部门选择");
    } else if (this.otype == "7" || this.otype == "8") {
        type = "G";
        this.selectPage.find(".selectManTitle").text("群组选择");
    } else if (this.otype == "9" || this.otype == "10") {
        type = "F";
        this.selectPage.find(".selectManTitle").text("相对岗位选择");
    }

    this.inputType = (parseInt(this.otype) % 2) ? "checkgroup" : "radiogroup";
    return type;
}

UCML.MSelectManBox.prototype.renderPage = function() {
    var opts = this;
    $.ajax({
        url: getServicePath() + "ucml_mobile/getUsers.ashx?reqCount=" + opts.reqCount + "&type=" + opts.getSelectType(),
        dataType: "jsonp",
        type: "post",
        jsonpCallback: "onManLoad",
        success: function(dd) {
            var data = eval(dd);
            opts.currData = data;
            //添加查询栏
            opts.selectPage.find("#" + opts.id + "_selectPage_content").append(opts.searchEl);

            //添加选择列表栏
            if (opts.inputType == "checkgroup") {
                opts.selectedListEl.append(opts.getSelectedListStr());
                opts.selectPage.find("#" + opts.id + "_selectPage_content").append(opts.selectedListEl);
            }

            //添加数据列表栏
            opts.listviewEl.append(opts.getListStr(data));
            opts.scrollEl.find(".pullUp").before(opts.listviewEl);
            opts.listviewOuter.append(opts.scrollEl);
            opts.selectPage.find("#" + opts.id + "_selectPage_content").append(opts.listviewOuter);

            //添加page
            $.mobile.activePage.after(opts.selectPage);

            if (opts.UCMLIScroll.myScroll) {
                opts.UCMLIScroll.myScroll.refresh();
            } else {
                opts.UCMLIScroll.loaded();
            }

            opts.reqCount++;

            $.mobile.changePage("#" + opts.id + "_selectPage");

        },
        error: function() {
            alert("fail to load data!");
        }
    });
}

UCML.MSelectManBox.prototype.getSelectedListStr = function() {
    var selectedValue = this.bc.getFieldValue(this.dataValueField),
        selectedText = this.bc.getFieldValue(this.dataFld);

    if (UCML.isEmpty(selectedValue) || (selectedValue == this.bc.getEmptyGuid())) return;

    var valueArr = selectedValue.split(","),
        textArr = selectedText.split(",");

    var selectedListStr = "";
    var src = UCMLLocalResourcePath + "Images/WebIM/chat_boy.gif";
    for (var i = 0; i < valueArr.length; i++) {
        selectedListStr += '<div class="selectedItem" sid="' + valueArr[i] + '"><img app="' + src + '"><p>' + textArr[i] + '</p></div>';
    }
    return selectedListStr;
}

UCML.MSelectManBox.prototype.getListStr = function (data) {
    var liststr = '';
    var name = this.id + "_" + this.inputType;

    for (var i = 0; i < data.length; i++) {
        var defaultCheck = "";
        var oid = this.formatOID(data[i].oid);
        if (this.hasCheck(oid)) defaultCheck = "checked";
        var text = data[i].name;
        if (this.getSelectType() == "U") text = data[i].name + "/" + data[i].dept;
        liststr += '<li lid="' + oid + '" class="hasCheck">' +
            '<input oid="' + oid + '" ' + defaultCheck + ' type="' + ((this.inputType == "checkgroup") ? "checkbox" : "radio") + '" name="' + name + '" data-role="none">' +
            text +
            '</li>';
    }
    return liststr;
}

UCML.MSelectManBox.prototype.formatOID = function(oid) {
    if (this.inputType == "checkgroup")
        return "(" + this.getSelectType() + ":" + oid + ")";
    else
        return oid;
}

//判断默认选中
UCML.MSelectManBox.prototype.hasCheck = function(oid) {
    var hasChecked = false;
    var checkedValue = this.bc.getFieldValue(this.dataValueField);
    if (checkedValue.indexOf(oid) > -1)
        hasChecked = true;
    return hasChecked;
}

//加载更多数据
UCML.MSelectManBox.prototype.loadMoreMan = function() {
    var opts = this.ctl;
    if (opts.currData.length % 20) return; //假如为20的倍数数据则可以加载

    $.ajax({
        url: getServicePath() + "ucml_mobile/getUsers.ashx?reqCount=" + opts.reqCount + "&type=" + opts.getSelectType(),
        dataType: "jsonp",
        type: "post",
        jsonpCallback: "onManLoad",
        success: function(dd) {
            var data = eval(dd);

            for (var i = 0; i < data.length; i++) {
                opts.currData.add(data[i]);
            }

            opts.listviewEl.append(opts.getListStr(data));

            opts.reqCount++;
        },
        error: function() {
            alert("fail to load data");
        }
    });
}

UCML.reg("UCML.MSelectManBox", UCML.MSelectManBox);

UCML.MCount = function (id) {
    this.minValue = 1;
    this.maxValue = -1;
    this.increment = 1;
    UCML.MCount.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MCount, UCML.Input, {
    ctype: "UCML.MCount",
    setProperties: function () {
        UCML.MCount.superclass.setProperties.call(this);
        this.minValue = parseInt(this.getAttribute("minValue")) || this.minValue;
        this.maxValue = parseInt(this.getAttribute("maxValue")) || this.maxValue;
        this.increment = parseInt(this.getAttribute("increment")) || this.increment;
    },
    onRender: function () {
        var outerEl = $('<div class="sn-count" style="display:inline-block"></div>');
        var minEl = $('<a href="javascript:void(0)" class="min" id="minCount"></a>');
        var input = $('<input class="input-count" value="1">');
        var addEl = $('<a href="javascript:void(0)" class="add" id="addCount"></a>');
        outerEl.append(minEl);
        outerEl.append(input);
        outerEl.append(addEl);
        this.el.append(outerEl);

        this.minEl = minEl;
        this.input = input;
        this.addEl = addEl;
    },
    setValue: function (val, trigger) {
        this.value = val;
        if (trigger !== false) {
            this.fireEvent("setValue", val);
        }

        this.input.val(val);

        if (val <= this.minValue) {
            this.minEl.addClass("cover");
        } else {
            this.minEl.removeClass("cover");
        }

        if (this.maxValue != -1 && val >= this.maxValue) {
            this.addEl.addClass("cover");
        } else {
            this.addEl.removeClass("cover");
        }
    },
    bindEvents: function () {
        UCML.MCount.superclass.bindEvents.call(this);
        var opts = this;
        //input框离焦事件
        this.input.on("blur", function () {
            var el = $(this);
            opts.setValue(el.val());
        });

        //减一图标点击事件
        this.minEl.on("click", function (ctx) {
            var el = $(this);
            if (el.hasClass("cover")) {
                return;
            }
            var value = parseInt(opts.input.val());
            opts.setValue(value - opts.increment);
        });

        //加一图标点击事件
        this.addEl.on("click", function () {
            var el = $(this);
            if (el.hasClass("cover")) {
                return;
            }

            var value = parseInt(opts.input.val());
            opts.setValue(value + opts.increment);
        });
    }
});

UCML.reg("UCML.MCount", UCML.MCount);

//日期选择
UCML.MDatePick = function (id) {

    this.calendarCls = ["hdp__day available", "selected", "in", "out"];
    this.calendarT1 = ["今天", "明天", "后天"];
    this.calendarT2 = ["开始", "结束"];
    this.calendarT3 = ["点击选择开始时间", "点击选择结束时间"];

    UCML.MDatePick.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MDatePick, UCML.Input, {
    ctype: "UCML.MDatePick",
    onRender: function () {
        UCML.MDatePick.superclass.onRender.call(this);
        this.el.attr("readonly", "readonly");
    },
    bindEvents: function () {
        UCML.MDatePick.superclass.bindEvents.call(this);
        var opts = this;
        this.el.on("click", function () {
            opts.showDatePicker();
        });
    },
    setProperties: function () {
        UCML.MDatePick.superclass.setProperties.call(this);
        this.startDateField = this.el.attr("startDateField");
        this.endDateField = this.el.attr("endDateField");
    },
    setValue: function (val, trigger) {
        this.value = val;
        if (trigger !== false) {
            this.fireEvent("setValue", val);
        }

        var dateArr = val.split(";");
        if (dateArr.length == 2) {
            var startDate = new Date(Number(dateArr[0]));
            var endDate = new Date(Number(dateArr[1]));
            this.el.val((startDate.getMonth() + 1) + "月" + startDate.getDate() + "日" + "--" + (endDate.getMonth() + 1) + "月" + endDate.getDate() + "日");
            this.dataTable.setFieldValue(this.startDateField, startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate());
            this.dataTable.setFieldValue(this.endDateField, endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate());
        } else {
            var startDate = this.dataTable.getFieldValue(this.startDateField);
            var endDate = this.dataTable.getFieldValue(this.endDateField);
            this.el.val((startDate != "" ? (startDate.substr(5, 2) + "月" + startDate.substr(8) + "日") : "-月-日") + "--" + (endDate != "" ? (endDate.substr(5, 2) + "月" + endDate.substr(8) + "日") : "-月-日"));
        }
    }
});

UCML.MDatePick.prototype.showDatePicker = function () {
    if ($("#hotel-date-picker").length == 0) {
        var outer = $('<div id="hotel-date-picker" style="-webkit-transform-origin:0px 0px;opacity:1;-webkit-transform:scale(1,1);display:block"></div>');

        var backdrop = $('<div class="backdrop"></div>');

        var hdp = $('<div class="hdp" style="overflow:hidden"></div>');

        var hdp_header = $('<div class="hdp__header"><a class="hdp__cancel" href="javascript:">取消</a><span class="hdp__tip">' + this.calendarT3[0] + '</span><a class="hdp__ok" href="javascript:">完成</a></div>');

        var hdp_week = $('<ul class="hdp__week"><li class="hdp__weekend">日</li><li>一</li><li>二</li><li>三</li><li>四</li><li>五</li><li class="hdp__weekend">六</li></ul>');

        var hdp_calendar = $('<ul class="hdp__calendar" style="overflow:auto;"></ul>');
        hdp_calendar.html(this.getCalendar());

        hdp.append(hdp_header);
        hdp.append(hdp_week);
        hdp.append(hdp_calendar);

        outer.append(backdrop);
        outer.append(hdp);

        $("body").append(outer).addClass("show-hotel-date-picker");
        $("html").css({ "font-size": "312.5%", "overflow": "hidden" });

        var opts = this;

        //按钮点击事件
        hdp_header.find("a.hdp__cancel").click(function (e) {
            $("#hotel-date-picker").hide();
            $("body").removeClass("show-hotel-date-picker");
            $("html").css({ "font-size": "100%", "overflow": "visible" });
        });

        hdp_header.find("a.hdp__ok").click(function (e) {
            if (hdp_header.find("span.hdp__tip").text() != opts.calendarT3[0] && hdp_header.find("span.hdp__tip").text() != opts.calendarT3[1]) {
                var value = hdp_calendar.find("li.in").attr("dateTs") + ";" + hdp_calendar.find("li.out").attr("dateTs");
                opts.setValue(value);
            }
            $("#hotel-date-picker").hide();
            $("body").removeClass("show-hotel-date-picker");
            $("html").css({ "font-size": "100%", "overflow": "visible" });
        });

        var lis = hdp_calendar.find("li.hdp__day");
        //日历点击事件
        lis.on("click", function (e) {
            var that = $(this);

            if (hdp_header.find("span.hdp__tip").text() != opts.calendarT3[1]) {
                hdp_calendar.find("li").attr("class", opts.calendarCls[0]);
                that.addClass(opts.calendarCls[1]);
                that.addClass(opts.calendarCls[2]);
                hdp_header.find("span.hdp__tip").text(opts.calendarT3[1]);
            } else if (hdp_header.find("span.hdp__tip").text() == opts.calendarT3[1]) {
                if (that.hasClass("in")) {
                    return;
                }

                that.addClass(opts.calendarCls[1]);
                that.addClass(opts.calendarCls[3]);
                hdp_header.find("span.hdp__tip").text(opts.calendarT3[0]);

                var startIndex = lis.index(hdp_calendar.find("li.in"));
                var endIndex = lis.index(hdp_calendar.find("li.out"));

                if (startIndex < endIndex) {
                    for (var i = startIndex; i < endIndex; i++) {
                        lis.eq(i).addClass(opts.calendarCls[1]);
                    }
                } else {
                    lis.eq(startIndex).removeClass(opts.calendarCls[2]);
                    lis.eq(startIndex).addClass(opts.calendarCls[3]);
                    lis.eq(endIndex).removeClass(opts.calendarCls[3]);
                    lis.eq(endIndex).addClass(opts.calendarCls[2]);
                    for (var i = endIndex; i < startIndex; i++) {
                        lis.eq(i).addClass(opts.calendarCls[1]);
                    }
                }

                var startDate = new Date(Number(hdp_calendar.find("li.in").attr("dateTs")));
                var endDate = new Date(Number(hdp_calendar.find("li.out").attr("dateTs")));

                hdp_header.find("span.hdp__tip").text((startDate.getMonth() + 1) + "月" + startDate.getDate() + "日--" + (endDate.getMonth() + 1) + "月" + endDate.getDate() + "日");
            }

        });
    } else {
        $("#hotel-date-picker").show();
        $("body").addClass("show-hotel-date-picker");
        $("html").css({ "font-size": "312.5%", "overflow": "hidden" });
    }
}

//获取当前月最大天数
UCML.MDatePick.prototype.getMaxDayCurMonth = function (year, month) {
    if (month == 12) {
        return ((new Date(year + 1, 1, 1)).getTime() - (new Date(year, month, 1).getTime())) / 86400000;
    } else {
        return ((new Date(year, month + 1, 1)).getTime() - (new Date(year, month, 1).getTime())) / 86400000;
    }
}

UCML.MDatePick.prototype.getCalendar = function () {
    var mydate = new Date();
    var curDay = mydate.getDay(); //当前星期
    var curDate = mydate.getDate(); //当前日
    var curMonth = mydate.getMonth() + 1; //当前月
    var curYear = mydate.getFullYear();
    var maxday = this.getMaxDayCurMonth(curYear, curMonth - 1);

    var calendarStr = "";
    if (curDay > 0) {
        calendarStr += '<li class="hdp__day--padding" style="width:' + (curDay / 7 * 100) + '%"></li>';
    }
    
    //当前月
    for (var i = 0; i <= maxday - curDate; i++) {
        var dateTs = (new Date(curYear, curMonth - 1, curDate + i)).getTime();
        calendarStr += '<li class="' + this.calendarCls[0] + '" dateTs="' + dateTs + '"><div>';
        calendarStr += '<span>' + ((curDate == 1 && i == 0) ? (curMonth + "月") : (curDate + i)) + '</span>';
        calendarStr += '<span class="textTip label">' + (i < 3 ? this.calendarT1[i] : "") + '</span>';
        calendarStr += '<span class="textTip in">' + this.calendarT2[0] + '</span>';
        calendarStr += '<span class="textTip out">' + this.calendarT2[1] + '</span>';
        calendarStr += '</div></li>';
    }

    //下一月
    var nextMonth = (curMonth == 12 ? 1 : curMonth + 1);
    var maxday_next = (curMonth == 12 ? this.getMaxDayCurMonth(curYear + 1, 0) : this.getMaxDayCurMonth(curYear, nextMonth - 1));

    for (var i = 1; i <= maxday_next; i++) {
        var dateTs = (new Date((curMonth == 12 ? curYear + 1 : curYear), nextMonth - 1, i)).getTime();
        calendarStr += '<li class="' + this.calendarCls[0] + '" dateTs="' + dateTs + '"><div>';
        calendarStr += '<span>' + ((i == 1) ? (nextMonth + "月") : i) + '</span>';
        calendarStr += '<span class="textTip label"></span>';
        calendarStr += '<span class="textTip in">' + this.calendarT2[0] + '</span>';
        calendarStr += '<span class="textTip out">' + this.calendarT2[1] + '</span>';
        calendarStr += '</div></li>';
    }

    //如果还有下下个月
    if (maxday_next + maxday - curDate < 60) {
        var nextnextMonth = nextMonth == 12 ? 1 : nextMonth + 1;
        for (var i = 1; i < 60 - maxday_next - maxday + curDate; i++) {
            var dateTs = (new Date((nextnextMonth == 12 ? curYear + 1 : curYear), nextnextMonth - 1, i)).getTime();
            calendarStr += '<li class="' + this.calendarCls[0] + '" dateTs="' + dateTs + '"><div>';
            calendarStr += '<span>' + ((i == 1) ? (nextnextMonth + "月") : i) + '</span>';
            calendarStr += '<span class="textTip label"></span>';
            calendarStr += '<span class="textTip in">' + this.calendarT2[0] + '</span>';
            calendarStr += '<span class="textTip out">' + this.calendarT2[1] + '</span>';
            calendarStr += '</div></li>';
        }
    }
    return calendarStr;
}

UCML.reg("UCML.MDatePick", UCML.MDatePick);

UCML.MInputBtn = function (id) {
    UCML.MInputBtn.superclass.constructor.call(this, id);
}