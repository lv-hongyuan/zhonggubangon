UCML.MobileDateBox = function(id) {

	this.prevYears = 10;
	this.nextYears = 10;

	UCML.MobileDateBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MobileDateBox, UCML.Input, {
	ctype: "UCML.MobileDateBox",
	autoEl: 'input',
	init: function () {
		UCML.MobileDateBox.superclass.init.call(this);
	},
	setProperties: function () { 
        UCML.MobileDateBox.superclass.setProperties.call(this);

        this.prevYears = this.getAttribute("prevYears") || this.prevYears;
        this.nextYears = this.getAttribute("nextYears") || this.nextYears;
    },
	onRender: function () {
		UCML.MobileDateBox.superclass.onRender.call(this);

		var thisType = null;
		var bcBase = eval(this.el.attr("BCName") + "Base");

		var fieldColumn;
		if(bcBase)
			fieldColumn= bcBase.getColumn(this.el.attr("dataFld"));
		
		if(fieldColumn) {
			thisType = fieldColumn.fieldType.toLowerCase();
		}else {
			thisType = "date";
		}

		var currYear = (new Date()).getFullYear();
		var opt = {};
		opt.date = {preset : 'date'};
		opt.datetime = {preset : 'datetime'};
		opt.time = {preset : 'time'};

		var opts = this.el;
		opt.default = {
			theme: 'android-ics light', //皮肤样式
	        display: 'modal', //显示方式【modal】【inline】【bubble】【top】【bottom】
	        mode: 'scroller', //日期选择模式【scroller】【clickpick】【mixed】
			lang:'zh',
			//dateFormat: 'yyyy-mm-dd', // 日期格式
			//setText: '确定', //确认按钮名称
			//cancelText: '清空',//取消按钮名称
			//dateOrder: 'yymmdd', //面板中日期排列格
			//dayText: '日',
			//monthText: '月',
			//yearText: '年',
			//showNow: true,
			//nowText: "明天",
			//howOnFocus: false,
			//height: 45,
			//width: 90
			//rows: 3,
			onSelect: function(value, inst){
				if(bcBase) {
					if(thisType.indexOf('time') != -1) {
						var currSec = (new Date()).getSeconds() + "";
						currSec = (currSec.length == 1 ? "0" + currSec : currSec);
						bcBase.setFieldValue(opts.attr("dataFld"), value + ":" + currSec);
					}else {
						bcBase.setFieldValue(opts.attr("dataFld"), value);
					}
				}
			},
	        startYear: currYear - this.prevYears, //开始年份
	        endYear: currYear + (this.nextYears - 0) //结束年份
		};

		this.el.mobiscroll($.extend(opt['default'], opt[thisType]));
    }
});

UCML.MobileDateBox.prototype.bindEvents = function () {

	UCML.MobileDateBox.superclass.bindEvents.call(this);
}

UCML.reg("UCML.MobileDateBox", UCML.MobileDateBox);