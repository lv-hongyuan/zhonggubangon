﻿UCML.ListView = function(id) {
	//this.id = id;
	
	UCML.ListView.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ListView, UCML.Applet, {
	autoEl: "ul",
	groupField: "SYS_Created",//分组字段
	groupType: "date",
	listField: "ActivityChineseName",//显示名称字段
	listTheme: "a",//显示风格
	
	onRender:function(){

	},
	open: function () {
        UCML.ListView.superclass.open.call(this);
    }
});

UCML.ListView.prototype.bindEvents = function() {
	UCML.ListView.superclass.bindEvents.call(this);
	
	if(!UCML.isEmpty(this.dataTable)){
		this.dataTable.on("onLoad", this.loadData, this);
	}
	
	var opts = this;
	//listview向右滑动事件
	$("#"+this.id).bind("swiperight", function(){
		if(opts.getPageIndex() > 1) {
			$.mobile.loading('show', {
				text: '加载中...', //加载器中显示的文字  
				textVisible: true, //是否显示文字  
				theme: 'a',        //加载器主题样式a-e  
				textonly: false,   //是否只显示文字  
				html: ""           //要显示的html内容，如图片等  
			});
			opts.dataTable.LoadPrevPage();
		}else {
			$.mobile.loading('show', {
				text: '第一页', //加载器中显示的文字  
				textVisible: true, //是否显示文字  
				theme: 'a',        //加载器主题样式a-e  
				textonly: true,   //是否只显示文字  
				html: ""           //要显示的html内容，如图片等  
			});
			
			var interval = setInterval(function(){
				$.mobile.loading('hide');
				//clearInterval("interval");
				}, 3000);
		}
	});
	
	//listview向左滑动事件
	$("#"+this.id).bind("swipeleft", function(){
		if(opts.getPageIndex() < opts.getPageCount()){
			$.mobile.loading('show', {
				text: '加载中...', //加载器中显示的文字  
				textVisible: true, //是否显示文字  
				theme: 'a',        //加载器主题样式a-e  
				textonly: false,   //是否只显示文字  
				html: ""           //要显示的html内容，如图片等  
			});
			opts.dataTable.LoadMoreData();
		}else {
			$.mobile.loading('show', {
				text: '最后一页', //加载器中显示的文字  
				textVisible: true, //是否显示文字  
				theme: 'a',        //加载器主题样式a-e  
				textonly: true,   //是否只显示文字  
				html: ""           //要显示的html内容，如图片等  
			});
			
			var interval = setInterval(function(){
				$.mobile.loading('hide');
				//clearInterval("interval");
				}, 3000);
		}
	});
	
	//listview点击事件
	$("a."+this.el.attr("linkID")+"Item").each(function(index,el){
		$(this).bind("click",function(){
			opts.onMenuClick(index,el);
		});
	});
	
}

UCML.ListView.prototype.refreshList = function() {
	this.el.listview("refresh");
}

UCML.ListView.prototype.loadData = function(data) {
	var oldIndex = this.dataTable.recordIndex;
	var recCount = this.dataTable.getRecordCount();
	var lastResult = null;
	
	if(this.el.html() != "") {
		this.el.html("");
	}
	
	for(var i=0; i<recCount; i++) {
		this.dataTable.SetIndexNoEvent(i);
		
		var listName = this.dataTable.getFieldValue(this.listField);
		var groupValue = this.dataTable.getFieldValue(this.groupField);
		var result = this.compareDate(groupValue.substring(0,10));
			
		if(lastResult !== result)
		{	//创建分组
			this.el.append("<li data-role=\"list-divider\">" + result + "</li>");
		}
		
		this.el.append("<li><a href=\"#" + this.el.attr("linkID") + "\" class=\"" + this.el.attr("linkID") + "\" data-rel=\"popup\" data-transition=\"none\">" + listName + "</a></li>");	
		lastResult =result;
	}
	this.dataTable.SetIndexNoEvent(oldIndex);	
		
	var opts = this;
	//menuitem点击事件
	$("a."+this.el.attr("linkID")).each(function(index){
		$(this).bind("click",function(){
			//定位数据
			opts.dataTable.SetIndexNoEvent(index);
		});
	});
	
	this.refreshList();
	$.mobile.loading('hide');
}

UCML.ListView.prototype.compareDate = function(str) {
	var date = new Date();
	var compStr = date.getFullYear() + "-" + String.leftPad(date.getMonth() + 1, 2, '0') + "-" + String.leftPad(date.getDate(), 2, '0');//今天
	date = date.addDays(-1);
	var compStr2 = date.getFullYear() + "-" + String.leftPad(date.getMonth() + 1, 2, '0') + "-" + String.leftPad(date.getDate(), 2, '0');//昨天
		
	if(str == compStr)
		return "今天";
	else if(str == compStr2)
		return "昨天";
	else
		return str;
}

UCML.ListView.prototype.onMenuClick = function(index,el) {
	var name = this.dataTable.getFieldValue(this.listField);
	
	if($(el).hasClass("exeTask")){//执行任务

		if (this.dataTable.getRecordCount()==0) return;
		var fReadTask = this.dataTable.getFieldValue("fReadTask");
		var TaskKind = this.dataTable.getFieldValue("TaskKind");
		var OID = this.dataTable.getOID();
		var FlowID = this.dataTable.getFieldValue("FlowID");
		var ActivityID = this.dataTable.getFieldValue("ActivityID");
		var InstanceID = this.dataTable.getFieldValue("InstanceID");
		var BPOID = this.dataTable.getFieldValue("BPOID");
		if(BPOID.indexOf("_Mobile")==-1)//手机版默认加后缀
		{
		   BPOID=BPOID+ "_Mobile";
		}
		var URL =  "../"+BPOID+".HTML?TaskKind="+TaskKind+"&fReadTask="+fReadTask+"&COMMAND=FinishTask&TaskID="+OID+"&InstanceID="+InstanceID+"&callbackFn=taskCallBackFn&FlowID="+FlowID+"&ActivityID="+ActivityID;
		window.location.href = URL;

	}else if($(el).hasClass("flowTrace")){//流程跟踪
		if (this.dataTable.getRecordCount()==0) return;
		var AssignTaskOID   = this.dataTable.getOID();
		var InstanceID = this.dataTable.getFieldValue("InstanceID");

		var URL = getServicePath()+"UCMLCommon/WF/BPO_WF_FlowTrace.aspx?TaskID="+AssignTaskOID+"&InstanceID="+InstanceID;
		var config = {
			frameMode: "parent", maximizable: true,
            collapsible: true, URL: URL, scroll: "yes", draggable: true,
            resizable: true, title: "流程图"
		}
		var w = new UCML.OpenShowWindow(config);
        w.open();

	}else if($(el).hasClass("rollBack")){//任务回滚
		
	}else if($(el).hasClass("entrustTask")){//委托任务
		
	}
}

UCML.ListView.prototype.getPageIndex = function() {

	var pageNumber = parseInt(this.dataTable.getDefaultPageCount());
	
	return parseInt(parseInt(this.dataTable.StartPos) / pageNumber) + 1;
}

UCML.ListView.prototype.getPageCount = function() {
	var totalRecordCount = parseInt(this.dataTable.getTotalRecordCount());
	if (totalRecordCount == 0) {
        totalRecordCount = this.dataTable.getRecordCount();
    }
	var pageNumber = parseInt(this.dataTable.getDefaultPageCount());
	
	var pageCount = parseInt(totalRecordCount / pageNumber);
    if (totalRecordCount % pageNumber != 0) {
		pageCount++;
    }
	return pageCount;
}