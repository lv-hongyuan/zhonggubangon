﻿
UCML = {

    version: '2.3.8.5',
    updated: '2013-12-27'
};

if (window["UCML_Services"]) {//前端版本与后台版本进行验证
    if (UCML_Services.version > UCML.version) {
        alert("您的前端框架与后台DLL不匹配，请更新");
    }
}


UCML.apply = function (o, c, defaults) {
    if (defaults) {
        UCML.apply(o, defaults);
    }
    if (o && c && typeof c == 'object') {
        for (var p in c) {
            o[p] = c[p];
        }
    }
    return o;
};


(function () {
    var idSeed = 0,
        toString = Object.prototype.toString,
        ua = navigator.userAgent.toLowerCase(),
        check = function (r) {
            return r.test(ua);
        },
        DOC = document,
        isStrict = DOC.compatMode == "CSS1Compat",
        isOpera = check(/opera/),
        isChrome = check(/chrome/),
        isWebKit = check(/webkit/),
        isSafari = !isChrome && check(/safari/),
        isSafari2 = isSafari && check(/applewebkit\/4/),
        isSafari3 = isSafari && check(/version\/3/),
        isSafari4 = isSafari && check(/version\/4/),
        isIE = !isOpera && check(/msie/),
        isIE7 = isIE && check(/msie 7/),
        isIE8 = isIE && check(/msie 8/),
        isIE6 = isIE && !isIE7 && !isIE8,
        isGecko = !isWebKit && check(/gecko/),
        isGecko2 = isGecko && check(/rv:1\.8/),
        isGecko3 = isGecko && check(/rv:1\.9/),
        isBorderBox = isIE && !isStrict,
        isWindows = check(/windows|win32/),
        isMac = check(/macintosh|mac os x/),
        isAir = check(/adobeair/),
        isLinux = check(/linux/),
        isSecure = /^https/i.test(window.location.protocol);


    if (isIE6) {
        try {
            DOC.execCommand("BackgroundImageCache", false, true);
        } catch (e) { }
    }

    UCML.apply(UCML, {

        SSL_SECURE_URL: isSecure && isIE ? 'javascript:""' : 'about:blank',

        isStrict: isStrict,

        isSecure: isSecure,

        isReady: false,




        enableGarbageCollector: true,


        enableListenerCollection: false,


        enableNestedListenerRemoval: false,


        USE_NATIVE_JSON: false,


        applyIf: function (o, c) {
            if (o) {
                for (var p in c) {
                    if (!UCML.isDefined(o[p])) {
                        o[p] = c[p];
                    }
                }
            }
            return o;
        },


        id: function (el, prefix) {
            return (el = UCML.getDom(el) || {}).id = el.id || (prefix || "UCML-gen") + (++idSeed);
        },
        extend: function () {

            var io = function (o) {
                for (var m in o) {
                    this[m] = o[m];
                }
            };
            var oc = Object.prototype.constructor;

            return function (sb, sp, overrides) {
                if (UCML.isObject(sp)) {
                    overrides = sp;
                    sp = sb;
                    sb = overrides.constructor != oc ? overrides.constructor : function () { sp.apply(this, arguments); };
                }
                var F = function () { },
                    sbp,
                    spp = sp.prototype;

                F.prototype = spp;
                sbp = sb.prototype = new F();
                sbp.constructor = sb;
                sb.superclass = spp;
                if (spp.constructor == oc) {
                    spp.constructor = sp;
                }
                sb.override = function (o) {
                    UCML.override(sb, o);
                };
                sbp.superclass = sbp.supr = (function () {
                    return spp;
                });
                sbp.override = io;
                UCML.override(sb, overrides);
                sb.extend = function (o) { return UCML.extend(sb, o); };
                return sb;
            };
        } (),


        override: function (origclass, overrides) {
            if (overrides) {
                var p = origclass.prototype;
                UCML.apply(p, overrides);
                if (UCML.isIE && overrides.hasOwnProperty('toString')) {
                    p.toString = overrides.toString;
                }
            }
        },


        namespace: function () {
            var o, d;
            UCML.each(arguments, function (v) {
                d = v.split(".");
                o = window[d[0]] = window[d[0]] || {};
                UCML.each(d.slice(1), function (v2) {
                    o = o[v2] = o[v2] || {};
                });
            });
            return o;
        },
        emptyFn: function () {
        },
        urlEncode: function (o, pre) {
            var empty,
                buf = [],
                e = encodeURIComponent;

            UCML.iterate(o, function (key, item) {
                empty = UCML.isEmpty(item);
                UCML.each(empty ? key : item, function (val) {
                    buf.push('&', e(key), '=', (!UCML.isEmpty(val) && (val != key || !empty)) ? (UCML.isDate(val) ? UCML.encode(val).replace(/"/g, '') : e(val)) : '');
                });
            });
            if (!pre) {
                buf.shift();
                pre = '';
            }
            return pre + buf.join('');
        },


        urlDecode: function (string, overwrite) {
            if (UCML.isEmpty(string)) {
                return {};
            }
            var obj = {},
                pairs = string.split('&'),
                d = decodeURIComponent,
                name,
                value;
            UCML.each(pairs, function (pair) {
                pair = pair.split('=');
                name = d(pair[0]);
                value = d(pair[1]);
                obj[name] = overwrite || !obj[name] ? value :
                            [].concat(obj[name]).concat(value);
            });
            return obj;
        },


        urlAppend: function (url, s) {
            if (!UCML.isEmpty(s)) {
                return url + (url.indexOf('?') === -1 ? '?' : '&') + s;
            }
            return url;
        },


        toArray: function () {
            return isIE ?
                 function (a, i, j, res) {
                     res = [];
                     for (var x = 0, len = a.length; x < len; x++) {
                         res.push(a[x]);
                     }
                     return res.slice(i || 0, j || res.length);
                 } :
                 function (a, i, j) {
                     return Array.prototype.slice.call(a, i || 0, j || a.length);
                 }
        } (),

        isIterable: function (v) {

            if (UCML.isArray(v) || v.callee) {
                return true;
            }

            if (/NodeList|HTMLCollection/.test(toString.call(v))) {
                return true;
            }


            return ((v.nextNode || v.item) && UCML.isNumber(v.length));
        },


        each: function (array, fn, scope) {
            if (UCML.isEmpty(array, true)) {
                return;
            }
            if (!UCML.isIterable(array) || UCML.isPrimitive(array)) {
                array = [array];
            }
            for (var i = 0, len = array.length; i < len; i++) {
                if (fn.call(scope || array[i], array[i], i, array) === false) {
                    return i;
                };
            }
        },


        getDom: function (el) {
            if (!el || !DOC) {
                return null;
            }
            return el.dom ? el.dom : (UCML.isString(el) ? DOC.getElementById(el) : el);
        },


        getBody: function () {
            return UCML.get(DOC.body || DOC.documentElement);
        },

        isEmpty: function (v, allowBlank) {
            return v === null || v === undefined || ((UCML.isArray(v) && !v.length)) || (!allowBlank ? v === '' : false);
        },


        isArray: function (v) {
            return toString.apply(v) === '[object Array]';
        },


        isDate: function (v) {
            return toString.apply(v) === '[object Date]';
        },


        isObject: function (v) {
            return !!v && Object.prototype.toString.call(v) === '[object Object]';
        },


        isPrimitive: function (v) {
            return UCML.isString(v) || UCML.isNumber(v) || UCML.isBoolean(v);
        },


        isFunction: function (v) {
            return toString.apply(v) === '[object Function]';
        },


        isNumber: function (v) {
            return typeof v === 'number' && isFinite(v);
        },


        isString: function (v) {
            return typeof v === 'string';
        },


        isBoolean: function (v) {
            return typeof v === 'boolean';
        },


        isElement: function (v) {
            return !!v && v.tagName;
        },


        isDefined: function (v) {
            return typeof v !== 'undefined';
        },

        transXMLToText: function (result) {
            var text = result.text;
            if (UCML.isIE) {
                text = result.documentElement.text;
            }
            else {
                text = result.documentElement.textContent;
            }
            return text;
        }
        ,

        isOpera: isOpera,

        isWebKit: isWebKit,

        isChrome: isChrome,

        isSafari: isSafari,

        isSafari3: isSafari3,

        isSafari4: isSafari4,

        isSafari2: isSafari2,

        isIE: isIE,

        isIE6: isIE6,

        isIE7: isIE7,

        isIE8: isIE8,

        isGecko: isGecko,

        isGecko2: isGecko2,

        isGecko3: isGecko3,

        isBorderBox: isBorderBox,

        isLinux: isLinux,

        isWindows: isWindows,

        isMac: isMac,

        isAir: isAir
    });


    UCML.ns = UCML.namespace;
})();



UCML.apply(Function.prototype, {

    createInterceptor: function (fcn, scope) {
        var method = this;
        return !UCML.isFunction(fcn) ?
                this :
                function () {
                    var me = this,
                        args = arguments;
                    fcn.target = me;
                    fcn.method = method;
                    return (fcn.apply(scope || me || window, args) !== false) ?
                            method.apply(me || window, args) :
                            null;
                };
    },


    createCallback: function () {

        var args = arguments,
            method = this;
        return function () {
            return method.apply(window, args);
        };
    },


    createDelegate: function (obj, args, appendArgs) {
        var method = this;
        return function () {
            var callArgs = args || arguments;
            if (appendArgs === true) {
                callArgs = Array.prototype.slice.call(arguments, 0);
                callArgs = callArgs.concat(args);
            } else if (UCML.isNumber(appendArgs)) {
                callArgs = Array.prototype.slice.call(arguments, 0);
                var applyArgs = [appendArgs, 0].concat(args);
                Array.prototype.splice.apply(callArgs, applyArgs);
            }
            return method.apply(obj || window, callArgs);
        };
    },


    defer: function (millis, obj, args, appendArgs) {
        var fn = this.createDelegate(obj, args, appendArgs);
        if (millis > 0) {
            return setTimeout(fn, millis);
        }
        fn();
        return 0;
    }
});

UCML.applyIf(String, {

    format: function (format) {
        var args = UCML.toArray(arguments, 1);
        return format.replace(/\{(\d+)\}/g, function (m, i) {
            return args[i];
        });
    },
    formatToArray: function (format, array) {
        return format.replace(/\{(\d+)\}/g, function (m, i) {
            return array[i];
        });
    }
});


UCML.applyIf(Array.prototype, {

    indexOf: function (o, from) {
        var len = this.length;
        from = from || 0;
        from += (from < 0) ? len : 0;
        for (; from < len; ++from) {
            if (this[from] === o) {
                return from;
            }
        }
        return -1;
    },
    add: function (o) {
        this[this.length] = o;
        return o;
    },
    remove: function (o) {
        var index = this.indexOf(o);
        if (index != -1) {
            this.splice(index, 1);
        }
        return this;
    }
});





UCML.apply(Function.prototype, {

    createSequence: function (fcn, scope) {
        var method = this;
        return !UCML.isFunction(fcn) ?
                this :
                function () {
                    var retval = method.apply(this || window, arguments);
                    fcn.apply(scope || this || window, arguments);
                    return retval;
                };
    }
});



UCML.applyIf(String, {


    escape: function (string) {
        return string.replace(/('|\\)/g, "\\$1");
    },
    leftPad: function (val, size, ch) {
        var result = String(val);
        if (!ch) {
            ch = " ";
        }
        while (result.length < size) {
            result = ch + result;
        }
        return result;
    }
});


String.prototype.toggle = function (value, other) {
    return this == value ? other : value;
};


String.prototype.trim = function () {
    var re = /^\s+|\s+$/g;
    return function () { return this.replace(re, ""); };
} ();



Date.prototype.getElapsed = function (date) {
    return Math.abs((date || new Date()).getTime() - this.getTime());
};

Date.prototype.add = function (milliseconds) {
    var m = this.getTime() + milliseconds;
    return new Date(m);
};
Date.prototype.addSeconds = function (second) {
    return this.add(second * 1000);
};
Date.prototype.addMinutes = function (minute) {
    return this.addSeconds(minute * 60);
};
Date.prototype.addHours = function (hour) {
    return this.addMinutes(60 * hour);
};

Date.prototype.addDays = function (day) {
    return this.addHours(day * 24);
};

Date.isLeepYear = function (year) {
    return ((year % 4 == 0 && year % 100 != 0)|| year % 400 == 0)
};

Date.daysInMonth = function (year, month) {
    if (month == 2) {
        if ((year % 4 == 0 && year % 100 != 0)|| year % 400 == 0)
            return 29;
        else
            return 28;
    }
    else if ((month <= 7 && month % 2 == 1) || (month > 7 && month % 2 == 0))
        return 31;
    else
        return 30;
};

Date.prototype.addMonth = function () {
    var m = this.getMonth();
    if (m == 11) return new Date(this.getFullYear() + 1, 0, this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds());

    var daysInNextMonth = Date.daysInMonth(this.getFullYear(), this.getMonth() + 1);
    var day = this.getDate();
    if (day > daysInNextMonth) {
        day = daysInNextMonth;
    }
    return new Date(this.getFullYear(), this.getMonth() + 1, day, this.getHours(), this.getMinutes(), this.getSeconds());
};

Date.prototype.subMonth = function () {
    var m = this.getMonth();
    if (m == 0) return new Date(this.getFullYear() - 1, 11, this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds());
    var day = this.getDate();
    var daysInPreviousMonth = Date.daysInMonth(this.getFullYear(), this.getMonth());
    if (day > daysInPreviousMonth) {
        day = daysInPreviousMonth;
    }
    return new Date(this.getFullYear(), this.getMonth() - 1, day, this.getHours(), this.getMinutes(), this.getSeconds());
};

Date.prototype.addMonths = function (addMonth) {
    var result = this;	
    if (addMonth > 0) {
        while (addMonth > 0) {
            result = result.addMonth();
            addMonth--;
        }
    } else if (addMonth < 0) {
        while (addMonth < 0) {
            result = result.subMonth();
            addMonth++;
        }
    } else {
        result = this;
    }
    return result;
};

Date.prototype.addYears = function (year) {
    return new Date(parseInt(this.getFullYear()) + parseInt(year), this.getMonth(), this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds());
};


UCML.applyIf(Number.prototype, {

    constrain: function (min, max) {
        return Math.min(Math.max(this, min), max);
    }
});

UCML.ns("UCML.util");
/*---------------------------------------自定义事件-------------------------------------------*/
(function () {

    var UCMLUTIL = UCML.util,
    TOARRAY = UCML.toArray,
    EACH = UCML.each,
    ISOBJECT = UCML.isObject,
    TRUE = true,
    FALSE = false;
    UCMLUTIL.Observable = function () {
        var me = this, e = me.events;
        if (me.listeners) {
            me.on(me.listeners);
            delete me.listeners;
        }
        me.events = e || {};
    };

    UCMLUTIL.Observable.prototype = {
        // private
        filterOptRe: /^(?:scope|delay|buffer|single)$/,

        fireEvent: function () {

            var a = TOARRAY(arguments),
            ename = a[0].toLowerCase(),
            me = this,
            ret = TRUE,
            ce = me.events[ename],
            q,
            c;
            if (me.eventsSuspended === TRUE) {
                if (q = me.eventQueue) {
                    q.push(a);
                }
            }
            else if (ISOBJECT(ce) && ce.bubble) {
                if (ce.fire.apply(ce, a.slice(1)) === FALSE) {
                    return FALSE;
                }
                c = me.getBubbleTarget && me.getBubbleTarget();
                if (c && c.enableBubble) {
                    if (!c.events[ename] || !UCML.isObject(c.events[ename]) || !c.events[ename].bubble) {
                        c.enableBubble(ename);
                    }
                    return c.fireEvent.apply(c, a);
                }
            }
            else {
                if (ISOBJECT(ce)) {
                    a.shift();
                    ret = ce.fire.apply(ce, a);
                }
            }
            return ret;
        },
        addListener: function (eventName, fn, scope, o) {
            var me = this,
            e,
            oe,
            isF,
        ce;
            if (ISOBJECT(eventName)) {
                o = eventName;
                for (e in o) {
                    oe = o[e];
                    if (!me.filterOptRe.test(e)) {
                        me.addListener(e, oe.fn || oe, oe.scope || o.scope, oe.fn ? oe : o);
                    }
                }
            } else {
                eventName = eventName.toLowerCase();
                ce = me.events[eventName] || TRUE;
                if (UCML.isBoolean(ce)) {
                    me.events[eventName] = ce = new UCMLUTIL.Event(me, eventName);
                }
                ce.addListener(fn, scope, ISOBJECT(o) ? o : {});
            }
        },

        removeListener: function (eventName, fn, scope) {
            var ce = this.events[eventName.toLowerCase()];
            if (ISOBJECT(ce)) {
                ce.removeListener(fn, scope);
            }
        },

        purgeListeners: function () {
            var events = this.events,
            evt,
            key;
            for (key in events) {
                evt = events[key];
                if (ISOBJECT(evt)) {
                    evt.clearListeners();
                }
            }
        },

        addEvents: function (o) {
            var me = this;
            me.events = me.events || {};
            if (UCML.isString(o)) {
                var a = arguments,
                i = a.length;
                while (i--) {
                    me.events[a[i]] = me.events[a[i]] || TRUE;
                }
            } else {
                UCML.applyIf(me.events, o);
            }
        },

        hasListener: function (eventName) {
            var e = this.events[eventName];
            return ISOBJECT(e) && e.listeners.length > 0;
        },

        suspendEvents: function (queueSuspended) {
            this.eventsSuspended = TRUE;
            if (queueSuspended && !this.eventQueue) {
                this.eventQueue = [];
            }
        },

        resumeEvents: function () {
            var me = this,
            queued = me.eventQueue || [];
            me.eventsSuspended = FALSE;
            delete me.eventQueue;
            EACH(queued, function (e) {
                me.fireEvent.apply(me, e);
            });
        }
    };

    var OBSERVABLE = UCMLUTIL.Observable.prototype;

    OBSERVABLE.on = OBSERVABLE.addListener;

    OBSERVABLE.un = OBSERVABLE.removeListener;

    UCMLUTIL.Observable.releaseCapture = function (o) {
        o.fireEvent = OBSERVABLE.fireEvent;
    };

    function createTargeted(h, o, scope) {
        return function () {
            if (o.target == arguments[0]) {
                h.apply(scope, TOARRAY(arguments));
            }
        };
    };

    function createBuffered(h, o, fn, scope) {
        fn.task = new UCMLUTIL.DelayedTask();
        return function () {
            fn.task.delay(o.buffer, h, scope, TOARRAY(arguments));
        };
    }

    function createSingle(h, e, fn, scope) {
        return function () {
            e.removeListener(fn, scope);
            return h.apply(scope, arguments);
        };
    }

    function createDelayed(h, o, fn, scope) {
        return function () {
            var task = new UCMLUTIL.DelayedTask();
            if (!fn.tasks) {
                fn.tasks = [];
            }
            fn.tasks.push(task);
            task.delay(o.delay || 10, h, scope, TOARRAY(arguments));
        };
    };

    UCMLUTIL.Event = function (obj, name) {
        this.name = name;
        this.obj = obj;
        this.listeners = [];
    };

    UCMLUTIL.Event.prototype = {
        addListener: function (fn, scope, options) {
            var me = this,
            l;
            scope = scope || me.obj;
            if (!me.isListening(fn, scope)) {
                l = me.createListener(fn, scope, options);
                if (me.firing) { // if we are currently firing this event, don't disturb the listener loop
                    me.listeners = me.listeners.slice(0);
                }
                me.listeners.push(l);
            }
        },

        createListener: function (fn, scope, o) {
            o = o || {}, scope = scope || this.obj;
            var l = {
                fn: fn,
                scope: scope,
                options: o
            }, h = fn;
            if (o.target) {
                h = createTargeted(h, o, scope);
            }
            if (o.delay) {
                h = createDelayed(h, o, fn, scope);
            }
            if (o.single) {
                h = createSingle(h, this, fn, scope);
            }
            if (o.buffer) {
                h = createBuffered(h, o, fn, scope);
            }
            l.fireFn = h;
            return l;
        },

        findListener: function (fn, scope) {
            var list = this.listeners,
            i = list.length,
            l,
            s;
            while (i--) {
                l = list[i];
                if (l) {
                    s = l.scope;
                    if (l.fn == fn && (s == scope || s == this.obj)) {
                        return i;
                    }
                }
            }
            return -1;
        },

        isListening: function (fn, scope) {
            return this.findListener(fn, scope) != -1;
        },

        removeListener: function (fn, scope) {
            var index,
            l,
            k,
            me = this,
            ret = FALSE;
            if ((index = me.findListener(fn, scope)) != -1) {
                if (me.firing) {
                    me.listeners = me.listeners.slice(0);
                }
                l = me.listeners[index].fn;
                // Cancel buffered tasks
                if (l.task) {
                    l.task.cancel();
                    delete l.task;
                }
                // Cancel delayed tasks
                k = l.tasks && l.tasks.length;
                if (k) {
                    while (k--) {
                        l.tasks[k].cancel();
                    }
                    delete l.tasks;
                }
                me.listeners.splice(index, 1);
                ret = TRUE;
            }
            return ret;
        },
        clearListeners: function () {
            var me = this,
            l = me.listeners,
            i = l.length;
            while (i--) {
                me.removeListener(l[i].fn, l[i].scope);
            }
        },

        fire: function () {
            var me = this,
            args = TOARRAY(arguments),
            listeners = me.listeners,
            len = listeners.length,
            i = 0,
            l;

            if (len > 0) {
                me.firing = TRUE;
                for (; i < len; i++) {
                    l = listeners[i];
                    if (l && l.fireFn.apply(l.scope || me.obj || window, args) === FALSE) {
                        return (me.firing = FALSE);
                    }
                }
            }
            me.firing = FALSE;
            return TRUE;
        }
    };
})();



/*---------------------------------------自定义事件补充-------------------------------------------*/
UCML.apply(UCML.util.Observable.prototype, function () {
    function getMethodEvent(method) {
        var e = (this.methodEvents = this.methodEvents ||
        {})[method], returnValue, v, cancel, obj = this;

        if (!e) {
            this.methodEvents[method] = e = {};
            e.originalFn = this[method];
            e.methodName = method;
            e.before = [];
            e.after = [];

            var makeCall = function (fn, scope, args) {
                if (!UCML.isEmpty(v = fn.apply(scope || obj, args))) {
                    if (UCML.isObject(v)) {
                        returnValue = !UCML.isEmpty(v.returnValue) ? v.returnValue : v;
                        cancel = !!v.cancel;
                    }
                    else
                        if (v === false) {
                            cancel = true;
                        }
                        else {
                            returnValue = v;
                        }
                }
            };

            this[method] = function () {
                var args = UCML.toArray(arguments);
                returnValue = v = undefined;
                cancel = false;

                UCML.each(e.before, function (b) {
                    makeCall(b.fn, b.scope, args);
                    if (cancel) {
                        return returnValue;
                    }
                });

                if (!UCML.isEmpty(v = e.originalFn.apply(obj, args))) {
                    returnValue = v;
                }
                UCML.each(e.after, function (a) {
                    makeCall(a.fn, a.scope, args);
                    if (cancel) {
                        return returnValue;
                    }
                });
                return returnValue;
            };
        }
        return e;
    }

    return {
        beforeMethod: function (method, fn, scope) {
            getMethodEvent.call(this, method).before.push({
                fn: fn,
                scope: scope
            });
        },
        afterMethod: function (method, fn, scope) {
            getMethodEvent.call(this, method).after.push({
                fn: fn,
                scope: scope
            });
        },

        removeMethodListener: function (method, fn, scope) {
            var e = getMethodEvent.call(this, method), found = false;
            UCML.each(e.before, function (b, i, arr) {
                if (b.fn == fn && b.scope == scope) {
                    arr.splice(i, 1);
                    found = true;
                    return false;
                }
            });
            if (!found) {
                UCML.each(e.after, function (a, i, arr) {
                    if (a.fn == fn && a.scope == scope) {
                        arr.splice(i, 1);
                        return false;
                    }
                });
            }
        },
        relayEvents: function (o, events) {
            var me = this;
            function createHandler(ename) {
                return function () {
                    return me.fireEvent.apply(me, [ename].concat(UCML.toArray(arguments)));
                };
            }
            UCML.each(events, function (ename) {
                me.events[ename] = me.events[ename] || true;
                o.on(ename, createHandler(ename), me);
            });
        },
        enableBubble: function (events) {
            var me = this;
            if (!UCML.isEmpty(events)) {
                events = UCML.isArray(events) ? events : UCML.toArray(arguments);
                UCML.each(events, function (ename) {
                    ename = ename.toLowerCase();
                    var ce = me.events[ename] || true;
                    if (UCML.isBoolean(ce)) {
                        ce = new UCML.util.Event(me, ename);
                        me.events[ename] = ce;
                    }
                    ce.bubble = true;
                });
            }
        }
    };
} ());



UCML.util.Observable.capture = function (o, fn, scope) {
    o.fireEvent = o.fireEvent.createInterceptor(fn, scope);
};


UCML.util.Observable.observeClass = function (c, listeners) {
    if (c) {
        if (!c.fireEvent) {
            UCML.apply(c, new UCML.util.Observable());
            UCML.util.Observable.capture(c.prototype, c.fireEvent, c);
        }
        if (UCML.isObject(listeners)) {
            c.on(listeners);
        }
        return c;
    }
};

/*----------------------------------------------------------事件管理类-----------------------------------------------------*/
UCML.EventManager = function () {
    function listen(el, eventName, fn, scope, data) {
        function fnToScope(e) {
            e.data = data;
            fn.call(scope || el, el, e);
        }
        return fnToScope;
    }
    var pub = {
        addListener: function (el, eventName, fn, scope, data) {
            data = data || scope;
            if (!el.jquery) {
                el = UCML.getDom(el);
            }
            $(el).bind(eventName, data, listen(el, eventName, fn, scope, data));
        },
        removeListener: function (el, eventName, fn) {
            if (!el.jquery) {
                el = UCML.getDom(el);
            }
            if (!UCML.isEmpty(eventName) && !UCML.isEmpty(fn)) {
                $(el).unbind(eventName, listen(el, eventName, fn));
            }
            else if (!UCML.isEmpty(eventName)) {
                $(el).unbind(eventName);
            }
            else {
                $(el).unbind();
            }
        }
    }
    pub.on = pub.addListener;

    pub.un = pub.removeListener;
    return pub;
} ();

UCML.on = UCML.EventManager.on;

UCML.un = UCML.EventManager.un;



UCML.util.ClickRepeater = UCML.extend(UCML.util.Observable, {

    constructor: function (el, config) {
        //     this.el = Ext.get(el);
        //     this.el.unselectable();

        this.el = el;

        UCML.apply(this, config);

        this.addEvents(
        "mousedown",
        "click",
        "mouseup"
        );

        if (!this.disabled) {
            this.disabled = true;
            this.enable();
        }

        // allow inline handler
        if (this.handler) {
            this.on("click", this.handler, this.scope || this);
        }

        UCML.util.ClickRepeater.superclass.constructor.call(this);
    },

    interval: 20,
    delay: 250,
    preventDefault: true,
    stopDefault: false,
    timer: 0,

    enable: function () {
        if (this.disabled) {
            //  this.el.on('mousedown', this.handleMouseDown, this);
            UCML.on(this.el, 'mousedown', this.handleMouseDown, this);
            if (UCML.isIE) {
                //       this.el.on('dblclick', this.handleDblClick, this);
                UCML.on(this.el, 'dblclick', this.handleDblClick, this);
            }

            if (this.preventDefault || this.stopDefault) {

                this.on('click', this.eventOptions, this);
                //       UCML.on(this.el, 'click', this.eventOptions, this);
            }
        }
        this.disabled = false;
    },

    /**
    * Disables the repeater and stops events from firing.
    */
    disable: function (/* private */force) {
        if (force || !this.disabled) {
            clearTimeout(this.timer);
            if (this.pressClass) {
                this.el.removeClass(this.pressClass);
            }
            //    Ext.getDoc().un('mouseup', this.handleMouseUp, this);
            UCMl.un(document, "mouseup", this.handleMouseUp);
            //  this.el.removeAllListeners();
        }
        this.disabled = true;
    },

    /**
    * Convenience function for setting disabled/enabled by boolean.
    * @param {Boolean} disabled
    */
    setDisabled: function (disabled) {
        this[disabled ? 'disable' : 'enable']();
    },

    eventOptions: function (el, e) {
        if (this.preventDefault) {
            //      e.preventDefault();
        }
        if (this.stopDefault) {
            //     e.stopEvent();
            e.stopPropagation();
        }
    },

    // private
    destroy: function () {
        this.disable(true);
        //      Ext.destroy(this.el);
        this.purgeListeners();
    },

    handleDblClick: function (el, e) {
        clearTimeout(this.timer);
        this.el.blur();

        this.fireEvent("mousedown", el, e);
        this.fireEvent("click", el, e);
    },

    // private
    handleMouseDown: function (el, e) {

        clearTimeout(this.timer);
        this.el.blur();
        //    if (this.pressClass) {
        //       this.el.addClass(this.pressClass);
        //    }
        this.mousedownTime = new Date();
        UCML.on(document, "mouseup", this.handleMouseUp, this);
        //     Ext.getDoc().on("mouseup", this.handleMouseUp, this);
        //  this.el.on("mouseout", this.handleMouseOut, this);
        UCML.on(this.el, 'mouseout', this.handleMouseOut, this);
        this.fireEvent("mousedown", el, e);
        this.fireEvent("click", el, e);

        // Do not honor delay or interval if acceleration wanted.
        if (this.accelerate) {
            this.delay = 400;
        }
        this.timer = this.click.defer(this.delay || this.interval, this, [e]);
    },

    // private
    click: function (e) {
        this.fireEvent("click", this, e);
        this.timer = this.click.defer(this.accelerate ?
            this.easeOutExpo(this.mousedownTime.getElapsed(),
                400,
                -390,
                12000) :
            this.interval, this, [e]);
    },

    easeOutExpo: function (t, b, c, d) {
        return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
    },

    // private
    handleMouseOut: function () {
        clearTimeout(this.timer);
        if (this.pressClass) {
            this.el.removeClass(this.pressClass);
        }
        UCML.on(this.el, 'mouseover', this.handleMouseReturn, this);
        //   this.el.on("mouseover", this.handleMouseReturn, this);
    },

    // private
    handleMouseReturn: function () {
        UCML.un(this.el, 'mouseover', this.handleMouseReturn);
        //    this.el.un("mouseover", this.handleMouseReturn, this);
        //  if (this.pressClass) {
        this.el.addClass(this.pressClass);
        //  }
        //  this.click();
    },

    // private
    handleMouseUp: function (e) {
        clearTimeout(this.timer);
        UCML.un(this.el, 'mouseover', this.handleMouseReturn);
        UCML.un(this.el, 'mouseout', this.handleMouseOut);
        //     this.el.un("mouseover", this.handleMouseReturn, this);
        //    this.el.un("mouseout", this.handleMouseOut, this);
        //    Ext.getDoc().un("mouseup", this.handleMouseUp, this);
        UCML.un(document, "mouseup", this.handleMouseUp);
        //     this.el.removeClass(this.pressClass);
        this.fireEvent("mouseup", this, e);
    }
});

UCML.Template = function (html) {
    var me = this,
        a = arguments,
        buf = [],
        v;

    if (UCML.isArray(html)) {
        html = html.join("");
    } else if (a.length > 1) {
        for (var i = 0, len = a.length; i < len; i++) {
            v = a[i];
            if (typeof v == 'object') {
                UCML.apply(me, v);
            } else {
                buf.push(v);
            }
        };
        html = buf.join('');
    }

    /**@private*/
    me.html = html;
    if (me.compiled) {
        me.compile();
    }
};
UCML.Template.prototype = {
    re: /\{([\w-]+)\}/g,
    applyTemplate: function (values) {
        var me = this;

        return me.compiled ?
                me.compiled(values) :
                me.html.replace(me.re, function (m, name) {
                    return values[name] !== undefined ? values[name] : "";
                });
    },
    set: function (html, compile) {
        var me = this;
        me.html = html;
        me.compiled = null;
        return compile ? me.compile() : me;
    },
    compile: function () {
        var me = this,
            sep = UCML.isGecko ? "+" : ",";

        function fn(m, name) {
            name = "values['" + name + "']";
            return "'" + sep + '(' + name + " == undefined ? '' : " + name + ')' + sep + "'";
        }

        eval("this.compiled = function(values){ return " + (UCML.isGecko ? "'" : "['") +
             me.html.replace(/\\/g, '\\\\').replace(/(\r\n|\n)/g, '\\n').replace(/'/g, "\\'").replace(this.re, fn) +
             (UCML.isGecko ? "';};" : "'].join('');};"));
        return me;
    },
    insertFirst: function (el, values, returnElement) {
        return this.doInsert('afterBegin', el, values, returnElement);
    },
    insertBefore: function (el, values, returnElement) {
        return this.doInsert('beforeBegin', el, values, returnElement);
    },
    insertAfter: function (el, values, returnElement) {
        return this.doInsert('afterEnd', el, values, returnElement);
    },
    append: function (el, values, returnElement) {
        return this.doInsert('beforeEnd', el, values, returnElement);
    },

    doInsert: function (where, el, values, returnEl) {
        el = UCML.getDom(el);
        var newNode = UCML.DomHelper.insertHtml(where, el, this.applyTemplate(values));
        return returnEl ? UCML.get(newNode, true) : newNode;
    },
    overwrite: function (el, values, returnElement) {
        el = UCML.getDom(el);
        el.innerHTML = this.applyTemplate(values);
        return returnElement ? UCML.get(el.firstChild, true) : el.firstChild;
    }
};

UCML.Template.prototype.apply = UCML.Template.prototype.applyTemplate;

UCML.Template.from = function (el, config) {
    el = UCML.getDom(el);
    return new UCML.Template(el.value || el.innerHTML, config || '');
};




UCML.apply(UCML.Template.prototype, {
    disableFormats: false,
    re: /\{([\w-]+)(?:\:([\w\.]*)(?:\((.*?)?\))?)?\}/g,
    argsRe: /^\s*['"](.*)["']\s*$/,
    compileARe: /\\/g,
    compileBRe: /(\r\n|\n)/g,
    compileCRe: /'/g,
    applyTemplate: function (values) {
        var me = this,
            useF = me.disableFormats !== true,
            fm = UCML.util.Format,
            tpl = me;

        if (me.compiled) {
            return me.compiled(values);
        }
        function fn(m, name, format, args) {
            if (format && useF) {
                if (format.substr(0, 5) == "this.") {
                    return tpl.call(format.substr(5), values[name], values);
                } else {
                    if (args) {
                        // quoted values are required for strings in compiled templates,
                        // but for non compiled we need to strip them
                        // quoted reversed for jsmin
                        var re = me.argsRe;
                        args = args.split(',');
                        for (var i = 0, len = args.length; i < len; i++) {
                            args[i] = args[i].replace(re, "$1");
                        }
                        args = [values[name]].concat(args);
                    } else {
                        args = [values[name]];
                    }
                    return fm[format].apply(fm, args);
                }
            } else {
                return values[name] !== undefined ? values[name] : "";
            }
        }
        return me.html.replace(me.re, fn);
    },
    compile: function () {
        var me = this,
            fm = UCML.util.Format,
            useF = me.disableFormats !== true,
            sep = UCML.isGecko ? "+" : ",",
            body;

        function fn(m, name, format, args) {
            if (format && useF) {
                args = args ? ',' + args : "";
                if (format.substr(0, 5) != "this.") {
                    format = "fm." + format + '(';
                } else {
                    format = 'this.call("' + format.substr(5) + '", ';
                    args = ", values";
                }
            } else {
                args = ''; format = "(values['" + name + "'] == undefined ? '' : ";
            }
            return "'" + sep + format + "values['" + name + "']" + args + ")" + sep + "'";
        }

        // branched to use + in gecko and [].join() in others
        if (UCML.isGecko) {
            body = "this.compiled = function(values){ return '" +
                   me.html.replace(me.compileARe, '\\\\').replace(me.compileBRe, '\\n').replace(me.compileCRe, "\\'").replace(me.re, fn) +
                    "';};";
        } else {
            body = ["this.compiled = function(values){ return ['"];
            body.push(me.html.replace(me.compileARe, '\\\\').replace(me.compileBRe, '\\n').replace(me.compileCRe, "\\'").replace(me.re, fn));
            body.push("'].join('');};");
            body = body.join('');
        }
        eval(body);
        return me;
    },

    // private function used to call members
    call: function (fnName, value, allValues) {
        return this[fnName](value, allValues);
    }
});
UCML.Template.prototype.apply = UCML.Template.prototype.applyTemplate;



UCML.util.Format = function () {
    var trimRe = /^\s+|\s+$/g,
        stripTagsRE = /<\/?[^>]+>/gi,
        stripScriptsRe = /(?:<script.*?>)((\n|\r|.)*?)(?:<\/script>)/ig,
        nl2brRe = /\r?\n/g;

    return {
        ellipsis: function (value, len, word) {
            if (value && value.length > len) {
                if (word) {
                    var vs = value.substr(0, len - 2),
                        index = Math.max(vs.lastIndexOf(' '), vs.lastIndexOf('.'), vs.lastIndexOf('!'), vs.lastIndexOf('?'));
                    if (index == -1 || index < (len - 15)) {
                        return value.substr(0, len - 3) + "...";
                    } else {
                        return vs.substr(0, index) + "...";
                    }
                } else {
                    return value.substr(0, len - 3) + "...";
                }
            }
            return value;
        },
        undef: function (value) {
            return value !== undefined ? value : "";
        },
        defaultValue: function (value, defaultValue) {
            return value !== undefined && value !== '' ? value : defaultValue;
        },
        htmlEncode: function (value) {
            return !value ? value : String(value).replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
        },
        htmlDecode: function (value) {
            return !value ? value : String(value).replace(/&gt;/g, ">").replace(/&lt;/g, "<").replace(/&quot;/g, '"').replace(/&amp;/g, "&");
        },
        trim: function (value) {
            return String(value).replace(trimRe, "");
        },
        substr: function (value, start, length) {
            return String(value).substr(start, length);
        },
        lowercase: function (value) {
            return String(value).toLowerCase();
        },
        uppercase: function (value) {
            return String(value).toUpperCase();
        },
        capitalize: function (value) {
            return !value ? value : value.charAt(0).toUpperCase() + value.substr(1).toLowerCase();
        },

        // private
        call: function (value, fn) {
            if (arguments.length > 2) {
                var args = Array.prototype.slice.call(arguments, 2);
                args.unshift(value);
                return eval(fn).apply(window, args);
            } else {
                return eval(fn).call(window, value);
            }
        },
        usMoney: function (v) {
            v = (Math.round((v - 0) * 100)) / 100;
            v = (v == Math.floor(v)) ? v + ".00" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
            v = String(v);
            var ps = v.split('.'),
                whole = ps[0],
                sub = ps[1] ? '.' + ps[1] : '.00',
                r = /(\d+)(\d{3})/;
            while (r.test(whole)) {
                whole = whole.replace(r, '$1' + ',' + '$2');
            }
            v = whole + sub;
            if (v.charAt(0) == '-') {
                return '-$' + v.substr(1);
            }
            return "$" + v;
        },
        date: function (v, format) {
            if (!v) {
                return "";
            }
            if (!UCML.isDate(v)) {
                v = new Date(Date.parse(v));
            }
            return v.dateFormat(format || "m/d/Y");
        },
        dateRenderer: function (format) {
            return function (v) {
                return UCML.util.Format.date(v, format);
            };
        },
        stripTags: function (v) {
            return !v ? v : String(v).replace(stripTagsRE, "");
        },
        stripScripts: function (v) {
            return !v ? v : String(v).replace(stripScriptsRe, "");
        },
        fileSize: function (size) {
            if (size < 1024) {
                return size + " bytes";
            } else if (size < 1048576) {
                return (Math.round(((size * 10) / 1024)) / 10) + " KB";
            } else {
                return (Math.round(((size * 10) / 1048576)) / 10) + " MB";
            }
        },
        math: function () {
            var fns = {};

            return function (v, a) {
                if (!fns[a]) {
                    fns[a] = new Function('v', 'return v ' + a + ';');
                }
                return fns[a](v);
            };
        } (),
        round: function (value, precision) {
            var result = Number(value);
            if (typeof precision == 'number') {
                precision = Math.pow(10, precision);
                result = Math.round(value * precision) / precision;
            }
            return result;
        },
        number: function (v, format) {
            if (!format) {
                return v;
            }
            v = UCML.num(v, NaN);
            if (isNaN(v)) {
                return '';
            }
            var comma = ',',
                dec = '.',
                i18n = false,
                neg = v < 0;

            v = Math.abs(v);
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n = true;
                comma = '.';
                dec = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {
                v = v.toFixed(psplit[1].length);
            } else if (2 < psplit.length) {
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {
                v = v.toFixed(0);
            }

            var fnum = v.toString();

            psplit = fnum.split('.');

            if (hasComma) {
                var cnum = psplit[0],
                    parr = [],
                    j = cnum.length,
                    m = Math.floor(j / 3),
                    n = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }

                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
        },
        numberRenderer: function (format) {
            return function (v) {
                return UCML.util.Format.number(v, format);
            };
        },
        plural: function (v, s, p) {
            return v + ' ' + (v == 1 ? s : (p ? p : s + 's'));
        },
        nl2br: function (v) {
            return UCML.isEmpty(v) ? '' : v.replace(nl2brRe, '<br/>');
        }
    };
} ();

function isTouchDevice() {

    try {

        document.createEvent("TouchEvent");

        return true;

    } catch (e) {

        return false;

    }

}

function touchScroll(id) {

    if (isTouchDevice()) { //if touch events exist...
        alert("dd");
        var el = document.getElementById(id);

        var scrollStartPos = 0;



        document.getElementById(id).addEventListener("touchstart", function (event) {

            scrollStartPos = this.scrollTop + event.touches[0].pageY;
            alert(scrollStartPos);
            event.preventDefault();

        }, false);



        document.getElementById(id).addEventListener("touchmove", function (event) {

            this.scrollTop = scrollStartPos - event.touches[0].pageY;

            event.preventDefault();

        }, false);

    }

}