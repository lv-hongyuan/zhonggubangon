﻿UCML.ValidateCode = function (id) {
    this.imgsrc = getServicePath() + "OtherSource/getValidateCode.ashx";
    UCML.ValidateCode.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ValidateCode, UCML.Applet, {
    onRender: function () {
        UCML.ValidateCode.superclass.onRender.call(this);
        var input = $('<input type="text" id="code" class="u-input-custom">');
        var img = $('<img alt="看不清？点击更换" height="31px" title="看不清？点击更换" app="' + this.imgsrc + '?t=' + (new Date().getTime()) + '">');
        var tip = $('<a href="javascript:void(0);">看不清?</a>');
        this.el.append(input);
        this.el.append("&nbsp;&nbsp;");
        this.el.append(img);
        this.el.append("&nbsp;&nbsp;");
        this.el.append(tip);

        //input.css({ "height": "27px", "border": "1px solid #ddd", "border-radius": ".3125em" });
        img.css({ "height": "30px", "vertical-align": "middle" });
        tip.css({ "vertical-align": "baseline" });

        this.img = img;
        var opts = this;
        img.click(function () {
            opts.ChangeValidateCode();
        });

        tip.click(function () {
            opts.ChangeValidateCode();
        });
    }
});

UCML.ValidateCode.prototype.ChangeValidateCode = function () {
    var url = this.imgsrc + "?t=" + (new Date().getTime());
    this.img.attr("src", url);
}