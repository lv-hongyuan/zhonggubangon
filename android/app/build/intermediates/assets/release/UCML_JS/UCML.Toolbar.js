﻿(function () {

    /*
    * 工具栏 Toolbar
    * 设计思路:toolbar作为一个工具栏，就是一个操作集合的容器，里面可以包含各种HTML标记元素。
    * 外围是一个大DIV，DIV里面包含一个填满的table，这个table 只有一行，但是可以多列，每一个列里面装在了toolbar上的子节           * 点元素如按钮、文本框、下拉框等等。需要提供的API主要是进行这个table的单元操作，就是怎么把子元素装载到toolbar里面，
    * 并且可以获得toolbar装载的元素，可以进行元素属性变更或移除.首先要实现一个toolbar子类元素对象item和相关装载元素的方法
    * 装载的类型有dom原型元素，element节点元素,Field JS构建控件，这样可以使得toolbar多样化，更加丰富的UI实现效果。
    */
    UCML.ToolBar = function (id) {
        this.items = new Array();
        UCML.ToolBar.superclass.constructor.call(this, id);

    }

    UCML.extend(UCML.ToolBar, UCML.Container, {
        ctype: "UCML.ToolBar",
        disable: function () {// 禁用所有元素
            for (var i = 0; i < this.items.length; i++) {
                if (this.items[i].disable) {
                    this.items[i].disable();
                }
            }
        },
        enable: function () {  // 启用所有元素
            for (var i = 0; i < this.items.length; i++) {
                if (this.items[i].enable) {
                    this.items[i].enable();
                }
            }
        }
    });

    UCML.ToolBar.prototype.init = function () {
        UCML.ToolBar.superclass.init.call(this);

    }

    UCML.ToolBar.prototype.onRender = function () {
        this.el.append("<table cellspacing=\"0\" ><tr></tr></table>");
        this.el.addClass("toolbar");

        this.tr = this.el.find("tr");

        var items = this.el.find("[ctype]");


        for (var i = 0; i < items.length; i++) {
            var opt = UCML.create(items[i], $(items[i]).attr("ctype"));
            if (opt) {
                this.addItem(opt);
            }
        }
    }


    // 下一个块
    UCML.ToolBar.prototype.nextBlock = function (item) {


        var td = document.createElement("td");


        // if (this.Wrap && this.el.width() <= this.tr.width()) {
        if (this.Wrap) {
            this.tr.parent().parent().hide();
            var table = $("<div style='float:left'><table cellspacing=\"0\" ><tr><td></td></tr></table></div>");
            this.el.append(table);
            td = table.find("td").last();

            td.append(item.el);

        }
        else {
            td = $(td);
            //    alert(item.el.width());
            this.tr.append(td);
            td.append(item.el);
        }

        item.td = td;
        return td;
    }

    UCML.ToolBar.prototype.addItem = function (item) {
        item.render(this.nextBlock(item));
        this.items.add(item);
        return item;
    }

    UCML.ToolBar.prototype.getItem = function (index) {
        return this.items[index];
    }

    UCML.ToolBar.prototype.addFillCtl = function (config) {
        return this.addItem(new T.FillCtl(config));
    }

    UCML.ToolBar.prototype.addDropDownList = function (config) {
        return this.addItem(new T.DropDownList(config));
    }

    UCML.ToolBar.prototype.addTextBox = function (config) {
        return this.addItem(new T.TextBox(config));
    }

    //增加图片按钮，2014-10-20   
    UCML.ToolBar.prototype.addImageButton = function (config) {
        return this.addItem(new T.ImageButton(config));
    }

    /*
    
    var d = this.addDateBox();
    d.on("setValue", function (v) { alert(v); })
    d.getValue()//获取值 按钮中可以调用
    */
    UCML.ToolBar.prototype.addDateBox = function (config) {
        var opts = new T.DateBox(config);
        var el = opts.combo;
        el.width("150");
        this.addElement(el);
        return opts;
    }

    UCML.ToolBar.prototype.addSeparator = function (config) {
        return this.addItem(new T.Separator(config));
    }

    // 添加空白符
    UCML.ToolBar.prototype.addSpacer = function (config) {
        return this.addItem(new T.Spacer(config));
    }

    // 空白填充
    UCML.ToolBar.prototype.addFill = function (config) {
        return this.addItem(new T.Fill(config));
    }

    // 空白填充
    UCML.ToolBar.prototype.addLabel = function (config) {
        return this.addItem(new T.Label(config));
    }

    // 添加element元素
    UCML.ToolBar.prototype.addElement = function (el) {
        return this.nextBlock({ el: $(el) });
    }

    UCML.ToolBar.prototype.addButton = function (config) {
        return this.addItem(new T.Button(config));
    }

    UCML.ToolBar.prototype.addCheckButton = function (config) {
        return this.addItem(new T.CheckButton(config));
    }

    UCML.ToolBar.prototype.addSplitButton = function (config) {
        return this.addItem(new T.SplitButton(config));
    }

    // 添加文本
    UCML.ToolBar.prototype.addText = function (config) {
        return this.addItem(new T.TextItem(text, id));
    }

    //控件填充,将指定位置的替代为ctl
    UCML.ToolBar.prototype.FillSpaceCtl = function (index, ctl) {
        this.items[index].td.html("").append(ctl.el);
    }

    var T = UCML.ToolBar;


    UCML.Toolbar = UCML.ToolBar;

    //注册toolbar对象

    UCML.reg('UCML.ToolBar', UCML.ToolBar);

    //构建toolbar的Item对象
    T.Item = function (config) {
        this.config = config;
        T.Item.superclass.constructor.call(this, config);
    }

    UCML.extend(T.Item, UCML.Commponent, {
        ctype: "UCML.ToolBar.Item",
        getIndex: function () {
            return this.td[0].cellIndex;
        }, render: function (td) {
            //  T.Separator.superclass.render.call(this, td);
        }
    });


    T.Item.prototype.init = function () {
        //   if (!this.DefaultEvent) {
        //       this.DefaultEvent = 'click';
        //   }

        //   this.on(this.DefaultEvent, this.onClick, this);

    }

    //初始化Item原型
    T.Item.prototype.onClick = function (el, e) {
        e.preventDefault();
        if (this.handler) {
            this.handler.call(this.scope || this, el, e);
        }
    }


    //显示元素
    T.Item.prototype.show = function () {
        this.hidden = false;
        //this.td.style.display = "";
        this.td.show();
    }

    //隐藏元素
    T.Item.prototype.hide = function () {
        this.hidden = true;
        this.td.hide();

        //this.td.style.display = "none";

    }

    // 切换是否可见
    T.Item.prototype.setVisible = function (visible) {
        if (visible) {
            this.show();
        } else {
            this.hide();
        }
    }


    // 禁用并且设置启用样式
    T.Item.prototype.disable = function () {
        this.td.addClass("item-disabled");
        this.disabled = true;

    }

    // 启用并且设置禁用样式
    T.Item.prototype.enable = function () {
        this.td.removeClass("item-disabled");
        this.disabled = false;

    }

    // 注册Item对象
    UCML.reg('UCML.ToolBar.Item', T.Item);
    T.Button = UCML.extend(UCML.Linkbutton, {
        getIndex: function () {
            return this.td[0].cellIndex;
        },
        hide: function () {
            this.hidden = true;
            this.td.hide();
        },
        show: function () {
            this.hidden = false;
            this.td.show();
        },
        setVisible: function (visible) {
            if (visible) {
                this.show();
            } else {
                this.hide();
            }
        }
    });

    T.ImageButton = UCML.extend(UCML.ImageButton, {
        getIndex: function () {
            return this.td[0].cellIndex;
        },
        hide: function () {
            this.hidden = true;
            this.td.hide();
        },
        show: function () {
            this.hidden = false;
            this.td.show();
        },
        setVisible: function (visible) {
            if (visible) {
                this.show();
            } else {
                this.hide();
            }
        }
    });

    T.CheckButton = UCML.extend(UCML.CheckButton, {
        getIndex: function () {
            return this.td[0].cellIndex;
        },
        hide: function () {
            this.hidden = true;
            this.td.hide();
        },
        show: function () {
            this.hidden = false;
            this.td.show();
        },
        setVisible: function (visible) {
            if (visible) {
                this.show();
            } else {
                this.hide();
            }
        }
    });

    T.SplitButton = UCML.extend(UCML.Splitbutton, {
        getIndex: function () {
            return this.td[0].cellIndex;
        }
    });

    T.DropDownList = UCML.extend(UCML.DropDownList, {
        getIndex: function () {
            return this.td[0].cellIndex;
        }
    });

    T.TextBox = UCML.extend(UCML.TextBox, {
        getIndex: function () {
            return this.td[0].cellIndex;
        }
    });

    T.DateBox = UCML.extend(UCML.DateBox, {
        getIndex: function () {
            return this.td[0].cellIndex;
        }
    });

    UCML.reg('UCML.ToolBar.Button', T.Button);
    //UCML.reg('UCML.ToolBar.ImageButton', T.ImageButton);
    UCML.reg('UCML.ToolBar.SplitButton', T.SplitButton);

    UCML.reg('UCML.ToolBar.DropDownList', T.DropDownList);

    UCML.reg('UCML.ToolBar.TextBox', T.TextBox);
    UCML.reg('UCML.ToolBar.TextBox', T.DateBox);

    // 构建分隔符
    T.Separator = function () {
        T.Separator.superclass.constructor.call(this);

    };

    // 继承T.Item
    UCML.extend(T.Separator, T.Item, {
        enable: UCML.emptyFn,
        disable: UCML.emptyFn,
        focus: UCML.emptyFn,
        autoEl: 'span',
        render: function (td) {
            T.Separator.superclass.render.call(this, td);
            td.html("<span class='separator'></span>");
        }
    });

    // 注册Separator对象
    UCML.reg('UCML.ToolBar.Separator', T.Separator);


    // 构建空白符
    T.Spacer = function () {
        T.Spacer.superclass.constructor.call(this);
    };
    // 继承Item
    UCML.extend(T.Spacer, T.Item, {
        enable: UCML.emptyFn,
        disable: UCML.emptyFn,
        focus: UCML.emptyFn,
        render: function (td) {
            T.Spacer.superclass.render.call(this, td);
            td.addClass("spacer");
        }
    });

    // 注册Spacer对象
    UCML.reg('UCML.ToolBar.Spacer', T.Spacer);

    // 构建空白填充
    T.Fill = UCML.extend(T.Spacer, {
        // private
        render: function (td) {
            T.Fill.superclass.render.call(this, td);
            td.css("width", "100%");
        }
    });
    // 注册Fill对象
    UCML.reg('UCML.ToolBar.Fill', T.Fill);

    // 构建分隔符
    T.Label = function (config) {
        if (UCML.isString(config)) {
            this.text = config;
        }
        else {
            this.text = config.text || "";
        }
        T.Label.superclass.constructor.call(this, config);

    };

    // 继承T.Item
    UCML.extend(T.Label, T.Item, {
        enable: UCML.emptyFn,
        disable: UCML.emptyFn,
        focus: UCML.emptyFn,
        autoEl: 'span',
        render: function (td) {

            T.Label.superclass.render.call(this, td);

            this.el = $("<span class='label'>" + this.text + "</span>");
            td.html(this.el);
        },
        /*
        *设置label值
        */
        setText: function (text) {
            this.el.html(text);
        },
        /*
        *获取label值
        */
        getText: function () {
            return this.el.html();
        }
    });

    // 注册Separator对象
    UCML.reg('UCML.ToolBar.Label', T.Label);


    UCML.Pagination = function (id) {
        this.dataTable = null;
        UCML.Pagination.superclass.constructor.call(this, id);
    }


    UCML.extend(UCML.Pagination, UCML.ToolBar, {
        ctype: "UCML.Pagination",
        pageIndex: 1,
        total: 0,
        pageSize: 10,
        pageNumber: 0,
        pageCount: 0,
        displayMsg: null,
        bindEvents: function () {
            UCML.Pagination.superclass.bindEvents.call(this);

            this.dataTable.on("onLoad", this.updateMsg, this);
            this.dataTable.on("OnAfterInsert", this.updateMsg, this);
            this.dataTable.on("OnAfterDelete", this.updateMsg, this);


        }
    });

    UCML.Pagination.prototype.init = function () {
        UCML.Pagination.superclass.init.call(this);
    }

    UCML.Pagination.prototype.onRender = function () {
        UCML.Pagination.superclass.onRender.call(this);

        this.addFill();
        this.addLabel("");

        if (this.havePage) {
            this.addButton({ text: UCML.Languge.DBLoadFirstPage, pagination: this, onClick: this.onLoadFirstPage });
            this.addButton({ text: UCML.Languge.DBLoadPrevPage, pagination: this, onClick: this.onLoadPrevPage });
            this.addButton({ text: UCML.Languge.DBLoadMore, pagination: this, onClick: this.onLoadMoreData });
            this.addButton({ text: UCML.Languge.DBLoadLastPage, pagination: this, onClick: this.onLoadLastPage });

            this.addLabel(UCML.Languge.DBTogether2);
            this.indexBox = this.addTextBox();
            this.indexBox.el.width(20)
            this.addLabel(UCML.Languge.DBTogether3);
            this.gotoPageBtn = this.addButton({ text: UCML.Languge.DBGoto, pagination: this });
            this.gotoPageBtn.on("click", function () {
                if (this.applet) {
                    if (this.indexBox.getValue() != "" && !isNaN(this.indexBox.getValue())) {
                        this.applet.onGotoPage(parseInt(this.indexBox.getValue()));
                    }
                }
            }, this);
        }
    }

    UCML.Pagination.prototype.updateMsg = function () {

        var msg = "";
        var totalRecordCount = parseInt(this.dataTable.getTotalRecordCount());
        var pageNumber = parseInt(this.dataTable.getDefaultPageCount());
        var recordCount = this.dataTable.getRecordCount();
        if (totalRecordCount == 0) {
            totalRecordCount = recordCount;
            msg = UCML.Languge.DBPool + totalRecordCount + UCML.Languge.DBNote;
        }
        else if (recordCount == 0) {
            msg = UCML.Languge.DBPool + "0" + UCML.Languge.DBNote;
        }
        else if (this.havePage && pageNumber > 0) {

            if (!this.displayMsg) {
                this.displayMsg = UCML.Languge.Pagination_DisplayMsg;
            }

            var msgTmp = new UCML.Template(this.displayMsg);
            this.total = totalRecordCount;
            this.pageNumber = pageNumber;
            if (this.pageNumber == "-1") {
                this.pageNumber = 0;
                this.pageIndex = 1;
            }
            else {
                this.pageCount = parseInt(this.total / this.pageNumber);
                if (this.total % this.pageNumber != 0) {
                    this.pageCount++;
                }
                this.pageIndex = parseInt(parseInt(this.dataTable.StartPos) / this.pageNumber) + 1;
            }

            msg = msgTmp.apply({ pageIndex: this.pageIndex, total: this.total, pageCount: this.pageCount, pageNumber: this.pageNumber, pageSize: this.pageSize });

        }
        else {
            msg = UCML.Languge.DBPool + totalRecordCount + UCML.Languge.DBNote + UCML.Languge.DBFrom + (parseInt(parseInt(this.dataTable.StartPos) + parseInt(1))) + UCML.Languge.DBBeginReadCount + this.dataTable.ReadCount + UCML.Languge.DBNote;
        }
        $(this.getItem(1).td).children("span").html(msg);

    }

    //上一页
    UCML.Pagination.prototype.onLoadPrevPage = function () {
        if (this.pagination.applet) {
            this.pagination.applet.onLoadPrevPage();
        }
    }

    //下一页
    UCML.Pagination.prototype.onLoadMoreData = function () {
        if (this.pagination.applet) {
            this.pagination.applet.onLoadMoreData();
        }
    }

    //首页
    UCML.Pagination.prototype.onLoadFirstPage = function () {
        if (this.pagination.applet) {
            this.pagination.applet.onLoadFirstPage();
        }
    }

    //末页
    UCML.Pagination.prototype.onLoadLastPage = function () {
        if (this.pagination.applet) {
            this.pagination.applet.onLoadLastPage();
        }
    }



    UCML.reg('UCML.Pagination', UCML.Pagination);




    UCML.PaginationMin = function (id) {
        UCML.PaginationMin.superclass.constructor.call(this, id);
    }

    UCML.extend(UCML.PaginationMin, UCML.Pagination, {
        ctype: "UCML.PaginationMin",
        displayMsg: null,
        onRender: function () {
            UCML.Pagination.superclass.onRender.call(this);
            this.addFill();
            this.addLabel("");
            var opts = this;
            if (this.havePage) {
                if (this.applet.showPageCount) {
                    this.addLabel("每页记录数");
                    var dpl = this.addDropDownList();

                    var currentPageCount = RecordCountInfo[this.dataTable.BCName].PageCount;
                    var indexKey = this.applet.pageCountKey || [10, 15, 20, 30, 50, 100];
                    if (indexKey.indexOf(currentPageCount) == -1) {
                        dpl.add(currentPageCount, currentPageCount, true);
                    }
                    for (var i = 0; i < indexKey.length; i++) {
                        var seleced = false;
                        if (currentPageCount == indexKey[i])
                            seleced = true;
                        dpl.add(indexKey[i], indexKey[i], seleced);
                    }

                    var dt = this.dataTable;
                    dpl.on("change", function (text, value) {
                        dt.setDefaultPageCount(value);
                        CallSetPageCount(dt.BCName, text);
                    });

                    function CallSetPageCount(BCName, pageCount) {
                        var config = { methodName: "SetPageCount", params: { BCName: BCName, pageCount: pageCount }, onSuccess: succeeded_SetPageCount, onFailure: failed_SetPageCount };
                        return this.invoke(config);
                    }

                    function succeeded_SetPageCount(obj, text, methodName) {
                        dt.Refresh();
                    }

                    function failed_SetPageCount(obj, text, methodName) {
                        alert(text);
                    }
                }

                var firstbtn = $("<div title='" + UCML.Languge.DBLoadFirstPage + "' class='firstbtn'>&#160;</div>");
                firstbtn.bind("click", function () {
                    opts.applet.onLoadFirstPage();
                });

                this.addElement(firstbtn);

                var prevbtn = $("<div title='" + UCML.Languge.DBLoadPrevPage + "' class='prevbtn'>&#160;</div>");
                prevbtn.bind("click", function () {
                    opts.applet.onLoadPrevPage();
                });

                var morebtn = $("<div title='" + UCML.Languge.DBLoadMore + "' class='morebtn'>&#160;</div>");
                morebtn.bind("click", function () {
                    opts.applet.onLoadMoreData();
                });

                var lastbtn = $("<div title='" + UCML.Languge.DBLoadLastPage + "' class='lastbtn'>&#160;</div>");
                lastbtn.bind("click", function () {
                    opts.applet.onLoadLastPage();
                });

                var refreshbtn = $("<div title='" + UCML.Languge.DBRefresh + "' class='refreshbtn'>&#160;</div>");
                refreshbtn.bind("click", function () {
                    opts.applet.dataTable.Refresh();
                });

                this.addElement(firstbtn);
                this.addElement(prevbtn);
                this.addElement(morebtn);
                this.addElement(lastbtn);
                this.addElement(refreshbtn);
            }
        }
    });

    UCML.reg('UCML.PaginationMin', UCML.PaginationMin);

    UCML.PaginationMobile = function (id) {
        UCML.PaginationMobile.superclass.constructor.call(this, id);
    }

    UCML.extend(UCML.PaginationMobile, UCML.Pagination, {
        ctype: "UCML.PaginationMobile",
        displayMsg: null,
        onRender: function () {
            UCML.Pagination.superclass.onRender.call(this);

            this.addFill();
            var opts = this;
            if (this.havePage) {
                this.addButton({ id: "Btn_firstPage",
                    isMobile: true,
                    tooltip: "",
                    toolbar: opts,
                    iconCls: "ui-icon-carat-l",
                    onClick: function (el, e) {
                        opts.applet.onLoadFirstPage();
                    }
                });
                this.addButton({ id: "Btn_prevPage",
                    tooltip: "",
                    isMobile: true,
                    toolbar: opts,
                    iconCls: "ui-icon-arrow-l",
                    onClick: function (el, e) {
                        opts.applet.onLoadPrevPage();
                    }
                });
                this.addButton({ id: "Btn_nextPage",
                    tooltip: "",
                    isMobile: true,
                    toolbar: opts,
                    iconCls: "ui-icon-arrow-r",
                    onClick: function (el, e) {
                        opts.applet.onLoadMoreData();
                    }
                });
                this.addButton({ id: "Btn_lastPage",
                    tooltip: "",
                    isMobile: true,
                    toolbar: opts,
                    iconCls: "ui-icon-carat-r",
                    onClick: function (el, e) {
                        opts.applet.onLoadLastPage();
                    }
                });
            }
        }
    });

    UCML.reg('UCML.PaginationMobile', UCML.PaginationMobile);
})();