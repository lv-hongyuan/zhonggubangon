﻿UCML.OpenShowWindow = function (id) {

    UCML.OpenShowWindow.superclass.constructor.call(this, id);
}

UCML.extend(UCML.OpenShowWindow, UCML.Window, {
    //打开窗体的URL 
    URL: null,
    // 是否能被拖动  
    draggable: false,
    //是否能改变窗体大小
    resizable: false,
    //是否有阴影，有些皮肤无此效果
    showShadow: true,
    //是否遮盖（模态） 
    modal: true,
    //是否有外框,部分皮肤不生效
    border: false,
    // 是否有关闭按钮
    closable: true,
    //窗体标题
    title: "请选择....",
    //窗体高度
    height: 600,
    // 窗体宽度
    width: 600,
    //窗体里是否有滚动条
    scroll: "no",
    left: 0,
    //打开窗体的位置
    frameMode: "self", //self 本窗口，parent 父窗口 ,frame 最外框  
    close: function () {
        UCML.OpenShowWindow.superclass.close.call(this);
        this.destroy();
    },
    init: function () {
        if (this.frameMode == "parent") {
            this.window = this.window || window.parent;
        }
        else if (this.frameMode == "frame") {
            this.window = this.window || window;
            while (this.window.parent && this.window.parent != this.window) {
                this.window = window.parent;
            }
        }
        else {
            this.window = this.window || window;
        }
        this.window["window-max-zIndex"] = this.window["window-max-zIndex"] || UCML.Window.zIndex++;
        this.zIndex = this.window["window-max-zIndex"]++;

        this.rendeTo = this.window.document.body;
        UCML.OpenShowWindow.superclass.init.call(this);
    },
    onRender: function () {

        UCML.OpenShowWindow.superclass.onRender.call(this);

        if (this.URL) {
            var content = $("<iframe app=" + this.URL + "  scrolling=" + this.scroll + "  frameborder=0></iframe>", this.window.document.body);
            this.body.append(content);

            this.frame = content;

            var w = window;
            var opts = this;
            this.frame.bind("load", function () {
                this.contentWindow.openerWindow = w; //谁弹出我的那个窗口对象（页面对象）
                this.contentWindow.currentWindow = opts; //当前弹出的外框对象  （层）
            });

        //    if (this.scroll == "yes") {
                this.frame.contents().find("body").css("overflow-x", "hidden").css("overflow-y", "auto");
      //      }
        }
    },
    setSize: function (param) {
        UCML.OpenShowWindow.superclass.setSize.call(this, param);
        if (this.frame) {
            this.frame.height(this.body.height());
            this.frame.width(this.body.width());
        }
    }
});
