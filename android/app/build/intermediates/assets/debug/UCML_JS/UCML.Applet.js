﻿
/**
* @class UCML.Applet
* @extends UCML.Commponent
* @description 视图组件基类
* @param {String} id 容器控件id
*/
UCML.Applet = function (id) {

    /**   
    * @property private theDataTable 
    * @description BC数据
    * @type UCML.theDataTable
    */
    this.dataTable = null;
    
    /**   
    * @property columns 
    * @description 视图组件列(VC)
    * @type Array
    */
    this.columns = null;



    this.isInit = false;
    //   this.on("init", function () { debugger return false});
    //   BusinessObject.addApplet(this);

    UCML.Applet.superclass.constructor.call(this, id);
    //执行父类中的构造函数

    this.addEvents("onLoadData");
}

UCML.extend(UCML.Applet, UCML.Container, {
    ctype: "UCML.Applet",
    /**   
    * @property  moduleMode 
    * @description 模块显示方式 0：默认，1：面板（可收缩），2分组框
    * @type int
    */
    moduleMode: 0,
    showLoadMask: false,
    haveScroll: false,
    afterRender: function () {
        UCML.Applet.superclass.afterRender.call(this);
        //编辑VC的toolbar
        if ((typeof BPOIsJQMPage !== "undefined") && BPOIsJQMPage) {
            var theme = BPOPageTheme || "a";
            this.toolbar.addClass("toolbar-" + theme);

            this.el.addClass("applet-jqmtheme-" + theme);
        }
        else if (this.IsJQMPage) {
            var theme = this.JQMPageTheme || "a";
            this.toolbar.addClass("toolbar-" + theme);
        }
    },
    beforeRender: function () {
        UCML.Applet.superclass.beforeRender.call(this);

        this.toolbar = $("#" + this.id + "Toolbar_Module");
        this.toolbar.addClass("toolbar");


        this.el.addClass("applet-box");


        this.module = window[this.id + "_Module"];
        var module = $("#" + this.id + "_Module");
        if (this.module && this.module.ctype) {
            this.layout = this.module;
        }
        else if (module.length > 0) { //自适应高
            module.addClass("applet-module");
            switch (this.moduleMode) {
                case 0:
                    this.module = new UCML.Container({ el: module, alignHeight: this.alignHeight, isSetProperties: false,
                        alignWidth: this.alignWidth, ratioHeight: this.ratioHeight, ratioWidth: this.ratioWidth,
                        width: this.width, height: this.height
                    });
                    break;
                case 1:
                    this.module = new UCML.Panel({ el: module, alignHeight: this.alignHeight, isSetProperties: false,
                        alignWidth: this.alignWidth, ratioHeight: this.ratioHeight, ratioWidth: this.ratioWidth,
                        width: this.width, height: this.height,
                        title: this.el.attr("title") || this.id, collapsible: true
                    });
                    break;
                case 2:
                    this.module = new UCML.GropBox({ el: module, alignHeight: this.alignHeight, isSetProperties: false,
                        alignWidth: this.alignWidth, ratioHeight: this.ratioHeight, ratioWidth: this.ratioWidth,
                        width: this.width, height: this.height
                    });
                    break;
                case 3:
                    this.module = new UCML.Panel({ el: module, alignHeight: this.alignHeight, isSetProperties: false,
                        alignWidth: this.alignWidth, ratioHeight: this.ratioHeight, ratioWidth: this.ratioWidth,
                        width: this.width, height: this.height,
                        title: this.el.attr("title") || this.id
                    });
                    break;
                default:
                    this.module = new UCML.Container({ el: module, alignHeight: this.alignHeight, isSetProperties: false,
                        alignWidth: this.alignWidth, ratioHeight: this.ratioHeight, ratioWidth: this.ratioWidth,
                        width: this.width, height: this.height
                    });
            }
            this.layout = this.module;
        }
        this.el.attr("title", "");

        this.setPerm();
    },
    setPerm: function () {
        this.ColumnPerm = new UCML.Applet.ColumnPerm(this);
        if (this.ColumnPerm) {
            for (var i = 0; i < this.ColumnPerm.length; i++) {
                var fieldName = this.ColumnPerm[i]["fieldName"];
                var allowModify = this.ColumnPerm[i]["allowModify"];
                var allowVisible = this.ColumnPerm[i]["allowVisible"];
                var defaultValue = this.ColumnPerm[i]["defaultValue"];
                var notAllowNull = this.ColumnPerm[i]["notAllowNull"];

                var objColumn = this.getColumn(fieldName);

                if (objColumn) {
                    if (isNaN(allowVisible)) {
                        objColumn.display = allowVisible == "true" ? true : false;
                    }
                    else {
                        objColumn.display = allowVisible == 1 ? true : false;
                    }

                    if (isNaN(allowModify)) {
                        objColumn.allowModify = allowModify == "true" ? true : false;
                    }
                    else {
                        objColumn.allowModify = allowModify == 1 ? true : false;
                    }
                    if (defaultValue != "" || notAllowNull == "true") {
                        var bcCol = this.dataTable.getColumn(fieldName);
                        if (defaultValue != "")
                            bcCol.defaultValue = defaultValue;
                        if (notAllowNull == "true")
                            bcCol.allowNull = false;
                    }
                }
            }
        }

        this.ButtonPerm = new UCML.Applet.ButtonPerm(this);
        this.DropDownButtonPerm = new UCML.Applet.DropDownButtonPerm(this);

        if (this.ButtonPerm) {
            for (var i = 0; i < this.ButtonPerm.length; i++) {
                var caption = this.ButtonPerm[i]["caption"];
                var enabled = this.ButtonPerm[i]["enabled"];
                var visible = this.ButtonPerm[i]["visible"];
                var resp_appletbuttonoid = this.ButtonPerm[i]["resp_appletbuttonoid"];
                var buttontype = this.ButtonPerm[i]["buttontype"];
                /*
                var button;
                if (!isNaN(caption)) {
                button = UCML.get(caption);
                }
                else {
                button = window[this.id + "Toolbar_Module"].getItem(i); //VC_DOCDataEnfi_DownToolbar_Module;
                }*/
                var button = window[this.id + "Toolbar_Module"].getItem(i);
                if (button) {
                    if (button.setDisabled && (enabled == "true" || enabled == 1)) {
                        button.enable();
                    }
                    else if (button.setDisabled && enabled == "false" || enabled == 0) {
                        button.disable();
                    }

                    if (visible == "false" || visible == 0) {
                        button.hide();
                    }
                    else if (visible == "true" || visible == 1) {
                        button.show();
                    }
                    //下拉菜单
                    if ((enabled == "true" || enabled == 1) && (visible == "true" || visible == 1)) {
                        if (buttontype == "9" || buttontype == 9) {
                            if (this.DropDownButtonPerm) {

                                for (var j = 0; j < this.DropDownButtonPerm.length; j++) {
                                    var caption = this.DropDownButtonPerm[j]["caption"];
                                    var enabled = this.DropDownButtonPerm[j]["enabled"];
                                    var visible = this.DropDownButtonPerm[j]["visible"];
                                    var resp_appletbutton_fk = this.DropDownButtonPerm[j]["resp_appletbutton_fk"];
                                    var buttonid = this.DropDownButtonPerm[j]["buttonid"];
                                    if (resp_appletbuttonoid == resp_appletbutton_fk) {
                                        var item = button.menu.getMenuItemById(buttonid);
                                        if (item) {
                                            if ((enabled == "true" || enabled == 1)) {
                                                item.el[0].disabled = false;
                                            }
                                            else if (enabled == "false" || enabled == 0) {
                                                item.el[0].disabled = true;
                                                item.el.unbind("click");
                                            }

                                            if (visible == "false" || visible == 0) {
                                                item.el.hide();
                                            }
                                            else if (visible == "true" || visible == 1) {
                                                item.el.show();
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                }

            }


            this.AppletPerm = new UCML.Applet.AppletPerm(this);
            if (this.AppletPerm) {
                for (var i = 0; i < this.AppletPerm.length; i++) {
                    var caption = this.AppletPerm[i]["caption"];
                    var enabled = this.AppletPerm[i]["enabled"];
                    var visible = this.AppletPerm[i]["visible"];
                    var appletname = this.AppletPerm[i]["appletname"];
                    var button = window[this.id + "Toolbar_Module"]
                    if (appletname) {
                        if (appletname == this.id) {
                            if (enabled == "false" || enabled == 0) {
                                this.enabledEdit = false;
                                if (button) {
                                    button.disable();
                                }
                            }
                            else {
                                this.enabledEdit = true;
                            }
                        }
                        if (visible == "false" || visible == 0) {
                            if (this.moduleMode == 0) {
                                if (appletname == this.id) {
                                    this.layout.hide();
                                    var tabPanel = this.el.parents('.tabs-container');

                                    if (tabPanel && tabPanel[0]) {
                                        var tabPanelObj = eval(tabPanel[0].id);
                                        for (var i = 0; i < tabPanelObj.panels.length; i++) {
                                            var panel = tabPanelObj.panels[i];
                                            if (panel) {
                                                var tabId = "Tab_" + this.id;
                                                if (panel.config.body[0].id == tabId) {
                                                    tabPanelObj.closeTab(i);
                                                }

                                            }
                                        }
                                    }

                                }
                            }
                            else if (this.moduleMode == 1) {
                                if (appletname == this.id) {
                                    this.layout.hide();
                                }
                            }
                            else if (this.moduleMode == 2) {
                                if (appletname == this.id) {
                                    this.layout.hide();
                                }
                            }
                            else if (this.moduleMode == 3) {
                                if (appletname == this.id) {
                                    this.layout.hide();
                                }
                            }
                            else {
                                if (appletname == this.id) {
                                    this.layout.hide();
                                }
                            }
                        }

                    }
                }
            }
        }

        this.CustomButtonPerm = new UCML.Applet.CustomButtonPerm(this);
        if (this.CustomButtonPerm) {
            for (var i = 0; i < this.CustomButtonPerm.length; i++) {
                //var caption = this.CustomButtonPerm[i]["caption"];
                var enabled = this.CustomButtonPerm[i]["enabled"];
                var visible = this.CustomButtonPerm[i]["visible"];
                var buttonid = this.CustomButtonPerm[i]["buttonid"];
                if (buttonid) {
                    if (buttonid.indexOf('$') != -1) {
                        continue;
                    }
                    var obj = $('#' + buttonid);
                    if (obj) {
                        if (enabled == "false" || enabled == 0) {
                            obj.attr("disabled", true);
                        }
                        else {
                            obj.removeAttr("disabled");
                        }
                        if (visible == "false" || visible == 0) {
                            obj.hide();
                        }
                        else {
                            obj.show();
                        }
                    }
                }
            }
        }

    },
    getHasLayout: function () {
        return this.layout ? true : false;
    },
    showMenu: function (x, y) {
        var menu = this.menu;
        if (this.haveMenu && menu) {


            var dbody = getPageArea(window.document);
            var dbWidth = dbody.width;
            var dbHeight = dbody.height;
            var qmWidth = menu.el.outerWidth();

            var qmHeight = menu.el.outerHeight();
            if (x + qmWidth > dbWidth) {
                x = x - (x + qmWidth - dbWidth);
            }
            if (y + qmHeight > dbHeight) {
                y = y - (y + qmHeight - dbHeight);
            }
            x = x + $(window.document.body).scrollLeft();
            y = y + $(window.document.body).scrollTop();

            menu.show({ left: x - 4, top: y });
        }
    },
    getModuleEl: function () {
        if (this.module) {
            return this.module.el;
        }
        else {
            return this.el;
        }
    }
    ,
    showMask: function (type) {
        this.maskType = type || this.maskType;

        var text = "";
        if (this.maskType == "load") {
            text = "<div class=\"applet_loading\"></div>";
        }

        var moduleEl = this.getModuleEl();
        if (!this.mask) {
            text = text || "";
            this.mask = $('<div class="applet-mask">' + text + '</div>').appendTo(moduleEl);
            // $("<iframe app=\"about:blank\"  style=\"position:absolute; visibility:inherit; top:0px; left:0px; width:" + this.panelWidth + "; height:" + this.panelHeight + "; z-index:-1; filter='progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)';\"></iframe>");
        }


        var elPosition = moduleEl.position();
        this.mask.css({
            //				zIndex: $.fn.window.defaults.zIndex++,
            width: moduleEl.width(),
            height: moduleEl.height(),
            left: elPosition.left,
            top: elPosition.top
        });

        if (this.maskType == "load") {
            var elLoading = this.mask.find(".applet_loading");
            var elMask = this.mask;
            elLoading.css("top", (elMask.height() - elLoading.height()) / 2);
            elLoading.css("left", (elMask.width() - elLoading.width()) / 2);
        }

    }
    ,
    hideMask: function () {
        if (this.mask) {
            this.mask.remove();
            delete this.mask;
        }
    }
    ,
    bindEvents: function () {
        UCML.Applet.superclass.bindEvents.call(this);
        var opt = this;
        if (this.haveCustomMenu) {
            this.el.bind("contextmenu", function (e) {
                return false;
            });
        }
        else if (this.haveMenu) {
            this.el.bind("contextmenu", function (e) {
                opt.showMenu(e.clientX, e.clientY);
                return false;
            });
            this.onMenuReady();
            this.menu.on("menuItemClick", this.onMenuItemClick, this);
        }

        if (opt.alignHeight == true || opt.alignWidth == true || UCML.isEmpty(opt.ratioHeight) == false || UCML.isEmpty(opt.ratioWidth) == false) {
            this.getModuleEl().parent().trigger("_resize", function () {
                //     opt.setSize(null, "applet-bindEvents");
            });
        }

        if (this.dataTable && this.showLoadMask) {
            this.dataTable.on("beforeGetData", function () {
                opt.showMask("load");
            });
            this.dataTable.on("afterGetData", function () {
                opt.hideMask();
            });
        }

        UCML.Container.superclass.bindEvents.call(this);


        this.on("setsize", function () {
            if (this.mask) {
                opt.showMask();
            }
        });

    }, onMenuItemClick: function (menuId, menuItem) {
        if (menuId == "cmd_LoadMore") {
            this.onLoadMoreData();
        }
        else if (menuId == "cmd_LoadPrevPage") {
            this.onLoadPrevPage();
        }
        else if (menuId == "cmd_LoadFirstPage") {
            this.onLoadFirstPage();
        }
        else if (menuId == "cmd_LoadLastPage") {
            this.onLoadLastPage();
        }
        else if (menuId == "cmd_ConfigColumns") {
            window.open(UCMLLocalResourcePath + "UCMLCommon/SYS/BPO_AppletColumn_PersonJS.aspx?VCName=" + this.id + "&ParentBPOName=" + this.BusinessObject.BPOName, "", "location=no,menubar=no,toolbar=no,status=yes,directories=no,scrollbars=no,resizable=no,width=300,height=360");
        }
        else if (menuId == "cmd_Refresh") {
            this.dataTable.Refresh();
        } else if (menuId == "cmd_InsertRecord") {
            this.dataTable.Insert();
        }
        else if (menuId == "cmd_DeleteRecord") {
            this.dataTable.Delete();
        }
        else if (menuId == "cmd_Submit") {
            if (this.BusinessObject.el == window)//兼容弹出的VC
            {
                BusinessSubmit();
            }
            else {
                this.BusinessObject.BusinessSubmit();
            }
        }
        else if (menuId == "cmd_Cancel") {
            this.dataTable.RejectChanges();
        }
        else if (menuId == "cmd_Query") {
            this.ExecuteQuery();
        }
        else if (menuId == "cmd_QueryCancel") {
            this.clearData();
        }
        else if (menuId == "cmd_OutExcel") {
            ExportToExcelById(this.id);
        }
        else if (menuId == "cmd_AllOutExcel") {
            ExportToExcel(this.dataTable.BCName, this, "", "", "");
        }
    },
    //设置VC按钮是否启用 true启用,false为禁用
    setToolbarEnabled: function (val) {
        var vcToolbar = window["ToolBar" + this.id];
        if (!vcToolbar) {
            return vcToolbar;
        }
        if (val) {
            vcToolbar.enable();
        }
        else {
            vcToolbar.disable();
        }
    }
})

UCML.Applet.prototype.open = function () {

    if (this.BusinessObject) {
        this.BusinessObject.addApplet(this);
        this.init();
    }
}

/**
*取得匹配元素当前计算的高度值(px)
*/
UCML.Applet.prototype.getHeight = function () {
    return this.getModuleEl().height();
}

/**
*取得匹配元素当前计算的宽度值(px)
*/
UCML.Applet.prototype.getWidth = function () {
    return this.getModuleEl().width();
}

UCML.Applet.prototype.init = function () {
    UCML.Applet.superclass.init.call(this);
    this.dataTable = this.dataTable || this.BusinessObject.getDataTableByBCName(this.getAttribute("BCName"));
    if (!this.enabledEdit) {
        this.disabled = true;
    }
};

UCML.Applet.prototype.isVisible = function () {
    if (this.getModuleEl().css("display") != "" && this.getModuleEl().css("display") != "none") {
        return true;
    } else {
        return false;
    }
}


UCML.Applet.prototype.show = function () {
    this.getModuleEl().show();
}

UCML.Applet.prototype.hide = function () {
    this.getModuleEl().hide();
}


UCML.Applet.prototype.loadData = function (data) {

}

UCML.Applet.prototype.afterFieldChange = function (fieldName, value) {

}

/**   
* @method setDataTable 
* @description 设置BC对象
* @param {UCML.theDataTable} theDataTable BC对象      
*/
UCML.Applet.prototype.setDataTable = function (value) {
    this.dataTable = value;
}

/**   
* @method setColumns 
* @description 设置BC对象
* @param {UCML.theDataTable} theDataTable BC对象      
*/
UCML.Applet.prototype.setColumns = function (value) {
    this.columns = value;

    if (this.dataTable) {
        for (var i = 0; i < this.columns.length; i++) {
            var fieldName = this.columns[i].fieldName;
            var bcCol = this.dataTable.getColumn(fieldName);
            if (bcCol) {
                UCML.applyIf(this.columns[i], bcCol);
            }
        }
    }
}

/**   
* @method getColumns 
* @description 获取VC列信息      
*/
UCML.Applet.prototype.getColumns = function () {
    return this.columns;
}


UCML.Applet.prototype.setEnabledEdit = function (value) {
    this.enabledEdit = value;
}

UCML.Applet.prototype.setSize = function (param, a) {
    UCML.Applet.superclass.setSize.call(this, param, a);
    if (!isNaN(this.height)) {
        this.el.height(this.height - this.toolbar.outerHeight(true));
    }

}

/*
2015-03-09 VC_Module下有div（例如上传附件控件）时重新计算高度
*/
UCML.Applet.prototype.setSize = function (param, a) {
    UCML.Applet.superclass.setSize.call(this, param, a);
    if (!isNaN(this.height)) {
        if (this.el.prevAll().length > 0) {
            var prevHeight = 0;
            this.el.prevAll().each(function (index, el) {
                prevHeight += $(this).outerHeight(true);
            });
            this.el.height(this.height - prevHeight);
        } else {
            this.el.height(this.height);
        }
    }

}

UCML.Applet.prototype.getColumn = function (fieldName) {
    if (this.columns) {
        for (var i = 0; i < this.columns.length; i++) {
            var col = this.columns[i];
            if (col.fieldName == fieldName) {
                return col;
            }
        }
    }
    return null;
}
//VC_T_OrderList.SetCustomizeMenuPerm(VC_T_OrderList.menu);
UCML.Applet.prototype.SetCustomizeMenuPerm = function (menuObj) {
    //debugger
    this.CustomizeMenuPerm = new UCML.Applet.CustomizeMenuPerm(this);
    if (this.CustomizeMenuPerm) {
        for (var i = 0; i < menuObj.items.length; i++) {
            for (var j = 0; j < this.CustomizeMenuPerm.length; j++) {
                var caption = this.CustomizeMenuPerm[j]["caption"];
                var enabled = this.CustomizeMenuPerm[j]["enabled"];
                var visible = this.CustomizeMenuPerm[j]["visible"];
                var menuid = this.CustomizeMenuPerm[j]["menuid"];

                if (menuObj.items[i].menuId == menuid) {
                    var item = menuObj.getMenuItemById(menuid);
                    if ((enabled == "true" || enabled == 1)) {
                        item.el[0].disabled = false;
                    }
                    else if (enabled == "false" || enabled == 0) {
                        item.el[0].disabled = true;
                        item.el.unbind("click");
                    }

                    if (visible == "false" || visible == 0) {
                        item.el.hide();
                    }
                    else if (visible == "true" || visible == 1) {
                        item.el.show();
                    }
                }
            }
        }
    }
}



//VC中列权限控制
UCML.Applet.ColumnPerm = function (vc) {
    if (!window["appletColumnPerm"]) {
        return null;
    }

    var objList = window["appletColumnPerm"][vc.id] || [];
    try {
        this.data = BusinessData[vc.id + 'CloumnPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var fieldName = this.data[i][3];
                var allowModify = this.data[i][1];
                var allowVisible = this.data[i][2];
                var defaultValue = this.data[i][7];
                var notAllowNull = this.data[i][8];

                var obj = {};
                obj["fieldName"] = fieldName;
                obj["allowModify"] = allowModify;
                obj["allowVisible"] = allowVisible;
                obj["defaultValue"] = defaultValue;
                obj["notAllowNull"] = notAllowNull;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}

UCML.Applet.ColumnPerm.fields = ["RESP_AppletColumnOID", "AllowModify", "AllowVisible", "FieldName", "ChineseName", "AllowEdit", "Visible", "AppletColumn_FK", "UCML_RESPONSIBILITY_FK", "Applet_FK"];


UCML.Applet.ColumnPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.ColumnPerm.fields.indexOf(name);
}

UCML.Applet.ColumnPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}

//VC中的按钮的控制
UCML.Applet.ButtonPerm = function (vc) {
    if (!window["appletButtonPerm"]) {
        return null;
    }

    var objList = window["appletButtonPerm"][vc.id] || [];
    try {
        this.data = BusinessData[vc.id + 'ButtonPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "Enabled");
                var visible = this.getValue(i, "AllowVisible");
                var resp_appletbuttonoid = this.getValue(i,"RESP_AppletButtonOID");
                var buttontype = this.getValue(i, "ButtonType");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["resp_appletbuttonoid"] = resp_appletbuttonoid;
                obj["buttontype"] = buttontype;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}
UCML.Applet.ButtonPerm.fields = ["RESP_AppletButtonOID", "Caption", "Enabled", "AllowVisible", "ButtonType", "Applet_FK", "AppletButton_FK", "UCML_RESPONSIBILITY_FK"];
UCML.Applet.ButtonPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.ButtonPerm.fields.indexOf(name);
}

UCML.Applet.ButtonPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}

UCML.Applet.DropDownButtonPerm = function (vc) {
    if (!window["appletButtonPerm"]) {
        return null;
    }

    var objList = [];
    try {
        this.data = BusinessData[vc.id + 'DropDownButtonPerm'];
        
        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "AllowModify");
                var visible = this.getValue(i, "AllowVisible");
                var resp_appletbutton_fk = this.getValue(i, "RESP_AppletButton_FK");
                var buttonid = this.getValue(i,"ButtonId");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["buttonid"] = buttonid;
                obj["resp_appletbutton_fk"] = resp_appletbutton_fk;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}
UCML.Applet.DropDownButtonPerm.fields = ["RESP_AppletDropDownButtonOID", "AllowModify", "AllowVisible", "Caption", "ButtonId", "AppletName", "UCMLClass_FK", "RESP_AppletButton_FK", "Applet_FK", "CheckButtonColl_FK", "UCML_RESPONSIBILITY_FK"];
UCML.Applet.DropDownButtonPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.DropDownButtonPerm.fields.indexOf(name);
}
UCML.Applet.DropDownButtonPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}



UCML.Applet.CustomizeMenuPerm = function (vc) {
    if (!window["appletButtonPerm"]) {
        return null;
    }

    var objList = [];
    try {
        this.data = BusinessData[vc.id + 'CustomizeMenuPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "AllowModify");
                var visible = this.getValue(i, "AllowVisible");
                var menuid = this.getValue(i, "MenuId");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["menuid"] = menuid;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}
UCML.Applet.CustomizeMenuPerm.fields = ["RESP_AppletCustomizeMenuOID", "AllowModify", "AllowVisible", "Caption", "MenuId", "AppletName", "UCMLClass_FK", "Applet_FK", "UCML_RESPONSIBILITY_FK", "NodeTypeDefine_FK", "NodeTypeMenuItemsOID"];
UCML.Applet.CustomizeMenuPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.CustomizeMenuPerm.fields.indexOf(name);
}
UCML.Applet.CustomizeMenuPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}


UCML.Applet.CustomButtonPerm = function (vc) {

    var objList = [];
    try {
        this.data = BusinessData[vc.id + 'CustomButtonPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "AllowModify");
                var visible = this.getValue(i, "AllowVisible");
                var buttonid = this.getValue(i, "ButtonID");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["buttonid"] = buttonid;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}
UCML.Applet.CustomButtonPerm.fields = ["RESP_AppletCustomButtonOID", "AllowModify", "AllowVisible", "Caption", "ButtonType", "AppletName", "ButtonID", "UCMLClass_FK", "Applet_FK", "UCML_RESPONSIBILITY_FK", "AppletButton_FK"];
UCML.Applet.CustomButtonPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.CustomButtonPerm.fields.indexOf(name);
}
UCML.Applet.CustomButtonPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}


UCML.Applet.AppletPerm = function (vc) {

    var objList = [];
    try {
        this.data = BusinessData[vc.dataTable.BPOName + 'AppletPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "AllowModify");
                var visible = this.getValue(i, "AllowVisible");
                var appletname = this.getValue(i, "AppletName");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["appletname"] = appletname;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}
UCML.Applet.AppletPerm.fields = ["RESP_AppletLinkOID", "AppletName", "Caption", "AllowModify", "AllowVisible", "ClassName"];
UCML.Applet.AppletPerm.prototype.getFieldIndex = function (name) {
    return UCML.Applet.AppletPerm.fields.indexOf(name);
}
UCML.Applet.AppletPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}