﻿

UCML.Linkbutton = function (id) {
    this.id = null;
    this.disabled = false;
    this.plain = false;
    this.text = '';
    this.iconCls = null;
    UCML.Linkbutton.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Linkbutton, UCML.Commponent, {
    autoEl: 'a',
    ctype: "UCML.Linkbutton"
});

UCML.Linkbutton.prototype.init = function () {
    UCML.Linkbutton.superclass.init.call(this);


    //   this.id = t.attr('id');
    //   this.disabled = (t.attr('disabled') ? true : undefined);


    this.el.removeAttr('disabled');
}

UCML.Linkbutton.prototype.setProperties = function () {
    UCML.Linkbutton.superclass.setProperties.call(this);
    this.plain = (this.el.attr('plain') ? this.el.attr('plain') == 'true' : this.plain);
    this.text = this.text || $.trim(this.el.html());
    this.iconCls = this.el.attr('icon') || this.iconCls;
}

UCML.Linkbutton.prototype.bindEvents = function () {
    UCML.Linkbutton.superclass.bindEvents.call(this);
    this.on("click", this.click, this);

    /*
    this.on("mousedown", function () {
     //   this.el.addClass("check-btn");
        return false;
    }, this);

    this.on("mouseup", function () {
      //  this.el.removeClass("check-btn");
        return false;
    }, this);*/
}

UCML.Linkbutton.prototype.click = function (el, e) {
    if (this.onClick) {
        if (!this.disabled) {
            this.onClick.call(this, el, e);
        }
    }
}

//判断iconcls是样式还是图标资源，true则为样式，false为资源文件
UCML.Linkbutton.prototype.IsCssOrRes = function (iconCls) {
    iconCls = iconCls.toLowerCase();
    if(iconCls.indexOf(".jpg") != -1 || iconCls.indexOf(".png") != -1 || 
        iconCls.indexOf(".gif") != -1 || iconCls.indexOf(".bmp") != -1)
    {
        return false;
    }
    return true;
}

UCML.Linkbutton.prototype.onRender = function () {
    if (this.isMobile) {
        this.el.addClass("ui-btn");
        this.el.attr("data-mini", "true");
        this.el.attr("data-inline", "true");

        if (this.iconCls) { //jqm样式图标
            this.el.addClass(this.iconCls);
            if (this.iconPos) {
                this.el.addClass("ui-btn-icon-" + this.iconPos);
            } else {
                this.el.addClass("ui-btn-icon-left");
            }
        }

        if (this.toolbar && this.toolbar.ctype == "UCML.PaginationMobile") { //分页栏的设置
            this.el.attr("data-mini", "false");

            if (this.toolbar.el.length > 0 && typeof BPOIsJQMPage != undefined) {
                var theme = BPOIsJQMPage ? BPOPageTheme : (this.toolbar.applet.IsJQMPage ? this.toolbar.applet.JQMPageTheme : "a");
                if (theme == "a") {
                    this.el.css({ "background": "#e6e6e6", "border": "none", "padding-right": "0px" });
                }
                else if (theme == "b") {
                    this.el.css({ "background": "#1d1d1d", "border": "none", "padding-right": "0px" });
                }
            }
        } else {
            if (this.text) {
                this.el.html(this.text);
            }
            else {
                this.el.attr("data-mini", "false");
                this.el.addClass("ui-btn-icon-notext");
                this.el.css({ "border-radius": " .3125em", "padding-left": "0px" });
            }
        }

        this.el.textinput();
        return;
    }


    this.el.empty();
    this.el.addClass('l-btn');
    if (this.id) {
        this.el.attr('id', this.id);
    } else {
        this.el.removeAttr('id');
    }
    if (this.plain) {
        this.el.addClass('l-btn-plain');
    } else {
        this.el.removeClass('l-btn-plain');
    }

    if (this.text) {
        this.el.html(this.text).wrapInner(
					'<span class="l-btn-left">' +
					'<span class="l-btn-text">' +
					'</span>' +
					'</span>'
			);
        if (this.iconCls) {
            if (this.IsCssOrRes(this.iconCls))
                this.el.find('.l-btn-text').addClass(this.iconCls).css('padding-left', '20px');
            else
                this.el.find('.l-btn-text').css({ 'padding-left': '20px', 'background': 'url(' + this.iconCls + ') no-repeat' });
        }
    } else {
        this.el.html('&nbsp;').wrapInner(
					'<span class="l-btn-left">' +
					'<span class="l-btn-text">' +
					'<span class="l-btn-empty"></span>' +
					'</span>' +
					'</span>'
			);
        if (this.iconCls) {
            if (this.IsCssOrRes(this.iconCls))
                this.el.find('.l-btn-empty').addClass(this.iconCls);
            else
                this.el.find('.l-btn-empty').css({ 'background': 'url(' + this.iconCls + ') no-repeat' });
        }
    }

    this.setDisabled(this.disabled);
}

UCML.Linkbutton.prototype.setDisabled = function (disabled) {
    if (disabled) {
        this.disabled = true;
        var href = this.el.attr('href');
        if (href) {
            this.href = href;
            this.attr('href', 'javascript:void(0)');
        }
        var onclick = this.el.attr('onclick');
        if (onclick) {
            this.onclick = onclick;
            this.el.attr('onclick', null);
        }
        this.el.addClass('l-btn-disabled');
    } else {
        this.disabled = false;
        if (this.href) {
            this.el.attr('href', this.href);
        }
        if (this.onclick) {
            this.dom.onclick = this.onclick;
        }
        this.el.removeClass('l-btn-disabled');
    }
}

UCML.Linkbutton.prototype.enable = function () {

    this.setDisabled(false);
}
UCML.Linkbutton.prototype.disable = function () {
    this.setDisabled(true);
}

UCML.Linkbutton.prototype.setText = function (val) {
    this.el.find(".l-btn-left .l-btn-text").html(val);
}

UCML.reg("UCML.Linkbutton", UCML.Linkbutton);


UCML.CheckButton = function (id) {
    this.checked = false;
    UCML.CheckButton.superclass.constructor.call(this, id);
}

UCML.extend(UCML.CheckButton, UCML.Linkbutton, {
    ctype: "UCML.CheckButton"
});

UCML.CheckButton.prototype.init = function () {
    UCML.CheckButton.superclass.init.call(this);
}

UCML.CheckButton.prototype.click = function () {
    if (!this.checked) {
        this.el.addClass("check-btn");
        this.checked = true;
    }
    else {
        this.checked = false;
        this.el.removeClass("check-btn");
    }
    UCML.CheckButton.superclass.click.call(this);
}

UCML.reg("UCML.CheckButton", UCML.CheckButton);