﻿UCML.Validatebox = function (id) {
    this.required = false;
    this.validType = null;
    this.missingMessage = '输入不能为空';
    this.invalidMessage = null;
    /*
    this.rules = {
        email: {
            validator: function (value) {
                return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
            },
            message: 'Please enter a valid email address.'
        },
        url: {
            validator: function (value) {
                return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
            },
            message: 'Please enter a valid URL.'
        },
        length: {
            validator: function (value, param) {
                var len = $.trim(value).length;
                return len >= param[0] && len <= param[1]
            },
            message: 'Please enter a value between {0} and {1}.'
        }
    };*/
    UCML.Spinner.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Validatebox, UCML.Commponent, { ctype: "UCML.Validatebox"

});

UCML.Validatebox.prototype.init = function () {
    UCML.Validatebox.superclass.init.call(this);
    this.el.addClass('validatebox-text');

    this.required = (this.el.attr('required') ? (this.el.attr('required') == 'true' || this.el.attr('required') == true) : this.required);
    this.validType = (this.el.attr('validType') || this.validType);
    this.missingMessage = (this.el.attr('missingMessage') || this.missingMessage);
    this.invalidMessage = (this.el.attr('invalidMessage') || this.invalidMessage);
}

UCML.Validatebox.prototype.destroyBox = function () {
    var tip = this.tip;
    if (tip) {
        tip.remove();
    }
    //  this.el.remove();
}

UCML.Validatebox.prototype.bindEvents = function () {
    var box = this.el;
    var tip = this.tip;
    var opts = this;

    var time = null;
    box.unbind('.validatebox').bind('focus.validatebox', function () {
        if (time) {
            clearInterval(time);
        }
        time = setInterval(function () {
            opts.validate();
        }, 200);
    }).bind('blur.validatebox', function () {
        clearInterval(time);
        time = null;
        //    opts.hideTip();
    }).bind('mouseover.validatebox', function () {
        if (box.hasClass('validatebox-invalid')) {
            opts.showTip();
        }
    }).bind('mouseout.validatebox', function () {
        opts.hideTip();
    });
}

UCML.Validatebox.prototype.showTip = function () {
    var box = this.el;
    var msg = this.message;
    var tip = this.tip;
    if (!tip) {
        tip = $(
				'<div class="validatebox-tip">' +
					'<span class="validatebox-tip-content">' +
					'</span>' +
					'<span class="validatebox-tip-pointer">' +
					'</span>' +
				'</div>'
			).appendTo('body');
        this.tip = tip;
    }
    tip.find('.validatebox-tip-content').html(msg);


    tip.width(box.outerWidth());
    var boxWidthl = box.offset().left + box.outerWidth();

    if (boxWidthl > 0) {
        tip.css({
            display: 'block',
            left: boxWidthl,
            top: box.offset().top
        });

        var width = tip.find(".validatebox-tip-content").outerWidth(true) + tip.find(".validatebox-tip-pointer").outerWidth(true);

        if (boxWidthl + width >= $(window.document.body).outerWidth(true)) {
            tip.addClass("validatebox-tip-left");
            tip.css({
                display: 'block',
                left: box.offset().left,
                top: box.offset().top + box.outerHeight(true)
            });
        }
    }
}

/**
* hide tip message.
*/
UCML.Validatebox.prototype.hideTip = function () {
    var tip = this.tip;
    if (tip) {
        tip.remove();
        this.tip = null;
    }
}

UCML.Validatebox.prototype.setTipMessage = function (msg) {
    this.message = msg;
}

/**
* do validate action
*/
UCML.Validatebox.prototype.validate = function () {
    var opts = this;
    var tip = this.tip;
    var box = this.el;
    var value = box.val();


    // if the box is disabled, skip validate action.
    var disabled = box.attr('disabled');
    if (disabled == true || disabled == 'true') {
        return true;
    }

    if (opts.required) {
        if (value == '') {
            box.addClass('validatebox-invalid');
            this.setTipMessage(opts.missingMessage);
            this.showTip();
            return false;
        }
    }
    if (opts.validType) {
        var result = /([a-zA-Z_]+)(.*)/.exec(opts.validType);
        var rule = opts.rules[result[1]];
        if (value && rule) {
            var param = eval(result[2]);
            if (!rule['validator'](value, param)) {
                box.addClass('validatebox-invalid');

                var message = rule['message'];
                if (param) {
                    for (var i = 0; i < param.length; i++) {
                        message = message.replace(new RegExp("\\{" + i + "\\}", "g"), param[i]);
                    }
                }
                this.setTipMessage(opts.invalidMessage || message);
                this.showTip();
                return false;
            }
        }
    }

    box.removeClass('validatebox-invalid');
    this.hideTip();
    return true;
}

UCML.Validatebox.prototype.destroy = function () {
    this.destroyBox();
}
UCML.Validatebox.prototype.isValid = function () {
    return this.validate();
}