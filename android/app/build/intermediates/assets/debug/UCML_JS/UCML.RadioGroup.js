﻿
/**
* @class UCML.RadioGroup
* @extends UCML.InputList
* @description 多选按钮列表控件
* @param {String} id 容器控件id
*/
UCML.RadioGroup = function (id) {
    this.columns = 1;
    this.addEvents("blur");
    UCML.RadioGroup.superclass.constructor.call(this, id);
    //显示列数

}

UCML.extend(UCML.RadioGroup, UCML.InputList, {
    ctype: "UCML.RadioGroup",
    onRender: function () {
        $('<table border="0" cellspacing="0" cellpadding="0"><tbody></tbody></table>').appendTo(this.el);
    }, setProperties: function () {
        UCML.RadioGroup.superclass.setProperties.call(this);
        this.columns = this.getAttribute("columns") || this.columns;
    }
});

UCML.RadioGroup.prototype.init = function () {
    UCML.RadioGroup.superclass.init.call(this);
};

UCML.RadioGroup.prototype.renderedValue = function (val) {
    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.dataBind();
    }
    this.setValue(val);
    return this.text;
}

/**   
* @method setValue 
* @description 设置控件值      
*/
UCML.RadioGroup.prototype.setValue = function (value, trigger) {
    //value = value || "";
    if (value == undefined) {
        value = "";
    }

    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    this.value = value;
    this.text = "";
    var opt = this;

    this.el.find(":radio").each(function () {
        this.checked = false;
        $(this).attr("checked", false);
    });

    this.el.find(":radio[value='" + value + "']").each(function () {
        this.checked = true;
        var el = $(this);
        opt.text = el.attr("text");
        el.attr("checked", this.checked);
    });

}
/**   
* @method change 
* @description 数据发生变化时     
*/
UCML.RadioGroup.prototype.change = function (val, text) {
    this.value = val;
    this.text = text;
    this.setValue(this.value);
}

/**   
* @method setColumns 
* @description 设置显示列数      
*/
UCML.RadioGroup.prototype.setColumns = function (columns) {
    this.columns = columns;
}

/**   
* @method add 
* @description 添加一项    
*/
UCML.RadioGroup.prototype.add = function (text, value, checked) {
    text = text || "";
    if (value) {
        var table = this.el.children("table");
        var length = table.find("td").length;
        var cid = this.id + "RadioList_" + length;

        if (length % this.columns == 0) {
            $('<tr></tr>').appendTo($('tbody', table));
        }

        var td = $('<td></td>');
        var input = $('<input id="' + cid + '" name="' + this.id + '" type="radio" value="' + value + '"   text="' + text + '" ' + (this.disabled ? "disabled" : "") + '  ' + (checked ? "checked=true" : "") + '>');
        var label = $('<label for="' + cid + '">' + text + '</label>');
        input.appendTo(td);
        label.appendTo(td);
        td.appendTo($(table[0].rows[table[0].rows.length - 1]));

        var opt = this;
        input.bind("change", function (e) {

            var el = $(this);
            var text = el.attr("text");
            el.attr("checked", this.checked);
            opt.change(this.value, text);
            opt.fireEvent("valuechange", this.value, text);
           
            opt.fireEvent("blur");
        });
    }
}

/**   
* @method dataBind 
* @description 绑定控件   
*/
UCML.RadioGroup.prototype.dataBind = function () {
    UCML.RadioGroup.superclass.dataBind.call(this);
    this.el.children("table").children("tbody").empty();
    var recordCount = this.srcDataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        this.add(this.srcDataTable.getFieldValue(this.dataTextField), this.srcDataTable.getFieldValue(this.dataValueField));
    }
    this.srcDataTable.SetIndexNoEvent(0);
}

/**   
* @method getValue 
* @description 返回控件值      
* @return {Object} 
*/
UCML.RadioGroup.prototype.getValue = function () {
    return this.value;
}

UCML.RadioGroup.prototype.getText = function () {
    this.text = "";
    var opt = this;

    this.el.find(":radio:checked").each(function () {
        opt.text = opt.text + $(this).attr("text");
    });
    return this.text;
}

UCML.RadioGroup.prototype.bindCodeTable = function () {
    UCML.RadioGroup.superclass.bindCodeTable.call(this);
    this.el.children("table").children("tbody").empty();
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    if (codeList && codeList.length > 0) {
        for (var i = 0; i < codeList.length; i++) {
            this.add(codeList[i].caption, codeList[i].value);
        }
    }
}

UCML.RadioGroup.prototype.bindCustomData = function (texts, values) {
    UCML.RadioGroup.superclass.bindCustomData.call(this);
    this.el.children("table").children("tbody").empty();
    var textList = texts.split(";");
    var valueList = values.split(";");
    if (textList && valueList && valueList.length > 0) {
        for (var i = 0; i < valueList.length; i++) {
            this.add(textList[i], valueList[i]);
        }
    }
}

/**   
* @description 启用控件      
*/
UCML.RadioGroup.prototype.enable = function (ch) {
    this.setEnabledEdit(true);
}

/**   
* @description 禁用控件      
*/
UCML.RadioGroup.prototype.disable = function (ch) {
    this.setEnabledEdit(false);
}


UCML.RadioGroup.prototype.setEnabledEdit = function (value) {
    this.disabled = value ? false : true;
    if (this.disabled) {
        this.el.find("input").attr("disabled", "true");
    }
    else {
        this.el.find("input").removeAttr("disabled");
    }
}


UCML.reg("UCML.RadioGroup", UCML.RadioGroup);