﻿/*
*UCML.MobileFileUpload.js
*文件上传控件
*Date:2013/07/16
*/

UCML.MobileFileUpload = function (id) {
    //是否允许删除已上传的文件
    this.allowDelete = true;

    //注册事件
    this.addEvents("beforeUpload", "success", "error");

    UCML.MobileFileUpload.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MobileFileUpload, UCML.Applet, {
    setProperties: function () {

        this.fileField = this.getAttribute("fileField") || this.fileField || "";
        this.filePathField = this.getAttribute("filePathField") || this.filePathField || "";
        this.fileNameField = this.getAttribute("fileNameField") || this.fileNameField || "";
        this.fileTypeField = this.getAttribute("fileTypeField") || this.fileTypeField || "";
        this.fileFKField = this.getAttribute("fileFKField") || this.fileFKField || "";

        this.uploadPath = this.getAttribute("uploadPath") || this.uploadPath || "";
        this.uploadAction = this.getAttribute("uploadAction") || this.uploadAction || "Upload2File";

        this.dataTable = eval(this.getAttribute("BCBase")) || this.dataTable;
        this.masterDataTable = eval(this.getAttribute("MasterBCBase")) || this.masterDataTable;

        this.tableName = this.getAttribute("tableName") || this.dataTable.TableName || "";
        this.allowDownload = this.getAttribute("AllowDownload") ? (this.getAttribute("AllowDownload") == "True") : this.allowDownload;

        if (this.dataTable && this.dataTable.EnabledDelete == false) {
            this.allowDelete = false;
        }
    },

    onRender: function () {
        //	this.el.addClass("fj_upload");
        //this.innerHTML = '<div class="fj_upload"><a href="#" class="fj_upload_a"><span class="fj_icon"></span>上传附件</a></div><div class="upload_process_bar"><div class="upload_current_process"></div></div><div class="process_info"></div>';
        this.innerHTML = '<div class="fj_upload"><a href="#" class="fj_upload_a fj_select_lib">来自相册</a><a href="#" class="fj_upload_a fj_select_camera">来自相机</a></div><div class="upload_process_bar"><div class="upload_current_process"></div></div><div class="process_info"></div>';
        this.el.append(this.innerHTML);

        if (this.dataTable && this.dataTable.EnabledAppend == false) {
            this.hideUploadButton();
        }
        opts = this;
        this.bindUploadEvents();
    },
    bindEvents: function () {
        UCML.Applet.superclass.bindEvents.call(this);
        this.dataTable.on("onLoad", this.loadData, this);
     //   this.dataTable.on("OnRecordChange", this.recordChange, this);
    }
});

//显示上传按钮
UCML.MobileFileUpload.prototype.showUploadButton = function () {
    this.el.find(".fj_upload_a").show();
}
//隐藏上传按钮
UCML.MobileFileUpload.prototype.hideUploadButton = function () {
    this.el.find(".fj_upload_a").hide();
}

//获取文件的下载地址
UCML.MobileFileUpload.prototype.getDownloadUrl = function (isOpen) {
    if (!this.allowDownload) {
        return "javascript:void(0)";
    }

    if (this.uploadAction == "Upload2File") {
        return getServicePath() + "UCMLCommon/Sys/UCMLFileDownload.aspx?type=0&TableName=" + this.tableName + "&FilePathField=" + this.filePathField + "&FileNameField=" + this.fileNameField + "&OID=" + this.dataTable.getOID() + (isOpen ? "&ac=open" : "");
    }
    else {
        return getServicePath() + "UCMLCommon/Sys/UCMLFileDownload.aspx?type=1&TableName=" + this.tableName + "&FileField=" + this.fileField + "&FileNameField=" + this.fileNameField + "&OID=" + this.dataTable.getOID() + (isOpen ? "&ac=open" : "");
    }
}

//同步提交数据
UCML.MobileFileUpload.prototype.syncSubmit = function () {
	//BusinessSubmit(true,false);
    if (BusinessObject.BeforeSubmit === undefined || BusinessObject.BeforeSubmit() === true) {
        if (BusinessObject.CanSubmit() == false) return true;
        //var changeXML = opts.dataTable.getChangeXML();
        var changeXML = BusinessObject.getChangeXML();//主表如果存在变化数据也进行提交
        if (opts.dataTable.ChangeCount() > 0) {
            ShowMask();
            ShowMessage("正在提交信息，请等待......");
            var config = { methodName: "BusinessSubmit", params: { DeltaXml: changeXML }, async: false };

            var text = BusinessObject.invoke(config);

            opts.dataTable.MergeChange();
            HideMessage();
            hidenMask();
        }
    }
}

//获取文件名
UCML.MobileFileUpload.prototype.getFileName = function () {
    return this.dataTable.getFieldValue(this.fileNameField);
}

//刷新BC数据
UCML.MobileFileUpload.prototype.update = function () {
    this.dataTable.Refresh();
}

UCML.MobileFileUpload.prototype.getDateString = function () {
    var today = new Date();
    var year = today.getFullYear(),
        month = today.getMonth() + 1,
        day = today.getDate(),
        hour = today.getHours(),
        minute = today.getMinutes(),
        second = today.getSeconds();

    var dateFormatString = "";
    dateFormatString += (year+"").substr(2);
    dateFormatString += month < 10 ? "0" + month : month + "";
    dateFormatString += day < 10 ? "0" + day : day + "";
    dateFormatString += hour < 10 ? "0" + hour : hour + "";
    dateFormatString += minute < 10 ? "0" + minute : minute + "";
    dateFormatString += second < 10 ? "0" + second : second + "";
    return dateFormatString;
}

UCML.MobileFileUpload.prototype.renderUploadFile = function (file) {
    //var $li = $('<div id="fj_list_' + file.oid + '" class="fj_list" title="' + file.name + '"><a href="#" href="' + file.url + '" class="fj_download"></a><a href="#" class="fj_download fj_delect"></a><h2>' + file.name + '</h2> <p>21K</p></div>');
    var li = '<div id="fj_list_' + file.oid + '" class="fj_list" title="' + file.name + '">';
    if(opts.allowDownload) {
        li += '<a href="#" class="fj_download fj_downloadfile"></a>';
    }
    if(opts.allowDelete) {
        li += '<a href="#" class="fj_download fj_delect"></a>';
    }
    li += '<h2>' + file.name + '</h2> </div>';//<p>21K</p>
    var $li = $(li);
    //var opts = this;
    if (opts.allowDelete) {
        $close = $li.find('.fj_delect');
        //删除文件按钮动作
        $close.click(function () {
            var li = $(this).parent();
            var OID = li.attr("id").substr(8);
            opts.dataTable.LocateOID(OID);
            var answer = confirm("是否确定删除文件？");
            if (answer) {
                if (opts.uploadAction == "Upload2File") {
                    var url = getServicePath() + 'UCMLCommon/Sys/UCMLFileUpload_Mobile.aspx';
                    var data = { Action: "Delete", Path: encodeURI(opts.dataTable.getFieldValue(opts.filePathField)) };
                    $.post(url, data, function (data) { }, "json");
                }
                opts.dataTable.Delete();
                opts.syncSubmit();
                li.remove();
            }
        });
    }

    if(opts.allowDownload) {
        $download = $li.find('.fj_downloadfile');
        //下载文件按钮动作
        $download.click(function () {
            var li = $(this).parent();
            var OID = li.attr("id").substr(8);
            if(OID==null || OID=="") return false;
            var url = getServicePath()+"ucml_mobile/getFile.ashx?RECORDOID="+OID;
            $.ajax({
                url: url,
                dataType: "jsonp",
                jsonpCallback:"jsonpcallback",
                success: function(data) {
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                            fileSystem.root.getDirectory("Download", {create:true}, 
                                function(fileEntry) {
                                    var filePath = fileEntry.fullPath + "/" + data.filePath.substr(data.filePath.lastIndexOf("\/")+1);
                                    var uri = encodeURI(getServicePath() + data.filePath);
                                    try {
                                        var fileTransfer = new FileTransfer();

                                        //下载
                                        fileTransfer.download(uri, filePath,
                                            function(entry){
                                                var $process_info = opts.el.find('.process_info');
                                                $process_info.html($process_info.html()+"&nbsp;&nbsp;下载成功!");

                                                //隐藏进度条
                                                window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2000);
                                            },
                                            function(error){
                                                var $process_info = opts.el.find('.process_info');
                                                $process_info.html($process_info.html()+"&nbsp;&nbsp;下载失败!");

                                                //隐藏进度条
                                                window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2000);
                                            }
                                            );

                                        //获取下载进度,下载时loaded为total的2倍?
                                        fileTransfer.onprogress = function(progressEvent) {
                                            if (progressEvent.lengthComputable) {
                                                //已经上传
                                                var loaded = progressEvent.loaded/2;
                                                //文件总长度
                                                var total = progressEvent.total;

                                                //计算百分比，用于显示进度条
                                                var percent = parseInt((loaded / total) * 100);

                                                //换算成MB
                                                loaded = (loaded / 1024 / 1024).toFixed(2);
                                                total = (total / 1024 / 1024).toFixed(2);
                                                opts.el.find('.process_info').html(loaded + 'M/' + total + 'M');
                                                opts.el.find('.upload_current_process').css({ 'width': percent + '%' });
                                            }
                                        }

                                        //显示进度条
                                        $('.upload_process_bar,.process_info').show();

                                    }catch(e) {
                                        alert("下载异常:");
                                    }
                                },
                                function(){
                                    alert("创建download目录失败");
                                });
                        },
                        function(evt) {
                            alert("加载文件系统失败");
                        }
                        );
                },
                error: function(data) {
                    alert("服务端数据异常");
                }
            });
        });
    }

    opts.el.append($li);
}

UCML.MobileFileUpload.prototype.clearProcess = function () {
    this.el.find('.upload_process_bar,.process_info').hide();
    ft.abort();
};

/**
* 上传意外终止处理。
* @param message
*/
UCML.MobileFileUpload.prototype.uploadBroken = function (message) {
    alert(message);
    this.clearProcess();
}

UCML.MobileFileUpload.prototype.uploadFile = function (fileURI) {
    //content:/media/external/images/media/5750
    
    window.resolveLocalFileSystemURI(fileURI, function(fileEntry) {
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
        options.mimeType = "multipart/form-data";
        options.chunkedMode = false;
        ft = new FileTransfer();
        
        //文件信息存储在子表
        if (opts.masterDataTable) {
            var fk_OID = opts.masterDataTable.getOID();
            opts.dataTable.Insert();
            opts.dataTable.setFieldValue(opts.fileFKField, fk_OID);
        }
        else {
            if (opts.dataTable.getRecordCount() == 0) {
                alert("表记录为空，无法上传！");
                return false;
            }
        }
        //--start  兼容系统流程办理的附件上传
        var assignTaskOID = getURLParameters("TaskID");
        
        if(assignTaskOID) {
            opts.dataTable.setFieldValue("AssignTaskOID", assignTaskOID);
        }
        //--end
        if (opts.syncSubmit() === false) return false; //取消上传

        var postParams = {
            Action: opts.uploadAction,
            TableName: opts.tableName,
            FileNameField: opts.fileNameField,
            FileTypeField: opts.fileTypeField,
            RecordID: opts.dataTable.getOID()
        }
        if (postParams.Action == "Upload2File") {
            postParams.uploadPath = opts.uploadPath;
            postParams.FilePathField = opts.filePathField;
        }
        else {
            postParams.FileField = opts.fileField;
        }
        postParams.BusinessID = BusinessObject.BPOName;
        options.params = postParams;

        var uploadUrl = encodeURI(getServicePath() + "UCMLCommon/SYS/UCMLFileUpload_Mobile.aspx");

        ft.upload(fileEntry.fullPath, uploadUrl, opts.uploadSuccess, opts.uploadFailed, options);
        
        //获取上传进度
        ft.onprogress = opts.uploadProcessing;
        //显示进度条
        $('.upload_process_bar,.process_info').show();
    }, function() {
        alert("error");
    });

}

UCML.MobileFileUpload.prototype.uploadSuccess = function (result) {
    var $process_info = opts.el.find('.process_info');
    $process_info.html($process_info.html()+"&nbsp;&nbsp;上传成功!");

    //隐藏进度条
    window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2000);

    //opts.loadData();
    //opts.dataTable.Refresh();
    var file = {};
    file.oid = opts.dataTable.getOID();
    //file.url = opts.getDownloadUrl();
    file.name = opts.dataTable.getFieldValue(opts.fileNameField);
    opts.renderUploadFile(file);
}

UCML.MobileFileUpload.prototype.uploadFailed = function (error) {
    //e.code, e.source, e.target, e.http_status, e.body
    var $process_info = opts.el.find('.process_info');
    $process_info.html($process_info.html()+"&nbsp;&nbsp;上传失败!");

    //隐藏进度条
    window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2000);

    opts.dataTable.Delete();
    opts.syncSubmit();

    
}

/**
* 上传过程回调，用于处理上传进度，如显示进度条等。
*/
UCML.MobileFileUpload.prototype.uploadProcessing = function (progressEvent) {
    if (progressEvent.lengthComputable) {
        //已经上传
        var loaded = progressEvent.loaded;
        //文件总长度
        var total = progressEvent.total;

        //计算百分比，用于显示进度条
        var percent = parseInt((loaded / total) * 100);
        //换算成MB
        loaded = (loaded / 1024 / 1024).toFixed(2);
        total = (total / 1024 / 1024).toFixed(2);
        opts.el.find('.process_info').html(loaded + 'M/' + total + 'M');
        opts.el.find('.upload_current_process').css({ 'width': percent + '%' });
    }
};


UCML.MobileFileUpload.prototype.bindUploadEvents = function () {
    //var opts = this;
    function openFileSelector() {
        var source = null;
        if($(this).hasClass("fj_select_lib")) {
            //从图库取相片
            source = navigator.camera.PictureSourceType.PHOTOLIBRARY;
        }else {
            //从相机获取
            source = navigator.camera.PictureSourceType.CAMERA;
        }

        //描述类型，取文件路径
        var destinationType = navigator.camera.DestinationType;

        //媒体类型，设置为ALLMEDIA即支持任意文件选择
        var mediaType = navigator.camera.MediaType.ALLMEDIA;
        var options = {
            quality: 50,
            destinationType: destinationType.FILE_URI,//DATA_URL----"data:image/jpeg;base64," + imageData
            sourceType : source,
            mediaType : mediaType//targetWidth:100,targetHeight:100
        };
        navigator.camera.getPicture(opts.uploadFile, opts.uploadBroken, options);
    }
    //this.el.find(".fj_upload_a").click(openFileSelector);
    this.el.find(".fj_select_lib").click(openFileSelector);
    this.el.find(".fj_select_camera").click(openFileSelector)
}

UCML.MobileFileUpload.prototype.loadData = function (e) {	
    this.el.find("div.fj_list").remove();
    if (this.masterDataTable) {//文件存储在子表
        for (var i = 0; i < this.dataTable.getRecordCount(); i++) {
            this.dataTable.SetIndexNoEvent(i);
            var file = {};
            file.oid = this.dataTable.getOID();
            file.url = this.getDownloadUrl();
            file.name = this.dataTable.getFieldValue(this.fileNameField);

            this.renderUploadFile(file);
        }
    }
    else {//文件存储在主表
        var file = {};
        file.oid = this.dataTable.getOID();
        file.url = this.getDownloadUrl();
        file.name = this.dataTable.getFieldValue(this.fileNameField);

        this.renderUploadFile(file);
    }
}

UCML.MobileFileUpload.prototype.recordChange = function () {
    if (!this.masterDataTable) {
        this.ul.children().remove();
        var file = {};
        file.oid = this.dataTable.getOID();
        file.url = this.getDownloadUrl();
        file.name = this.dataTable.getFieldValue(this.uploadConfig.fileNameField);

        this.renderUploadFile(file);
    }
}

//注册控件
UCML.reg("UCML.MobileFileUpload", UCML.MobileFileUpload);