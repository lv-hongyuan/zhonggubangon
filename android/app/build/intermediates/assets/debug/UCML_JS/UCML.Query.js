﻿UCML.Query = function (id) {
    this.controls = new Array();
    this.MTVFieldList = [];
    this.queryControl = [];
    UCML.Query.superclass.constructor.call(this, id);
};

UCML.extend(UCML.Query, UCML.Applet, {
    ctype: "UCML.Query",
    itemFirstText: "全部",
    init: function () {
        UCML.Query.superclass.init.call(this);
        this.initControl();
    },
    bindEvents: function () {
        UCML.Query.superclass.bindEvents.call(this);

        UCML.on(this.el.find("[ctype]"), "blur", this.validateSQL, this);
    }
    ,
    initControl: function () {//初始化控件
        var inputs = this.el.find("[ctype]"); //dataField 匈牙利命名方式
        for (var i = 0; i < inputs.length; i++) {
            var ctl = this.addControl(inputs[i]);
            if (ctl) {
                this.setControl(ctl);
            }
        }
        $("#" + this.id + "EditPanel").show();
    }, setControl: function (ctl) { //设置控件参数
        ctl.fieldName = ctl.getAttribute("dataFld") || ctl.fieldName || "";
        ctl.defaultM = ctl.getAttribute("defaultM") || ctl.defaultM || "";
        ctl.defaultV = ctl.getAttribute("defaultV") || ctl.defaultV || "";

        //如果设置默认查询值则按照默认查询值进行查询
        if (ctl.defaultM) {
            if (ctl.defaultM == "1") {// 常值
                ctl.setValue(ctl.defaultV);
            }
            else if (ctl.defaultM == "2" && ctl.defaultV && window[ctl.defaultV]) {//函数取值
                var val = window[ctl.defaultV].call(window, ctl, this);
                ctl.setValue(val);
            }
        }

        //根据设置的字段禁用查询条件
        if (ctl.fieldName && ctl.fieldName != "") {
            var bcCom = this.dataTable.getColumn(ctl.fieldName);

            if (bcCom && bcCom.isCodeTable) {
                if (ctl.isCodeTable == true && UCML.isEmpty(this.codeTable)) { //控件是代码表但是没设置代码表名称直接去BC上字段的代码表
                    ctl.codeTable = bcCom.codeTable;
                    ctl.bindCodeTable(); //重新绑定代码表
                }
            }

            var col = this.getColumn(ctl.fieldName);
            if (col && !col.allowModify) {
                ctl.disable();
            }
        }
    },
    addControl: function (o) {
        var el = $(UCML.getDom(o));
        var BCName = el.attr("BCName") || (this.dataTable ? this.dataTable.BCName : this.el.attr("BCName"));
        var ctype = el.attr("ctype") || "UCML.Input";
        if (ctype && BCName) {
            el.attr("BCName", BCName);
            el.attr("VCName", this.id);
			if(ctype=="UCML.BCLinkBox") {
                el.attr("controlValue", "true");
            }
            var opt = UCML.create({ el: el, BusinessObject: this.BusinessObject, isSetData: false, disabled: this.disabled, isSetProperties: true, applet: this }, ctype);
            this.controls.add(opt);
            return opt;
        }

    },
    onMenuReady: function () {
        var menu = new UCML.Menu();
        if (this.enabledEdit) {
            menu.addMenuItem("cmd_Query", "查询");
            menu.addSeparator();
            menu.addMenuItem("cmd_QueryCancel", "清空条件");
        }
        this.menu = menu;
    },
    //清除查询条件并且执行查询
    clearQuery: function (nStartPos, nRecords) {

        this.clearData();
        nStartPos = nStartPos == undefined ? 0 : nStartPos;
        nRecords = nRecords == undefined ? this.dataTable.getDefaultPageCount() : nRecords;
        this.dataTable.theSQLCondi = "";
        this.dataTable.getData(nStartPos, nRecords);
    }
    ,
    GetQuerySQL: function () {
        var SQLWhere = "";

        var j = 0;
        var DataBaseType = this.dataTable.DataBaseType == undefined ? "" : this.dataTable.DataBaseType;
        //-----------------
        for (var i = 0; i < this.columns.length; i++) {
            var objColumn = this.columns[i];
            var value = "";
            if (objColumn.InnerLinkComp == "true") {

                value = this.BuildMTVWhere(objColumn);
                if (j == 0) {
                    if (value != "") {
                        SQLWhere = SQLWhere + " " + objColumn.leftBracket + value + objColumn.rightBracket + " ";
                    }
                    else {
                        SQLWhere = SQLWhere + " " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                    }
                }
                else {
                    if (value != "") {
                        SQLWhere = SQLWhere + " " + objColumn.logicConnect + " " + objColumn.leftBracket + value + objColumn.rightBracket + " ";
                    }
                    else {
                        SQLWhere = SQLWhere + " " + objColumn.logicConnect + " " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                    }
                }
                //孙毅2009-07-02添加
                j++;
                //-----------------
                continue;
            }


            if (this.columns[i].display == false) continue;

            var columnInfo = this.columns[i];
            var bcColumn = this.dataTable.getColumn(columnInfo.fieldName);

            var value = "";
            if (columnInfo.controlID) {
                value = window[columnInfo.controlID].getValue();
            }
            else {
                value = this.findFieldValue(columnInfo.fieldName) || "";
            }

            var tableName = this.dataTable.TableName;
            var fieldName = objColumn.fieldName;
            if (bcColumn.fieldKind == 0) {
                tableName = this.dataTable.TableName;
                j++;
            }
            else if (bcColumn.fieldKind == 2) {
                fieldName = bcColumn.lookupResultField;
                tableName = "al" + bcColumn.foreignKeyField;
                j++;
            }
            else if (bcColumn.fieldKind == 10) {
                fieldName = bcColumn.lookupResultField;
                tableName = "al" + bcColumn.foreignKeyField;
                j++;
            } else if (bcColumn.fieldKind == 5 && bcColumn.QueryRefColumn) {
                fieldName = bcColumn.QueryRefColumn;
                var s = fieldName.split('.');
                if (s.length > 1) {
                    tableName = s[0];
                    fieldName = s[1];
                    j++;
                }
                else {
                    continue;
                }

            }
            else {
                continue;
            }

            if (j == 1) {
                if (value != "") {
                    if (objColumn.MutiValueCol || objColumn.mutiValueCol) {//如果是多选查询
                        SQLWhere = SQLWhere + " " + objColumn.leftBracket + getMutiValueCol(tableName + "." + fieldName, objColumn, value) + objColumn.rightBracket + " ";
                    }
                    else {
                        value = getFormatValue(value);
                        SQLWhere = SQLWhere + " " + objColumn.leftBracket + tableName + "." + fieldName + " " + objColumn.operationIndent + " " + value + objColumn.rightBracket + " ";
                    }
                }
                else {
                    SQLWhere = SQLWhere + " " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                }
            }
            else {
                if (value != "") {
                    if (objColumn.MutiValueCol || objColumn.mutiValueCol) {//如果是多选查询
                        SQLWhere = SQLWhere + " " + objColumn.logicConnect + " " + objColumn.leftBracket + getMutiValueCol(tableName + "." + fieldName, objColumn, value) + objColumn.rightBracket + " ";
                    }
                    else {
                        value = getFormatValue(value);
                        SQLWhere = SQLWhere + " " + objColumn.logicConnect + " " + objColumn.leftBracket + tableName + "." + fieldName + " " + objColumn.operationIndent + " " + value + objColumn.rightBracket + " ";
                    }
                }
                else {
                    SQLWhere = SQLWhere + " " + objColumn.logicConnect + " " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                }
            }
        }

        for (var i = 0; i < this.queryControl.length; i++) {

            var value = window[this.queryControl[i].controlId].getValue();
            var name = this.queryControl[i].name;
            var operationIndent = this.queryControl[i].operationIndent || "=";
            var logicConnect = this.queryControl[i].logicConnect || " and ";
            if (value != "") {
                if (operationIndent == "like") {
                    var likeMode = this.queryControl[i].likeMode == undefined ? 0 : this.queryControl[i].likeMode; //模糊查询匹配模式 ，0为完全匹配，1为向左匹配，2为向右匹配

                    if (likeMode == 1) {
                        if (j > 0) {
                            SQLWhere = SQLWhere + " " + logicConnect + " " + name + " " + operationIndent + "  '%" + value + "'";
                        }
                        else {
                            SQLWhere = SQLWhere + " " + name + " " + operationIndent + "  '%" + value + "'";
                        }
                    }
                    else if (likeMode == 2) {
                        if (j > 0) {
                            SQLWhere = SQLWhere + " " + logicConnect + " " + name + " " + operationIndent + "  '" + value + "%'";
                        }
                        else {
                            SQLWhere = SQLWhere + " " + name + " " + operationIndent + "  '" + value + "%'";
                        }
                    } else {
                        if (j > 0) {
                            SQLWhere = SQLWhere + " " + logicConnect + " " + name + " " + operationIndent + "  '%" + value + "%'";
                        }
                        else {
                            SQLWhere = SQLWhere + " " + name + " " + operationIndent + "  '%" + value + "%'";
                        }
                    }

                }
                else {
                    if (j > 0) {
                        SQLWhere = SQLWhere + " " + logicConnect + " " + name + " " + operationIndent + "  '" + value + "'";
                    }
                    else {
                        SQLWhere = SQLWhere + " " + name + " " + operationIndent + "  '" + value + "'";
                    }

                }
                j++;
            }
        }



        function getMutiValueCol(fieldName, objColumn, value) {
            value = value.replace(/\'/g, "");
            var values = value.split(";");
            var operationIndent = objColumn.operationIndent || "=";
            var logicConnect = " or ";
            var sql = "";
            if (values.length > 0) {
                sql = " ( ";
                for (var i = 0; i < values.length; i++) {

                    if (i > 0) {
                        sql += " " + logicConnect + " ";
                    }
                    sql += fieldName + " " + objColumn.operationIndent + " " + getFormatValue(values[i]);
                }
                sql += " )";

            }
            return sql;
        }

        function getFormatValue(value) {
            if (bcColumn.fieldType == 'Float' ||
				bcColumn.fieldType == 'Double' ||
				bcColumn.fieldType == 'Money' ||
				bcColumn.fieldType == 'Numeric' ||
				bcColumn.fieldType == 'Long' ||
				bcColumn.fieldType == 'Byte' ||
				bcColumn.fieldType == 'Short' ||
				bcColumn.fieldType == 'Int'
			) {
            }
            else if (DataBaseType == "ORACLE" && (bcColumn.fieldType == 'Date' || bcColumn.fieldType == 'DateTime' || bcColumn.fieldType == 'Time')) {
                if (value != "") {
                    if (bcColumn.fieldType == 'Date') {
                        value = "to_date('" + value + "','yyyy-mm-dd')";
                    } else if (bcColumn.fieldType == 'DateTime') {
                        value = "to_date('" + value + "','yyyy-mm-dd hh24:mi:ss')";
                    } else if (bcColumn.fieldType == 'Time') {
                        value = "to_date('" + value + "','hh24:mi:ss')";
                    }
                }
            }
            else {
                if (value != "") {
                    if (objColumn.operationIndent == "Like") {
                        value = "'%" + value + "%'";
                    }
                    else {
                        value = "'" + value + "'";
                    }
                }
            }
            return value;
        }

        return SQLWhere;
    }
   ,
    /**
    第一个反正是0就可以了，第二个 就是条数
    -1就是全部
    */
    ExecuteQuery: function (nStartPos, nRecords) {
        //汤龙斌 修复子BC无法查询问题
        nStartPos = nStartPos == undefined ? 0 : nStartPos;
        nRecords = nRecords == undefined ? this.dataTable.getDefaultPageCount() : nRecords;
        this.dataTable.theSQLCondi = this.GetQuerySQL();
        this.dataTable.getData(nStartPos, nRecords);
    },
    clearData: function () {
        for (var i = 0; i < this.controls.length; i++) {
            if (this.controls[i].setValue) {
                this.controls[i].setValue("");
            }
        }
    }
    ,
    addQueryControl: function (name, controlId, operationIndent, logicConnect) {
        var config = { name: name, controlId: controlId, operationIndent: operationIndent, logicConnect: logicConnect };
        this.queryControl.add(config);
    },
    addQueryControlForLike: function (name, controlId, operationIndent, logicConnect, likeMode) {
        var config = { name: name, controlId: controlId, operationIndent: operationIndent, logicConnect: logicConnect, likeMode: likeMode };
        this.queryControl.add(config);
    }
    ,
    getQueryControl: function () {
        return this.queryControl;
    },
    setQueryControl: function (value) {
        this.queryControl = value;
    },
    clearQueryControl: function () {
        this.queryControl = [];
    },
    findFieldValue: function (fieldName) {
        for (var i = 0; i < this.controls.length; i++) {
            if (this.controls[i].fieldName == fieldName) {
                return this.controls[i].getValue();
            }
        }
        return null;
    }
    ,
    AddMTVField: function (fieldName, dataTable, valueField) {
        var obj = new Object();
        obj.fieldName = fieldName;
        obj.dataTable = dataTable;
        obj.valueField = valueField;
        this.MTVFieldList[this.MTVFieldList.length] = obj;
    },
    BuildMTVWhere: function (objColumn) {
        var MTVFieldList = this.MTVFieldList;
        var SQLWhere = "";
        for (var i = 0; i < MTVFieldList.length; i++) {
            if (objColumn.fieldName == MTVFieldList[i].fieldName) {
                var tableName = this.dataTable.TableName;
                if (objColumn.fieldKind == 0) {
                    tableName = this.dataTable.TableName;
                }
                else if (objColumn.fieldKind == 2) {
                    tableName = "al" + objColumn.foreignKeyField;
                }
                else if (objColumn.fieldKind == 10) {
                    tableName = "al" + objColumn.foreignKeyField;
                }
                else {
                    break;
                }

                var dt = MTVFieldList[i].dataTable;

                for (var n = 0; n < dt.getRecordCount(); n++) {
                    dt.SetIndexNoEvent(n);
                    var value = dt.getFieldValue(MTVFieldList[i].valueField);

                    if (value == null || value == undefined) value = "";


                    if (objColumn.fieldType == 'Float' ||
						objColumn.fieldType == 'Double' ||
						objColumn.fieldType == 'Money' ||
						objColumn.fieldType == 'Numeric' ||
						objColumn.fieldType == 'Long' ||
						objColumn.fieldType == 'Byte' ||
						objColumn.fieldType == 'Short' ||
						objColumn.fieldType == 'Int'
					) {
                    }
                    else {
                        if (value != "") {
                            if (objColumn.operationIndent == "Like") {
                                value = "'%" + value + "%'";
                            }
                            else {
                                value = "'" + value + "'";
                            }
                        }
                    }

                    if (n == 0) {
                        if (value != "") {
                            SQLWhere = SQLWhere + " " + objColumn.leftBracket + tableName + "." + objColumn.fieldName + " " + objColumn.operationIndent + " " + value + objColumn.rightBracket + " ";
                        }
                        else {
                            SQLWhere = SQLWhere + " " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                        }
                    }
                    else {
                        if (value != "") {
                            SQLWhere = SQLWhere + " OR " + objColumn.leftBracket + tableName + "." + objColumn.fieldName + " " + objColumn.operationIndent + " " + value + objColumn.rightBracket + " ";
                        }
                        else {
                            SQLWhere = SQLWhere + " OR " + objColumn.leftBracket + "1=1" + objColumn.rightBracket + " ";
                        }
                    }

                }
                break;
            }
        }
        if (SQLWhere != "")
            return "(" + SQLWhere + ")";
        else return "";
    }
});

UCML.Query.prototype.validateSQL = function (el, e)
{
    this.fireEvent("blur", e);
    var target = UCML.isElement(e) ? e : e.target;
    var $target = $(target);

    var reg = /select|update|delete|exec|count|'|"|=|>|<|%/i;
    if(reg.test($target.val())) {
        alert("参数非法、请勿输入特殊字符和SQL关键字！");
        $target.focus();
    }
}