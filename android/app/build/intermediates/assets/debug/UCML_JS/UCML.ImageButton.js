/**
* @class UCML.ImageButton
* @extend UCML.InputList
* @description 图片按钮
* @param {string} 控件id
*/
UCML.ImageButton = function (id) {
	this.disabled = false;
	UCML.ImageButton.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ImageButton, UCML.InputList, {
	ctype: "UCML.ImageButton",
	autoEl: "img",
	bindEvents: function() {
		this.on("click", this.click, this);
		this.on("mousemove", this.addBgk, this);
		this.on("mouseout", this.removeBgk, this);
	}
});

UCML.ImageButton.prototype.init = function() {
	UCML.ImageButton.superclass.init.call(this);
}

UCML.ImageButton.prototype.setProperties = function() {
	UCML.ImageButton.superclass.setProperties.call(this);
	this.src = this.src || this.el.attr("src");
	this.alt = this.alt || this.el.attr("alt");
	this.width = this.width || this.el.attr("width");
	this.height = this.height || this.el.attr("height");
	this.text = this.text || this.el.attr("title");
}

UCML.ImageButton.prototype.onRender = function() {
	UCML.ImageButton.superclass.onRender.call(this);

	this.el.attr("src", this.src);
	this.el.attr("alt", this.alt);
	this.el.attr("width", this.width);
	this.el.attr("height", this.height);
	this.el.attr("title", this.text);

	this.el.css({"cursor":"pointer"});
}

UCML.ImageButton.prototype.click = function (el, e) {
    if (!this.disabled) {
        this.onClick.call(this, el, e);
    }
}

UCML.ImageButton.prototype.addBgk = function() {
	//this.el.css({"border":"1px solid #eee"});
	if(this.src2 != ""){
		this.el.attr("src", this.src2);
	}
}

UCML.ImageButton.prototype.removeBgk = function() {
	//this.el.css({"border":"0px"});
	this.el.attr("src", this.src);
}

UCML.ImageButton.prototype.setDisabled = function (disabled) {
    if (disabled) {
        this.disabled = true;
        var href = this.el.attr('href');
        if (href) {
            this.href = href;
            this.attr('href', 'javascript:void(0)');
        }
        var onclick = this.el.attr('onclick');
        if (onclick) {
            this.onclick = onclick;
            this.el.attr('onclick', null);
        }
		this.el.css({"cursor":"default"});
    } else {
        this.disabled = false;
        if (this.href) {
            this.el.attr('href', this.href);
        }
        if (this.onclick) {
            this.dom.onclick = this.onclick;
        }
		this.el.css({"cursor":"pointer"});
    }
}

UCML.ImageButton.prototype.enable = function () {
    this.setDisabled(false);
}
UCML.ImageButton.prototype.disable = function () {
    this.setDisabled(true);
}

UCML.reg("UCML.ImageButton", UCML.ImageButton);