﻿(function () {
    function ruleCheck(val, dataTable, columnName) {
        var value = val;
        var objColumn = dataTable.getColumnByName(columnName);
        if (typeof (ACColumnInputRule) != "undefined") {
            for (var m = 0; m < ACColumnInputRule.length; m++) {
                if (objColumn.fieldName == ACColumnInputRule[m].FieldName && dataTable.BCName == ACColumnInputRule[m].BCName) {
                    var inputParams = new Array();

                    inputParams[inputParams.length] = value;
                    if (ACColumnInputRule[m].ExParam1 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = ACColumnInputRule[m].ExParam1;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (ACColumnInputRule[m].ExParam2 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = ACColumnInputRule[m].ExParam2;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (ACColumnInputRule[m].ExParam3 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = ACColumnInputRule[m].ExParam3;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    if (ACColumnInputRule[m].ExParam4 != "") {
                        var strsrl = new Array(); //定义一数组 
                        var strrlist = ACColumnInputRule[m].ExParam4;
                        strsrl = strrlist.split("["); //字符分割
                        if (strsrl[0] == "") {
                            var thisvalue = strsrl[1].split("]")[0];
                            inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                        }
                        else {
                            inputParams[inputParams.length] = strrlist;
                        }
                    }
                    var result = ExecuteRule(ACColumnInputRule[m].RuleMethod, inputParams);
                    result.PromptMode = ACColumnInputRule[m].PromptMode;
                    return result;
                }
            }
        }

        if ((objColumn && objColumn.allowNull == true && !UCML.isEmpty(value)) && (objColumn.RuleList != null && objColumn.RuleList.length > 0)) {
            for (var i = 0; i < objColumn.RuleList.length; i++) {
                var inputParams = new Array();

                inputParams[inputParams.length] = value;
                if (objColumn.RuleList[i].ExParam1 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam1;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam2 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam2;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam3 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam3;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam4 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam4;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                var result = ExecuteRule(objColumn.RuleList[i].RuleMethod, inputParams);
                result.PromptMode = objColumn.RuleList[i].PromptMode;
                result.ReturnDescText = objColumn.RuleList[i].InputTip;
                return result;
            }

        }
    }

    function doRuleCheck(ctl, val, dataTable, columnName) {
        var result = ruleCheck(val, dataTable, columnName);

        var tip = $(".validatebox-tip");
        if (tip) {
            tip.each(function () {
                $(this).hide();
            });
        }

        if (result) {
            if (result.ReturnCode != "OK") {
                if (result.PromptMode == 0) {
                    var vctl = ctl.applet.getValiateControl(ctl);
                    if (vctl) {
                        vctl.setTipMessage(result.ReturnDescText);
                        vctl.showTip();
                    }
                    else {
                        vctl = new UCML.Validatebox({ dom: ctl.dom, id: UCML.id() });
                        ctl.applet.setValiateControl(ctl.id, vctl);
                        vctl.setTipMessage(result.ReturnDescText);
                        vctl.showTip();
                    }
                }
                else if (result.PromptMode == 1) {
                    alert(result.ReturnDescText);
                }
                return false;
            }
            else {
                if (result.PromptMode == 0) {
                    var vctl = ctl.applet.getValiateControl(ctl);
                    if (vctl) {
                        vctl.hideTip();
                    }
                }
            }
        }
        else {
            var vctl = ctl.applet.getValiateControl(ctl);
            if (vctl) {
                vctl.hideTip();
            }
        }
        return true;
    }

    /**
    * @class UCML.Edit
    * @extends UCML.Applet
    * @description 编辑控件
    * @param {object} id 容器控件id
    */
    UCML.Edit = function (id) {

        /**   
        * @method controls 
        * @description 编辑控件集合
        */
        this.controls = new Array();

        this.fieldNameControls = {};

        this.valiateControls = {};
        /**   
        * @method isToLabel 
        * @description 是否转换为浏览状态
        */
        this.isToLabel = false;
        UCML.Edit.superclass.constructor.call(this, id);
    };

    /**
    * @class UCML.Edit
    * @extends UCML.Applet
    * @description 编辑控件,继承自UCML.Applet
    */
    UCML.extend(UCML.Edit, UCML.Applet, {
        ctype: "UCML.Edit",
        itemFirstText: UCML.Languge.DLLitemFirstText,
        editTableCols: 3, //编辑表格列数
        showBCLink: true,
        onRender: function () {
            UCML.Edit.superclass.onRender.call(this);

            //增加自动表单功能
            if (this.userDefineHTML == false) {
                this.renderEditPanel();
            }
            if (this.haveScroll) {
                this.el.css("overflow", "auto");
            }
            else {
                this.el.css("overflow", "hidden");
            }
        },
        renderEditPanel: function () {
            this.el.html("");
            var editPanel = $("<DIV style=\"WIDTH: 100%; HEIGHT: auto; OVERFLOW: hidden\" id=" + this.id + "editPanel  class=UCML-Header ></div>");
            var editTable = this.renderEditTable(this.editTableCols);
            editPanel.append(editTable);
            this.el.append(editPanel);
            editPanel.show();
            return editPanel;
        }
        ,
        editTableStr: "<table style=\"width: 100%;\" border=\"0\" style=\"table-layout: fixed !important;display: table;\" cellSpacing=\"0\" cellPadding=\"0\">{rows}</tbody>",
        editRowStr: '<tr>{cells}</tr>',
        editCellStr: '<td style="width:{width}%; text-align: right;" id="{fieldName}Label" class="UCML-LabelCell" noWrap=""><div id="{fieldName}LabelValue" class="UCML-LabelText">{caption}</div></td>' +
    '<td style="width:{width}%;" id="{fieldName}Cell" class="UCML-InputCell" noWrap="" >{input}</td>'
        ,
        renderEditTable: function (cols) {
            var editTableTpl = new UCML.Template(this.editTableStr);

            var rowsStr = [];

            var rowCount = 0;
            var clength = this.columns.length;
            var cellWidth = 100 / (cols * 2);

            if (clength % cols == 0) {
                rowCount = parseInt(clength / cols);
            }
            else {
                rowCount = parseInt(clength / cols) + 1;
            }


            for (var i = 0; i < rowCount; i++) {
                var cellStr = [];

                var n = i * cols;
                var n1 = n == 0 ? 0 : n + 1;
                var n2 = n == 0 ? cols - 1 : n + cols;
                for (var j = n1; j <= n2; j++) {
                    var editCellTpl = new UCML.Template(this.editCellStr);

                    var inputStr = "";
                    var labelStr = "";
                    var fieldName = "";
                    var EditType = "";
                    var caption = "";
                    if (j >= 0 && j < clength) {

                        fieldName = this.columns[j].fieldName;
                        caption = this.columns[j].caption;
                        EditType = this.columns[j].EditType;

                        inputStr = this.getInputCell(EditType, fieldName);

                        cellStr.add(editCellTpl.apply({ input: inputStr, label: labelStr, fieldName: fieldName, caption: caption, width: cellWidth }));
                    }
                    else {
                        cellStr.add(editCellTpl.apply({ input: inputStr, label: labelStr, fieldName: fieldName, caption: caption, width: cellWidth }));
                    }
                }

                var editRowTpl = new UCML.Template(this.editRowStr);
                rowsStr.add(editRowTpl.apply({ cells: cellStr.join('') }));
            }

            return editTableTpl.apply({ rows: rowsStr.join('') });
        }
        ,
        getInputCell: function (EditType, fieldName) {
            var type = EditType.toLowerCase();
            switch (type) {
                case "ucml.textbox":
                    return '<input id="' + fieldName + 'Edit" type="text" dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-inputframe" />';
                    break;
                case "ucml.password ":
                    return '<input id="' + fieldName + 'Edit" type="password" dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-inputframe" />';
                    break;
                case "ucml.textarea":
                    return '<textarea id="' + fieldName + 'Edit" style="width:400px; height:200px"  dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-textareaframe" />';
                    break;
                case "ucml.checkbox":
                    return '<input id="' + fieldName + 'Edit" type="checkbox" dataFld="' + fieldName + '"  ctype="' + EditType + '"  />';
                    break;
                case "ucml.radio":
                    return '<input id="' + fieldName + 'Edit" type="radio" dataFld="' + fieldName + '"  ctype="' + EditType + '"  />';
                    break;
                case "ucml.hidden":
                    return '<input id="' + fieldName + 'Edit" type="hidden" dataFld="' + fieldName + '"  ctype="' + EditType + '"  />';
                    break;
                case "ucml.image":
                    return '<img id="' + fieldName + 'Edit"  dataFld="' + fieldName + '"  ctype="' + EditType + '"  />';
                    break;
                case "ucml.label":
                    return '<div id="' + fieldName + 'Edit"  dataFld="' + fieldName + '"  ctype="' + EditType + '" ></div>';
                    break;
                case "ucml.bclink":
                    return '<a id="' + fieldName + 'Edit"  dataFld="' + fieldName + '"  ctype="' + EditType + '" ></a>';
                    break;
                case "ucml.html":
                    return '<textarea id="' + fieldName + 'Edit" style="width:400px; height:200px"  dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-textareaframe" />';
                    break;
                case "ucml.datebox":
                    return '<input id="' + fieldName + 'Edit" type="text" dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-inputframe" />';
                    break;
                case "ucml.timebox":
                    return '<input id="' + fieldName + 'Edit" type="text" dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-inputframe" />';
                    break;
                case "ucml.datetimebox":
                    return '<input id="' + fieldName + 'Edit" type="text" dataFld="' + fieldName + '"  ctype="' + EditType + '" class="ucml-inputframe" />';
                    break;
                default:
                    return '<input id="' + fieldName + 'Edit" type="text" dataFld="' + fieldName + '"  ctype="UCML.TextBox" class="ucml-inputframe" />';
                    break
            }
        }
        ,
        bindEvents: function () {

            UCML.Edit.superclass.bindEvents.call(this);
            if (!UCML.isEmpty(this.dataTable)) {
                if (this.IsJQMPage) {
                    var opts = this;
                    var curPage = $("#" + this.id + "_Page");
                    if (curPage.length > 0 && curPage.css("display").toLowerCase() == "none") {
                        $(document).on("pageinit", "#" + this.id + "_Page", function () {
                            opts.onAfterLoadData();
                            opts.BusinessObject.on("onAfterLoadData", opts.onAfterLoadData, opts);
                            opts.dataTable.on("OnRecordChange", opts.BCLoadData, opts); //记录发生变化时
                            opts.dataTable.on("onLoad", opts.BCLoadData, opts);
                            opts.dataTable.on("OnFieldChange", opts.onFieldChange, opts);
                            opts.dataTable.on("OnValiate", opts.onValiate, opts);
                        });
                    }
                    else {
                        this.BusinessObject.on("onAfterLoadData", this.onAfterLoadData, this);
                        this.dataTable.on("OnRecordChange", this.BCLoadData, this); //记录发生变化时
                        this.dataTable.on("onLoad", this.BCLoadData, this);
                        this.dataTable.on("OnFieldChange", this.onFieldChange, this);
                        this.dataTable.on("OnValiate", this.onValiate, this);
                    }
                } else {
                    this.BusinessObject.on("onAfterLoadData", this.onAfterLoadData, this);
                    this.dataTable.on("OnRecordChange", this.BCLoadData, this); //记录发生变化时
                    this.dataTable.on("onLoad", this.BCLoadData, this);
                    this.dataTable.on("OnFieldChange", this.onFieldChange, this);
                    this.dataTable.on("OnValiate", this.onValiate, this);
                }
            }

            var $input = this.el.find("[dataFld][ctype!='UCML.Label']");

            UCML.on(this.el.find("[dataFld][ctype!='UCML.TextArea']"), "keydown", function (el, e) {
                if (e.keyCode == 13) {
                    e.preventDefault();

                    var index = $input.index($("#" + e.target.id));
                    if (index == -1) {

                    } else {
                        if (index == $input.length - 1) {
                            index = -1;
                        }

                        var flag = 0;
                        for (var i = index; i < $input.length - 1; i++) {
                            if ($($input[i + 1]).attr("disabled") != "disabled") {
                                index = i;
                                flag = 1;
                                break;
                            }
                        }
                        if (flag == 0) {
                            for (var i = -1; i < $input.length - 1; i++) {
                                if ($($input[i + 1]).attr("disabled") != "disabled") {
                                    index = i;
                                    break;
                                }
                            }
                        }
                        $input[index + 1].focus();
                        if ($($input[index + 1]).attr("ctype") != "UCML.DropDownList" && $($input[index + 1]).attr("ctype") != "UCML.TextArea") {
                            $input[index + 1].select();
                        }
                    }
                }
            }, this);
        }, onMenuReady: function () {
            var menu = new UCML.Menu();
            if (this.enabledEdit) {
                menu.addMenuItem("cmd_InsertRecord", UCML.Languge.DBInsertRecord);
                menu.addMenuItem("cmd_DeleteRecord", UCML.Languge.DBDeleteRecord);

                menu.addSeparator();
                menu.addMenuItem("cmd_Submit", UCML.Languge.DBSubmit);
                menu.addMenuItem("cmd_Cancel", UCML.Languge.DBCancel);
                menu.addMenuItem("cmd_Refresh", UCML.Languge.DBRefresh);
            }
            this.menu = menu;
        }
    , initControl: function () {//初始化控件
        var beginTime = new Date();
        var inputs = this.el.find("[ctype]"); //dataField 匈牙利命名方式
        //   console.info("p2:" + (new Date() - beginTime));
        for (var i = 0; i < inputs.length; i++) {
            var ctl = this.addControl(inputs[i]);
            if (ctl) {
                this.setControl(ctl);
            }
        }
        this.el.find('.UCML-Header').show();
        //  console.info("p3:" + (new Date() - beginTime));
        //    alert(new Date() - beginTime);
    }, setControl: function (ctl) { //设置控件参数
        ctl.fieldName = ctl.getAttribute("dataFld") || ctl.fieldName || "";

        ctl.BusinessObject = this.BusinessObject;
        if (!ctl.dataTable && ctl.getAttribute("BCName")) {
            ctl.dataTable = ctl.BusinessObject.getDataTableByBCName(ctl.getAttribute("BCName"));
            // ctl.dataTable = eval(ctl.getAttribute("BCName") + "Base");
        }
        if (ctl.dataTable && ctl.fieldName) {

            var bcCom = ctl.dataTable.getColumn(ctl.fieldName);
            if (bcCom && bcCom.isCodeTable) {
                if (ctl.isCodeTable == true && UCML.isEmpty(this.codeTable)) { //控件是代码表但是没设置代码表名称直接去BC上字段的代码表
                    ctl.codeTable = bcCom.codeTable;
                    ctl.bindCodeTable(); //重新绑定代码表
                }
                else {
                    ctl.isCodeTable = bcCom.isCodeTable;
                    ctl.codeTable = bcCom.codeTable;
                }
            }

            if (this.showBCLink && bcCom.objBCLinkColl && ctl.ctype == "UCML.Label") {
                var pBody = ctl.el.parents(".UCML-InputCell");
                this.controls.remove(ctl);
                ctl.destroy();

                var BCLinkConfig = bcCom.objBCLinkColl[0];
                var caption;
                if (BCLinkConfig.caption) {
                    caption = BCLinkConfig.caption;
                }

                var BCLinkEL = $("<a ctype='UCML.BCLink' dataFld='" + ctl.fieldName + "'  linkIndex='0' href='javascript:void(0)' title='" + (BCLinkConfig.title || "") + "'  BCName='" + ctl.dataTable.BCName + "'  >" + (caption || "请选择") + "</a>");
                pBody.append(BCLinkEL);

                var ctl = this.addControl(BCLinkEL[0]);

                ctl.fieldName = ctl.getAttribute("dataFld") || ctl.fieldName || "";
                ctl.dataTable = ctl.BusinessObject.getDataTableByBCName(ctl.getAttribute("BCName"));
                // ctl.dataTable = eval(ctl.getAttribute("BCName") + "Base");
            }

            ctl.on("setValue", this.setControlValue, ctl);
            this.controls.add(ctl);
        }

        if (ctl.fieldName && ctl.fieldName != "") {
            this.fieldNameControls[ctl.fieldName] = this.fieldNameControls[ctl.fieldName] || [];
            this.fieldNameControls[ctl.fieldName].add(ctl);

            var col = this.getColumn(ctl.fieldName);
            if (col && !col.allowModify) {
                ctl.disable();
            }
            if (col && !col.display) {
                ctl.hide();
                $("#" + ctl.fieldName + "LabelValue").hide();
            }
            //添加录入提示
            if (col && col.PromptText && col.PromptText != "") {
                ctl.el.attr("title", col.PromptText);
            }
        }
    }
    , setControlValue: function (value) {
        var columnInfo = this.dataTable.getColumn(this.fieldName);
        doRuleCheck(this, value, this.dataTable, this.fieldName);
        this.dataTable.setFieldValue(this.fieldName, value, this);
    },
        setValiateControl: function (id, ctl) {
            this.valiateControls[id] = ctl;
        },
        getValiateControl: function (ctl) {
            return this.valiateControls[ctl.id];
        },
        getValiateControlById: function (id) {
            return this.valiateControls[id];
        },
        clearValiateControls: function () {
            for (var item in this.valiateControls) {
                item.destroy();
            }
            this.valiateControls = {};
        },
        onValiate: function (config) {
            for (var i = 0; i < this.controls.length; i++) {
                if (this.controls[i].fieldName == config.fieldName) {
                    //                if (this.controls[i].focus) {
                    //                    this.controls[i].focus();
                    //                }
                    var vctl = this.getValiateControl(this.controls[i]);
                    if (vctl) {
                        vctl.setTipMessage(config.msg);
                        vctl.showTip();
                    }
                    else {
                        vctl = new UCML.Validatebox({ dom: this.controls[i].dom, id: UCML.id() });
                        this.setValiateControl(this.controls[i].id, vctl);
                        vctl.setTipMessage(config.msg);
                        vctl.showTip();
                    }

                    return false;
                }
            }
        }
    });

    /**
    * @class UCML.Edit
    * @extends UCML.Applet
    * @description 初始化Edit
    */
    UCML.Edit.prototype.init = function () {
        UCML.Edit.superclass.init.call(this);
        //装载编辑控件

        //  var beginTime2 = new Date();
        this.initControl();
        //var endTime2 = new Date();
        //   alert(this.id + "查找控件时间:" + (endTime2 - beginTime2));
    }

    UCML.Edit.prototype.onAfterLoadData = function (data) {
        /*
        for (var i = 0; i < this.controls.length; i++) {
        if (this.controls[i].setValueBack) {
        if (this.controls[i].dataTable && this.controls[i].fieldName) {
        var value = this.dataTable.getFieldValue(this.controls[i].fieldName);

        this.controls[i].setValueBack(value);
        }
        }
        }*/

        var opts = this;
        setTimeout(function () {
            opts.loadData(data);
        }, 1);
    }

    /**   
    * @method loadData 
    * @description 装载数据 
    */
    UCML.Edit.prototype.BCLoadData = function (data) {
        if (this.BusinessObject && this.BusinessObject.dataLoading == false) {//如果是首次加载
            return;
        }

        var opts = this;
        setTimeout(function () {
            opts.loadData(data);
        }, 100);
    }

    /**   
    * @method loadData 
    * @description 装载数据 
    */
    UCML.Edit.prototype.loadData = function (data) {
        for (var i = 0; i < this.controls.length; i++) {
            if (this.controls[i].setValueBack) {
                if (this.controls[i].dataTable && this.controls[i].fieldName) {
                    var value = this.dataTable.getFieldValue(this.controls[i].fieldName);

                    this.controls[i].setValueBack(value);

                    var vctl = this.getValiateControl(this.controls[i]);
                    if (vctl) {
                        this.setValiateControl(this.controls[i].id, null);
                        vctl.destroy();
                    }
                }
            }
        }

    }
    /**   
    * @method clearData 
    * @description 清除数据 
    */
    UCML.Edit.prototype.clearData = function () {
        for (var i = 0; i < this.controls.length; i++) {
            if (this.controls[i].setValueBack) {
                this.controls[i].setValueBack("");
                var vctl = this.getValiateControl(this.controls[i]);
                if (vctl) {
                    this.setValiateControl(this.controls[i].id, null);
                    vctl.destroy();
                }
            }
        }
    }


    /**   
    * @method addControl 
    * @description 添加控件
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.addControl = function (o) {
        var el = $(UCML.getDom(o));
        var BCName = el.attr("BCName") || (this.dataTable ? this.dataTable.BCName : this.el.attr("BCName"));
        var ctype = el.attr("ctype") || "UCML.Input";
        var ctl;
        if (ctype && BCName) {
            el.attr("BCName", BCName);
            el.attr("VCName", this.id);


            ctl = UCML.create({ el: el, BusinessObject: this.BusinessObject, disabled: this.disabled, isSetProperties: true, applet: this }, ctype);
        }
        return ctl;
    }
    /**   
    * @method setToLabel 
    * @description 设置字段呈现为浏览状态
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.setToLabel = function (val) {
        this.isToLabel = val;
    }

    /**   
    * @method deleteRecord 
    * @description 删除记录
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.deleteRecord = function () {
        this.dataTable.Delete();
        this.loadData();
    }
    /**   
    * @method insertRecord 
    * @description 添加记录
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.insertRecord = function () {
        this.dataTable.Insert();
        this.loadData();
    }
    /**   
    * @method updateRecord 
    * @description 更新记录
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.updateRecord = function () {
        this.loadData();
    }
    /**   
    * @method onFieldChange 
    * @description 修改时字段发生变化
    * @param {Object} control 控件    
    */


    UCML.Edit.prototype.onFieldChange = function (e, sender) {

        var controls = this.fieldNameControls[e.fieldName] || [];
        for (var i = 0; i < controls.length; i++) {
            if (controls[i] != sender && controls[i].getValue() != e.value) {
                controls[i].setValue(e.value, false);
                doRuleCheck(controls[i], e.value, controls[i].dataTable, controls[i].fieldName);
            }
        }
    }


    UCML.Edit.prototype.setEnabledEditByFieldName = function (val, fieldName) {
        if (fieldName) {
            var controls = this.fieldNameControls[fieldName] || [];
            for (var i = 0; i < controls.length; i++) {
                if (val == true) {
                    controls[i].enable();
                }
                else {
                    controls[i].disable();
                }
            }
        }
    }

    /**   
    * @method setEnabledEdit 
    * @description 设置字段是否可编辑
    * @param {Object} control 控件    
    */
    UCML.Edit.prototype.setEnabledEdit = function (val, fieldName) {
        UCML.Edit.superclass.setEnabledEdit(val);

        if (fieldName) {
            this.setEnabledEditByFieldName(val, fieldName);
        }
        else {
            for (var i = 0; i < this.controls.length; i++) {
                if (val == true) {
                    this.controls[i].enable();
                }
                else {
                    this.controls[i].disable();
                }
            }
        }
    }

})();




