﻿UCML.TabPanel = function (id) {
    this.panels = [];
    this.addEvents("tabSelect", "tabChange");
    UCML.TabPanel.superclass.constructor.call(this, id);
}

UCML.extend(UCML.TabPanel, UCML.Container, {
    ctype: "UCML.TabPanel",
    selectPanel: null,
    plain: false,
    border: true,
    showMode: 0, //0 top
    scrollIncrement: 100,
    scrollDuration: 400,
    onLoad: function () { },
    onSelect: function (title) { },
    onClose: function (title) { },
    onRender: function () {
        UCML.TabPanel.superclass.onRender.call(this);

        this.el.addClass('tabs-container');
        this.el.wrapInner('<div class="tabs-content"/>');
        var opts = this;
        this.header = $('<div class="tabs-header">'
				+ '<div class="tabs-scroller-left"></div>'
				+ '<div class="tabs-scroller-right"></div>'
				+ '<div class="tabs-wrap">'
				+ '<ul class="tabs"></ul>'
				+ '</div>'
				+ '</div>');

        //添加jqm的样式设置
        if ((typeof BPOIsJQMPage !== "undefined") && BPOIsJQMPage) {
            var theme = BPOPageTheme || "a";
            this.header.addClass("tabs-header-" + theme);
        }
        else if (this.el.attr("data-role") == "page" && !UCML.isEmpty(this.el.attr("data-theme"))) {
            var theme = this.el.attr("data-theme") || "a";
            this.el.addClass('tabs-container-' + theme);
            this.header.addClass("tabs-header-" + theme);
        }

        if (this.showMode == 0) {
            this.el.prepend(this.header);
        }
        else {
            this.el.append(this.header);
        }
        //  this.header = $('>div.tabs-header', this.el);
        this.content = $('>div.tabs-content', this.el);
        this.tabs = $('ul.tabs', this.header);

        if (opts.plain == true) {
            this.header.addClass('tabs-header-plain');
        } else {
            this.header.removeClass('tabs-header-plain');
        }
        if (opts.border == true) {
            this.header.removeClass('tabs-header-noborder');
            this.content.removeClass('tabs-content-noborder');
        } else {
            this.header.addClass('tabs-header-noborder');
            this.content.addClass('tabs-content-noborder');
        }
    },
    afterRender: function () {
        var opts = this;
        $('>div.tabs-content>div', this.el).each(function () {
            var el = $(this);
            opts.addTab({ body: el, title: el.attr("title"), scrollers: false });
            el.removeAttr('title');
        });
        if (this.panels.length > 0) {
            this.selectPanel = this.panels[0];
            this.selectPanel.show();
            this.selectPanel.tab.addClass('tabs-selected');
            this.selectPanel.tab.blur();
        }

        UCML.TabPanel.superclass.afterRender.call(this);
    },
    setProperties: function () {
        UCML.TabPanel.superclass.setProperties.call(this);
        this.border = (this.el.attr('border') ? this.el.attr('border') == 'true' : this.border);
        this.plain = (this.el.attr('plain') ? this.el.attr('plain') == 'true' : this.plain);
    }
    , getPanel: function (v) {
        var index = this.getPanelIndex(v);
        if (index > -1) {
            return this.panels[index];
        }
        return false;
    }
})


UCML.TabPanel.prototype.getTabLeftPosition = function (tab) {
    var w = 0;
    var b = true;
    $('ul.tabs li', this.header).each(function () {
        if (this == tab) {
            b = false;
        }
        if (b == true) {
            w += $(this).outerWidth(true);
        }
    });
    return w;
}

UCML.TabPanel.prototype.getMaxScrollWidth = function () {
    var tabsWidth = 0; // all tabs width
    $('ul.tabs li', this.header).each(function () {
        tabsWidth += $(this).outerWidth(true);
    });
    var wrapWidth = $('.tabs-wrap', this.header).width();
    var padding = parseInt($('.tabs', this.header).css('padding-left'));

    return tabsWidth - wrapWidth + padding;
}

UCML.TabPanel.prototype.setScrollers = function () {
    var tabsWidth = 0;
    $('ul.tabs li', this.header).each(function () {
        tabsWidth += $(this).outerWidth(true);
    });

    if (tabsWidth > this.header.width()) {
        $('.tabs-scroller-left', this.header).css('display', 'block');
        $('.tabs-scroller-right', this.header).css('display', 'block');
        $('.tabs-wrap', this.header).addClass('tabs-scrolling');

        if ($.boxModel == true) {
            $('.tabs-wrap', this.header).css('left', 2);
        } else {
            $('.tabs-wrap', this.header).css('left', 0);
        }
        var width = this.header.width()
				- $('.tabs-scroller-left', this.header).outerWidth()
				- $('.tabs-scroller-right', this.header).outerWidth();
        $('.tabs-wrap', this.header).width(width);

    } else {
        $('.tabs-scroller-left', this.header).css('display', 'none');
        $('.tabs-scroller-right', this.header).css('display', 'none');
        $('.tabs-wrap', this.header).removeClass('tabs-scrolling').scrollLeft(0);
        $('.tabs-wrap', this.header).width(this.header.width());
        $('.tabs-wrap', this.header).css('left', 0);
    }
}

UCML.TabPanel.prototype.setSize = function (param, a) {
    UCML.TabPanel.superclass.setSize.call(this, param, a);
    if (this.height != "auto") {
        var height = this.el.height();
        var contentHeight = height;
        if ($.boxModel == true) {
            contentHeight = height - this.header.outerHeight(true) - (this.content.outerHeight(true) - this.content.height());
        } else {
            contentHeight = height - this.header.outerHeight(true) - (this.content.outerHeight(true) - this.content.height());
        }

        this.selectPanel.body.css("overflow", "hidden");//适应屏幕时去掉 非ie浏览器的滚动条
       
        this.content.height(contentHeight);
        if (this.selectPanel) {
            this.selectPanel.setSize({ height: contentHeight, width: this.el.width() }, "TabPanel");
        }
    }
    else {
        this.content.height('auto');
        if (this.selectPanel) {
            this.selectPanel.setSize({ height: 'auto', width: this.width }, "TabPanel");
        }
    }
    this.setScrollers();
}


UCML.TabPanel.prototype.bindEvents = function () {
    UCML.TabPanel.superclass.bindEvents.call(this);
    var opts = this;

    $('.tabs-scroller-left, .tabs-scroller-right', this.header).hover(
			function () { $(this).addClass('tabs-scroller-over'); },
			function () { $(this).removeClass('tabs-scroller-over'); }
		);

    $('.tabs-scroller-left', this.header).unbind('.tabs').bind('click.tabs', function () {
        var wrap = $('.tabs-wrap', this.header);
        var pos = wrap.scrollLeft() - opts.scrollIncrement;
        wrap.animate({ scrollLeft: pos }, opts.scrollDuration);
    });

    $('.tabs-scroller-right', this.header).unbind('.tabs').bind('click.tabs', function () {
        var wrap = $('.tabs-wrap', this.header);
        var pos = Math.min(
					wrap.scrollLeft() + opts.scrollIncrement,
					opts.getMaxScrollWidth()
			);
        wrap.animate({ scrollLeft: pos }, opts.scrollDuration);
    });
}

UCML.TabPanel.prototype.createTab = function (panel) {

    var opts = this;

    var tab = $('<li></li>');
    var tab_span = $('<span></span>').html(panel.title);
    var tab_a = $('<a class="tabs-inner"></a>')
				.attr('href', 'javascript:void(0)')
				.append(tab_span);
    tab.append(tab_a).appendTo(this.tabs);


    if (panel.closable) {
        tab_span.addClass('tabs-closable');
        tab_a.after('<a href="javascript:void(0)" class="tabs-close"></a>');
    }
    if (panel.icon) {
        tab_span.addClass('tabs-with-icon');
        tab_span.after($('<span/>').addClass('tabs-icon').addClass(options.icon));
    }
    if (panel.content) {
        panel.body.html(panel.content);
    }

    tab.unbind('.tabs').bind('click.tabs', function () {
        opts.selectTab(tab);
    });

    $('.tabs-close', tab).unbind('.tabs').bind('click.tabs', function () {
        opts.closeTab(tab);
    });

    return tab;
}

UCML.TabPanel.prototype.addTab = function (options) {
    if (UCML.isString(options)) {
        options = { title: options };
    }
    options = options || {};
    options = UCML.apply({
        id: null,
        title: '',
        content: '',
        href: null,
        cache: true,
        icon: null,
        closable: false,
        selected: false,
        rendeTo: this.content,
        noheader: true,
        border: false,

        doSize: false,
        closed: true,
        scrollers: true
    }, options);

    if (options.scrollers) {
        this.setScrollers();
    }
    var panel = new UCML.Panel(options);
    panel.tab = this.createTab(panel);
    this.panels.add(panel);
}

UCML.TabPanel.prototype.getPanelIndex = function (v) {
    var name = "title";
    if (UCML.isNumber(v)) {
        return v;
    }
    if (UCML.isObject(v)) {
        name = "tab";
    }
    for (var i = 0; i < this.panels.length; i++) {
        if (this.panels[i][name] == v) {
            return i;
        }
    }
    return -1;
}

// close a tab with specified title
UCML.TabPanel.prototype.closeTab = function (v) {
    var panel = this.getPanel(v);
    if (panel) {

        var index = this.panels.indexOf(panel);
        this.panels.remove(panel);
        if (this.panels.length > 0 && index == this.panels.length) {
            this.selectTab(index - 1);
        }
        else if (index <= this.panels.length - 1) {
            this.selectTab(index);
        }
        else {
            this.selectTab(0);
        }
        panel.tab.remove();
        panel.destroy();


        //        var wrap = $('>div.tabs-header .tabs-wrap', this.el);
        //        var pos = Math.min(
        //					wrap.scrollLeft(),
        //					this.getMaxScrollWidth()
        //			);
        //        wrap.animate({ scrollLeft: pos }, this.scrollDuration);

        this.setScrollers();
    }
}

//获取头
UCML.TabPanel.prototype.getHeader = function (v) {
    var panel = this.getPanel(v);
    if (panel) {
        return panel.tab;
    }
}


UCML.TabPanel.prototype.hidenTab = function (v) {
    var panel = this.getPanel(v);
    if (panel) {

        var index = this.panels.indexOf(panel);

        if (this.panels.length > 0 && index == this.panels.length) {
            this.selectTab(index - 1);
        }
        else if (index <= this.panels.length - 1) {
            this.selectTab(index);
        }
        else {
            this.selectTab(0);
        }
        panel.tab.hide();
        panel.close();
        this.setScrollers();
    }
}

UCML.TabPanel.prototype.showTab = function (v) {
    var panel = this.getPanel(v);
    if (panel) {
        var index = this.panels.indexOf(panel);

        if (this.panels.length > 0 && index == this.panels.length) {
            this.selectTab(index - 1);
        }
        else if (index <= this.panels.length - 1) {
            this.selectTab(index);
        }
        else {
            this.selectTab(0);
        }
        panel.tab.show();
        panel.open();
        this.setScrollers();
    }
}

// active the selected tab item, if no selected item then active the first item
UCML.TabPanel.prototype.selectTab = function (v) {
    var beginData = new Date();
    var panel = this.getPanel(v);
    if (panel) {

        if (this.selectPanel) {
            this.selectPanel.tab.removeClass('tabs-selected');
            this.selectPanel.hide();
        }
        panel.tab.addClass('tabs-selected');
     //   panel.tab.blur();  解决滚动条跳动问题
        this.selectPanel = panel;
        panel.show();
      

        this.setSize(null, "selectTab");
        //        var wrap = $('.tabs-wrap', this.header);
        //        var leftPos = this.getTabLeftPosition(panel.tab);
        //        var left = leftPos - wrap.scrollLeft();
        //        var tabsWidth = 0;
        //        $('ul.tabs li', this.header).each(function () {
        //            tabsWidth += $(this).outerWidth(true);
        //        });
        //        var right = left + tabsWidth;
        //        if (left < 0 || right > wrap.innerWidth()) {
        //            var pos = Math.min(
        //						leftPos - (wrap.width() - $(this).width()) / 2,
        //						this.getMaxScrollWidth()
        //				);
        //            wrap.animate({ scrollLeft: pos }, this.scrollDuration);
        //        }
        var paenlIndex = this.panels.indexOf(panel);
        this.fireEvent('tabSelect', panel, paenlIndex);
        this.onSelect.call(this, panel, paenlIndex);
    }
}

UCML.TabPanel.prototype.exists = function (v) {
    return this.getPanel(v);
}
