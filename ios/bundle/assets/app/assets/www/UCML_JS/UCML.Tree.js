﻿UCML.Tree = function (id) {
    this.addEvents(
    /**
    * @event nodeclick
    * @description 点击节点时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeclick',

    /**
    * @event nodecheckclick
    * @description 点击复选框选中时
    * @param {UCML.TreeNode} node 节点
    */
            'nodecheckclick',

    /**
    * @event nodedblclick
    * @description 双击节点时
    * @param {UCML.TreeNode} node 节点
    */
            'nodedblclick',

    /**
    * @event nodecontextmenu
    * @description 节点右击时
    * @param {UCML.TreeNode} node 节点
    */
            'nodecontextmenu',
            'nodeexpandcollapseclick',

    /**
    * @event nodeexpand
    * @description 节点展开时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeexpand',

    /**
    * @event nodecollapse
    * @description 节点收缩时
    * @param {UCML.TreeNode} node 节点
    */
            'nodecollapse',

    /**
    * @event nodeselect
    * @description 节点选中时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeselect',

    /**
    * @event nodeunselect
    * @description 节点撤销选中时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeunselect',

    /**
    * @event nodecheck
    * @description 节点复选框选中时
    * @param {UCML.TreeNode} node 节点
    */
            'nodecheck',

    /**
    * @event nodeuncheck
    * @description 节点复选框撤销选中时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeuncheck',

    /**
    * @event checkchange
    * @description 节点复选框状态发生变化时
    * @param {UCML.TreeNode} node 节点
    */
            'checkchange',

    /**
    * @event nodeadd
    * @description 节点添加时
    * @param {UCML.TreeNode} node 节点
    */
            'nodeadd',

    /**
    * @event nodebeforeadd
    * @description 节点添加前
    * @param {UCML.TreeNode} node 节点
    */
            'nodebeforeadd',

    /**
    * @event nodebeforeremove
    * @description 节点移除前
    * @param {UCML.TreeNode} node 节点
    */
            'nodebeforeremove',

    /**
    * @event nodebeforeloaddata
    * @description 节点数据装载前
    * @param {UCML.TreeNode} node 节点
    */
            'nodebeforeloaddata',

    /**
    * @event change
    * @description 节点发生变化时
    * @param {UCML.TreeNode} node 节点
    */
            'change',

    /**
    * @event nodebeforerender
    * @description 节点渲染前
    * @param {UCML.TreeNode} node 节点
    */
            'nodebeforerender',

    /**
    * @event noderender
    * @description 节点渲染时
    * @param {UCML.TreeNode} node 节点
    */
            'noderender'
        );

    this.selectNode = null;
    this.focusNode = null;
    this.checkNodes = Array();
    this.childFilter = []; //是否有孩子结点 是数组用于动态树和子表树 判断是否有孩子结点,
    this.nodes = {};
    UCML.Tree.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Tree, UCML.Applet, {
    ctype: "UCML.Tree",

    /**   
    * @property rootId 
    * @description 根节点ID
    * @type String
    */
    rootId: "00000000-0000-0000-0000-000000000000",

    /**   
    * @property rootVisible 
    * @description 根节点是否显示
    * @type Boolean
    */
    rootVisible: false,

    /**   
    * @property rootText 
    * @description 根节点文本
    * @type String
    */
    rootText: UCML.Languge.Tree_RootText,

    /**   
    * @property rootExpand 
    * @description 根节点是否展开
    * @type Boolean
    */
    rootExpand: true,

    /**   
    * @property animate 
    * @description 是否启用动画
    * @type Boolean
    */
    animate: false,

    /**   
    * @property animate 
    * @description 是否显示复选框
    * @type Boolean
    */
    showCheckbox: false,


    showLine: true,
    showIcon: true,


    /**   
    * @property expandDepth 
    * @description 节点展开层次深度
    * @type int
    */
    expandDepth: 0,

    /**   
    * @property clicksToEdit 
    * @description  点击节点触发编辑 1:单击触发,2:双击触发
    * @type int
    */
    clicksToEdit: 2,

    /**   
    * @property clicksToSelect 
    * @description  点击节点触发选择 1:单击触发,2:双击触发
    * @type int
    */
    clicksToSelect: 1,

    /**   
    * @property selectForToggle 
    * @description  节点选择时是否联动状态切换
    * @type Boolean
    */
    selectForToggle: false,

    /**   
    * @property selectForCheck 
    * @description  节点选择时是否选中复选框
    * @type Boolean
    */
    selectForCheck: true,

    /**   
    * @property selectMode 
    * @description  选择模式 0无选择 1单个选择，2多个选择
    * @type int
    */
    selectMode: 1,

    /**   
    * @property checkboxMode 
    * @description  复选框模式  0默认 ,1 子集联选，2 父级联选,3 子父联选, -1不联选
    * @type int
    */
    checkboxMode: 0,

    /**   
    * @property checked 
    * @description  是否默认选中复选框
    * @type Boolean
    */
    checked: false,

    /**   
    * @property trackMouseOver 
    * @description  是否支持鼠标悬停
    * @type Boolean
    */
    trackMouseOver: true,

    /**   
    * @property draggable 
    * @description  是否支持拖拽
    * @type Boolean
    */
    draggable: false,

    pathSeparator: "/",

    onLoadSuccess: function () { },
    onLoadError: function () { },
    onClick: function (node) { }, // node: id,text,attributes,target
    onDblClick: function (node) { }, // node: id,text,attributes,target
    setRootNode: function (node) {
        this.root = node;
        node.ownerTree = this;
        node.showCheckbox = this.showCheckbox;
        node.checked = this.checked;
        node.isRoot = true;
        this.registerNode(node);
        return node;
    },
    onRender: function () {
        if (this.imageFilePath != "" || this.imageFilePath != null) {
            var aryFimage = this.imageFilePath.split(";");
            this.openImgSrc = aryFimage[0];
            this.closeImgSrc = aryFimage[1] || this.openImgSrc;
            this.leafImgSrc = aryFimage[2] || this.openImgSrc;
        }
        this.ul = $('<ul  class="tree ' + (this.showLine ? 'x-tree-lines' : 'x-tree-no-lines') + '" ></ul>');
        this.editBody = $('<div class="tree-edit-body"></div>')

        this.root = new UCML.TreeNode({ text: this.rootText, id: this.rootId, expanded: this.rootExpand });
        this.setRootNode(this.root);
        if (this.rootVisible) {
            this.renderRoot(this.ul);
        }
        else {
            this.root.renderChildren(this.ul);
        }

        this.el.append(this.ul);
        this.el.append(this.editBody);

        if (this.haveScroll) {
            this.el.css("overflow", "auto");
        }
        else {
            this.el.css("overflow", "hidden");
        }

        this.el.attr("tabindex", "-1");
        this.on('keydown', this.onKeyDown, this);
    },
    getRootId: function (id) {
        var keyFiledType = this.dataTable.getColumn(this.dataTable.PrimaryKey).fieldType;
        var ParentOID;
        if (keyFiledType == "UCMLKey" || keyFiledType == "Guid")
            ParentOID = this.dataTable.getEmptyGuid();
        else
            ParentOID = '0';
    }
    ,
    onKeyDown: function (el, e) {
        var focusNode = this.getFocusNode();
        if (!focusNode) {
            return;
        }
        if (e.keyCode == 38) {//UP
            var ps = focusNode.previousSibling;
            if (ps) {
                if (!ps.isExpanded() || ps.childNodes.length < 1) {
                    this.nodeDblClick(ps);
                } else {
                    var lc = ps.lastChild;
                    while (lc && lc.isExpanded() && lc.childNodes.length > 0) {
                        lc = lc.lastChild;
                    }
                    this.nodeDblClick(lc);
                }
            } else if (focusNode.parentNode && (this.rootVisible || !focusNode.parentNode.isRoot)) {
                this.nodeDblClick(focusNode.parentNode);
            }

        } else if (e.keyCode == 40)//DOWN
        {
            if (!focusNode.isExpanded() && focusNode.hasChildNodes() && focusNode.nextSibling) {
                this.nodeDblClick(focusNode.nextSibling);
            }
            else if (focusNode.isExpanded() && focusNode.hasChildNodes()) {
                this.nodeDblClick(focusNode.item(0));
            }
            else if (focusNode.nextSibling) {
                this.nodeDblClick(focusNode.nextSibling);
            }
            else if (focusNode.parentNode && focusNode.parentNode.nextSibling) {
                this.nodeDblClick(focusNode.parentNode.nextSibling);
            }
        }
        else if (e.keyCode == 37)//LEFT
        {
            if (focusNode.hasChildNodes() && focusNode.isExpanded()) {
                focusNode.collapse();
            } else if (focusNode.parentNode && (this.rootVisible || focusNode.parentNode != this.root)) {
                this.nodeDblClick(focusNode.parentNode);
            }
        }
        else if (e.keyCode == 39)//RIGHT
        {
            if (focusNode.hasChildNodes()) {
                if (!focusNode.isExpanded()) {
                    focusNode.expand();
                } else if (focusNode.firstChild) {
                    this.nodeDblClick(focusNode.firstChild);
                }
            }
        }
    }
    ,
    renderRoot: function () {
        this.root.render(this.ul);
    },
    setSize: function (param) {
        UCML.Tree.superclass.setSize.call(this, param);

    },
    bindEvents: function () {
        UCML.Tree.superclass.bindEvents.call(this);

        if (this.selectMode == 2) {
            this.selectNode = new Array();
        }

        if (this.dataTable) {
            this.dataTable.on("onLoad", this.loadData, this);

            this.dataTable.on("OnFieldChange", this.onFieldChange, this);

            this.dataTable.on("OnRecordChange", this.onRecordChange, this);
        }
        if (this.clicksToSelect == 3 || this.clicksToSelect == 1) {
            this.on("nodeclick", this.nodeDblClick, this);
        }
        if (this.clicksToSelect == 3 || this.clicksToSelect == 2) {
            this.on("nodedblclick", this.nodeDblClick, this);
        }

        if (this.clicksToEdit == 1) {
            this.on("nodeclick", this.beginEdit, this);
        } else {
            this.on('nodedblclick', this.beginEdit, this);
        }

        if (this.showCheckbox) {
            this.on("nodecheckclick", this.nodeCheckClick, this);
        }

        this.on("nodebeforeadd", this.nodeBeforeAdd, this);
        this.on("nodeexpandcollapseclick", this.nodeExpandCollapseClick, this);

        this.on("nodebeforeremove", this.nodeBeforeRemove, this);
        this.on("nodebeforeloaddata", this.nodeBeforeLoadData, this);
        if (this.draggable) {
            this.on("noderender", this.dragNode, this);
        }
    },
    dragNode: function (node) {
        var opts = this;

        node.node.UCML_draggable({ disabled: false, revert: true, cursor: "pointer", proxy: function (_19) {
            var p = $("<div class=\"tree-node-proxy tree-dnd-no\"></div>").appendTo("body");
            p.html($(_19).find(".tree-title").html());
            p.hide();
            return p;
        }, deltaX: 15, deltaY: 15, onStartDrag: function () {
            $(this).draggable("proxy").css({ left: -10000, top: -10000 });
        }, onDrag: function (e) {
            $(this).draggable("proxy").show();
            this.pageY = e.pageY;
        }
        }).UCML_droppable({ accept: "div.tree-node", onDragOver: function (e, _1a) {
            var _1b = _1a.pageY;
            var top = $(this).offset().top;
            var _1c = top + $(this).outerHeight();
            $(_1a).draggable("proxy").removeClass("tree-dnd-no").addClass("tree-dnd-yes");
            $(this).removeClass("tree-node-append tree-node-top tree-node-bottom");
            if (_1b > top + (_1c - top) / 2) {
                if (_1c - _1b < 5) {
                    $(this).addClass("tree-node-bottom");
                } else {
                    $(this).addClass("tree-node-append");
                }
            } else {
                if (_1b - top < 5) {
                    $(this).addClass("tree-node-top");
                } else {
                    $(this).addClass("tree-node-append");
                }
            }
        }, onDragLeave: function (e, _1d) {
            $(_1d).draggable("proxy").removeClass("tree-dnd-yes").addClass("tree-dnd-no");
            $(this).removeClass("tree-node-append tree-node-top tree-node-bottom");
        }, onDrop: function (e, nodeEl) {
            var targetEl = this;
            var dropFn, dropPosition;
            if ($(this).hasClass("tree-node-append")) {
                dropFn = dropAppendNode;
            } else {
                dropFn = dropInsertBeforeNode;
                dropPosition = $(this).hasClass("tree-node-top") ? "top" : "bottom";
            }
            var nodeId = $(nodeEl).attr("nodeId");

            var node = opts.getNode(nodeId);

            var targetNodeId = $(targetEl).attr("nodeId");

            var targetNode = opts.getNode(targetNodeId);

            if (targetNode && node) {
                setTimeout(function () {
                    dropFn(node, targetNode, dropPosition);
                }, 0);
            }

            $(this).removeClass("tree-node-append tree-node-top tree-node-bottom");
        }
        });

        function dropSetNode(node, targetNode) {
            var newNode = new UCML.TreeNode(node.attributes);
            var cs = node.childNodes;
            newNode.childNodes = cs;
            newNode.ownerTree = targetNode.ownerTree;
            for (var i = 0; i < cs.length; i++) {
                cs[i].parentNode = newNode;
            }
            node.remove();
            return newNode;
        }

        function setDropDataParent(node, id) {
            var dataTalbe = node.ownerTree.dataTable;

            var index = node.ownerTree.dataTable.GetGuidBy_Index(node.id);
            if (index > 0) {
                dataTalbe.SetIndexNoEvent(index);
                dataTalbe.setFieldValue(node.ownerTree.ParentFieldName, dataTalbe.GetOIDBy_GUID(id));
            }
            dataTalbe.LocateOID(id);
        }

        function dropAppendNode(node, targetNode) {
            var newNode = dropSetNode(node, targetNode);
            targetNode.appendChild(newNode);
            setDropDataParent(newNode, targetNode.id);
        }

        function dropInsertBeforeNode(node, targetNode, dropPosition) {
            var newNode = dropSetNode(node, targetNode);
            if (dropPosition == "top") {
                targetNode.parentNode.insertBefore(newNode, targetNode);
            }
            else {
                targetNode.parentNode.insertBefore(newNode, targetNode.nextSibling);
            }
            setDropDataParent(newNode, targetNode.parentNode.id);
        }
    },
    onFieldChange: function (e) {
        if (e.fieldName == this.TextFieldName) {
            var OID = this.dataTable.OID_GUID();
            var node = this.getNode(OID);
            if (node) {
                node.setText(e.value);
            }
        }
    },
    onRecordChange: function (e, sender) {
        if (sender == this) {
            return;
        }
        var OID = this.dataTable.OID_GUID();
        var node = this.getNode(OID);
        if (node) {
            this.setFocusNode(node);
        }
    }
    ,
    setSelectNode: function (node) {
        if (this.selectMode == 1) {//如果单个选择模式
            if (this.selectNode) {
                this.selectNode.unSelect();
                this.selectNode = null;
            }
            node.select();
            this.selectNode = node;
        } else if (this.selectMode == 2) {
            if (node.selected) {
                node.unSelect();
                this.selectNode.remove(node);
            }
            else {
                node.select();
                this.selectNode.add(node);
            }
        }
    },
    nodeDblClick: function (node) {
        this.setFocusNode(node);

        this.setSelectNode(node);

        if (this.selectForToggle) {
            node.toggle();
        }
        if (this.selectForCheck && this.showCheckbox) {
            this.nodeCheckClick(node);
        }

        this.fireEvent("change", node);
    }
    ,
    beginEdit: function (node) {
        if (this.enabledEdit == false || node == this.root) {
            return false;
        }

        if (this.dataTable.IsRecordOwner() == false) {
            alert(UCML.Languge.BCnotRightEdit);
            return false;
        }

        this.endEdit();
        var el = node.node.children('.tree-title');
        var obj = new Object();
        obj.rendeTo = this.editBody;
        obj.dataTable = node.dataTable || this.dataTable;
        obj.fieldName = this.TextFieldName;
        obj.value = el.html();

        obj.ctype = "UCML.TextBox";
        var dataCell = UCML.create(obj, obj.ctype);
        var offset = el.offset();
        var top = offset.top;
        var left = offset.left;
        dataCell.setOffset(top, left);
        dataCell.setValue(obj.value);

        dataCell.on("blur", this.endEdit, this);
        // dataCell.addClass("datagrid-editable-input");

        var width = this.el.outerWidth() - left;
        var height = el.outerHeight();

        this.activeEditor = dataCell;
        this.activeEditor.node = node;
        dataCell.resize(width, height);

        if (this.activeEditor.focus) {
            this.activeEditor.focus();
        }
        return dataCell;
    }
    ,
    endEdit: function () {
        if (this.activeEditor) {
            var value = this.activeEditor.getValue();
            this.activeEditor.node.setText(value);
            this.dataTable.setFieldValue(this.TextFieldName, value);
            this.activeEditor.destroy();
            delete this.activeEditor;
        }
    },
    nodeCheckClick: function (node) {
        this.setFocusNode(node);

        var pn = node.parentNode;

        var checked = node.checked === false;

        node.toggleCheck(checked);

        if (this.checkboxMode == -1) {
            return;
        }
        //复选框模式  0默认 ,1 子集联选，2 父级联选,3 子父联选, -1不联选
        if (this.checkboxMode == 0) {// 0默认
            this.setParentCheckbox2(node);
        }
        else if (this.checkboxMode == 2) {
            this.setParentCheckbox(node, checked);
        }
        else if (this.checkboxMode == 3) {
            this.setParentCheckbox3(node);
        }

        if (this.checkboxMode == 0 || this.checkboxMode == 1 || this.checkboxMode == 3) {
            this.setChildCheckbox(node, checked);
        }
    }
    ,
    nodeExpandCollapseClick: function (node) {
        node.toggle();
    }
    ,
    //根据oid获取树的节点信息
    getNode: function (OID) {
        return this.nodes[OID];
    }
    ,
    registerNode: function (node) {
        this.nodes[node.id] = node;
    },
    unregisterNode: function (node) {
        delete this.nodes[node.id];
    }
    ,
    nodeBeforeAdd: function (node) {
        if (this.dataTable && node.readStatus == false) {
            //            var pOID = this.dataTable.getOID();
            //            this.dataTable.Insert();
            //            node.id = this.dataTable.getOID();
            //            this.dataTable.setFieldValue(this.TextFieldName, node.text);
            //            this.dataTable.setFieldValue(this.ParentFieldName, pOID);
        }
    },
    nodeBeforeRemove: function (node) {
        //      this.removeChildData(node);
        //      this.dataTable.LocateOID(node.id);
        //      this.dataTable.Delete();
    }
    ,
    deleteNode: function () {
        if (this.focusNode) {
            var pNode = this.focusNode.parentNode;
            if (this.dataTable.enabledDelete == false) return;

            var nextSibling = this.focusNode.nextSibling;
            var previousSibling = this.focusNode.previousSibling;
            this.focusNode.remove();
            this.removeChildData(this.focusNode);
            this.dataTable.LocateOID(this.focusNode.id, this);
            this.dataTable.Delete();
            //            if (this.ParentFieldName != "NONE")
            //                this.dataTable.DeleteAllChild(this.ParentFieldName, this.focusNode.id);
            //            else {
            //                this.dataTable.Delete();
            //            }

            if (this.nodes[this.focusNode.id]) {

                this.nodes[this.focusNode.id] = null;
            }

            if (nextSibling) {
                this.setFocusNode(nextSibling);
            }
            else if (previousSibling) {
                this.setFocusNode(previousSibling);
            }
            else if (pNode && pNode.id != this.rootId) {
                this.setFocusNode(pNode);
            }
            else {
                this.setFocusNode(null);

            }


        }
        else {
            alert(UCML.Languge.Tree_DeleteCheckNode);
        }
    }
    ,
    nodeBeforeLoadData: function (node) {

        this.BusinessObject.getDynTreeSubNodes(this.dataTable.BCName, this.ParentFieldName, node.id, this.TextFieldName, this.loadChildData, function (o) { alert("节点数据加载错误") }, node, false);
    }
    ,
    rebuildWhereTreeNodes: function (condi) {
        condi = condi || "";
        this.BusinessObject.getReBuildWhereTreeNodes(this.dataTable.BCName, this.ParentFieldName, this.rootId, this.TextFieldName, condi, function (result, text) {
            var data = eval(text);
            var nodeDatas = data[0]; //树节点数据
            var BCDatas = data[1]; //bc数据


            this.dataTable.claerChildData();
            BusinessData[this.dataTable.BCName] = BCDatas;
            this.dataTable.LoadResults(BCDatas);

            this.clearNode();

            for (var n = 0; n < nodeDatas.length; n++) {
                var nodeData = nodeDatas[n]; //得到BC
                if (nodeData) {
                    var nodes = nodeData.value;
                    var strBCBase = nodeData.BCName + "Base";
                    var bcBase = eval(strBCBase);
                    for (var i = 0; i < nodes.length; i++) {

                        bindChildNode(this.root, bcBase, nodes[i]);
                    }
                }
            }

            function bindChildNode(pNode, bcBase, node, data) {
                var node;
                if (node.ifHavaChild == "true") {
                    node = new UCML.AsyncTreeNode({ text: node.text, id: node.id, readStatus: true });
                    node.dataTable = bcBase;
                    pNode.appendChild(node);

                }
                else {
                    node = new UCML.TreeNode({ text: node.text, id: node.id, readStatus: true });
                    node.dataTable = bcBase;
                    pNode.appendChild(node);
                }
            }

        }, function (o) { alert(UCML.Languge.Tree_QueryNodeError) }, this, false);
    }
   ,
    loadChildData: function (result, text) {
        var data = eval(text);
        var nodeDatas = data[0]; //树节点数据
        var BCDatas = data[1]; //bc数据
        this.ownerTree.setBCDatas(BCDatas);
        for (var n = 0; n < nodeDatas.length; n++) {
            var nodeData = nodeDatas[n]; //得到BC
            if (nodeData) {
                var nodes = nodeData.value;
                var strBCBase = nodeData.BCName + "Base";
                var bcBase = eval(strBCBase);
                for (var i = 0; i < nodes.length; i++) {

                    bindChildNode(this, bcBase, nodes[i]);
                }
            }
        }

        function bindChildNode(pNode, bcBase, node, data) {
            var node;
            if (node.ifHavaChild == "true") {
                node = new UCML.AsyncTreeNode({ text: node.text, id: node.id, readStatus: true });
                node.dataTable = bcBase;
                pNode.appendChild(node);

            }
            else {
                node = new UCML.TreeNode({ text: node.text, id: node.id, readStatus: true });
                node.dataTable = bcBase;
                pNode.appendChild(node);
            }
        }


        this.loading = false;
        this.loaded = true;
        this.expand();

    },
    bindChildNode: function (bcBase, node, data) {

        var node;
        if (node.ifHavaChild == "true") {
            node = new UCML.AsyncTreeNode({ text: node.text, id: node.id, readStatus: true });
            this.appendChild(node);
        }
        else {
            node = new UCML.TreeNode({ text: node.text, id: node.id, readStatus: true });
            this.appendChild(node);
        }
    }
    ,
    setBCDatas: function (BCDatas) {
        for (var i = 0; i < BCDatas.length; i++) {
            var bcObj = BCDatas[i];
            var bcName = bcObj.BCName;
            var strBCBase = bcName + "Base";



            var bcBase = eval(strBCBase); //如果前端存在则处理

            //      var oldIndex = bcBase.recordIndex;

            var bcValue = bcObj.value;
            for (var j = 0; j < bcValue.length; j++) {
                var bcD = bcValue[j];
                bcBase.addRecord(bcD);
            }

            //    bcBase.recordIndex = oldIndex;
        }
    }
    ,
    getNodeById: function (id) {
        return this.nodes[id];
    }
    ,
    setFocusNode: function (node) {
        if (this.focusNode == node) {
            return;
        }

        if (this.focusNode) {
            this.focusNode.node.removeClass('tree-node-selected');
            if (node) {
                node.node.addClass('tree-node-selected');
            }
        }

        if (this.dataTable && node) {
            this.dataTable.LocateOID(this.dataTable.GetOIDBy_GUID(node.id), this);
        }
        this.focusNode = node;
    }
    ,
    expandAll: function () {

    },
    collapseAll: function () {

    },
    //清除掉已经选中的树节点
    clearCheckNode: function () {
        while (this.checkNodes.length > 0) {
            this.checkNodes[0].setCheck(false);
        }
        this.checkNodes = [];
    },
    clearNode: function () {
        for (n in this.nodes) {
            if (this.nodes[n].remove) {
                this.nodes[n].remove();
            }
        }
        this.nodes = {};
    },
    setChildCheckbox: function (node, checked) {
        for (var i = 0; i < node.childNodes.length; i++) {
            node.childNodes[i].toggleCheck(checked);
            this.setChildCheckbox(node.childNodes[i], checked);
        }
    }
    ,
    removeChildData: function (node) {
        for (var i = 0; i < node.childNodes.length; i++) {
            if (this.dataTable) {
                this.dataTable.LocateOID(node.childNodes[i].id);
                this.dataTable.Delete();
            }
            this.removeChildData(node.childNodes[i]);
        }
    }
    ,
    setParentCheckbox: function (node, checked) {
        var pn = node.parentNode;
        if (pn) {
            pn.toggleCheck(checked);
            this.setParentCheckbox(pn, checked);
        }
    }
    ,
    setParentCheckbox2: function (node) {
        var pn = node.parentNode;
        if (pn) {
            if (isAllSelected(node)) {
                pn.toggleCheck(true);
            } else if (isAllNull(node)) {
                pn.toggleCheck(false);
            } else {
                pn.toggleCheck(false);
                pn.updateCheckIcon('tree-checkbox2');
            }
            this.setParentCheckbox2(pn);
        }

        function isAllSelected(n) {
            var b = true;
            for (var i = 0; i < pn.childNodes.length; i++) {
                if (!pn.childNodes[i].checked) {
                    return false;
                }
            }
            return b;
        }
        function isAllNull(n) {
            var b = true;
            for (var i = 0; i < pn.childNodes.length; i++) {
                if (pn.childNodes[i].checked) {
                    return false;
                }
            }
            return b;
        }
    }
    ,
    setParentCheckbox3: function (node) {
        var pn = node.parentNode;
        if (pn) {
            if (isAllNull(node)) {
                pn.toggleCheck(false);
            } else {
                pn.toggleCheck(true);
            }
            this.setParentCheckbox3(pn);
        }
        function isAllNull(n) {
            var b = true;
            for (var i = 0; i < pn.childNodes.length; i++) {
                if (pn.childNodes[i].checked) {
                    return false;
                }
            }
            return b;
        }
    }
    ,
    addChild: function (text) {
        //  if (this.enabledEdit == false) return;

        if (this.focusNode) {
            var ParentOID = this.focusNode.id;
            ParentOID = this.dataTable.GetOIDBy_GUID(ParentOID);

            this.dataTable.Insert();
            this.dataTable.setFieldValue(this.TextFieldName, text);
            this.dataTable.setFieldValue(this.ParentFieldName, ParentOID);

            var node = new UCML.TreeNode({ text: text, id: this.dataTable.OID_GUID() });
            this.focusNode.appendChild(node);
            this.focusNode.expand();
            return node;
        }
        else {
            alert(UCML.Languge.Tree_CheckRootNode);
            return null;
        }
    },
    //在某个节点下添加子节点
    //add by:zyn;add time:20130722
    addChildByNode: function (node, text) {
        if (node) {
            this.setFocusNode(node);
        }
        this.addChild(text);
    },
    addRoot: function (text) {
        //   if (this.enabledEdit == false) return;

        var keyFiledType = this.dataTable.getColumn(this.dataTable.PrimaryKey).fieldType;
        var ParentOID;
        if (keyFiledType == "UCMLKey" || keyFiledType == "Guid")
            ParentOID = this.dataTable.getEmptyGuid();
        else
            ParentOID = '0';

        this.dataTable.Insert();
        this.dataTable.setFieldValue(this.TextFieldName, text);
        this.dataTable.setFieldValue(this.ParentFieldName, ParentOID);

        var node = new UCML.TreeNode({ text: text, id: this.dataTable.OID_GUID() });

        this.root.appendChild(node);
        return node;
    },
    loadData: function (nodes) {
        var beginTime = new Date();
        this.clearNode();

        try {
            this.dataNodes = eval(this.id + 'Nodes');
        }
        catch (e) {
            this.dataNodes = null;
        }
        if (this.dataNodes) {
            this.loadDataNodes(this.root, this.dataNodes);
        }
        else {
            if (this.fSubTableTreeMode == "true")//如果是子表树，动态
            {
                this.childFilter = eval(this.dataTable.BCName + '_ChildInfo');

                this.childFilter = this.childFilter || [];

                this.loadSubTreeNodes();
            } else if (this.fSubTableTreeMode == "false" && this.ifDynamicCreateTree == "true")//如果非子表树，却是动态树
            {
                try {
                    this.BCTreeData = true;
                    this.childFilter = eval(this.dataTable.BCName + '_ChildInfo');
                } catch (e) {
                    this.childFilter = [];
                }

                if (this.childFilter.length == 0) {
                    try {
                        this.childFilter = eval(this.id + 'ChildFilter') || [];

                    } catch (e) {
                        this.childFilter = [];
                    }
                }

                this.loadDynTreeNodes();
            } else//非子表树，非动态==>静态树
            {
                var opts = this;
                setTimeout(function () {
                    opts.loadStaticTreeNodes();
                }, 100);
            }
        }
        //     alert(new Date() - beginTime);
    }
    ,
    //根据节点数据装载节点
    loadDataNodes: function (baseNode, dataNodes) {
        for (var i = 0; i < dataNodes.length; i++) {
            if (UCML.isObject(dataNodes[i])) {
                var node = new UCML.TreeNode({ text: dataNodes[i].text, id: dataNodes[i].id, checked: dataNodes[i].checked, imageUrl: dataNodes[i].imageUrl, readStatus: true });
                if (dataNodes[i].children.length > 0) {
                    node.leaf = false;
                }
                baseNode.appendChild(node);
                if (!node.leaf) {
                    this.loadDataNodes(node, dataNodes[i].children);
                }
            }
        }
        this.dataTable.SetIndexNoEvent(0);
    },
    loadStaticTreeNodes: function () {
        var rowCount = this.dataTable.getRecordCount();
        for (var i = 0; i < rowCount; i++) {
            this.dataTable.SetIndexNoEvent(i);
            var parentValue = this.dataTable.getFieldValue(this.ParentFieldName);
            var primaryKeyValue = this.dataTable.getFieldValue(this.dataTable.PrimaryKey);
            var textFieldNameValue = this.dataTable.getFieldValue(this.TextFieldName);
            if (parentValue == this.rootId || parentValue == null) {
                var node = new UCML.TreeNode({ text: textFieldNameValue, id: primaryKeyValue, readStatus: true });
                this.root.appendChild(node);
                this.findchildren(primaryKeyValue, node);
            }
        }
        this.dataTable.SetIndexNoEvent(0);
    }
    ,
    loadDynTreeNodes: function () {
        var rowCount = this.dataTable.getRecordCount();
        for (var i = 0; i < rowCount; i++) {
            this.dataTable.SetIndexNoEvent(i);
            var parentValue = this.dataTable.getFieldValue(this.ParentFieldName);
            var primaryKeyValue = this.dataTable.getFieldValue(this.dataTable.PrimaryKey);
            var textFieldNameValue = this.dataTable.getFieldValue(this.TextFieldName);
            var node = { text: textFieldNameValue, id: primaryKeyValue, readStatus: true };
            if (this.hasChild(primaryKeyValue)) {
                node = new UCML.AsyncTreeNode(node);
                node.dataTable = this.dataTable;
                this.root.appendChild(node);
            }
            else {
                node = new UCML.TreeNode(node);
                node.dataTable = this.dataTable;
                this.root.appendChild(node);
            }
        }
        this.dataTable.SetIndexNoEvent(0);
    }
    ,
    hasChild: function (id) {

        if (this.BCTreeData) {

            var flag = false;
            for (var i = 0; i < this.childFilter.length; i++) {
                if (this.childFilter[i] == id) {

                    flag = true;
                    break;

                }
            }
            return flag;

        }
        else {
            var flag = false;
            for (var i = 0; i < this.childFilter.length; i++) {
                if (this.childFilter[i][0] == id) {
                    if (this.childFilter[i][1] == 1) {
                        flag = true;
                        break;
                    }
                }
            }
            return flag;
        }
    }
    ,
    findchildren: function (v, node) {
        var recordCount = this.dataTable.getRecordCount();
        //有子节点
        // if (this.haveChildren(v)) {
        for (var i = 0; i < recordCount; i++) {
            this.dataTable.SetIndexNoEvent(i);
            var parentFieldNameValue = this.dataTable.getFieldValue(this.ParentFieldName);
            var primaryKeyValue = this.dataTable.getFieldValue(this.dataTable.PrimaryKey);
            var textFieldNameValue = this.dataTable.getFieldValue(this.TextFieldName);
            if (parentFieldNameValue == v) {
                var child = new UCML.TreeNode({ text: textFieldNameValue, id: primaryKeyValue, readStatus: true });
                node.appendChild(child);
                //递归添加子节点
                this.findchildren(primaryKeyValue, child);
            }
        }
        //  }
    }
    ,
    haveChildren: function (v) {
        var recordCount = this.dataTable.getRecordCount();
        var flag = false;
        //查找是否有子节点
        for (var i = 0; i < recordCount; i++) {
            this.dataTable.SetIndexNoEvent(i);
            var parentFieldNameValue = this.dataTable.getFieldValue(this.ParentFieldName);
            if (parentFieldNameValue == v) {
                flag = true;
                break;
            }
        }
        return flag;
    },
    getCheckNode: function () {
        return this.checkNodes;
    },
    getSelectNode: function () {
        return this.selectNode;
    },
    getFocusNode: function () {
        return this.focusNode;
    }
    , getNodeImg: function (node) {
        var src;
        if (node.isLeaf()) {
            src = this.leafImgSrc;
        }
        else if (node.expanded) {
            src = this.openImgSrc;
        }
        else {
            src = this.closeImgSrc;
        }
        return src;
    }
});


UCML.TreeNode = function (attributes) {
    this.attributes = attributes || {};
    //子节点（数组）
    this.childNodes = [];
    if (!this.childNodes.indexOf) { // indexOf is a must
        this.childNodes.indexOf = function (o) {
            for (var i = 0, len = this.length; i < len; i++) {
                if (this[i] == o) {
                    return i;
                }
            }
            return -1;
        };
    }

    //父节点
    this.parentNode = null;

    //第一个子节点
    this.firstChild = null;

    //最后一个子节点
    this.lastChild = null;

    //上一个节点
    this.previousSibling = null;

    //下一个节点
    this.nextSibling = null;

    if (typeof attributes == "string") {
        attributes = { text: attributes };
    }
    //    else {
    //        UCML.apply(this, attributes);
    //    }
    this.rendered = attributes.rendered === true;
    this.readStatus = attributes.readStatus === true;
    this.expanded = attributes.expanded === true;
    this.text = attributes.text;
    this.leaf = attributes.leaf == undefined ? true : attributes.leaf;
    this.id = this.attributes.id;
    this.selected = this.attributes.selected === true;
    this.imageUrl = this.attributes.imageUrl;

    if (!this.id) {
        this.id = UCML.id(null, "xnode-");
        this.attributes.id = this.id;
    }

    this.addEvents(
        "textchange",
        "beforeexpand",
        "beforecollapse",
        "expand",
        "disabledchange",
        "collapse",
        "beforeclick",
        "click",
        "checkchange",
        "dblclick",

        "contextmenu",

        "beforechildrenrendered"
    );
};
UCML.extend(UCML.TreeNode, UCML.util.Observable, {
    //根据索引获得当前节点的子节点
    item: function (index) {
        return this.childNodes[index];
    },
    //获取节点在父节点中的索引，child子节点，node为父节点
    indexOf: function (child) {
        return this.childNodes.indexOf(child);
    },
    //获得当前节点的深度（层级）
    getDepth: function () {
        var depth = 0;
        var p = this;
        while (p.parentNode) {
            p = p.parentNode;
            if (!p.isRoot || (p.isRoot && p.ownerTree.rootVisible)) {
                ++depth;
            }

        }
        return depth;
    },

    setId: function (id) {
        if (id !== this.id) {
            var t = this.ownerTree;
            if (t) {
                t.unregisterNode(this);
            }
            this.id = id;
            if (t) {
                t.registerNode(this);
            }
            this.onIdChange(id);
        }
    },
    getOwnerTree: function () {
        return this.ownerTree;
    }
    ,
    getPath: function (attr) {
        attr = attr || "id";
        var p = this.parentNode;
        var b = [this.attributes[attr]];
        while (p) {
            b.unshift(p.attributes[attr]);
            p = p.parentNode;
        }
        var sep = this.getOwnerTree().pathSeparator;
        return sep + b.join(sep);
    },
    toString: function () {
        return "[Node" + (this.id ? " " + this.id : "") + "]";
    }
    ,
    //是否叶子节点
    isLeaf: function () {
        return this.leaf;
    },
    //获取当前节点中的bc数据,name字段名称
    getBCValue: function (name, OID) {
        OID = OID || this.id;
        var dataTable = this.ownerTree.dataTable;
        if (dataTable && dataTable.dataStore) {
            var fieldIndex = dataTable.dataStore.columnIndex(name);
            if (fieldIndex > 0) {
                var row = dataTable.dataStore.bcData.item(OID);
                if (row) {
                    return row[fieldIndex];
                }
            }
        }
    }
    ,
    //设置当前节点bc数据，name字段名称,value值
    setBCValue: function (name, value, OID) {
        OID = OID || this.id;
        var dataTable = this.ownerTree.dataTable;
        if (dataTable && dataTable.dataStore) {
            var fieldIndex = dataTable.dataStore.columnIndex(name);
            if (fieldIndex > 0) {
                var row = dataTable.dataStore.bcData.item(OID);
                if (row) {
                    row[fieldIndex] = value;
                }
            }
        }
    }
    ,
    // private 是否第一个子节点
    setFirstChild: function (node) {
        this.firstChild = node;
    },

    //private是否最后一个子节点
    setLastChild: function (node) {
        this.lastChild = node;
    },
    //是否最后一个节点
    isLast: function () {
        return (!this.parentNode ? true : this.parentNode.lastChild == this);
    },
    //是否第一个节点
    isFirst: function () {
        return (!this.parentNode ? true : this.parentNode.firstChild == this);
    },
    //是否包含子节点
    hasChildNodes: function () {
        return !this.isLeaf() && this.childNodes.length > 0;
    },
    isExpandable: function () {
        return this.attributes.expandable || this.hasChildNodes();
    },
    //是否已经展开
    isExpanded: function () {
        return this.expanded;
    },
    sort: function (fn, scope) {
        var cs = this.childNodes;
        var len = cs.length;
        if (len > 0) {
            var sortFn = scope ? function () { fn.apply(scope, arguments); } : fn;
            cs.sort(sortFn);
            for (var i = 0; i < len; i++) {
                var n = cs[i];
                n.previousSibling = cs[i - 1];
                n.nextSibling = cs[i + 1];
                if (i === 0) {
                    this.setFirstChild(n);
                }
                if (i == len - 1) {
                    this.setLastChild(n);
                }
            }
        }
    },
    appendChild: function (node) {
        this.ownerTree.fireEvent("nodebeforeadd", node);
        node.ownerTree = this.ownerTree;
        node.showCheckbox = node.showCheckbox || node.ownerTree.showCheckbox;
        node.checked = node.checked || node.ownerTree.checked;
        index = this.childNodes.length;
        if (index === 0) {
            this.setFirstChild(node);
        }
        this.childNodes.push(node);
        node.parentNode = this;
        var ps = this.childNodes[index - 1];
        if (ps) {
            node.previousSibling = ps;
            ps.nextSibling = node;
        } else {
            node.previousSibling = null;
        }
        node.nextSibling = null;
        this.setLastChild(node);


        if (this.rendered) {
            node.render();
            if (this.isLeaf()) {
                this.setLeaf(false);
            }
        }
        this.ownerTree.registerNode(node);
        this.ownerTree.fireEvent("nodeadd", node);
        return node;
    },
    insertBefore: function (node, refNode) {
        if (!refNode) {
            return this.appendChild(node);
        }
        if (node == refNode) {
            return false;
        }

        if (this.fireEvent("beforeinsert", this.ownerTree, this, node, refNode) === false) {
            return false;
        }
        var index = this.childNodes.indexOf(refNode);
        var oldParent = node.parentNode;
        var refIndex = index;

        if (oldParent == this && this.childNodes.indexOf(node) < index) {
            refIndex--;
        }

        if (oldParent) {
            if (node.fireEvent("beforemove", node.getOwnerTree(), node, oldParent, this, index, refNode) === false) {
                return false;
            }
            oldParent.removeChild(node);
        }
        if (refIndex === 0) {
            this.setFirstChild(node);
        }
        this.childNodes.splice(refIndex, 0, node);
        node.parentNode = this;
        var ps = this.childNodes[refIndex - 1];
        if (ps) {
            node.previousSibling = ps;
            ps.nextSibling = node;
        } else {
            node.previousSibling = null;
        }
        node.nextSibling = refNode;
        refNode.previousSibling = node;
        node.ownerTree = this.ownerTree;
        node.showCheckbox = node.showCheckbox || node.ownerTree.showCheckbox;
        node.checked = node.checked || node.ownerTree.checked;


        if (this.rendered) {
            node.render(this.subul, refNode.li);
            if (this.isLeaf()) {
                this.setLeaf(false);
            }
        }
        this.ownerTree.registerNode(node);
        this.ownerTree.fireEvent("nodeadd", node);

        this.fireEvent("insert", this.ownerTree, this, node, refNode);
        if (oldParent) {
            node.fireEvent("move", this.ownerTree, node, oldParent, this, refIndex, refNode);
        }
        return node;
    },
    updateExpandIcon: function () {
        if (this.rendered) {
            if (this.expanded) {
                this.expandCollapse.removeClass('tree-collapsed tree-collapsed-hover').addClass('tree-expanded');
                this.nodeIcon.addClass('tree-folder-open');
                var ul = this.subul;
                ul.css('display', 'block');
            }
            else {
                this.expandCollapse.removeClass('tree-expanded tree-expanded-hover').addClass('tree-collapsed');
                this.nodeIcon.removeClass('tree-folder-open');
                var ul = this.subul;
                ul.css('display', 'none');
            }
            var imgSrs = this.imageUrl || this.ownerTree.getNodeImg(this);
            if (imgSrs) {
                this.nodeIcon.css("background-image", "url(" + imgSrs + ")");
            }
        }
    },
    updateCheckIcon: function (v) {
        if (this.checkbox) {
            this.checkbox.removeClass().addClass('tree-checkbox ' + v);
        }
    },
    setLeaf: function (v) {
        if (this.rendered && this.node) {
            this.leaf = v;
            var opts = this;
            if (v) {
                this.nodeIcon.removeClass().addClass('tree-file');
                this.expandCollapse.unbind('.tree');
                this.expandCollapse.removeClass('tree-collapsed tree-collapsed-hover');
                this.expandCollapse.removeClass('tree-expanded tree-expanded-hover');

            }
            else {
                this.updateExpandIcon();
                this.nodeIcon.removeClass().addClass('tree-folder');
                this.expandCollapse.unbind('.tree').bind('click.tree', function () {
                    // opts.toggle();
                    opts.ownerTree.fireEvent("nodeexpandcollapseclick", opts);
                    return false;
                }).bind('mouseenter.tree', function () {
                    if (opts.expanded) {
                        $(this).addClass('tree-expanded-hover');
                    } else {
                        $(this).addClass('tree-collapsed-hover');
                    }
                }).bind('mouseleave.tree', function () {
                    if (opts.expanded) {
                        $(this).removeClass('tree-expanded-hover');
                    } else {
                        $(this).removeClass('tree-collapsed-hover');
                    }
                });
            }
            var imgSrs = this.ownerTree.getNodeImg(this);
            if (imgSrs) {
                this.nodeIcon.css("background-image", "url(" + imgSrs + ")");
            }
        }

    },
    remove: function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
        return this;
    },
    removeChild: function (node) {
        var index = this.childNodes.indexOf(node);
        if (index == -1) {
            return false;
        }
        if (this.ownerTree.fireEvent("nodebeforeremove", node) === false) {
            return false;
        }

        // remove it from childNodes collection
        this.childNodes.splice(index, 1);

        // update siblings
        if (node.previousSibling) {
            node.previousSibling.nextSibling = node.nextSibling;
        }
        if (node.nextSibling) {
            node.nextSibling.previousSibling = node.previousSibling;
        }

        // update child refs
        if (this.firstChild == node) {
            this.setFirstChild(node.nextSibling);
        }
        if (this.lastChild == node) {
            this.setLastChild(node.previousSibling);
        }

        // clear any references from the node
        node.parentNode = null;
        node.previousSibling = null;
        node.nextSibling = null;

        //     var ul = li.parent();
        node.li.remove();
        if (!this.hasChildNodes()) {
            this.setLeaf(true);
        }

        this.ownerTree.unregisterNode(node);

        //      this.fireEvent("remove", this.ownerTree, this, node);
        return node;
    },
    setText: function (text) {
        var oldText = this.text;
        this.text = text;
        this.attributes.text = text;
        if (this.rendered) {
            this.node.children('.tree-title').html(text);
        }
        this.fireEvent("textchange", this, text, oldText);
    },
    select: function () {
        this.toggleSelect(true);
    },
    unSelect: function () {
        this.toggleSelect(false);
    },
    isSelected: function () {
        return this.selected;
    },
    toggleSelect: function (v) {
        this.setSelect(v);
        if (v) {
            this.ownerTree.fireEvent("nodeselect", this);
        }
        else {
            this.ownerTree.fireEvent("nodeunselect", this);
        }
    }
    ,
    setSelect: function (v) {
        if (v) {
            this.node.addClass('tree-node-selected');
        }
        else {
            this.node.removeClass('tree-node-selected');
        }
        this.selected = v;
    }
    , check: function () {
        this.toggleCheck(true);
    }
    ,
    unCheck: function () {
        this.toggleCheck(false);
    }
    ,
    toggleCheck: function (v) {
        if (this.ownerTree.fireEvent("checkchange", this, v) === true) {
            this.setCheck(v);
        }
        if (v) {
            this.ownerTree.fireEvent("nodecheck", this);
        }
        else {
            this.ownerTree.fireEvent("nodeuncheck", this);
        }
    },
    setCheck: function (v) {
        v = v == undefined ? true : v;
        if (v) {
            this.updateCheckIcon('tree-checkbox1');
            if (this.ownerTree.checkNodes.indexOf(this) == -1) {
                this.ownerTree.checkNodes.add(this);
            }
        }
        else {
            this.updateCheckIcon('tree-checkbox0');
            this.ownerTree.checkNodes.remove(this);
        }
        this.checked = v;
    },
    expand: function () {

        if (this.isLeaf()) {
            return;
        }
        if (this.ownerTree.fireEvent("nodeexpand", this) === true) {
            this.expanded = true;
            this.updateExpandIcon();
            var ul = this.subul;

            if (this.ownerTree.animate) {
                ul.slideDown();
            } else {
                ul.css('display', 'block');
            }
        }
    },
    collapse: function () {
        if (this.isLeaf()) {
            return;
        }
        if (this.ownerTree.fireEvent("nodecollapse", this) === true) {
            this.expanded = false;
            this.updateExpandIcon();
            var ul = this.subul;
            if (ul.length > 0) {
                if (this.ownerTree.animate) {
                    ul.slideUp();
                } else {
                    ul.css('display', 'none');
                }
            }
        }
    },
    toggle: function () {
        if (this.expanded) {
            this.collapse();
        } else {
            this.expand();
        }
    },
    renderChildren: function (ct) {
        this.rendered = true;
        this.childrenRendered = true;
        this.subul = ct;
        var cs = this.childNodes;
        for (var i = 0, len = cs.length; i < len; i++) {
            cs[i].rendered = false;
            cs[i].childrenRendered = false;
            cs[i].render(ct);
        }
    },
    // private
    render: function (ct, targetLi) {
        if (!this.rendered) {
            if (this.ownerTree.fireEvent("nodebeforerender", this) === true) {
                this.rendered = true;
                var li = $('<li></li>');
                if (targetLi) {
                    li.insertBefore(targetLi);
                }
                else {
                    li.appendTo(ct || this.parentNode.subul);
                }

                var node = $('<div class="tree-node" nodeId=' + this.id + '></div>').appendTo(li);

                var subul = $('<ul style="display:none"></ul>').appendTo(li);

                $('<span class="tree-title"></span>').html(this.text).appendTo(node);

                var imgSrs = this.ownerTree.getNodeImg(this);
                var nodeIcon = $('<span class="tree-file"></span>');
                if (imgSrs) {
                    nodeIcon.css("background-image", "url(" + imgSrs + ")");
                }
                nodeIcon.prependTo(node);

                if (this.showCheckbox) {
                    var checkbox = $('<span ></span>');
                    checkbox.prependTo(node);

                    checkbox.unbind('.tree').bind('click.tree', function () {
                        opts.ownerTree.fireEvent("nodecheckclick", opts, this);
                        return false;
                    });

                    this.checkbox = checkbox;
                    this.setCheck(this.checked); //默认选中
                }

                var expandCollapse = $('<span class="tree-hit tree-indent"></span>');
                expandCollapse.prependTo(node);

                var indents = [];

                for (var j = 0; j < this.getDepth(); j++) {
                    indents.add('<span class="tree-indent"></span>');
                }
                $(indents.join('')).prependTo(node);


                this.expandCollapse = expandCollapse;
                this.nodeIcon = nodeIcon;
                this.li = li;
                this.subul = subul;
                this.node = node;

                if (this.childNodes.length > 0) {
                    this.leaf = false;
                }

                if (!this.childrenRendered) {
                    this.renderChildren(subul);
                }

                var opts = this;

                node.unbind('.tree').bind('dblclick.tree', function () {
                    return opts.ownerTree.fireEvent("nodedblclick", opts);
                }).bind('click.tree', function () {
                    opts.ownerTree.fireEvent("nodeclick", opts);
                }).bind('mouseenter.tree', function () {
                    $(this).addClass('tree-node-hover');
                    return false;
                }).bind('mouseleave.tree', function () {
                    $(this).removeClass('tree-node-hover');
                    return false;
                }).bind('contextmenu.tree', function () {
                    opts.ownerTree.nodeDblClick(opts); //增加右击选中节点
                    return opts.ownerTree.fireEvent("nodecontextmenu", opts);
                });

                this.ownerTree.fireEvent("noderender", this);

                if (this.selected) {
                    this.select();
                }

                if (!this.leaf) {
                    this.setLeaf(false);
                }

                if (this.expanded || this.getDepth() < this.ownerTree.expandDepth || this.ownerTree.expandDepth == -1) {
                    this.expand();
                }
            }

        }
    },
    destroy: function () {
        if (this.childNodes) {
            for (var i = 0, l = this.childNodes.length; i < l; i++) {
                this.childNodes[i].destroy();
            }
            this.childNodes = null;
        }
        if (this.ui.destroy) {
            this.ui.destroy();
        }
    }
});

UCML.AsyncTreeNode = function (attributes) {
    UCML.AsyncTreeNode.superclass.constructor.call(this, attributes);
    this.loaded = this.attributes.loaded === true;
    this.loading = false;
    this.leaf = false;
    this.addEvents('beforeload', 'load');
}

UCML.extend(UCML.AsyncTreeNode, UCML.TreeNode, {
    expand: function () {
        if (this.loading) {
            return;
        }
        if (this.loaded == false) {
            this.nodeIcon.addClass('tree-loading');
            this.ownerTree.fireEvent("nodebeforeloaddata", this);
            if (this.fireEvent("beforeload", this) === false) {
                return;
            }
            this.loading = true;
            this.fireEvent("load", this);
            this.loading = false;
            return;
        }
        else {
            this.nodeIcon.removeClass('tree-loading');
        }
        UCML.AsyncTreeNode.superclass.expand.call(this);
    }
    ,
    isLoading: function () {
        return this.loading;
    },
    isLoaded: function () {
        return this.loaded;
    },
    hasChildNodes: function () {
        if (!this.isLeaf() && !this.loaded) {
            return true;
        } else {
            return UCML.AsyncTreeNode.superclass.hasChildNodes.call(this);
        }
    }
    ,
    reload: function () {
        this.collapse();
        while (this.firstChild) {
            this.removeChild(this.firstChild);
        }
        this.loaded = false;
        this.expand();
    }
}
);

UCML.SubTableTree = function (id) {
    this.BCTreeParams = [];
    UCML.SubTableTree.superclass.constructor.call(this, id);
}
//子表树
UCML.extend(UCML.SubTableTree, UCML.Tree, {
    loadData: function () {
        this.clearNode();
        this.childFilter = eval(this.id + 'ChildFilter') || [];
        this.initBCTreeParams();
        this.loadDynTreeNodes();


    },
    bindChildNode: function (bcBase, node) {
        var node;
        if (node.ifHavaChild == "true") {
            node = new UCML.AsyncTreeNode({ text: node.text, id: node.id, readStatus: true });
            node.dataTable = bcBase;
            this.appendChild(node);
        }
        else {
            node = new UCML.TreeNode({ text: node.text, id: node.id, readStatus: true });
            node.dataTable = bcBase;
            this.appendChild(node);
        }
    }
    ,
    //重写addRoot方法
    //节点必须要设置dataTable
    addRoot: function (text) {
        var keyFiledType = this.dataTable.getColumn(this.dataTable.PrimaryKey).fieldType;
        var ParentOID;
        if (keyFiledType == "UCMLKey" || keyFiledType == "Guid")
            ParentOID = this.dataTable.getEmptyGuid();
        else
            ParentOID = '0';

        this.dataTable.Insert();
        this.dataTable.setFieldValue(this.TextFieldName, text);
        this.dataTable.setFieldValue(this.ParentFieldName, ParentOID);

        var node = new UCML.TreeNode({ text: text, id: this.dataTable.OID_GUID() });
        node.dataTable = this.dataTable; //子表必须要设置dataTable
        this.root.appendChild(node);
        return node;
    },
    //重写addChild方法
    //增加子BC数据，有外键值，父键值为空GUID
    addChild: function (text) {
        if (this.focusNode) {
            var FkOID = this.focusNode.id;
            FkOID = this.focusNode.dataTable.GetOIDBy_GUID(FkOID);

            var subDataTable = this.focusNode.dataTable.theDetailTables[0];
            if (subDataTable) {
                var subTreeParam = this.BCTreeParams[subDataTable.BCName];

                subDataTable.Insert();
                subDataTable.setFieldValue(subTreeParam.TextFieldName, text);
                subDataTable.setFieldValue(subTreeParam.ParentFieldName, subDataTable.getEmptyGuid());
                subDataTable.setFieldValue(subTreeParam.SubFKField, FkOID);

                var node = new UCML.TreeNode({ text: text, id: subDataTable.OID_GUID() });
                node.dataTable = subDataTable;
                this.focusNode.appendChild(node);
                this.focusNode.expand();
                return node;
            } else {
                alert(UCML.Languge.Tree_HaveNodeChild);
            }
        } else {
            this.addRoot(text);
        }
    },
    //添加子数据方法
    //增加子层数据，根级BC层没有外键值，子层BC外键值为其上一层BC所在节点主键，只有父键值
    addChildSelf: function (text) {
        if (this.focusNode) {
            var ParentOID = this.focusNode.id;
            ParentOID = this.focusNode.dataTable.GetOIDBy_GUID(ParentOID);

            var selfTreeParam = this.BCTreeParams[this.focusNode.dataTable.BCName];

            this.focusNode.dataTable.Insert();
            this.focusNode.dataTable.setFieldValue(selfTreeParam.TextFieldName, text);
            this.focusNode.dataTable.setFieldValue(selfTreeParam.ParentFieldName, ParentOID);
            //this.focusNode.dataTable.setFieldValue(selfTreeParam.SubFKField, FkOID);
            var levelBC = selfTreeParam.levelBC;
            if (levelBC != 0) { //假如不是第一层，则需要找外键               
                var parentNode = this.focusNode.parentNode;
                while (levelBC == this.BCTreeParams[parentNode.dataTable.BCName].levelBC) {
                    parentNode = parentNode.parentNode;
                }
                this.focusNode.dataTable.setFieldValue(selfTreeParam.SubFKField, parentNode.id);
            }

            var node = new UCML.TreeNode({ text: text, id: this.focusNode.dataTable.OID_GUID() });
            node.dataTable = this.focusNode.dataTable;
            this.focusNode.appendChild(node);
            this.focusNode.expand();
            return node;
        } else {
            this.addRoot(text);
        }
    },
    //重写
    //通过节点增加子节点
    //{param} node:节点对象，mode:增加类型0-添加子级BC数据  其他增加本BC子数据，text：文本
    addChildByNode: function (node, mode, text) {
        if (node) {
            this.setFocusNode(node);
        }
        if (mode == 0) {
            this.addChild(text);
        } else {
            this.addChildSelf(text);
        }
    },
    //重写endEdit方法，主要是获得焦点节点的dataTable，进行数据定位
    endEdit: function () {
        if (this.activeEditor) {
            var value = this.activeEditor.getValue();
            this.activeEditor.node.setText(value);
            this.activeEditor.node.dataTable.LocateOID(this.activeEditor.node.id);
            this.activeEditor.node.dataTable.setFieldValue(this.BCTreeParams[this.activeEditor.node.dataTable.BCName].TextFieldName, value);
            this.activeEditor.destroy();
            delete this.activeEditor;
        }
    },
    //重写deleteNode方法
    deleteNode: function () {
        if (this.focusNode) {
            if (this.focusNode.dataTable.enabledDelete == false) return;

            var nextSibling = this.focusNode.nextSibling;
            var previousSibling = this.focusNode.previousSibling;
            var parentNode = this.focusNode.parentNode;
            this.focusNode.remove();
            this.removeChildData(this.focusNode);
            this.focusNode.dataTable.LocateOID(this.focusNode.id, this);
            this.focusNode.dataTable.Delete();
            //            if (this.ParentFieldName != "NONE")
            //                this.dataTable.DeleteAllChild(this.ParentFieldName, this.focusNode.id);
            //            else {
            //                this.dataTable.Delete();
            //            }

            if (this.nodes[this.focusNode.id]) {

                this.nodes[this.focusNode.id] = null;
            }

            if (nextSibling) {
                this.setFocusNode(nextSibling);
            }
            else if (previousSibling) {
                this.setFocusNode(previousSibling);
            }
            else if (parentNode && parentNode.id != this.rootId) {
                this.setFocusNode(parentNode);
            }
            else {
                this.setFocusNode(null);

            }
        }
        else {
            alert(UCML.Languge.Tree_DeleteCheckNode);
        }
    },
    //重写删除子节点数据
    removeChildData: function (node) {
        for (var i = 0; i < node.childNodes.length; i++) {
            if (node.childNodes[i].dataTable) {
                node.childNodes[i].dataTable.LocateOID(node.childNodes[i].id);
                node.childNodes[i].dataTable.Delete();
            }
            this.removeChildData(node.childNodes[i]);
        }
    },
    nodeBeforeLoadData: function (node) {
        var dataTable = node.dataTable ? node.dataTable : this.dataTable;
        var param = this.BCTreeParams[dataTable.BCName];
        this.BusinessObject.getTreeSubNodesWithChild(dataTable.BCName, this.id, param.ParentFieldName, node.id, param.TextFieldName, this.loadChildData, function (o, txt) {
            alert(txt);
        }, node, false);
    }
    , getNodeImg: function (node) {
        var src;
        var dataTable = node.dataTable ? node.dataTable : this.dataTable;
        var param = this.BCTreeParams[dataTable.BCName];
        if (param) {
            if (node.isLeaf()) {
                src = param.leafImgSrc;
            }
            else if (node.expanded) {
                src = param.openImgSrc;
            }
            else {
                src = param.closeImgSrc;
            }
            return src;
        }
    }
    ,
    initBCTreeParams: function () {

        var tOpen = "";
        var tClose = "";
        var tLeaf = "";
        if (this.imageFilePath != "" || this.imageFilePath != null) {
            var aryFimage = this.imageFilePath.split(";");
            tOpen = aryFimage[0];
            tClose = aryFimage[1] || tOpen;
            tLeaf = aryFimage[2] || tOpen;
        }
        this.LevelBCParams = {};

        this.BCTreeParams[this.dataTable.BCName] = { ParentFieldName: this.ParentFieldName, TextFieldName: this.TextFieldName, SubFKField: null, openImgSrc: tOpen, closeImgSrc: tClose, leafImgSrc: tLeaf, levelBC: 0 };
        if (this.SubBCs != null && this.SubBCs != "" && this.SubFKFields != null && this.SubFKFields != "") {
            var arySubFKFields = this.SubFKFields.split(";");
            var arySubBCs = this.SubBCs.split(";");
            var arySubParentFields = this.SubParentFields.split(";");
            var arySubPicFields = this.SubPicFields.split("@");
            var arySubLabelFields = this.SubLabelFields.split(";");
            for (var i = 0; i < arySubBCs.length; i++) {

                var bcName = arySubBCs[i];
                var strSubParentField = arySubParentFields[i] || "";
                var strSubFKField = arySubFKFields[i] || "";
                var strSubLabelField = arySubLabelFields[i] || "";
                var strSubPicFields = arySubPicFields[i] || "";
                var arySubPicField = strSubPicFields.split(";");
                var tOpen = arySubPicField[0];
                var tClose = arySubPicField[1] || tOpen;
                var tLeaf = arySubPicField[2] || tOpen;
                this.BCTreeParams[bcName] = { ParentFieldName: strSubParentField, TextFieldName: strSubLabelField, SubFKField: strSubFKField, openImgSrc: tOpen, closeImgSrc: tClose, leafImgSrc: tLeaf, levelBC: i + 1 };

                //this.BCTreeParams[bcName].levelBC = i + 1;
                this.LevelBCParams[i + 1] = bcName;
                /*
                if (this.dataTable.BCName != bcName) {
                var strBCBase = bcName + "Base";
                var bcBase = eval(strBCBase);
                if (bcBase) {
                bcBase.on("OnAfterInsert", this.insertNode, this);
                }
                }*/
            }
        }
    }
});



UCML.ComboxTree = function (id) {

    this.caption = "请选择";
    this.dataTextField = "";
    this.dataValueField = "";
    this.tarTextDataField = "";
    this.dataFld = "";
    this.parentNodeField = "";
    this.nodeNameField = "";

    UCML.ComboxTree.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ComboxTree, UCML.Combo, {
    panelWidth: 380,
    ctype: "UCML.ComboxTree",

    setProperties: function () {
        UCML.ComboxTree.superclass.setProperties.call(this);

        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute
("srcBCName")) || undefined;
        this.dataTextField = this.getAttribute("dataTextField") || this.dataTextField;
        this.dataValueField = this.getAttribute("dataValueField") || this.dataValueField;
        this.tarTextDataField = this.getAttribute("TarTextDataField") || this.tarTextDataField;
        this.dataFld = this.getAttribute("dataFld") || this.dataFld;
        this.parentNodeField = this.getAttribute("ParentNodeField") || this.parentNodeField;
        this.nodeNameField = this.getAttribute("NodeNameField") || this.nodeNameField;

    },
    onRender: function () {
        UCML.ComboxTree.superclass.onRender.call(this);
        UCML.un(this.el, "blur");
        UCML.on(this.el, "blur", function () {
            this.fireEvent("blur");
        }, this);

        this.setAttribute("readOnly", true);
        if (!this.tree) {
            this.tree = new UCML.Tree({ isSetProperties: false,
                rendeTo: this.panel.body

            });
            this.tree.on("change", this.change, this);
        }

        this.tree.dataTable = this.srcDataTable;
        this.tree.ParentFieldName = this.parentNodeField;
        this.tree.TextFieldName = this.nodeNameField;
        this.tree.haveScroll = true;
        this.tree.enabledEdit = false;
        this.tree.selectMode = 1;
        this.tree.rendeTo = this.panel.body;
        this.tree.BusinessObject = this.BusinessObject;
        this.tree.imageFilePath = "";
        this.tree.open();
        //   this.tree.BusinessObject.on("onAfterLoadData", this.onAfterLoadData, this);
    }
    ,
    onAfterLoadData: function () {
        if (this.tree) {
            this.tree.loadData();
        }
    },
    getText: function () {
        if (this.dataTable)//编辑VC模式
        {
            return this.dataTable.getFieldValue(this.tarTextDataField);
        }
        else//查询VC模式
        {
            return this.text;
        }
    }
    ,
    getValue: function () {
        return this.value;
    }
    ,
    setValue: function (value, trigger) {
        if (trigger !== false) {
            this.fireEvent("setValue", value);
        }
        this.value = value;

        if (this.value == "") {
            this.text = "";
        }
        this.setText(this.getText());
    }
    ,
    change: function (node) {
        this.text = this.srcDataTable.getFieldValue(this.dataTextField);
        var value = this.srcDataTable.getFieldValue(this.dataValueField);
        this.value = value;
        if (this.dataTable)//编辑VC模式
        {
            this.dataTable.setFieldValue(this.tarTextDataField, this.text, this);
            this.dataTable.setFieldValue(this.fieldName, this.value, this);
            this.setText(this.text);
        }
        else//查询VC模式
        {
            this.setValue(value);
        }
        this.blur();
    }
    ,
    arrowClick: function (el) {
        this.showPanel();
    },
    close: function () {
        UCML.ComboxTree.superclass.hidePanel.call(this);
    },
    bindEvents: function () {
        UCML.ComboxTree.superclass.bindEvents.call(this);
    }
});



UCML.reg("UCML.ComboxTree", UCML.ComboxTree);
