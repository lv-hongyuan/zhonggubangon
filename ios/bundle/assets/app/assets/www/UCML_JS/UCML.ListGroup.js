﻿/**
*	列表组，各种列表情况集合
*/
UCML.ListGroup = function (id) {
    /*
    列表种类: 
    singlecol--纯文本,
    multicol--多列,
    imageIcon--图片图标,
    checkgroup--复选框组,
    radiogroup--单选框组,
    imageDesc--图片说明
    */
    this.type = "singlecol";
    this.titleField; //显示标题文本字段
    this.descField; //详细描述字段
    this.extraTitleFields; //额外的显示文本字段,最多2个,2个则用分号隔开
    this.iconField; //图标字段
    this.srcDataTable; //数据来源BC
    this.totalField; //统计字段
    this.dividerField; //分组字段

    this.listviewEl;

    //列表允许滚动属性
    this.hasScroll = false;
    this.scrollEl;
    this.UCMLIScroll;

    this.hasMore = false;

    //查询控件属性
    this.hasSearch = false;
    this.searchObj;

    //选择列表属性
    this.hasSelectedList = false;
    this.selectedListEl;

    this.hasChecked = false;
    this.checkOID = [];

    this.addEvents("listclick", "imgclick", "deleteitemclick");

    UCML.ListGroup.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ListGroup, UCML.Commponent, {
    ctype: "UCML.ListGroup",
    hasNumber: false, //是否带序号,带序号则用ol,否则用ul
    inset: true, //是否有圆角,默认是true
    hasLinked: true, //是否有链接动作,true时则添加a标签,则需考虑点击动作怎么设置
    hasFilter: false, //是否添加过滤框
    filter_placeholder: "", //过滤框默认显示文本
    hasCount: false, //是否带统计数量标记, 配合totalField使用
    alignRight: false,
    setProperties: function () {
        UCML.ListGroup.superclass.setProperties.call(this);

        this.type = this.getAttribute("type") || this.type;

        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("srcBCName") || this.getAttribute("BCName") || this.srcBCName) || undefined;
        this.titleField = this.getAttribute("titleField") || this.getAttribute("dataFld") || this.titleField;
        this.totalField = this.getAttribute("totalField") || this.totalField;
        this.extraTitleFields = this.getAttribute("extralTitleFields") || this.extraTitleFields;
        this.iconField = this.getAttribute("iconField") || this.iconField;
        this.descField = this.getAttribute("descField") || this.descField;
        this.dividerField = this.getAttribute("dividerField") || this.dividerField;

        this.hasLinked = (UCML.isEmpty(this.getAttribute("hasLinked")) || this.getAttribute("hasLinked") == "false") ? false : this.hasLinked;
        this.hasNumber = this.getAttribute("hasNumber") == "true" ? true : this.hasNumber;
        this.inset = (UCML.isEmpty(this.getAttribute("inset")) || this.getAttribute("inset") == "false") ? false : this.inset;
        this.hasCount = this.getAttribute("hasCount") == "true" ? true : this.hasCount;

        this.alignRight = this.getAttribute("alignRight") == "true" ? true : this.alignRight;

        this.listviewEl = (this.hasNumber && this.hasNumber != 'false') ? $('<ol data-role="listview"' + ((this.inset) ? ' data-inset="true"' : ' data-inset="false"') + '>') : $('<ul data-role="listview"' + ((this.inset) ? ' data-inset="true"' : '  data-inset="false"') + '>');
        this.hasScroll = this.getAttribute("hasScroll") == "true" ? true : this.hasScroll;
        this.scrollEl = $('<div class="scroller"><div id="' + this.id + '_pullDown" class="pullDown"><span class="pullDownIcon"></span><span class="pullDownLabel">准备刷新...</span></div><div id="' + this.id + '_pullUp" class="pullUp"><span class="pullUpIcon"></span><span class="pullUpLabel">上拉加载更多...</span></div></div>');
        this.UCMLIScroll = new UCML.IScroll({
            pEl: this.el,
            ctl: this
            //,pullDownAction: this.pullDownActionCallback
        });

        this.hasSearch = this.getAttribute("hasSearch") == "true" ? true : this.hasSearch;

        if (this.hasSearch) {
            this.searchObj = new UCML.MSearch({
                srcDataTable: this.srcDataTable,
                searchField: this.titleField
            });
        }

        this.hasSelectedList = this.getAttribute("hasSelectedList") == "true" ? true : this.hasSelectedList;
        this.selectedListEl = $('<div id="selectedList"></div>');

        this.hasMore = this.getAttribute("hasMore") == "true" ? true : this.hasMore;
        this.clickLoadMore = $('<div class="click-load-more"><a href="javascript:void(0);">点击加载更多</a></div>');

        this.hasChecked = this.getAttribute("hasChecked") == "true" ? true : this.hasChecked;
    }
});

UCML.ListGroup.prototype.init = function () {
    UCML.ListGroup.superclass.init.call(this);

    if (this.hasSearch) {
        this.el.before(this.searchObj.el);
    }

    if (this.hasSelectedList && this.type == "checkgroup") {
        this.el.before(this.selectedListEl);
    }

    if (this.hasMore) {
        this.el.after(this.clickLoadMore);
    }

    this.setScrollTop();
}

UCML.ListGroup.prototype.setScrollTop = function () {
    var prevs = this.el.prevAll();
    if (prevs.length > 0) {
        var height = 0;
        prevs.each(function (index, context) {
            var height1 = $(this).outerHeight(true);
            var height2 = $(this).children().outerHeight(true);
            height += (height1 > height2) ? height1 : height2;
        });
        this.el.css({
            top: height + "px"
        });
    }
}

UCML.ListGroup.prototype.onRender = function () {
    UCML.ListGroup.superclass.onRender.call(this);
    if (this.hasScroll) {
        this.el.addClass("wrapper");
    }
}

UCML.ListGroup.prototype.clearData = function () {
    if (!this.reserveData) {
        this.srcDataTable.ClearData();
    }
}

UCML.ListGroup.prototype.bindEvents = function () {
    UCML.ListGroup.superclass.bindEvents.call(this);

    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.titleField)) {
        this.srcDataTable.isLoadHistoryData = true;
        this.srcDataTable.on("onLoad", this.dataBind, this);
        this.srcDataTable.on("beforeGetData", this.clearData, this);
    } else {
        this.bindCustomData();
    }

    var opts = this;
    //列表的点击事件
    this.listviewEl.on("click", function (e) {
        var itemEl = null; //li元素
        var thisEl = $(e.target); //目标元素
        if (e.target.nodeName.toLowerCase() == "li") { //无a标签、无img标签
            itemEl = $(e.target);
        } else {
            itemEl = $(e.target).parents("li[lid]");
        }

        if (itemEl) {
            var lid = itemEl.attr("lid");
            if (UCML.isEmpty(lid)) {
                if (thisEl.attr("data-role") == "list-divider") {//如果点击分组，则滚动条滚动到相应位置
                    $("html,body").animate({ scrollTop: thisEl.offset().top }, 1000);
                }
                return;
            }

            if (opts.srcDataTable) opts.srcDataTable.LocateOID(lid); //定位数据
            if (e.target.nodeName.toLowerCase() == "img") opts.fireEvent("imgclick", itemEl); //注册图标点击事件
            else if (thisEl.hasClass("bclink-action")) {
                var fn = thisEl.attr("urlFn");
                if (!UCML.isEmpty(fn)) {
                    eval(fn)();
                }
            }
            else if (thisEl.hasClass("image-check")) {
                if (thisEl.attr("checked") == "checked") {
                    opts.checkOID.add(lid);
                } else {
                    opts.checkOID.remove(lid);
                }
            }
            else opts.fireEvent("listclick", itemEl); //注册listclick事件

            var chk = itemEl.find("input[type]").not($(".image-check"));
            if (chk.length > 0) {
                if (e.target.nodeName.toLowerCase() == "input") { //直接点击在单选/复选按钮上
                    if (thisEl.attr("type") == "checkbox") { //点击在复选按钮上
                        if (chk.attr("checked")) {
                            var str = '<div class="selectedItem" sid="' + lid + '"><img app="' + UCMLLocalResourcePath + 'images/orderedList0.png"><p>' + itemEl.text() + '</p></div>';
                            opts.selectedListEl.append(str);

                            opts.checkOID.remove(lid);
                            opts.checkOID.add(lid);

                            opts.setScrollTop();
                        } else {
                            var selectedItem = opts.selectedListEl.find('div[sid="' + lid + '"]');
                            if (selectedItem.length > 0) {
                                selectedItem.remove();
                            }
                            var deleteItem = opts.selectedListEl.find('div[indexid="' + lid + '"]');
                            if (deleteItem.length > 0) {
                                deleteItem.remove();
                            }

                            opts.checkOID.remove(lid);

                            opts.setScrollTop();
                        }
                    }
                    e.stopPropagation(); //防止事件冒泡
                } else if (chk.attr("type") == "checkbox") { //点击在复选框所在li
                    if (chk.attr("checked") == "checked") {
                        chk.removeAttr("checked");
                        var selectedItem = opts.selectedListEl.find('div[sid="' + lid + '"]');
                        if (selectedItem.length > 0) {
                            selectedItem.remove();
                        }
                        var deleteItem = opts.selectedListEl.find('div[indexid="' + lid + '"]');
                        if (deleteItem.length > 0) {
                            deleteItem.remove();
                        }

                        opts.checkOID.remove(lid);

                        opts.setScrollTop();
                    } else {
                        chk.attr("checked", "checked");
                        var str = '<div class="selectedItem" sid="' + lid + '"><img app="' + UCMLLocalResourcePath + 'images/orderedList0.png"><p>' + itemEl.text() + '</p></div>';
                        opts.selectedListEl.append(str);

                        opts.checkOID.remove(lid);
                        opts.checkOID.add(lid);

                        opts.setScrollTop();
                    }
                } else if (chk.attr("type") == "radio") { //点击在单选框所在li
                    if (chk.attr("checked") == "checked") {

                    } else {
                        chk.attr("checked", "checked");
                    }
                }
            }
        }
    });

    //选择项的点击事件
    this.selectedListEl.on("click", function (e) {
        var itemEl = null;
        var thisEl = $(e.target); //目标元素
        if (e.target.nodeName.toLowerCase() == "div") {
            itemEl = $(e.target);
        } else {
            itemEl = $(e.target).parents("div.selectedItem");
        }

        if (itemEl) {
            if (itemEl.hasClass("deleteItem")) {
                var indexid = itemEl.attr("indexid");

                opts.fireEvent("deleteitemclick", indexid);

                var selectedItem = opts.selectedListEl.find('div[sid="' + indexid + '"]');
                var checkedItem = opts.el.find("li[lid='" + indexid + "']").find("input[type]");
                if (selectedItem.length > 0) {
                    selectedItem.remove();
                }
                if (checkedItem.length > 0) {
                    checkedItem.removeAttr("checked");
                }
                itemEl.remove();

                opts.checkOID.remove(indexid);

                opts.setScrollTop();
            } else {
                var sid = itemEl.attr("sid");

                if (opts.selectedListEl.find('div[indexid="' + sid + '"]').length < 1) {
                    var str = '<div class="selectedItem deleteItem" indexid="' + sid + '"><img app="' + UCMLLocalResourcePath + 'images/delete.png"><p>删除</p></div>';
                    opts.selectedListEl.append(str);
                }
                opts.setScrollTop();
            }
        }
    });

    //加载更多事件
    this.clickLoadMore.on("click", function (e) {
        opts.reserveData = true;
        opts.srcDataTable.LoadMoreData();
        opts.reserveData = false;
    });
}

UCML.ListGroup.prototype.getListGroup = function () {
    var recordCount = this.srcDataTable.getRecordCount();
    var arr_group = [];
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        var groupvalue = this.srcDataTable.getFieldValue(this.dividerField);
        if (arr_group.indexOf(groupvalue) == -1) {
            arr_group.add(groupvalue);
        }
    }
    return arr_group;
}

UCML.ListGroup.prototype.getDividerText = function (value) {
    var col = this.srcDataTable.getColumn(this.dividerField);
    if (col.isCodeTable) {
        return GetCodeCaption(col.codeTable, value);
    } else {
        return value;
    }
}

UCML.ListGroup.prototype.getListStrGroup = function (data) {
    var recordCount = this.srcDataTable.getRecordCount();
    var listviewStr = "";

    var groupList = this.getListGroup();
    for (var g = 0; g < groupList.length; g++) {
        listviewStr += '<li data-role="list-divider">' + this.getDividerText(groupList[g]) + '</li>';
        listviewStr += this.getListStr(groupList[g]);
    }
    return listviewStr;
}

UCML.ListGroup.prototype.getListStr = function (filter) {
    var recordCount = this.srcDataTable.getRecordCount();
    var listviewStr = "";

    if (this.type == "singlecol") {
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
					this.srcDataTable.getFieldValue(this.titleField) +
					((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' + this.srcDataTable.getFieldValue(this.titleField) +
					((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</li>';
            }
        }
    } else if (this.type == "multicol") {
        var colnum = 1,
			extraField = [];
        if (!UCML.isEmpty(this.extraTitleFields)) {
            extraField = this.extraTitleFields.split(";");
            colnum = colnum + extraField.length;
        }

        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
					this.srcDataTable.getFieldValue(this.titleField) +
					((colnum == 1) ? '' : ((colnum == 2) ? '<span class="ui-text-right">' + this.srcDataTable.getFieldValue(extraField[0]) + '</span>' : '<span class="ui-text-center">' + this.srcDataTable.getFieldValue(extraField[0]) + '</span><span class="ui-text-right">' + this.srcDataTable.getFieldValue(extraField[1]) + '</span>')) +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' + this.srcDataTable.getFieldValue(this.titleField) +
					((colnum == 1) ? '' : ((colnum == 2) ? '<span class="ui-text-right">' + this.srcDataTable.getFieldValue(extraField[0]) + '</span>' : '<span class="ui-text-center">' + this.srcDataTable.getFieldValue(extraField[0]) + '</span><span class="ui-text-right">' + this.srcDataTable.getFieldValue(extraField[1]) + '</span>')) +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</li>';
            }
        }
    } else if (this.type == "imageIcon") {
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            var imgsrc = this.srcDataTable.getFieldValue(this.iconField);
            if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
            if (this.getImgSrc) imgsrc = this.getImgSrc();
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
					(!UCML.isEmpty(this.iconField) ? '<img class="ui-li-icon" app="' + this.formatUrl(imgsrc) + '">' : '') +
					this.srcDataTable.getFieldValue(this.titleField) +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' +
					(!UCML.isEmpty(this.iconField) ? '<img class="ui-li-icon" app="' + this.formatUrl(imgsrc) + '">' : '') +
					this.srcDataTable.getFieldValue(this.titleField) +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</li>';
            }
        }
    } else if (this.type == "checkgroup") {
        var name = this.id + "_checkbox";
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            var defaultCheck = "";
            if (this.hasSelectedList && this.selectedListEl.find('div[sid="' + oid + '"]').length > 0) {
                defaultCheck = "checked";
            }
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
					(this.alignRight ? '<input type="checkbox" ' + defaultCheck + ' id="checkitem' + i + '" data-role="none" class="ui-checkbox-right" name="' + name + '">' : '<input type="checkbox" ' + defaultCheck + ' id="checkitem' + i + '" data-role="none" name="' + name + '">') +
					this.srcDataTable.getFieldValue(this.titleField) +
					'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' +
					(this.alignRight ? '<input type="checkbox" ' + defaultCheck + ' id="checkitem' + i + '" data-role="none" class="ui-checkbox-right" name="' + name + '">' : '<input type="checkbox" ' + defaultCheck + ' id="checkitem' + i + '" data-role="none" name="' + name + '">') +
					this.srcDataTable.getFieldValue(this.titleField) +
					'</li>';
            }
        }
    } else if (this.type == "radiogroup") {
        var name = this.id + "_radiobox";

        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
				(this.alignRight ? '<input type="radio" id="radioitem' + i + '" data-role="none" class="ui-radiobox-right" name="' + name + '">' : '<input type="radio" id="radioitem' + i + '" data-role="none" name="' + name + '">') +
				this.srcDataTable.getFieldValue(this.titleField) +
				'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' +
				(this.alignRight ? '<input type="radio" id="radioitem' + i + '" data-role="none" class="ui-radiobox-right" name="' + name + '">' : '<input type="radio" id="radioitem' + i + '" data-role="none" name="' + name + '">') +
				this.srcDataTable.getFieldValue(this.titleField) +
				'</li>';
            }
        }
    } else if (this.type == "imageDesc") {
        var name = this.id + "_checkbox";
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (!UCML.isEmpty(filter) && this.srcDataTable.getFieldValue(this.dividerField) != filter) {
                continue;
            }
            var oid = this.srcDataTable.getOID();
            var imgsrc = this.srcDataTable.getFieldValue(this.iconField);
            if (imgsrc == ".jpg" || imgsrc == ".png" || imgsrc == ".gif") imgsrc = this.getSysImgSrc();
            if (this.getImgSrc) imgsrc = this.getImgSrc();
            if (this.hasLinked) {
                listviewStr += '<li lid="' + oid + '"><a>' +
					(!UCML.isEmpty(this.iconField) ? '<img app="' + this.formatUrl(imgsrc) + '">' : '') +
					(!UCML.isEmpty(this.titleField) ? '<h3>' + this.srcDataTable.getFieldValue(this.titleField) + '</h3>' : '') +
					(!UCML.isEmpty(this.descField) ? '<p>' + this.srcDataTable.getFieldValue(this.descField) + '</p>' : '') +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'</a></li>';
            } else {
                listviewStr += '<li lid="' + oid + '">' +
					(!UCML.isEmpty(this.iconField) ? '<img app="' + this.formatUrl(imgsrc) + '">' : '') +
					(!UCML.isEmpty(this.titleField) ? '<h3>' + this.srcDataTable.getFieldValue(this.titleField) + '</h3>' : '') +
					(!UCML.isEmpty(this.descField) ? '<p>' + this.srcDataTable.getFieldValue(this.descField) + '</p>' : '') +
                    ((this.hasCount && !UCML.isEmpty(this.totalField)) ? '<span class="ui-li-count">' + this.srcDataTable.getFieldValue(this.totalField) + '</span>' : '') +
					'<span>' + this.renderBCLink() + '</span>' +
                    '</li>';
            }
        }
    }

    return listviewStr;
}

UCML.ListGroup.prototype.hideBCLink = function (index) {
    if (index) {
        $("a.bclink-action[linkIndex=" + index + "]").hide();
    } else {
        $("a.bclink-action").hide();
    }
}

UCML.ListGroup.prototype.showBCLink = function (index) {
    if (index) {
        $("a.bclink-action[linkIndex=" + index + "]").show();
    } else {
        $("a.bclink-action").show();
    }
}

UCML.ListGroup.prototype.renderBCLink = function () {
    var col = this.srcDataTable.getColumn(this.titleField);
    var linkStr = "";
    if (col.objBCLinkColl) {
        for (var i = col.objBCLinkColl.length; i > 0; i--) {
            var objBCLink = col.objBCLinkColl[i - 1];
            if (objBCLink.BCRunMode == 5) {
                linkStr += '<a class="ui-btn ui-btn-inline ui-mini bclink-action" linkIndex="' + (i - 1) + '" urlFn="' + objBCLink.urlFuncName + '">' + objBCLink.caption + '</a>';
            }
        }
    }
    if (this.hasChecked) {
        linkStr += '<input class="image-check" type="checkbox" data-role="none" style="float:right;width:15px;height:15px;margin-top:-30px;" />';
    }
    return linkStr;
}

UCML.ListGroup.prototype.dataBind = function (data) {
    var listviewStr = "";

    if (!UCML.isEmpty(this.dividerField)) {
        listviewStr = this.getListStrGroup();
    } else {
        listviewStr = this.getListStr();
    }

    this.listviewEl.html("").append(listviewStr);

    if (this.hasScroll) {
        this.scrollEl.find(".pullUp").before(this.listviewEl);
        this.el.append(this.scrollEl).trigger('create');
        if (this.hasSearch && this.UCMLIScroll.myScroll) {
            this.UCMLIScroll.myScroll.refresh();
        }
    } else {
        this.el.append(this.listviewEl).trigger('create');
    }

    this.listviewEl.listview("refresh");

    if (this.hasScroll && !this.UCMLIScroll.myScroll) {
        this.UCMLIScroll.loaded();
    }
}

//返回复选框列表选中的OID数组
UCML.ListGroup.prototype.getCheckOID = function () {
//    if (this.type != "checkgroup") {
//        return [];
//    } else {
//        return this.checkOID;
//    }
    return this.checkOID;
}

UCML.ListGroup.prototype.formatUrl = function (url) {
    if ((url.indexOf("http://") > -1) || (url.indexOf("https://") > -1)) { //网络资源
        return url;
    } else { //服务器资源
        url = UCMLLocalResourcePath + url;
        return url;
    }
}

UCML.ListGroup.prototype.getSysImgSrc = function () {
    var imgURL = "File/Images/" + this.srcDataTable.TableName + "_" + this.iconField + "/" + UCML.UserInfo.getUserOID() + "/" + this.srcDataTable.getOID() + this.srcDataTable.getFieldValue(this.iconField);
    return imgURL;
}

UCML.ListGroup.prototype.bindCustomData = function () {
    var str = '<ul data-role="listview">' +
        		'<li><a>没有绑定数据源!</a></li>' +
        		'</ul>';
    this.el.append(str).trigger('create');
}

UCML.reg("UCML.ListGroup", UCML.ListGroup);

UCML.IScroll = function (config) {
    this.myScroll = null;
    this.pullDownEl;
    this.pullDownOffset;
    this.pullUpEl;
    this.pullUpOffset;
    this.generatedCount = 0;
    this.pEl = config.pEl;
    this.ctl = config.ctl || undefined;
    this.pullDownActionCallback = config.pullDownAction || undefined;
    this.pullUpActionCallback = config.pullUpAction || undefined;
}

UCML.IScroll.prototype.loaded = function () {
    this.pullDownEl = document.getElementById(this.pEl.attr("id") + '_pullDown');
    if (this.pullDownEl)
        this.pullDownOffset = this.pullDownEl.offsetHeight;
    this.pullUpEl = document.getElementById(this.pEl.attr("id") + '_pullUp');
    if (this.pullUpEl)
        this.pullUpOffset = this.pullUpEl.offsetHeight;

    var opts = this;
    this.myScroll = new iScroll(this.pEl[0], {
        scrollbarClass: 'myScrollbar',
        /* 重要样式 */
        useTransition: false,
        /* 此属性不知用意，本人从true改为false */
        topOffset: 51,
        onRefresh: function () {
            if ($(opts.pullDownEl).hasClass('loading')) {
                $(opts.pullDownEl).removeClass('loading');
                opts.pullDownEl.querySelector('.pullDownLabel').innerHTML = '下拉刷新...';
            } else if ($(opts.pullUpEl).hasClass('loading')) {
                $(opts.pullUpEl).removeClass('loading');
                opts.pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
            }
        },
        onScrollMove: function () {
            if (this.y > 5 && !$(opts.pullDownEl).hasClass('flip')) {
                $(opts.pullDownEl).addClass('flip');
                opts.pullDownEl.querySelector('.pullDownLabel').innerHTML = '松手开始更新...';
                this.minScrollY = 0;
            } else if (this.y < 5 && $(opts.pullDownEl).hasClass('flip')) {
                $(opts.pullDownEl).removeClass('flip');
                opts.pullDownEl.querySelector('.pullDownLabel').innerHTML = '下拉刷新...';
                this.minScrollY = -opts.pullDownOffset;
            } else if (this.y < (this.maxScrollY - 5) && !$(opts.pullUpEl).hasClass('flip')) {
                $(opts.pullUpEl).addClass('flip');
                opts.pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始更新...';
                this.maxScrollY = this.maxScrollY;
            } else if (this.y > (this.maxScrollY + 5) && $(opts.pullUpEl).hasClass('flip')) {
                $(opts.pullUpEl).removeClass('flip');
                opts.pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
                this.maxScrollY = opts.pullUpOffset;
            }
        },
        onScrollEnd: function () {
            if ($(opts.pullDownEl).hasClass('flip')) {
                $(opts.pullDownEl).removeClass('flip');
                $(opts.pullDownEl).addClass('loading');
                opts.pullDownEl.querySelector('.pullDownLabel').innerHTML = '加载中...';
                opts.pullDownAction();
            } else if ($(opts.pullUpEl).hasClass('flip')) {
                $(opts.pullUpEl).removeClass('flip');
                $(opts.pullUpEl).addClass('loading');
                opts.pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
                opts.pullUpAction();
            }
        }
    });
}

UCML.IScroll.prototype.pullDownAction = function () {
    var opts = this;
    setTimeout(function () {
        if (opts.pullDownActionCallback) {
            opts.pullDownActionCallback.call(opts);
        }
        opts.myScroll.refresh();
    }, 100);
}

UCML.IScroll.prototype.pullUpAction = function () {
    var opts = this;
    setTimeout(function () {
        if (opts.pullUpActionCallback) {
            opts.pullUpActionCallback.call(opts);
        } else if (opts.ctl && opts.ctl.srcDataTable) {
            opts.ctl.reserveData = true;
            opts.ctl.srcDataTable.LoadMoreData();
            opts.ctl.reserveData = false;
            //            var datatable = BusinessObject.getDataTableByBCName(opts.pEl.attr("srcBCName"));
            //            if (datatable) {
            //                datatable.LoadMoreData();
            //            }
        }
        opts.myScroll.refresh();
    }, 100);
}

/**
*	折叠列表	主子数据	树结构数据
*/
UCML.FoldList = function (id) {

    //分类数据来源BC
    this.parentBC;

    //分类数据标题字段
    this.parentTitleField;

    //折叠区域数据来源BC
    this.childBC;
    this.childTitleField;
    this.foreignKeyField;

    this.addEvents("childlistclick");
    UCML.FoldList.superclass.constructor.call(this, id);
}

UCML.extend(UCML.FoldList, UCML.Commponent, {
    ctype: "UCML.FoldList",
    mini: "false", //是否小型样式
    inset: "true", //是否有圆角
    iconpos: "left", //图标位置, left right top bottom
    theme: "a", //分隔栏主题
    content_theme: "a", //分隔栏内容主题
    collapsed: "false", //是否收缩
    collapsed_icon: "", //收缩时图标
    expanded_icon: "", //展开时图标

    setProperties: function () {
        UCML.FoldList.superclass.setProperties.call(this);
        this.parentBC = BusinessObject.getDataTableByBCName(this.getAttribute("parentBC")) || undefined;
        this.parentTitleField = this.getAttribute("parentTitleField") || this.parentTitleField;

        this.childBC = BusinessObject.getDataTableByBCName(this.getAttribute("childBC")) || undefined;
        this.childTitleField = this.getAttribute("childTitleField") || this.childTitleField;
        this.foreignKeyField = this.getAttribute("foreignKeyField") || this.foreignKeyField;

        this.theme = this.getAttribute("theme") || this.theme;
        this.content_theme = this.getAttribute("content_theme") || this.content_theme;
    }
});

UCML.FoldList.prototype.init = function () {
    UCML.FoldList.superclass.init.call(this);

    if (!UCML.isEmpty(this.parentBC) && !UCML.isEmpty(this.parentTitleField)) {
        this.parentBC.on("onLoad", this.dataBind, this);
    } else {
        this.bindCustomData();
    }
}

UCML.FoldList.prototype.bindEvents = function () {
    UCML.FoldList.superclass.bindEvents.call(this);
    var opts = this;
    $(document).on("collapsibleexpand", "div.ucml-" + this.id + "-foldlist", function () {
        if ($(this).attr("loaded")) {
            return;
        }
        var oid = $(this).attr("oid");
        opts.parentBC.LocateOID(oid);

        if (!opts.parentBC.isChildData()) {
            var cur = opts.parentBC.getCurrentRecord();
            opts.parentBC.BusinessObject.loadChildDataEx(opts.parentBC.BCName, opts.parentBC.getOID(), opts.parentBC.turnArrayDataToXml(cur), opts.parentBC.subBC_SQLCondi, opts.parentBC.LoadChildResults, opts.parentBC.OnFailureCall, opts.parentBC);
        }

        var childRecordCount = opts.childBC.getRecordCount();
        var listviewStr = '<ul data-role="listview">';
        for (var i = 0; i < childRecordCount; i++) {
            opts.childBC.SetIndexNoEvent(i);
            var oid = opts.childBC.getOID();
            var title = opts.childBC.getFieldValue(opts.childTitleField);
            listviewStr += '<li class="' + opts.id + '-foldlist-li" lid="' + oid + '"><a href="#">' + title + '</a></li>';
        }
        listviewStr += '</ul>';
        $(this).attr("loaded", "true");
        $(this).find(".ui-collapsible-content").append(listviewStr).trigger('create');
    });

    $(document).on("click", "li." + this.id + "-foldlist-li", function (e) {
        var lid = $(this).attr("lid");
        opts.childBC.LocateOID(lid);
        opts.fireEvent("childlistclick", $(this));
        
        e.stopPropagation(); 
        e.preventDefault();
    });

}

UCML.FoldList.prototype.dataBind = function (data) {
    var recordCount = this.parentBC.getRecordCount();
    var collapsibleStr = "";
    for (var i = 0; i < recordCount; i++) {
        this.parentBC.SetIndexNoEvent(i);
        var oid = this.parentBC.getOID();
        var title = this.parentBC.getFieldValue(this.parentTitleField);
        collapsibleStr += '<div data-role="collapsible" class="ucml-' + this.id + '-foldlist" data-theme="' + this.theme + '" data-content-theme="' + this.content_theme + '" oid="' + oid + '"><h3>' + title + '</h3></div>';
    }
    this.el.append(collapsibleStr);
    this.el.trigger("create");


}

UCML.FoldList.prototype.bindCustomData = function () {
    var str = '<div data-role="collapsible"><h3>aaa</h3><ul data-role="listview"><li><a>abc</a></li><li><a>abc</a></li></ul></div>' +
		'<div data-role="collapsible"><h3>bbb</h3><ul data-role="listview"><li><a>abc</a></li><li><a>abc</a></li></ul></div>' +
		'<div data-role="collapsible"><h3>ccc</h3><ul data-role="listview"><li><a>abc</a></li><li><a>abc</a></li></ul></div>';
    this.el.append(str);
    this.el.trigger("create");
}

UCML.reg("UCML.FoldList", UCML.FoldList);

/**
*	手机版查询控件
*/
UCML.MSearch = function (id) {
    UCML.MSearch.superclass.constructor.call(this, id);
}

UCML.extend(UCML.MSearch, UCML.Input, {
    ctype: "UCML.MSearch",
    placeholder: "请输入关键字...",
    onRender: function () {
        UCML.MSearch.superclass.onRender.call(this);
        //this.el.append(this.searchEl);
        //this.searchEl.textinput();
        this.el.addClass("search-nav");
        this.el.append(this.inputEl);
        this.el.append(this.queryEl);
    },
    setProperties: function () {
        UCML.MSearch.superclass.setProperties.call(this);
        this.placeholder = this.getAttribute("placeholder") || this.placeholder;
        this.searchEl = $('<input id="searchInput" placeholder="' + this.placeholder + '" data-type="search" class="ui-input-text ui-body-c" />');

        this.inputEl = $('<input type="search" class="search-input" placeholder="' + this.placeholder + '"/>');
        this.queryEl = $('<a href="javascript:void(0);" class="search-submit"></a>');

        //设置bc、字段
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute("srcBCName")) || BusinessObject.getDataTableByBCName(this.getAttribute("BCName")) || this.srcDataTable;
        this.searchField = this.getAttribute("searchField") || this.searchField;

        //设置表、字段、字段类型
        this.tableList = this.getAttribute("tablelist") || "";
        this.fieldList = this.getAttribute("fieldlist") || "";
        this.typeList = this.getAttribute("typelist") || "";
    },
    bindEvents: function () {
        UCML.MSearch.superclass.bindEvents.call(this);

        var opts = this;
        this.queryEl.on("click", function (e) {
            var inputText = opts.inputEl.val();
            if (UCML.isEmpty(inputText)) {
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", "");
                return;
            };

            //设置bc、字段
            if (opts.srcDataTable && !UCML.isEmpty(opts.searchField)) {
                var sqlcondi = "";
                var fieldList = opts.searchField.split(";");
                for (var i = 0; i < fieldList.length; i++) {
                    sqlcondi += "(";
                    var fieldType = opts.srcDataTable.getFieldType(fieldList[i]);
                    if (fieldType == 'Float' ||
						fieldType == 'Double' ||
						fieldType == 'Money' ||
						fieldType == 'Numeric' ||
						fieldType == 'Long' ||
						fieldType == 'Byte' ||
						fieldType == 'Short' ||
						fieldType == 'Int') {
                        if (opts.checkNum(inputText)) {
                            sqlcondi += opts.srcDataTable.TableName + "." + fieldList[i] + " = " + inputText;
                        } else {
                            sqlcondi += " 0=1 ";
                        }
                    } else {
                        sqlcondi += opts.srcDataTable.TableName + "." + fieldList[i] + " like '%" + inputText + "%'";
                    }
                    sqlcondi += ")";

                    if (i < fieldList.length - 1)
                        sqlcondi += " or ";
                }
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", sqlcondi);
            } else if (!UCML.isEmpty(opts.tableList) && !UCML.isEmpty(opts.fieldList) && !UCML.isEmpty(opts.typeList)) {
                var sqlcondi = "";
                var fieldList = opts.fieldList.split(";");
                var typeList = opts.typeList.split(";");
                var tableList = opts.tableList.split(";");
                for (var i = 0; i < fieldList.length; i++) {
                    sqlcondi += "(";
                    var fieldType = typeList[i];
                    if (fieldType == '13' || //Float
						fieldType == '14' || //Double
						fieldType == '15' || //Money
						fieldType == '4' || //Numeric
						fieldType == '31' || //Long
						fieldType == '12' || //Byte
						fieldType == '11' || //Short
						fieldType == '10') //Int 
                    {
                        if (opts.checkNum(inputText)) {
                            sqlcondi += tableList[i] + "." + fieldList[i] + " = " + inputText;
                        } else {
                            sqlcondi += " 0=1 ";
                        }

                    } else {
                        sqlcondi += tableList[i] + "." + fieldList[i] + " like '%" + inputText + "%'";
                    }
                    sqlcondi += ")";

                    if (i < fieldList.length - 1)
                        sqlcondi += " or ";
                }
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", sqlcondi);
            }
        });
        this.searchEl.on("keyup change input", function (e) {
            var inputText = $(this).val();
            if (UCML.isEmpty(inputText)) {
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", "");
                return;
            };

            //设置bc、字段
            if (opts.srcDataTable && !UCML.isEmpty(opts.searchField)) {
                var sqlcondi = "";
                var fieldList = opts.searchField.split(";");
                for (var i = 0; i < fieldList.length; i++) {
                    sqlcondi += "(";
                    var fieldType = opts.srcDataTable.getFieldType(fieldList[i]);
                    if (fieldType == 'Float' ||
						fieldType == 'Double' ||
						fieldType == 'Money' ||
						fieldType == 'Numeric' ||
						fieldType == 'Long' ||
						fieldType == 'Byte' ||
						fieldType == 'Short' ||
						fieldType == 'Int') {
                        if (opts.checkNum(inputText)) {
                            sqlcondi += opts.srcDataTable.TableName + "." + fieldList[i] + " = " + inputText;
                        } else {
                            sqlcondi += " 0=1 ";
                        }
                    } else {
                        sqlcondi += opts.srcDataTable.TableName + "." + fieldList[i] + " like '%" + inputText + "%'";
                    }
                    sqlcondi += ")";

                    if (i < fieldList.length - 1)
                        sqlcondi += " or ";
                }
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", sqlcondi);
            } else if (!UCML.isEmpty(opts.tableList) && !UCML.isEmpty(opts.fieldList) && !UCML.isEmpty(opts.typeList)) {
                var sqlcondi = "";
                var fieldList = opts.fieldList.split(";");
                var typeList = opts.typeList.split(";");
                var tableList = opts.tableList.split(";");
                for (var i = 0; i < fieldList.length; i++) {
                    sqlcondi += "(";
                    var fieldType = typeList[i];
                    if (fieldType == '13' || //Float
						fieldType == '14' || //Double
						fieldType == '15' || //Money
						fieldType == '4' || //Numeric
						fieldType == '31' || //Long
						fieldType == '12' || //Byte
						fieldType == '11' || //Short
						fieldType == '10') //Int 
                    {
                        if (opts.checkNum(inputText)) {
                            sqlcondi += tableList[i] + "." + fieldList[i] + " = " + inputText;
                        } else {
                            sqlcondi += " 0=1 ";
                        }

                    } else {
                        sqlcondi += tableList[i] + "." + fieldList[i] + " like '%" + inputText + "%'";
                    }
                    sqlcondi += ")";

                    if (i < fieldList.length - 1)
                        sqlcondi += " or ";
                }
                opts.srcDataTable.isLoadHistoryData = false;
                opts.srcDataTable.BusinessObject.condiQuery("", "", "", sqlcondi);
            }
        });
    }
});

/* 
*  判断字符串是否可以转化为纯数值类型
*  1.如果有小数点,则小数点只能有一个且不能在头、尾
*  2.如果没有小数点,则第一个不能为0
*/
UCML.MSearch.prototype.checkNum = function (str) {
    var letter;
    if (str.indexOf(".") != -1) {
        if (str.charAt[0] == '.' || str.charAt[str.length - 1] == '.')
            return false;
        var temp = str.replace(".", "");
        if (temp.indexOf(".") != -1)
            return false;
        letter = "1234567890.";
    } else {
        if (str.charAt[0] == "0")
            return false;
        letter = "1234567890";
    }

    for (var i = 0; i < str.length; i++) {
        var c = str.charAt(i);
        if (letter.indexOf(c) == -1) {
            return false;
        }
    }
    return true;
}

UCML.reg("UCML.MSearch", UCML.MSearch);

/**
*	树列表VC
*/
UCML.TreeList = function (id) {
    this.navbarEl;
    this.searchObj;
    this.selectedListEl;
    UCML.TreeList.superclass.constructor.call(this, id);
}

UCML.extend(UCML.TreeList, UCML.Applet, {
    ctype: "UCML.TreeList",
    rootId: "00000000-0000-0000-0000-000000000000",
    hasSearch: true,
    hasSelectedList: true,
    getRootId: function () {
        var keyFiledType = this.dataTable.getColumn(this.dataTable.PrimaryKey).fieldType;
        var ParentOID;
        if (keyFiledType == "UCMLKey" || keyFiledType == "Guid")
            ParentOID = this.dataTable.getEmptyGuid();
        else
            ParentOID = '0';
        return ParentOID;
    },
    setProperties: function () {
        UCML.TreeList.superclass.setProperties.call(this);

        this.rootId = this.getRootId();
        this.searchObj = new UCML.MSearch({
            srcDataTable: this.dataTable,
            searchField: this.TextFieldName
        });
        this.selectedListEl = $('<div id="selectedList"></div>');
        this.navbarEl = $('<div id="UNavbar"></div>');
        this.page = $("#" + this.id + "_Page");
    }
});

UCML.TreeList.prototype.bindEvents = function () {
    UCML.TreeList.superclass.bindEvents.call(this);
    if (this.dataTable) {
        this.dataTable.on("onLoad", this.loadData, this);
    }

    var opts = this;
    $(document).on("pagebeforeshow", "div[data-role='page']", function () {
        opts.setPageSize();
    });

    //单独定义hascheck事件是为了解决苹果浏览器的bug
    $(document).on("touchend click", "li.hasCheck", function (e) {
        if (e.target.nodeName.toLowerCase() != "li") return;
        if (!$(this).hasClass("touchmoving")) {
            var that = this;
            var el = $(this);
            setTimeout(function () {
                var chkEl = el.find('input[type="checkbox"]');
                var oid = chkEl.attr('oid');
                var text = el.text();
                if (chkEl.attr("checked") == "checked") {
                    var selectedItem = opts.selectedListEl.find('div[sid="' + oid + '"]');
                    if (selectedItem.length > 0) {
                        selectedItem.remove();
                    }
                    var deleteItem = opts.selectedListEl.find('div[indexid="' + oid + '"]');
                    if (deleteItem.length > 0) {
                        deleteItem.remove();
                    }
                    chkEl.removeAttr("checked");
                } else {
                    var str = '<div class="selectedItem" sid="' + oid + '"><img app="' + UCMLLocalResourcePath + 'images/orderedList0.png"><p>' + text + '</p></div>';
                    opts.selectedListEl.append(str);
                    chkEl.attr("checked", "checked");
                }

                opts.setPageSize();
            }, 0);
        } else {
            $(this).remove("touchmoving");
        }
    });

    $(document).on("touchmove", "li.hasCheck", function (e) {
        $(this).addClass("touchmoving");
    });

    $(document).on('click', $("#MainPanel"), function (e) {
        var el = $(e.target);

        if (!UCML.isEmpty(el.attr('cid'))) {
            //具有子数据的列表项的点击事件，跳转到子列表页
            var cid = el.attr('cid');
            var listview = opts.renderList(cid, true);
            if ($("#page_" + cid).length < 1) {
                var oPage = $('<div data-role="page" id="page_' + cid + '"></div>');
                oPage.append(listview);
                opts.page.after(oPage);
            }
            $.mobile.changePage("#page_" + cid);
        } else if (!UCML.isEmpty(el.attr('oid')) && !UCML.isEmpty(el.attr('type')) && el.attr('type') == "checkbox") {
            //复选框列表项点击事件，在选择项栏添加/删除列表项
            var oid = el.attr('oid');
            var text = el.parent().text();
            if (el.attr("checked") == "checked") {
                var str = '<div class="selectedItem" sid="' + oid + '"><img app="' + UCMLLocalResourcePath + 'images/orderedList0.png"><p>' + text + '</p></div>';
                opts.selectedListEl.append(str);
            } else {
                var selectedItem = opts.selectedListEl.find('div[sid="' + oid + '"]');
                if (selectedItem.length > 0) {
                    selectedItem.remove();
                }
                var deleteItem = opts.selectedListEl.find('div[indexid="' + oid + '"]');
                if (deleteItem.length > 0) {
                    deleteItem.remove();
                }
            }

            opts.setPageSize();
        } else if (el.hasClass("hasCheck")) {
            //复选框所在列表项li点击事件
            // var chkEl = el.find('input[type="checkbox"]');
            // var oid = chkEl.attr('oid');
            // var text = el.text();
            // if (chkEl.attr("checked") == "checked") {
            // 	var selectedItem = opts.selectedListEl.find('div[sid="' + oid + '"]');
            // 	if (selectedItem.length > 0) {
            // 		selectedItem.remove();
            // 	}
            // 	var deleteItem = opts.selectedListEl.find('div[indexid="' + oid + '"]');
            // 	if (deleteItem.length > 0) {
            // 		deleteItem.remove();
            // 	}
            // 	chkEl.removeAttr("checked");
            // } else {
            // 	var str = '<div class="selectedItem" sid="' + oid + '"><img app="images/orderedList0.png"><p>' + text + '</p></div>';
            // 	opts.selectedListEl.append(str);
            // 	chkEl.attr("checked", "checked");
            // }

            // opts.setPageSize();
        } else if ((el.hasClass("selectedItem") || el.parent().hasClass("selectedItem")) && !el.hasClass("deleteItem") && !el.parent().hasClass("deleteItem")) {
            //选择栏的子项点击事件，显示当前的删除项
            var sid = el.hasClass("selectedItem") ? el.attr("sid") : el.parent().attr("sid");
            if (opts.selectedListEl.find('div[indexid="' + sid + '"]').length < 1) {
                var str = '<div class="selectedItem deleteItem" indexid="' + sid + '"><img app="' + UCMLLocalResourcePath + 'images/delete.png"><p>删除</p></div>';
                opts.selectedListEl.append(str);
            }

            opts.setPageSize();
        } else if (el.hasClass("deleteItem") || el.parent().hasClass("deleteItem")) {
            //选择栏的删除项的点击事件，移除选择项、对应复选框勾选去掉、移除删除项
            var indexid = el.hasClass("deleteItem") ? el.attr("indexid") : el.parent().attr("indexid");
            var selectedItem = opts.selectedListEl.find('div[sid="' + indexid + '"]');
            var checkedItem = $('input[oid="' + indexid + '"]');
            if (selectedItem.length > 0) {
                selectedItem.remove();
            }
            if (checkedItem.length > 0) {
                checkedItem.removeAttr("checked");
            }
            el.hasClass("deleteItem") ? el.remove() : el.parent().remove();

            opts.setPageSize();
        }
    });
}

UCML.TreeList.prototype.setPageSize = function (opage) {
    var page = opage || $.mobile.activePage;
    var navbarheight = this.navbarEl.outerHeight(true);
    page.css({
        "padding-top": navbarheight + "px"
    });
}

UCML.TreeList.prototype.init = function () {
    UCML.TreeList.superclass.init.call(this);

    if (this.hasSearch) {
        this.navbarEl.append(this.searchObj.el);
    }

    if (this.hasSelectedList) {
        this.navbarEl.append(this.selectedListEl);
    }

    this.navbarEl.css({
        "position": "absolute",
        "top": "0px",
        "z-index": "999",
        "width": "100%"
    })

    this.page.before(this.navbarEl);

    this.setPageSize(this.page);

    this.searchObj.el.trigger("create");
}

UCML.TreeList.prototype.loadData = function (data) {
    try {
        this.dataNodes = eval(this.id + 'Nodes');
    } catch (e) {
        this.dataNodes = null;
    }
    if (this.dataNodes) {
        //this.loadDataNodes(this.dataNodes);
    } else {
        this.el.html("").append(this.renderList()).trigger('create');
    }
}

UCML.TreeList.prototype.renderList = function (parentID, hasBack) {
    var pid = parentID || this.rootId;
    var str = '<ul data-role="listview" data-inset="true">';
    if (hasBack) {
        str += '<li data-icon="back" class="pageback"><a href="#" data-rel="back">返回上一级</a></li>';
    }
    var recordCount = this.dataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.dataTable.SetIndexNoEvent(i);
        if (this.dataTable.getFieldValue(this.ParentFieldName) == pid) {
            var cid = this.dataTable.getOID();
            var hasChild = this.hasChild(cid, i);
            str += '<li' +
				(hasChild ? '><a cid="' + cid + '">' : ' class="hasCheck"><input type="checkbox" oid="' + cid + '" name="checkbox_' + this.id + '" data-role="none" />') +
				this.dataTable.getFieldValue(this.TextFieldName) +
				(hasChild ? '</a>' : '') +
				'</li>';
        }
    }
    str += '</ul>';
    return str;
}

UCML.TreeList.prototype.hasChild = function (cid, recIndex) {
    var hasChild = false;
    var recordCount = this.dataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.dataTable.SetIndexNoEvent(i);
        if (this.dataTable.getFieldValue(this.ParentFieldName) == cid) {
            hasChild = true;
            break;
        }
    }
    this.dataTable.SetIndexNoEvent(recIndex);
    return hasChild;
}