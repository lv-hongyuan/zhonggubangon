﻿UCML.DateBox = function (id) {
    if (!this.dateFmt) {
        this.dateFmt = "y-m-d h:mi:s";
    }
    this.formatter = function (date) {
        //        var y = date.getFullYear();
        //        var m = date.getMonth() + 1;
        //        var d = date.getDate();
        //        return y + '-' + m + '-' + d;
        return this.getData(date) + " " + this.getTime(date);
    };

    this.getData = function (date) {
        var str = this.dateFmt.split(' ')[0];
        str = str.replace('y', date.getFullYear());
        str = str.replace('m', String.leftPad(date.getMonth() + 1, 2, '0'));
        if (!this.hideDay)
            str = str.replace('d', String.leftPad(date.getDate(), 2, '0'));

        else
            str = str.replace('d', '01');
        return str;
    }

    this.getTime = function (time) {
        var str = this.dateFmt.split(' ')[1];
        str = str.replace('h', time.getHours());
        str = str.replace('mi', time.getMinutes());
        str = str.replace('s', time.getSeconds());
        return str;
    }

    this.parser = function (s) {
        var yIndex = this.dateFmt.indexOf('y'); //年
        var mIndex = this.dateFmt.indexOf('m'); //月
        var dIndex = this.dateFmt.indexOf('d'); //日
        var hIndex = this.dateFmt.indexOf('h'); //时
        var miIndex = this.dateFmt.indexOf('mi'); //分
        var sIndex = this.dateFmt.indexOf('s'); //秒

        var value = [];
        var yValue = String.leftPad(s.substring(yIndex, 4), 4, '0');
        value.add(Number(yValue));
        var mValue = s.substring(mIndex, 2);
        value.add(Number(mValue) - 1);
        var dValue = s.substring(dIndex, 2);
        value.add(Number(dValue));
        var hValue = s.substring(hIndex, 2);
        value.add(Number(hValue || 0));
        var miValue = s.substring(miIndex, 2);
        value.add(Number(miValue || 0));
        var sValue = s.substring(sIndex, 2);
        value.add(Number(sValue || 0));


        if (!s) {
            return new Date();
        } else {
            //            try
            //            {

            return new Date(value[0], value[1], value[2], value[3], value[4], value[5]);

            //            }catch(e)
            //            {
            //                return  new Date();
            //            }
        }

        //        var t = Date.parse(s);
        //        if (!isNaN(t)) {
        //            return new Date(t);
        //        } else {
        //            return new Date(value[0], value[1], value[2], value[3], value[4], value[5]);
        //        }
    };

    var opts = this;
    this.select = function (date) {
        if (date) {
            var value = opts.getData(date);
            if (opts.showTime) {
                value = value + " " + opts.timebox.getValue();
            }
            opts.setValue(value);

            opts.hidePanel();
            opts.onSelect.call(opts, date);
            opts.fireEvent("blur");
        }
    }

    this.onSelect = function (date) { }
    UCML.DateBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.DateBox, UCML.Combo, {
    ctype: "UCML.DateBox",
    autoEl: 'input',
    panelWidth: 180,
    panelHeight: 'auto',
    showTime: false,
    hideDay: false,
    keyHandler: {
        up: function () { },
        down: function () { },
        enter: function () { doEnter(this); },
        query: function (q) { doQuery(this, q); }
    },
    currentText: '今天',
    closeText: '清空',
    okText: '确定',
    timeText: "时间:"
});

UCML.DateBox.prototype.init = function () {
    UCML.DateBox.superclass.init.call(this);
    this.showTime = (this.el.attr('showTime') ? this.el.attr('showTime') == 'true' : this.showTime);
    this.hideDay = (this.el.attr('hideDay') ? this.el.attr('hideDay') == 'true' : this.hideDay);
    if (this.BusinessObject) {
        var BCBase = this.BusinessObject.getDataTableByBCName(this.el.attr('BCName'));
        if (BCBase) {
            var columnInfo = BCBase.getColumn(this.el.attr('dataFld'));
            this.dateFmt = columnInfo.dateTimeFormat ? columnInfo.dateTimeFormat : this.dateFmt;
        }
    }
    this.dateFmt = (this.el.attr('dateFmt') ? this.el.attr('dateFmt') : this.dateFmt);
    UCML.on(this.dom, "change", function () { this.setValue(this.dom.value); }, this);
}


UCML.DateBox.prototype.onRender = function () {
    UCML.DateBox.superclass.onRender.call(this);
    this.combo.addClass('datebox');
}


UCML.DateBox.prototype.showPanel = function () {
    //UCML.DateBox.superclass.showPanel.call(this);
    this.panel.el.css("z-index", UCML.Window.zIndex++);
    this.panel.open();

    var panelWidth = this.panel.getOuterWidth(true);
    var opts = this;
    (function () {
        if (opts.panel.el.is(':visible')) {
            var top = opts.combo.offset().top + opts.combo.outerHeight();
            if (top + opts.panel.el.outerHeight() > $(window).height() + $(document).scrollTop()) {
                top = opts.combo.offset().top - opts.panel.el.outerHeight();
            }
            if (top < $(document).scrollTop()) {
                top = opts.combo.offset().top + opts.combo.outerHeight();
            }
            var left = opts.combo.offset().left;
            if (left + panelWidth > $(window).width() + $(document).scrollLeft()) {
                left = (left + opts.combo.outerWidth()) - panelWidth;
            }

            opts.panel.el.css({
                left: left,
                top: top
            });
            setTimeout(arguments.callee, 200);
        }
    })();

    if (!opts.calendar) {

        createCalendar();

    }

    function createCalendar() {

        var panel = opts.panel.body;
        opts.calendar = new UCML.Calendar({ border: false, rendeTo: opts.panel.body, onSelect: opts.select, isSetProperties: false }, opts.hideDay); //hideDay只选择年月
        if (opts.showTime) {
            var time = new Date();
            var timePanel = $('<div class="datebox-time-panel"><span>' + opts.timeText + '</span><input ></input></div>').appendTo(panel);
            var tvalue = time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds();
            opts.timebox = new UCML.TimeBox({ el: timePanel.find('input'), width: 80, value: tvalue });
        }
        var button = $('<div class="datebox-button"></div>').appendTo(panel);

        if (opts.showTime) {
            $('<a href="javascript:void(0)" class="datebox-ok"></a>').html(opts.okText).appendTo(button);
            button.find('.datebox-ok').click(function () {
                var data = opts.getData(opts.calendar.current);
                var value = data + " " + opts.timebox.getValue();
                opts.setValue(value);
                opts.hidePanel();
                opts.fireEvent("blur");
            });
        }
        else {
            $('<a href="javascript:void(0)" class="datebox-current"></a>').html(opts.currentText).click(function () {
                var value = opts.getData(new Date());
                opts.setValue(value);
                opts.hidePanel();
                opts.fireEvent("blur");
            }).appendTo(button);
        }
        $('<a href="javascript:void(0)" class="datebox-close"></a>').html(opts.closeText).appendTo(button);
        button.find('.datebox-current,.datebox-close,.datebox-ok').hover(
					function () { $(this).addClass('datebox-button-hover'); },
					function () { $(this).removeClass('datebox-button-hover'); }
			);
        button.find('.datebox-current').click(function () {
            opts.calendar.current = new Date();
            opts.calendar.year = new Date().getFullYear();
            opts.calendar.month = new Date().getMonth() + 1;
            opts.calendar.show();
        });
        button.find('.datebox-close').click(function () {
            opts.setValue("");
            opts.hidePanel();
        });
    }
}

UCML.DateBox.prototype.doQuery = function (q) {
    this.setValue(q);
    this.fireEvent("blur");
}

UCML.DateBox.prototype.doEnter = function () {
    var value = this.formatter(this.calendar.current);
    this.setValue(value);
    this.hidePanel();
    this.fireEvent("blur");
}

UCML.DateBox.prototype.setValue = function (value, trigger) {
    UCML.DateBox.superclass.setValue.call(this, value, trigger);
    return;
    if (this.calendar) {
        this.calendar.setValue(this.parser(value));
    }
    if (this.showTime && this.timebox) {

        this.timebox.setValue(this.getTime(this.calendar.current));
    }
}


UCML.reg("UCML.DateBox", UCML.DateBox);

UCML.reg("UCML.Datebox", UCML.DateBox);

UCML.DateTimeBox = function (id) {
    UCML.DateTimeBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.DateTimeBox, UCML.DateBox, {
    ctype: "UCML.DateTimeBox",
    showTime: true
});

UCML.reg("UCML.DateTimeBox", UCML.DateTimeBox);

UCML.reg("UCML.DateTimebox", UCML.DateTimeBox);


// 2015-03-27 日期控件只选择年月(hideDay)
UCML.DateYMBox = function (id) {
    UCML.DateYMBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.DateYMBox, UCML.DateBox, {
    ctype: "UCML.DateYMBox",
    hideDay: true,
    dateFmt: "y-m"
});

UCML.reg("UCML.DateYMBox", UCML.DateYMBox);
UCML.reg("UCML.DateYMbox", UCML.DateYMBox);