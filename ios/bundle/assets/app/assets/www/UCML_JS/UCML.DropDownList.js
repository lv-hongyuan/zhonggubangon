﻿/**
* @class UCML.DropDownList
* @extends UCML.InputList
* @description 下拉框扩展控件
* @param {String} id 容器控件id
*/
UCML.DropDownList = function (id) {
    this.addEvents("change");
    UCML.DropDownList.superclass.constructor.call(this, id);
}

UCML.extend(UCML.DropDownList, UCML.InputList, {
    ctype: "UCML.DropDownList",
    autoEl: 'select',
    isSetItemFirstText: true,
    itemFirstText: null,
    itemFirstValue: "",
    bindEvents: function () {
        var opts = this;
        this.el.bind("change", function () {
            var value = this.options[this.selectedIndex].value;
            var text = this.options[this.selectedIndex].text;

            opts.setValue(value);
            opts.fireEvent("change", text, value);
            opts.onClick(text, value);
        });
    },
    getText: function () {
        return this.dom.options[this.dom.selectedIndex].text;
    }
    ,
    getValue: function () {
        if (this.dom.selectedIndex > -1) {
            return this.dom.options[this.dom.selectedIndex].value;
        }
        return "";
    },
    getOptions: function () {
        return this.dom.options;
    }
});

UCML.DropDownList.prototype.init = function () {
    UCML.DropDownList.superclass.init.call(this);

}

UCML.DropDownList.prototype.setProperties = function () {

    UCML.DropDownList.superclass.setProperties.call(this);
    var t = this.el;
    this.JIONDataField = t.attr('JIONDataField') || this.JIONDataField;
    this.isSetItemFirstText = (this.el.attr('isSetItemFirstText') ? (this.el.attr('isSetItemFirstText')
== 'false' ? false : true) : true);
}

UCML.DropDownList.prototype.onClick = UCML.emptyFn;
UCML.DropDownList.prototype.renderedValue = function (val) {
    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.dataBind();
    }
    this.setValue(val);
    return this.getText(); 
}

/**   
* @method onChange 
* @description 数据变化时   
*/
UCML.DropDownList.prototype.change = function (val, text) {

    this.value = val;
    this.text = text;
    // this.fireEvent("onchange", e, el);

    this.setData(this.value);
    if (this.JIONDataField) {
        this.dataTable.setFieldValue(this.JIONDataField, this.text);
    }

    if (this.srcDataTable) {
        var index = this.dom.selectedIndex;
        if (this.isSetItemFirstText) {
            index = index - 1;
        }
        this.srcDataTable.setRecordIndex(index);
        //  this.srcDataTable.LocateOID(this.value);
    }
}

/**   
* @method setValue 
* @description 设置下拉框选择值
* @param {Object} value 值     
*/
UCML.DropDownList.prototype.setValue = function (value, trigger) {

    this.value = value;
    this.text = "";

    for (var i = 0; i < this.dom.options.length; i++) {

        if (this.value == this.dom.options[i].value) {
            this.text = this.dom.options[i].text;

            var opt = this.dom.options[i];

            try {
                this.dom.options[i].selected = true;

            } catch (ex) {
            }
        }
        else {
            this.dom.options[i].selected = false;
        }
    }

    if (this.JIONDataField) {
        this.dataTable.setFieldValue(this.JIONDataField, this.text);
    }

    if (this.srcDataTable && value != "") {
        this.srcDataTable.Locate(this.dataValueField, value);
    }
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
}


UCML.DropDownList.prototype.selectByIndex = function (index) {
    this.dom.selectIndex = index;
    return this.dom.options[index].selected = true;
}

UCML.DropDownList.prototype.add = function (text, value, selected) {
    selected = selected == undefined ? false : selected;
    var optionElement = document.createElement("OPTION");
    this.dom.appendChild(optionElement);
    optionElement.value = value;
    optionElement.text = text;
    optionElement.selected = selected;
    if (this.dom.options.length == 1) {
        this.value = value;
        this.text = text;
        optionElement.selected = true;
    }
}

UCML.DropDownList.prototype.insert = function (index, text, value, selected) {
    var optionElement = document.createElement("OPTION");
    this.dom.options.add(optionElement, index);
    optionElement.value = value;
    optionElement.text = text;
    optionElement.selected = selected || false;

    if (this.dom.options.length == 1) {
        this.value = value;
        this.text = text;
        optionElement.selected = true;
    }
}
/*
* @method removebyindex
移除索引

*/
UCML.DropDownList.prototype.removeAt = function (index) {
    for (var i = 0; i < this.dom.options.length; i++) {
        if (i == index)
            this.dom.options.remove(i);
    }
}

UCML.DropDownList.prototype.clear = function () {
    this.el.find("option").remove();
}

/**   
* @method dataBind 
* @description 绑定控件   
*/
UCML.DropDownList.prototype.dataBind = function () {
    UCML.DropDownList.superclass.dataBind.call(this);
    this.dom.innerHTML = "";
    var recordCount = this.srcDataTable.getRecordCount();
    if (recordCount > 0) {
        var flag = false;
        for (var i = 0; i < recordCount; i++) {
            this.srcDataTable.SetIndexNoEvent(i);
            if (i == 0) {
                this.srcDataTable.childDatas[this.srcDataTable.getOID()] = false;
            }
            var optionElement = document.createElement("OPTION");
            this.dom.appendChild(optionElement);
            optionElement.value = this.srcDataTable.getFieldValue(this.dataValueField);
            optionElement.text = this.srcDataTable.getFieldValue(this.dataTextField);
            if (this.value == optionElement.value) {
                try {//为了解决IE6BUG
                    optionElement.selected = true;

                } catch (ex) {
                }
                this.dom.selectIndex = i;
                flag = true;
            }
            else {
                // optionElement.selected = false;
                try {
                    optionElement.selected = false;

                } catch (ex) {
                }
            }
        }
        if (flag) {
            this.srcDataTable.setRecordIndex(this.dom.selectIndex);
        }
        else {
            this.srcDataTable.SetIndexNoEvent(0);
        }
    }
    else {
        this.dom.selectIndex = -1;
    }
    if (this.isSetItemFirstText) {
        if (this.applet && this.applet.itemFirstText && this.itemFirstText == null) {//如果自己没有配置则使用VC的配置
            this.itemFirstText = this.applet.itemFirstText;
        }
        var col = this.srcDataTable.getColumn(this.dataValueField);
        if (col.fieldType == "UCMLKey" && this.itemFirstValue == "") {
            this.itemFirstValue = "00000000-0000-0000-0000-000000000000";
        }

        this.insert(0, (this.itemFirstText || ""), this.itemFirstValue, flag ? false : true);

        this.dom.selectIndex = 0;
    }

    this.fireEvent("databind", this, this.input);
}

/**   
* @method bindCodeTable 
* @description 绑定代码表
*/
UCML.DropDownList.prototype.bindCodeTable = function () {
    UCML.DropDownList.superclass.bindCodeTable.call(this);
    this.dom.innerHTML = "";
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    var flag = false;

    if (codeList && codeList.length > 0) {

        for (var i = 0; i < codeList.length; i++) {
            var optionElement = document.createElement("OPTION");
            this.dom.appendChild(optionElement);
            optionElement.value = codeList[i].value;
            optionElement.text = codeList[i].caption;
            if (this.value == optionElement.value) {
                optionElement.selected = true;
                this.dom.selectIndex = i;
                flag = true;
            }
            else {
                optionElement.selected = false;
            }
        }
    }

    if (this.isSetItemFirstText) {
        if (this.applet && this.applet.itemFirstText && this.itemFirstText == null) {//如果自己没有配置则使用VC的配置
            this.itemFirstText = this.applet.itemFirstText;
        }
        this.insert(0, (this.itemFirstText || ""), "", flag ? false : true);

        this.dom.selectIndex = 0;
    }
}
/**
* @method bindCustomData
* @description 绑定自定义数据
*/
UCML.DropDownList.prototype.bindCustomData = function (texts, values) {
    UCML.DropDownList.superclass.bindCustomData.call(this);
    this.dom.innerHTML = "";
    var flag = false;
    var textList = texts.split(";");
    var valueList = values.split(";");
    if (textList && valueList && valueList.length > 0) {
        for (var i = 0; i < valueList.length; i++) {
            var optionElement = document.createElement("OPTION");
            this.dom.appendChild(optionElement);
            optionElement.value = valueList[i];
            optionElement.text = textList[i];
            if (this.value == optionElement.value) {
                optionElement.selected = true;
                this.dom.selectIndex = i;
                flag = true;
            } else {
                optionElement.selected = false;
            }
        }
    }
    
    if (this.isSetItemFirstText) {
        if (this.applet && this.applet.itemFirstText && this.itemFirstText == null) {
            this.itemFirstText = this.applet.itemFirstText;
        }
        this.insert(0, (this.itemFirstText || ""), "", flag ? false : true);
        this.dom.selectIndex = 0;
    }
}

UCML.reg("UCML.DropDownList", UCML.DropDownList);
