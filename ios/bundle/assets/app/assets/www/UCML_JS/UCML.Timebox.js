﻿UCML.TimeBox = function (id) {
    UCML.TimeBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.TimeBox, UCML.Spinner, {
    ctype: "UCML.TimeBox",
    separator: ':',
    showSeconds: true,
    highlight: 0,
    increment: 1,
    spin: function (down) {
        this.doSpin(down);
     
    }
});

UCML.TimeBox.prototype.init = function () {
    UCML.TimeBox.superclass.init.call(this);
    this.on("blur", function () { this.setValue(this.dom.value);}, this);
}

UCML.TimeBox.prototype.setProperties = function () {
    UCML.TimeBox.superclass.setProperties.call(this);
    var t = this.el;
    this.separator = t.attr('separator') || this.separator;
    this.showSeconds = (t.attr('showSeconds') ? t.attr('showSeconds') == 'true' : this.showSeconds);
    this.highlight = (parseInt(t.attr('highlight')) || this.highlight);
}

UCML.TimeBox.prototype.onRender = function () {
    UCML.TimeBox.superclass.onRender.call(this);
    var opts = this;

    this.el.unbind('.timespinner');
    this.el.bind('click.timespinner', function () {
        var start = 0;
        if (this.selectionStart != null) {
            start = this.selectionStart;
        } else if (this.createTextRange) {
            var range = opts.dom.createTextRange();
            var s = document.selection.createRange();
            s.setEndPoint("StartToStart", range);
            start = s.text.length;
        }
        if (start >= 0 && start <= 2) {
            opts.highlight = 0;
        } else if (start >= 3 && start <= 5) {
            opts.highlight = 1;
        } else if (start >= 6 && start <= 8) {
            opts.highlight = 2;
        }
        opts.setHighlight();
    }).bind('blur.timespinner', function () {
        opts.fixValue();
     //   UCML.TimeBox.superclass.setData.call(opts);
    });
}

UCML.TimeBox.prototype.setValue = function (value, trigger) {
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    this.el.val(value);
    this.fixValue();
    this.value = value;
}

UCML.TimeBox.prototype.setHighlight = function () {
    var opts = this;
    var start = 0, end = 0;
    if (opts.highlight == 0) {
        start = 0;
        end = 2;
    } else if (opts.highlight == 1) {
        start = 3;
        end = 5;
    } else if (opts.highlight == 2) {
        start = 6;
        end = 8;
    }
    if (this.dom.selectionStart != null) {
        this.dom.setSelectionRange(start, end);
    } else if (this.dom.createTextRange) {
        var range = this.dom.createTextRange();
        range.collapse();
        range.moveEnd('character', end);
        range.moveStart('character', start);
 //       range.select();
    }
   // this.el.focus();
}

UCML.TimeBox.prototype.parseTime = function (value) {
    var opts = this;
    if (!value) return null;
    var vv;
	if(value.length==4)
	{
	     vv=[];
	     vv[0] = value.substr(0,2);
		 vv[1] = value.substr(2,2);
	}
	else if(value.length==6)
	{
	     vv=[];
	     vv[0] = value.substr(0,2);
		 vv[1] = value.substr(2,2);
		 vv[2] = value.substr(4,2);
	}
    else if (value.indexOf('.') != -1) {
        vv = value.split('.');
        if (vv.length > 1 && parseInt(vv[1]) > 0 && parseInt(vv[1]) < 10) {
            vv[1] = parseInt(vv[1]) * 10;
        }
    }
    else {
        vv = value.split(opts.separator);
    }
    for (var i = 0; i < vv.length; i++) {
        if (isNaN(vv[i])) return null;
    }
    while (vv.length < 3) {
        vv.push(0);
    }
    return new Date(1900, 0, 0, vv[0], vv[1], vv[2]);
}


UCML.TimeBox.prototype.fixValue = function () {
    var opts = this;
    var value = this.el.val();
    var time = this.parseTime(value);
    if (!time) {
        time = this.parseTime(opts.value);
    }
    if (!time) {
        opts.value = '';
        this.el.val('');
        return;
    }

    var minTime = this.parseTime(opts.min);
    var maxTime = this.parseTime(opts.max);
    if (minTime && minTime > time) time = minTime;
    if (maxTime && maxTime < time) time = maxTime;

    var tt = [formatNumber(time.getHours()), formatNumber(time.getMinutes())];
    if (opts.showSeconds) {
        tt.push(formatNumber(time.getSeconds()));
    }
    var val = tt.join(opts.separator);
    this.value = val;
    this.el.val(val);

    //  this.highlight();

    function formatNumber(value) {
        return (value < 10 ? '0' : '') + value;
    }
}

UCML.TimeBox.prototype.doSpin = function (down) {
    var opts = this;
    var val = this.el.val();
    if (val == '') {
        val = [0, 0, 0].join(opts.separator);
    }
    var vv = val.split(opts.separator);
    for (var i = 0; i < vv.length; i++) {
        vv[i] = parseInt(vv[i], 10);
    }
    if (down == true) {
        vv[opts.highlight] -= opts.increment;
    } else {
        vv[opts.highlight] += opts.increment;
    }
    this.el.val(vv.join(opts.separator));
    this.fixValue();
    this.setHighlight();
    this.value = this.getValue();
    this.setValue(this.value);
}

UCML.reg("UCML.TimeBox", UCML.TimeBox);

UCML.reg("UCML.Timebox", UCML.TimeBox);
