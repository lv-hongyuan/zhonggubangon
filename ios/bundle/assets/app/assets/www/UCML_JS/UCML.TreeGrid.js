﻿UCML.TreeGrid = function (id) {
    this.items = {};
    this.rootItems = [];
    this.subDataList = {};
    UCML.TreeGrid.superclass.constructor.call(this, id);
}

UCML.extend(UCML.TreeGrid, UCML.DataGrid, {
    havePage: true,
    /**   
    * @property expandDepth 
    * @description 节点展开层次深度
    * @type int
    */
    expandDepth: 0,
    haveChildField: false,
    haveChildFieldName: "haveChild", //默认为haveChild
    beforeRender: function () {
        UCML.TreeGrid.superclass.beforeRender.call(this);
        if (this.columns && this.columns.length > 0) {
            this.columns[0].cellFormatter = this.renderNodeCell;
        }
    }
    , setEditOffset: function (cell, obj) {
        if (this.columns[0].fieldName == obj.fieldName) {
            var title = $(".tree-title", cell);
            var offset = title.offset();
            //     obj.top = offset.top;
            obj.left = offset.left;
            obj.width = cell.offset().left + cell.outerWidth() - obj.left;
            //     obj.height = title.outerHeight();
        }
        return UCML.TreeGrid.superclass.setEditOffset.call(this, cell, obj);
    }
    ,
    addItem: function (OID) {
        this.items[OID] = {};
        this.items[OID].childs = [];
        return this.items[OID];
    }
    , renderNodeCell: function (v) {

        var str = "";
        var POID = this.dataTable.getFieldValue(this.ParentFieldName);
        var level;
        if (!this.items[v.OID]) {
            this.addItem(v.OID);
            this.items[v.OID].parentOID = POID;
        }
        else {
            this.items[v.OID].parentOID = POID;
        }

        if (this.items[POID]) {
            if (this.items[POID].childs.indexOf(v.OID) == -1) {
                this.items[POID].childs.add(v.OID);
            }
            this.items[v.OID].level = this.items[POID].level + 1;
        }
        else {
            this.items[v.OID].level = 1;
            if (this.rootItems.indexOf(v.OID) == -1) {
                this.rootItems.add(v.OID);
            }
        }

        if (this.haveChildField && this.haveChildFieldName) {
            this.items[v.OID].expanded = this.haveChild(v.OID);
        }
        else if ((this.items[v.OID].level < this.expandDepth || this.expandDepth == -1)) {
            this.items[v.OID].expanded = this.haveChild(v.OID);
        }

        return this.getRenderNodeStr(this.items[v.OID], v.OID, v.text);
    }
    ,
    //获取子节点数据
    setSubTreeData: function (OID) {
        this.items[OID].loaded = true;
        var text = this.BusinessObject.getTreeGridSubData(this.dataTable.BCName, this.ParentFieldName, OID, "", null, null, 0, -1, this);
        var oldIndex = this.dataTable.recordIndex;
        var index = this.dataTable.getRecordCount();
        var datas = eval(text) || {};
        for (var i = 0; i < datas.length; i++) {
            var bcValue = datas[i].data;
            var strBCBase = datas[i].BCName + "Base";
            var bcBase = eval(strBCBase);
            for (var j = 0; j < bcValue.length; j++) {
                var bcD = bcValue[j];
                bcBase.addRecord(bcD);
            }

            this.getItem(OID)["haveChilds"] = datas[i].haveChilds;
            //    bcBase.haveChilds = datas[i].haveChilds;
        }
    },
    getItem: function (OID) {
        return this.items[OID];
    }
    ,
    InsertRootTreeRow: function () {
        var oldIndex = this.dataTable.recordIndex;
        this.dataTable.Insert();
        var POID = this.dataTable.getEmptyGuid();
        //  if (this.dataTable.PrimaryKey != this.dataTable.TableName + "OID") {
        //      POID = "0";
        //  }
        this.dataTable.setFieldValue(this.ParentFieldName, POID);
    }
    ,
    InsertSubTreeRow: function () {
        var oldIndex = this.dataTable.recordIndex;
        var POID = this.dataTable.getOID();
        this.IsInsertSub = true;
        this.InsertSubOID = POID;
        this.dataTable.Insert();
        this.dataTable.setFieldValue(this.ParentFieldName, POID);
        this.IsInsertSub = false;
    }
    ,
    delChildRowByNode: function (node) {
        if (node && node.childs) {
            for (var i = 0; i < node.childs.length; i++) {
                this.delChildRow(node.childs[i]);
                UCML.TreeGrid.superclass.delRow.call(this, node.childs[i]);
            }
        }
    }
    ,
    delChildRow: function (OID) {
        var node = this.getItem(OID);
        this.delChildRowByNode(node);
    }
    ,
    delRow: function (OID) {
        var OID = OID || this.dataTable.getOID();
        var node = this.getItem(OID);
        if (node) {
            var POID = node.parentOID;
            var pNode = this.getItem(POID);
            if (pNode) {
                pNode.childs.remove(OID);
                var haveChilds = this.getHaveChild(OID);
                if (haveChilds) {
                    for (var i = 0; i < haveChilds.length; i++) {
                        if (haveChilds[i] == OID) {
                            haveChilds.remove(haveChilds[i]);
                            break;
                        }
                    }
                }
                this.delChildRowByNode(node);
                if (pNode && pNode.childs.length == 0) {
                    this.updateExpandIcon(POID, true);
                }
            }
        }
        UCML.TreeGrid.superclass.delRow.call(this, OID);
    }
    ,
    DeleteTreeRow: function (OID) {
        var OID = OID || this.dataTable.getOID();
        var node = this.getItem(OID);
        if (node) {
            if (node.childs.length > 0 || (this.haveChild(OID) && (node.haveChilds ? node.haveChilds.length > 0 : true))) {
                alert("还有子级数据，不能删除当前行!");
                return;
            }
        }
        this.dataTable.Delete();

    }
    ,
    onFieldChange: function (e) {
        if (e.fieldName == this.ParentFieldName) {
            var rowCount = this.dataTable.getRecordCount();

            var POID = e.value;
            var items = this.getItem(POID);
            if (items) {
                if (this.haveChild(POID)) {
                    this.doExpand(POID);
                }
                this.renderSubRows(POID, rowCount - 1, rowCount);
            }
            else {
                this.rootItems.remove(e.OID);
                this.rootItems.add(e.OID);
                this.renderRootRows(rowCount - 1, rowCount);
            }

            this.updateRow(POID);
            this.updateExpandIcon(POID, true);
            this.updataBody();
            //   this.setFocusRowAt(rowCount - 1);
        }
        else {
            UCML.TreeGrid.superclass.onFieldChange.call(this, e);
        }
    }
    ,
    addRow: function () {
        var item = this.addItem(this.dataTable.getOID());
        item.loaded = true;
    }
    ,
    getRenderNodeStr: function (node, OID, text) {
        OID = OID || this.dataTable.getOID();
        text = text || this.dataTable.getFieldValue(this.columns[0].fieldName);


        var indent = [];
        for (var j = 1; j < node.level; j++) {
            indent.add('<span class="tree-indent"></span>');
        }

        // var iconCls = "";
        if (node.childs.length > 0 || this.haveChild(OID)) {
            //    iconCls = "tree-folder";
            var expstr = node.expanded ? "tree-expanded" : "tree-collapsed";
            str = indent.join('') + "<span class='tree-hit " + expstr + "' ></span><span class='tree-icon tree-folder'></span><span class='tree-title' title='" + text + "'>" + text + "</span>";
        }
        else {
            //    iconCls = "tree-file";
            str = indent.join('') + "<span class='tree-indent'></span><span class='tree-icon tree-file'></span><span class='tree-title'  title='" + text + "'>" + text + "</span>";
        }
        return str;
    }
    ,
    getNodeIcon: function (OID) {

    }
    ,
    updateExpandIcon: function (OID, expanded) {
        OID = OID || this.dataTable.getOID();
        if (this.frozen) {
            row = this.getFrozenRowByOID(OID);
        }
        else {
            row = this.getRowByOID(OID);
        }
        if (row && row.length > 0) {
            var cell = this.getCell(row[0], 0);
            var nodeItem = this.getItem(OID);
            nodeItem.expanded = expanded;
            var text = this.getRenderNodeStr(nodeItem, OID, this.getTitle(cell));
            this.setCellText(cell, text);
        }
    }
    , renderRootRows: function (index, rowCount) {
        index = index || this.dataTable.getRecordCount() - 1;
        rowCount = rowCount || index + 1;

        if (this.frozen) {
            var frozenBodyStr = this.renderRows(this.frozenColumns, true, index, rowCount);
            this.frozenBodyTable.append(frozenBodyStr);

            var bodyStr = this.renderRows(this.notfrozenColumns, false, index, rowCount);
            this.bodyTable.append(bodyStr);
        }
        else {
            var bodyStr = this.renderRows(this.columns, true, index, rowCount);
            this.bodyTable.append(bodyStr);
        }

        this.updataBody();
    }
    ,
    loadData: function (data) {
        if (this.treeGirdHaveChilds && this.dataTable.MasterTable) {
            this.haveChilds = this.treeGirdHaveChilds[this.dataTable.MasterTable.getOID()] || null;
        }
        else {
            this.haveChilds = null;
        }
        this.items = {};
        this.rootItems = [];
        this.subDataList = {};
        UCML.TreeGrid.superclass.loadData.call(this, data);
    }
    ,
    renderSubRows: function (OID, index, rowCount) {
        index = index || this.dataTable.getRecordCount() - 1;
        rowCount = rowCount || index + 1;
        var OID = OID || this.dataTable.getOID();

        var items = this.getItem(OID);
        if (items && items.childs.length && items.childs.length > 0) {
            OID = getLastItem(items.childs[items.childs.length - 1], this);
        }

        function getLastItem(OID, opts) {
            var items = opts.getItem(OID);
            if (items.childs && items.childs.length == 0) {
                return OID;
            }
            else {
                return getLastItem(items.childs[items.childs.length - 1], opts);
            }
        }


        if (this.frozen) {
            var frozenBodyStr = this.renderRows(this.frozenColumns, true, index, rowCount);
            this.getFrozenRowByOID(OID).after(frozenBodyStr);

            var bodyStr = this.renderRows(this.notfrozenColumns, false, index, rowCount);
            this.getRowByOID(OID).after(bodyStr);
        }
        else {
            var bodyStr = this.renderRows(this.columns, true, index, rowCount);
            this.getRowByOID(OID).after(bodyStr);
        }
        this.updataBody();
    }
    ,
    getHaveChild: function (OID) {
        if (!this.haveChilds && !this.haveChildField) {
            this.haveChilds = eval(this.dataTable.BCName + '_ChildInfo');
            this.setTreeGridHaveChilds();
        }

        var parentItem = this.getItem(this.getItem(OID).parentOID);

        return parentItem ? parentItem["haveChilds"] : this.haveChilds;
    }
    ,
    haveChild: function (OID) {
        var haveChilds = this.getHaveChild(OID);
        var flag = false;

        //如果已经找到子节点定义
        if (haveChilds && haveChilds.length > 0) {
            for (var i = 0; i < haveChilds.length; i++) {
                if (haveChilds[i] == OID) {
                    flag = true;
                    break;
                }
            }
        }
        else if (this.haveChildField && this.haveChildFieldName) { //如果定义了 是否包含子节点对象
            var index = this.dataTable.getRecordIndex(); //记录当前索引
            this.dataTable.LocateOID(OID, this); //定位到当前数据行
            flag = this.dataTable.getFieldValue(this.haveChildFieldName) == "true";

            this.dataTable.setRecordIndex(index, this); //返回原来索引
        }
        return flag;
    },
    doExpand: function (OID) {
        var OID = OID || this.dataTable.getOID();
        this.items[OID].expanded = true;

        if (!this.items[OID].loaded) {

            var oldIndex = this.dataTable.recordIndex;
            var index = this.dataTable.getRecordCount();

            this.setSubTreeData(OID);

            var rowCount = this.dataTable.getRecordCount();
            this.renderSubRows(OID, index, rowCount);

            this.dataTable.recordIndex = oldIndex;

            this.setPagination();

        }
        else {
            this.setChildsState(OID, true);
        }
        this.updataBody();

    },
    onClick: function (el, e) {
        var target = $(e.target);
        if (target.hasClass("tree-collapsed")) {
            target.removeClass("tree-collapsed").addClass("tree-expanded");
            var row = this.findRow(target);
            if (row != false) {
                var OID = $(row).attr("OID");

                if (this.dataTable) {
                    OID = this.dataTable.GetOIDBy_GUID(OID)
                }
                this.expand(OID);
            }
        }
        else if (target.hasClass("tree-expanded")) {
            target.removeClass("tree-expanded").addClass("tree-collapsed");
            target.siblings(".tree-folder").removeClass("tree-folder-open");
            var row = this.findRow(target);
            if (row != false) {
                var OID = $(row).attr("OID");

                if (this.dataTable) {
                    OID = this.dataTable.GetOIDBy_GUID(OID);
                }
                this.collapsed(OID);
            }
            this.updataBody();
        }
        else {
            UCML.TreeGrid.superclass.onClick.call(this, el, e);
        }
    },
    expand: function (OID) {

        this.doExpand(OID);
    },
    collapsed: function (OID) {
        var OID = OID || this.dataTable.getOID();
        var node = this.items[OID];
        if (node) {
            node.expanded = false;
        }
        this.setChildsState(OID, false);
    },
    toggleRow: function (OID) {
        var OID = OID || this.dataTable.getOID();
        var node = this.items[OID];
        if (node && node.expanded) {
            this.collapsed(OID);
        }
        else {
            this.expand(OID);
        }
    }
    ,
    setChildsState: function (OID, v) {
        if (this.items[OID].childs) {
            for (var i = 0; i < this.items[OID].childs.length; i++) {
                var cOID = this.items[OID].childs[i];
                if (this.frozen) {
                    if (v) {
                        this.getFrozenRowByOID(cOID).show();
                        //                        if (this.items[OID].expanded) {
                        //                            this.setChildsState(cOID, v);
                        //                        }
                        this.setChildsState(cOID, this.items[OID].expanded);
                    }
                    else {
                        this.getFrozenRowByOID(cOID).hide();
                        this.setChildsState(cOID, v);
                    }

                }

                if (v) {
                    if (this.items[OID].expanded && this.items[cOID].expanded) {
                        this.getRowByOID(cOID).show();
                        this.setChildsState(cOID, v);
                    }
                    else {
                        if (this.items[OID].expanded) {
                            this.getRowByOID(cOID).show();
                        }
                        else {
                            this.getRowByOID(cOID).hide();
                        }
                        this.setChildsState(cOID, false);
                    }
                }
                else {
                    this.getRowByOID(cOID).hide();
                    this.setChildsState(cOID, v);
                }
            }
        }
    },
    setTitle: function (td, e) {
        if (this.columns[0].fieldName == e.fieldName) {
            td = $(td);
            var title = $(".tree-title", td);
            title.html(e.value);
        }
    }
    ,
    getTitle: function (td) {
        td = $(td);
        var title = $(".tree-title", td);
        return title.html();
    }
    ,
    bindEvents: function () {
        //    UCML.on(this.el, "click", this.onExpand, this);
        UCML.TreeGrid.superclass.bindEvents.call(this);
        this.on("fieldchange", this.setTitle, this);
        this.on("endedit", this.setTitle, this);

        this.on("afterrowrender", this.setRenderSubRows, this);

        if (!this.haveChildField && this.dataTable.MasterTable) {
            this.dataTable.MasterTable.on("onloadChildData", function () {
                this.setTreeGridHaveChilds();
            }, this);
        }

    }
    ,
    setTreeGridHaveChilds: function () {
        this.treeGirdHaveChilds = this.treeGirdHaveChilds || {};
        if (this.dataTable.MasterTable) {
            this.treeGirdHaveChilds[this.dataTable.MasterTable.getOID()] = eval(this.dataTable.BCName + '_ChildInfo');
        }
    },
    setRenderSubRows: function (v, rows) {
        if (this.IsInsertSub && this.items[this.InsertSubOID] && this.items[this.InsertSubOID].loaded && this.items[this.InsertSubOID]["index"] != undefined) {
            
        }
        else if (this.items[v.OID].expanded && !this.items[v.OID].loaded) {
            var oldIndex = this.dataTable.recordIndex;
            var index = this.dataTable.getRecordCount();
            this.setSubTreeData(v.OID);

            var rowCount = this.dataTable.getRecordCount();
            this.items[v.OID]["index"] = index;
            this.items[v.OID]["rowCount"] = rowCount;
            var subRows = this.renderRows(v.columns, v.frozen, index, rowCount);
            rows.add(subRows);

            this.dataTable.recordIndex = oldIndex;

            this.setPagination();
        }
        else if (this.items[v.OID].loaded && this.items[v.OID]["index"] != undefined) {
            var subRows = this.renderRows(v.columns, v.frozen, this.items[v.OID]["index"], this.items[v.OID]["rowCount"]);
            rows.add(subRows);
        }
    }
    ,
    //上一页
    onLoadPrevPage: function () {
        if (this.dataTable) {
            var startPos = this.dataTable.StartPos;
            var readCount = this.dataTable.ReadCount;
            var totalRecordCount = this.dataTable.nTotalRecordCount;
            var defaultPageCount = this.dataTable.DefaultPageCount;
            if (startPos == 0) return;
            var nStartPoseEx = parseInt(startPos) - parseInt(defaultPageCount);
            if (nStartPoseEx < 0) nStartPoseEx = 0;
            this.updateData("00000000-0000-0000-0000-000000000000", nStartPoseEx, defaultPageCount);
        }
    }
    ,
    updateData: function (parentOID, nStartPoseEx, nRecords) {
        var SQLCondi = "";
        if (this.dataTable.MasterTable) {
            var ForeignKey = this.dataTable.ForeignKey;

            //如果是自定义主外键时
            if (this.dataTable.getCustomRela()) {

                for (var m = 0; m < ForeignKey.ChildColumns.length; m++) {
                    parentOID = this.dataTable.MasterTable.getFieldValue(ForeignKey.ParentColumns[m]);
                    pkField = ForeignKey.ChildColumns[m];
                    if (m > 0) {
                        SQLCondi += " and ";
                    }
                    SQLCondi += this.dataTable.TableName + "." + pkField + " = '" + parentOID + "' ";
                }
            }
            else {
                SQLCondi = this.dataTable.TableName + "." + this.dataTable.LinkKeyName + " = '";
                if (this.dataTable.PK_COLUMN_NAME != "") {
                    SQLCondi += this.dataTable.MasterTable.getFieldValue(this.dataTable.PK_COLUMN_NAME) + "' ";
                }
                else {
                    SQLCondi += this.MasterTable.getOID() + "' ";
                }
            }
        }

        var text = this.BusinessObject.getTreeGridSubData(this.dataTable.BCName, this.ParentFieldName, parentOID, SQLCondi, null, null, nStartPoseEx, nRecords, this);
        var datas = eval(text);
        for (var i = 0; i < datas.length; i++) {
            var bcValue = datas[i].data;
            var strBCBase = datas[i].BCName + "Base";
            var bcBase = eval(strBCBase);
            this.haveChilds = datas[i].haveChilds;
            window[this.dataTable.BCName + '_ChildInfo'] = this.haveChilds;
            bcBase.SetDataPacketAndChild(bcValue, this);
        }
    }
    ,
    //下一页
    onLoadMoreData: function () {
        if (this.dataTable) {
            var startPos = this.dataTable.StartPos;
            var readCount = this.dataTable.ReadCount;
            var totalRecordCount = this.dataTable.nTotalRecordCount;
            var defaultPageCount = this.dataTable.DefaultPageCount;
            if ((parseInt(startPos) + parseInt(readCount)) >= parseInt(totalRecordCount)) {
                return;
            }
            var nStartPoseEx = parseInt(startPos) + parseInt(readCount);
            this.updateData("00000000-0000-0000-0000-000000000000", nStartPoseEx, defaultPageCount);
            this.dataTable.StartPos = nStartPoseEx;
        }
    },
    //首页
    onLoadFirstPage: function () {
        if (this.dataTable) {
            var nStartPoseEx = 0;
            var defaultPageCount = this.dataTable.DefaultPageCount;
            this.updateData("00000000-0000-0000-0000-000000000000", nStartPoseEx, defaultPageCount);
        }
    }
    ,
    //末页
    onLoadLastPage: function () {
        if (this.dataTable) {
            var startPos = this.dataTable.StartPos;
            var readCount = this.dataTable.ReadCount;
            var totalRecordCount = this.dataTable.nTotalRecordCount;
            var defaultPageCount = this.dataTable.DefaultPageCount;
            var pageNum = parseInt(parseInt(totalRecordCount) / parseInt(defaultPageCount));
            if (parseInt(parseInt(totalRecordCount) % parseInt(defaultPageCount)) != 0) pageNum++;
            var nStartPoseEx = (pageNum - 1) * parseInt(defaultPageCount);
            if (nStartPoseEx < 0) nStartPoseEx = 0;
            this.updateData("00000000-0000-0000-0000-000000000000", nStartPoseEx, defaultPageCount);
        }
    }
});

