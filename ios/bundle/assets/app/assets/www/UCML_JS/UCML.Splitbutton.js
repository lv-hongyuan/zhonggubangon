﻿UCML.Splitbutton = function (id) {
    this.menuId = null;
    this.duration = 100;
    this.split = true;
    UCML.Splitbutton.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Splitbutton, UCML.Linkbutton, {
});

UCML.Splitbutton.prototype.init = function () {
    UCML.Splitbutton.superclass.init.call(this);
    this.menuId = this.el.attr('menuId');
    this.duration = (parseInt(this.el.attr('duration')) || this.duration);

    var opts = this;
    var menubtn;
    var ice = "s";
    this.el.removeClass('s-btn-active s-btn-plain-active');
    if (this.split) {

        this.el.find(".l-btn-text").append('<span class="' + ice + '-btn-downarrow">&nbsp;</span>');
        menubtn = this.el.find('.s-btn-downarrow');
    }
    else {
        ice = "m";
        this.el.find(".l-btn-text").append('<span class="' + ice + '-btn-downarrow">&nbsp;</span>');
        menubtn = this.el;
    }
    //  if (this.menuId) {
    this.menu = new UCML.Menu();
    this.menu.onShow = function () {
        this.el.addClass((this.plain == true) ? '' + ice + '-btn-plain-active' : '' + ice + '-btn-active');
    };
    this.menu.onHide = function () {
        this.el.removeClass((this.plain == true) ? '' + ice + '-btn-plain-active' : '' + ice + '-btn-active');
    }
    //     this.menu.addMenuItem('aa', 'aa');
    //   this.menu.init();
    // }

    menubtn.unbind('.Splitbutton');
    this.el.unbind('.Splitbutton');
    if (opts.disabled == false && opts.menu) {
        menubtn.bind('click.Splitbutton', function () {
            UCML.Menu.hideMenuAll();
            opts.showMenu();
            return false;
        });
        var timeout = null;
        this.el.bind('mouseenter.Splitbutton', function () {
            UCML.Menu.hideMenuAll();
            timeout = setTimeout(function () {
                opts.showMenu();
            }, opts.duration);
            return false;
        }).bind('mouseleave.Splitbutton', function () {
            if (timeout) {
                clearTimeout(timeout);
            }
        });
    }
}

UCML.Splitbutton.prototype.showMenu = function () {
    if (this.disabled) {
        for (var i = 0; i < this.menu.items.length; i++) {
            var item = this.menu.items[i];
            item.el[0].disabled = true;
            item.el.unbind("click");
        }
    }
    var left = this.el.offset().left;
    if (left + this.menu.el.outerWidth() + 5 > $(window).width()) {
        left = $(window).width() - this.menu.el.outerWidth() - 5;
    }
    //this.menu.el.hide();
    //   $('.menu-top').menu('hide');
    this.menu.show({
        left: left,
        top: this.el.offset().top + this.el.outerHeight()
    });
    this.el.blur();
}

UCML.reg("UCML.Splitbutton", UCML.Splitbutton);


UCML.Menubutton = function (id) {
    UCML.Menubutton.superclass.constructor.call(this, id);
    this.split = false;
}

UCML.extend(UCML.Menubutton, UCML.Splitbutton, {
});

UCML.reg("UCML.Menubutton", UCML.Menubutton);