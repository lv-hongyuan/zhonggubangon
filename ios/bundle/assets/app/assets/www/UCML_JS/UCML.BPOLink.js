﻿/**
* @class UCML.BPOLink
* @extends UCML.Applet
* @description 编辑控件
* @param {object} id 容器控件id
*/
UCML.BPOLink = function (id) {

    UCML.BPOLink.superclass.constructor.call(this, id);
};
/**
* @class UCML.BPOLink
* @extends UCML.Applet
* @description 编辑控件,继承自UCML.Applet
*/
UCML.extend(UCML.BPOLink, UCML.Applet, {
    ctype: "UCML.BPOLink",
    scroll: "no",
    autoFrameHeight: true,
    setBusinessObjectURL: function (value) {
        if (this.frame && this.frame.length > 0) {
            this.frame[0].contentWindow.document.write('');
            //   this.frame[0].contentWindow.document.clear();
            //   this.frame[0].contentWindow.close();
            this.frame[0].src = "";
            this.frame.remove();
            delete this.frame;
            if ($.browser.msie) { CollectGarbage(); }
        }

        this.iframe = $("<iframe app='" + value + "' scrolling=" + this.scroll + " width='100%'   frameborder=0 ></IFRAME>");
        this.el.empty();

        var opts = this;

        this.iframe.bind("load", function () {
            if (opts.autoFrameHeight) {

                if ($) {
                    $(opts.iframe[0].contentWindow).bind("resize", function () {

                        opts.iframe.height($(opts.iframe[0].contentWindow.document.body).height());

                    });

                    opts.iframe.height($(opts.iframe[0].contentWindow.document.body).height());
                }


                opts.iframe.attr("height", '100%');
            }
        });



        this.el.append(this.iframe);
        this.setSize();
    }, setSize: function (param) {
        UCML.BPOLink.superclass.setSize.call(this, param);
        if (this.iframe) {
            if (!isNaN(this.width)) {
                this.iframe.width(this.el.width());

            }

            if (!isNaN(this.height)) {
                this.iframe.height(this.el.height());
            }

            if (this.autoFrameHeight) {
                this.iframe.height($(this.iframe[0].contentWindow.document.body).height());
            }

        }
    },

    destroy: function () {

        if (this.frame) {
            if (this.frame.length > 0) {
                this.frame[0].contentWindow.document.write('');
                this.frame[0].contentWindow.document.clear();
                //   this.frame[0].contentWindow.close();
                this.frame[0].src = "";

                if ($.browser.msie) { CollectGarbage(); }
            }

            this.frame.remove();
            delete this.frame;
        }
        UCML.BPOLink.superclass.destroy.call(this);

    }

});


UCML.BPOLink.prototype.setHight = function (value) {
    if (this.iframe) {
        //this.iframe.width(this.el.width());
        this.iframe.height(value);
    }
}

UCML.BPOLink.prototype.setHightAuto = function () {
    if (this.iframe) {
        var dh = this.iframe[0].contentWindow.document.documentElement.scrollHeight;
        var bh = this.iframe[0].contentWindow.document.body.scrollHeight;
        var h = Math.max(bh, dh);
        this.iframe.height(h);
    }
}
