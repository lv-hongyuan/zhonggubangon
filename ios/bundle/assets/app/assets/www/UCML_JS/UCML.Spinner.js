﻿UCML.Spinner = function (id) {
    this.addEvents("blur", "focus");
    UCML.Spinner.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Spinner, UCML.Input, {
    ctype: "UCML.Spinner",
    autoEl: 'input',
    width: 'auto',
    value: '',
    min: null,
    max: null,
    increment: 1,
    editable: true,
    disabled: false,
    spin: function (down) { }, 
    onSpinUp: function () { },
    onSpinDown: function () { },
    accelerate: false
});

UCML.Spinner.prototype.init = function () {
    UCML.Spinner.superclass.init.call(this);

    this.setDisabled(this.disabled);
    this.setSize();
    this.validatebox = new UCML.Validatebox(this.el);
    return this.spinner;
}


UCML.Spinner.prototype.onRender = function () {

    this.spinner = $('<span class="spinner"></span>').insertAfter(this.el);
    var table = $('<table width="100%" style="width:100%; table-layout:fixed" border=0 cellSpacing=0 ' +
    'cellPadding=0><tr><td vAlgign="top"></td><td  width="18">' +
    '<span class="spinner-arrow">' +
				'<span class="spinner-arrow-up"></span>' +
				'<span class="spinner-arrow-down"></span>' +
				'</span>' +
    '</td></tr></table>').appendTo(this.spinner);
 
    this.el.removeClass().addClass('spinner-text').prependTo(table.find("td")[0]);
    this.el.removeAttr('disabled');

    this.el.val(this.value);
    this.el.attr('readonly', !this.editable);
}


UCML.Spinner.prototype.setSize = function (width, height) {
    if (width) {
        this.width = width;
    }
    if (height) {
        this.height = height;
    }

    if (!isNaN(this.width)) {
        if ($.boxModel == true) {
            this.spinner.width(this.width - (this.spinner.outerWidth() - this.spinner.width()));
        } else {
            this.spinner.width(this.width);
        }
    }
}


UCML.Spinner.prototype.setOffset = function (top, left) {
    this.spinner.offset({ top: top, left: left });
}

UCML.Spinner.prototype.handleMouseWheel = function (el, e) {
    //disable scrolling when not focused
    //    if (this.wrap.hasClass('x-trigger-wrap-focus') == false) {
    //        return;
    //    }

    return;
    var delta = e.getWheelDelta();
    if (delta > 0) {
        //  this.onSpinUp();
        opts.spin.call(opts, false);
        opts.onSpinUp.call(opts);
        opts.validatebox.validate();

        e.stopEvent();
    }
    else {
        if (delta < 0) {
            //     this.onSpinDown();
            opts.spin.call(opts, false);
            opts.onSpinDown.call(opts);
            opts.validatebox.validate();
            e.stopEvent();
        }
    }


}

UCML.Spinner.prototype.focus = function () {
    this.spinner.find('input.spinner-text')[0].focus();
}

UCML.Spinner.prototype.bindEvents = function () {
    //   UCML.Spinner.superclass.bindEvents.call(this);
    var opts = this;
    var spinner = this.spinner;
    //  UCML.on(this.el, "mousewheel", this.handleMouseWheel, this);

    spinner.find('.spinner-arrow-up,.spinner-arrow-down').unbind('.spinner');
    if (!opts.disabled) {

        UCML.on(document, "mousedown.spinner", function () { this.fireEvent("blur"); }, this);
        var input = this.spinner.find('.spinner-text');

        input.bind('mousedown.spinner', function (e) {
            e.stopPropagation();
        });

        input.bind('focus.spinner', function () {
            if (opts.fireEvent("focus") === true) {
                //      opts.focus();
            }
        });

        spinner.bind('mousedown.spinner', function (e) {
            return false;
        });






        spinner.find('.spinner-arrow-up').bind('mouseenter.spinner', function () {
            $(this).addClass('spinner-arrow-hover');
        }).bind('mouseleave.spinner', function () {
            $(this).removeClass('spinner-arrow-hover');
        }); //.bind('click.spinner', function () {
        //  opts.spin.call(opts.scope, false);
        //  opts.onSpinUp.call(opts.scope);
        //  opts.validatebox.validate();
        // });

        //    var up = new UCML.Commponent(spinner.find('.spinner-arrow-up'));
        //    up.init();

        var up = new UCML.util.ClickRepeater(spinner.find('.spinner-arrow-up'), {
            accelerate: this.accelerate,
            stopDefault: false
        });
        up.on("click", function () {
            opts.spin.call(opts, false);
            opts.onSpinUp.call(opts);
         //   opts.validatebox.validate();
        });


        spinner.find('.spinner-arrow-down').bind('mouseenter.spinner', function () {
            $(this).addClass('spinner-arrow-hover');
        }).bind('mouseleave.spinner', function () {
            $(this).removeClass('spinner-arrow-hover');
        }); //.bind('click.spinner', function () {
        // opts.spin.call(opts.scope, true);
        // opts.onSpinDown.call(opts.scope);
        //  opts.validatebox.validate();
        //});
        var down = new UCML.util.ClickRepeater(spinner.find('.spinner-arrow-down'), {
            accelerate: this.accelerate,
            stopDefault: false
        });
        down.on("click", function () {
            opts.spin.call(opts, true);
            opts.onSpinDown.call(opts);
         //   opts.validatebox.validate();
        });
    }
}

UCML.Spinner.prototype.setDisabled = function (disabled) {
    var opts = this;
    if (disabled) {
        opts.disabled = true;
        this.el.attr('disabled', true);
    } else {
        opts.disabled = false;
        this.el.removeAttr('disabled');
    }
}

UCML.Spinner.prototype.parseOptions = function () {
    this.width = (parseInt(this.dom.style.width) || undefined);
    this.value = (this.el.val() || undefined);
    this.min = this.el.attr('min');
    this.max = this.el.attr('max');
    this.increment = (parseFloat(t.attr('increment')) || undefined);
    this.editable = (t.attr('editable') ? t.attr('editable') == 'true' : undefined);
    this.disabled = (t.attr('disabled') ? true : undefined);
}

UCML.Spinner.prototype.destroy = function () {
    var spinner = this.spinner;
    this.validatebox.destroy();
    spinner.remove();

}

UCML.Spinner.prototype.enable = function () {
    this.setDisabled(false);
    this.bindEvents();
}
UCML.Spinner.prototype.disable = function () {
    this.setDisabled(true);
    this.bindEvents();
}
UCML.Spinner.prototype.getValue = function () {
    return this.el.val();
}
UCML.Spinner.prototype.setValue = function (value) {
    this.value = value;
    this.el.val(value);

}
UCML.Spinner.prototype.clear = function () {
    this.value = '';
    this.el.val('');
}