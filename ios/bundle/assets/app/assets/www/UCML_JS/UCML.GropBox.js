﻿UCML.GropBox = function (id) {
    this.title = null;
    UCML.Container.superclass.constructor.call(this, id);
}

UCML.extend(UCML.GropBox, UCML.Container, {
    ctype: "UCML.GropBox"
})


UCML.GropBox.prototype.init = function () {
    UCML.GropBox.superclass.init.call(this);
}

UCML.GropBox.prototype.setProperties = function () {
    UCML.GropBox.superclass.setProperties.call(this);
    this.title = this.el.attr('title') || this.title; // 标题名称
}

UCML.GropBox.prototype.onRender = function () {
    if (this.title) {
        this.legend = $('<legend>' + this.title + '</legend>').prependTo(this.el);
    }
    //   this.body = $('<div class="gropbox_body">' + this.title + '</div>').prependTo(this.el);
}

UCML.GropBox.prototype.setSize = function (param) {
    UCML.GropBox.superclass.setSize.call(this, param);
}