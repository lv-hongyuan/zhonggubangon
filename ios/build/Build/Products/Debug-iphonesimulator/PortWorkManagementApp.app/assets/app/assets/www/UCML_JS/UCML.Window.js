﻿UCML.Window = function (id) {
    UCML.Window.superclass.constructor.call(this, id);

}

//层级
UCML.Window.zIndex = 9000;

UCML.extend(UCML.Window, UCML.Panel, {
    draggable: true,
    resizable: false,
    showShadow: true,
    modal: false,
    border: false,
    closable: true,
    onRender: function () {
        this.headerCls = "window-header " + this.headerCls;
        this.bodyCls = "window-body " + this.bodyCls;
        this.cls = "window " + this.cls;
        UCML.Window.superclass.onRender.call(this);

        if (this.mask) {
            this.mask.remove();
        }
        if (this.modal == true) {
            this.mask = $('<div class="window-mask"></div>', this.window.document).appendTo(this.window.document.body);
            this.mask.css({

                width: getPageArea(this.window.document).width,
                height: getPageArea(this.window.document).height,
                display: 'none'
            });
        }

        // create shadow
        //  if (this.shadow) this.shadow.remove();
        if (this.showShadow == true) {
            this.shadow = $('<div class="window-shadow"></div>', this.window.document).insertAfter(this.el);
            this.shadow.css({
                display: 'none'
            });
        }
    },
    bindEvents: function () {
        var opts = this;

        if (this.draggable) {
            this.el.UCML_draggable({
                document: opts.window.document,
                handle: '>div.panel-header>div.panel-title',
                //        disabled: state.options.draggable == false,
                onStartDrag: function (e) {

                    //   if (opts.mask) opts.mask.css('z-index', UCML.Window.zIndex);
                    //   if (opts.shadow) opts.shadow.css('z-index', UCML.Window.zIndex);
                    //   opts.el.css('z-index', UCML.Window.zIndex);

                    if (!opts.proxy) {
                        opts.proxy = $('<div class="window-proxy"></div>', opts.window.document).insertAfter(opts.el);
                    }
                    opts.proxy.css({
                        display: 'none',
                        zIndex: UCML.Window.zIndex,
                        left: e.data.left,
                        top: e.data.top,
                        width: ($.boxModel == true ? (opts.el.outerWidth() - (opts.proxy.outerWidth() - opts.proxy.width())) : opts.el.outerWidth()),
                        height: ($.boxModel == true ? (opts.el.outerHeight() - (opts.proxy.outerHeight() - opts.proxy.height())) : opts.el.outerHeight())
                    });
                    setTimeout(function () {
                        if (opts.proxy) opts.proxy.show();
                    }, 500);
                },
                onDrag: function (e) {
                    opts.proxy.css({
                        display: 'block',
                        left: e.data.left,
                        top: e.data.top
                    });
                    return false;
                },
                onStopDrag: function (e) {

                    opts.left = e.data.left;
                    opts.top = e.data.top;
                    //  $(target).window('move');
                    opts.move();
                    opts.proxy.remove();
                    opts.proxy = null;
                }
            });

            if (this.window && (opts.alignHeight == true || opts.alignWidth == true || UCML.isEmpty(opts.ratioHeight) == false || UCML.isEmpty(opts.ratioWidth) == false)) {
                $(this.window).resize(function () {
                    opts.resize();
                    if (opts.mask) {
                        opts.mask.css({
                            width: $(opts.window).width(),
                            height: $(opts.window).height()
                        });
                        setTimeout(function () {
                            if (opts.mask) {
                                opts.mask.css({
                                    width: getPageArea(opts.window.document).width,
                                    height: getPageArea(opts.window.document).height
                                });
                            }
                        }, 50);
                    }
                });
            }
        }

        if (this.resizable) {
            this.el.UCML_resizable({
                document: opts.window.document,
                onStartResize: function (e) {
                    if (!opts.proxy) {
                        opts.proxy = $('<div class="window-proxy"></div>', opts.window.document).insertAfter(opts.el);
                    }
                    opts.proxy.css({
                        zIndex: UCML.Window.zIndex,
                        left: e.data.left,
                        top: e.data.top,
                        width: ($.boxModel == true ? (e.data.width - (opts.proxy.outerWidth() - opts.proxy.width())) : e.data.width),
                        height: ($.boxModel == true ? (e.data.height - (opts.proxy.outerHeight() - opts.proxy.height())) : e.data.height)
                    });
                },
                onResize: function (e) {
                    opts.proxy.css({
                        left: e.data.left,
                        top: e.data.top,
                        width: ($.boxModel == true ? (e.data.width - (opts.proxy.outerWidth() - opts.proxy.width())) : e.data.width),
                        height: ($.boxModel == true ? (e.data.height - (opts.proxy.outerHeight() - opts.proxy.height())) : e.data.height)
                    });
                    return false;
                },
                onStopResize: function (e) {
                    opts.left = e.data.left;
                    opts.top = e.data.top;
                    opts.width = e.data.width;
                    opts.height = e.data.height;
                    //    debugger;
                    opts.resize();
                    opts.proxy.remove();
                    opts.proxy = null;
                }
            });
        }
    },
    //隐藏窗体
    close: function () {
        UCML.Window.superclass.close.call(this);
        if (this.closed == true) {
            if (this.shadow) this.shadow.hide();
            if (this.mask) this.mask.hide();
        }
    },
    //开启窗体
    open: function () {
        UCML.Window.superclass.open.call(this);
        if (this.mask) {
            this.mask.css({
                display: 'block',
                zIndex: UCML.Window.zIndex++
            });
        }
        if (this.shadow) {
            this.shadow.css({
                //      display: 'block',
                zIndex: UCML.Window.zIndex++
                /*
                left: this.left,
                top: this.top,
                width: this.el.outerWidth(),
                height: this.el.outerHeight()*/
            });
        }
        this.el.css('z-index', UCML.Window.zIndex++);

        this.resize();
    },
    getOffsetForWindow: function (targetEl) {
        var offset = targetEl.offset();
        var left = offset.left;
        var top = offset.top + targetEl.outerHeight(true);

        var elHeight = parseInt(this.height);
        if (top + elHeight > $(window).height() + $(document).scrollTop()) {
            top = offset.top - elHeight;
        }

        top = top < 0 ? 0 : top;

        var elWidth = parseInt(this.width);

        if (left + elWidth > $(window).width() + $(document).scrollLeft()) {
            left = (left + targetEl.outerWidth(true)) - elWidth;
        }

        left = left < 0 ? 0 : left;

        return { left: left, top: top };
    },
    setSize: function (param) {
        UCML.Window.superclass.setSize.call(this, param);

        if (this.left == null) {
            var width = this.width;
            if (isNaN(width)) {
                width = this.el.outerWidth();
            }
            this.left = ($(this.window).width() - width) / 2 + $(this.window.document).scrollLeft();
            this.left = this.left < 0 ? 0 : this.left;
        }
        if (this.top == null) {
            var height = this.height;
            if (isNaN(height)) {
                height = this.el.outerHeight();
            }
            this.top = ($(this.window).height() - height) / 2 + $(this.window.document).scrollTop();
            this.top = this.top < 0 ? 0 : this.top;
        }

        this.el.css({
            left: this.left,
            top: this.top
        });

        var shadowWidth = this.el.outerWidth();
        var shadowHeight = this.el.outerHeight();

        if ($.boxModel == true) {
            shadowWidth = shadowWidth - (shadowWidth - this.el.width());
            shadowHeight = shadowHeight - (shadowHeight - this.el.height());
        }

        if (this.shadow) {
            this.shadow.css({
                left: this.left,
                top: this.top,
                width: shadowWidth,
                height: shadowHeight
            });
        }
    },
    //移动位置 {left:0,top:0}
    move: function (param) {
        UCML.Window.superclass.move.call(this, param);
        if (this.shadow) {
            this.shadow.css({
                left: this.left,
                top: this.top
            });
        }
    },
    //最小化
    minimize: function () {
        UCML.Window.superclass.minimize.call(this);
        if (this.shadow) this.shadow.hide();
        if (this.mask) this.mask.hide();
    },
    //折叠
    collapse: function () {
        UCML.Window.superclass.collapse.call(this);
        if (this.shadow) this.shadow.hide();
    },
    //展开
    expand: function () {
        UCML.Window.superclass.expand.call(this);
        if (this.shadow) {
            this.shadow.show();
        }
    },
    //销毁
    destroy: function () {
        UCML.Window.superclass.destroy.call(this);
        if (this.mask) {
            this.mask.remove();
            delete this.mask;
        }

        if (this.shadow) {
            this.shadow.remove();
            delete this.shadow;
        }
    }
});



/*
$(window).resize(function () {
$('.window-mask').css({
width: $(window).width(),
height: $(window).height()
});
setTimeout(function () {
$('.window-mask').css({
width: getPageArea().width,
height: getPageArea().height
});
}, 50);
});*/

function getPageArea(document) {
    if (document.compatMode == 'BackCompat') {//quirks mode怪异模型
        return {
            width: Math.max(document.body.scrollWidth, document.body.clientWidth),
            height: Math.max(document.body.scrollHeight, document.body.clientHeight)
        }
    } else {
        return {
            width: Math.max(document.documentElement.scrollWidth, document.documentElement.clientWidth),
            height: Math.max(document.documentElement.scrollHeight, document.documentElement.clientHeight)
        }
    }
}