﻿function jsonpcallback(o, methodName) {
    //alert(methodName);
}

/*
记录:
新增_servicePath属性，
更改getPath，setPath方法名称,
invoke为websevice请求方法,
*/
UCML.BusinessUnit = function (el) {
    this.CodeTableList = new Array();
    this.onLoad = function () { };
    this.el = window || el;
    UCML.apply(el, this);
    UCML.BusinessUnit.superclass.constructor.call(this);
    this.UseTableList = new Array();
    this.applets = new Array();
    this.addEvents("onInit", "beforeOpen", "afterOpen", "onLoad", "onLoadData", "onBeforeLoadData", "onAfterLoadData", "beforerender", "render", "afterrender", "AfterCondiQuery");
    return this;
}

UCML.extend(UCML.BusinessUnit, UCML.util.Observable, {
    servicePath: "", //服务URL
    initState: false,
    dataLoading: false,
    promptSubmit: true,
    theBPOName: "",
    theDeltaData: null,
    fitWindow: false, //窗口适应模式
    getPath: function () {
        return this.servicePath;
    },
    setPath: function (servicePath) {
        this.servicePath = servicePath;
    },
    setSkin: function (name, value) {
        var ucmlappCss = document.getElementById("ucmlappCss");
        if (ucmlappCss) {
            ucmlappCss.href = UCMLLocalResourcePath + value + "css/ucmlapp.css";
        }

        this.fireEvent('skinChange', name, value);
        if (this.el.skinChange) {
            this.el.skinChange(name, value);
        }
    },
    open: function () {
        setTimeout(function () {
            //单元入口
            var path = getServicePath() + "UCMLWebServiceEntryForPostMessage.html";


            if ($("#bpo_frame").length == 0) {
                $(document.body).append("<iframe id='bpo_frame' app=" + path + " style='display:none'></iframe>");
            }
        }, 200);
        //  window["openerWindow"] = window["openerWindow"] || window["opener"];
		
        this.fireEvent('beforeOpen');
        if (this.el.onBeforeOpen) {
            this.el.onBeforeOpen();
        }

        UCML.applyIf(this.el, this);
        if (this.el.PrepareColumn) {
            this.el.PrepareColumn();
        }


        this.fireEvent('beforerender');
        if (this.el.beforeRender) {
            this.el.beforeRender();
        }

        if (this.fitWindow) {
            $('html').css({
                height: '100%',
                overflow: 'hidden'
            });
            $('body').css({
                height: '100%',
                overflow: 'hidden',
                border: 'none'
            });

            var MainPanel = new UCML.Container({ el: $("#MainPanel"), alignWidth: true, alignHeight: true });
            
        }

        if (this.el.setTemplate) {
            this.el.setTemplate();
        }


        if (this.el.onRender) {
            this.el.onRender();
            this.fireEvent('render');
        }
        if (this.el.afterRender) {
            this.el.afterRender();
            this.fireEvent('afterrender');
        }

        if (this.el.onInit) {
            this.el.onInit();
        }

        if (this.el.currentWindow) {
            this.el.currentWindow.setSize();
        }
		
        if (this.el.BusinessInit) {
		
            var opts = this;

            var bu_methodName = "BusiViewString";
            var bu_params = {};
			var sURL=window.location.href.toString();
			if(sURL.indexOf("?")>0) {
				var arrParams=sURL.split("?");
				bu_params={urlParams:arrParams[1]}
			}
            if (window["BusiView_MethodName"]) {
                bu_methodName = window["BusiView_MethodName"];
            }
            if (window["BusiView_Params"]) {
                bu_params = window["BusiView_Params"];
            }
            var config = { methodName: bu_methodName, params: bu_params, onSuccess: function (obj, text, methodName) {
			
                var o = eval(text);
				if(this.el.setFlowBusiness) {
					this.el.setFlowBusiness()
				}
				
                opts.fireEvent("onLoad");

                window.RecordCountInfo = RecordCountInfo;
				if(BusinessData && opts.el.BusinessData == null)
				{
					opts.el.BusinessData = BusinessData;
				}
                opts.loadData(opts.el.BusinessData);

                opts.afterLoadData();
                opts.el.BusinessInit();
            }, onFailure: this.failed_ChangeBusiViewString, useGet: false
            };
            return this.invoke(config);



        }
        else {
            this.fireEvent("onLoad");
            this.loadData(this.el.BusinessData);
            this.afterLoadData();
        }


        this.fireEvent('afterOpen');
        if (this.el.onAfterOpen) {
            this.el.onAfterOpen();
        }
        this.initState = true;
    }
    ,
    //处理流程中的业务
    setFlowBusiness: function () {
        if (this.el.BusinessData) {
            this.flowActionlist = {}; //流程动作列表


            //如果有流程中的权限数据
            var permissionCodeList = this.el.BusinessData['PermissionCodeEx'];
            if (permissionCodeList) {
                var appletColumnPerm = {};
                var appletButtonPerm = {};

                for (var i = 0; i < permissionCodeList.length; i++) {
                    var objectType = permissionCodeList[i][1];
                    var appletName = permissionCodeList[i][10];
                    if (objectType == 3) {//列
                        var ColumnPerm = appletColumnPerm[appletName] || [];

                        var obj = {};
                        obj["fieldName"] = permissionCodeList[i][3];
                        obj["allowModify"] = permissionCodeList[i][4];
                        obj["allowVisible"] = permissionCodeList[i][5];
                        ColumnPerm.add(obj);

                        appletColumnPerm[appletName] = ColumnPerm;
                    }
                    else if (objectType == 2)//按钮
                    {
                        var ButtonPerm = appletButtonPerm[appletName] || [];

                        var obj = {};
                        obj["caption"] = permissionCodeList[i][3];
                        obj["enabled"] = permissionCodeList[i][4];
                        obj["visible"] = permissionCodeList[i][5];
                        ButtonPerm.add(obj);

                        appletButtonPerm[appletName] = ButtonPerm;
                    }
                    else if (objectType == 4)//流程操作
                    {
                        //type 1基础操作 2常用操作

                        var obj = { name: permissionCodeList[i][3], caption: permissionCodeList[i][2], type: permissionCodeList[i][7] };

                        this.flowActionlist[permissionCodeList[i][3]] = obj;

                    }
                }
            }
            this.el["appletColumnPerm"] = appletColumnPerm;
            this.el["appletButtonPerm"] = appletButtonPerm;
        }

    }
    ,
    loadData: function (data) {
        // alert("loadData---" + window.RecordCountInfo["BC_TORDER0217"].ReadCount);
        if (data) {

            this.NotifyTable(data);
            this.fireEvent("onLoadData", data);
            this.dataLoading = true;
        }


    },
    addApplet: function (applet) {
        //   applet.BusinessObject = this;
        this.applets.add(applet);
    },
    afterLoadData: function () {
        this.fireEvent("onAfterLoadData");
    },
    addTask: function (methodName) {
        this.taskList = this.taskList || {};
        this.taskList[methodName] = {};
        this.taskList[methodName]["status"] = 0;
    },
    setTask: function (methodName, config) {
        this.taskList = taskList || {};
        this.taskList[methodName] = {};
        UCML.apply(this.taskList[methodName], config);
    },
    getTask: function (methodName) {
        this.taskList = this.taskList || {};
        return this.taskList[methodName];
    },
    removeTask: function (methodName) {
        this.taskList = this.taskList || {};
        delete this.taskList[methodName];
    },
    invoke: function (servicePath, methodName, useGet, params, onSuccess, onFailure, userContext, async, toTask) {
        //json参数传入时
        if (arguments.length == 1) {
            var p = servicePath;
            methodName = p.methodName;
            useGet = p.useGet;
            params = p.params || {};
            onSuccess = p.onSuccess;
            onFailure = p.onFailure;
            onFailure = p.onFailure;
            userContext = p.userContext;
            async = p.async;
            servicePath = p.servicePath;
        }
        servicePath = servicePath || this.servicePath;

        if (window["getServicePath"]) {
            servicePath = getServicePath();
        }

        if (toTask && getTask(methodName)) {
            return; //任务正在执行中
        }
        else {
            addTask(methodName);
        }

        var _parameters = "";
        var parametCount = 0;
        var _paraNames = "";
        for (var n in params) {
            if (parametCount > 0) {
                _parameters += ",";
                _paraNames += ",";
            }
            _parameters += params[n];
            _paraNames += n;
            parametCount++;
        }

        async = async == undefined ? true : async;
        params["_bpoName"] = this.BPOName + "Service";
        params["_methodName"] = methodName;
        params["s"] = 0;
        params["_paraNames"] = _paraNames;

        var soect = userContext || this;

        if (!useGet) {

            //单元入口
            var path = servicePath + "UCMLWebServiceEntryForJsonp.aspx";
            var meName = "";
            var dataType = "jsonp";
            var type = "post";

            var opt = this;

            $.ajax({
                type: type,
                dataType: dataType,
                url: path + '/' + meName,
                jsonpCallback: "jsonpcallback",
                data: params,
                success: function (o) {

                    removeTask(methodName);
                    var text = o;
                    var obj = eval(o);
                    if (obj && obj.status == 0) {
                        onFailure.call(soect, o, obj.text, methodName);
                    }
                    else {
                        onSuccess.call(soect, o, obj.text, methodName);
                    }
                },
                error: function (o) {

                    removeTask(methodName);
                    onFailure.call(soect, o, o.responseText, methodName);

                }
            });
			
        } else {
            var iframe = window.frames["bpo_frame"];
            if(iframe && iframe.contentWindow) {
                pm({
                    target: iframe.contentWindow,
                    type: "UCMLWebServiceEntry",
                    data: params,
                    success: function (text) {

                        removeTask(methodName);

                        var obj = eval(text);
                        if (obj && obj.status == 0 && onFailure) {
                            onFailure.call(soect, text, obj.text, methodName);
                        }
                        else if (onSuccess) {
                            onSuccess.call(soect, text, obj.text, methodName);
                        }

                        return obj.text;
                    }
                });
            }
        }
    },
    startBCLink: function (BCLink) {
        BCLink.el = $(BCLink.el);
        if (BCLink.BCRunMode) {//js控件
            var fieldName = BCLink.el.attr("dataFld");
            if (!fieldName) {
                return;
            }
            BCLink.fieldName = fieldName;

            var bc;
            if (!BCLink.dataTable) {
                bc = eval(BCLink.el.attr("BCName") + "Base");
                BCLink.dataTable = bc;
            }
            else {
                bc = BCLink.dataTable;
            }
        }
        else if (UCML.isElement(BCLink)) {
            var el = $(BCLink);

            var fieldName = el.attr("dataFld");
            if (!fieldName) {
                return;
            }
            var bc = eval(el.attr("BCName") + "Base");
            if (!bc) {
                return;
            }
            var linkIndex = el.attr("linkIndex") || 0;

            var objBCLinkColl = bc.getColumn(fieldName).objBCLinkColl;
            if (!objBCLinkColl) {
                return;
            }
            BCLink = objBCLinkColl[linkIndex];
            BCLink.el = el;
            BCLink.dataTable = bc;

        }

        if (BCLink.BCRunMode != 4) {
            if (BCLink.BCRunMode == 5) {
                var fn = eval(BCLink.urlFuncName);
                fn(BCLink);
                return;
            }

            var bcLinKBPOName = BCLink.BCName + 'BPO';
            var linkWindow = UCML.get(bcLinKBPOName);
			
            if (!linkWindow) {

                linkWindow = BCLink.showWindow = new UCML.Window({ id: bcLinKBPOName, isSetProperties: false, title: BCLink.caption, rendeTo: window.document.body, closable: true,
                    collapsible: true, doSize: true, closed: true, draggable: true, resizable: true, modal: false, width: BCLink.dropDownWidth, height: BCLink.dropDownHeight
                });
				
                var offset = BCLink.showWindow.getOffsetForWindow(BCLink.combo || BCLink.el);

                linkWindow.move({ left: offset.left, top: offset.top, width: BCLink.dropDownWidth, height: BCLink.dropDownHeight });
                linkWindow.linkState = 1;

                linkWindow.open();

                $(document).bind("mousedown.linkWindow", function (e) {
                    var el = $(e.target);
                    if (el.hasClass("window") || el.parents(".window").length > 0) {//点击弹出窗口不关闭

                    }
                    else if (el.hasClass("menu") || el.parents(".menu").length > 0) {//点击弹出窗口的右击菜单不关闭

                    }
                    else {
                        linkWindow.close();
                    }
                });

            }
            else {
                BCLink.showWindow = linkWindow;
                var offset = BCLink.showWindow.getOffsetForWindow(BCLink.combo || BCLink.el);
                linkWindow.move({ left: offset.left, top: offset.top, width: BCLink.dropDownWidth, height: BCLink.dropDownHeight });
                linkWindow.open();
            }

            if (linkWindow.linkState == 1) {
                $.getScript(getServicePath() + "BCClient/" + BCLink.BCName + ".js", function () {
                    linkWindow.linkState = 2;

                    var AppletObject = eval("new UCML.AppletUnit." + BCLink.BCName + "Service()");
                    AppletObject.el = AppletObject;
                    AppletObject.BCLink = BCLink;
					
                    //AppletObject.open();

                    if (BCLink.setAppletObject) {
                        BCLink.setAppletObject(AppletObject);
                    }
                    else {
                        BCLink.AppletObject = AppletObject;
                    }
                    linkWindow.AppletObject = AppletObject;
                    linkWindow.AppletObject.dataLoading = true;
                    //BCLink.AppletObject.InitBusinessEnv();
					
					window["BusiView_MethodName"] = "CondiInitBusinessEnvExString";
					window["BusiView_Params"] = BCLink.AppletObject.getInitBusinessCondi();
					
                    AppletObject.open();
                });
            }
            else {
                if (BCLink.setAppletObject) {
                    BCLink.setAppletObject(linkWindow.AppletObject);
                }
                else {
                    BCLink.AppletObject = linkWindow.AppletObject;
                }
                linkWindow.AppletObject.dataLoading = true;
                linkWindow.AppletObject.BCLink = BCLink;

                linkWindow.AppletObject.InitBusinessEnv();
            }
        }
        else {
            window.open(eval(BCLink.urlFuncName + "(BCLink)"), "", "location=no,menubar=yes,toolbar=no,status=no,directories=no,scrollbars=yes,resizable=yes");
        }
    },
    //启动弹出窗口VC
    startWindowVC: function (VCName, title, dWidth, dHeight, dLeft, dTop) {
        dWidth = UCML.isEmpty(dWidth) ? 500 : dWidth;
        dHeight = UCML.isEmpty(dHeight) ? 400 : dHeight;
        var dId = VCName + 'VC';
        var linkWindow = UCML.get(dId);
        if (!linkWindow) {

            linkWindow = new UCML.Window({ id: dId, isSetProperties: false, title: title, rendeTo: window.document.body, closable: true,
                collapsible: true, doSize: true, closed: true, draggable: true, resizable: true, modal: false, width: dWidth, height: dHeight, left: dLeft, top: dTop
            });

            linkWindow.move({ left: dLeft, top: dTop, width: dWidth, height: dHeight });
            linkWindow.linkState = 1;
            linkWindow.open();
        }
        else {
            linkWindow.move({ left: dLeft, top: dTop, width: dWidth, height: dHeight });
            linkWindow.open();
        }
	
        if (linkWindow.linkState == 1) {
            $.getScript(getServicePath() + "BCClient/" + VCName + ".js", function () {
                linkWindow.linkState = 2;

                var AppletObject = eval("new UCML.AppletUnit." + VCName + "Service()");
                AppletObject.el = AppletObject;
                AppletObject.open();

                linkWindow.AppletObject = AppletObject;
                AppletObject.InitBusinessEnv();
            });
        }
        else {
            linkWindow.AppletObject.InitBusinessEnv();
        }
    }
    ,
    //获取弹出窗口VC的对象
    getWindowVC: function (VCName) {
        var dId = VCName + 'VC';
        var linkWindow = UCML.get(dId);
        return linkWindow;
    }
    ,
    ShowMask: function () {
        var body = $(document.body);
        var mask = body.children(".datagrid-mask");
        var maskmsg = body.children(".datagrid-mask").children(".datagrid-mask-msg");
        if (mask.length == 0) {
            body.append("<div class=\"datagrid-mask\"><div class=\"datagrid-mask-msg\">" + UCML.Languge.BCRefreshLanguge + "</div></div>");
            mask = body.children(".datagrid-mask");
            maskmsg = body.children(".datagrid-mask").children(".datagrid-mask-msg");
        }
        mask.css({ height: $(window).height(), width: $(window).width(), display: "block" });
        maskmsg.css({ top: (mask.height() - maskmsg.height()) / 2, left: (mask.width() - maskmsg.width()) / 2 });
    },
    hidenMask: function () {
        $(document.body).children(".datagrid-mask").css({ display: "none" });
    },
    CheckUniqueID: function (BCName, FieldName, Value, KeyValue, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'CheckUniqueID', false, { BCName: BCName, FieldName: FieldName, Value: Value, KeyValue: KeyValue }, succeededCallback, failedCallback, userContext);
    },
    GetUniqueID: function (KindStr, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetUniqueID', false, { KindStr: KindStr }, succeededCallback, failedCallback, userContext);
    },
    GetSequenceNo: function (KindStr, succeededCallback, failedCallback, userContext, async) {
        return this.invoke(this.getPath(), 'GetSequenceNo', false, { KindStr: KindStr }, succeededCallback, failedCallback, userContext, async);
    },
    putTOExcel: function (nBCName, FieldLists, nStartPos, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'putTOExcel', false, { nBCName: nBCName, FieldLists: FieldLists, nStartPos: nStartPos, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix }, succeededCallback, failedCallback, userContext);
    },
    GetResourceData: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetResourceData', false, {}, succeededCallback, failedCallback, userContext);
    },
    GetReportURL: function (ReportID, nStartPos, nRecords, ReportParam, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetReportURL', false, { ReportID: ReportID, nStartPos: nStartPos, nRecords: nRecords, ReportParam: ReportParam }, succeededCallback, failedCallback, userContext);
    },
    GetSrvReportResult: function (ReportID, nStartPos, nRecords, ReportParam, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetSrvReportResult', false, { ReportID: ReportID, nStartPos: nStartPos, nRecords: nRecords, ReportParam: ReportParam }, succeededCallback, failedCallback, userContext);
    },
    GetDateSequenceNo: function (KindStr, format, sequenceCount, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetDateSequenceNo', false, { KindStr: KindStr, format: format, sequenceCount: sequenceCount }, succeededCallback, failedCallback, userContext);
    },
    GetIndicatorValue: function (auth, KpiID, Owner, OwnerType, StartDate, EndDate, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetIndicatorValue', false, { auth: auth, KpiID: KpiID, Owner: Owner, OwnerType: OwnerType, StartDate: StartDate, EndDate: EndDate }, succeededCallback, failedCallback, userContext);
    },
    GetDetailBusinessURL: function (auth, KpiID, Owner, OwnerType, StartDate, EndDate, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetDetailBusinessURL', false, { auth: auth, KpiID: KpiID, Owner: Owner, OwnerType: OwnerType, StartDate: StartDate, EndDate: EndDate }, succeededCallback, failedCallback, userContext);
    },
    Login: function (UserName, Password, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'Login', false, { UserName: UserName, Password: Password }, succeededCallback, failedCallback, userContext);
    },
    LoginMobile: function (UserName, Password,DeviceID succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'LoginMobile', false, { UserName: UserName, Password: Password, DeviceID: DeviceID }, succeededCallback, failedCallback, userContext);
    },
    Logout: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'Logout', false, {}, succeededCallback, failedCallback, userContext);
    },
    GetUserName: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetUserName', false, {}, succeededCallback, failedCallback, userContext);
    },
    GetSystemDateTime: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetSystemDateTime', false, {}, succeededCallback, failedCallback, userContext, false);
    },
    BusinessSubmit_Inner: function (xmlData, succeededCallback, failedCallback, userContext, async) {
        if (async == undefined || async == null) {
            async = true;
        }
        return this.invoke(this.getPath(), 'BusinessSubmit', false, { xmlData: xmlData }, succeededCallback, failedCallback, userContext, async);
    },
    //权限初始化函数
    RightSystemInit: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'RightSystemInit', false, {}, succeededCallback, failedCallback, userContext);
    },
    //计算层次码
    // 调用此函数可以是系统默认主键
    ComputeClassCode: function (TableName, ParentFieldName, ClassCodeFieldName, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'ComputeClassCode', false, { TableName: TableName, ParentFieldName: ParentFieldName, ClassCodeFieldName: ClassCodeFieldName }, succeededCallback, failedCallback, userContext);
    },
    // <summary>
    // 读入指定表名的业务组件数据
    // 前端JavaScript可调用
    // </summary>
    // <param name="TableName">实体表名</param>					   
    // <param name="nStartPos">开始位置</param>
    // <param name="nRecords">废弃</param>
    // <param name="fieldList">条件字段列表,以分号';'分割</param>
    // <param name="valueList">条件的值列表,以分号';'分割</param>
    // <param name="condiIdentList">条件的逻辑表达式列表,以分号';'分割</param>
    // <param name="SQLCondi">直接的条件语句</param>
    // <param name="SQLCondiType">直接的条件语句的连接方式 0--AND 1--OR </param>
    // <returns>FBPODataSet</returns>
    getCondiActorData: function (TableName, nStartPos, nRecords, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getCondiActorDataString', false, { TableName: TableName, nStartPos: nStartPos, nRecords: nRecords, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix }, succeededCallback, failedCallback, userContext, true, false);
    },

    /*BCName：BC名称
    nStartPos：起始行 默认为0
    nRecords：返回记录数 -1为全部，如果是10则是10条
    fieldList：字段集合，如"a1"，两个就是"a1;a2"
    valueList:值的集合，跟字段的顺序要一一对应 如"'1';'1'"
    condiIndentList:运算符号集合，跟字段的顺序一一对应 如"=;>="
    SQLCondi:自定义sql 如果字段集合的方式满足不了可以用 自定义sql  如 "a1 like '%5%'"
    SQLCondiType:条件类型 默认为0， 0为and的方式， 1为or，并且还是或者的关系
    SQLFix：查询后缀 如需要进行排序分组等语法 "order by a1"
    succeededCallback:调用成功执行的函数 默认可以设置为 condiQueryObj
    failedCallback：默认可设置为failedCallback
    userContext：上下文变量 可以不设置 也可以设置为this，设置这个变量后在调用成功和失败的方法中可以通过 this的方式使用
    */
    getCondiActorDataBC: function (BCName, nStartPos, nRecords, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, succeededCallback, failedCallback, userContext, async) {
        async = async == undefined ? true : async;

        return this.invoke(this.getPath(), 'getCondiActorDataBCString', false, { BCName: BCName, nStartPos: nStartPos, nRecords: nRecords, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix }, succeededCallback, failedCallback, userContext, async, false);
    },
    //统计业务组件名称的业务组件数据
    getTotalData: function (BCName, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, TotalFields, TotalModes, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getTotalData', false, { BCName: BCName, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix, TotalFields: TotalFields, TotalModes: TotalModes }, succeededCallback, failedCallback, userContext, false);
    },
    //TreeGrid读取子级数据
    getTreeGridSubData: function (BCName, parentFieldName, parentOID, SQLCondi, succeededCallback, failedCallback, nStartPos, nRecords, userContext) {
        return this.invoke(this.getPath(), 'getTreeGridSubData', false, { BCName: BCName, parentFieldName: parentFieldName, parentOID: parentOID, SQLCondi: SQLCondi, nStartPos: nStartPos, nRecords: nRecords }, succeededCallback, failedCallback, userContext, false);
    },
    //得到动态树的子节点
    getDynTreeSubNodes: function (BCName, parentFieldName, parentOID, CaptionField, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getDynTreeSubNodes', false, { BCName: BCName, parentFieldName: parentFieldName, parentOID: parentOID, CaptionField: CaptionField }, succeededCallback, failedCallback, userContext, false, false);
    },
    //查询树
    getReBuildWhereTreeNodes: function (BCName, parentFieldName, parentOID, CaptionField, Condi, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getReBuildWhereTreeNodes', false, { BCName: BCName, parentFieldName: parentFieldName, parentOID: parentOID, CaptionField: CaptionField, Condi: Condi }, succeededCallback, failedCallback, userContext, false, false);
    },
    //得到子表树节点
    getTreeSubNodesWithChild: function (BCName, VCName, parentFieldName, parentOID, CaptionField, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getTreeSubNodesWithChild', false, { BCName: BCName, VCName: VCName, parentFieldName: parentFieldName, parentOID: parentOID, CaptionField: CaptionField }, succeededCallback, failedCallback, userContext, true, false);
    },
    //读入业务数据
    getCondiBusinessData: function (nStartPos, nRecords, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'getCondiBusinessDataString', false, { nStartPos: nStartPos, nRecords: nRecords, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix }, succeededCallback, failedCallback, userContext, true, false);
    },
    loadChildDataEx: function (BCName, OID, recordXml, SubBC_SQLCondi, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'loadChildDataExString', true, { BCName: BCName, OID: OID, recordXml: recordXml, SubBC_SQLCondi: SubBC_SQLCondi }, succeededCallback, failedCallback, userContext, false);
    },
    CondiInitBusinessEnvEx: function (fieldList, valueList, condiIndentList, sqlCondi, succeededCallback, failedCallback, userContext) {
		return this.invoke(this.getPath(), 'CondiInitBusinessEnvExString', false, { fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, sqlCondi: sqlCondi }, succeededCallback, failedCallback, userContext);
    },

    /*
    *@decs:导出VC数据到Excel中
    *@param:VCName{string} 要导出的VC对象或名称
    *@param:allRows{boolean} 是否导出全部数据，值为false或undefined时只导出当前页
    *@param:Columns{string} 要导出的列名，为空时导出全部VC列
    *@param:exceptColumns{string} 不导出的列名
    *@param:mergeColumns{string} 要合并的列名
    *@param:nStartPos{integer} 查询条件字段
    *@param:nRecord{integer} 查询条件字段
    *@param:condiField{string} 查询条件字段
    *@param:condiValue{string} 查询条件字段值
    *@param:condiIndent{string} 查询条件逻辑符
    *@param:sqlCondi{string} 自定义where条件
    *@param:sqlCondiType{integer} 自定义where条件的连接方式，0:and,1:or 
    */
    ExportVC2Excel: function (VCObject, allRows, Columns, exceptColumns, mergeColumns, nStartPos, nRecord, condiField, condiValue, condiIndent, sqlCondi, sqlCondiType) {
        if (typeof VCObject == "string") {
            VCObject = eval(VCObject);
        }
        var bcName = VCObject.dataTable.BCName;
        if (!Columns) {
            Columns = $.map(VCObject.columns, function (el) { return el.fieldName; }).join(";");
        }
        //过滤不导出的列
        if (exceptColumns) {
            var colsFilter = exceptColumns.split(";");
            Columns = $.map(Columns.split(";"), function (col) {
                if (colsFilter.indexOf(col) == -1) {
                    return col;
                }
            }).join(";");
        }
        //确定要导出的行,全部导出时从数据库读取
        if (allRows) {
            var rows = "";
        }
        else {
            var rows = getBCXMLData(bcName);
        }

        //获取BC过滤条件
        var BCBase = VCObject.dataTable;
        var fieldList = BCBase.getFieldList();
        var valueList = BCBase.getValueList();
        var condiIndentList = BCBase.getCondiIndentList();
        //如果是子表时
        if (BCBase.TableType == "S") {
            var ForeignKey = BCBase.ForeignKey;
            //如果是自定义主外键时
            if (BCBase.getCustomRela()) {
                for (var m = 0; m < ForeignKey.ChildColumns.length; m++) {
                    parentOID = BCBase.MasterTable.getFieldValue(ForeignKey.ParentColumns[m]);
                    pkField = ForeignKey.ChildColumns[m];
                    fieldList = fieldList + BCBase.TableName + '.' + pkField + ';';
                    valueList = valueList + parentOID + ';';
                    condiIndentList = condiIndentList + ' = ;'
                }
            }
            else {
                fieldList = fieldList + BCBase.TableName + '.' + BCBase.LinkKeyName + ';';
                if (BCBase.PK_COLUMN_NAME != "") {
                    valueList = valueList + BCBase.MasterTable.getFieldValue(BCBase.PK_COLUMN_NAME) + ';';
                }
                else {
                    valueList = valueList + BCBase.MasterTable.getOID() + ';';
                }
                condiIndentList = condiIndentList + ' = ;'
            }
        }

        window.param = {};
        param.BPOName = BusinessObject.BPOName;
        param.BCName = bcName;
        param.Columns = Columns;
        param.Rows = rows;
        param.VCName = VCObject.id;
        param.mergeColumns = mergeColumns || "";

        param.condiField = condiField || "";
        param.condiField += fieldList;

        param.condiValue = condiValue || "";
        param.condiValue += valueList;

        param.condiIndent = condiIndent || "";
        param.condiIndent += condiIndentList;

        param.sqlCondi = sqlCondi || "";
        param.sqlCondiType = sqlCondiType || 0;
        param.nStartPos = nStartPos || 0;
        param.nRecord = nRecord || -1;

        var proxyUrl = UCMLLocalResourcePath + "ExportProxy.aspx?P=param";
        var $frame = $("#excelDownloadIframe");
        if ($frame.length != 0) {
            $frame.remove();
        }
        $frame = $('<iframe id="excelDownloadIframe" name="excelDownloadIframe" app="' + proxyUrl + '"></iframe>').css("display", "none");
        $("body").append($frame);
    },
    ExportToExcel: function (BCName, VCObject, MergerFieldIndex, TitleTextLists, notFields) {
        ShowMessage(UCML.Languge.BU_ExportToText);
        MergerFieldIndex = UCML.isEmpty(MergerFieldIndex) ? "" : MergerFieldIndex;
        TitleTextLists = TitleTextLists || "";
        notFields = notFields || "";
        this.ExportVC2Excel(VCObject, true, TitleTextLists, notFields, MergerFieldIndex);
        HideMessage();
    },
    ExportToExcelByBcColumns: function (BCName, VCObject, nStartPos, nRecords, MergerFieldIndex, TitleTextLists, BcColumns) {
        ShowMessage(UCML.Languge.BU_ExportToText);
        nStartPos = nStartPos || 0;
        nRecords = nRecords || -1;
        MergerFieldIndex = UCML.isEmpty(MergerFieldIndex) ? "" : MergerFieldIndex;
        TitleTextLists = TitleTextLists || "";
        var nFieldLists = [];
        var BCBase = eval(BCName + "Base");
        if (!BcColumns || BcColumns == "") {
            for (var i = 0; i < BCBase.columns.length; i++) {
                nFieldLists.push(BCBase.columns[i].fieldName);
            }
        }
        else {
            nFieldLists = BcColumns.split(";");
        }
        if (TitleTextLists) {
            nFieldLists = TitleTextLists.split(";");
        }
        this.ExportVC2Excel(VCObject, true, nFieldLists.join(";"), "", MergerFieldIndex, nStartPos, nRecords);
        HideMessage()
    },
    ExportToExcelById: function (id, notFields) {
        this.ExportVC2Excel(id, false, "", notFields);
    },
    getBCXMLData: function (bcName) {
        var strXML = '<NewDataSet>';
        var codeXML = '';
        for (var t = 0; t < this.UseTableList.length; t++) {
            var bcBase = this.UseTableList[t].dataStore;
            var bcCols = this.UseTableList[t].getColumns();
            if (bcBase.BCName != bcName) {
                continue;
            }
            //获取记录的xml
            for (var j = 0; j < bcBase.getLength(); j++) {
                strXML += '<' + bcBase.BCName + '>';
                var RecordData = bcBase.getRecordData(j);
                for (var i = 0; i < bcCols.length; i++) {
                    value = UCML.util.Format.htmlEncode(RecordData[i]);
                    if (bcCols[i].isCodeTable) {
                        value = this.GetCodeCaption(bcCols[i].codeTable, value);
                    }

                    strXML += '<' + bcCols[i].fieldName + '><![CDATA[' + value + ']]></' + bcCols[i].fieldName + '>';
                }
                strXML += '</' + bcBase.BCName + '>';
            }
            strXML += '</NewDataSet>';
            return strXML;
        }
    },
    CallReportExportTo: function (ReportID, nStartPos, nRecords, OutputMode, ReportParam) {
        ShowMessage(UCML.Languge.BU_ReportExportToText);
        if (OutputMode == "PDF" || OutputMode == "HTML") {
            return this.invoke(this.getPath(), 'ReportExportTo', false, { ReportID: ReportID, nStartPos: nStartPos, nRecords: nRecords, OutputMode: OutputMode, ReportParam: ReportParam
            }, CallBPOExportToSucceeded, failedToExcelSucceeded, this);
        }
        else {
            return this.invoke(this.getPath(), 'ReportExportTo', false, { ReportID: ReportID, nStartPos: nStartPos, nRecords: nRecords, OutputMode: OutputMode, ReportParam: ReportParam
            }, ExportToExcelSucceeded, failedToExcelSucceeded, this);
        }
    },
    CallBPOExportTo: function (BPOName, AssemblyName, OutputMode, SQLCondi) {
        ShowMessage(UCML.Languge.BU_ReportExportToText);
        if (OutputMode == "PDF" || OutputMode == "HTML") {
            return this.invoke(this.getPath(), 'BPOExportTo', false, { BPOName: BPOName, AssemblyName: AssemblyName, OutputMode: OutputMode, SQLCondi: SQLCondi
            }, CallBPOExportToSucceeded, failedToExcelSucceeded, this);
        }
        else {
            return this.invoke(this.getPath(), 'BPOExportTo', false, { BPOName: BPOName, AssemblyName: AssemblyName, OutputMode: OutputMode, SQLCondi: SQLCondi
            }, ExportToExcelSucceeded, failedToExcelSucceeded, this);
        }
    },
    ExportToExcelSucceeded: function (result, text, e) {
        if (text.length > 0) {
            $("#ExportToExcel_frame").remove();
            $(document.body).append("<iframe id='ExportToExcel_frame' app=" + UCMLLocalResourcePath + text + " style='display:none'></iframe>");
        }
        else {
            alert(UCML.Languge.BU_ExportToError + text);
        }
        HideMessage();
    }
    ,
    CallBPOExportToSucceeded: function (result, text, e) {
        if (text.length > 0) {
            open(UCMLLocalResourcePath + text);
        }
        else {
            alert(UCML.Languge.BU_ReportExportToError + text);
        }
        HideMessage();
    }
    ,
    failedToExcelSucceeded: function (result, text) {
        alert(UCML.Languge.BU_ReportExportToText + text);
        HideMessage();
    }
    ,
    createEventObject: function () {
        return new Object();
    },
    HideSelect: function () {

    },
    ShowSelect: function () {

    },
    AddUseTable: function (DataTable) {
        //    DataTable.BusinessObject = this;
        this.UseTableList[this.UseTableList.length] = DataTable;
        DataTable.setDeltaData(this.theDeltaData);
    },
    NotifyTable: function (data) {

        for (var i = 0; i < this.UseTableList.length; i++) {
            this.UseTableList[i].loadData(data[this.UseTableList[i].BCName]);
        }
    },
    NotifyTableBC: function (BCName, data) {
        var bcBase = this.getDataTableByBCName(BCName);
        if (bcBase) {
            bcBase.LoadResults(data);
        }
    },
    getRootTable: function () {
        return this.UseTableList[0];
    },
    getBCList: function () {
        return this.UseTableList;
    },
    MergeChange: function () {
        for (var i = 0; i < this.UseTableList.length; i++) {
            this.UseTableList[i].MergeChange();
        }
    },
    getURLParameters: function (ParamName) {
        var sURL = window.document.URL.toString();

        while (true) {
            if (sURL.lastIndexOf("#") == (sURL.length - 1))
                sURL = sURL.substring(0, sURL.length - 1);
            else
                break;
        }

        if (sURL.indexOf("?") > 0) {
            var arrParams = sURL.split("?");

            var arrURLParams = arrParams[1].split("&");

            var arrParamNames = new Array(arrURLParams.length);
            var arrParamValues = new Array(arrURLParams.length);

            var i = 0;
            for (i = 0; i < arrURLParams.length; i++) {
                var sParam = arrURLParams[i].split("=");
                arrParamNames[i] = sParam[0];
                if (sParam[1] != "")
                    arrParamValues[i] = unescape(sParam[1]);
                else
                    arrParamValues[i] = "No Value";
            }

            for (i = 0; i < arrURLParams.length; i++) {
                if (arrParamNames[i] == ParamName) return arrParamValues[i];
            }
        }
        return null;
    },
    ShowMessage: function (str) {
        var el = $("#message_panel");
        if (el.length == 0) {
            el = $("<div id='message_panel'><span class='message_text'></span><span class='loading_image'><img app='" + UCMLLocalResourcePath + "Images/message_panel.gif' width='16' height='16' /></span></div>");
            $(window.document.body).append(el);
        }
        var content = $(window);
        var top = (content.height() - el.height()) / 2;
        var left = (content.width() - el.width()) / 2;
        el.css({ top: top, left: left, position: "absolute" });
        el.show().find(".message_text").html(str);
    },
    HideMessage: function () {
        var el = $("#message_panel");
        el.hide();
    },
    condiQuery: function (fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, nStartPos, nRecords) {
        this.ShowMessage(UCML.Languge.BCRefreshLanguge);
        var rootTable = this.getRootTable();
        rootTable.SetCondiList(fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType);

        SQLCondiType = SQLCondiType || 0;
        if (!SQLCondi) {
            SQLCondi = "";
            SQLCondiType = 0;
        }
        nStartPos = nStartPos || 0;
        nRecords = nRecords || -1;

        this.getCondiBusinessData(nStartPos, nRecords, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, "", this.condiQueryObj, this.failedCallback, null);
    },
    condiActorQuery: function (BCName, nStartPos, nRecords, fieldList, valueList, condiIndentList, SQLCondi, SQLCondiType, SQLFix, succeededCallback, failedCallback, userContext) {

        var bcBase = this.getDataTableByBCName(BCName);
        if (!bcBase) {
            return;
        }
        if (!succeededCallback) {
            succeededCallback = bcBase.onAfterGetData;
        }
        if (!failedCallback) {
            failedCallback = bcBase.onFailureCall;
        }

        if (!userContext) {
            userContext = bcBase;
        }

        return this.invoke(this.getPath(), 'getCondiActorDataBCString', false, { BCName: BCName, nStartPos: nStartPos, nRecords: nRecords, fieldList: fieldList, valueList: valueList, condiIndentList: condiIndentList, SQLCondi: SQLCondi, SQLCondiType: SQLCondiType, SQLFix: SQLFix }, succeededCallback, failedCallback, userContext, true, false);
    },
    //查询失败调用函数
    failedCallback: function (result, text) {

        alert(text);
    },
    onCondiQuerySuccessCall: function (result, text) {
        this.onQueryResult(text);
    },
    //查询失败调用函数
    failedCallback: function (result, text) {

        alert(text);
    },
    onCondiQuerySuccessCall: function (result, text) {
        this.onQueryResult(result, text);
    },
    //查询成功调用函数
    onQueryResult: function (result, text) {
	
        this.HideMessage();
        if (text) {
            eval(text);
        }
        this.loadData(BusinessData);
    },
    condiQueryObj: function (result, text) {

        this.HideMessage();

        if (text) {
            eval(text);
        }
        this.loadData(BusinessData);

        if (this.OnAfterCondiQuery) {
            this.OnAfterCondiQuery();
        }
        this.fireEvent("OnAfterCondiQuery");
    },
    getDataTableByBCName: function (bcName) {
        for (var i = 0; i < this.UseTableList.length; i++) {
            if (this.UseTableList[i].BCName == bcName) {
                return this.UseTableList[i];
            }
        }
        return null;
    },
    getXMLData: function () {
        var strXML = '<NewDataSet>';
        var codeXML = '';
        for (var t = 0; t < this.UseTableList.length; t++) {
            var bcBase = this.UseTableList[t].dataStore;
            var bcCols = this.UseTableList[t].getColumns();
            //获取记录的xml
            for (var j = 0; j < bcBase.getLength(); j++) {
                strXML += '<' + bcBase.BCName + '>';
                var RecordData = bcBase.getRecordData(j);
                for (var i = 0; i < bcCols.length; i++) {
                    var value = UCML.util.Format.htmlEncode(RecordData[i]);
                    if (bcCols[i].isCodeTable && bcCols[i].codeTable) {
                        var caption = GetCodeCaption(bcCols[i].codeTable, RecordData[i]);
                        if (caption) {
                            value = UCML.util.Format.htmlEncode(caption);
                        }
                        else {
                            value = "";
                        }
                    }
                    strXML += '<' + bcCols[i].fieldName + '><![CDATA[' + value + ']]></' + bcCols[i].fieldName + '>';
                }
                strXML += '</' + bcBase.BCName + '>';
            }
            //获取代码表的xml
            var codeTableList = [];
            var j = 0;
            for (var k = 0; k < bcCols.length; k++) {
                if (bcCols[k].isCodeTable == true) {
                    var isHave = false;
                    for (var n = 0; n < codeTableList.length; n++) {//判断，防止输出重复的代码表
                        if (bcCols[k].codeTable == codeTableList[n].codeTableID) {
                            isHave = true;
                            break;
                        }
                    }
                    if (!isHave) {
                        var objCode = { codeTableID: bcCols[k].codeTable };
                        codeTableList[j] = objCode;
                        j++;
                    }
                }
            }
            for (var m = 0; m < codeTableList.length; m++) {
                var codeList = this.GetCodeValue(codeTableList[m].codeTableID);
                for (var cl = 0; cl < codeList.length; cl++) {
                    codeXML += '<' + codeTableList[m].codeTableID + '>';
                    codeXML += '<CodeValueOID><![CDATA[' + codeList[cl].codeTableID + ']]></CodeValueOID>';
                    codeXML += '<CodeID><![CDATA[' + codeList[cl].value + ']]></CodeID>';
                    codeXML += '<CodeName><![CDATA[' + codeList[cl].caption + ']]></CodeName>';
                    codeXML += '</' + codeTableList[m].codeTableID + '>';
                }
            }
        }
        if (codeXML != "") {
            strXML += codeXML;
        }
        strXML += '</NewDataSet>';
        return strXML;
    }
    ,
    getChangeXML: function () {
        var strXML = '<root>';
        for (var t = 0; t < this.UseTableList.length; t++) {
            var bcBase = this.UseTableList[t].dataStore;
            if (this.UseTableList[t].isChangeData) {
                for (var i = 0; i < bcBase.changeData.items.length; i++) {
                    strXML += '<' + bcBase.BCName + ' UpdateKind="'; //this.changeData.items[i][this.record.length+1];
                    if (bcBase.changeData.items[i][bcBase.record.length + 1] != "" && bcBase.changeData.items[i][bcBase.record.length + 1] != "null") {
                        strXML += bcBase.changeData.items[i][bcBase.record.length + 1];
                    }
                    strXML += '">';
                    for (var j = 0; j < bcBase.record.length; j++) {
                        var value = UCML.util.Format.htmlEncode(bcBase.changeData.items[i][j]);
                        strXML += '<' + bcBase.record[j].fieldName + '>' + value + '</' + bcBase.record[j].fieldName + '>';
                    }
                    strXML += '</' + bcBase.BCName + '>';
                }
            }
        }
        // strXML += this.getServerPropertyXML(); //增加属性变化  废掉了
        strXML += '</root>';

        return strXML;
    }, AssignTaskTo: function (TaskOIDs, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'AssignTaskTo', false, { TaskOIDs: TaskOIDs }, succeededCallback, failedCallback, userContext);
    },
    AssignTaskForRead: function (AssignTaskID, MasualAssign, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'AssignTaskForRead', false, { AssignTaskID: AssignTaskID, MasualAssign: MasualAssign }, succeededCallback, failedCallback, userContext);
    },
    AssignTaskForSign: function (AssignTaskID, MasualAssign, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'AssignTaskForSign', false, { AssignTaskID: AssignTaskID, MasualAssign: MasualAssign }, succeededCallback, failedCallback, userContext);
    },
    AssignTaskToMe: function (TaskOIDs, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'AssignTaskToMe', false, { TaskOIDs: TaskOIDs }, succeededCallback, failedCallback, userContext);
    }, TaskReturn: function (succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'TaskReturn', false, {}, succeededCallback, failedCallback, userContext);
    },
    GetbackTask: function (FlowID, TaskID, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'GetbackTask', false, { FlowID: FlowID, TaskID: TaskID }, succeededCallback, failedCallback, userContext);
    },
    RollbackTask: function (FlowID, TaskID, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'RollbackTask', false, { FlowID: FlowID, TaskID: TaskID }, succeededCallback, failedCallback, userContext);
    },
    FinishTask: function (AssignTaskID, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'FinishTask', false, { AssignTaskID: AssignTaskID }, succeededCallback, failedCallback, userContext);
    },
    FinishTaskEx: function (FlowID, AssignTaskID, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'FinishTaskEx', false, { FlowID: FlowID, AssignTaskID: AssignTaskID }, succeededCallback, failedCallback, userContext);
    },
    CreateFlow: function (FlowID, succeededCallback, failedCallback, userContext) {
        return this.invoke(this.getPath(), 'CreateFlow', false, { FlowID: FlowID }, succeededCallback, failedCallback, userContext);
    },
    /**
    *@decs:导出VC数据到Excel中
    *@param:VCName{string} 要导出的VC对象或名称
    *@param:allRows{boolean} 是否导出全部数据，值为false或undefined时只导出当前页
    *@param:Columns{string} 要导出的列名，为空时导出全部VC列
    *@param:exceptColumns{string} 不导出的列名
    *@param:mergeColumns{string} 要合并的列名
    *@param:nStartPos{integer} 查询条件字段
    *@param:nRecord{integer} 查询条件字段
    *@param:condiField{string} 查询条件字段
    *@param:condiValue{string} 查询条件字段值
    *@param:condiIndent{string} 查询条件逻辑符
    *@param:sqlCondi{string} 自定义where条件
    *@param:sqlCondiType{integer} 自定义where条件的连接方式，0:and,1:or 
    */
    ExportVC2Excel: function (VCObject, allRows, Columns, exceptColumns, mergeColumns, nStartPos, nRecord, condiField, condiValue, condiIndent, sqlCondi, sqlCondiType, fileName) {
        if (typeof VCObject == "string") {
            VCObject = eval(VCObject);
        }
        //获取BC过滤条件
        var BCBase = VCObject.dataTable;

        var bcName = VCObject.dataTable.BCName;
        if (!Columns) {
            Columns = $.map(VCObject.columns, function (el) { return el.fieldName; }).join(";");
        }
        //过滤不导出的列
        if (exceptColumns) {
            var colsFilter = exceptColumns.split(";");
            Columns = $.map(Columns.split(";"), function (col) {
                if (colsFilter.indexOf(col) == -1) {
                    return col;
                }
            }).join(";");
        }
        //确定列标题
        var vcColumns = VCObject.columns;
        var bcColumns = BCBase.columns;

        var Captions = $.map(Columns.split(";"), function (columnName) {
            //优先从VC找标题
            for (var i = 0; i < vcColumns.length; i++) {
                if (vcColumns[i].fieldName == columnName) {
                    return vcColumns[i].caption;
                }
            }
            //VC找不到时再从BC列中找
            for (var j = 0; j < bcColumns.length; j++) {
                if (bcColumns[j].fieldName == columnName) {
                    return bcColumns[j].caption;
                }
            }
        }).join(";");

        //确定要导出的行,全部导出时从数据库读取
        if (allRows) {
            var rows = "";
        }
        else {
            var rows = getBCXMLData(bcName);
        }


        var fieldList = BCBase.getFieldList();
        var valueList = BCBase.getValueList();
        var condiIndentList = BCBase.getCondiIndentList();
        //如果是子表时
        if (BCBase.TableType == "S") {
            var ForeignKey = BCBase.ForeignKey;
            //如果是自定义主外键时
            if (BCBase.getCustomRela()) {
                for (var m = 0; m < ForeignKey.ChildColumns.length; m++) {
                    parentOID = BCBase.MasterTable.getFieldValue(ForeignKey.ParentColumns[m]);
                    pkField = ForeignKey.ChildColumns[m];
                    fieldList = fieldList + BCBase.TableName + '.' + pkField + ';';
                    valueList = valueList + parentOID + ';';
                    condiIndentList = condiIndentList + ' = ;'
                }
            }
            else {
                fieldList = fieldList + BCBase.TableName + '.' + BCBase.LinkKeyName + ';';
                if (BCBase.PK_COLUMN_NAME != "") {
                    valueList = valueList + BCBase.MasterTable.getFieldValue(BCBase.PK_COLUMN_NAME) + ';';
                }
                else {
                    valueList = valueList + BCBase.MasterTable.getOID() + ';';
                }
                condiIndentList = condiIndentList + ' = ;'
            }
        }

        window.param = {};
        param.BPOName = BusinessObject.BPOName;
        param.BCName = bcName;
        param.Columns = Columns;
        param.Captions = Captions;
        param.Rows = rows;
        param.VCName = VCObject.id;
        param.mergeColumns = mergeColumns || "";
        param.fileName = fileName;
        param.condiField = condiField || "";
        param.condiField += fieldList;

        param.condiValue = condiValue || "";
        param.condiValue += valueList;

        param.condiIndent = condiIndent || "";
        param.condiIndent += condiIndentList;

        param.sqlCondi = sqlCondi || "";
        param.sqlCondiType = sqlCondiType || 0;
        param.nStartPos = nStartPos || 0;
        param.nRecord = nRecord || -1;

        var proxyUrl = UCMLLocalResourcePath + "ExportProxy.aspx?P=param";
        var $frame = $("#excelDownloadIframe");
        if ($frame.length != 0) {
            $frame.remove();
        }
        $frame = $('<iframe id="excelDownloadIframe" name="excelDownloadIframe" app="' + proxyUrl + '"></iframe>').css("display", "none");
        $("body").append($frame);
    },
    ExportToExcel: function (BCName, VCObject, MergerFieldIndex, TitleTextLists, notFields) {
        ShowMessage(UCML.Languge.BU_ExportToText);
        MergerFieldIndex = UCML.isEmpty(MergerFieldIndex) ? "" : MergerFieldIndex;
        TitleTextLists = TitleTextLists || "";
        notFields = notFields || "";
        this.ExportVC2Excel(VCObject, true, TitleTextLists, notFields, MergerFieldIndex);
        HideMessage();
    },
    ExportToExcelByBcColumns: function (BCName, VCObject, nStartPos, nRecords, MergerFieldIndex, TitleTextLists, BcColumns) {
        ShowMessage(UCML.Languge.BU_ExportToText);
        nStartPos = nStartPos || 0;
        nRecords = nRecords || -1;
        MergerFieldIndex = UCML.isEmpty(MergerFieldIndex) ? "" : MergerFieldIndex;
        TitleTextLists = TitleTextLists || "";
        var nFieldLists = [];
        var BCBase = eval(BCName + "Base");
        if (!BcColumns || BcColumns == "") {
            for (var i = 0; i < BCBase.columns.length; i++) {
                nFieldLists.push(BCBase.columns[i].fieldName);
            }
        }
        else {
            nFieldLists = BcColumns.split(";");
        }
        if (TitleTextLists) {
            nFieldLists = TitleTextLists.split(";");
        }
        this.ExportVC2Excel(VCObject, true, nFieldLists.join(";"), "", MergerFieldIndex, nStartPos, nRecords);
        HideMessage();
    },
    ExportToExcelById: function (id, notFields) {
        this.ExportVC2Excel(id, false, "", notFields);
    },
    getBCXMLData: function (bcName) {
        var strXML = '<NewDataSet>';
        var codeXML = '';
        for (var t = 0; t < this.UseTableList.length; t++) {
            var bcBase = this.UseTableList[t].dataStore;
            var bcCols = this.UseTableList[t].getColumns();
            if (bcBase.BCName != bcName) {
                continue;
            }
            //获取记录的xml
            for (var j = 0; j < bcBase.getLength(); j++) {
                strXML += '<' + bcBase.BCName + '>';
                var RecordData = bcBase.getRecordData(j);
                for (var i = 0; i < bcCols.length; i++) {
                    value = UCML.util.Format.htmlEncode(RecordData[i]);
                    if (bcCols[i].isCodeTable) {
                        value = this.GetCodeCaption(bcCols[i].codeTable, value);
                    }

                    strXML += '<' + bcCols[i].fieldName + '><![CDATA[' + value + ']]></' + bcCols[i].fieldName + '>';
                }
                strXML += '</' + bcBase.BCName + '>';
            }
            strXML += '</NewDataSet>';
            return strXML;
        }
    },
    /**
    *@method 导入Excel到指定BC
    *@param BCName {string} BC名称
    *@param kvPair {string} 使用固定值的列和值的序列，列名和值之前用:隔开，多个列值对之间用;隔开
    *@param autoMatch {boolean}   是否启动列名自动匹配，为true时会自动寻找Excel第一行中的列值，如果发现与BC的某列名称或标题相同，则导入该Excel列到相应BC列
    *@param withParentRow {boolean} 是否导入主表记录，配合BPO导入模板使用
    */
    importExcel: function (BCName, kvPair, autoMatch, withParentRow) {
        kvPair = kvPair || "";
        var url = "uploadExcel.html?BPOName=" + this.BPOName + "&BCName=" + BCName + "&KV=" + kvPair;
        if (autoMatch) {
            url += "&AutoMatch=1";
        }
        if (withParentRow) {
            url += "&WithParentRow=1";
        }
        OpenWindow(url, "&nbsp;", 1, { width: 600, height: 380, resizable: false, collapsible: false });
    }
});

/**     
* @method public havePermisionCode 
* @description 获取控制码权限 
* @param {string} PermisionCode      
* @return {bool} 
*/
UCML.BusinessUnit.prototype.havePermisionCode = function (PermisionCode) {

    var BPOPermControlCode = this.el.BusinessData[this.BPOName + 'PermControlCode'];

    var flag = false;
    if (BPOPermControlCode && BPOPermControlCode.length > 0)//如果设置权限码
    {
        for (var i = 0; i < BPOPermControlCode.length; i++) {
            if (BPOPermControlCode[i][1] == PermisionCode && BPOPermControlCode[i][3] == "true")//如果能找到对应的权限码并且设置为允许
            {
                flag = true;
                break;
            }
        }
    }

    return flag;
}

UCML.BusinessUnit.prototype.BusinessSubmit = function (val, async) {
    if (this.BeforeSubmit && this.BeforeSubmit() === true) {
        if (val !== false) {
            if (this.CanSubmit() == false) return;
        }
        this.ShowMessage("正在提交信息，请等待......");

        var changeXML = this.getChangeXML();

        this.BusinessSubmit_Inner(changeXML, this.OnSubmitSuccessCall, this.OnSubmitFailureCall, this, async);
    }
}

UCML.BusinessUnit.prototype.BusinessSubmit_pm = function(val, async) {
    if (this.BeforeSubmit && this.BeforeSubmit() === true) {
        if (val !== false) {
            if (this.CanSubmit() == false) return;
        }
        this.ShowMessage("正在提交信息，请等待......");

        var changeXML = this.getChangeXML();

        if(async == undefined || async == null) {
            async = true;
        }

        return this.invoke(this.getPath(), 'BusinessSubmit', true, {xmlData: changeXML}, this.OnSubmitSuccessCall, this.OnSubmitFailureCall, this, async);
    }
}

//单BC提交
UCML.BusinessUnit.prototype.BusinessSubmitByBcName = function (BcName, val) {
    if (this.BeforeSubmit && this.BeforeSubmit() === true) {

        this.BeforeSubmit();
    }
    var bcBase = this.getDataTableByBCName(BcName);
    if (bcBase) {
        if (val !== false) {
            bcBase.Valiate();
        }
        this.ShowMessage("正在提交信息，请等待......");
        this.BusinessSubmit_Inner(bcBase.getChangeXML(), this.OnSubmitSuccessCall, this.OnSubmitFailureCall, this);
    }
}

//是否有变化数据
UCML.BusinessUnit.prototype.isChangeData = function () {
    for (var i = 0; i < this.UseTableList.length; i++) {
        if (this.UseTableList[i].ChangeCount() > 0) {
            return true;
        }
    }
    return false;
}

UCML.BusinessUnit.prototype.CanSubmit = function () {
    var result = true
    for (var i = 0; i < this.UseTableList.length; i++) {
        result = this.UseTableList[i].Valiate();
        if (result == false) {

            break;
        }
    }
    return result;
}

UCML.BusinessUnit.prototype.OnSubmitSuccessCall = function (result, text, methodName) {
    this.HideMessage();
    this.fireEvent("SubmitSuccess");
    for (var i = 0; i < this.UseTableList.length; i++) {
        if (this.UseTableList[i].fIDENTITYKey == true) {
            this.promptSubmit = false;
            var rows = text.split("@");
            for (var m = 0; m < rows.length; m++) {
                var fields = rows[m].split(";");
                if (fields.length == 1) break;
                var bcBase = eval(fields[0] + "Base");
                if (bcBase.LocateOID_GUID(fields[1]) != null)
                    bcBase.setFieldValue(bcBase.PrimaryKey, fields[2]);
            }
            break;
        }
    }
    this.MergeChange();

    var name = BusinessObject.BPOName + "_changeXML";
    this.setStorageData(name, "");

    if (this.promptSubmit == true) {
        alert(text);
    }

    if (this.AfterSubmit) {
        this.AfterSubmit();
    }
    return;
}

UCML.BusinessUnit.prototype.OnSubmitFailureCall = function (result, text, methodName) {
    this.HideMessage();
    if ((result.status == "404" || result.status == "12029") && window.localStorage && window["isChangeDataToStorage"]) {
        alert("您的网络可能遇到点故障，我们将您需要保存的数据存在本地浏览器中，以便下次网络畅通后继续提交");

        var changeXML = this.getChangeXML();
        var name = BusinessObject.BPOName + "_changeXML";
        this.setStorageData(name, changeXML);
    }
    else {
        alert(text);
    }
}

UCML.BusinessUnit.prototype.loadStorageDataChangeXML = function () {
    var name = BusinessObject.BPOName + "_changeXML";
    var data = this.getStorageData(name);
    if (!UCML.isEmpty(data)) {
        if (confirm("您上次有未提交的数据,是否进行提交")) {

            this.BusinessSubmit_Inner(data, this.OnSubmitSuccessCall, this.OnSubmitFailureCall, this);
        }
    }
}

UCML.BusinessUnit.prototype.setStorageData = function (name, data) {
    if (window.localStorage) {
        localStorage[name] = data;
    }
}

UCML.BusinessUnit.prototype.getStorageData = function (name) {
    if (window.localStorage) {
        return localStorage[name];
    }
    else {
        return null;
    }
}

UCML.BusinessUnit.prototype.mergeChangeAll = function () {
    this.MergeChange();
}


/**    
* @method private GetCodeValue 
* @description 获取代码表数据
* @param {string} CodeTableID 
*/
UCML.BusinessUnit.prototype.GetCodeValue = function (CodeTableID) {
    var evObj = this.createEventObject()
    evObj.result = false;
    evObj.CodeTableID = CodeTableID;
    evObj.CodeList = null;
    this.fireEvent("OnGetCodeValue", evObj);
    if (evObj.result == true) {
        return evObj.CodeList;
    }

    for (var i = 0; i < this.CodeTableList.length; i++) {
        obj = this.CodeTableList[i];
        if (obj.CodeTableID == CodeTableID) {
            return obj.CodeList;
        }
    }

    //   this.theMainDataPacket = BusinessObject.getBusinessDataPacket();
    //   if (this.theMainDataPacket == undefined) return null;
    //   if (this.theMainDataPacket == null) return null;
    var codeData = BusinessData[CodeTableID];
    if (codeData == null || codeData.length == 0) return null;
    var CodeList = new Array();
    for (var i = 0; i < codeData.length; i++) {
        var obj = new Object();
        obj.codeTableID = codeData[i][0];
        obj.value = codeData[i][1];
        obj.caption = codeData[i][2];
        CodeList[i] = obj;
    }
    var obj = new Object();
    obj.CodeTableID = CodeTableID;
    obj.CodeList = CodeList;
    this.CodeTableList[this.CodeTableList.length] = obj;

    return CodeList;
}

/**    
* @method private GetCodeID 
* @description 获取代码编码
* @param {string} CodeTableID 
* @param {string} caption 
*/
UCML.BusinessUnit.prototype.GetCodeID = function (CodeTableID, caption) {
    var CodeList = this.GetCodeValue(CodeTableID);
    if (CodeList != null) {
        for (var j = 0; j < CodeList.length; j++) {
            if (caption == CodeList[j].caption) {
                return CodeList[j].value;
            }
        }
    }
    return null;
}

/**    
* @method private GetCodeCaption 
* @description 获取代码表Text
* @param {string} CodeTableID 
* @param {string} CodeValue 
*/
UCML.BusinessUnit.prototype.GetCodeCaption = function (CodeTableID, CodeValue) {
    var CodeList = this.GetCodeValue(CodeTableID);
    if (CodeList != null) {
        for (var j = 0; j < CodeList.length; j++) {
            if (CodeValue == CodeList[j].value) {
                return CodeList[j].caption;
            }
        }
    }
    return "";
}


/**    
* @method private OpenWindow 
* @description 弹出窗口
* @param {string} url 页面地址 
* @param {string} title 页面标题
* @param {string} openMode 弹出模式 1，弹出层 2，页面跳转（location） 3，打开新窗口（open）  
* @param {object} config 配置属性
*/
UCML.BusinessUnit.prototype.OpenWindow = function (url, title, openMode, config) {

    openMode = openMode == undefined ? 1 : openMode;

    if (openMode == 1) {

        if (!window.currentWindow && window.opener && window.UCMLLocalResourcePath)//从外部弹出的  解决分目录问题
        {
            var pathname = window.location.pathname.toString();
            var path = pathname.substring(0, pathname.lastIndexOf('/') + 1);

            var rootPath = path + window.UCMLLocalResourcePath;
            url = rootPath + url;
        }
        else {
            //	url=window.UCMLLocalResourcePath+url;
        }
        var defaultConfig = { frameMode: "frame", maximizable: true,
            collapsible: true, URL: url, scroll: "yes", draggable: true,
            resizable: true, title: title
        };
        if (config) {
            UCML.apply(defaultConfig, config);
        }
        config = defaultConfig;

        if (UCML.isEmpty(config.width) && UCML.isEmpty(config.ratioWidth)) {
            config.alignWidth = true; //适应宽
        }
        if (UCML.isEmpty(config.height) && UCML.isEmpty(config.ratioWidth)) {
            config.alignHeight = true; //适应高
        }

        var w = new UCML.OpenShowWindow(config);
        w.open();
        return w;
    }
    else if (openMode == 2) {
        window.location.href = url;
    }
    else if (openMode == 3) {
        window.open(url, "", "location=no,menubar=no,toolbar=no,status=yes,directories=no,scrollbars=yes,resizable=no,width=800,height=600");
    }
}



UCML.BusinessUnit.prototype.saveQueryCondi = function (BPOName, VCName, CondiName, CondiValue, succeededCallback, failedCallback, userContext) {
    return this.invoke(this.getPath(), 'SaveQueryCondi', false, { BPOName: BPOName, VCName: VCName, CondiName: CondiName, CondiValue: CondiValue }, succeededCallback, failedCallback, userContext);
}
UCML.BusinessUnit.prototype.loadQueryCondi = function (BPOName, VCName, IsUser, succeededCallback, failedCallback, userContext) {
    return this.invoke(this.getPath(), 'LoadQueryCondi', false, { BPOName: BPOName, VCName: VCName, IsUser: IsUser }, succeededCallback, failedCallback, userContext);
}
UCML.BusinessUnit.prototype.deleteQueryCondi = function (OID, succeededCallback, failedCallback, userContext) {
    return this.invoke(this.getPath(), 'DeleteQueryCondi', false, { OID: OID }, succeededCallback, failedCallback, userContext);
}


/*设置服务端属性*/
UCML.BusinessUnit.prototype.UpdateServerProperty = function (name, value) {
    var ServerPropertyChange = window["ServerPropertyChange"] || {};
    ServerPropertyChange[name] = value;
    window["ServerPropertyChange"] = ServerPropertyChange;
}

UCML.BusinessUnit.prototype.getServerPropertyXML = function () {

    var ServerPropertyChange = window["ServerPropertyChange"];
    if (ServerPropertyChange) {
        var xmlStr = "<PropertyList UpdateKind='ukModify'>"
        var xmlStr2 = "<PropertyList  >"

        for (var item in ServerProperty) {
            xmlStr += "<" + item + ">";
            xmlStr += "null";
            xmlStr += "</" + item + ">";

            xmlStr2 += "<" + item + ">";
            var updateValue = ServerPropertyChange[item];
            if (updateValue) {
                xmlStr2 += updateValue;
            }
            else {
                xmlStr2 += "null";
            }
            xmlStr2 += "</" + item + ">";
        }
        xmlStr += "</PropertyList>";
        xmlStr2 += "</PropertyList>";

        return xmlStr + xmlStr2;
    }
    else {
        return "";
    }

}







/*!
* @property  selectManObj
* @description 存放BC，id字段名，标题字段名对象
*                     格式为 { bcName: bcName, idField: idField, titleField: titleField }
*/
UCML.BusinessUnit.prototype.selectManObj = null;
/*!
* @method  openSelectMan
* @description 弹出选人对话框
* @param {Int} selTyle 选人类型
*        具体见BPO_Select_User_Organize_MainJS.aspx.cs中TypeClass类中组合
* @param {Int} openMode 弹出模式 
*        用数字1来表示层弹出模式，数字2表示window.open弹出模式
* @param {String} bcName 要赋值的业务组件的组件名(BC的名称，字符串).	 
* @param {String} idField 要赋值的业务组件中，存放id序列的字段名 	       
* @param {String} titleField 要赋值的业务组件中，存放标题序列的字段名
* @return {null}
*/
UCML.BusinessUnit.prototype.openSelectMan = function (selTyle, openMode, bcName, idField, titleField, obj) {
    openMode = openMode || 1;
    selTyle = selTyle || 1;
    this.selectManObj = { bcName: bcName, idField: idField, titleField: titleField };

    var url = "BPO_Select_User_Organize_MainJS.aspx?type=" + selTyle + "&mode=" + openMode + "&callbackFn=callBackSelectMan&initFn=initSelectMan";
    if (openMode == 1) {
        var config = { frameMode: "frame", maximizable: true,
            collapsible: true, URL: url, scroll: "yes", draggable: true,
            resizable: true, width: 750, height: 550, title: UCML.Languge.BU_SelectMan_Title
        };
        obj = obj || {};
        UCML.apply(config, obj);
        var w = new UCML.OpenShowWindow(config);
        w.open();
        return w;
    }
    else if (openMode == 2) {
        return window.open(UCMLLocalResourcePath + url, UCML.Languge.BU_SelectMan_Title, "location=no;menubar=no;toolbar=no;status=no;directories=no;scrollbars=auto;resizable=no;width=750px;height=420px");
    }
}
/*!
* @method  initSelectMan
* @description 弹出的选人对话框中，已选中部分处理的回调函数
*                     此函数由弹出选人页面中调用
* @return {null||Object}
*/
UCML.BusinessUnit.prototype.initSelectMan = function () {
    if (!selectManObj) return null;
    try {
        var id = eval(selectManObj.bcName + "Base.getFieldValue('" + selectManObj.idField + "')");
        var Caption = eval(selectManObj.bcName + "Base.getFieldValue('" + selectManObj.titleField + "')");
        //去头部逗号
        if (id.indexOf(",") == 0) id = id.substring(1, id.length - 1);
        var idarr = id.split(",");
        var Captionarr = Caption.split(",");
        var objArr = new Array();

        for (var i = 0; i < idarr.length; i++) {
            var o = new Object();
            o.tp = idarr[i].split(":")[0].substring(idarr[i].split(":")[0].length - 1, idarr[i].split(":")[0].length);
            o.id = idarr[i].split(":")[1].substring(0, 36);
            o.name = Captionarr[i];
            o.fvalue = idarr[i];
            objArr.push(o);
        }
        return objArr;
    }
    catch (e) {
        return null;
    }
}
/*!
* @method  callBackSelectMan
* @description 弹出的选人对话框中，确认选择后赋值函数
*                     此函数由弹出选人页面中调用
* @param {String} str 选中的数据的id序列	 
*                                多选时格式：(U:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx),(O:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx),(G:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx)
*                                单选时格式: xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
* @param {String} strN 选中数据的标题序列
*                                  多选时格式: 张三,李四,商务部,领导群
*                                  单选时格式: 张三
* @return {null||Object}
*/
UCML.BusinessUnit.prototype.callBackSelectMan = function (str, strN) {
    if (selectManObj) {
        try {
            eval(selectManObj.bcName + "Base.setFieldValue('" + selectManObj.idField + "',str)");
            eval(selectManObj.bcName + "Base.setFieldValue('" + selectManObj.titleField + "',strN)");
        }
        catch (e)
        { }
    }
}

//视图组件权限专用
UCML.BusinessUnit.prototype.CallChangeBusiView = function (viewIndex) {
    var config = { methodName: "ChangeBusiViewString", params: { viewIndex: viewIndex }, onSuccess: this.succeeded_ChangeBusiViewString, onFailure: this.failed_ChangeBusiViewString, useGet: false };
    return this.invoke(config);
}
UCML.BusinessUnit.prototype.succeeded_ChangeBusiViewString = function (obj, text, methodName) {
    eval(text);
    this.loadData(this.el.BusinessData);
}

UCML.BusinessUnit.prototype.failed_ChangeBusiViewString = function (obj, text, methodName) {
}


function getResourceData(id) {

    if (BusinessData["ResourceData"] && BusinessData["ResourceData"].length > 0) {
        for (var i = 0; i < BusinessData["ResourceData"].length; i++) {
            if (id == BusinessData["ResourceData"][i][0]) {
                return BusinessData["ResourceData"][i][1];
            }
        }
    }
    return "";
}

function ResetColumns(OldColumns, VCName) {

    var value = $("#" + VCName + "_ColumnSetup").val();
    if (value == undefined||value == "") return OldColumns;
    eval(value);
    
    var newVCCloumns = [];
    for (var i = 0; i < cusColumns.length; i++) {
        for (var j = 0; j < OldColumns.length; j++) {
            if (cusColumns[i].fieldName == OldColumns[j].fieldName) {
                OldColumns[j].width = cusColumns[i].width;
                 OldColumns[j].display = cusColumns[i].display;
                newVCCloumns[newVCCloumns.length] = OldColumns[j];
                break;
            }
        }
    }
    eval(VCName+"Columns=newVCCloumns;");

  
    return newVCCloumns;
}

















