﻿/**
*	@extend: UCML.FuzzyQuerys
*	@description: 多字段模糊查询控件
*/
UCML.FuzzyQuerys = function(id) {

	UCML.FuzzyQuerys.superclass.constructor.call(this, id);
}

UCML.extend(UCML.FuzzyQuerys, UCML.Applet, {
    onRender: function () {
        var $table = $('<table style="height:100%;width: 100%"><tr><td class="textInput"></td><td style="width:24px" align="center" valign="middle"  class="btnInput"></td></tr></table>');
        var $input = $('<input type="text" value="请输入查询关键字(多个空格隔开)">');
        var src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA5FBMVEUQk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk90Qk93///+4HOQ0AAAASnRSTlMADHnK9vvcmCpD6oQCQvrJVRkRO5z8lg6bA0z5dtDLVgr1IRzFRRe/UddG7S6Mr1ngHvdnLOtomiMNbvB/b+bdq/3GWIEGJysPgJnwjVQAAAABYktHREtpC4VQAAAACXBIWXMAAAsSAAALEgHS3X78AAAApUlEQVQY01WO1RLCUAxEF2iR4lKguLu7u3X//4MotzLDPiTZk0lmAcDl9kiy1+eHpYBCITkYEj4cYTQWTyRTKtM/n1GY1cQml2fMaG56NOu2wKJRSyzbzypV1gCJdRugwSYQYcsBbXaALnu21/ocAEOObDDmxEgynXFu+sXSnFbkegNsd3vyIDZHlTydZRHfJIuLRF5v94dD8Hy9P0bTSR1/0nV8AQxKGP7vP9VnAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE0LTA0LTEzVDE0OjAyOjQzKzAyOjAwJzY5rQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxNC0wNC0xM1QxNDowMjo0MyswMjowMFZrgREAAAAASUVORK5CYII=";
        var $btn = $('<img app="' + src + '" border="0"  align="middle" />');

        this.el.css({ "font-size": "16px" });
        $input.css({ "width": "99%", "height": "20px", "margin": "5px", "border": "1px solid #b6b6b6", "border-color": "#9a9a9a #cdcdcd #cdcdcd #9a9a9a", "color": "rgb(146, 140, 140)" });
        $btn.css({ "width": "16px", "height": "16px", "margin": "0px", "border-radius": "0px", "letter-spacing": "5px" });

        $table.find(".textInput").append($input);
        $table.find(".btnInput").append($btn);
        this.el.append($table);

        this.input = $input;
        this.btn = $btn;



    },
    setProperties: function () {
        UCML.FuzzyQuerys.superclass.setProperties.call(this);
    },
    getIndex: function () {
        return this.td[0].cellIndex;
    },
    bindEvents: function () {
        UCML.FuzzyQuerys.superclass.bindEvents.call(this);

        var opts = this;
        //input事件
        this.input.focus(function (e) {
            if (this.value == "请输入查询关键字(多个空格隔开)")
                this.value = "";
        }).blur(function (e) {
            if (this.value == "")
                this.value = "请输入查询关键字(多个空格隔开)";
        });
        //按钮查询事件
        this.btn.click(function (e) {
            //取输入的值并将多个空格合并为一个
            var queryValue = opts.input.attr("value");
            queryValue = queryValue.replace(/\s+/g, ' ');

            if (opts.FieldList != "") {
                var SqlCondi = "";
                if (queryValue != "" && queryValue != "请输入查询关键字(多个空格隔开)") {
                    var queryArr = queryValue.split(" ");
                    var fieldList = opts.FieldList.split(";");
                    var typeList = opts.TypeList.split(";");
                    var tableList = opts.TableList.split(";");
                    for (var i = 0; i < fieldList.length; i++) {
                        //为每个字段匹配所有值
                        SqlCondi += "(";
                        for (var j = 0; j < queryArr.length; j++) {
                            var fieldType = typeList[i];
                            if (fieldType == '13' || //Float
								fieldType == '14' || //Double
								fieldType == '15' || //Money
								fieldType == '4' || //Numeric
								fieldType == '31' || //Long
								fieldType == '12' || //Byte
								fieldType == '11' || //Short
								fieldType == '10') //Int
                            {

                                if (opts.checkNum(queryArr[j])) {

                                    SqlCondi += tableList[i] + "." + fieldList[i] + " = " + queryArr[j];

                                } else {
                                    SqlCondi += " 0 = 0 ";
                                }
                                if (j < queryArr.length - 1)
                                    SqlCondi += " or ";

                            } else {

                                SqlCondi += tableList[i] + "." + fieldList[i] + " like '%" + queryArr[j] + "%'";
                                if (j < queryArr.length - 1)
                                    SqlCondi += " or ";
                            }
                        }
                        SqlCondi += ")";
                        if (i < fieldList.length - 1)
                            SqlCondi += " or ";
                    }
                } else {
                    SqlCondi = "";
                }

                opts.BusinessObject.condiQuery("", "", "", SqlCondi);
            } else {
                alert("未指定查询字段");
            }

        });
    }
});

/* 
*  判断字符串是否可以转化为纯数值类型
*  1.如果有小数点,则小数点只能有一个且不能在头、尾
*  2.如果没有小数点,则第一个不能为0
*/
UCML.FuzzyQuerys.prototype.checkNum = function(str) {
	var letter;
	if(str.indexOf(".") != -1) {
		if(str.charAt[0] == '.' || str.charAt[str.length-1] == '.')
			return false;
		var temp = str.replace(".", "");
		if(temp.indexOf(".") != -1)
			return false;
		letter = "1234567890.";
	}
	else {
		if(str.charAt[0] == "0")
			return false;
		letter = "1234567890";
	}

	for(var i = 0; i < str.length; i++) {
		var c = str.charAt(i);
		if(letter.indexOf(c) == -1) {
			return false;
		}
	}
	return true;
}

UCML.FuzzyQuerys.prototype.init = function () {
    UCML.FuzzyQuerys.superclass.init.call(this);
};