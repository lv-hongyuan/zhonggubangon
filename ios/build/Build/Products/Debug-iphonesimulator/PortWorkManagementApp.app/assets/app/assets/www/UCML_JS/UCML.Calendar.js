﻿UCML.Calendar = function (id) {
    this.year = this.current.getFullYear();
    this.month = this.current.getMonth() + 1;
    this.onSelect = function (date) { };
    UCML.Calendar.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Calendar, UCML.Container, {
    ctype: "UCML.Calendar",
    border: true,
    current: new Date(),
    weeks: [UCML.Languge.Calendar_Week7, UCML.Languge.Calendar_Week1, UCML.Languge.Calendar_Week2, UCML.Languge.Calendar_Week3, UCML.Languge.Calendar_Week4, UCML.Languge.Calendar_Week5, UCML.Languge.Calendar_Week6],
    months: [UCML.Languge.Calendar_Month1, UCML.Languge.Calendar_Month2, UCML.Languge.Calendar_Month3, UCML.Languge.Calendar_Month4, UCML.Languge.Calendar_Month5, UCML.Languge.Calendar_Month6, UCML.Languge.Calendar_Month7, UCML.Languge.Calendar_Month8, UCML.Languge.Calendar_Month9, UCML.Languge.Calendar_Month10, UCML.Languge.Calendar_Month11, UCML.Languge.Calendar_Month12]
})


UCML.Calendar.prototype.init = function () {
    UCML.Calendar.superclass.init.call(this);

    // this.setSize();
    this.show();

}

UCML.Calendar.prototype.onRender = function () {
    this.el.addClass('calendar').wrapInner(
				'<div class="calendar-header">' +
					'<div class="calendar-prevmonth"></div>' +
					'<div class="calendar-nextmonth"></div>' +
					'<div class="calendar-prevyear"></div>' +
					'<div class="calendar-nextyear"></div>' +
					'<div class="calendar-title">' +
						'<span>Aprial 2010</span>' +
					'</div>' +
				'</div>' +
				'<div class="calendar-body">' +
					'<div class="calendar-menu">' +
						'<div class="calendar-menu-year-inner">' +
							'<span class="calendar-menu-prev"></span>' +
							'<span><input class="calendar-menu-year" type="text"></input></span>' +
							'<span class="calendar-menu-next"></span>' +
						'</div>' +
						'<div class="calendar-menu-month-inner">' +
						'</div>' +
					'</div>' +
				'</div>'
		);
    if (this.border == false) {
        this.el.addClass('calendar-noborder');
    }
    this.el.find('div.calendar-menu').hide();
}

UCML.Calendar.prototype.bindEvents = function () {
    UCML.Calendar.superclass.bindEvents.call(this);
    var opt = this;
    this.el.find('.calendar-title span').hover(
			function () { $(this).addClass('calendar-menu-hover'); },
			function () { $(this).removeClass('calendar-menu-hover'); }
		).click(function () {
		    var menu = opt.el.find('.calendar-menu');
		    if (menu.is(':visible')) {
		        menu.hide();
		    } else {
		        opt.showSelectMenus();
		    }
		});

    $('.calendar-prevmonth,.calendar-nextmonth,.calendar-prevyear,.calendar-nextyear', this.el).hover(
			function () { $(this).addClass('calendar-nav-hover'); },
			function () { $(this).removeClass('calendar-nav-hover'); }
		);
    this.el.find('.calendar-nextmonth').click(function () {
        opt.showMonth(1);
    });
    this.el.find('.calendar-prevmonth').click(function () {
        opt.showMonth(-1);
    });
    this.el.find('.calendar-nextyear').click(function () {
        opt.showYear(1);
    });
    this.el.find('.calendar-prevyear').click(function () {
        opt.showYear(-1);
    });

}

UCML.Calendar.prototype.showMonth = function (delta) {
    this.month += delta;
    if (this.month > 12) {
        this.year++;
        this.month = 1;
    } else if (this.month < 1) {
        this.year--;
        this.month = 12;
    }
    this.show();

    var menu = this.el.find('.calendar-menu-month-inner');
    menu.find('td.calendar-selected').removeClass('calendar-selected');
    menu.find('td:eq(' + (this.month - 1) + ')').addClass('calendar-selected');
}


UCML.Calendar.prototype.showYear = function (delta) {
    this.year += delta;
    this.show();

    var menu = this.el.find('.calendar-menu-year');
    menu.val(this.year);
}

UCML.Calendar.prototype.showSelectMenus = function () {
    this.el.find('.calendar-menu').show();

    var opt = this;
    if (this.el.find('.calendar-menu-month-inner').is(':empty')) {
        this.el.find('.calendar-menu-month-inner').empty();
        var t = $('<table></table>').appendTo(this.el.find('.calendar-menu-month-inner'));
        var idx = 0;
        for (var i = 0; i < 3; i++) {
            var tr = $('<tr></tr>').appendTo(t);
            for (var j = 0; j < 4; j++) {
                $('<td class="calendar-menu-month"></td>').html(this.months[idx++]).attr('abbr', idx).appendTo(tr);
            }
        }

        this.el.find('.calendar-menu-prev,.calendar-menu-next').hover(
					function () { $(this).addClass('calendar-menu-hover'); },
					function () { $(this).removeClass('calendar-menu-hover'); }
			);
        this.el.find('.calendar-menu-next').click(function () {
            var y = opt.el.find('.calendar-menu-year');
            if (!isNaN(y.val())) {
                y.val(parseInt(y.val()) + 1);
            }
        });
        this.el.find('.calendar-menu-prev').click(function () {
            var y = opt.el.find('.calendar-menu-year');
            if (!isNaN(y.val())) {
                y.val(parseInt(y.val() - 1));
            }
        });

        this.el.find('.calendar-menu-year').keypress(function (e) {
            if (e.keyCode == 13) {
                opt.setDate();
            }
        });

        this.el.find('.calendar-menu-month').hover(
					function () { $(this).addClass('calendar-menu-hover'); },
					function () { $(this).removeClass('calendar-menu-hover'); }
			).click(function () {
			    var menu = opt.el.find('.calendar-menu');
			    menu.find('.calendar-selected').removeClass('calendar-selected');
			    $(this).addClass('calendar-selected');
			    opt.setDate();
			});
    }



    var body = this.el.find('.calendar-body');
    var sele = this.el.find('.calendar-menu');
    var seleYear = sele.find('.calendar-menu-year-inner');
    var seleMonth = sele.find('.calendar-menu-month-inner');

    seleYear.find('input').val(this.year).focus();
    seleMonth.find('td.calendar-selected').removeClass('calendar-selected');
    seleMonth.find('td:eq(' + (this.month - 1) + ')').addClass('calendar-selected');

    if ($.boxModel == true) {
        sele.width(body.outerWidth() - (sele.outerWidth() - sele.width()));
        sele.height(body.outerHeight() - (sele.outerHeight() - sele.height()));
        seleMonth.height(sele.height() - (seleMonth.outerHeight() - seleMonth.height()) - seleYear.outerHeight());
    } else {
        sele.width(body.outerWidth());
        sele.height(body.outerHeight());
        seleMonth.height(sele.height() - seleYear.outerHeight());
    }
}

UCML.Calendar.prototype.setDate = function () {
    var menu = this.el.find('.calendar-menu');
    var year = menu.find('.calendar-menu-year').val();
    var month = menu.find('.calendar-selected').attr('abbr');
    if (!isNaN(year)) {
        this.year = parseInt(year);
        this.month = parseInt(month);
        this.show();
    }
    menu.hide();
}

UCML.Calendar.prototype.getWeeks = function (year, month) {
    var dates = [];
    var lastDay = new Date(year, month, 0).getDate();
    for (var i = 1; i <= lastDay; i++) dates.push([year, month, i]);

    // group date by week
    var weeks = [], week = [];
    while (dates.length > 0) {
        var date = dates.shift();
        week.push(date);
        if (new Date(date[0], date[1] - 1, date[2]).getDay() == 6) {
            weeks.push(week);
            week = [];
        }
    }
    if (week.length) {
        weeks.push(week);
    }

    var firstWeek = weeks[0];
    if (firstWeek.length < 7) {
        while (firstWeek.length < 7) {
            var firstDate = firstWeek[0];
            var date = new Date(firstDate[0], firstDate[1] - 1, firstDate[2] - 1)
            firstWeek.unshift([date.getFullYear(), date.getMonth() + 1, date.getDate()]);
        }
    } else {
        var firstDate = firstWeek[0];
        var week = [];
        for (var i = 1; i <= 7; i++) {
            var date = new Date(firstDate[0], firstDate[1] - 1, firstDate[2] - i);
            week.unshift([date.getFullYear(), date.getMonth() + 1, date.getDate()]);
        }
        weeks.unshift(week);
    }

    var lastWeek = weeks[weeks.length - 1];
    while (lastWeek.length < 7) {
        var lastDate = lastWeek[lastWeek.length - 1];
        var date = new Date(lastDate[0], lastDate[1] - 1, lastDate[2] + 1);
        lastWeek.push([date.getFullYear(), date.getMonth() + 1, date.getDate()]);
    }
    if (weeks.length < 6) {
        var lastDate = lastWeek[lastWeek.length - 1];
        var week = [];
        for (var i = 1; i <= 7; i++) {
            var date = new Date(lastDate[0], lastDate[1] - 1, lastDate[2] + i);
            week.push([date.getFullYear(), date.getMonth() + 1, date.getDate()]);
        }
        weeks.push(week);
    }

    return weeks;
}

UCML.Calendar.prototype.setValue = function (current) {
    this.current = current;
    this.year = this.current.getFullYear();
    this.month = this.current.getMonth() + 1;
    //   this.show();
}


UCML.Calendar.prototype.show = function () {
    this.el.find('.calendar-title span').html(this.months[this.month - 1] + ' ' + this.year);
    var opt = this;

    var body = this.el.find('div.calendar-body');
    body.find('>table').remove();

    var t = $('<table cellspacing="0" cellpadding="0" border="0"><thead></thead><tbody></tbody></table>').prependTo(body);
    var tr = $('<tr></tr>').appendTo(t.find('thead'));
    for (var i = 0; i < this.weeks.length; i++) {
        tr.append('<th>' + this.weeks[i] + '</th>');
    }
    var weeks = this.getWeeks(this.year, this.month);
    for (var i = 0; i < weeks.length; i++) {
        var week = weeks[i];
        var tr = $('<tr></tr>').appendTo(t.find('tbody'));
        for (var j = 0; j < week.length; j++) {
            var day = week[j];
            $('<td class="calendar-day calendar-other-month"></td>').attr('abbr', day[0] + ',' + day[1] + ',' + day[2]).html(day[2]).appendTo(tr);
        }
    }
    t.find("td[abbr^='" + this.year + ',' + this.month + "']").removeClass('calendar-other-month');

    var now = new Date();
    var today = now.getFullYear() + ',' + (now.getMonth() + 1) + ',' + now.getDate();
    t.find("td[abbr='" + today + "']").addClass('calendar-today');

    if (this.current) {
        t.find('.calendar-selected').removeClass('calendar-selected');
        var current = this.current.getFullYear() + ',' + (this.current.getMonth() + 1) + ',' + this.current.getDate();
        t.find("td[abbr='" + current + "']").addClass('calendar-selected');
    }

    t.find('tr').find('td:first').addClass('calendar-sunday');
    t.find('tr').find('td:last').addClass('calendar-saturday');

    t.find('td').hover(
			function () { $(this).addClass('calendar-hover'); },
			function () { $(this).removeClass('calendar-hover'); }
		).click(function () {
		    t.find('.calendar-selected').removeClass('calendar-selected');
		    $(this).addClass('calendar-selected');
		    var parts = $(this).attr('abbr').split(',');
		    opt.current = new Date(parts[0], parseInt(parts[1]) - 1, parts[2]);
		    opt.onSelect.call(opt, opt.current);
		});
}



UCML.Calendar.prototype.setSize = function (param) {
    UCML.Calendar.superclass.setSize.call(this, param);

    var t = this.el;

    if (!isNaN(this.width)) {
        if ($.boxModel == true) {
            t.width(this.width - (t.outerWidth() - t.width()));
            t.height(this.height - (t.outerHeight() - t.height()));
        } else {
            t.width(this.width);
            t.height(this.height);
        }
    }
    if (!isNaN(this.height)) {
        var body = t.children('.calendar-body');
        var header = t.children('.calendar-header');
        var height = t.height() - header.outerHeight();
        if ($.boxModel == true) {
            body.height(height - (body.outerHeight() - body.height()));
        } else {
            body.height(height);
        }
    }
}