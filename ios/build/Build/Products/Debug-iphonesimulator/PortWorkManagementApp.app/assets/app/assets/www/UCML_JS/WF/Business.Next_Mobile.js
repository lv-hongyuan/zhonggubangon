﻿/*!

*  
* @fileoverview JS版自定义控件，工作流通用下一步VC控件
*               开发思想 ： 1.定义数据源BC及对应的含义字段，在BC读取数据后，初始化内置对象 nextActivityData 
*                           2.根据 nextActivityData中定义的描述，绘制界面
*                           3.当用户操作界面时，nextActivityData 中的值联动变化
*                           4.最终可以通过 nextActivityData 值通过事件，传递到页面，由页面完成启动过程
*  
* @date: 2013-01-19 
* @author sunyi
* @version 1.0
*/

UCML.namespace("Business");
/**
* @class Business.Next
* @extends UCML.Applet
* @constructor
* @description 工作流通用启动【通用】自定义控件
* @param {object} id 容器控件id
*/
Business.Next = function (id) {
    this.addEvents('afterNextActivityData', 'submit', 'beforeClose');
    this.nextActivityData = new UCML.Common.HasTable();
    Business.Next.superclass.constructor.call(this, id);
};

/**
* @class Business.Next
* @extends UCML.Applet
* @description 编辑控件,继承自UCML.Applet
*/
UCML.extend(Business.Next, UCML.Applet, {
    ctype: "Business.Next",
    bindEvents: function () {
        Business.Next.superclass.bindEvents.call(this);
        if (!UCML.isEmpty(this.dataTable)) {
            this.BusinessObject.on("onAfterLoadData", this.loadData, this);
        }
    },
    onRender: function () {
        Business.Next.superclass.onRender.call(this);
        this.el.addClass("flow-next-body");
        this.el.append(this.toAcPanel); //选择节点区
        this.el.append(this.choiceManPanel);  //选择人员区
        this.el.append(this.toolButtonPanel);    //按钮控件区


    },
    //选人按钮出现规则
    //1:无论什么情况都出现选人按钮
    //2:无默认人时出现选人按钮
    //3:无论什么情况都不出现选人按钮
    butShowRule: 1,
    toAcPanel: $('<div class="flow-next-toAc"></div>'),
    choiceManPanel: $('<div class="flow-next-man"></div>'),
    toolButtonPanel: $('<div class="tool-botton"></div>'),
    //输出节点的输出模式（1,2）1：join输出 ,2:and输出
    splitMode: 1,

    //通过方法循环BC后得到下一步节点集合,含执行人信息，初始化时完成，已方便后来不用再次循环BC数据
    //nextActivityData: null,

    //执行人获取模式（0,1） 默认为0:Json串，可为另一个BC数据
    executorModel: 0,

    //流向节点信息结构
    nextActivity: {
        //下一步流向节点信息的数据BC对象
        nextActivityBC: null,
        //节点ID字段
        toActivityIDFName: "ToActivityID",
        //节点名称字段
        toActivityNameFName: "Name",
        //存放节点类型字段 -- 是否需要执行人 （自动节点，还是人工节点判断）
        toActivityTypeFName: "ActivityType",
        //任务分配模式 字段 -- 执行人为单选还是多选判断                     
        toTaskAssignModeFName: "TaskAssignMode",
        //当前节点输出类型字段 -- 选择节点是单选还是多选判断
        fromSplitModeFName: "FromSplitMode",
        //存放节点位置字段 -- 是否为结束节点
        toNodePostition: "NodePostition",
        //当执行人模式为0(json串)时，存放json串字段的字段名                     
        toMasterJsonFName: "MasterJson"
        //---------可扩展参阅人，会签人等

    },
    //执行人信息结构
    nextExecutor: {
        //下一步执行人信息的数据BC对象
        nextExecutorBC: null,
        //执行人名称字段
        personNameFName: "PersonName",
        //执行人key字段
        userOIDFName: "UserOID",
        //所属节点字段
        acIDFName: "ActivityID"
    },



    /**   
    * @method setNextActivityInfo 
    * @description 设置节点对应的字段名
    * @param  {UCML.Table} bcobject bc对象
    * @param  {String}    toActivityID 节点ID字段名 
    * @param  {string} toActivityName 节点名字段名 
    * @param  {string} toMasterJson 执行人Json字段名
    * @param  {string} toActivityType 节点类型字段名
    * @param  {string} toTaskAssignMode 节点执行模式字段名
    * @param  {string} fromSplitMode 流出节点流出模式字段名                    
    */
    setNextActivityInfo: function (bcobject, toActivityID, toActivityName, toMasterJson, toActivityType, toTaskAssignMode, fromSplitMode, toNodePostition) {
        this.nextActivity.nextActivityBC = bcobject;
        this.nextActivity.toActivityIDFName = toActivityID || "ToActivityID";
        this.nextActivity.toActivityNameFName = toActivityName || "Name";
        this.nextActivity.toMasterJsonFName = toMasterJson || "MasterJson";
        this.nextActivity.toActivityTypeFName = toActivityType || "ActivityType";
        this.nextActivity.toTaskAssignModeFName = toTaskAssignMode || "TaskAssignMode";
        this.nextActivity.fromSplitModeFName = fromSplitMode || "FromSplitMode";
        this.nextActivity.toNodePostition = toNodePostition || "NodePostition";

    },
    //设置执行人
    setExecutorInfo: function (bcobject, personNameFName, userOIDFName, acIDFName) {
        this.nextExecutor.nextExecutorBC = bcobject
        this.nextExecutor.personNameFName = personNameFName || "PersonName";
        this.nextExecutor.userOIDFName = userOIDFName || "UserOID";
        this.nextExecutor.acIDFName = acIDFName || "ActivityID";
    },
    //选中的节点对象
    acobject: null,
    //是否已经选中结束节点
    isCheckEndNode: false


});

Business.Next.prototype.init = function () {
    UCML.Edit.superclass.init.call(this);

}

Business.Next.prototype.loadData = function () {
    // debugger;
    UCML.Edit.superclass.init.call(this);
    this.initNextActivityData();
    this.buildToAcPanel();
    this.createtoolButton();
}

Business.Next.prototype.buildToAcPanel = function () {
    this.toAcPanel.html("");
    var pTitlePanel = $('<div class="title-panel">选择下一步流向:</div>');
    this.toAcPanel.append(pTitlePanel);
    var pChoicePanel = $('<div></div>');
    this.toAcPanel.append(pChoicePanel);
    this.createToACradio(pChoicePanel);
}


/**   
* @method buildChoiceManPanel 
* @description 创建选人区域
* @param  {String}    acid 节点ID 
* @param  {string} acname 节点中文名                        
*/
Business.Next.prototype.buildChoiceManPanel = function (acid, acname) {
    var cpanel = $('<div style="display: none;"></div>');
    cpanel.attr('AC', acid);
    this.choiceManPanel.append(cpanel);
    var a = this.nextActivityData.get(acid);
    if (a) {
        if (a.toActivityType == 1) { //人工节点需要执行人选择
            var pTitlePanel = $('<div class="title-panel">选择<font COLOR="red">' + acname + '</font>执行人:</div>');
            cpanel.append(pTitlePanel);
            var pChoicePanel = $('<div class="master-panel"></div>'); //主办人区
            cpanel.append(pChoicePanel);
            var n = this.createChoiceMan(pChoicePanel, acid);
            if (this.butShowRule == 1 || (this.butShowRule == 2 && !n))
                this.createChoiceManButton(pChoicePanel, acid);
        }
        a.view = cpanel;
    }
}




/**   
* @method createChoiceMan 
* @description 创建执行人
* @param  {object}    o 创建执行到o对象中(jquery object) 
* @param  {string} acid 节点id                        
*/
Business.Next.prototype.createChoiceMan = function (o, acid) {
    var rb = 0;
    if (o && acid) {
        var a = this.nextActivityData.get(acid);
        if (a) {
            var m = a.toTaskAssignMode;
            for (var i = 0; i < a.executor.getLength(); i++) {
                var b = a.executor.itemAt(i);
                var p = $('<span style="margin-left: 5px">' + b.PersonName + '</span>');
                var c;
                if (m == 1) {
                    c = $('<input type="radio" name="' + acid + '_executor" value="(U:' + b.UserOID + ')"/>');
                }	
                else {
                    c = $('<input type="checkbox" name="' + acid + '_executor" value="(U:' + b.UserOID + ')"/>');
                }
                c.attr('OID', b.UserOID);
                c.attr('checked', b.Ischeck);
                UCML.on(c, "click", this.manClick, this, a);
                p.prepend(c);
                o.append(p);
                rb = 1;
            }
        }
    }
    return rb;
}


/**   
* @method createChoiceManButton 
* @description 创建选择人按钮
* @param  {object}    o 创建执行到o对象中(jquery object) 
* @param  {string} acid 节点id                        
*/
Business.Next.prototype.createChoiceManButton = function (o, acid) {
    /*
	if (o) {
        var btp = $('<div style="text-align:right"></div>');
        var bt = $("<input type='button'  value='选人'/>");
        UCML.on(bt, "click", this.manButClick, this, acid);
        btp.append(bt);
        o.append(btp);
    }*/
	
	if (o) {
        var btp = $('<div style="text-align:right"></div>');
		var bta = $('<a data-rel="popup" href="#popupVideo" data-position-to="window"></a>');
        var bt = $("<input type='button'  value='选人' class='button_choose'/>");
        UCML.on(bt, "click", this.manButClick_Mobile, this, acid);
		bta.append(bt);
		btp.append(bta);
        o.append(btp);
    }
}

Business.Next.prototype.manButClick_Mobile = function(e, el) {
	var a = this.nextActivityData.get(el.data);
	if (a) {
		this.acobject = a;
		var url = "../selectMan_Mobile.html?type=1";
		$("#selectManIframe").attr("src",url);
        $("#popupVideo").show();
		
		pm.bind("selectManEntry", function (data) {
			//该data是选人页面的tempData,成功返回后重绘人员选项
			
			var str = "",strN = "";
			var prefix = false;
			for (var p in data) {
				if (prefix) {
					str += ",";
					strN += ",";
				}
				str += data[p].oid;
				strN += data[p].name;
				
				prefix = true;
			}
			
			selectMan(str, strN);
			
            //$("#popupVideo").popup("close");
            $("#popupVideo").hide();
		});

        //选人页面返回按钮回调操作
        pm.bind("selectManClose", function (data) {
            //$("#popupVideo").popup("close");
            $("#popupVideo").hide();
        });
	}
}

/**   
* @method manButClick 
* @description 选择人按钮onclick按下时，响应函数
* @param  {object}    e 按下的按钮对象 
* @param  {object}   el  按下的按钮对象                       
*/
Business.Next.prototype.manButClick = function (e, el) {
    var a = this.nextActivityData.get(el.data);
    if (a) {
        this.acobject = a;
        var strUrl = "BPO_Select_User_Organize_MainJS.aspx?type=8&callbackFn=selectMan";
        var w = new UCML.OpenShowWindow({ frameMode: "frame", maximizable: true,
            collapsible: true, URL: strUrl, scroll: "yes", draggable: true,
            resizable: true, width: 700, height: 600, title: "选人"
        });
        w.open();
    }
}
/**   
* @method selectMan 
* @description 当选人页面执行选人确定后，回调bpo页面函数，可回调此函数，完成从新创建选人部分
* @param  {string}    str 选择的人id串 
* @param  {string}   strN  选择的人名字串                      
*/
Business.Next.prototype.selectMan = function (str, strN) {
    if (!str || !strN) {
        return;
    }
	
    var getmanArr = function (id, Caption) {
        //去头部逗号
        if (id.indexOf(",") == 0) id = id.substring(1, id.length - 1);
        var idarr = id.split(",");
        var Captionarr = Caption.split(",");
        var objArr = new Array();
        for (var i = 0; i < idarr.length; i++) {
            if (idarr[i].length == 36) {
                idarr[i] = "(U:" + idarr[i] + ")";
            }
            var o = new Object();
            //o.tp = idarr[i].split(":")[0].substring(idarr[i].split(":")[0].length - 1, idarr[i].split(":")[0].length);
            o.id = idarr[i].split(":")[1].substring(0, 36);
            o.name = Captionarr[i];
            o.fvalue = idarr[i];
            objArr.push(o);
        }
        return objArr;
    }
    if (this.acobject) {
        //debugger;
        var a = new UCML.Common.HasTable();
        this.acobject.executor = a;
        var manArr = getmanArr(str, strN);
        for (var i = 0; i < manArr.length; i++) {
            var manInfo = new Object();
            manInfo.PersonName = manArr[i].name;
            manInfo.UserOID = manArr[i].id;
            manInfo.value = manArr[i].fvalue;
            if ((this.acobject.toTaskAssignMode == 1 && i == 0) || this.acobject.toTaskAssignMode != 1) {
                manInfo.Ischeck = true;
            }
            else manInfo.Ischeck = false;
            a.add(manInfo.UserOID, manInfo);
        }
        var p = this.choiceManPanel.children("div[AC=" + this.acobject.toActivityID + "]").children(".master-panel");
        p.html("");
        this.createChoiceMan(p, this.acobject.toActivityID);
        this.createChoiceManButton(p, this.acobject.toActivityID);
    }
}




/**   
* @method manClick 
* @description  选中人（单选或多选按钮onclick）时，事件处理
* @param  {object}    e 
* @param  {object}   el                       
*/
Business.Next.prototype.manClick = function (e, el) {
    //清除选中的人 
    var clearManCheck = function (manobj) {
        var length = manobj.getLength();
        for (var i = 0; i < length; i++) {
            manobj.itemAt(i).Ischeck = false;
        }
    }
    var m = el.data.toTaskAssignMode;
    if (m == 1) {  //单选人,将已经选择的人员记忆住        
        clearManCheck(el.data.executor);
        var uoid = e.attr('OID');
        var man = el.data.executor.get(uoid);
        man.Ischeck = true;
    }
    else {  //多选人
        var uoid = e.attr('OID');
        var man = el.data.executor.get(uoid);
        man.Ischeck = e.attr('checked');
    }
}


/**   
* @method createToACradio 
* @description  创建可选节点区域，根据分类形成多选或单选
* @param  {object} o   创建执行到o对象中(jquery object)                      
*/
Business.Next.prototype.createToACradio = function (o) {
    if (o) {
        if (this.splitMode == 2) {
            for (var i = 0; i < this.nextActivityData.getLength(); i++) {
                var a = this.nextActivityData.itemAt(i);
                var pRadio = $("<input type='checkbox' name='nextNode' np='" + a.toNodePostition + "' value='" + a.toActivityID + "'  />");
                var pspan = $("<span style='margin-left: 5px'>" + a.toActivityName + "</span>");
                pspan.prepend(pRadio);
                o.append(pspan);
                UCML.on(pRadio, "click", this.acCheckboxClick, this, pRadio);
                this.buildChoiceManPanel(a.toActivityID, a.toActivityName);
            }
        }
        else {
            for (var i = 0; i < this.nextActivityData.getLength(); i++) {
                var a = this.nextActivityData.itemAt(i);
                var pRadio = $("<input type='radio' name='nextNode' np='" + a.toNodePostition + "' value='" + a.toActivityID + "'  />");

                var pspan = $("<span style='margin-left: 5px'>" + a.toActivityName + "</span>");
                pspan.prepend(pRadio);
                o.append(pspan);
                UCML.on(pRadio, "click", this.acRadioClick, this, pRadio);
                this.buildChoiceManPanel(a.toActivityID, a.toActivityName);
                if (i == 0) {
                    pRadio.attr("checked", true);
                    a.Ischeck = true;
                    this.acRadioClick(pRadio);
                    // this.buildChoiceManPanel(a.toActivityID, a.toActivityName);
                }
            }
        }
    }

}


/**   
* @method acRadioClick 
* @description  单选节点时，事件处理
* @param  {object}    e 
* @param  {object}   el                       
*/
Business.Next.prototype.acRadioClick = function (e, el) {
    var length = this.nextActivityData.getLength();
    for (var i = 0; i < length; i++) {
        if (e.val() == this.nextActivityData.itemAt(i).toActivityID) {
            this.nextActivityData.itemAt(i).Ischeck = true;
            this.choiceManPanel.children('div[AC="' + e.val() + '"]').show();
        }
        else {
            this.nextActivityData.itemAt(i).Ischeck = false;
            this.choiceManPanel.children('div[AC="' + this.nextActivityData.itemAt(i).toActivityID + '"]').hide();
        }
    }
}

/**   
* @method acCheckboxClick 
* @description  多选节点时，事件处理
* @param  {object}    e 
* @param  {object}   el                       
*/
Business.Next.prototype.acCheckboxClick = function (e, el) {
    var a = this.nextActivityData.get(e.val());
    if (e[0].checked == true) {
        if (a.toNodePostition == 1) {
            this.toAcPanel.find(":checkbox[name='nextNode'][np!='1']").attr("checked", false);
            this.isCheckEndNode = true;
            this.acRadioClick(e, el);
        }
        else {
            if (this.isCheckEndNode == true) {
                var np = this.toAcPanel.find(":checkbox[name='nextNode'][np ='1']");
                if (np.length > 0) {
                    this.nextActivityData.get(np.val()).Ischeck = false;
                    np.attr("checked", false);
                    this.isCheckEndNode = false;
                }
            }
            this.choiceManPanel.children('div[AC="' + e.val() + '"]').show();
            a.Ischeck = true;
        }
    }
    else {
        this.choiceManPanel.children('div[AC="' + e.val() + '"]').hide();
        a.Ischeck = false;
    }
}

/**   
* @method createtoolButton 
* @description  创建按钮确定按钮
* @param  {object}    o 创建执行到o对象中(jquery object)                        
*/
Business.Next.prototype.createtoolButton = function (o) {
    o = o || this.toolButtonPanel;
    var submitb = $("<input type='button' value='  确 认 ' class='button_yes'>");
    var cancelb = $("<input type='button' value='  关 闭 ' class='button_yes button_close'>");
    o.append(submitb);
    o.append(cancelb);
    UCML.on(submitb, "click", this.submitclick, this, submitb);
    UCML.on(cancelb, "click", this.cancelclick, this, cancelb);
}

/**   
* @method submitclick 
* @description  点击确认按钮时，事件响应函数。
* @param  {object}   e                     
*/
Business.Next.prototype.submitclick = function (e) {
    
    var a = this.getAllcheckAC();
    this.fireEvent('submit', a);
}
/**   
* @method cancelclick 
* @description  点击关闭按钮时，事件响应函数。
* @param  {object}   e                     
*/
Business.Next.prototype.cancelclick = function (e) {
    pm({
        target: window.parent,
        type: "sendTaskClose",
        success: function (text) {
            
        }
    });
    /*
    if (window.currentWindow) {
        window.currentWindow.close();
    }*/
}
/**   
* @method getAllcheckAC 
* @description  获取被选中的节点和被选中的人的数组
* @return  {array}   结构为 [{"id":"节点id1","error":true,"errormeg":"错误提示信息1","masterPerformers":"主办人串1" },
*                            {"id":"节点id2","error":true,"errormeg":"错误提示信息2","masterPerformers":"主办人串2" }]                    
*/
Business.Next.prototype.getAllcheckAC = function () {
    var a = [];
    if (this.nextActivityData) {
        //debugger;
        var length = this.nextActivityData.getLength();
        for (var i = 0; i < length; i++) {
            var na = this.nextActivityData.itemAt(i);
            if (na.Ischeck) {
                var obj = new Object();
                obj.id = na.toActivityID; //选中的节点
                var mv = [];
                if (na.toActivityType == 1) {
                    for (var j = 0; j < na.executor.getLength(); j++) {
                        if (na.executor.itemAt(j).Ischeck) {
                            mv.push(na.executor.itemAt(j).value);
                        }
                    }
                }
                obj.masterPerformers = mv.join(',');      //节点主办人
                if (na.toActivityType == 1 && obj.masterPerformers == "") {
                    obj.error = true;
                    obj.errormeg = "【" + na.toActivityName + "】为人工节点，请选择执行人";
                }
                a.push(obj);
            }
        }
    }
    return a;
}

/**   
* @method initNextActivityData 
* @description  通过循环BC初始化nextActivityData对象和splitMode对象
*     this.nextActivityData对象 结构为 
*      {
*               toActivityID        @type string    流向的节点ID
*               toActivityName      @type string    流向的节点名
*               toActivityType      @type string    流向的节点类型(自动节点，人工节点，路由节点，子流程节点)
*               toTaskAssignMode    @type string    流向的节点任务分配方式(全部人员，负载平衡)
*               toNodePostition     @type string    流向节点的位置 (开始位置，结束位置，中间位置)
*               Ischeck             @type boolean   是否选中此节点 
*               view                @type object    对应的选人区域对象    
*               executor            @type UCML.Common.Collection    执行人信息
*               {
*                 PersonName  @type string  执行人名
*                 UserOID     @type string  执行人UserOID
*                 value       @type string  执行人群组串 如：(U:0000000-0000-0000-0000-000000000001)
*                 Ischeck     @type boolean 是否选中
*               }
*       }
*                                         
*/
Business.Next.prototype.initNextActivityData = function () {
    if (this.nextActivity.nextActivityBC) {
        for (var i = 0; i < this.nextActivity.nextActivityBC.getRecordCount(); i++) {
            this.nextActivity.nextActivityBC.SetIndexNoEvent(i);
            var o = new Object();
            o.toActivityID = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toActivityIDFName);
            o.toActivityName = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toActivityNameFName);
            o.toActivityType = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toActivityTypeFName); ;
            o.toTaskAssignMode = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toTaskAssignModeFName);
            o.toNodePostition = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toNodePostition);
            o.Ischeck = false;
            o.view = null;
            if (i == 0) {
                var lo = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.fromSplitModeFName);
                if (lo && lo != "") this.splitMode = lo;
            }
            var a = new UCML.Common.HasTable();
            if (this.executorModel == 0) {
                var x = this.nextActivity.nextActivityBC.getFieldValue(this.nextActivity.toMasterJsonFName);
                try {
                    var manInfoArr = eval(x);
                    for (var j = 0; j < manInfoArr.length; j++) {
                        if (manInfoArr[j].UserOID && manInfoArr[j].UserOID != "") {
                            manInfoArr[j].value = "(U:" + manInfoArr[j].UserOID + ")";
                        }
                        else {
                            manInfoArr[j].value = "";
                        }
                        a.add(manInfoArr[j].UserOID, manInfoArr[j]);
                    }
                }
                catch (e) { }
            }
            else {
                for (var j = 0; j < this.nextExecutor.nextExecutorBC.getRecordCount(); j++) {
                    this.nextExecutor.nextExecutorBC.SetIndexNoEvent(j);
                    if (this.nextExecutor.nextExecutorBC.getFieldValue(this.nextExecutor.acIDFName) && this.nextExecutor.nextExecutorBC.getFieldValue(this.nextExecutor.acIDFName) == o.toActivityID) {
                        var manInfo = new Object();
                        manInfo.PersonName = this.nextExecutor.nextExecutorBC.getFieldValue(this.nextExecutor.personNameFName);
                        manInfo.UserOID = this.nextExecutor.nextExecutorBC.getFieldValue(this.nextExecutor.userOIDFName);
                        if (manInfo.UserOID && manInfo.UserOID != "")
                            manInfo.value = "(U:" + manInfo.UserOID + ")"
                        else
                            manInfo.value = "";
                        manInfo.Ischeck = false;
                        a.add(manInfo.UserOID, manInfo);
                    }
                }
            }
            o.executor = a;
            this.nextActivityData.add(o.toActivityID, o);
        }
        this.fireEvent('afterNextActivityData', this.nextActivityData);
    }
    // this.nextActivityData = arr;
}


