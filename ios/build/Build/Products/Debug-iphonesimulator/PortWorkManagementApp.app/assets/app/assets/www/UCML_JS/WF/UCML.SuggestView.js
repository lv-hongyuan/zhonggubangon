﻿/*
 *流程意见控件
 *date:2013/06/06
 */

UCML.SuggestView = function (id) {
    //意见的BC字段名称
	this.ActivityIDField="ActivityID";					//分组字段,以活动编号分组
    this.ActivityNameField="ActivityName";				//分组标签字段,显示活动名称
    this.PersonNameField = "PersonName"; 				//审批人
    this.DateField = "SYS_Created"; 					//创建日期
    this.OpinionTypeField = "BusiField1"; 				//审批结果
    this.OpinionField = "Idea"; 						//审批意见
    this.ActivityChineseName = "ActivityChineseName"; 	//任务名称
    this.FinishDate = "FinishDate"; 					//完成时间
		this.FileNameField = "FileName";					//文件名
		this.HaveFiles=true;
		var OpinionCodeArray = [["0", "已阅"], ["1", "同意"], ["2", "不同意"]];
	 
    this.HaveHead = true;
    
    UCML.SuggestView.superclass.constructor.call(this, id); //必须调用父类初始化
}

UCML.extend(UCML.SuggestView, UCML.Applet, {
	SuggestItems:[],
	FileBCName:"BC_WF_AssignTaskFiles",
    FileM_FK :"AssignTaskOID",
	IsAllowViewIdea:true, 		//是否过滤意见内容，对应工作流活动节点'禁止浏览意见的节点'功能。默认为过滤。
	IsShowEmpty:false,			//是否显示意见为空的数据，默认为不显示
	bindEvents:function(){
		UCML.Applet.superclass.bindEvents.call(this);
        this.dataTable.on("onLoad", this.loadData, this);
        this.fileDataTable=eval(this.FileBCName + "Base");
        this.fileDataTable.on("onLoad", this.loadData, this);
	},
	/*
	 *从BC中加载数据
	 */
	loadData:function(){
				if(!this.dataTable.dataStore||!this.fileDataTable.dataStore){
					return false;	
				}
        this.SuggestItems=[];
        for (var i = 0; i < this.dataTable.getRecordCount(); i++) {
            this.dataTable.SetIndexNoEvent(i);
            if (this.IsShowEmpty == false && this.dataTable.getFieldValue(this.OpinionField) == '') continue;
            if (this.IsAllowViewIdea) {
                var NotViewIdea = this.dataTable.getFieldValue('NotAllowViewIdeaNodes');
               
                if (NotViewIdea.indexOf(this.dataTable.getFieldValue(this.ActivityIDField)) != -1) {
                    continue;
                }
            }
			
            var suggest = new UCML.SuggestView.WFSuggest();
            suggest.OID = this.dataTable.getFieldValue(this.dataTable.TableName + "OID");
            suggest.PersonName = this.dataTable.getFieldValue(this.PersonNameField);
            suggest.Date = this.dataTable.dateFilterValue(this.dataTable.getFieldValue(this.DateField),'y-m-d h:mi:s');
            suggest.OpinionType = this.dataTable.getFieldValue(this.OpinionTypeField);
            suggest.Opinion = this.dataTable.getFieldValue(this.OpinionField);
            suggest.ActivityChineseName = this.dataTable.getFieldValue(this.ActivityChineseName);
            suggest.FinishDate = this.dataTable.getFieldValue(this.FinishDate);
            if (this.HaveFiles == true || this.HaveFiles == "true") {
                suggest.Files = this.GetFilesByMOID(suggest.OID);
            }
            this.SuggestItems.push(suggest);
        }
		//渲染内容
		this.box.children().remove();
		if(this.SuggestItems.length!=0){
			var thisControl=this;
			$.each(this.SuggestItems,function(index,el){
				var $table=$(String.format(thisControl.ItemHTML,el.PersonName,el.OpinionType,el.FinishDate,el.Opinion,el.Files,el.Date,index,0,el.ActivityChineseName));
				$table.find("img").click(thisControl.toggle).attr("src",UCMLLocalResourcePath+"Images/SuggestView/flex.gif");
				thisControl.box.append($table).find('table a[oid]').unbind("click").click(thisControl.downloadFile);
			});
			
		}
	},
	
	ItemHTML:'<table cellpadding="0" cellspacing="0" bordercolor="#c5c4c2"><tr><th class="form_th" width="20%"><span>任务名称：<b>{8}</b></span></th><th class="form_th" style="font-weight:100;"><span>执行人：<b>{0}</b></span><span>执行结果：<b>{1}</b></span><span>完成时间：{2}</span><span class="flex"><a href="javascript:void(0)" ><img width="10" height="10" index="{6}" state="{7}"/></a></span></tr><tr><td class="td1">执行意见</td><td class="td2">{3}</td></tr><tr><td class="td3">文件信息</td><td class="td4">{4}&nbsp;</td></tr><tr><td class="td1">创建日期:</td><td class="td2">{5}</td></tr></table>',
	
	/*
	 *显示控件
	 */
	onRender:function(){
		this.box=$('<div class="table_box"></div>');
		this.el.append(this.box);
	},
	
	showItem:function(index){
	},
	toggle:function(){
		var index=parseInt($(this).attr("index"));
		var state=parseInt($(this).attr("state"));
		if(state==0){
			$(this).attr("src",UCMLLocalResourcePath+"Images/SuggestView/flex1.gif").attr("state",1);;
		}
		else {
			$(this).attr("src",UCMLLocalResourcePath+"Images/SuggestView/flex.gif").attr("state",0);
		}
		$("table").eq(index).find("tr>td").toggle();
	},
	
	downloadFile:function ()
	{
		var OID=$(this).attr("oid");
		if(OID==null||OID=="") return false;
		var url=UCMLLocalResourcePath+"UCMLCommon/WF/BPO_WF_DownLoadTaskFiles.aspx?RECORDOID="+OID;
		window.open(url);
	},
	
	GetFilesByMOID:function (OID) {
		var BCBase = eval(this.FileBCName + "Base");
		var files = new Array();
		var RecordObject;
		var count = BCBase.getRecordCount();
		for (var i = 0; i < count; i++) {
			BCBase.SetIndexNoEvent(i);
			if (BCBase.getFieldValue(this.FileM_FK) == OID) {
				RecordObject = new Object();
				RecordObject.OID = BCBase.getFieldValue(BCBase.TableName+"OID");
				RecordObject.FileName = BCBase.getFieldValue(this.FileNameField);
				files.push(RecordObject);
			}
		}
		return $.map(files,function(file,index){
			var link='<a href="javascript:void(0)" title="点击下载" oid="{0}">{1}</a>';
			return String.format(link,file.OID,file.FileName);
		}).join("&nbsp;&nbsp;");
	}
});

/*
 *意见对象
 */
UCML.SuggestView.WFSuggest=function (){
	//意见属性定义
	this.OID=null;
    this.PersonName = null;
    this.Date =null;
    this.OpinionType = null;
    this.Opinion =null;
    this.ActivityChineseName = null;
    this.FinishDate =null;
	this.HaveFiles = true;
    this.Files ="";		
}