﻿
UCML.DataGrid = function (id) {
    this.frozenColumns = new Array(); //固定列
    this.notfrozenColumns = new Array(); //非固定列
    this.addEvents(
            'click', //鼠标单击时
            'dblclick', //鼠标双击时
            'contextmenu', //鼠标右击时
            'mousedown', //鼠标按下时
            'mouseup', //鼠标弹起时
            'mouseover', //鼠标移至时
            'mouseout', //鼠标移出时
            'keypress', //键盘按下时,按下立即响应
            'keydown', //键盘按下时,弹起时响应
            'cellkeydown',//单元格keydown事件
            'cellmousedown', //单元格鼠标按下时  
            'rowmousedown', //行鼠标按下时
             'headermousedown', //表头鼠标按下时
             'groupmousedown',
              'containermousedown',
             'cellclick', //单元格单击时
            'celldblclick', //单元格双击时
            'rowclick', //行单击时
            'rowdblclick', //行双击时
            'headerclick', //表头单击时
            'headerdblclick', //表头双击时
                'groupclick',
                'groupdblclick',
                'containerclick',
                'containerdblclick',
            'rowcontextmenu', //行右击鼠标时
            'cellcontextmenu', //单元格右击鼠标时
            'headercontextmenu', //表头右击鼠标时
                'groupcontextmenu',
                'containercontextmenu',

            'bodyscroll', //body滚动时
            'columnresize', //列调宽度变化时
    //            'columnmove',
            'sortchange', //自动排序时
                'groupchange',
            "beginedit", //编辑开始时
            "beforebeginedit", //编辑开始前
            "fieldchange", //字段变化时
            "endedit", //编辑结束时
            "beforerowrender", //行绘制前
            "afterrowrender", //行绘制后
            "checkChange"//复选框选择变化时 选中行也会触发 但是必须是多行选择模型才行
        );

    //  this.enableBubble("beforerowrender"); 增加冒泡事件

    this.checkOID = [];
    UCML.DataGrid.superclass.constructor.call(this, id);
}

UCML.extend(UCML.DataGrid, UCML.Applet, {
    itemFirstText: UCML.Languge.DLLitemFirstText,
    /**   
    * @property headRowCount 
    * @description 表头行数
    * @type int
    */
    headRowCount: 1,

    pasteRowFormat: "", //Peter ADD 
    /** excel粘贴格式
    etc:3,2,5,1
    剪贴板的1列贴入到Grid的第3列
    剪贴板的2列贴入到Grid的第2列
    剪贴板的3列贴入到Grid的第5列
    剪贴板的4列贴入到Grid的第1列

    */

    /**   
    * @property rowNumbers 
    * @description 是否显示行号
    * @type Boolean
    */
    rowNumbers: false,

    /**   
    * @property rowNumberWidth 
    * @description 显示列号占的宽度，自适应式为百分比，否则为像素
    * @type int
    */
    rowNumberWidth: null,

    /**   
    * @property rowNumberText 
    * @description 行号表头显示的文本，默认为‘序号’
    * @type String
    */
    rowNumberText: UCML.Languge.DBRowNumberText,

    /**   
    * @property showCheckBox 
    * @description 是否显示复选框
    * @type Boolean
    */
    showCheckBox: false,

    /**   
    * @property crossPageCheck 
    * @description 跨页复选框选择
    * @type Boolean
    */
    crossPageCheck: false,

    /**   
    * @property showCheckBoxWidth 
    * @description 复选框列号占的宽度，自适应式为百分比，否则为像素
    * @type int
    */
    showCheckBoxWidth: null,

    /**   
    * @property border 
    * @description 是否选择边框
    * @type Boolean
    */
    border: true,

    /**   
    * @property striped 
    * @description 是否启用交叉行
    * @type Boolean
    */
    striped: true,

    /**   
    * @property havePage 
    * @description 是否分页
    * @type Boolean
    */
    havePage: true,

    /**   
    * @property rowHeight 
    * @description 数据行高，默认null，(不用写单位)
    * @type int
    */
    rowHeight: null,

    /**   
    * @property headRowHeight 
    * @description 表头行高，默认null，(不用写单位)
    * @type int
    */
    headRowHeight: null,

    /**
    * @description 行号列是否在复选框列前
    */
    rowNumberBeforeCheckbox: true,

    /**
    * @description 自是否定义右键菜单
    */
    haveCustomMenu: false,

    /**   
    * @property autoSort 
    * @description 是否排序
    * @type Boolean
    */
    autoSort: true,

    /**   
    * @property haveQueryRow 
    * @description 是否显示查询行
    * @type Boolean
    */
    haveQueryRow: false,

    /**   
    * @property haveMergerTable 
    * @description 是否默认执行合并表格方法
    * @type Boolean
    */
    haveMergerTable: false,

    /**
    * @property keyFieldName 
    * @description 键值列名称
    * @type String
    */
    keyFieldName: null,

    /**
    * @property mergerFields 
    * @description 根据键值列名称合并列
    * @type String
    */
    mergerFields: null, //值根据;分开

    /**   
    * @property haveMenu 
    * @description 是否显示菜单
    * @type Boolean
    */
    haveMenu: false,

    /**   
    * @property enabledEdit 
    * @description 是否允许编辑
    * @type Boolean
    */
    enabledEdit: true,
    colUnit: "px",

    /**   
    * @property nowrap 
    * @description 文字不换行
    * @type Boolean
    */
    nowrap: true,

    /**   
    * @property noHeader 
    * @description 是否显示表头
    * @type Boolean
    */
    noHeader: false,

    /**   
    * @property rowOverCls 
    * @description 鼠标方式浮动样式
    * @type String
    */
    rowOverCls: "datagrid-row-over",

    /**   
    * @property rowSelectCls 
    * @description 选中行样式
    * @type String
    */
    rowSelectCls: "datagrid-row-selected",
    /**
    * @property cellSelectCls 
    * @description 选中单元格样式
    * @type String
    */
    cellSelectCls: "active-cell",

    activeRowCls: "datagrid-row-active",

    ruleCheckCellCls: "rulecheck-cell",
    // resize: false, //私有的调整大小中

    /**   
    * @property trackMouseOver 
    * @description 鼠标方式是否显示浮动样式
    * @type Boolean
    */
    trackMouseOver: false,

    /**   
    * @property clicksToEdit 
    * @description 编辑模式 1，单击编辑，2双击编辑
    * @type int
    */
    clicksToEdit: 1,

    /**   
    * @property clicksToCheck 
    * @description 点击行复选框选中模式 0无， 1，单击选中
    * @type int
    */
    clicksToCheck: 1,

    /**   
    * @property rowSelectMode 
    * @description 行选择模式 1，单行选择，2多行选择
    * @type int
    */
    rowSelectMode: 1, //0,无,1单行,2多行

    /**   
    * @property groupToggleMode 
    * @description 分组切换模式 1，单击，2双击 ，0不切换
    * @type int
    */
    groupToggleMode: 1,

    /**   
    * @property showPager 
    * @description 是否显示分页栏
    * @type Boolean
    */
    showPager: true,

    /**
    * @property cellSelectMode 
    * @description 单元格选择模式 1，单行选择，2多行选择
    * @type int   
    */
    cellSelectMode: 1, //0,无,1单格,2多格

    showLoadMask: true,

    /**   
    * @property changeDataToView 
    * @description 数据发生变化是否触发视图变化
    * @type Boolean
    */
    changeDataToView: true,

    /**   
    * @property enabledSelect 
    * @description 是否启用选择单元格选中
    * @type Boolean
    */
    enabledSelect: false,

    /**   
    * @property insertRecordTop 
    * @description 插入记录是否顶部
    * @type Boolean
    */
    insertRecordTop: false,

    /**   
    * @property insertRecordByActiveRow 
    * @description 是否启用通过焦点行控制插入记录位置
    * @type Boolean
    */
    insertRecordByActiveRow: false,

    /**   
    * @property insertRecordByActiveRowTop 
    * @description 是否启用通过焦点行控制插入记录位置，true焦点行上面，否则为焦点行下面
    * @type Boolean
    */
    insertRecordByActiveRowTop: false,

    /**   
    * @property asyncLoadData 
    * @description 是否异步加载数据，true启用异步装载
    * @type Boolean
    */
    asyncLoadData: true,

    /**   
    * @property asyncLoadDataByRecordCount 
    * @description 异步装载符合多少条才开始
    * @type int
    */
    asyncLoadDataByRecordCount: 300,

    /**   
    * @property paginationCtype 
    * @description 自定义分页控件类型
    * @type String
    */
    paginationCtype: "UCML.PaginationMin",

    frozenMasterStr: '<div class="datagrid-view"><textarea class="toexcel-area" ></textarea><div class="datagrid-view1"style="{view1style}">' +
    '<div class="datagrid-header"><div class="datagrid-header-inner">{frozenHeaderStr}</div></div>' +
    '<div class="datagrid-body"><div class="datagrid-body-inner">{frozenBodyStr}</div></div></div>' +
    '<div class="datagrid-view2" style="{view2style}"><div class="datagrid-header"><div class="datagrid-header-inner">' +
    '{headerStr}</div></div><div class="datagrid-body">{bodyStr}</div></div>' +
			  '<div class="datagrid-resize-proxy"></div></div><div class="datagrid-pager"></div><div class="datagrid-edit-body"></div>',
    masterStr: '<div tabindex="-1" hidefocus="on" class="datagrid-view"><textarea class="toexcel-area" ></textarea><div class="datagrid-header"><div class="datagrid-header-inner">' +
    '{headerStr}</div></div><div class="datagrid-body">{bodyStr}</div>' +
			  '<div class="datagrid-resize-proxy"></div></div><div class="datagrid-pager"></div><div class="datagrid-edit-body"></div>',
    frozenHeaderStr: '<table border="0" cellspacing="0" cellpadding="0" style="{tstyle}" class="datagrid-header-table"><tbody>{columnRow}{rows}</tbody></table>',
    headerStr: '<table border="0" cellspacing="0" cellpadding="0" style="{tstyle}" class="datagrid-header-table"><tbody>{columnRow}{rows}</tbody></table>',
    frozenBodyStr: '<table tabindex="-1" hidefocus="on" border="0" cellspacing="0" cellpadding="0" style="{tstyle}" class="datagrid-body-table"><tbody>{columnRow}{rows}</tbody></table>',
    bodyStr: '<table tabindex="-1" border="0"  cellspacing="0" cellpadding="0" style="{tstyle}"  class="datagrid-body-table"><tbody>{columnRow}{rows}</tbody></table>',
    headerRowStr: '<tr style="{style}">{cells}</tr>',
    headerCellStr: '<td style="{style}" fieldName="{fieldName}" frozen="{frozen}" title="{title}" class="datagrid-header-cell"><div class="datagrid-cell" ><span class="datagrid-header-text">{value}</span><span class="datagrid-sort-icon">&#160;</span></div></td>',
    headerNumberStr: '<td style="{style}"class="datagrid-header-cell" rowspan="{rowspan}"><div class="datagrid-header-rownumber" >{value}</div></td>',
    headerShowCheckBoxStr: '<td style="{style}" class="datagrid-header-cell" rowspan="{rowspan}" ><div class="datagrid-cell datagrid-header-check datagrid-cell-nowrap" ><input type="checkbox" value="on" /></div></td>',
    bodyRowStr: '<tr class="datagrid-row {cls}" rowType="bodyRow" OID="{OID}" style="{style}"  >{cells}</tr>',
    bodyCellStr: '<td tabIndex="0" hidefocus="on" style="{style}"  class="datagrid-td datagrid-column-{fieldName}" fieldName="{fieldName}" >' +
    '<div  class="datagrid-cell {cls}"  title="{title}" >{value}' +
    '</div></td>',
    columnStr: '<td  style="height: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; border-top-color: currentColor; border-right-color: currentColor; border-bottom-color: currentColor; border-left-color: currentColor; border-top-width: 0px; border-right-width: 0px; border-bottom-width: 0px; border-left-width: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none;{style}"   fieldName="{fieldName}" ></td>',
    columnRowStr: '<tr style="{style}" rowType="colRow" >{cells}</tr>',
    bodyNumberStr: '<td class="datagrid-td-rownumber datagrid-cell-height" style="{style}"><div class=" datagrid-cell-rownumber">{value}</div></td>',
    bodyShowCheckBoxStr: '<td class="datagrid-td" style="{style}"><div class="datagrid-cell datagrid-cell-check datagrid-cell-nowrap"><input type="checkbox" value="on"  {checked} /></div></td>',
    queryCellStr: '<td  fieldName="{fieldName}"  style="{style}" class="datagrid-header-query-cell"><TABLE border=0 cellSpacing=0 cellPadding=0  class="datagrid-header-query-tb"><TBODY><TR>' + '<TD class="datagrid-header-query-tb-cell" style="padding:0px; margin:0px;border:0px;"  width="100%" noWrap><INPUT  fieldName="{fieldName}" id="queryInput_{fieldName}{VCName}" isCodeTable="{isCodeTable}" codeTable="{codeTable}"  ctype="{ctype}"  style="BORDER-BOTTOM: 0px; BORDER-LEFT: 0px; WIDTH: 100%; BORDER-TOP: 0px; BORDER-RIGHT: 0px" class="UCML-Grid-Row UCML-Cell-Brd queryEdit" ></TD>' + '<TD class="datagrid-header-query-tb-cell" style="padding:0px; margin:0px;border:0px;" bgColor=#ffffff  width="15" noWrap><span style="CURSOR: hand"  class="queryEditImg"  ></span></TD></TR></TBODY></TABLE></td>',
    groupRowStr: '<tr style="{style}" class="datagrid-row datagrid-groupRow {cls}" rowType="groupRow" ><td tabIndex="0" hidefocus="on" style="{style}" colspan={colspan} ><div class="datagrid-cell-group" OID="{OID}" groupValue="{groupValue}">{groupItems}</div></td></tr>',
    groupItemStr: '',
    onRender: function () {
        //孙毅修改2012.11.28 个人列信息定义
        var elColumn = $("#" + this.id + "_ColumnSetup");
        if (elColumn.length > 0) {
            var conf = elColumn.val().split(";");
            for (var i = 0; i < conf.length; i++) {
                var objColumn = this.getColumn(conf[i]);
                if (objColumn) {
                    objColumn.display = false;
                }
            }
        }

        UCML.DataGrid.superclass.onRender.call(this);
        this.el.addClass("datagrid-wrap");

        this.setColumns(this.getColumns()); //设置列信息
        if (this.frozen) {
            this.setFrozenRender(this.totalWidth);
        }
        else {
            this.setRender(this.totalWidth, this.alignWidth);
        }

        if (this.border == true) {
            this.el.removeClass('datagrid-noborder');
        } else {
            this.el.addClass('datagrid-noborder');
        }

        for (var i = 0; i < this.columns.length; i++) {
            var col = this.columns[i];
            if (col.isCustomerControl == true) {
                if (!col.EditControl) {
                    var el = $("#" + col.controlID);
                    if (el.length > 0) {
                        var obj = {};
                        obj.ctype = el.attr("ctype") || "UCML.TextBox";
                        obj.dom = el[0];
                        obj.isRuleCheck = false;
                        obj.applet = this;
                        obj.BusinessObject = this.BusinessObject;
                        obj.fieldName = col.fieldName;
                        obj.dataTable = this.dataTable;

                        obj.rendeTo = this.el.find('.datagrid-edit-body');
                        col.EditControl = UCML.create(obj, obj.ctype);
                        col.EditControl.hide();
                    }
                }
            }
            if (this.haveQueryRow) {
                var el = $("#queryInput_" + col.fieldName + this.id);
                if (el.length > 0) {
                    var obj = {};

                    obj.ctype = el.attr("ctype");

                    obj.ctype = obj.ctype || "UCML.TextBox";


                    if (col.isCodeTable && col.codeTable) {
                        obj.ctype = "UCML.CheckBoxGroupBox";
                    }

                    obj.dom = el[0];

                    obj.applet = this;
                    obj.BusinessObject = this.BusinessObject;

                    col.queryEditControl = UCML.create(obj, obj.ctype);

                }

            }
        }
        this.setPager();


    }
    ,
    setPager: function () {
        if (this.showPager) {
            if ((typeof IsHTMLPage !== "undefined") && IsHTMLPage && (((typeof BPOIsJQMPage !== "undefined") && BPOIsJQMPage) || this.IsJQMPage)) {
                this.paginationCtype = "UCML.PaginationMobile";
            }
            this.setPagination();
        }
        else {
            this.el.children('.datagrid-pager').hide();
        }
    }
    ,
    bindHeader: function () {
        this.setHeaderResizable();

        var opts = this;
        this.headerTable.find("tr td .datagrid-header-check :checkbox").bind("click", function () {
            opts.checkRows(this.checked);
        });
    },

    setRender: function (totalWidth, fit) {
        var headerStr = "";
        var bodyStr = "";

        var columnRowStr = this.getRenderColumns(this.columns, true);

        if (this.noHeader === false) {
            headerStr = this.createHeader(this.headRowCount, this.columns, true, totalWidth, columnRowStr);
        }
        var bodyTpl = new UCML.Template(this.bodyStr);

        bodyStr = bodyTpl.apply({ columnRow: columnRowStr, rows: "", tstyle: "width:" + totalWidth + this.colUnit });

        var masterTpl = new UCML.Template(this.masterStr);
        var masterStr = masterTpl.apply({ headerStr: headerStr,
            bodyStr: bodyStr, vcid: this.id
        });
        this.el.append(masterStr);

        this.view = this.el.children('div.datagrid-view');
        this.body = this.view.children('.datagrid-body');
        this.bodyTable = this.body.children('table');

        this.header = this.view.children('.datagrid-header');

        this.headerTable = this.header.find(".datagrid-header-inner .datagrid-header-table");
        this.bindHeader();

        if (fit) {

            if (this.haveScroll) {
                this.body.css("overflow-x", "hidden").css("overflow-y", "auto");
            }
            else {
                this.body.css("overflow", "hidden");
            }
        }
        else {
            if (this.haveScroll) {
                this.body.css("overflow", "auto");
            }
            else {
                this.body.css("overflow", "hidden");
            }
        }

        this.setRenderTotalRow(columnRowStr);

        var opts = this;


        this.body.scroll(function () {

            opts.endEdit();

            opts.header.children(".datagrid-header-inner").scrollLeft(opts.body.scrollLeft());

            if (opts.totalRowBody) {
                opts.totalRowBody.scrollLeft(opts.body.scrollLeft());
            }

            opts.fireEvent('bodyscroll', opts.body);
        });


    },
    setRenderTotalRow: function (columnRowStr) {
        var havaTotalRow = this.getHaveTotalRow(); //是否有汇总行
        var totalRowPosition = this.getTotalRowPosition(); //汇总行位置 
        if (havaTotalRow) {
            if (this.totalRowBody) {
                this.totalRowBody.replaceWith("");
            }

            var totalRowBodyTpl = new UCML.Template(this.totalRowBodyStr);
            var totalStr = this.renderTotalRow(this.columns, true);
            var totalRowBodyStr = totalRowBodyTpl.apply({ columnRow: columnRowStr, tstyle: "width:" + this.totalWidth + this.colUnit, totalStr: totalStr });

            if (totalRowPosition == "top") {
                this.header.after(totalRowBodyStr);
            }
            else {
                this.body.after(totalRowBodyStr);
            }
        }

        this.totalRowBody = this.view.children('.datagrid-totalrowbody');
        this.totalRowTable = this.totalRowBody.children('.datagrid-body-table');
    }
    ,
    bindFrozenHeader: function () {
        this.setHeaderResizable();
        var opts = this;

        this.frozenHeaderTable.find("tr td .datagrid-header-check :checkbox").bind("click", function () {
            opts.checkRows(this.checked);
        });
    }
    , setFrozenRender: function (totalWidth) {
        var frozenTotalWidth = this.getTotalWidth(this.frozenColumns);
        var notTotalWidth = this.getTotalWidth(this.notfrozenColumns);

        frozenTotalWidth = frozenTotalWidth + (totalWidth - frozenTotalWidth - notTotalWidth);


        var frozenColumnRowStr = this.getRenderColumns(this.frozenColumns, true);
        var notfrozenColumnRowStr = this.getRenderColumns(this.notfrozenColumns, false);

        //设置固定列部分表头
        var frozenHeaderStr = "";
        var frozenBodyStr = "";

        if (this.noHeader === false) {
            frozenHeaderStr = this.createHeader(this.headRowCount, this.frozenColumns, true, frozenTotalWidth, frozenColumnRowStr);
        }
        var frozenBodyTpl = new UCML.Template(this.frozenBodyStr);
        frozenBodyStr = frozenBodyTpl.apply({ columnRow: frozenColumnRowStr, rows: "", tstyle: "width:" + frozenTotalWidth + this.colUnit });

        var headerStr = "";
        var bodyStr = "";

        if (this.noHeader === false) {
            headerStr = this.createHeader(this.headRowCount, this.notfrozenColumns, false, notTotalWidth, notfrozenColumnRowStr);
        }
        var bodyTpl = new UCML.Template(this.bodyStr);
        bodyStr = bodyTpl.apply({ columnRow: notfrozenColumnRowStr, rows: "", tstyle: "width:" + notTotalWidth + this.colUnit });

        var masterTpl = new UCML.Template(this.frozenMasterStr);
        var masterStr = masterTpl.apply({ frozenHeaderStr: frozenHeaderStr, headerStr: headerStr, frozenBodyStr: frozenBodyStr,
            bodyStr: bodyStr, vcid: this.id
        });
        this.el.append(masterStr);

        this.view = this.el.children('div.datagrid-view');

        this.header = this.view.find('.datagrid-view2 .datagrid-header');
        this.headerTable = this.header.find(".datagrid-header-inner .datagrid-header-table");
        this.body = this.view.find('.datagrid-view2 .datagrid-body');
        this.bodyTable = this.body.children('table');

        this.frozenHeader = this.view.find('.datagrid-view1 .datagrid-header');
        this.frozenHeaderTable = this.frozenHeader.find('.datagrid-header-inner .datagrid-header-table');
        this.frozenBody = this.view.find('.datagrid-view1 .datagrid-body');
        this.frozenBodyTable = this.frozenBody.find('.datagrid-body-inner table');

        this.setFrozenRenderTotalRow(frozenTotalWidth, notTotalWidth, frozenColumnRowStr, notfrozenColumnRowStr);

        this.bindFrozenHeader();

        var opts = this;
        opts.body.scroll(function () {

            opts.endEdit();
            opts.header.children(".datagrid-header-inner").scrollLeft(opts.body.scrollLeft());
            opts.frozenBody.scrollTop(opts.body.scrollTop());
            opts.frozenBody.children(".datagrid-body-inner").scrollTop(opts.body.scrollTop());

            if (opts.totalRowBody) {
                opts.totalRowBody.scrollLeft(opts.body.scrollLeft());
            }

            opts.fireEvent('bodyscroll', opts.body);
        });


        if (this.haveScroll) {
            this.body.css("overflow", "auto");
        }
        else {
            this.body.css("overflow", "hidden");
        }
    }
    ,
    setFrozenRenderTotalRow: function (frozenTotalWidth, notTotalWidth, frozenColumnRowStr, notfrozenColumnRowStr) {
        var havaTotalRow = this.getHaveTotalRow(); //是否有汇总行
        var totalRowPosition = this.getTotalRowPosition(); //汇总行位置 
        if (havaTotalRow) {
            if (this.totalRowBody) {
                this.totalRowBody.replaceWith("");
            }
            if (this.frozenTotalRowBody) {
                this.frozenTotalRowBody.replaceWith("");
            }

            var totalRowBodyTpl = new UCML.Template(this.totalRowBodyStr);

            var frozenTotalStr = this.renderTotalRow(this.frozenColumns, true);
            var totalStr = this.renderTotalRow(this.notfrozenColumns, false);

            var totalRowBodyStr = totalRowBodyTpl.apply({ columnRow: notfrozenColumnRowStr, tstyle: "width:" + notTotalWidth + this.colUnit, totalStr: totalStr });

            if (totalRowPosition == "top") {
                this.header.after(totalRowBodyStr);
            }
            else {
                this.body.after(totalRowBodyStr);
            }


            var frozenTotalRowBodyStr = totalRowBodyTpl.apply({ columnRow: frozenColumnRowStr, tstyle: "width:" + frozenTotalWidth + this.colUnit, totalStr: frozenTotalStr });

            if (totalRowPosition == "top") {
                this.frozenHeader.after(frozenTotalRowBodyStr);
            }
            else {
                this.frozenBody.after(frozenTotalRowBodyStr);
            }
        }

        this.frozenTotalRowBody = this.view.find('.datagrid-view1 .datagrid-totalrowbody');
        this.frozenTotalRowTable = this.frozenTotalRowBody.children('.datagrid-body-table');

        this.totalRowBody = this.view.find('.datagrid-view2 .datagrid-totalrowbody');
        this.totalRowTable = this.totalRowBody.children('.datagrid-body-table');
    }
    ,
    setHeaderResizable: function () {
        var opts = this;
        $('.datagrid-header .datagrid-header-cell', opts.el).UCML_resizable({
            handles: 'e',
            minWidth: 50,
            onStartResize: function (e) {
                $('.datagrid-resize-proxy', opts.el).css({
                    left: e.pageX - $(opts.el).offset().left - 1
                });

                $('.datagrid-resize-proxy', opts.el).css('display', 'block');
            },
            onResize: function (e) {

                $('.datagrid-resize-proxy', opts.el).css({
                    left: e.pageX - $(opts.el).offset().left - 1
                });

                return false;
            },
            onStopResize: function (e) {
                opts.fixColumnSize(this, e.data);
                $('.datagrid-view2 .datagrid-header', opts.el).scrollLeft($('.datagrid-view2 .datagrid-body', opts.el).scrollLeft());
                $('.datagrid-resize-proxy', opts.el).css('display', 'none');

                opts.setSize();
            }
        });
        $('.datagrid-view1 .datagrid-header .datagrid-header-cell', opts.el).UCML_resizable({
            onStopResize: function (e) {
                opts.fixColumnSize(this, e.data);
                $('.datagrid-view2 .datagrid-header', opts.el).scrollLeft($('.datagrid-view2 .datagrid-body', opts.el).scrollLeft());
                $('.datagrid-resize-proxy', opts.el).css('display', 'none');

                opts.setSize();
            }
        });
    }
    ,
    getRenderColumns: function (columns, frozen) {

        var columnsTpl = new UCML.Template(this.columnRowStr);

        var cellsStr = [];

        if (frozen == true && this.rowNumbers) {
            var headerNumberTpl = new UCML.Template(this.columnStr);
            cellsStr.add(headerNumberTpl.apply({
                style: "width:" + this.rowNumberWidth + this.colUnit,
                value: this.rowNumberText
            }));
        }

        if (frozen == true && this.showCheckBox) {

            var headerShowCheckBoxTpl = new UCML.Template(this.columnStr);
            cellsStr.add(headerShowCheckBoxTpl.apply({
                style: "width:" + this.showCheckBoxWidth + this.colUnit,
                value: ""
            }));
        }

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            var display = "";
            if (!col.display) {
                display = " display:none;";
            }
            var headerCellTpl = new UCML.Template(this.columnStr);

            cellsStr.add(headerCellTpl.apply({
                value: col.caption,
                fieldName: col.fieldName,
                style: "width:" + col.width + this.colUnit + ";" + display
            }));

        }
        return columnsTpl.apply({ style: "height:0px;",
            cells: cellsStr.join('')
        });
    }
    ,
    setHeader: function (value) {
        this.headerTable.html(value);

        var tds = this.headerTable.find("td,th");
        for (var i = 0; i < tds.length; i++) {
            var tdel = $(tds[i]);

            var tempcolspan = tdel.attr('colspan') || "1";
            var temprowspan = tdel.attr('rowspan') || "1";
            var tdstyle = "";

            var fieldName = tdel.attr('fieldName');
            var strAttr;
            var dwidth = "";
            if (parseInt(tempcolspan) == 1 && fieldName != "") {
                var col = this.getColumn(fieldName);
                if (col) {
                    tdstyle = "width:" + col.width + this.colUnit + ";";
                    strAttr = " fieldName='" + fieldName + "' ";
                    dwidth = col.width + this.colUnit;
                }
            }
            else {
                strAttr = "";
            }

            tdel.replaceWith("<td class='datagrid-header-cell'  " + strAttr + " style='" + tdstyle + "' rowspan='" + temprowspan + "' colspan='" + tempcolspan + "'><div class=\"datagrid-cell\" ><span>" + tdel.html() + "</span></div></td>");

        }
        this.header.width(this.body.width());
        this.body.width(this.body.width());

        var tableWidth = this.bodyTable.width();
        this.headerTable.width(tableWidth);

        this.setSize();
    },
    /**   
    * @method checkRows 
    * @description 选中复选框（全选）
    * @param {bool} checked
    */
    checkRows: function (checked) {

        var count = this.getRowCount();
        for (var i = 0; i < count; i++) {
            if (this.showCheckBox) {
                var rowIndex = i;
                var row;
                if (this.frozen) {
                    row = this.getFrozenRow(rowIndex);
                }
                else {
                    row = this.getRow(rowIndex);
                }
                if (row) {
                    if (checked) {
                        this.onRowSelect(row);
                    }
                    else {
                        this.unRowSelect(row);
                    }
                    this.setFocusRow(row);
                    this.checkChange(row, checked);
                }
            }
        }
    }
    ,
    afterRender: function () {
        UCML.DataGrid.superclass.afterRender.call(this);

        if ((typeof BPOIsJQMPage !== "undefined") && BPOIsJQMPage) {
            var theme = BPOPageTheme || "a";
            this.header.addClass("datagrid-header-" + theme);
            if (this.pagination) {
                this.pagination.el.addClass("toolbar-" + theme);
            }
        }
        else if (this.IsJQMPage) {
            var theme = this.JQMPageTheme || "a";
            this.header.addClass("datagrid-header-" + theme);
            if (this.pagination) {
                this.pagination.el.addClass("toolbar-" + theme);
            }
        }
    }
    ,
    open: function () {
        UCML.DataGrid.superclass.open.call(this);
    }
    ,
    setColumns: function (value) {
        UCML.DataGrid.superclass.setColumns.call(this, value);
        var totalWidth = this.getTotalWidth(this.columns);
        if (this.alignWidth) {
            var dWdith = 100;
            this.rowNumberWidth = this.rowNumberWidth || 5;
            this.showCheckBoxWidth = this.showCheckBoxWidth || 5;


            if (this.rowNumbers) {
                dWdith -= this.rowNumberWidth;
            }
            if (this.showCheckBox) {
                dWdith -= this.showCheckBoxWidth;
            }
            for (var i = 0; i < this.columns.length; i++) {
                this.columns[i].width = this.columns[i].width / totalWidth * dWdith;
            }
            this.frozen = false;
            this.totalWidth = 100;
            this.colUnit = "%";
        }
        else {
            this.rowNumberWidth = this.rowNumberWidth || 40;
            this.showCheckBoxWidth = this.showCheckBoxWidth || 40;
            if (this.rowNumbers) {
                totalWidth += this.rowNumberWidth;
            }
            if (this.showCheckBox) {
                totalWidth += this.showCheckBoxWidth;
            }
            this.totalWidth = totalWidth;

            this.setFrozenColumns(); //设置固定列
            var frozen = this.showCheckBox || this.rowNumbers || (this.frozenColumns && this.frozenColumns.length > 0);
            this.frozen = frozen;
        }
    },
    getTotalWidth: function (columns) {
        var totalWidth = 0;
        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            if (col.display == true) {
                totalWidth += parseFloat(col.width);
            }
        }
        return totalWidth;
    },
    onQueryMenuReady: function () {

        var queryMenu = new UCML.Menu();
        queryMenu.addMenuItem("cmd_NOTSET", UCML.Languge.DBNOTSET);
        queryMenu.addSeparator();
        queryMenu.addMenuItem("cmd_GreaterEQU", UCML.Languge.DBGreaterEQU);
        queryMenu.addMenuItem("cmd_Greater", UCML.Languge.DBGreater);
        queryMenu.addMenuItem("cmd_EQU", UCML.Languge.DBEQU, "icon-ok");
        queryMenu.addMenuItem("cmd_LessEQU", UCML.Languge.DBLessEQU);
        queryMenu.addMenuItem("cmd_Less", UCML.Languge.DBLess);
        queryMenu.addMenuItem("cmd_NOTEQU", UCML.Languge.DBNOTEQU);
        queryMenu.addMenuItem("cmd_LIKE", UCML.Languge.DBLIKE);
        queryMenu.addMenuItem("cmd_BETWEEN", UCML.Languge.DBBETWEEN);
        queryMenu.addSeparator();
        queryMenu.addMenuItem("cmd_IS", UCML.Languge.DBIS);
        queryMenu.addMenuItem("cmd_ISNOT", UCML.Languge.DBISNOT);
        queryMenu.addMenuItem("cmd_EQUEmpty", UCML.Languge.DBEQUEmpty);
        queryMenu.addMenuItem("cmd_NOTEQUEmpty", UCML.Languge.DBNOTEQUEmpty);
        queryMenu.addSeparator();
        queryMenu.addMenuItem("cmd_RunQuery", UCML.Languge.DBRunQuery);
        queryMenu.addMenuItem("cmd_ClearThisQuery", UCML.Languge.DBClearThisQuery);
        queryMenu.addMenuItem("cmd_ClearQuery", UCML.Languge.DBClearQuery);
        this.queryMenu = queryMenu;


        for (var i = 0; i < this.columns.length; i++) {
            if (this.columns[i].display == true) {
                this.columns[i].queryCmd = "LIKE";
                this.columns[i].cmd = "cmd_LIKE";
            }
        }
    },
    onMenuReady: function () {
        var menu = new UCML.Menu();
        menu.addMenuItem("cmd_LoadFirstPage", UCML.Languge.DBLoadFirstPage);
        menu.addMenuItem("cmd_LoadPrevPage", UCML.Languge.DBLoadPrevPage);
        menu.addMenuItem("cmd_LoadMore", UCML.Languge.DBLoadMore);
        menu.addMenuItem("cmd_LoadLastPage", UCML.Languge.DBLoadLastPage);
        if (this.enabledEdit) {
            menu.addSeparator();
            menu.addMenuItem("cmd_InsertRecord", UCML.Languge.DBInsertRecord);
            menu.addMenuItem("cmd_DeleteRecord", UCML.Languge.DBDeleteRecord);

            menu.addSeparator();
            menu.addMenuItem("cmd_Submit", UCML.Languge.DBSubmit);
            menu.addMenuItem("cmd_Cancel", UCML.Languge.DBCancel);
        }
        menu.addSeparator();
        menu.addMenuItem("cmd_OutExcel", UCML.Languge.DBOutExcel);
        menu.addMenuItem("cmd_AllOutExcel", UCML.Languge.DBAllOutExcel);
        menu.addMenuItem("cmd_Refresh", UCML.Languge.DBRefresh);
        menu.addMenuItem("cmd_ConfigColumns", UCML.Languge.DBConfigColumns);
        this.menu = menu;
    },
    onQueryMenuItemClick: function (menuId, menuItem) {
        var queryMenu = this.queryMenu;
        var column = this.getColumn(queryMenu.fieldName);
        if (menuId == "cmd_NOTSET") {
            column.queryCmd = "";
            queryMenu.setCheckedMenuItem(menuId);
            column.cmd = menuId;
            this.dataTable.SetCondiList("", "", "");
        }
        else if (menuId == "cmd_GreaterEQU") {
            column.queryCmd = ">=";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_Greater") {
            column.queryCmd = ">";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_EQU") {
            column.queryCmd = "=";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_NOTEQU") {
            column.queryCmd = "<>";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_LessEQU") {
            column.queryCmd = "<=";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_Less") {
            column.queryCmd = "<";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_LIKE") {
            column.queryCmd = "LIKE";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_IS") {
            column.queryCmd = "IS";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        } else if (menuId == "cmd_ISNOT") {
            column.queryCmd = "IS NOT";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        } else if (menuId == "cmd_EQUEmpty") {
            column.queryCmd = "Empty";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        } else if (menuId == "cmd_NOTEQUEmpty") {
            column.queryCmd = "Not Empty";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_BETWEEN") {
            column.queryCmd = "BETWEEN";
            column.cmd = menuId;
            queryMenu.setCheckedMenuItem(menuId);
        }
        else if (menuId == "cmd_RunQuery") {
            this.onRunQuery();
        }
        else if (menuId == "cmd_ClearThisQuery") {
            this.setQueryEditValue(queryMenu.fieldName, "");
            this.onRunQuery();
        }
        else if (menuId == "cmd_ClearQuery") {
            for (var i = 0; i < this.columns.length; i++) {
                if (this.columns[i].display == true) {
                    this.setQueryEditValue(this.columns[i].fieldName, "");
                }
            }
            this.onRunQuery();
        }
    },
    //添加列
    addColumn: function (fieldName, caption, display, allowModify, width) {

        var objColumn = new Object();
        objColumn.fieldName = fieldName || "column_" + this.columns.length++;
        objColumn.caption = caption || "列_" + this.columns.length++;
        objColumn.display = display || true;
        objColumn.allowModify = allowModify || true;
        objColumn.width = width || 100;
        objColumn.isFixColumnValue = false;
        objColumn.fixColumnValue = "";
        objColumn.isCustomerControl = true;
        objColumn.customerControl = "";
        objColumn.editContrl = "";
        objColumn.EditType = "UCML.TextBox";
        objColumn.mutiValueCol = false;
        this.columns.add(objColumn);
    }
    , rebuildTotalRow: function () {
        if (!this.frozen) {
            var columnRowStr = this.getRenderColumns(this.columns, true);
            this.setRenderTotalRow(columnRowStr);
        }
        else {
            var frozenColumnRowStr = this.getRenderColumns(this.frozenColumns, true);
            var notfrozenColumnRowStr = this.getRenderColumns(this.notfrozenColumns, false);

            var frozenTotalWidth = this.getTotalWidth(this.frozenColumns);
            var notTotalWidth = this.getTotalWidth(this.notfrozenColumns);

            this.setFrozenRenderTotalRow(frozenTotalWidth, notTotalWidth, frozenColumnRowStr, notfrozenColumnRowStr);
        }
        this.updateTotalRow();
    }
    ,
    rebuildHeader: function () {
        if (!this.frozen) {
            var columnRowStr = this.getRenderColumns(this.columns, true);
            var headerStr = "";
            if (this.noHeader === false) {
                headerStr = this.createHeader(this.headRowCount, this.columns, true, this.totalWidth, columnRowStr);
            }

            this.headerTable.replaceWith(headerStr);

            this.headerTable = this.header.find(".datagrid-header-inner .datagrid-header-table");

            this.bindHeader();
        }
        else {
            var frozenTotalWidth = this.getTotalWidth(this.frozenColumns);
            var notTotalWidth = this.getTotalWidth(this.notfrozenColumns);

            frozenTotalWidth = frozenTotalWidth + (this.totalWidth - frozenTotalWidth - notTotalWidth);

            var frozenColumnRowStr = this.getRenderColumns(this.frozenColumns, true);
            var notfrozenColumnRowStr = this.getRenderColumns(this.notfrozenColumns, false);

            var headerStr = "";

            //设置固定列部分表头
            var frozenHeaderStr = "";

            if (this.noHeader === false) {
                headerStr = this.createHeader(this.headRowCount, this.notfrozenColumns, false, notTotalWidth, notfrozenColumnRowStr);
                frozenHeaderStr = this.createHeader(this.headRowCount, this.frozenColumns, true, frozenTotalWidth, frozenColumnRowStr);
            }

            this.headerTable.replaceWith(headerStr);
            this.frozenHeaderTable.replaceWith(frozenHeaderStr);

            this.headerTable = this.header.find(".datagrid-header-inner .datagrid-header-table");

            this.frozenHeaderTable = this.frozenHeader.find('.datagrid-header-inner .datagrid-header-table');

            this.bindFrozenHeader();
        }

    },
    rebuildDataBody: function () {
        if (this.frozen) {
            var frozenColumnRowStr = this.getRenderColumns(this.frozenColumns, true);
            var notfrozenColumnRowStr = this.getRenderColumns(this.notfrozenColumns, false);

            var frozenTotalWidth = this.getTotalWidth(this.frozenColumns);
            var notTotalWidth = this.getTotalWidth(this.notfrozenColumns);

            frozenTotalWidth = frozenTotalWidth + (this.totalWidth - frozenTotalWidth - notTotalWidth);

            this.bodyTable.empty();
            this.frozenBodyTable.empty();

            var frozenBodyStr = this.renderRows(this.frozenColumns, true);
            var bodyStr = this.renderRows(this.notfrozenColumns, false);

            //开始渲染内容
            this.frozenBodyTable.append(frozenColumnRowStr).width(frozenTotalWidth + this.colUnit);

            this.bodyTable.append(notfrozenColumnRowStr).width(notTotalWidth + this.colUnit);
        }
        else {
            this.bodyTable.empty();
            var columnRowStr = this.getRenderColumns(this.columns, true);
            var bodyStr = this.renderRows(this.columns, true);

            this.bodyTable.append(columnRowStr).width(this.totalWidth + this.colUnit);
        }
        this.loadData();
    }
    ,
    rebuildQueryRow: function () {
        for (var i = 0; i < this.columns.length; i++) {
            var col = this.columns[i];
            if (this.haveQueryRow) {
                var el = $("#queryInput_" + col.fieldName + this.id);
                if (el.length > 0) {
                    var obj = {};

                    obj.ctype = el.attr("ctype");

                    obj.ctype = obj.ctype || "UCML.TextBox";


                    if (col.isCodeTable && col.codeTable) {
                        obj.ctype = "UCML.CheckBoxGroupBox";
                    }

                    obj.dom = el[0];

                    obj.applet = this;
                    obj.BusinessObject = this.BusinessObject;

                    col.queryEditControl = UCML.create(obj, obj.ctype);
                }

            }
        }
    },
    /**   
    * @method rebuildGrid 
    * @description 重新渲染grid
    */
    rebuildGrid: function () {
        this.setColumns(this.getColumns()); //设置列信息
        this.rebuildHeader();
        this.rebuildQueryRow();
        this.setPagination();
        this.rebuildDataBody();
        this.rebuildTotalRow();
        this.setSize();
    }
    ,
    onRunQuery: function () {
        var fieldList = "";
        var valueList = "";
        var condiIndentList = "";
        var flag = 0;
        var dataTable = this.dataTable;
        var bpo = this.BusinessObject;

        var SQLCondi = "";

        var flagCodeTable = false;

        for (var i = 0; i < this.columns.length; i++) {
            var column = this.columns[i];
            var fieldName = column.fieldName;

            if (column.display == true) {

                var value = "";
                if (column && column.queryEditControl) {
                    value = column.queryEditControl.getValue();
                }

                var cmd = column.queryCmd || "";
                var fieldKind = column.fieldKind;


                if (value != "" && cmd != "") {

                    if (fieldKind == 0) {//实字段
                        fieldName = dataTable.TableName + "." + fieldName;
                    }
                    else if (fieldKind == 2) {//连接虚字段
                        fieldName = "al" + column.foreignKeyField + "." + column.lookupResultField;
                    }
                    else if (fieldKind == 10) {//多值字段
                        fieldName = "al" + column.foreignKeyField + "." + fieldName;
                    }
                    else if (fieldKind == 5 && column.QueryRefColumn) {//SQL语句列

                        fieldName = column.QueryRefColumn;
                    }
                    else {
                        continue;
                    }

                    if (column.isCodeTable && column.codeTable) {

                        if (flagCodeTable) {
                            SQLCondi += " and ";
                        }
                        SQLCondi += codeTableWhereByValue(fieldName, value);

                        flagCodeTable = true;
                    }
                    else {
                        if (cmd == "BETWEEN") {
                            var v = value.toUpperCase();
                            var r = v.match('AND');
                            if (r == null) {
                                alert('请输入"value1 and value2"格式条件');
                                return;
                            }
                        }

                        fieldList += fieldName + ";";

                        valueList = valueList + value + ";";
                        condiIndentList = condiIndentList + cmd + ";";
                    }
                    flag = 1;
                }
                else if (cmd != "") {
                    if (fieldKind == 0) {//实字段
                        fieldName = dataTable.TableName + "." + fieldName;
                    }
                    else if (fieldKind == 2) {//连接虚字段
                        fieldName = "al" + column.foreignKeyField + "." + column.lookupResultField;
                    }
                    else if (fieldKind == 10) {//多值字段
                        fieldName = "al" + column.foreignKeyField + "." + fieldName;
                    }
                    else if (fieldKind == 5 && column.QueryRefColumn) {//SQL语句列

                        fieldName = column.QueryRefColumn;
                    }

                    if (cmd == "IS" || cmd == "IS NOT") {
                        if (flagCodeTable) {
                            SQLCondi += " and ";
                        }
                        SQLCondi += fieldName + " " + cmd + " NULL";
                        flagCodeTable = true;
                        continue;
                    } else if (cmd == "Empty" || cmd == "Not Empty") {
                        if (flagCodeTable) {
                            SQLCondi += " and ";
                        }
                        if (cmd == "Empty")
                            SQLCondi += fieldName + " = ''";
                        else
                            SQLCondi += fieldName + " <> ''";
                        flagCodeTable = true;
                        continue;
                    }
                }
            }

        }

        dataTable.setSQLCondi(SQLCondi);


        dataTable.SetCondiList(fieldList, valueList, condiIndentList);
        this.dataTable.getData(0, this.dataTable.getDefaultPageCount());

        function codeTableWhereByValue(fieldName, value) {
            value = value.replace(/\'/g, "");
            var values = value.split(";");
            var logicConnect = " or ";
            var sql = "";
            if (values.length > 0) {
                sql = " ( ";
                for (var i = 0; i < values.length; i++) {

                    if (i > 0) {
                        sql += " " + logicConnect + " ";
                    }
                    sql += fieldName + " = '" + values[i] + "'";
                }
                sql += " )";

            }
            return sql;
        }
    },
    setQueryEditValue: function (fieldName, value) {
        var col = this.getColumn(fieldName);
        if (col && col.queryEditControl) {
            col.queryEditControl.setValue(value);
        }
    },
    mergerData: function (keyFieldName, mergerFields) {
        if (keyFieldName == null || mergerFields == null) {
            return;
        }
        var fieldIndex = mergerFields.split(";");

        if (fieldIndex == null || fieldIndex.length == 0) {
            return;
        }

        var col = this.getColumn(keyFieldName);
        var keyIndex = -1;
        var tb;
        if (this.frozen) {
            if (col.fixedColumn) {
                keyIndex = this.getFrozenColumnIndex(keyFieldName);
                tb = this.frozenBodyTable[0];

                if (!this.alignWidth) {
                    if (this.rowNumbers) {
                        keyIndex++;
                    }
                    if (this.showCheckBox) {
                        keyIndex++;
                    }
                }
            } else {
                keyIndex = this.getNotFrozenColumnIndex(keyFieldName);
                tb = this.bodyTable[0];

                if (this.alignWidth) {
                    if (this.rowNumbers) {
                        keyIndex++;
                    }
                    if (this.showCheckBox) {
                        keyIndex++;
                    }
                }
            }
        }
        else {
            keyIndex = this.getColumnIndex(keyFieldName);
            tb = this.bodyTable[0];

            if (this.alignWidth) {
                if (this.rowNumbers) {
                    keyIndex++;
                }
                if (this.showCheckBox) {
                    keyIndex++;
                }
            }

        }


        var fieldList = [];

        for (var m = 0; m < fieldIndex.length; m++) {
            var fieldName = fieldIndex[m];
            var fieldCol = this.getColumn(fieldName);
            var zi = -1;
            var t;
            if (this.frozen) {
                if (fieldCol.fixedColumn) {
                    zi = this.getFrozenColumnIndex(fieldName);
                    t = this.frozenBodyTable[0];

                    if (!this.alignWidth) {
                        if (this.rowNumbers) {
                            zi++;
                        }
                        if (this.showCheckBox) {
                            zi++;
                        }
                    }

                } else {
                    zi = this.getNotFrozenColumnIndex(fieldName);
                    t = this.bodyTable[0];

                    if (this.alignWidth) {
                        if (this.rowNumbers) {
                            zi++;
                        }
                        if (this.showCheckBox) {
                            zi++;
                        }
                    }
                }
            }
            else {
                zi = this.getColumnIndex(fieldName);
                t = this.bodyTable[0];

                if (this.alignWidth) {
                    if (this.rowNumbers) {
                        zi++;
                    }
                    if (this.showCheckBox) {
                        zi++;
                    }
                }
            }


            if (zi >= 0 && t != null) {
                fieldList.add({ tb: t, index: zi });
            }
        }

        if (keyIndex >= 0 && tb != null && fieldList.length > 0) {
            for (var i = 1; i < tb.rows.length; i++) {
                var n = 1;
                for (var j = i + 1; j < tb.rows.length; j++) {
                    var deleteCellCount = 0; //当前记录已经合并过的单元格数
                    if ((tb.rows[i].cells.length >= keyIndex && tb.rows[j].cells.length >= keyIndex) && tb.rows[i].cells[keyIndex].innerHTML == tb.rows[j].cells[keyIndex].innerHTML) {
                        n = n + 1;
                        for (var m = 0; m < fieldList.length; m++) {
                            var field = fieldList[m];
                            field.tb.rows[j].deleteCell(field.index - deleteCellCount);
                            deleteCellCount++;
                            field.tb.rows[i].cells[field.index].rowSpan = n;
                        }
                    }
                    else {
                        break;
                    }
                }
                i = i + n - 1;
            }
        }
    }
    ,
    //合并表头 startRow开始行,endRow结束行,startCol开始列,endCol结束列,text合并后文本
    mergeHeadCells: function (startRow, endRow, startCol, endCol, text) {
        var table = this.headerTable[0];
        this.mergeTableCells(table, startRow, endRow, startCol, endCol, text);
    },
    mergeFrozeHeadCells: function (startRow, endRow, startCol, endCol, text) {
        var table = this.frozenHeaderTable[0];
        this.mergeTableCells(table, startRow, endRow, startCol, endCol, text);
    },
    mergeBodyTableCells: function (startRow, endRow, startCol, endCol, text) {
        var table = this.bodyTable[0];
        this.mergeTableCells(table, startRow, endRow, startCol, endCol, text);
    },
    mergeFrozenBodyCells: function (startRow, endRow, startCol, endCol, text) {
        var table = this.frozenBodyTable[0];
        this.mergeTableCells(table, startRow, endRow, startCol, endCol, text);
    },
    mergeTableCells: function (table, startRow, endRow, startCol, endCol, text) {
        startRow++;
        endRow++;

        var colSpan = endCol - startCol + 1;
        var rowSpan = endRow - startRow + 1;
        var width = 0;
        for (var i = startCol; i <= endCol; i++) {
            var cell = $(table.rows[startRow].cells[i]);
            width = width + cell.outerWidth();
        }

        for (var n = startRow; n <= endRow; n++) {
            for (var i = startCol; i < endCol; i++) {

                table.rows[n].deleteCell(startCol + 1);
            }
        }
        for (var n = startRow + 1; n <= endRow; n++) {
            table.rows[n].deleteCell(startCol);
        }

        table.rows[startRow].cells[startCol].rowSpan = rowSpan;
        table.rows[startRow].cells[startCol].colSpan = colSpan;

        var cell = table.rows[startRow].cells[startCol];
        var cellEl = $(cell);

        /*   var headerCellTpl = new UCML.Template(this.headerCellStr);
        text = headerCellTpl.apply({
        value: text,
        fieldName: cellEl.attr("fieldName"),
        style: "width:" + width + "px;"
        });
        cellEl = cellEl.replaceWith(text);
        cellEl.attr("rowSpan", rowSpan);
        cellEl.attr("colSpan", colSpan);
        */

        cellEl.find(".datagrid-cell").html(text);
        var c = cellEl.outerWidth() - cellEl.width();
        //   cellEl.width(width - c);
        //   table.rows[startRow].cells[startCol].innerHTML = text;
        //   $(table.rows[startRow].cells[startCol]).width(width);
    },
    clearAllBCLink: function () {
        for (var i = 0; i < this.columns.length; i++) {
            //this.columns[i].objBCLinkColl = null;
            var fieldName = this.columns[i].fieldName;
            this.setShowBCLink(fieldName, false);
        }
    },
    //设置是否显示bclink
    setShowBCLink: function (fieldName, value) {
        var objColumn = this.getColumn(fieldName);
        if (objColumn) {
            objColumn.showBCLink = value;
        }
    }
    ,
    onLoadPrevPage: function () {
        if (this.dataTable) {
            this.dataTable.LoadPrevPage();
        }
    },
    //下一页
    onLoadMoreData: function () {
        if (this.dataTable) {
            this.dataTable.LoadMoreData();
        }
    },
    //首页
    onLoadFirstPage: function () {
        if (this.dataTable) {
            this.dataTable.LoadFirstPage();
        }
    }
    ,
    //末页
    onLoadLastPage: function () {
        if (this.dataTable) {
            this.dataTable.LoadLastPage();
        }
    },
    onGotoPage: function (pageIndex) {
        if (this.dataTable) {
            this.dataTable.GotoPage(pageIndex);
        }
    }
    , ExportToExcel: function () {

    }
});

UCML.DataGrid.prototype.init = function () {
    UCML.DataGrid.superclass.init.call(this);
}

//设置固定列
UCML.DataGrid.prototype.setFrozenColumns = function (columns) {
    columns = columns || this.columns;
    this.frozenColumns = [];
    this.notfrozenColumns = [];
    for (var i = 0; i < columns.length; i++) {
        if (columns[i].fixedColumn) {
            this.frozenColumns[this.frozenColumns.length] = columns[i];
        }
        else {
            this.notfrozenColumns[this.notfrozenColumns.length] = columns[i];
        }
    }
}

UCML.DataGrid.prototype.getFrozenColumns = function () {
    return this.frozenColumns;
}

UCML.DataGrid.prototype.getFrozenColumnIndex = function (fieldName) {
    if (fieldName == null || this.frozenColumns == null) {
        return -1;
    }
    for (var i = 0; i < this.frozenColumns.length; i++) {
        if (this.frozenColumns[i].fieldName == fieldName) {
            return i;
        }
    }
    return -1;
}

UCML.DataGrid.prototype.getNotFrozenColumnIndex = function (fieldName) {
    if (fieldName == null || this.notfrozenColumns == null) {
        return -1;
    }
    for (var i = 0; i < this.notfrozenColumns.length; i++) {
        if (this.notfrozenColumns[i].fieldName == fieldName) {
            return i;
        }
    }
    return -1;
}

UCML.DataGrid.prototype.getColumnIndex = function (fieldName) {
    if (fieldName == null || this.columns == null) {
        return -1;
    }
    for (var i = 0; i < this.columns.length; i++) {
        if (this.columns[i].fieldName == fieldName) {
            return i;
        }
    }
    return -1;
}


UCML.DataGrid.prototype.getNotfrozenColumns = function () {
    return this.notfrozenColumns;
}

UCML.DataGrid.prototype.setPagination = function () {
    if (!this.showPager) {
        return;
    }

    var pager = this.el.children('.datagrid-pager');
    if (pager.length == 0) {
        return;
    }

    if (!this.pagination) {
        if (this.paginationCtype) {//自定义类型
            this.pagination = new UCML.create({ dom: pager[0], dataTable: this.dataTable, applet: this, havePage: this.havePage }, this.paginationCtype);
        }
        else {
            this.pagination = new UCML.Pagination({ dom: pager[0], dataTable: this.dataTable, applet: this, havePage: this.havePage });
        }
    }
    else {
        this.pagination.updateMsg();
    }
}

//创建比啊头 私有
UCML.DataGrid.prototype.createHeader = function (headRowCount, columns, frozen, totalWidth, columnRowStr) {
    var headerTpl = new UCML.Template(frozen == true ? this.frozenHeaderStr : this.headerStr);
    var rowsStr = [];
    for (var i = 0; i < headRowCount; i++) {
        var headerRowTpl = new UCML.Template(this.headerRowStr);
        var cellsStr = [];

        if (i == 0) {
            if(frozen == true && this.rowNumbers && this.showCheckBox) {
                var headerNumberTpl = new UCML.Template(this.headerNumberStr);
                var headerShowCheckBoxTpl = new UCML.Template(this.headerShowCheckBoxStr);
                if(this.rowNumberBeforeCheckbox) {
                    cellsStr.add(headerNumberTpl.apply({
                        value: this.rowNumberText,
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                    cellsStr.add(headerShowCheckBoxTpl.apply({
                        value: "",
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                }else {
                    cellsStr.add(headerShowCheckBoxTpl.apply({
                        value: "",
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                    cellsStr.add(headerNumberTpl.apply({
                        value: this.rowNumberText,
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                }
            }else {
                if (frozen == true && this.rowNumbers) {
                    var headerNumberTpl = new UCML.Template(this.headerNumberStr);
                    cellsStr.add(headerNumberTpl.apply({
                        //    style: "width:" + this.rowNumberWidth + this.colUnit,
                        value: this.rowNumberText,
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                }

                if (frozen == true && this.showCheckBox) {

                    var headerShowCheckBoxTpl = new UCML.Template(this.headerShowCheckBoxStr);
                    cellsStr.add(headerShowCheckBoxTpl.apply({
                        //    style: "width:" + this.showCheckBoxWidth + this.colUnit,
                        value: "",
                        rowspan: headRowCount + (this.haveQueryRow ? 1 : 0)
                    }));
                }
            }
        }

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            var display = "";
            if (!col.display) {
                display = " display:none;";
            }

            var title = "";
            if(col.inputTip && col.inputTip != "")
            {
                title = col.inputTip;
            }

            var headerCellTpl = new UCML.Template(this.headerCellStr);

            var caption = col.caption;
            if (col.headFormatter) {
                caption = col.headFormatter.call(this, { caption: caption, fieldName: col.fieldName, column: col }, col);
            }

            cellsStr.add(headerCellTpl.apply({
                value: caption,
                fieldName: col.fieldName,
                style: display,
                title: title
            }));

        }
        rowsStr[i] = headerRowTpl.apply({
            style: "height:" + this.headRowHeight + "px;",
            cells: cellsStr.join('')
        });
    }

    if (this.haveQueryRow) {
        var headerRowTpl = new UCML.Template(this.headerRowStr);
        var cellsStr = [];

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            var display = "";
            if (!col.display) {
                display = " display:none;";
            }

            var queryCellTpl = new UCML.Template(this.queryCellStr);
            cellsStr.add(queryCellTpl.apply({
                value: col.caption,
                fieldName: col.fieldName,
                BCName:this.dataTable.BCName,               
                VCName:this.id,
                ctype: col.EditType,
                style: display,
                isCodeTable: col.isCodeTable,
                codeTable: col.codeTable
            }));

        }
        rowsStr[rowsStr.length] = headerRowTpl.apply({
            style: "height:" + this.headRowHeight + "px;",
            cells: cellsStr.join('')
        });
    }
    return headerTpl.apply({ columnRow: columnRowStr, rows: rowsStr.join(''), tstyle: "width:" + totalWidth + this.colUnit });
}

UCML.DataGrid.prototype.setFrozenBodySize = function (param) {
    var beginDate = new Date();
    var width = this.el.width();
    var height = this.el.height();
    
    var el = this.el;
    var view = el.children('div.datagrid-view');
    var view1 = view.children("div.datagrid-view1");
    var view2 = view.children("div.datagrid-view2");

    var view1_header = view1.children("div.datagrid-header");
    var view2_header = view2.children("div.datagrid-header");
    var table1 = view1_header.find("table");
    var table2 = view2_header.find("table");

    if (this.alignWidth) {
        //   view1.width(table1.css("width"));
        //   view2.width(table2.css("width"));
        //  view2.css("position", null);
        view2.find('.datagrid-header .datagrid-header-inner').css("padding", "0");
        //    view2.width("100%");
    }
    else {
        view1.width(view1.find("table").width());
        view2.width(width - view1.outerWidth());
        view1.children("div.datagrid-header,div.datagrid-body,div.datagrid-totalrowbody").width(view1.width());
        view2.children("div.datagrid-header,div.datagrid-body,div.datagrid-totalrowbody").width(view2.width());
        view2.css("left", view1.outerWidth());
    }
    var hh;

    view1_header.css("height", 'auto');
    view2_header.css("height", 'auto');
    table1.css("height", 'auto');
    table2.css("height", 'auto');
    hh = Math.max(table1.height(), table2.height());
    //    table1.height(hh);
    //    table2.height(hh);
    if ($.boxModel == true) {
        view1_header.height(hh - (view1_header.outerHeight() - view1_header.height()));
        view2_header.height(hh - (view2_header.outerHeight() - view2_header.height()));
    } else {
        view1_header.height(hh - (view1_header.outerHeight() - view1_header.height()));
        view2_header.height(hh - (view2_header.outerHeight() - view2_header.height()));
    }
    var body = el.find("div.datagrid-body");

    if (this.height == 'auto') {
        this.updataBody();
    } else {

        var bodyHeight = height - view2.children("div.datagrid-header").outerHeight(true) -
        el.children(".datagrid-pager:visible").outerHeight(true) - this.totalRowBody.outerHeight(true);
        body.height(bodyHeight);
        view.height(view2.height());

        this.frozenHeaderTable.height(this.headerTable.outerHeight());
        this.frozenBodyTable.height(this.bodyTable.outerHeight(true));

        var innerHeight = (body[1].offsetHeight - body[1].clientHeight);
        if (body[1].clientHeight > 0 && innerHeight > 0) {
            bodyHeight = bodyHeight - innerHeight;
            view1.find(".datagrid-body .datagrid-body-inner").height(bodyHeight);
        }
        else {
            view1.find(".datagrid-body .datagrid-body-inner").css("height", 'auto');
        }
    }

    var innerWidth = (body[1].offsetWidth - body[1].clientWidth);
    if (body[1].clientWidth > 0 && innerWidth > 0) {
        var view2Width = view2.width();
        view2_header.children(".datagrid-header-inner").width(view2Width - innerWidth);
        this.totalRowBody.width(view2Width - innerWidth);
    }
    else {
        view2_header.children(".datagrid-header-inner").css("width", "100%");
    }

    //  alert(new Date() - beginDate);
}

UCML.DataGrid.prototype.setSize = function (param, a) {
    UCML.DataGrid.superclass.setSize.call(this, param);
    
    if (this.frozen) {
        this.setFrozenBodySize();
    } else {
        var el = this.el;

        var width = this.el.width();
        var height = this.el.height();

        var view = el.children('div.datagrid-view');
        var body = this.body;
        var header = this.header;


        if (this.alignWidth) {
            view.width(width);
            body.width(width);
            header.width(width);
            this.toolbar.width(width);
            this.totalRowBody.width(width);
            if (this.pagination) {
                this.pagination.setSize({ width: width });
            }
            el.css("width", null);
        }
        else {

            body.width(width);
            header.width(width);
            this.toolbar.width(width);
            this.totalRowBody.width(width);
        }

        if (this.height == 'auto') {
            this.updataBody();

        }
        else {
            var viewHeight = height - el.children(".datagrid-pager:visible").outerHeight(true);
            view.height(viewHeight);

            var bodyHeight = viewHeight - header.outerHeight(true) - this.totalRowBody.outerHeight(true);
            body.height(bodyHeight);

            //  this.bodyTable.height(bodyHeight);
        }

        var innerWidth = (body[0].offsetWidth - body[0].clientWidth);
        if (body[0].clientWidth > 0 && innerWidth > 0) {
            header.children(".datagrid-header-inner").width(width - innerWidth);
            this.totalRowBody.width(width - innerWidth);
        }
        else {
            header.children(".datagrid-header-inner").css("width", "100%");
        }
    }
}

UCML.DataGrid.prototype.updataBody = function () {
    if (this.haveMergerTable) {
        this.mergerData(this.keyFieldName, this.mergerFields);
    }
    if (this.height == 'auto') {
        if (this.frozen) {

            var view = this.el.children('div.datagrid-view');
            var view1 = view.children("div.datagrid-view1");
            var view2 = view.children("div.datagrid-view2");
            var body = this.el.find("div.datagrid-body");
            var bodyHeight = $('.datagrid-body table', view2).outerHeight();

            this.frozenHeaderTable.height(this.headerTable.outerHeight());
            this.frozenBodyTable.height(bodyHeight);

            var innerHeight = (body[1].offsetHeight - body[1].clientHeight);

            if (body[1].clientHeight > 0 && innerHeight > 0) {
                bodyHeight = bodyHeight + innerHeight;
                body.height(bodyHeight);
            }
            else {
                body.css("height", 'auto');
            }

            view.height(view2.height());
        }
        else {

            var el = this.el;
            var width = this.el.width();
            var height = this.el.height();
            var view = el.children('div.datagrid-view');
            var body = view.children('div.datagrid-body');
            var header = view.children("div.datagrid-header");

            var bodyHeight = this.bodyTable.outerHeight();
            body.height(bodyHeight);

            var innerHeight = (body[0].offsetHeight - body[0].clientHeight);
            if (body[0].clientHeight > 0 && innerHeight > 0) {
                bodyHeight = bodyHeight + innerHeight;
                body.height(bodyHeight);
            }
            else {
                body.css("height", "auto");
            }
        }
    }
    else {
        if (this.frozen) {
            this.setSize();
        }
        else {
            var el = this.el;
            var width = this.el.width();
            var height = this.el.height();
            var view = el.children('div.datagrid-view');
            var body = view.children('div.datagrid-body');
            var header = view.children("div.datagrid-header");

            var innerWidth = (body[0].offsetWidth - body[0].clientWidth);
            if (body[0].clientWidth > 0 && innerWidth > 0) {
                header.children(".datagrid-header-inner").width(width - innerWidth);
                this.totalRowBody.width(width - innerWidth);
            }
            else {
                header.children(".datagrid-header-inner").css("width", "100%");
            }
        }
    }
}

UCML.DataGrid.prototype.bindEvents = function () {

    UCML.DataGrid.superclass.bindEvents.call(this);

    var opt = this;

    if (!UCML.isEmpty(this.dataTable)) {
        this.BusinessObject.on("onAfterLoadData", this.onAfterLoadData, this);
        this.dataTable.on("onLoad", this.BCLoadData, this);
        //     this.dataTable.on("onLoad", this.loadData, this);
        this.dataTable.on("OnFieldChange", this.onFieldChange, this);
        this.dataTable.on("OnAfterInsert", this.addRow, this);
        this.dataTable.on("OnAfterDelete", this.delRow, this);
        this.dataTable.on("OnValiate", this.onValiate, this);

        if (this.rowSelectMode == 1) {//单行选择模式
            this.dataTable.on("OnRecordChange", this.onRecordChange, this);
        }
    }
    if (this.cellSelectMode == 1) {//单元格选择模式为 单选时
        UCML.on(this.view, "keydown", this.handleKeyDown, this);
    }

    UCML.on(this.el, "mousedown", this.onMouseDown, this);
    UCML.on(this.el, "click", this.onClick, this);
    UCML.on(this.el, "dblclick", this.onDblClick, this);
    UCML.on(this.el, "keydown", this.onKeyDown, this);
    this.el.bind("contextmenu", function (e) {
        opt.processEvent('contextmenu', e);
        //  return false;
    });
    if (this.trackMouseOver) {
        UCML.on(this.bodyTable, "mouseover", this.onRowOver, this);
        if (this.frozenBodyTable) {
            UCML.on(this.frozenBodyTable, "mouseover", this.onRowOver, this);
            UCML.on(this.frozenBodyTable, "mouseout", this.onRowOut, this);
        }
        UCML.on(this.bodyTable, "mouseout", this.onRowOut, this);
    }

    this.bindSelectCellEvents(); //绑定选中单元格

    this.on("cellclick", this.cellClick, this);

    this.on('celldblclick', this.cellDbClick, this);

    this.on("rowclick", this.onRowClick, this);


    if (this.groupToggleMode == 1) {//分组切换模式
        this.on("groupclick", this.groupClick, this);
    }
    else if (this.groupToggleMode == 2) {
        this.on("groupdblclick", this.groupClick, this);
    }

    this.on("headerclick", onHeaderCellClick);

    /*
    this.on("headercontextmenu", function (cell, e) { //右击菜单分组
    e.stopPropagation();
    if (!cell.menu) {
    cell.menu = new UCML.Menu();
    cell.menu.addMenuItem("cmd_NOTSET", "分组");
    }
    cell.menu.show({ left: e.clientX, top: e.clientY });

    var fieldName = $(cell).attr('fieldName');
    this.groupField = fieldName;
    this.loadData();

    return false;
    }, this);
    */

    function onHeaderCellClick(headerCell, e) {
        var headerCell = $(headerCell);

        if (($(e.target).hasClass("datagrid-header-text") == true || $(e.target).hasClass("datagrid-sort-icon") == true)) {

            var fieldName = headerCell.attr('fieldName');
            if (!fieldName) {
                return;
            }

            var col = opt.getColumn(fieldName);
            if (!col) {
                return;
            }

            if (this.autoSort === false || col.autoSort === false) return;

            if (this.fireEvent('sortchange', col) !== false) {

                var sortMode = col.sortMode;

                var c = '';
                if (sortMode == null || sortMode == 3) {
                    sortMode = 0;
                    c = 'datagrid-sort-asc';
                    sortOrder = 'desc';
                }
                else if (sortMode == 0) {
                    sortMode = 1;
                    c = 'datagrid-sort-desc';
                }
                else {
                    c = '';
                    sortMode = 3;
                }

                col.sortMode = sortMode;
                $('.datagrid-header .datagrid-cell', this.el).removeClass('datagrid-sort-asc datagrid-sort-desc');
                headerCell.children(".datagrid-cell").addClass(c);

                opt.dataTable.sort(fieldName, sortMode);
            }
        }
    }
    $('.datagrid-header td:has(.datagrid-cell)', this.el).hover(
			function () { $(this).addClass('datagrid-header-over'); },
			function () { $(this).removeClass('datagrid-header-over'); }
		);

    if (this.haveQueryRow) {
        this.onQueryMenuReady();

        $(".datagrid-header .datagrid-header-query-cell[fieldName] .queryEdit", this.el).live("keydown", function (e) {
            if (e.keyCode == 13) {//回车进行查询
                var col = opt.getColumn($(this).attr("fieldName"));
                if (col && col.queryEditControl) {
                    col.queryEditControl.blur();
                }
                setTimeout(function () {
                    opt.onRunQuery();
                }, 200);
                return false;
            }
        }).live("click", function () {
            opt.clearActiveCell();
        });

        $('.datagrid-header .datagrid-header-query-cell[fieldName] .queryEditImg', this.el).live("click", function (e) {

            e.stopPropagation();
            var fieldName = $(this).parent().parent().parent().parent().parent().attr("fieldName");
            var queryMenu = opt.queryMenu;
            queryMenu.fieldName = fieldName;
            var x = e.clientX;
            var y = e.clientY;
            var dbody = $(window.document.body);
            var dbWidth = dbody.outerWidth();
            var qmWidth = queryMenu.el.outerWidth();
            var dbHeight = dbody.outerHeight();
            var qmHeight = queryMenu.el.outerHeight();

            if (qmWidth > dbWidth) {
                x = 0;
            }
            else if (x + qmWidth > dbWidth) {
                x = x - (x + qmWidth - dbWidth);
            }

            if (qmHeight > dbHeight) {
                y = 0;
            }
            else if (y + qmHeight > dbHeight) {
                y = y - (y + qmHeight - dbHeight);
            }

            var column = opt.getColumn(queryMenu.fieldName);
            queryMenu.setCheckedMenuItem(column.cmd);
            opt.queryMenu.show({ left: x - 4, top: y });
        });
        this.queryMenu.on("menuItemClick", this.onQueryMenuItemClick, this);
    }
}

//删除行
UCML.DataGrid.prototype.delRow = function (OID) {
    if (!this.changeDataToView) {
        return;
    }

    this.activeRow = null;
    this.activeCell = null;
    var index = this.getRowIndexByOID(OID);
    var rowCount = this.getRowCount();

    if(this.rowNumbers) {
        if(this.frozen)
            this.updateRowNumber(this.getFrozenRowByOID(OID));
        else
            this.updateRowNumber(this.getRowByOID(OID));
    }

    this.getRowByOID(OID).remove();
    if (this.frozen) {
        this.getFrozenRowByOID(OID).remove();
    }

    this.updateTotalRow();
    this.setSize();


}

UCML.DataGrid.prototype.updateRowNumber = function (tr, rownumber) {
    var text = (!isNaN(rownumber)) ? (rownumber+1) : tr.find(".datagrid-cell-rownumber").text();
    var nexttr = tr.next();
    if(nexttr.length > 0) {
        nexttr.find(".datagrid-cell-rownumber").text(text);
        this.updateRowNumber(nexttr, parseInt(text));
    }
}

UCML.DataGrid.prototype.onRecordChange = function (e, sender) {
    if (sender == this) {
        return;
    }
    var OID = this.dataTable.OID_GUID();
    var row = this.getRowByOID(OID);
    if (row.length) {
        this.clearActiveCell();
        this.setFocusRow(row);
    }
}

/**   
* @method rebuildGrid 
* @description 清除焦点单元格  
*/
UCML.DataGrid.prototype.clearActiveCell = function (e, sender) {
    if (this.activeCell) {
        this.unCellSelect(this.activeCell);
        this.activeCell = null;
    }
}

UCML.DataGrid.prototype.insertRecord = function () {
    this.dataTable.Insert();
}

UCML.DataGrid.prototype.addRow = function () {
    if (!this.changeDataToView) {
        return;
    }

    var rowCount = this.dataTable.getRecordCount();

    if (this.frozen) {
        var frozenBodyStr = this.renderRows(this.frozenColumns, true, rowCount - 1, rowCount);

        var bodyStr = this.renderRows(this.notfrozenColumns, false, rowCount - 1, rowCount);

        if (this.insertRecordByActiveRow && this.activeRow) { //插入记录根据焦点来控制
            var selectTr, frozenSelectTr;
            selectTr = this.bodyTable.find("tr." + this.activeRowCls);
            frozenSelectTr = this.frozenBodyTable.find("tr." + this.activeRowCls);

            if (this.insertRecordByActiveRowTop) {
                frozenSelectTr.before(frozenBodyStr);
                selectTr.before(bodyStr);
            }
            else {
                frozenSelectTr.after(frozenBodyStr);
                selectTr.after(bodyStr);
            }
        }
        else if (this.insertRecordTop) {
            this.frozenBodyTable.find("tr:first").after(frozenBodyStr);
            this.bodyTable.find("tr:first").after(bodyStr);
        }
        else {
            this.frozenBodyTable.append(frozenBodyStr);
            this.bodyTable.append(bodyStr);
        }
    }
    else {
        var bodyStr = this.renderRows(this.columns, true, rowCount - 1, rowCount);

        if (this.insertRecordByActiveRow && this.activeRow) { //插入记录根据焦点来控制
            var selectTr;
            selectTr = this.bodyTable.find("tr." + this.activeRowCls);

            if (this.insertRecordByActiveRowTop) {
                selectTr.before(bodyStr);
            }
            else {
                selectTr.after(bodyStr);
            }
        }
        else if (this.insertRecordTop) {
            this.bodyTable.find("tr:first").after(bodyStr);
        }
        else {
            this.bodyTable.append(bodyStr);
        }
    }

    this.setSize();

    if (this.insertRecordTop || rowCount <= 1) {
        this.body.scrollTop(0);
        this.setFocusRowAt(0);
    }
    else {
        this.body.scrollTop(this.bodyTable.innerHeight());
        this.setFocusRowAt(rowCount - 1);
    }
}

UCML.DataGrid.prototype.onValiate = function (config) {
    var tr = this.getRowByOID(config.OID);
    var td = this.getCellByName(tr, config.fieldName);
    return this.doRuleCheck(td, config.columnInfo, config.value);
}

//字段发生变化时数据发生改变
UCML.DataGrid.prototype.onFieldChange = function (e) {
    if (!this.changeDataToView) {
        return;
    }

    var col = this.getColumn(e.fieldName);
    var bcCol = this.dataTable.getColumn(e.fieldName);
    if (col) {
        var OID = this.dataTable.OID_GUID();

        var tr, td;
        if (this.frozen && col.fixedColumn) {
            tr = this.getFrozenRowByOID(OID);
            td = this.getCellByName(tr, e.fieldName);
        }
        else {
            tr = this.getRowByOID(OID);
            td = this.getCellByName(tr, e.fieldName);
        }

        if (this.fireEvent("fieldchange", td, e) === false) {
            return;
        }

        this.doRuleCheck(td, bcCol, e.value);
        this.setCellText(td, this.renderCell(col));
        this.updateTotalRowByFieldName(e.fieldName, col);

    }
}

UCML.DataGrid.prototype.setBCLink = function (fieldName, index, config) {
    var OID = this.dataTable.OID_GUID();
    this.rows = this.rows || {};
    this.rows[OID] = this.rows[OID] || {};
    this.rows[OID][fieldName] = this.rows[OID][fieldName] || {};
    this.rows[OID][fieldName]["BCLink"] = this.rows[OID][fieldName]["BCLink"] || {};
    if (this.rows[OID][fieldName]["BCLink"][index]) {
        UCML.apply(this.rows[OID][fieldName]["BCLink"][index], config);
    }
    else {
        this.rows[OID][fieldName]["BCLink"][index] = config;
    }
}

UCML.DataGrid.prototype.getBCLink = function (fieldName, index) {
    var OID = this.dataTable.OID_GUID();
    if (!this.rows) {
        return false;
    }
    if (!this.rows[OID]) {
        return false;
    }
    if (!this.rows[OID][fieldName]) {
        return false;
    }
    if (!this.rows[OID][fieldName]["BCLink"]) {
        return false;
    }
    if (!this.rows[OID][fieldName]["BCLink"][index]) {
        return false;
    }
    return this.rows[OID][fieldName]["BCLink"][index];
}

UCML.DataGrid.prototype.renderCell = function (col) {
    var bcCol = this.dataTable.getColumn(col.fieldName);
    var OID = this.dataTable.OID_GUID();
    var value = "";
    if (col.isFixColumnValue) {
        value = UCML.isEmpty(col.fixColumnValue) ? "" : col.fixColumnValue;
    }
    else {
        value = this.dataTable.getFieldValue(col.fieldName);
    }
    var text = "";
    if (col.EditControl && col.EditControl.renderedValue) {
        text = col.EditControl.renderedValue(value);
    }
    else {
        if (bcCol.isCodeTable) {
            value = this.BusinessObject.GetCodeCaption(bcCol.codeTable, value);
        }

        if (col.cellFormatter) {
            col.isShowTitle = false;
            var returnText = col.cellFormatter.call(this, { text: text, OID: OID, fieldName: col.fieldName, column: col });
            if (returnText) {
                return returnText;
            }
        }


        var objBCLinkColl = col.objBCLinkColl || bcCol.objBCLinkColl;

        if (col.showBCLink !== false && objBCLinkColl) {
            col.isShowTitle = false;
            text = "";
            this.CustomButtonPerm = new UCML.DataGrid.CustomButtonPerm(this);
            var flag = true;
            for (var i = 0; i < objBCLinkColl.length; i++) {
                if (objBCLinkColl[i].BCRunMode != 1) {
                    flag = false;
                    break;
                }
            }

            for (var n = 0; n < objBCLinkColl.length; n++) {

                if (objBCLinkColl[n].isDropDownMode == false) {
                    var caption = "";
                    if (objBCLinkColl[n].BCRunMode == 5) {
                        caption = value == "" ? objBCLinkColl[n].caption == "" ? UCML.Languge.BCLinkText : objBCLinkColl[n].caption : value;
                    }
                    else {
                        caption = value == "" ? UCML.Languge.BCLinkText : value;
                        if (flag) {
                            if (objBCLinkColl[n].condiFieldName && objBCLinkColl[n].constValue) {
                                if (this.dataTable.getFieldValue(objBCLinkColl[n].condiFieldName) != objBCLinkColl[n].constValue) {
                                    continue;
                                }
                            }
                        }
                    }
                    var BCLinkConfig = this.getBCLink(col.fieldName, n);
                    var style = "";
                    var disabled = "";
                    if (BCLinkConfig) {

                        if (BCLinkConfig.disable === true) {
                            style = "color: gray;"
                            disabled = " disabled=true";
                        }
                        if (BCLinkConfig.hiden === true) {
                            style = style + "display: none;"
                        }
                        if (BCLinkConfig.caption) {
                            caption = BCLinkConfig.caption;
                        }
                    }
                    else {
                        if (this.CustomButtonPerm) {
                            for (var i = 0; i < this.CustomButtonPerm.length; i++) {
                                //var caption = this.CustomButtonPerm[i]["caption"];
                                var enabled = this.CustomButtonPerm[i]["enabled"];
                                var visible = this.CustomButtonPerm[i]["visible"];
                                var buttonid = this.CustomButtonPerm[i]["buttonid"];
                                if (buttonid) {
                                    var obj = buttonid.split('$');
                                    if (col.fieldName == obj[0] && n == obj[1]) {
                                        if (enabled == "false" || enabled == 0) {
                                            style = "color: gray;"
                                            disabled = " disabled=true";
                                        }

                                        if (visible == "false" || visible == 0) {
                                            style = style + "display: none;"
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    BCLinkConfig.cls = BCLinkConfig.cls || "";
                    text += "<a ctype='UCML.BCLink' dataFld='" + col.fieldName + "'  '" + disabled + "' style='" + style + "' linkIndex='" + n + "' href='javascript:void(0)' title='" + (BCLinkConfig.title || "") + "' class='datagrid-cell-bclink " + (BCLinkConfig.cls || "") + " ' BCName='" + this.dataTable.BCName + "'  >" + caption + "</a>";
                }
            }

        }
        else {
            text = value;
            if (col.EditType == "UCML.CheckBox" || (UCML.isEmpty(col.EditType) == true && bcCol.fieldType == "Boolean")) {
                col.isShowTitle = false;
                col.align = "center";
                var attrStr = "";
                if (value == "true") {
                    attrStr += "checked ";
                }
                if (!col.allowModify || this.enabledEdit == false) {
                    attrStr += "disabled ";
                }
                text = "<input type='CheckBox' ctype='UCML.CheckBox' dataFld='" + col.fieldName + "' " + attrStr + "  BCName='" + this.dataTable.BCName + "' />";
            }
            else if (col.EditType == "UCML.Image") {
                if (text != "") {
                    if (text == ".jpg" || text == ".png" || text == ".jpeg" || text == ".gif" || text == ".bmp") {
                        //系统上传的保存在数据库字段
                        var src = getServicePath() + 'File/Images/' + this.dataTable.TableName + '_' + col.fieldName + '/' + this.dataTable.getUserOID() + '/' + this.dataTable.getOID() + text;
                    } else if (text.indexOf("https://") != -1 || text.indexOf("http://") != -1) {   //网络地址
                        var src = text;
                    } else if (text.indexOf("/") != -1) {//自定义路径,必须相对根目录下
                        var src = getServicePath() + text;
                    } else {//系统上传的保存文件名
                        var src = getServicePath() + 'File/Images/System/' + text;
                    }
                    text = '<img height=\'' + this.rowHeight + 'px\' width=\'100%\' app=\'' + src + '\'';
                }
            }
        }
    }
    return UCML.isEmpty(text) ? "&#160;" : text;
}

UCML.DataGrid.prototype.renderRows = function (columns, frozen, start, end, number) {
    var rowCount = this.dataTable.getRecordCount();
    var rowsStr = [];
    start = UCML.isEmpty(start) ? 0 : start;
    end = UCML.isEmpty(end) ? rowCount : end;

    var oldIndex = this.dataTable.recordIndex;

    for (var i = start; i < end; i++) {
        this.dataTable.SetIndexNoEvent(i);
        var OID = this.dataTable.OID_GUID();

        var rowData = {};
        if (this.beforeRowrender) {//如果监听了 行渲染的事件
            rowData = this.beforeRowrender.call(this, { OID: OID }) || {};
        }

        this.fireEvent("beforerowrender", { OID: OID }, rowsStr);

        var bodyRowTpl = new UCML.Template(this.bodyRowStr);
        var cellsStr = [];
        var cls = "";
        if (i % 2 && this.striped) {
            cls += " datagrid-row-alt";
        }

        //jqm样式
        if ((typeof BPOIsJQMPage !== "undefined") && BPOIsJQMPage) {
            var theme = BPOPageTheme || "a";
            if (i % 2 && this.striped) {
                cls += " datagrid-row-alt-" + theme;
            } else {
                cls += " datagrid-row-" + theme;
            }
        } else if (this.IsJQMPage) {
            var theme = this.JQMPageTheme || "a";
            if (i % 2 && this.striped) {
                cls += " datagrid-row-alt-" + theme;
            } else {
                cls += " datagrid-row-" + theme;
            }
        }

        if (frozen == true && this.rowNumbers && this.showCheckBox) {
            var bodyNumberTpl = new UCML.Template(this.bodyNumberStr);
            var text = number || i + 1 + this.dataTable.StartPos;

            var checkBoxValue = "";
            if (this.checkOID && this.checkOID.indexOf(OID) != -1) {
                checkBoxValue = "checked";
            }
            var bodyShowCheckBoxTpl = new UCML.Template(this.bodyShowCheckBoxStr);

            if (this.rowNumberBeforeCheckbox) {
                cellsStr.add(bodyNumberTpl.apply({
                    value: text
                    //   style: "width:" + this.rowNumberWidth + this.colUnit
                }));
                cellsStr.add(bodyShowCheckBoxTpl.apply({
                    //     style: "width:" + this.showCheckBoxWidth + this.colUnit,
                    checked: checkBoxValue
                }));
            } else {
                cellsStr.add(bodyShowCheckBoxTpl.apply({
                    //     style: "width:" + this.showCheckBoxWidth + this.colUnit,
                    checked: checkBoxValue
                }));
                cellsStr.add(bodyNumberTpl.apply({
                    value: text
                    //   style: "width:" + this.rowNumberWidth + this.colUnit
                }));
            }
        } else {
            if (frozen == true && this.rowNumbers) {
                var bodyNumberTpl = new UCML.Template(this.bodyNumberStr);

                var text = number || i + 1 + this.dataTable.StartPos;

                cellsStr.add(bodyNumberTpl.apply({
                    value: text
                    //   style: "width:" + this.rowNumberWidth + this.colUnit
                }));
            }

            if (frozen == true && this.showCheckBox) {
                var checkBoxValue = "";
                if (this.checkOID && this.checkOID.indexOf(OID) != -1) {
                    checkBoxValue = "checked";
                }
                var bodyShowCheckBoxTpl = new UCML.Template(this.bodyShowCheckBoxStr);
                cellsStr.add(bodyShowCheckBoxTpl.apply({
                    //     style: "width:" + this.showCheckBoxWidth + this.colUnit,
                    checked: checkBoxValue
                }));
            }
        }

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            var display = "";
            if (!col.display) {
                display = " display:none;";
            }
            var bodyCellTpl = new UCML.Template(this.bodyCellStr);
            var text = this.renderCell(col);
            var cellCls = "";

            if (this.nowrap) {
                cellCls = "datagrid-cell-nowrap";
            }

            var title = "";
            if (col.isShowTitle !== false) {//控制标题显示
                if (col.EditType != "UCML.Image")
                    title = text;
            }

            var cellData = {};
            if (col.beforeCellrender) {//如果监听了 行渲染的事件
                cellData = col.beforeCellrender.call(this, { OID: OID, value: text,
                    fieldName: col.fieldName
                }) || {};
            }

            cellData["style"] = "text-align:" + (col.align || 'left') + ";" + display + (cellData["style"] || '');

            cellData = UCML.apply({
                value: text,
                fieldName: col.fieldName,
                cls: cellCls,
                title: title
            }, cellData);


            cellsStr.add(bodyCellTpl.apply(cellData));
        }

        rowData = UCML.apply({
            style: "height:" + this.rowHeight + "px;",
            cells: cellsStr.join(''),
            OID: OID,
            cls: cls
        }, rowData);

        rowsStr.add(bodyRowTpl.apply(rowData));
        this.fireEvent("afterrowrender", { OID: OID, columns: columns, frozen: frozen }, rowsStr);

    }

    this.dataTable.recordIndex = oldIndex;

    return rowsStr.join('');
}

UCML.DataGrid.prototype.updateRow = function (OID) {
    var OID = OID || this.dataTable.OID_GUID();
    var index = this.dataTable.GetOIDIndex(OID);
    if (index < 0) {
        return;
    }

    if (this.frozen) {
        var frozenBodyStr = this.renderRows(this.frozenColumns, true, index, index + 1);
        this.getFrozenRowByOID(OID).replaceWith(frozenBodyStr);

        var bodyStr = this.renderRows(this.notfrozenColumns, false, index, index + 1);
        this.getRowByOID(OID).replaceWith(bodyStr);
    }
    else {
        var bodyStr = this.renderRows(this.columns, true, index, index + 1);
        this.getRowByOID(OID).replaceWith(bodyStr);
    }
}

UCML.DataGrid.prototype.setDataBody = function () {
    if (this.frozen) {
        var notfrozenColumnRow = this.bodyTable.find("tr:first");
        var frozenColumnRow = this.frozenBodyTable.find("tr:first");

        this.bodyTable.empty();
        this.frozenBodyTable.empty();

        var oldIndex = this.dataTable.recordIndex;
        var rowCount = this.dataTable.getRecordCount();

        var frozenBodyStr = this.renderRows(this.frozenColumns, true, 0, rowCount);
        var bodyStr = this.renderRows(this.notfrozenColumns, false, 0, rowCount);

        //开始渲染内容
        this.frozenBodyTable.append(frozenColumnRow);
        this.frozenBodyTable.append(frozenBodyStr);

        this.bodyTable.append(notfrozenColumnRow);
        this.bodyTable.append(bodyStr);

    }
    else {
        var notfrozenColumnRow = this.bodyTable.find("tr:first");
        this.bodyTable.empty();

        var bodyStr = this.renderRows(this.columns, true);

        this.bodyTable.append(notfrozenColumnRow);
        this.bodyTable.append(bodyStr);
    }
}

//修改读数据时刻bug
UCML.DataGrid.prototype.onAfterLoadData = function (data) {
    if (this.BusinessObject.dataLoading == true) {

        var rowCount = this.dataTable.getRecordCount();
        if (this.asyncLoadData && rowCount > this.asyncLoadDataByRecordCount) {//如果数据比较多则采用异步装载
            var opts = this;
            setTimeout(function () {
                opts.loadData(data);
            }, 100);
        }
        else {
            this.loadData(data);
        }
    }
}

UCML.DataGrid.prototype.BCLoadData = function (data) {
    if (this.BusinessObject.dataLoading == false) {//如果是首次加载
        return;
    }
    this.loadData(data);
}

UCML.DataGrid.prototype.loadData = function (data) {
    this.fireEvent("beforeloaddata", {});
    this.activeRow = null;
    this.activeCell = null;
    if (!this.crossPageCheck) {
        this.checkOID = [];
    }

    var beginDate = new Date();

    if (this.groupField) { //分组
        this.loadDataByGroup();
        this.updateTotalRow();

        this.updataBody();
    }
    else {
        this.setDataBody();
        this.updateTotalRow();
        this.updataBody();
    }
    this.fireEvent("loaddata", {});

    if (this.OnAppendDataPacket) {
        this.OnAppendDataPacket.call(this);
    }

    //   alert(new Date() - beginDate);
}

UCML.DataGrid.prototype.loadDataByGroup = function (data) {
    var rowCount = this.dataTable.getRecordCount();
    var oldIndex = this.dataTable.recordIndex;

    var groupField = this.groupField;
    var groupTotalFields = null;
    var groupTotalCaptions = null;
    if (this.groupTotalFields) groupTotalFields = this.groupTotalFields.split(";");
    if (this.groupTotalCaptions) groupTotalCaptions = this.groupTotalCaptions.split(";");

    var groupDatas = {}; //分组数据
    var col = this.getColumn(groupField);

    for (var i = 0; i < rowCount; i++) {
        this.dataTable.SetIndexNoEvent(i);
        var OID = this.dataTable.OID_GUID();
        var groupValue = this.dataTable.getFieldValue(groupField); //获得分组值

        if (col.isCodeTable && col.codeTable) {
            groupValue = GetCodeCaption(col.codeTable, groupValue);
        }

        if (!groupDatas[groupValue]) {//如果没有建立集合则需要创建
            groupDatas[groupValue] = {};
            groupDatas[groupValue]["OID"] = OID;
            groupDatas[groupValue]["groupValue"] = groupValue;
            groupDatas[groupValue]["items"] = [];
            groupDatas[groupValue]["total"] = [];
            if (groupTotalFields != null)
                for (var j = 0; j < groupTotalFields.length; j++) {
                    groupDatas[groupValue]["total"][groupTotalFields[j]] = {};
                    groupDatas[groupValue]["total"][groupTotalFields[j]].caption = groupTotalCaptions[j];
                    groupDatas[groupValue]["total"][groupTotalFields[j]].value = 0;
                    var totalcol = this.getColumn(groupTotalFields[j]);
                    groupDatas[groupValue]["total"][groupTotalFields[j]].col = totalcol;
                }
        }
        groupDatas[groupValue]["items"].add(OID);
        if (groupTotalFields != null)
            for (var j = 0; j < groupTotalFields.length; j++) {
                groupDatas[groupValue]["total"][groupTotalFields[j]].value += parseFloat(this.dataTable.getFieldValue(groupTotalFields[j]));
            }
    }
    this.dataTable.recordIndex = oldIndex;
    this.groupDatas = groupDatas;

    if (this.frozen) {
        this.bodyTable.empty();
        this.frozenBodyTable.empty();

        //开始渲染内容
        this.frozenBodyTable.append(this.renderGroupGrid(groupDatas, this.frozenColumns, true));
        this.bodyTable.append(this.renderGroupGrid(groupDatas, this.notfrozenColumns, false));

    }
    else {
        this.bodyTable.empty();
        this.bodyTable.append(this.renderGroupGrid(groupDatas, this.columns, true));
    }

}


UCML.DataGrid.prototype.renderGroupGrid = function (groupDatas, columns, fisrtTable) {
    var rowsStr = [];

    var columnRowStr = this.getRenderColumns(columns, fisrtTable);
    rowsStr.add(columnRowStr);

    var columnCount = columns.length;

    if (fisrtTable) {
        if (this.rowNumbers) {
            columnCount++;
        }
        if (this.showCheckBox) {
            columnCount++;
        }
    }

    var number = 1;

    for (var name in groupDatas) {
        var groupItem = groupDatas[name];
        this.fireEvent("beforegrouprender", { OID: groupItem.OID, groupValue: groupItem.groupValue });

        //渲染分组
        var groupRowTpl = new UCML.Template(this.groupRowStr);

        var groupItems = "";
        if (fisrtTable) {
            groupItems = "<a class='datagrid-group-icon datagrid-group-collapse' >&#160;</a>" + (groupItem.groupValue == "" ? "&#160;" : groupItem.groupValue);
        }
        else {
            groupItems = "<a class='datagrid-group-icon'>&#160;</a>" + "&#160;";
        }
        var groupTotalFields = null;
        var groupTotalCaptions = null;
        if (this.groupTotalFields) groupTotalFields = this.groupTotalFields.split(";");
        if (this.groupTotalCaptions) groupTotalCaptions = this.groupTotalCaptions.split(";");
        if (this.groupTotalFields)
            for (var j = 0; j < groupTotalFields.length; j++) {
                var value = groupDatas[groupItem.groupValue]["total"][groupTotalFields[j]].value;
                var col = groupDatas[groupItem.groupValue]["total"][groupTotalFields[j]].col;
                if (col.EditControl && col.EditControl.renderedValue) {
                    value = col.EditControl.renderedValue(value);
                }
                groupItems += "&#160;&#160;&#160;&#160;<a >" + groupDatas[groupItem.groupValue]["total"][groupTotalFields[j]].caption + ":" +
                value + "</a>" + "&#160;";
            }

        rowsStr.add(groupRowTpl.apply({
            style: "height:" + this.rowHeight + "px;",
            groupItems: groupItems,
            colspan: columnCount,
            cls: "",
            OID: groupItem.OID,
            groupValue: groupItem.groupValue
        }));


        //渲染分组项
        for (var i = 0; i < groupItem["items"].length; i++) {
            var itemOID = groupItem["items"][i];
            index = this.dataTable.GetOIDIndex(itemOID);
            if (index > -1) {
                rowsStr.add(this.renderRows(columns, fisrtTable, index, index + 1, number++));
            }
        }
    }

    return rowsStr.join('');
}

//显示分组项
UCML.DataGrid.prototype.showGroupItem = function (groupValue) {
    this.setGroupItemState(groupValue, true);
}

//隐藏分组项
UCML.DataGrid.prototype.hideGroupItem = function (groupValue) {
    this.setGroupItemState(groupValue, false);
}

//私有的
UCML.DataGrid.prototype.setGroupItemState = function (groupValue, v) {
    var groupItem = this.groupDatas[groupValue];
    if (groupItem) {
        for (var i = 0; i < groupItem["items"].length; i++) {
            var itemOID = groupItem["items"][i];
            if (this.frozen) {
                if (v) {
                    this.getFrozenRowByOID(itemOID).show();
                    this.getRowByOID(itemOID).show();
                }
                else {
                    this.getFrozenRowByOID(itemOID).hide();
                    this.getRowByOID(itemOID).hide();
                }
            }
            else {
                if (v) {
                    this.getRowByOID(itemOID).show();
                }
                else {
                    this.getRowByOID(itemOID).hide();
                }
            }
        }
    }
}

UCML.DataGrid.prototype.groupToggle = function (target) {
    if (this.fireEvent('groupchange') !== false) {
        var el = $(target);
        var groupValue = el.attr("groupValue");
        var icons = el.children(".datagrid-group-icon");
        if (icons.hasClass("datagrid-group-collapse"))//展开
        {
            icons.removeClass("datagrid-group-collapse").addClass("datagrid-group-expand");


            this.hideGroupItem(groupValue);
        }
        else if (icons.hasClass("datagrid-group-expand")) { //折叠
            icons.removeClass("datagrid-group-expand").addClass("datagrid-group-collapse");

            this.showGroupItem(groupValue);
        }
        else {
            this.groupToggle(this.frozenBodyTable.find("tr[rowType=groupRow] > td > div[groupValue=" + groupValue + "]"));
        }
    }
}

// private
UCML.DataGrid.prototype.processEvent = function (name, e) {
    this.fireEvent(name, e);
    var target = UCML.isElement(e) ? e : e.target;
    var el = $(target);

    var headerCell = this.findHeaderCell(el); //如果头部单元格
    if (headerCell != false) {
        this.fireEvent('header' + name, headerCell, e);
        return;
    }

    var cell = this.findCell(el);
    //判断是否单元格
    if (cell != false) {
    
        var row = cell.parentNode;
        this.fireEvent('row' + name, row, e);
        this.fireEvent('cell' + name, row, cell, e);
    } else {
        var row = this.findRow(target);
        var elRow = $(row);
        if (row != false && elRow.attr("rowType") == "bodyRow") {//数据行
            this.fireEvent('row' + name, row, e);
        }
        if (row != false && elRow.attr("rowType") == "groupRow") {//分组行
            this.fireEvent('group' + name, row, e);
        }
        else {
            this.fireEvent('container' + name, e);
        }
    }
}

UCML.DataGrid.prototype.onClick = function (el, e) {
    var target = $(e.target);

    this.processEvent('click', e);

    if (this.showCheckBox && (this.clicksToCheck == 1 && this.rowSelectMode == 2) == false && target.parent().hasClass("datagrid-cell-check")) {//如果点击的是复选框时
        this.checkChange(this.findRow(e.target), e.target.checked);
        return;
    }

    if (target.attr("ctype") == "UCML.BCLink") {
        /* if (this.enabledEdit == false) {
        return;
        }*/

        var fieldName = target.attr("dataFld");
        var index = parseInt(target.attr("linkIndex")) || 0;
        var BCLinkConfig = this.getBCLink(fieldName, index);
        var style = "";
        if (BCLinkConfig) {
            if (BCLinkConfig.disable === true) {
                return;
            }
        }
        startBCLink(target[0])
    }
    else if (target.attr("ctype") == "UCML.CheckBox") {
        var value = e.target.checked ? "true" : "false";
        this.dataTable.setFieldValue(target.attr("dataFld"), value);
    }
}

// private
UCML.DataGrid.prototype.onMouseDown = function (el, e) {
    this.processEvent('mousedown', e);
}

// private
UCML.DataGrid.prototype.onMouseup = function (el, e) {
    this.processEvent('mouseup', e);
}

// private
UCML.DataGrid.prototype.onMouseMove = function (el, e) {
    this.processEvent('mousemove', e);
}

// private
UCML.DataGrid.prototype.onDblClick = function (el, e) {
    this.processEvent('dblclick', e);
}

// private
UCML.DataGrid.prototype.onKeyDown = function (el, e) {
    this.processEvent('keydown', e);
}

UCML.DataGrid.prototype.onRowOver = function (el, e) {
    var rowIndex = this.findRowIndex(e.target);

    if (rowIndex !== false) {
        this.addRowClass(rowIndex, this.rowOverCls);
    }
}

UCML.DataGrid.prototype.onRowOut = function (el, e) {
    var rowIndex = this.findRowIndex(e.target);
    if (rowIndex !== false) {
        this.removeRowClass(rowIndex, this.rowOverCls);
    }
}


/**   
* @method setColumnVisible 
* @description 设置显示、隐藏列
* @param {String} fieldName  
* @param {bool} visible     
*/
UCML.DataGrid.prototype.setColumnVisible = function (fieldName, visible) {
    var col = this.getColumn(fieldName);
    if (col) {
        col.display = visible;
    }
}

/**   
* @method setColumnEnabled 
* @description 设置列是否允许修改
* @param {String} fieldName  
* @param {bool} canModify     
*/
UCML.DataGrid.prototype.setColumnEnabled = function (fieldName, canModify) {
    var col = this.getColumn(fieldName);
    if (col) {
        col.allowModify = canModify;
    }
}

/**   
* @method setFieldPermission 
* @description 设置操作列信息
* @param {String} fieldName  
* @param {bool} visible  
* @param {bool} canModify        
*/
UCML.DataGrid.prototype.setFieldPermission = function (fieldName, visible, canModify) {
    var col = this.getColumn(fieldName);
    if (col) {
        col.allowModify = canModify;
        col.display = visible;
    }
}

/**   
* @method setFocusRowAt 
* @description 设置焦点行(索引)
* @param {int} rowIndex      
*/
UCML.DataGrid.prototype.setFocusRowAt = function (rowIndex) {

    rowIndex = rowIndex || 0;
    //  rowIndex = rowIndex + 1; //处理定义列信息占一行
    var row = this.frozen ? this.getFrozenRow(rowIndex) : this.getRow(rowIndex);
    this.setFocusRow(row);
}

/**   
* @method setFocusRow 
* @description 设置焦点行(tr对象)
* @param {tr} row      
*/
UCML.DataGrid.prototype.setFocusRow = function (row) {
    var el = $(row);
    var OID = el.attr("OID");

    //如果还是当前行则不触发选择事件
    if (this.dataTable.OID_GUID() != OID) {
        this.dataTable.LocateOID_GUID(OID, this);
    }

    if (this.rowSelectMode <= 0) {
        return;
    }
    else if (this.rowSelectMode == 1) {//单行选择模式
        if (this.activeRow) {
            this.removeRowClass(this.getRowIndex(this.activeRow), this.activeRowCls);
        }

        this.unRowSelect(this.activeRow);
        this.onRowSelect(row);
    }
    else { //多行选择模式
        if (this.isRowSelect(row)) {
            this.unRowSelect(row);
        }
        else {
            this.onRowSelect(row);
        }
    }


    this.addRowClass(this.getRowIndex(row), this.activeRowCls);
    this.activeRow = row;
}

/**   
* @method unRowSelect 
* @description 取消行选中
* @param {tr} row      
*/
UCML.DataGrid.prototype.unRowSelect = function (row) {
    if (!UCML.isEmpty(row)) {
        var rowIndex = this.getRowIndex(row);
        if (rowIndex < 0) {
            return;
        }

        this.removeRowClass(rowIndex, this.rowSelectCls);
    }
}

/**   
* @method checkChange 
* @description 行复选框选中
* @param {tr} row 
* @param {bool} checked     
*/
UCML.DataGrid.prototype.checkChange = function (row, checked,noEvent) {
    if (!UCML.isEmpty(row)) {
        var rowIndex = this.getRowIndex(row);
        if (rowIndex < 0) {
            return;
        }
        if (this.frozen) {
            row = this.getFrozenRow(rowIndex);
        }
        else {
            row = this.getRow(rowIndex);
        }

        if (row) {
            var el = $(row);
            var checkBox = el.find(".datagrid-cell-check :checkbox");
            if (checkBox.length) {
                checkBox.attr("checked", checked);
                if(noEvent==undefined||noEvent==false)
                this.fireEvent('checkChange', checked, row);

                if (checked) {
                    this.onRowSelect(el);
                    var OID = el.attr("OID");
                    this.checkOID.remove(OID);
                    this.checkOID.add(OID);
                }
                else {
                    this.unRowSelect(el);
                    var OID = el.attr("OID");
                    this.checkOID.remove(OID);
                }
            }
        }
    }
}

/**   
* @method removeRowCheckBoxByOID 
* @description 根据OID移除复选框
* @param {String} OID 
*/
UCML.DataGrid.prototype.removeRowCheckBoxByOID = function (OID) {
    if (this.frozen) {
        var height = this.getFrozenRowByOID(OID).find(".datagrid-cell-check").outerHeight(true);
        this.getFrozenRowByOID(OID).find(".datagrid-cell-check").find("input").remove();
        this.getFrozenRowByOID(OID).find(".datagrid-cell-check").height(height);
    } else {
        var height = this.getRowByOID(OID).find(".datagrid-cell-check").outerHeight(true);
        this.getRowByOID(OID).find(".datagrid-cell-check").find("input").remove();
        this.getRowByOID(OID).find(".datagrid-cell-check").height(height);
    }
}

/**   
* @method removeRowCheckBoxByOID 
* @description 根据OID移除复选框（索引）
* @param {int} i 
*/
UCML.DataGrid.prototype.removeRowCheckBoxByAt = function (i) {
    if (this.frozen) {
        var height = $(this.getFrozenRows()[i + 1]).find(".datagrid-cell-check").outerHeight(true);
        $(this.getFrozenRows()[i + 1]).find(".datagrid-cell-check").find("input").remove();
        $(this.getFrozenRows()[i + 1]).find(".datagrid-cell-check").height(height);
    }
    else {
        var height = $(this.getRows()[i + 1]).find(".datagrid-cell-check").outerHeight(true);
        $(this.getRows()[i + 1]).find(".datagrid-cell-check").find("input").remove();
        $(this.getRows()[i + 1]).find(".datagrid-cell-check").height(height);
    }
}

/**   
* @method onRowSelect 
* @description 设置行选择
* @param {tr} row 
*/
UCML.DataGrid.prototype.onRowSelect = function (row) {
    if (!UCML.isEmpty(row)) {
        var rowIndex = this.getRowIndex(row);
        if (rowIndex < 0) {
            return;
        }
        this.addRowClass(rowIndex, this.rowSelectCls);
    }
}

/**   
* @method isRowSelect 
* @description 判断行是否选中
* @param {tr} row 
*/
UCML.DataGrid.prototype.isRowSelect = function (row) {
    if (row) {
        row = $(row);
        return row.hasClass(this.rowSelectCls);
    }
}

/**   
* @method isRowCheck 
* @description 判断行复选框是否选中
* @param {tr} row 
*/
UCML.DataGrid.prototype.isRowCheck = function (row) {
    if (row) {
        row = $(row);
        return row.find(".datagrid-cell-check :checkbox")[0].checked;
    }
}

/**   
* @method isRowCheckByOID 
* @description 根据OID判断行复选框是否选中
* @param {String} OID 
*/
UCML.DataGrid.prototype.isRowCheckByOID = function (OID) {
    if (OID) {
        var row;
        if (this.frozen) {
            row = this.getFrozenRowByOID(OID);
        } else {
            row = this.getRowByOID(OID);
        }
        if (row && row.length > 0) {
            return this.isRowCheck(row);
        }
        return false;
    }
}

/**   
* @method deleteCheckRow 
* @description 删除已选中的行
*/
UCML.DataGrid.prototype.deleteCheckRow = function () {
    var checks = this.getCheckOID();
    for (var i = 0; i < checks.length; i++) {
        if (this.dataTable.LocateOID(checks[i])) {
            this.dataTable.Delete();
        }
    }
}

/**   
* @method isRowSelectByOID 
* @description 根据oid判断当前行是否被选中
* @param {String} OID 
*/
UCML.DataGrid.prototype.isRowSelectByOID = function (OID) {
    if (OID) {
        row = this.getRowByOID(OID);
        if (row && row.length > 0) {
            return row.hasClass(this.rowSelectCls);
        }
        return false;
    }
}

/**   
* @method getSelectRows 
* @description 获得当前选中的行（jquery数组）
*/
UCML.DataGrid.prototype.getSelectRows = function () {
    return this.bodyTable.find('tr.' + this.rowSelectCls);
}

/**   
* @method getSelectOID 
* @description 返回已经选中行的（oid数组）
*/
UCML.DataGrid.prototype.getSelectOID = function () {
    var rows = this.bodyTable.find('tr.' + this.rowSelectCls);
    var OIDS = [];
    for (var i = 0; i < rows.length; i++) {
        OIDS.add($(rows[i]).attr("OID"));
    }
    return OIDS;
}

/**   
* @method getCheckOID 
* @description 返回已经选中复选框的（oid数组）
*/
UCML.DataGrid.prototype.getCheckOID = function (separator) {
    if (separator) {
        return this.checkOID.join(separator);
    }
    else {
        return this.checkOID;
    }
}

/**   
* @method setCheckOID 
* @description 根据OID设置选中复选框记录
* @param {Array或String} values
* @param {String} separator values是字符串的情况下需要设置，如";"
*/
UCML.DataGrid.prototype.setCheckOID = function (values, separator) {
    checkRows = values;
    if (separator) {
        checkRows = values.split(separator);
    }

    for (var i = 0; i < checkRows.length; i++) {
        var oid = checkRows[i];
        var row = this.getRowByOID(oid);
        if (row.length > 0) {
            this.checkChange(row, true,true);
        }
    }
}

/**   
* @method getCheckByFieldValues 
* @description 获取已经选中记录的某字段值默认为数组，如果传入第二个参数separator分隔符则，拼成一个字符串
* @param {String} fieldName
* @param {String} separator 如";"，如果不设置这个参数则反馈的数组设置返回是个字符串
*/
UCML.DataGrid.prototype.getCheckByFieldValues = function (fieldName, separator) {
    var checkRows = this.getCheckOID();
    var dataTable = this.dataTable;

    var values = [];
    var oldIndex = dataTable.getRecordIndex();

    for (var i = 0; i < checkRows.length; i++) {
        var oid = checkRows[i];
        if (dataTable.LocateOID_GUID(oid)) {
            values.add(dataTable.getFieldValue(fieldName));
        }
    }
    dataTable.SetIndexNoEvent(oldIndex);
    if (separator) {
        return values.join(separator);
    }
    else {
        return values;
    }
}

/**   
* @method setFocusCell 
* @description 设置焦点单元格
* @param {td} cell
*/
UCML.DataGrid.prototype.setFocusCell = function (cell, preventFocus) {
    //    if (this.activeCell == cell) {
    //        return;
    //    }

    if (this.rowSelectMode > 0) {
        var row = cell.parentNode;
        if (this.activeRow != row) {
            this.setFocusRow(row);
        }
    }
    if (this.cellSelectMode <= 0 || this.rowSelectMode == 2) {
        return;
    }
    if (this.cellSelectMode == 1) {//单格
        this.unCellSelect(this.activeCell);
        this.onCellSelect(cell);
    }
    //    else { //多格
    //        if (this.isCellSelect(cell)) {
    //            this.unCellSelect(cell);
    //        }
    //        else {
    //            this.onCellSelect(cell);
    //        }
    //    }

    if (preventFocus !== true) {
        //       if (UCML.isGecko) {
        cell.focus();
        //  cell.setActive();
        //        } else {
        //            cell.focus.defer(1, cell);
        //        }
    }

    cell = $(cell);
    this.activeCell = cell[0];


    //   this.frozenBodyTable[0].focus();

}

/**   
* @method isCellSelect 
* @description 单元格是否选中
* @param {td} cell
*/
UCML.DataGrid.prototype.isCellSelect = function (cell) {
    if (cell) {
        cell = $(cell);
        return cell.hasClass(this.cellSelectCls);
    }
}

/**   
* @method unCellSelect 
* @description 撤销选中单元格
* @param {td} cell
*/
UCML.DataGrid.prototype.unCellSelect = function (cell) {
    if (cell) {
        cell = $(cell);
        cell.removeClass(this.cellSelectCls);
    }
}

/**   
* @method onCellSelect 
* @description 选中单元格
* @param {td} cell
*/
UCML.DataGrid.prototype.onCellSelect = function (cell) {
    if (cell) {
        cell = $(cell);
        cell.addClass(this.cellSelectCls);
    }
}

UCML.DataGrid.prototype.onRowClick = function (row) {
    if (this.endEdit() === false) {//先结束编辑框内容，在可进行选择
        return;
    }

    this.setFocusRow(row);

    if (this.showCheckBox && this.clicksToCheck == 1 && this.rowSelectMode == 2) {//如果启用复选框时则行选中复选框也选中
        var checked = this.isRowSelect(row);
        this.checkChange(row, checked);
    }
}

UCML.DataGrid.prototype.groupClick = function (row) {
    this.groupToggle($(row).find("td .datagrid-cell-group"));
}


UCML.DataGrid.prototype.cellClick = function (row, cell) {
    if (this.endEdit() === false) {//先结束编辑框内容，在可进行选择
        return;
    }
    if (this.clicksToEdit == 1) {
        this.beginEdit(cell);
    }
    else if (this.cellSelectMode == 1) {
        this.setFocusCell(cell);
    }

    if (this.onCellDbClick) {
        this.onCellDbClick(row, cell);
    }

    if (this.OnCellClick) {
        this.OnCellClick(row, cell);
    }
}

UCML.DataGrid.prototype.cellDbClick = function (row, cell) {
    if (this.clicksToEdit == 2) {
        this.beginEdit(cell);
    } else if (this.cellSelectMode == 2) {
        this.setFocusCell(cell);
    }

    if (this.onCellDbClick) {
        this.onCellDbClick(row, cell);
    }

    if (this.OnCellDbClick) {
        this.OnCellDbClick(row, cell);
    }

    if (this.OnCellDblClick) {
        this.OnCellDblClick(row, cell);
    }
}


UCML.DataGrid.prototype.handleKeyDown = function (el, e) {
    //LEFT: 37,
    /** Key constant @type Number */
    //  UP: 38,
    /** Key constant @type Number */
    //  RIGHT: 39,
    /** Key constant @type Number */
    //  DOWN: 40,
    //       alert(e);

    if (!this.activeCell)
        return;
    var tr = this.activeCell.parentNode;
    var table = tr.parentNode.parentNode;
    var td;

    if (e.keyCode == 38 && !this.activeEditor) {//UP
        if (tr.rowIndex > 1) {
            tr = table.rows[tr.rowIndex - 1];
            td = tr.cells[this.activeCell.cellIndex];
            if (td == null) {
                var i = tr.rowIndex - 1;
                while (td == null && i > 0) {
                    td = table.rows[i].cells[this.activeCell.cellIndex];
                    i--;
                }
            }
        }
    }
    else if (e.keyCode == 40 && !this.activeEditor)//DOWN
    {

        if (tr.rowIndex < table.rows.length - 1) {

            tr = table.rows[tr.rowIndex + this.activeCell.rowSpan];
            td = tr.cells[this.activeCell.cellIndex];

        }

    }
    else if (e.keyCode == 37 && !this.activeEditor)//LEFT
    {
        var nIndex = 0;
        //如果左边有固定的内容则不能移动
        if (this.showCheckBox) {
            nIndex++;
        }
        if (this.rowNumbers) {
            nIndex++;
        }

        if (this.activeCell.cellIndex > 0) {
            td = tr.cells[this.activeCell.cellIndex - this.activeCell.colSpan];
        }
        else if (this.activeCell.cellIndex == nIndex && this.frozen == true && this.bodyTable[0] == table) {
            td = this.frozenBodyTable[0].rows[tr.rowIndex].cells[this.frozenBodyTable[0].rows[tr.rowIndex].cells.length - 1];
        }
    }
    else if (e.keyCode == 39 && !this.activeEditor)//RIGHT
    {
        if (this.activeCell.cellIndex < tr.cells.length - 1) {
            td = tr.cells[this.activeCell.cellIndex + this.activeCell.colSpan];
        }
        else if (this.activeCell.cellIndex == tr.cells.length - 1 && this.frozen == true && this.frozenBodyTable[0] == table && this.frozenBodyTable[0].rows[tr.rowIndex].cells.length > 0) {
            td = this.bodyTable[0].rows[tr.rowIndex].cells[0];
        }
        else if ((this.activeCell.cellIndex + this.activeCell.colSpan - 1) == tr.cells.length - 1 && tr.rowIndex < table.rows.length - 1) {
            td = table.rows[tr.rowIndex + 1].cells[0];
        }
    }
    else if (e.keyCode == 13 && !this.activeEditor)//Enter
    {
        if (!this.beginEdit(this.activeCell)) {
            this.selectNextCell();
        }
    }

    if (td) {//设置焦点单元格
        this.setFocusCell(td);
        return;
    }

    if (!e.ctrlKey && this.activeCell && this.clicksToEdit == 2) {//双击编辑时
        if (this.activeEditor) {

        } else {
            this.beginEdit(this.activeCell);
        }
    }
}

/**   
* @method selectUpRow 
* @description 选中上一行
*/
UCML.DataGrid.prototype.selectUpRow = function () {
    if (!this.activeRow) {
        this.setFocusRowAt(0);
    }
    else if (this.activeRow.rowIndex > 1 && this.activeRow.rowIndex <= this.getRowCount()) {
        this.setFocusRowAt(this.activeRow.rowIndex - 2);
    }
}

/**   
* @method selectNextRow 
* @description 选中下一行
*/
UCML.DataGrid.prototype.selectNextRow = function () {
    if (!this.activeRow) {
        this.setFocusRowAt(0);
    }
    else if (this.activeRow.rowIndex > 0 && this.activeRow.rowIndex < this.getRowCount()) {
        this.setFocusRowAt(this.activeRow.rowIndex);
    }
}

/**   
* @method selectNextCell 
* @description 选中下一个单元格
*/
UCML.DataGrid.prototype.selectNextCell = function () {
    if (!this.activeCell) {
        return;
    }
    var td;
    var tr = this.activeCell.parentNode;
    var table = tr.parentNode.parentNode;

    var nIndex = 0;
    //如果左边有固定的内容则不能移动
    if (this.showCheckBox) {
        nIndex++;
    }
    if (this.rowNumbers) {
        nIndex++;
    }

    if (this.activeCell.cellIndex < tr.cells.length - 1) {
        td = tr.cells[this.activeCell.cellIndex + this.activeCell.colSpan];
    }
    else if (this.activeCell.cellIndex == tr.cells.length - 1 && this.frozen == true && this.frozenBodyTable[0] == table && this.frozenBodyTable[0].rows[tr.rowIndex].cells.length > 0) {
        td = this.bodyTable[0].rows[tr.rowIndex].cells[0];
    }
    else if ((this.activeCell.cellIndex + this.activeCell.colSpan - 1) == tr.cells.length - 1 && tr.rowIndex < table.rows.length - 1) {
        if (this.frozen) {
            td = this.frozenBodyTable[0].rows[tr.rowIndex + 1].cells[0];
        }
        else {
            td = table.rows[tr.rowIndex + 1].cells[0];
        }
    }
    else {
        this.setFocusCell(this.activeCell);
    }

    if (td) {//设置焦点单元格
        this.setFocusCell(td);

        if (UCML.isIE7 || UCML.isIE6) {
            var body = $(table).parents(".datagrid-body");
            var scrollWidth = body[0].scrollWidth;
            var width = body.width();

            var left = parseInt((scrollWidth - width) / width * $(this.activeCell).position().left);
            $(table).parents(".datagrid-body")[0].scrollLeft = left;
        }
        return;
    }
    //如果为最后一个单元格则新增一条数据，并移动单元格焦点
    else
    {
        this.dataTable.Insert();
        if (this.frozen) {
            td = this.frozenBodyTable[0].rows[tr.rowIndex + 1].cells[0];
        }
        else {
            td = table.rows[tr.rowIndex + 1].cells[0];
        }
        this.setFocusCell(td);

        if (UCML.isIE7 || UCML.isIE6) {
            var body = $(table).parents(".datagrid-body");
            var scrollWidth = body[0].scrollWidth;
            var width = body.width();

            var left = parseInt((scrollWidth - width) / width * $(this.activeCell).position().left);
            $(table).parents(".datagrid-body")[0].scrollLeft = left;
        }
        return;
    }

}

/**   
* @method addRowClass 
* @description 根据行索引设置行样式
* @param {int} rowIndex 行索引
* @param {String} cls 样式            
*/
UCML.DataGrid.prototype.addRowClass = function (rowIndex, cls) {
    $(this.getRow(rowIndex)).addClass(cls);
    if (this.frozen) {
        $(this.getFrozenRow(rowIndex)).addClass(cls);
    }
}

/**   
* @method removeRowClass 
* @description 根据行索引移除行样式
* @param {int} rowIndex 行索引
* @param {String} cls 样式            
*/
UCML.DataGrid.prototype.removeRowClass = function (rowIndex, cls) {
    $(this.getRow(rowIndex)).removeClass(cls);
    if (this.frozen) {
        $(this.getFrozenRow(rowIndex)).removeClass(cls);
    }
}

UCML.DataGrid.prototype.findRowIndex = function (el) {
    var row = this.findRow(el);
    return row ? row.rowIndex : false;
}

UCML.DataGrid.prototype.findRow = function (el) {
    var tr;
    el = $(el);
    if (el.hasClass("datagrid-row")) {
        return el[0];
    }
    tr = el.parents("tr.datagrid-row");
    if (tr.length > 0) {
        return tr[0];
    }
    else {
        return false;
    }
}

/**   
* @method getFrozenRowByOID 
* @description 根据OID获得冻结部分行
* @param {String} OID           
*/
UCML.DataGrid.prototype.getFrozenRowByOID = function (OID) {
    // if (this.dataTable) {
    //     OID = this.dataTable.GetGUIDBy_OID(OID);
    // }
    return this.frozenBodyTable.find("tr[OID=" + OID + "]");
    //  return $(this.getFrozenRow(this.dataTable.GetOIDIndex(OID)));
}

/**   
* @method getFrozenRow 
* @description 根据索引获得冻结部分行
* @param {int} rowIndex           
*/
UCML.DataGrid.prototype.getFrozenRow = function (rowIndex) {
    if (rowIndex >= 0) {
        return this.getFrozenRows()[rowIndex + 1];
    }
}

/**   
* @method getCellByName 
* @description 根据行和字段名称获得单元格
* @param {tr} row 
* @param {String} name              
*/
UCML.DataGrid.prototype.getCellByName = function (row, name) {
    return $(row).children("[fieldName=" + name + "]");

}

/**   
* @method getRow 
* @description 根据索引获得行对象
* @param {int} rowIndex             
*/
UCML.DataGrid.prototype.getRow = function (rowIndex) {
    if (rowIndex >= 0) {
        return this.getRows()[rowIndex + 1];
    }
}

/**   
* @method getRowIndex 
* @description 根据行对象获得行索引
* @param {tr} row             
*/
UCML.DataGrid.prototype.getRowIndex = function (row) {
    var rowIndex = -1;
    if (!UCML.isNumber(row)) {
        row = $(row);
        if (row.length > 0) {
            rowIndex = row[0].rowIndex - 1;
        }
    }
    else {
        rowIndex = row;
    }
    return rowIndex;
}

/**   
* @method getRowByOID 
* @description 根据OID获得行对象
* @param {String} OID             
*/
UCML.DataGrid.prototype.getRowByOID = function (OID) {
    if (OID.length < 36) {
        if (this.dataTable) {
            OID = this.dataTable.GetGUIDBy_OID(OID);
        }
    }
    return this.bodyTable.find("tr[OID=" + OID + "]");
}

/**   
* @method getRowCount 
* @description 获得总行数          
*/
UCML.DataGrid.prototype.getRowCount = function () {
    if (this.frozen) {
        return this.getFrozenRows().length - 1;
    }
    else {
        return this.getRows().length - 1;
    }
}

/**   
* @method getRowIndexByOID 
* @description 根据OID获得行索引
* @param {String} OID             
*/
UCML.DataGrid.prototype.getRowIndexByOID = function (OID) {

    var row;
    var index = -1;
    if (this.frozen) {
        row = this.getFrozenRowByOID(OID);
    }
    else {
        row = this.getRowByOID(OID);
    }
    if (row && row.length > 0) {
        index = row[0].rowIndex - 1;
    }
    return index;
}

/**   
* @method getRows 
* @description 获得所有行       
*/
UCML.DataGrid.prototype.getRows = function () {
    return this.bodyTable[0].rows;
    //  return this.bodyTable.find("tr[rowType=bodyRow]");
}

/**   
* @method getFrozenRows 
* @description 获得冻结部分所有行       
*/
UCML.DataGrid.prototype.getFrozenRows = function () {
    return this.frozenBodyTable[0].rows;
    // return this.frozenBodyTable.find("tr[rowType=bodyRow]");
}

UCML.DataGrid.prototype.findCell = function (el) {
    var td;
    el = $(el);
    if (el.hasClass("datagrid-td")) {
        return el[0];
    }
    td = el.parents("td.datagrid-td");
    if (td.length > 0) {
        return td[0];
    }
    else {
        return false;
    }
}

UCML.DataGrid.prototype.findHeaderCell = function (el) {
    var td;
    el = $(el);
    if (el.hasClass("datagrid-header-cell")) {
        return el[0];
    }
    td = el.parents("td.datagrid-header-cell");
    if (td.length > 0) {
        return td[0];
    }
    else {
        return false;
    }
}

UCML.DataGrid.prototype.findCellIndex = function (el) {
    var cell = this.findCell(el);
    return cell ? cell.cellIndex : false;
}

/**   
* @method getCells 
* @description 根据行对象获得当前行的所有单元格
* @param {tr} row            
*/
UCML.DataGrid.prototype.getCells = function (row) {
    return row.cells;
}

/**   
* @method getCell 
* @description 根据行对象和列索引获得单元格
* @param {tr} row
* @param {int} cellIndex            
*/
UCML.DataGrid.prototype.getCell = function (row, cellIndex) {
    return this.getCells(row)[cellIndex];
}


/**   
* @method isFrozenColumn 
* @description 根据字段判断是否冻结列
* @param {String} fieldName         
*/
UCML.DataGrid.prototype.isFrozenColumn = function (fieldName) {
    var col = this.getColumn(fieldName);
    if (col.fixedColumn) {
        return true;
    }
    return false;
}


/**   
* @method getCurrentColumnInfo 
* @description 根据当前焦点行获得当前列定义信息       
*/
UCML.DataGrid.prototype.getCurrentColumnInfo = function () {
    if (this.activeCell != null) {
        var fieldName = $(this.activeCell).attr("fieldName");
        return this.getColumn(fieldName);
    }
    else return this.columns[0];


}

/**   
* @method columnIsNumber 
* @description 根据字段名称判断是否数字类型
* @param {String} fieldName 字段名称       
*/
UCML.DataGrid.prototype.columnIsNumber = function (fieldName) {
    return this.dataTable.columnIsNumber(fieldName);
}

/**   
* @method getTotalDataByFieldName 
* @description 根据字段名称和聚合类型服务端统计数据
* @param {String} fieldName 字段名称
* @param {String} mode 集合类型（SUM、AVG、COUNT、MAX、MIN）        
*/
UCML.DataGrid.prototype.getTotalDataByFieldName = function (fieldName, mode) {
    if (!fieldName || !mode) {
        return "";
    }

    var columnInfo = this.dataTable.getColumn(fieldName)
    var curField = this.dataTable.TableName + "." + columnInfo.fieldName;
    if (columnInfo.fieldKind == 2)
        curField = "al" + columnInfo.foreignKeyField + "." + columnInfo.lookupResultField;
    else if (columnInfo.fieldKind == 5 && columnInfo.QueryRefColumn) {
        curField = bcColumn.QueryRefColumn;
    }

    var cond = this.dataTable.getForeignCondition();

    //return getTotalData(this.dataTable.BCName, cond.fieldList + this.dataTable.getFieldList(), cond.valueList + this.dataTable.getValueList(), cond.condiIndentList + this.dataTable.getCondiIndentList(), "", 0, "", curField, mode, function () { }, function (obj, text) { alert("统计出错:" + text); }, null, false);
    //参数this.dataTable.theSQLCondi 支持服务端统计的前端查询
    return getTotalData(this.dataTable.BCName, cond.fieldList + this.dataTable.getFieldList(), cond.valueList + this.dataTable.getValueList(), cond.condiIndentList + this.dataTable.getCondiIndentList(), this.dataTable.theSQLCondi, 0, "", curField, mode, function () { }, function (obj, text) { alert("统计出错:" + text); }, null, false);
}

/**   
* @method setCellText 
* @description 根据单元格对象设置单元格内容
* @param {td} cell  
* @param {String} value          
*/
UCML.DataGrid.prototype.setCellText = function (cell, value) {
    $(cell).find("div.datagrid-cell").html(value).removeClass("datagrid-editable");
}

UCML.DataGrid.prototype.beginEdit = function (cell) {

    this.setFocusCell(cell, true);

    cell = $(cell);
    if (this.enabledEdit == false) {
        return false;
    }

    if (this.dataTable.IsRecordOwner() == false) {
        alert("您现在无权修改这条记录!");
        return false;
    }
    else {

        var fieldName = cell.attr("fieldName");
        var col = this.getColumn(fieldName);
        if (!col) {
            return false;
        }
        var bcColumn = this.dataTable.getColumn(col.fieldName);
        var value = this.dataTable.getFieldValue(col.fieldName);

        var objBCLinkColl = col.objBCLinkColl || bcColumn.objBCLinkColl;

        if (!col.allowModify || objBCLinkColl) {
            return false;
        }

        var config = { column: col, bcColumn: bcColumn, value: value, fieldName: fieldName };

        var col = config.column;
        var fieldName = config.fieldName;
        var value = config.value;
        var bcColumn = config.bcColumn;

        if (this.fireEvent("beforebeginedit", { value: value, fieldName: fieldName }) === false) {
            return false;
        }

        var ctype = col.EditType || col.editContrl;

        if (this.endEdit() === false) {
            return false;
        }
        this.editCell = cell[0];

        var obj = new Object();

        if (bcColumn.isCodeTable) {
            document.body.onselectstart = document.body.ondrag = function () { return false; }
            ctype = "UCML.DropDownList";
            obj.isCodeTable = true;
            obj.isSetProperties = false;
            obj.codeTable = bcColumn.codeTable;
        }
        else {
            document.body.onselectstart = null;
        }
        obj.dataTable = this.dataTable;
        obj.fieldName = fieldName;
        obj.value = value;
        obj.applet = this;
        obj.BusinessObject = this.BusinessObject;

        if (this.dataTable.columnIsNumber(fieldName)) {
            obj.precision = bcColumn.decLength;
        }

        if (!col.EditControl) {
            if (!ctype) {
                switch (bcColumn.fieldType) {
                    case "Date":
                        ctype = "UCML.Datebox";
                        break;
                    case "DateTime":
                        ctype = "UCML.DateTimebox";
                        break;
                    case "Time":
                        ctype = "UCML.Timebox";
                        break;
                    case "Boolean":
                        return false;
                        break;
                }
            }
            else {
                switch (ctype) {
                    case "INPUT":
                        ctype = "UCML.TextBox";
                        break;
                    case "UCML.CheckBox":
                        return;
                    case "DATETIME":
                    case "DATEEDIT":
                        ctype = "UCML.DateTimeBox";
                        break;
                    case "DATE":
                        ctype = "UCML.Datebox";
                        break;
                    case "TIME":
                        ctype = "UCML.Timebox";
                        break;
                    case "TEXTAREA":
                        ctype = "UCML.TextArea";

                        break;
                    case "UCML.Html":
                        ctype = "UCML.TextBox";
                        break;
                }
            }
            obj.ctype = ctype || "UCML.TextBox";

            if (ctype == "UCML.TextArea") {//不是文本域的时候
                var cellWidth = cell.outerWidth();
                var cellHeight = cell.outerHeight();
                obj.width = cellWidth < 200 ? 200 : cellWidth;
                obj.height = cellHeight < 50 ? 50 : cellHeight;
            }
            this.activeEditor = this.createEdit(cell, obj);
            //   col.EditControl = this.activeEditor;
        }
        else {

            col.EditControl.dataTable = this.dataTable;
            col.EditControl.fieldName = fieldName;
            obj.value = value;
            this.activeEditor = col.EditControl;
            this.setEditOffset(cell, obj);
            this.activeEditor.show();
        }
        if (ctype != "UCML.TextArea") {//不是文本域的时候
            UCML.on(this.activeEditor, "keydown", this.onActiveEditorKeyDown, this);

            //解决文本框回车自动提交问题
            this.activeEditor.el.bind("keypress", function (e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
        }

        this.activeEditor.on("blur", this.onEditBlur, this);
        this.activeEditor.on("setValue", this.setControlValue, this.activeEditor);
        this.updateEdit(cell, this.activeEditor, obj);

        if (this.activeEditor.focus) {
            this.activeEditor.focus();
        }
        if (this.activeEditor.select) {
            this.activeEditor.select();
        }
        return true;
    }
}

UCML.DataGrid.prototype.setControlValue = function (value) {
    var bcCol = this.applet.dataTable.getColumn(this.fieldName);
    this.applet.doRuleCheck(this.applet.editCell, bcCol, value);

    this.dataTable.setFieldValue(this.fieldName, value, this);
}

UCML.DataGrid.prototype.onEditBlur = function (edit) {
    document.body.onselectstart = null;
    this.endEdit();
}

UCML.DataGrid.prototype.onActiveEditorKeyDown = function (el, e) {

    if (e.keyCode == 13)//Enter
    {
        if (this.activeEditor && this.activeEditor.blur) {
            this.activeEditor.blur();
        }

        var opts = this;
        setTimeout(function () {
            if (opts.endEdit() === false) {
                return false;
            }
            opts.selectNextCell();
        }, 200);
    }
}

//私有的
UCML.DataGrid.prototype.createEdit = function (cell, obj) {

    this.setEditOffset(cell, obj); //先设置好位置
    obj.rendeTo = this.el.children('.datagrid-edit-body');
    obj.isRuleCheck = false;
    var dataCell = UCML.create(obj, obj.ctype);
    if (dataCell.tagName == "input" || dataCell.tagName == "textarea") {//如果是文本框活着文本域
        dataCell.addClass("datagrid-editable-input");
    }
    return dataCell;
}

UCML.DataGrid.prototype.setEditOffset = function (cell, obj) {
    var offset = cell.offset();
    obj.top = obj.top || offset.top;
    obj.left = obj.left || offset.left;

    obj.width = obj.width || cell.outerWidth();
    obj.height = obj.height || cell.outerHeight();

    var body = cell.parents(".datagrid-body");
    var bodyLeft = body.offset().left;
    var bodyRight = bodyLeft + body[0].clientWidth;

    if (obj.left + obj.width > bodyRight) {//如果编辑控件宽度溢出的时候
        obj.width = bodyRight - obj.left;
    }

    if (obj.left < 0) {
        obj.width = obj.width - (bodyLeft - obj.left);
        obj.left = bodyLeft;
    }
    else if (obj.left < bodyLeft) {
        obj.left = bodyLeft;
        obj.width = obj.width - (bodyLeft - obj.left);
    }

    var w = cell.outerWidth(true) - cell.innerWidth();
    var h = cell.outerHeight(true) - cell.innerHeight();
    obj.left = obj.left - w;
    obj.top = obj.top - h;


    this.el.children('.datagrid-edit-body').offset({ top: obj.top, left: obj.left });
}

UCML.DataGrid.prototype.updateEdit = function (cell, dataCell, obj) {
    dataCell.setValue(obj.value, false);
    dataCell.resize(obj.width, obj.height);
    return dataCell;
}

UCML.DataGrid.prototype.doRuleCheck = function (cell, col, val) {
    var value = val;
    var objColumn = col;
    var result = null;
    if (typeof (ACColumnInputRule) != "undefined") {
        for (var m = 0; m < ACColumnInputRule.length; m++) {
            if (objColumn.fieldName == ACColumnInputRule[m].FieldName && this.dataTable.BCName == ACColumnInputRule[m].BCName) {
                var inputParams = new Array();
                inputParams[inputParams.length] = value;
                if (ACColumnInputRule[m].ExParam1 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = ACColumnInputRule[m].ExParam1;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (ACColumnInputRule[m].ExParam2 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = ACColumnInputRule[m].ExParam2;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (ACColumnInputRule[m].ExParam3 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = ACColumnInputRule[m].ExParam3;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (ACColumnInputRule[m].ExParam4 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = ACColumnInputRule[m].ExParam4;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                var rresult = ExecuteRule(ACColumnInputRule[m].RuleMethod, inputParams);
                rresult.PromptMode = ACColumnInputRule[m].PromptMode;
                result = rresult;
                if (result) {
                    cell = $(cell);
                    if (cell.length > 0) {
                        if (result.ReturnCode != "OK") {
                            cell.addClass(this.ruleCheckCellCls);
                            if (!cell[0].Validatebox) {
                                cell[0].Validatebox = new UCML.Validatebox(cell[0]);
                            }
                            cell[0].Validatebox.setTipMessage(result.ReturnDescText);
                            cell[0].Validatebox.showTip();

                            return false;
                        }
                        else {
                            cell.removeClass(this.ruleCheckCellCls);

                            if (cell[0].Validatebox) {
                                cell[0].Validatebox.hideTip();
                            }
                        }
                    }
                }
            }
        }
    }
    if (result == null)
        if ((objColumn.allowNull == true && !UCML.isEmpty(value)) && (objColumn.RuleList != null && objColumn.RuleList.length > 0)) {
            for (var i = 0; i < objColumn.RuleList.length; i++) {
                var inputParams = new Array();

                inputParams[inputParams.length] = value;
                if (objColumn.RuleList[i].ExParam1 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam1;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam2 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam2;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam3 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam3;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                if (objColumn.RuleList[i].ExParam4 != "") {
                    var strsrl = new Array(); //定义一数组 
                    var strrlist = objColumn.RuleList[i].ExParam4;
                    strsrl = strrlist.split("["); //字符分割
                    if (strsrl[0] == "") {
                        var thisvalue = strsrl[1].split("]")[0];
                        inputParams[inputParams.length] = this.dataTable.getFieldValue(thisvalue);
                    }
                    else {
                        inputParams[inputParams.length] = strrlist;
                    }
                }
                var rresult = ExecuteRule(objColumn.RuleList[i].RuleMethod, inputParams);
                rresult.PromptMode = objColumn.RuleList[i].PromptMode;
                if(objColumn.RuleList[i].InputTip != "") {
                    rresult.ReturnDescText = objColumn.RuleList[i].InputTip;
                }
                result = rresult;

                if (result) {
                    cell = $(cell);
                    if (cell.length > 0) {
                        if (result.ReturnCode != "OK") {
                            if (result.PromptMode == 0) {
                                cell.addClass(this.ruleCheckCellCls);
                                if (!cell[0].Validatebox) {
                                    cell[0].Validatebox = new UCML.Validatebox(cell[0]);
                                }
                                cell[0].Validatebox.setTipMessage(result.ReturnDescText);
                                cell[0].Validatebox.showTip();
                                return false;
                            }
                            else if (result.PromptMode == 1) {
                                alert(result.ReturnDescText);
                            }
                        }
                        else {
                            cell.removeClass(this.ruleCheckCellCls);

                            if (cell[0].Validatebox) {
                                cell[0].Validatebox.hideTip();
                            }
                        }
                    }
                }
            }
        }
        else if (objColumn.allowNull == false && UCML.isEmpty(value)) {
            cell = $(cell);
            if (cell.length > 0) {
                cell.addClass(this.ruleCheckCellCls);
                if (!cell[0].Validatebox) {
                    cell[0].Validatebox = new UCML.Validatebox(cell[0]);
                }
                error = UCML.Languge.BCfieldLanguge + " " + objColumn.caption + " " + UCML.Languge.BCnotForNull;
                cell[0].Validatebox.setTipMessage(error);
                cell[0].Validatebox.showTip();
                return false;
            }
        }
        else {
            cell = $(cell);
            cell.removeClass(this.ruleCheckCellCls);
            if (cell[0] && cell[0].Validatebox) {
                cell[0].Validatebox.hideTip();
            }
        }
    return true;
}

UCML.DataGrid.prototype.endEdit = function () {

    if (this.activeEditor) {

        var fieldName = this.activeEditor.fieldName;
        var bcCol = this.dataTable.getColumn(fieldName);
        var value = this.activeEditor.getValue();

        var col = this.getColumn(fieldName);

        if (!this.doRuleCheck(this.editCell, bcCol, value)) {//验证不通过
            if (this.activeEditor.focus) {
                this.activeEditor.focus();
            }
            return false;
        }

        this.activeEditor.un("blur", this.onEditBlur, this);

        if (this.fireEvent("endedit", this.editCell, { value: value, fieldName: fieldName }) === false) {
            return false;
        }

        if (col.EditControl) {
            this.activeEditor.hide();
        }
        else {
            this.activeEditor.destroy();
        }


        if (bcCol.isCodeTable) {
            value = this.BusinessObject.GetCodeCaption(bcCol.codeTable, value);
        }
        this.setCellText(this.editCell, this.renderCell(col));
        delete this.activeEditor;
        delete this.editCell;
    }
    return true;
}

UCML.DataGrid.prototype.fixColumnSize = function (cell, data) {
    var opts = this;

    if (cell) {
        fix(cell, data);
    } else {
        $('.datagrid-header td', this.el).each(function () {
            var headerCell = $(this);
            var fieldName = headerCell.attr('fieldName');
            var col = opts.getColumn(fieldName);
            col.width = $.boxModel == true ? headerCell.width() : headerCell.outerWidth();
            fix(this);
        });
    }

    function fix(cell) {
        var headerCell = $(cell);
        if (headerCell.width() == 0) return;
        var fieldName = headerCell.attr('fieldName');
        var col = opts.getColumn(fieldName);

        if (!col) {
            return;
        }

        this.fireEvent('columnresize', { fieldName: fieldName, col: col, data: data });

        col.width = data.width;

        /*
        if ($.boxModel == true) {
        col.width = data.width - data.deltaWidth;
        }*/

        opts.el.find("tr[rowType='colRow'] > td[fieldName='" + fieldName + "']").width(col.width);
        $(this).css("width", null);

        var totalWidth;

        if (!opts.alignWidth) {

            if (col.fixedColumn) {
                totalWidth = opts.getTotalWidth(opts.frozenColumns);
                if (this.rowNumbers) {
                    totalWidth = +this.rowNumberWidth;
                }
                if (this.showCheckBox) {
                    totalWidth = +this.showCheckBoxWidth;
                }
                opts.frozenBodyTable.width(totalWidth + "px");

                if (opts.frozenTotalRowTable) {
                    opts.frozenTotalRowTable.width(totalWidth + "px");
                }
            }
            else {
                totalWidth = opts.getTotalWidth(opts.notfrozenColumns);
                opts.bodyTable.width(totalWidth + "px");
                if (opts.totalRowTable) {
                    opts.totalRowTable.width(totalWidth + "px");
                }
            }
            headerCell.parent().parent().parent().width(totalWidth + "px");
        }
    }
}


//汇总行部分
UCML.override(UCML.DataGrid, {
    totalRowBodyStr: '<div class="datagrid-totalrowbody"><table border="0" cellspacing="0" cellpadding="0" style="{tstyle}" class="datagrid-body-table"><tbody>{columnRow}{totalStr}</tbody></table></div>',
    totalRowStr: '<tr class=" {cls}"  style="{style}"  >{cells}</tr>',
    totalCellStr: '<td  style="{style}"  class="datagrid-column-{fieldName}" fieldName="{fieldName}" frozen="{frozen}">' +
    '<div  class="datagrid-cell {cls}" >{value}' +
    '</div></td>',
    /**   
    * @property totalText 
    * @description 统计行显示文本，默认为‘合计’
    * @type String
    */
    totalText: "合计",

    /**   
    * @property totalTextFieldName 
    * @description 统计行显示文本字段，默认为第一个字段
    * @type String
    */
    totalTextFieldName: null,

    /**   
    * @property haveTotalRow 
    * @description 是否显示统计行
    * @type Boolean
    */
    haveTotalRow: false,

    /**   
    * @property ComputeFieldList 
    * @description 设置需要统计的列,用分号分开
    * @type String
    */
    ComputeFieldList: null,

    /**   
    * @property haveBottomTotalRow 
    * @description 统计行是否显示在数据下面
    * @type Boolean
    */
    haveBottomTotalRow: true,

    getTotalText: function () {
        return this.totalText;
    },
    getHaveTotalRow: function () {
        return this.haveTotalRow ? true : false;
    },
    getComputeFieldList: function () {
        return this.ComputeFieldList || "";
    },
    isTotalField: function (fieldName) {
        var bcCol = this.dataTable.getColumn(fieldName);
        var flgComputeFieldList = false;

        var computeFieldList = this.getComputeFieldList();
        var fieldListArray = computeFieldList.split(';');
        if (fieldListArray.length > 0 && fieldListArray[0] != "") {//如果有设置定义统计列
            flgComputeFieldList = true;
        }
        if (getExistComputeField(fieldName)) {
            return true;
        }
        else if (!flgComputeFieldList && this.columnIsNumber(fieldName)) {//没有设置统计字段时继续判断是否是数字类型
            return true;
        }
        return false;

        function getExistComputeField(fieldName) {
            if (flgComputeFieldList) {
                for (var i = 0; i < fieldListArray.length; i++) {
                    if (fieldListArray[i] == fieldName) {
                        return true;
                    }
                }
            }
            return false;
        }
    },
    getTotalRowPosition: function () {
        if (this.haveBottomTotalRow) {
            return "bottom";
        }
        else {
            return "top";
        }
    }, updateTotalRow: function (columns) {
        columns = columns || this.columns;

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            if (this.totalTextFieldName == col.fieldName) {
                this.setTotalRowByFieldName(col.fieldName, this.getTotalText());
            }
            else {
                this.updateTotalRowByFieldName(col.fieldName, col);
            }
        }

    },
    updateTotalRowByFieldName: function (fieldName, col) {
        if (this.totalTextFieldName == fieldName) {
            return;
        }
        else if (col.totalCompute || this.isTotalField(fieldName)) {//是否为统计计算列
            if (col.serverTotalComputeMode) { //如果设置了服务端统计则按照服务端统计
                var value = this.getTotalDataByFieldName(fieldName, col.serverTotalComputeMode);
                this.setTotalRowByFieldName(fieldName, value);
            }
            else {
                switch (col.totalComputeMode) {//集合类型（SUM、AVG、COUNT、MAX、MIN）
                    case "SUM":
                        this.setTotalRowByFieldName(fieldName, this.dataTable.sum(fieldName));
                        break;
                    case "AVG":
                        this.setTotalRowByFieldName(fieldName, this.dataTable.avg(fieldName));
                        break;
                    case "MAX":
                        this.setTotalRowByFieldName(fieldName, this.dataTable.max(fieldName));
                        break;
                    case "MIN":
                        this.setTotalRowByFieldName(fieldName, this.dataTable.min(fieldName));
                        break;
                    case "COUNT":
                        this.setTotalRowByFieldName(fieldName, this.dataTable.count(fieldName));
                        break;
                    default:
                        this.setTotalRowByFieldName(fieldName, this.dataTable.sum(fieldName));
                        break;
                }
            }
        }
    }
    ,
    setTotalRowByFieldName: function (fieldName, value) {
        var table = this.totalRowTable;
        if (this.isFrozenColumn(fieldName)) {
            table = this.frozenTotalRowTable || table;
        }
        table.find("td[fieldName=" + fieldName + "] > .datagrid-cell").html(value);
    }
    ,
    renderTotalRow: function (columns, frozen) {
        var totalRowTpl = new UCML.Template(this.totalRowStr);
        var totalStr = "";
        var cellsStr = [];
        var cls = "";

        if (frozen == true && this.rowNumbers) {
            var totalCellTpl = new UCML.Template(this.totalCellStr);
            var text = this.getTotalText();

            this.totalTextFieldName = "rowNumber";
            cellsStr.add(totalCellTpl.apply({
                value: text
                //    style: "width:" + this.rowNumberWidth + this.colUnit
            }));
        }

        if (frozen == true && this.showCheckBox) {
            var totalCellTpl = new UCML.Template(this.totalCellStr);
            cellsStr.add(totalCellTpl.apply({
                //     style: "width:" + this.showCheckBoxWidth + this.colUnit,
                value: ""
            }));
        }

        for (var j = 0; j < columns.length; j++) {
            var col = columns[j];
            var display = "";
            if (!col.display) {
                display = " display:none;";
            }
            var totalCellTpl = new UCML.Template(this.totalCellStr);
            var text = "&#160;";

            if (!this.totalTextFieldName && j == 0) {//如果没有设置汇总文本列的时候
                this.totalTextFieldName = col.fieldName;
            }

            cellsStr.add(totalCellTpl.apply({
                value: text,
                fieldName: col.fieldName,
                frozen: frozen,
                style: "text-align:" + (col.align || 'left') + ";" + display,
                cls: ""
            }));
        }

        totalStr = totalRowTpl.apply({
            style: "height:" + this.rowHeight + "px;",
            cells: cellsStr.join(''),
            cls: cls
        });
        return totalStr;
    }
});


//剪切板程序部分
UCML.override(UCML.DataGrid, {
    clipboardCellSign: "\t",
    clipboardRowSign: "\r\n",
    setSelelctIndex: function () {
        this.selectMouseUp = null;

        this.selectRowStart = -1;
        this.selectRowEnd = -1;
        this.selectCellStart = -1;
        this.selectCellEnd = -1;

        this.selectRowStart_old = -1;
        this.selectRowEnd_old = -1;
        this.selectCellStart_old = -1;
        this.selectCellEnd_old = -1;
    },
    bindSelectCellEvents: function () {


        if (this.enabledSelect) {
            this.on("beforeloaddata", function () {//数据绑定前
                this.setSelelctIndex();
            }, this);

            /*
            if (this.frozenBodyTable) {
            UCML.on(this.frozenBodyTable, "mousedown", onTableMousedown, this);
            UCML.on(this.frozenBodyTable, "mouseup", onTableMouseup, this);
            UCML.on(this.frozenBodyTable, "mousemove", onTableMousemove, this);

            UCML.on(this.frozenBodyTable, "selectstart", onTableSelectstart, this);
            UCML.on(this.frozenBodyTable, "keydown", onTableKeydown, this);

            }*/

            UCML.on(this.bodyTable, "mousedown", onTableMousedown, this);
            UCML.on(this.bodyTable, "mouseup", onTableMouseup, this);
            UCML.on(this.bodyTable, "mousemove", onTableMousemove, this);
            UCML.on(this.bodyTable, "keydown", onTableKeydown, this);







            UCML.on(this.bodyTable, "selectstart", onTableSelectstart, this);

            function onTableMousedown(el, e) {
                if (e.button < 2) {

                    var target = e.target;
                    var el = $(target);

                    var cell = this.findCell(el);
                    if (cell) {
                        var row = cell.parentNode;

                        this.selectMouseUp = true;

                        this.selectCellStart = cell.cellIndex;
                        this.selectRowStart = row.rowIndex;

                        drawCellBackGround.call(this, -1, -1, -1, -1);
                    }
                }
            }

            function onTableMouseup(el, e) {
                if (e.button < 2 && this.selectRowStart && this.selectCellStart) {
                    var target = e.target;
                    var el = $(target);

                    var cell = this.findCell(el);

                    if (cell) {
                        var row = cell.parentNode;

                        this.selectMouseUp = false;

                        this.selectCellEnd = cell.cellIndex;
                        this.selectRowEnd = row.rowIndex;


                        if (this.selectRowEnd < this.selectRowStart) {
                            var c = this.selectRowStart;
                            this.selectRowStart = this.selectRowEnd;
                            this.selectRowEnd = c;
                        }

                        if (this.selectCellEnd < this.selectCellStart) {
                            var c = this.selectCellStart;
                            this.selectCellStart = this.selectCellEnd;
                            this.selectCellEnd = c;
                        }

                        drawCellBackGround.call(this, this.selectRowStart, this.selectRowEnd, this.selectCellStart, this.selectCellEnd);
                    }
                }
            }

            function onTableMousemove(el, e) {
                if (e.button < 2 && this.selectRowStart && this.selectCellStart) {
                    var _rowStart = this.selectRowStart;
                    var _cellStart = this.selectCellStart;
                    if (_rowStart == -1 && _cellStart == -1) return;
                    if (this.selectMouseUp != true) return;

                    var target = e.target;
                    var el = $(target);

                    var cell = this.findCell(el);
                    if (cell) {
                        var row = cell.parentNode;

                        this.selectMouseUp = false;

                        this.selectCellEnd = cell.cellIndex;
                        this.selectRowEnd = row.rowIndex;

                        if (this.selectRowEnd < this.selectRowStart) {
                            var c = this.selectRowStart;
                            this.selectRowStart = this.selectRowEnd;
                            this.selectRowEnd = c;
                        }

                        if (this.selectCellEnd < this.selectCellStart) {
                            var c = this.selectCellStart;
                            this.selectCellStart = this.selectCellEnd;
                            this.selectCellEnd = c;
                        }

                        drawCellBackGround.call(this, this.selectRowStart, this.selectRowEnd, this.selectCellStart, this.selectCellEnd);
                    }
                }
            }

            function onTableSelectstart() {
                if (this.selectMouseUp)
                    return false;
                else
                    return true;
            }

            function onTableKeydown(el, e) {

                if (e.ctrlKey && e.keyCode == 67) {//复制
                    this.copyRowData(this.selectRowStart, this.selectRowEnd, this.selectCellStart, this.selectCellEnd);
                    return false;
                }
                else if (e.ctrlKey && e.keyCode == 86) {//粘贴

                    this.pasteRowData(this.selectRowStart, this.selectRowEnd, this.selectCellStart, this.selectCellEnd);
                    return false;
                }



            }

            //执行绘制单元格背景
            function drawCellBackGround(srartRows, endRows, srartCells, endCells) {

                if (this.selectRowStart_old != -1 && this.selectRowEnd_old != -1 && this.selectCellStart_old != -1 && this.selectCellEnd_old != -1) {
                    for (i = this.selectRowStart_old; i <= this.selectRowEnd_old; i++) {
                        if (i < this.bodyTable[0].rows.length) {
                            for (j = this.selectCellStart_old; j <= this.selectCellEnd_old; j++) {

                                var td = this.bodyTable[0].rows(i).cells(j);
                                var el = $(td);
                                var fieldName = el.attr("fieldName");
                                if (fieldName) {
                                    el.css("backgroundColor", "");
                                }
                            }
                        }
                    }
                }

                this.selectRowStart_old = srartRows;
                this.selectRowEnd_old = endRows;
                this.selectCellStart_old = srartCells;
                this.selectCellEnd_old = endCells;

                if (srartRows == -1 && srartCells == -1) return;
                if (srartRows == endRows && srartCells == endCells) return;
                for (i = srartRows; i <= endRows; i++) {
                    for (j = srartCells; j <= endCells; j++) {
                        var td = this.bodyTable[0].rows(i).cells(j);
                        var el = $(td);
                        var fieldName = el.attr("fieldName");
                        if (fieldName) {
                            el.css("backgroundColor", "#0000ff");
                        }
                    }
                }

            }

            this.drawCellBackGround = drawCellBackGround;
        }
    },
    //执行粘贴
    pasteRowData: function (srartRows, endRows, srartCells, endCells) {

        srartRows = srartRows || this.selectRowStart;
        endRows = endRows || this.selectRowEnd;
        srartCells = srartCells || this.selectCellStart;
        endCells = endCells || this.selectCellEnd;

        if (srartRows == -1 || endRows == -1 || srartCells == -1 || endCells == -1) {
            alert("请选择单元格");
            return;
        }

        var ss = this.el.find(".toexcel-area");
        ss.focus();
        ss.select();
        var opts = this;
        // 等50毫秒，keyPress事件发生了再去处理数据 
        setTimeout(function () {
            opts.setRowData(ss.val(), srartRows, endRows, srartCells, endCells);
            opts.drawCellBackGround(srartRows, endRows, srartCells, endCells);
        }, 50);


        return;

        /*
        var str = "";
        try {
        str = window.clipboardData.getData("Text");
        }
        catch (e) {
        alert("禁止访问剪切板,错误代码:" + e.message);
        return;

        }
        */

        this.drawCellBackGround(srartRows, endRows, srartCells, endCells);
    },
    setRowData: function (str, srartRows, endRows, srartCells, endCells) {

        var rowList = str.split("\n");
        var index = this.dataTable.recordIndex;
        var rowlength = rowList.length;
        if (index + rowlength > this.dataTable.recordCount) {
            rowlength = this.dataTable.recordCount - index;
        }
        endRows = srartRows + rowlength - 1;
        var irTop = this.insertRecordTop;
        this.insertRecordTop = false;
        var pasteFM = this.pasteRowFormat.split(",")  //Peter ADD 
        var useFormatPaste = false; //Peter ADD 

        if (pasteFM.length > 1)//Peter ADD 
        {
            useFormatPaste = true;
        }

        //Peter ADD 格式化粘贴结束
        for (var m = srartRows; m < srartRows + rowlength; m++) {
            if (rowList[m - srartRows] != null && rowList[m - srartRows] != "") {
                if (m > this.getRowCount()) {
                    this.dataTable.Insert();
                }

                this.dataTable.SetIndexNoEvent(m - 1);
                var cellList = rowList[m - srartRows].split(this.clipboardCellSign);

                var celllength = cellList.length;
                if (srartCells + celllength > this.getRow(m - 1).cells.length) celllength = this.getRow(m - 1).cells.length - srartCells;
                endCells = srartCells + celllength - 1
                if (useFormatPaste)//Peter ADD 
                {
                    //Peter ADD 
                    var pasteFMlength = pasteFM.length;
                    for (var n = 0; n < pasteFMlength; n++) {
                        var colIndex = parseInt(pasteFM[n])
                        if ((colIndex == NaN) || (colIndex > this.getRow(m - 1).cells.length)) {
                            alert("贴入格式定义错误!")
                            return;
                        }
                        if (colIndex > 0) {

                            var td = this.getRow(m - 1).cells[colIndex - 1];
                            var fieldName = $(td).attr("fieldName");
                            if (fieldName != null)
                                this.dataTable.setFieldValue(fieldName, cellList[n]);
                        }

                    }


                }
                else {
                    for (var n = srartCells; n < srartCells + celllength; n++) {
                        var td = this.getRow(m - 1).cells[n];
                        var fieldName = $(td).attr("fieldName");
                        if (fieldName != null)
                            this.dataTable.setFieldValue(fieldName, cellList[n - srartCells]);
                    }
                }


            }
        }
        this.insertRecordTop = irTop;
        this.dataTable.SetIndexNoEvent(index);
    }
    ,
    copyRowData: function (srartRows, endRows, srartCells, endCells) {//复制
        srartRows = srartRows || this.selectRowStart;
        endRows = endRows || this.selectRowEnd;
        srartCells = srartCells || this.selectCellStart;
        endCells = endCells || this.selectCellEnd;

        if (srartRows == -1 || endRows == -1 || srartCells == -1 || endCells == -1) {
            alert("请选择单元格");
            return;
        }

        var str = "";
        var index = this.dataTable.recordIndex;
        for (var i = srartRows; i <= endRows; i++) {
            this.dataTable.SetIndexNoEvent(i - 1); //考虑位置减一
            for (var j = srartCells; j <= endCells; j++) {

                var fieldName = $(this.bodyTable[0].rows(i).cells(j)).attr("fieldName");
                if (fieldName != null) {
                    str += this.dataTable.getFieldValue(fieldName);
                    if (j != endCells) {
                        str += this.clipboardCellSign;

                    }
                }

            }
            if (i != endRows) str += this.clipboardRowSign;
        }


        this.dataTable.SetIndexNoEvent(index);
        try {
            window.clipboardData.setData("Text", str);
        }
        catch (e) {
            alert("禁止访问剪切板,错误代码:" + e.message);
        }
    }
});

UCML.DataGrid.prototype.setCustomColumn = function () {
    for (var i = 0; i < this.columns.length; i++) {
        this.columns[i].display = true;
    }
    this.rebuildGrid();
}

//为单元格赋值
UCML.DataGrid.prototype.setCellValue = function (fieldName, val, OID) {
    if (OID) {
        this.dataTable.LocateOID(OID);
    }
    this.dataTable.setFieldValueBack(fieldName, val);
    var row = this.getRow(this.dataTable.getRecordIndex());
    var cell = this.getCellByName(row, fieldName);
    this.setCellText(cell, val);
}

//兼容版
UCML.extend(UCML.DataGrid, {
    cellDbClick: function () {

    }
});
//列表查询类
UCML.override(UCML.DataGrid, {
    //查询对象
    gridSearchPanel: null,
    //创建查询
    createSearchPanel: function () {
        this.gridSearchPanel = new UCML.GridSearch({ title: "选择", applet: this, condiIsUser: this.condiIsUser });
    },
    //是否用户过滤
    condiIsUser: false,
    //显示查询
    DoSearch: function (isUser) {
        if (isUser && isUser == true) this.condiIsUser = true;
        else this.condiIsUser = false;
        if (this.gridSearchPanel)
            this.gridSearchPanel.open();
        else
            this.createSearchPanel();
    }


});
UCML.DataGrid.CustomButtonPerm = function (vc) {

    var objList = [];
    try {
        this.data = BusinessData[vc.id + 'CustomButtonPerm'];

        if (this.data) {
            for (var i = 0; i < this.data.length; i++) {
                var caption = this.getValue(i, "Caption");
                var enabled = this.getValue(i, "AllowModify");
                var visible = this.getValue(i, "AllowVisible");
                var buttonid = this.getValue(i, "ButtonID");
                var obj = {};
                obj["caption"] = caption;
                obj["enabled"] = enabled;
                obj["visible"] = visible;
                obj["buttonid"] = buttonid;
                objList.add(obj);
            }
        }
    }
    catch (e) {
        this.data = null;
    }

    return objList;
}

UCML.DataGrid.CustomButtonPerm.fields = ["RESP_AppletCustomButtonOID","AllowModify","AllowVisible","Caption","ButtonType","AppletName","ButtonID","UCMLClass_FK","Applet_FK","UCML_RESPONSIBILITY_FK","AppletButton_FK"];

UCML.DataGrid.CustomButtonPerm.prototype.getFieldIndex = function (name) {
    return UCML.DataGrid.CustomButtonPerm.fields.indexOf(name);
}
UCML.DataGrid.CustomButtonPerm.prototype.getValue = function (rowIndex, name) {
    var cellIndex = this.getFieldIndex(name);
    if (this.data && cellIndex > -1) {
        return this.data[rowIndex][cellIndex];
    }
}