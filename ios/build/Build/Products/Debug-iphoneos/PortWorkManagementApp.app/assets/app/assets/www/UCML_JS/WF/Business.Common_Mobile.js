//选人类型---赋值BC---值字段---文本字段
//多选D,单选S
//U-人员	P-岗位	O-部门	G-群组	F-相对岗位
//selType由以上2个字母组合
function openSelectMan_Mobile(selType, bcName, idField, titleField) {
	var showType = selType.substr(0,1);//D or S
	var selectType = selType.substr(1,1);//U or P or O or G or F

	selectManObj = {showType:showType,selectType:selectType,bcName:bcName,idField:idField,titleField:titleField};

	$.ajax({
		url:getServicePath()+"ucml_mobile/select_User_Org_Mobile.ashx?type="+selectType,
		dataType:"jsonp",
		jsonpCallback:"onManLoad_Man",
		success:function(data) {
			
		},
		error:function() {
			alert("fail to load");
		}
	});

}

//服务端读取数据
function onManLoad_Man(data) {
	//data[i].oid	data[i].name	data[i].dept
	rnBridge.exec(sucManLoad, error, "ExchangeService", "ShowListItem", [data, selectManObj.showType]);
}

//选取的数据
function sucManLoad(data) {
	//[oid@name,oid@name,oid@name]
	var strList = data.substring(1, data.length-1);
	var strArr = strList.split(",");
	var idStr = "", nameStr = "";
	for(var i = 0; i < strArr.length; i++) {
		//oid=substring(0,str.indexOf("@"));
		if(nameStr == "") {
			if(selectManObj.showType == "D")
				idStr = "("+selectManObj.selectType+":" + strArr[i].substring(0,strArr[i].indexOf("@")) + ")";
			else
				idStr = strArr[i].substring(0,strArr[i].indexOf("@"));
			nameStr = strArr[i].substr(strArr[i].indexOf("@")+1);
		}
		else {
			if(selectManObj.showType == "D")
				idStr += "," + "("+selectManObj.selectType+":" + strArr[i].substring(0,strArr[i].indexOf("@")) + ")";
			else
				idStr += "," + strArr[i].substring(0,strArr[i].indexOf("@"));
			nameStr += "," + strArr[i].substr(strArr[i].indexOf("@")+1);
		}
	}
	
	eval(selectManObj.bcName + "Base.setFieldValue('" + selectManObj.idField + "', idStr)");
	eval(selectManObj.bcName + "Base.setFieldValue('" + selectManObj.titleField + "', nameStr)");
}

function error() {

}	

//测试android调用js代码
function getMoreByAndr() {
	alert("android");
}