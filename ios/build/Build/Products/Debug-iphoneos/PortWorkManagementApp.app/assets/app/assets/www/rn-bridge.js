(function () {
    if (window.rnBridge) {
        return;
    }

    var rnBridge = function () {
    };

    var commandQueue = [];
    var baseCallbackId = 0;
    var callbacks = [];

    function massageArgsJsToNative(args) {
        // if (!args || utils.typeName(args) != 'Array') {
        //     return args;
        // }
        var ret = [];
        for (var i = 0; i < args.length; i++) {
            ret.push(args[i]);
        }
        args.forEach(function (arg, i) {
            // if (utils.typeName(arg) == 'ArrayBuffer') {
            //     ret.push({
            //         'CDVType': 'ArrayBuffer',
            //         'data': base64.fromArrayBuffer(arg)
            //     });
            // } else {
            //     ret.push(arg);
            // }
            ret.push(arg);
        });
        return ret;
    }

    function awaitPostMessage() {
        var isReactNativePostMessageReady = !!window.originalPostMessage;
        var queue = [];
        var currentPostMessageFn = function store(message) {
            if (queue.length > 100) queue.shift();
            queue.push(message);
        };
        if (!isReactNativePostMessageReady) {
            var originalPostMessage = window.postMessage;
            Object.defineProperty(window, "postMessage", {
                configurable: true,
                enumerable: true,
                get: function () {
                    return currentPostMessageFn;
                },
                set: function (fn) {
                    currentPostMessageFn = fn;
                    isReactNativePostMessageReady = true;
                    setTimeout(sendQueue, 0);
                }
            });
            window.postMessage.toString = function () {
                return String(originalPostMessage);
            };
        }

        function sendQueue() {
            while (queue.length > 0) window.postMessage(queue.shift());
        }
    }

    function rnExec() {
        var successCallback,
            failCallback,
            service,
            action,
            actionArgs,
            splitCommand;
        var callbackId = null;
        if (typeof arguments[0] !== "string") {
            // FORMAT ONE
            successCallback = arguments[0];
            failCallback = arguments[1];
            service = arguments[2];
            action = arguments[3];
            actionArgs = arguments[4];

            // Since we need to maintain backwards compatibility, we have to pass
            // an invalid callbackId even if no callback was provided since plugins
            // will be expecting it. The Cordova.exec() implementation allocates
            // an invalid callbackId and passes it even if no callbacks were given.
            callbackId = "INVALID";
        } else {
            // FORMAT TWO, REMOVED
            // try {
            //     splitCommand = arguments[0].split(".");
            //     action = splitCommand.pop();
            //     service = splitCommand.join(".");
            //     actionArgs = Array.prototype.splice.call(arguments, 1);
            //
            //     console.log('The old format of this exec call has been removed (deprecated since 2.1). Change to: ' +
            //         "cordova.exec(null, null, \"" + service + "\", \"" + action + "\"," + JSON.stringify(actionArgs) + ");"
            //     );
            //     return;
            // } catch (e) {
            // }
        }

        // If actionArgs is not provided, default to an empty array
        actionArgs = actionArgs || [];

        // Register the callbacks and add the callbackId to the positional
        // arguments if given.
        if (successCallback || failCallback) {
            callbackId = service + baseCallbackId++;
            callbacks[callbackId] = {
                success: successCallback,
                fail: failCallback
            };
        }

        actionArgs = massageArgsJsToNative(actionArgs);

        var command = [callbackId, service, action, actionArgs];

        // Stringify and queue the command. We stringify to command now to
        // effectively clone the command arguments in case they are mutated before
        // the command is executed.
        commandQueue.push(JSON.stringify(command));

        window.postMessage(JSON.stringify(command), "*");
    }

    rnBridge.exec = rnExec;

    // events
    function createEvent(type, data) {
        var event = document.createEvent("Events");
        event.initEvent(type, false, false);
        if (data) {
            for (var i in data) {
                if (data.hasOwnProperty(i)) {
                    event[i] = data[i];
                }
            }
        }
        return event;
    }

    function fireDocumentEvent(type, data) {
        if (type === "backbutton") {
            const isFirstPage = window.location.href.indexOf("firstPage");
            window.postMessage(
                JSON.stringify({
                    name: isFirstPage >= 0 ? "navBack" : "historyBack"
                })
            );
            return;
        }

        var evt = createEvent(type, data);
        setTimeout(function () {
            document.dispatchEvent(evt);
        }, 0);
    }

    // 回调
    function onMessage(event) {
        var data = JSON.parse(event.data) || {};

        if (data.callbackId) {
            // 回调
            var callbackId = data.callbackId;
            var result = data.result;
            var error = data.error;

            var callBack = callbacks[callbackId];
            setTimeout(function () {
                if (error) {
                    callBack.fail(error);
                } else {
                    callBack.success(result);
                }
            }, 0);
        } else if (data.nativeEvent) {
            // 事件
            fireDocumentEvent(data.nativeEvent);
        }
    }

    if (window.document.addEventListener) {
        window.document.addEventListener("message", onMessage, false);
    } else if (window.attachEvent) {
        window.attachEvent("onmessage", onMessage);
    }

    window.rnBridge = rnBridge;
})();