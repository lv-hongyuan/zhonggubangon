﻿
UCML.BCLinkBox = function (id) {
    this.queryFieldName = "";
    this.quickQuery = false;
    this.dropDownMode = true;
    this.dropDownWidth;
    this.dropDownHeight;
    this.BCName;
    this.caption = "请选择";
    this.BCRunMode = 1;
    this.srcFieldName = "";
    this.destFieldName = "";
    this.condiFieldName = "";
    this.constValue = "";
    this.targetTable;
    this.urlFuncName = "";

    UCML.BCLinkBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.BCLinkBox, UCML.Combo, {
    ctype: "UCML.BCLinkBox",
    showPanel: function () {
        startBCLink(this.dom);
    },
    setProperties: function () {

        UCML.BCLinkBox.superclass.setProperties.call(this);
        this.dropDownHeight = this.getAttribute("DropDownHeight") || this.dropDownHeight;
        this.dropDownWidth = this.getAttribute("DropDownWidth") || this.dropDownWidth;
        this.BCName = this.getAttribute("DropDownAppletName") || this.BCName;
        this.targetTable = BusinessObject.getDataTableByBCName(this.getAttribute
("targetTable")) || this.targetTable;
        this.srcFieldName = this.getAttribute("SrcFieldList") || this.srcFieldName;
        this.destFieldName = this.getAttribute("DestFieldList") || this.destFieldName;
        this.dataValueField = this.getAttribute("DataValueField") || this.dataValueField;
    },
    onRender: function () {
        UCML.BCLinkBox.superclass.onRender.call(this);
    }
    ,
    onQuickQuery: function (el, e) {
    /*
        if (e.keyCode == 13) {//回车
            var input = this.dom;
            var fieldName = this.getAttribute("dataFld");
            var columnInfo = this.dataTable.getColumn(fieldName);

            //启动BC
            if (columnInfo.objBCLinkColl != null) {
                for (var i = 0; i < columnInfo.objBCLinkColl.length; i++) {
                    if (columnInfo.objBCLinkColl[i].condiFieldName == "" || this.dataTable.getFieldValue(columnInfo.objBCLinkColl[i].condiFieldName) == columnInfo.objBCLinkColl[i].constValue) {
                        //根据BClink找到业务,执行Web查询服务,返回结果值,如果为真,取得值,否则弹出查询框
                        //   var theBPO = eval(theDataTable.BPOName + "BPO");
                        if (columnInfo.objBCLinkColl[i].isQuickQuery == true) {
                            columnInfo.objBCLinkColl[i].queryValue = input.value;
                            input.value = this.dataTable.getFieldValue(columnInfo.fieldName);
                        }
                        columnInfo.objBCLinkColl[i].quickIn = true;
                        //  if (theQueryEdit == true)
                        //      theDataTable.QueryChoiced = true;
                        this.showPanel();

                        break;
                    }
                }
            }
        }
        */
    }
    ,

    arrowClick: function (el) {
        this.showPanel();
    },
    onFocus: function () {

    }
    , onKeyDown: function (el, e) {
        this.onQuickQuery(el, e);
        return false;
    },
    onKeyUp: function () {
        return false;

    },
    close: function () {
        var bcLinKBPOName = this.BCName + 'BPO';

        var linkWindow = UCML.get(bcLinKBPOName);
        if (linkWindow) {
            linkWindow.close();
        }
    }
    ,
    setAppletObject: function (val) {

        //  this.onQuickQuery();
        this.AppletObject = val;

       
    },
    bindEvents: function () {
        var opts = this;
        var input = this.combo.find('.combo-text');
        var arrow = this.combo.find('.combo-arrow');

        if (!this.disabled) {
            //先解除绑定事件，以免重复绑定
            this.combo.unbind('.BCLink');
            arrow.unbind('.BCLink');
            this.un('keydown', this.onQuickQuery);
            UCML.un(input, 'click', this.onFocus);
            UCML.un(input, 'focus', this.onFocus);

            this.on('keydown', this.onKeyDown, this);
            this.on('keyup', this.onKeyUp, this);

            arrow.bind('click.BCLink', function () {
                opts.arrowClick(this);
            }).bind('mouseenter.BCLink', function () {
                $(this).addClass('combo-arrow-hover');
            }).bind('mouseleave.BCLink', function () {
                $(this).removeClass('combo-arrow-hover');
            });

            UCML.on(input, 'click', this.onFocus, this);
            UCML.on(input, 'focus', this.onFocus, this);

        }
        else {

            this.combo.unbind('.BCLink');
            arrow.unbind('.BCLink');
            this.un('keydown', this.onQuickQuery);
            UCML.un(input, 'click', this.onFocus);
            UCML.un(input, 'focus', this.onFocus);
        }
    }
});

UCML.reg("UCML.BCLinkBox", UCML.BCLinkBox);


UCML.BCLinkBoxForGrid = function (id) {
    this.queryFieldName = "";
    this.quickQuery = false;
    this.dropDownMode = true;
    this.dropDownWidth;
    this.dropDownHeight;
    this.BCName;
    this.caption = "请选择";
    this.BCRunMode = 1;
    this.srcFieldName = "";
    this.destFieldName = "";
    this.condiFieldName = "";
    this.constValue = "";
    this.targetTable;
    this.urlFuncName = "";
    this.objBCLoadCondiColl = [];
    UCML.BCLinkBoxForGrid.superclass.constructor.call(this, id);
}

UCML.extend(UCML.BCLinkBoxForGrid, UCML.BCLinkBox, {
    ctype: "UCML.BCLinkBoxForGrid",
    setAppletObject: function (val) {
        UCML.BCLinkBoxForGrid.superclass.setAppletObject.call(this,val);
        if (this.AppletObject) {
            this.AppletObject.on("loaddata", function () {

                this.selectUpRow();
            });
        }
    },
    showPanel: function () {

        startBCLink(this);
        var opts = this;
        if (this.showWindow) {
            this.showWindow.on("close", function () {

                opts.fireEvent("blur");
            });
        }
    },
    onQuickQuery: function () {
        this.queryValue = this.dom.value;
        this.queryValue = this.queryValue.replace("'", "");
        if (this.queryOldValue != this.queryValue) {
            this.queryOldValue = this.queryValue;

            var s = this.dataValueField.split(";")
            var sqlcondi = "";
            var TableName = this.AppletObject.getRootTable().TableName;
            for (var i = 0; i < s.length; i++) {
                if (sqlcondi != "") sqlcondi += " OR ";
                sqlcondi += TableName + "." + s[i] + " LIKE '%" + this.queryValue + "%' ";
            }
            this.AppletObject.condiQuery("", "", "", sqlcondi, 0);
        }
    },
    //传值操作，根据定义的值列表和DataValue和FieldName
    setRelaValue: function () {

        var s = this.dataValueField.split(";")
        this.dataTable.setActorData(this.AppletObject.getRootTable(), this.srcFieldName, this.destFieldName);
        /*
        this.setValue(this.AppletObject.getRootTable().getFieldValue(s[0]));
        this.dataTable.setFieldValue(this.fieldName, this.AppletObject.getRootTable().getFieldValue(s[0]));*/
    },
    onFocus: function () {

        if (this.showWindow) {
            if (this.showWindow.closed) {
                this.showWindow.open();
            }
        }
        else {
            this.showPanel();
        }
    },
    onKeyDown: function (el, e) {
        if (this.showWindow && this.showWindow.closed) {
            this.showWindow.open();
            return;
        }

        var vcObj = UCML.get(this.AppletObject.BPOName);
        if (e.keyCode == 38)//up
        {
            vcObj.selectUpRow();
        }
        else if (e.keyCode == 40)//down
        {
            vcObj.selectNextRow();

        } else if (e.keyCode == 13 && vcObj.activeRow) {//回车
            this.setRelaValue();
            this.showWindow.close();
        }
        else if (e.keyCode == 27) {
            this.showWindow.close();
        }
        return false;
    },
    onKeyUp: function (el, e) {

        if (e.keyCode == 40 || e.keyCode == 38 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 34 || e.keyCode == 33 |
	     e.keyCode == 27 || e.keyCode == 16 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 36 || e.keyCode == 20 ||
	     e.keyCode == 18 || e.keyCode == 145 || e.keyCode == 19 || e.keyCode == 144 || e.keyCode == 13 || e.keyCode == 9) return;

        if (e.keyCode == 13) {//回车

        } else {
            this.onQuickQuery();
        }

        return false;
    }
});

UCML.reg("UCML.BCLinkBoxForGrid", UCML.BCLinkBoxForGrid);

//选人文本框
UCML.SelectManBox = function (id) {
    UCML.SelectManBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.SelectManBox, UCML.Combo, {
    ctype: "UCML.SelectManBox",
    readOnly: true,
    otype: "1",
    destFieldName: "",
    dataValueField: "",
    BCName: "",
    showPanel: function () {
        if (this.selectWindow) {
            this.selectWindow.open();
        }
        else {
            this.selectWindow = openSelectMan(this.otype, 1, this.BCName, this.dataValueField, this.dataFld, { close: function () {
                UCML.OpenShowWindow.superclass.close.call(this);
            }
            });
        }
    },
    setProperties: function () {
        UCML.SelectManBox.superclass.setProperties.call(this);

        this.otype = this.getAttribute("otype") || this.otype; //选人类型字段
        this.dataFld = this.getAttribute("dataFld") || this.dataFld; //选人存名称的字段
        this.dataValueField = this.getAttribute("DataValueField") || this.dataValueField; //选人存值的字段
        if (!this.dataValueField) {
            this.dataValueField = this.dataFld;
        }
        this.BCName = this.getAttribute("BCName") || this.BCName;
    },
    onRender: function () {
        UCML.SelectManBox.superclass.onRender.call(this);

        if (this.readOnly) {
            this.setAttribute("readOnly", true);
        }
    },
    arrowClick: function (el) {
        this.showPanel();
    },
    onFocus: function () {
        this.showPanel();
    }
    ,
    close: function () {
        if (this.selectWindow) {
            this.selectWindow.close();
        }
    }
    ,
    bindEvents: function () {
        var opts = this;
        var input = this.combo.find('.combo-text');
        var arrow = this.combo.find('.combo-arrow');

        if (!this.disabled) {


            arrow.bind('click.BCLink', function () {
                opts.arrowClick(this);
            }).bind('mouseenter.BCLink', function () {
                $(this).addClass('combo-arrow-hover');
            }).bind('mouseleave.BCLink', function () {
                $(this).removeClass('combo-arrow-hover');
            });

            UCML.on(input, 'click', this.onFocus, this);
            UCML.on(input, 'focus', this.onFocus, this);

        }
        else {

            this.combo.unbind('.BCLink');
            arrow.unbind('.BCLink');

            UCML.un(input, 'click', this.onFocus);
            UCML.un(input, 'focus', this.onFocus);
        }
    }
});

UCML.reg("UCML.SelectManBox", UCML.SelectManBox);

//代码取值控件，2014.07.16 张嘉加

UCML.WebCodeInput = function (id) {
    this.queryFieldName = "";
    this.quickQuery = false;
    this.dropDownMode = true;
    this.dropDownWidth;
    this.dropDownHeight;
    this.BCName;
    this.caption = "请选择";
    this.BCRunMode = 1;
    this.srcFieldName = "";
    this.destFieldName = "";
    this.condiFieldName = "";
    this.constValue = "";
    this.targetTable;
    this.urlFuncName = "";
    this.objBCLoadCondiColl = [];
    this.DataTextField = "";
    this.JOINDataField = "";
    UCML.WebCodeInput.superclass.constructor.call(this, id);
}

UCML.extend(UCML.WebCodeInput, UCML.BCLinkBox, {
    ctype: "UCML.WebCodeInput",

    setAppletObject: function (val) {

        UCML.WebCodeInput.superclass.setAppletObject.call(this, val);
        if (this.AppletObject) {
            this.AppletObject.on("loaddata", function () {
                this.selectUpRow();
            });
        }
    },
    //控制弹出窗口

    showPanel: function () {
        //控制链接业务组建
        startBCLink(this);
        var opts = this;
        if (this.showWindow) {
            this.showWindow.on("close", function () {

                opts.fireEvent("blur");
            });
        }

    },

    onRender: function () {
        var span = $('<span class="combo"></span>').insertAfter(this.el);
        var table = $('<table width="100%" style="width:100%; table-layout:fixed" border=0 cellSpacing=0 cellPadding=0><tr><td ></td><td  width="20"><span class="combo-arrow"></span></td><td class="BCLinkBox-text" width="30%"></td></tr></table>').appendTo(span);
        var input = this.el.appendTo(table.find("td")[0]);
        input.addClass("combo-text");
        var input2 = $('<input id="' + this.id + 'textvalue" ctype = "UCML.TextBox"  style="width:99%;height:100%;border-width:0px;border-style:none;" dataFld="' + this.JOINDataField + '" />').appendTo(table.find("td")[2]);
        input2.addClass("combo-textvalue");
        $('<input type="hidden" class="combo-value">').appendTo(span);

        var name = this.el.attr('name');
        if (name) {
            span.find('input.combo-value').attr('name', name);
            this.el.removeAttr('name').attr('comboName', name);
        }
        input.attr('autocomplete', 'off');

        if (!this.editable) {
            input.attr('readonly', this.editable);
        }

        if (!this.panel) {


            var panel = new UCML.Panel({ el: $('<div ></div>').appendTo('body'), doSize: true, closed: true,
                style: { position: 'absolute' }, width: this.panelWidth, height: this.panelHeight, isSetProperties: false,
                bodyCls: "combo-panel"
            });
            this.panel = panel;
            //解决输入慢性能问题	
            //  $("<iframe app=\"about:blank\"  style=\"position:absolute; visibility:inherit; top:0px; left:0px; width:" + this.panelWidth + "; height:" + this.panelHeight + "; z-index:-1; filter='progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)';\"></iframe>").appendTo(this.panel.body);
            this.panel.el.bind('mousedown.combo', function (e) {
                return false;
            });
        }
        this.combo = span;

        UCML.on(this.el, "blur", function () {
            this.setValue(this.dom.value, false);

        }, this);

    },

    setValue: function (value, trigger) {

        if (trigger !== false) {
            this.fireEvent("setValue", value);
        }
        this.value = value;
        this.setText(value);
        this.setValues([value]);
        this.setTextValue(value);
    },
    setValues: function (values) {
        var oldValues = this.getValues();
        this.combo.find('input.combo-value').remove();
        var name = this.el.attr('comboName');
        for (var i = 0; i < values.length; i++) {
            var input = $('<input type="hidden" class="combo-value">').appendTo(this.combo);
            if (name) input.attr('name', name);
            input.val(values[i]);
        }

        var tmp = [];
        for (var i = 0; i < oldValues.length; i++) {
            tmp[i] = oldValues[i];
        }
        var aa = [];
        for (var i = 0; i < values.length; i++) {
            for (var j = 0; j < tmp.length; j++) {
                if (values[i] == tmp[j]) {
                    aa.push(values[i]);
                    tmp.splice(j, 1);
                    break;
                }
            }
        }

        if (aa.length != values.length || values.length != oldValues.length) {
            if (this.multiple) {
                this.onChange.call(this, values, oldValues);
            } else {
                this.onChange.call(this, values[0], oldValues[0]);
            }
        }


    },
    setTextValue: function (value) {
        var txtvalue = "";
        if (!this.AppletObject) {
            txtvalue = this.dataTable.getFieldValue(this.getAttribute("JOINDataField"));
        }
        else {
		if(value == this.AppletObject.getRootTable().getFieldValue(this.getAttribute("DataValueField")))
		{
            txtvalue = this.AppletObject.getRootTable().getFieldValue(this.getAttribute("DataTextField"));
		}
		else
		{
		    txtvalue = this.dataTable.getFieldValue(this.getAttribute("JOINDataField"));
			$("#" + this.id)[0].value = this.dataTable.getFieldValue(this.getAttribute("dataFld"));
		}
        }
        $("#" + this.id + "textvalue")[0].value = txtvalue;
    },
    onQuickQuery: function () {
        this.queryValue = this.dom.value;
        this.queryValue = this.queryValue.replace("'", "");
        if (this.queryOldValue != this.queryValue) {
            this.queryOldValue = this.queryValue;

            var s = this.dataValueField.split(";")
            var sqlcondi = "";
            var TableName = this.AppletObject.getRootTable().TableName;
            for (var i = 0; i < s.length; i++) {
                if (sqlcondi != "") sqlcondi += " OR ";
                sqlcondi += TableName + "." + s[i] + " LIKE '%" + this.queryValue + "%' ";
            }
            this.AppletObject.condiQuery("", "", "", sqlcondi, 0);
        }

    },
    //传值操作，根据定义的值列表和DataValue和FieldName
    setRelaValue: function () {
        var s = this.dataValueField.split(";")
        this.dataTable.setActorData(this.AppletObject.getRootTable(), this.srcFieldName, this.destFieldName);
    },

    onFocus: function () {

        if (this.showWindow) {
            if (this.showWindow.closed) {
                this.showWindow.open();
            }
        }
        else {
            this.showPanel();
        }
    },
    onKeyDown: function (el, e) {
        if (this.showWindow && this.showWindow.closed) {
            this.showWindow.open();
            return;
        }

        var vcObj = UCML.get(this.AppletObject.BPOName);
        if (e.keyCode == 38)//up
        {
            vcObj.selectUpRow();
        }
        else if (e.keyCode == 40)//down
        {
            vcObj.selectNextRow();

        } else if (e.keyCode == 13 && vcObj.activeRow) {//回车
            this.setRelaValue();
            this.showWindow.close();
        }
        else if (e.keyCode == 27) {
            this.showWindow.close();
        }
        return false;
    },
    onKeyUp: function (el, e) {

        if (e.keyCode == 40 || e.keyCode == 38 || e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 34 || e.keyCode == 33 |
	     e.keyCode == 27 || e.keyCode == 16 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 36 || e.keyCode == 20 ||
	     e.keyCode == 18 || e.keyCode == 145 || e.keyCode == 19 || e.keyCode == 144 || e.keyCode == 13 || e.keyCode == 9) return;

        if (e.keyCode == 13) {//回车

        } else {
            this.onQuickQuery();
        }
        //this.setRelaValue();
        return false;
    }
});

UCML.reg("UCML.WebCodeInput", UCML.WebCodeInput);
