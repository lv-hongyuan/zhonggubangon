﻿UCML.namespace("UCML.Portal");

UCML.Portal.ModuleView = function (id) {
    this.draggable = false;
    this.modal = false;
    UCML.Portal.ModuleView.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Portal.ModuleView, UCML.Panel, {
    ctype: "UCML.Portal.ModuleView",
    closable: false,
    alignWidth: true,
    collapsible: true,
    iconCls: "Portal-ModuleView-iconCls",
    setProperties: function () {
        UCML.Portal.ModuleView.superclass.setProperties.call(this);

        this.doPage = (this.el.attr('DoPage') ? this.el.attr('DoPage').toLowerCase() == 'true' : this.doPage); // 是否翻页

        this.haveMore = (this.el.attr('DoPage') ? this.el.attr('HaveMore').toLowerCase() == 'true' : this.haveMore);  // 是否翻页

        this.appletId = this.el.attr("AppletlPanel"); //vc容器
    },
    onRender: function () {
        this.headerCls = "Portal-ModuleView-header " + this.headerCls;
        this.bodyCls = "Portal-ModuleView-body " + this.bodyCls;
        this.cls = "Portal-ModuleView " + this.cls;

        UCML.Portal.ModuleView.superclass.onRender.call(this);

        this.renderDoPage();
    },
    getApplet: function () {

        if (this.appletId) {
            return window[this.appletId]
        }
        else {
            return null;
        }
    }
    ,
    loadPrevPage: function () {
        var applet = this.getApplet();
        if (applet) {
            applet.dataTable.LoadPrevPage();
        }
    }
    ,
    loadMoreData: function () {
        var applet = this.getApplet();
        if (applet) {
            applet.dataTable.LoadMoreData();
        }
    }
    ,
    moreOnClick: function () {
        var applet = this.getApplet();
        if (applet && applet.MoreOnClick) {
            applet.MoreOnClick();
        }
    },
    renderDoPage: function () {
        if (!this.doPage && !this.haveMore) {
            return;
        }
        var dopageStr = "<table class=Portal-ModuleView-doPage width=\"100%\"><tbody><tr>";
        if (this.doPage) {
            dopageStr += "<td noWrap=\"\" align=\"left\"><a href=\"javascript:" + this.id + ".loadPrevPage()\">【上页】</a></td>";
            dopageStr += "<td noWrap=\"\" align=\"left\"><a href=\"javascript:" + this.id + ".loadMoreData()\">【下页】</a></td>";
            dopageStr += "<td width=\"100%\" align=\"right\">";
        }
        if (this.haveMore) {
            dopageStr += "<a href=\"javascript:" + this.id + ".moreOnClick()\">【更多】</a>";
        }
        dopageStr += "</td></tr></tbody></table>";
        this.body.append(dopageStr);
    }
});