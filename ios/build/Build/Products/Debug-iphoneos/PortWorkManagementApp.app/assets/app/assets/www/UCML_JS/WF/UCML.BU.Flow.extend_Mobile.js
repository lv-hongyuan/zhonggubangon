﻿
/*************************************
* UCML.BU.Flow.extend.js 
* Date: 2013-01-15 
* author sunyi
*  
* 扩展UCML.BusinessUnit类中方法，添加打开启动下一步，通用执行下一步，同环节执行等
*                                 JS只在工作流启动页面，执行页面引入即可
*
*2013-06-20  下一步自由流程判断
***********************************
*/



/**   
* @method callFntoString 
* @description 对象转字符串处理
* @param {string||function||object||undefined} callbackFn 函数名对象
* @return {string} 函数名   
*/

UCML.BusinessUnit.prototype.callFntoString = function (callbackFn) {
    var retstr = "";
    if (typeof callbackFn == "function") { //函数转换为函数名字符串
        var a = callbackFn.toString();
        retstr = a.substring(0, a.indexOf("(")).replace("function", "").replace(/(^\s*)|(\s*$)/g, "");
    }
    if (typeof callbackFn == "string") {
        retstr = callbackFn;
    }
    return retstr;
}

/**   
* @method openExeCurTask 
* @description 打开启动流程通用下一步页面
* @param {string} businessOID 业务的主键值
* @param {string} flowid 要启动的流程模型ID 
* @param {string} flagTable 业务表名,此业务表需要存在，并具有StartFlowFlag字段和InstanceID字段        
* @param {string} flowData 需要在启动时，回写到流程数据表中的数据,
*                           格式为 key:value@@key:value@@key:value ,其中key为流程数据表中字段名，value为值
* @param {string} callbackfn 当流程启动成功后，需要回调的函数的函数名，此函数一般做关闭启动页面，刷新数据等作用    
* @param {number} butRule 选人按钮规则，//1:无论什么情况都出现选人按钮
*                                        //2:无默认人时出现选人按钮
*                                       //3:无论什么情况都不出现选人按钮  
* @param {number||null} openMode 打开模式，1：为层弹出，2为window.open打开
*/
UCML.BusinessUnit.prototype.openStartTask = function (businessOID, flowid, flagTable, flowData, callbackFn, butRule, openMode) {
    butRule = butRule == undefined ? 1 : butRule;
    openMode = openMode == undefined ? 1 : openMode;
    var strUrl = "UCMLCommon/WF/BPO_WF_SendTaskStartFlow.aspx?BusiKeyOID=" + businessOID + "&callbackFn=" + this.callFntoString(callbackFn) + "&FlowID=" + flowid + "&FlowData=" + flowData + "&FlagTable=" + flagTable + "&Rule=" + butRule;

    var title = "启动流程并选择流向";
    return openFlowWindow(strUrl, title, openMode);
}
/**   
* @method openSendTask 
* @description 打开流程执行通用下一步页面
* @param {string} taskid 任务id
* @param {string} callbackfn 当流程执行成功后，需要回调的函数的函数名，此函数一般做关闭启动页面，刷新数据等作用
* @param {number} butRule 选人按钮规则，//1:无论什么情况都出现选人按钮
*                                        //2:无默认人时出现选人按钮
*                                       //3:无论什么情况都不出现选人按钮
* @param {number||null} openMode 打开模式，1：为层弹出，2为window.open打开        
*/
UCML.BusinessUnit.prototype.openSendTask = function (taskid, callbackFn, butRule, openMode) {
    butRule = butRule == undefined ? 1 : butRule;
    var strUrl = window["UCMLLocalResourcePath"]+"MBPO/BPO_WF_SendTaskFlow_Mobile.HTML?TaskID=" + taskid + "&callbackFn=" + this.callFntoString(callbackFn) + "&Rule=" + butRule;
    var title = "选择下一步";

    return openFlowWindow(strUrl, title, openMode);
}
/**   
* @method openExeCurTask 
* @description 打开流程执行通用当前页面处理
* @param {string} taskid 任务id
* @param {string} actionid 执行动作
* @param {string} callbackfn 当流程执行成功后，需要回调的函数的函数名，此函数一般做关闭启动页面，刷新数据等作用
* @param {number||null} openMode 打开模式，1：为层弹出，2为window.open打开      
*/
UCML.BusinessUnit.prototype.openExeCurTask = function (taskid, actionid, callbackFn, openMode) {
    //var strUrl = 'UCMLCommon/WF/BPO_WF_ExeCurTask.aspx?strBusinesType=' + actionid + '&TaskID=' + taskid + '&callbackFn=' + this.callFntoString(callbackFn);
    var strUrl = window["UCMLLocalResourcePath"]+'MBPO/BPO_WF_ExeCurTask_Mobile.HTML?strBusinesType=' + actionid + '&TaskID=' + taskid + '&callbackFn=' + this.callFntoString(callbackFn);
    var title = "当前任务处理";
    return openFlowWindow(strUrl, title, openMode);
}

/**   
* @method openFlowTrace 
* @description 打开流程跟踪图
* @param {string} taskid 任务id
* @param {string} instanceID 流程实例id   
*/
UCML.BusinessUnit.prototype.openFlowTrace = function (taskid, instanceID, openMode) {
    //var strUrl = getServicePath() + "UCMLCommon/WF/BPO_WF_FlowTrace.aspx?TaskID=" + taskid + "&InstanceID=" + instanceID;
	var strUrl = UCMLLocalResourcePath+"UCMLCommon/WF/BPO_WF_FlowTracetest.HTML?TaskID="+taskid+"&InstanceID="+instanceID;
    var title = "流程跟踪";
    return openFlowWindow(strUrl, title, openMode);
}

UCML.BusinessUnit.prototype.openFlowWindow = function (strUrl, title, openMode) {debugger;
    openMode = openMode == undefined ? 1 : openMode;
    if (openMode == 1) {
        var w = new OpenWindow(strUrl, title, openMode);
        return w;
    } else {
        window.open(strUrl, title, "location=no;menubar=no;toolbar=no;status=no;directories=no;scrollbars=auto;resizable=no;width=600px;height=600px");
    }
}

/**   
* @method isDy
* @description 判断是否是自由流程
* @param {string} flowID 流程ID
* @param {string} callbackfn 当流程执行成功后，需要回调的函数的函数名，此函数一般做关闭启动页面，刷新数据等作用
* @param {number||null} openMode 打开模式，1：为层弹出，2为window.open打开        
*/
UCML.BusinessUnit.prototype.isDy = function (flowID) {
    var str = flowID.substring(0, 2);
    if (str == "Dy") {
        return true;
    }
    else {
        return false;
    }
}

/**   
* @method openSendTask_Dy 
* @description 打开自由流程下一步页面
* @param {string} taskid 流程ID
* @param {string} activityID 流程节点ID
* @param {string} callbackfn 当流程执行成功后，需要回调的函数的函数名，此函数一般做关闭启动页面，刷新数据等作用
* @param {number||null} openMode 打开模式，1：为层弹出，2为window.open打开        
*/
UCML.BusinessUnit.prototype.openSendTask_Dy = function (flowID, activityID) {
    var strUrl = "UCMLCommon/WF/BussModel/BPO_SendTask_Dy.aspx?FlowID=" + flowID + "&ActivityID=" + activityID;
    var title = "选择下一步";
    new OpenWindow(strUrl, title);
}

UCML.BusinessUnit.prototype.flowToAction = function (actionID, callbackFn) {
    callbackFn = callbackFn || UCML.emptyFn;
    var exeTo = false;
    var callbackTo = false;
    var taskID = getURLParameters('TaskID');
    var instanceID = getURLParameters('InstanceID');
    var flowID = getURLParameters('FlowID');
    var activityID = getURLParameters('ActivityID');

    if (window["on_" + actionID] && window["on_" + actionID]() == false) {
        return;
    }

    switch (actionID) {
        case "WF_HUIQIN": //会签
            exeTo = true;
            break;
        case "WF_XIEBAN": //协办
            exeTo = true;
            break;
        case "WF_SIGN": //加签
            exeTo = true;
            break;
        case "WF_TRACE": //流程跟踪
            openFlowTrace(taskID, instanceID)
            break;
        case "WF_READ": //知会/传阅
            exeTo = true;
            break;
        case "WF_TaskReturn": //退回任务
            callbackTo = true;
            CallTaskReturn();
            break;
        case "WF_BACK": //任意回退
            openFlowWindow(getServicePath() + "UCMLCommon/WF/BPO_WF_TaskReturnTo.aspx?TaskID=" + taskID + "&InstanceID=" + instanceID + "&ActivityID=" + activityID, "流程回退");
            break;
        case "WF_ABORT": //终止
            callbackTo = true;
            CallAbortFlowInst();
            break;
        case "BU_SAVE": //保存
            callbackTo = true;
            BusinessSubmit();
            break;
        case "WF_FINISHTASK": //完成任务
            callbackTo = true;
            if (window.confirm("确认完成任务吗?")) {
                var DeltaXml = this.getChangeXML();
                Call__FinishMyTask(DeltaXml, BC_WF_AssignTask_IdeaBase.getFieldValue("IdeaCode"));
            }
            break;
        case "WF_SIGNIDEA": //意见内容
            alert("无效的操作");
            break;
        case "WF_IDEAVIEW": //浏览意见
            openFlowWindow(window["UCMLLocalResourcePath"]+"MBPO/BPO_WF_File_Suggest_View_Mobile.HTML?InstanceID=" + instanceID, "浏览意见");
            break;
        case "WF_UPLOAD": //上传附件
 +           alert("无效的操作");
            break;
        case "WF_IDEATYPE": //意见决策
            alert("无效的操作");
            break;
        case "WF_NEXTSTEP": //下一步
            if (this.isChangeData()) {
                alert("请先提交数据，再继续下一步");
                return;
            }
            //判断是否是自由流程
            if (isDy(flowID)) {
                //var w = openSendTask_Dy(flowID, activityID);
				alert("自由流程，不支持手机浏览!");
            }
            else {
                window["openSendTask_Mobile"] = openSendTask(taskID, callbackFn);
                listenAfterNextTask();
            }

            break;
        case "WF_NEXTSTEP2": //下一步 无默认人时出现选人按钮 读取预设人，如果没有预设人则可以进行选人
            if (this.isChangeData()) {
                alert("请先提交数据，再继续下一步");
                return;
            }
            window["openSendTask_Mobile"] = openSendTask(taskID, callbackFn, 2);
            listenAfterNextTask();
            break;
        case "WF_NEXTSTEP3": //下一步 无论什么情况都不出现选人按钮 下一步 只读取预设人，不能选人
            if (this.isChangeData()) {
                alert("请先提交数据，再继续下一步");
                return;
            }
            window["openSendTask_Mobile"]= openSendTask(taskID, callbackFn, 3);
            listenAfterNextTask();
            break;
        case "WF_SIGNTO": //转签
            exeTo = true;
            break;
        case "WF_PAUSE": //挂起
            callbackTo = true;
            CallPauseFlowInst();
            break;
        case "WF_RESUME": //恢复
            callbackTo = true;
            CallResumeFlowInst();
            break;
    }

    if (exeTo) {
        var COMMAND = getURLParameters('COMMAND');

        if (COMMAND != 'startFlow') {
            window["openExeCurTask_Window"] = openExeCurTask(taskID, actionID, callbackFn);
            listenAfterFinishTask();
            return;
        }
    }

}

/**
* @method listenAfterNextTask
* @description 下一步完成后动作，以及相关事件
*/
UCML.BusinessUnit.prototype.listenAfterNextTask = function () {
    //下一步页面的确定按钮侦听事件
    pm.bind("sendTaskSuccess", function () {
        if(window["openSendTask_Mobile"]) {
            window.location = "BPO_TaskWait.HTML";
            window["openSendTask_Mobile"].close();
        }
    });

    //下一步页面的返回按钮侦听事件
    pm.bind("sendTaskClose", function () {
        if(window["openSendTask_Mobile"]) {
            window["openSendTask_Mobile"].close();
        }
    });
}

/**
* @method listenAfterFinishTask
* @description 流程动作执行完成后的动作，关闭层、页面跳转等
*/
UCML.BusinessUnit.prototype.listenAfterFinishTask = function () {
    //返回按钮侦听事件
    pm.bind("exeCurTaskClose", function () {
        if(window["openExeCurTask_Window"]) {
            window["openExeCurTask_Window"].close();
        }
    });

    //确定完成执行成功侦听事件
    pm.bind("exeCurTaskSuccess", function () {
        if(window["openExeCurTask_Window"]) {
            window.location = "BPO_TaskWait.HTML";
            window["openExeCurTask_Window"].close();
        }
    });
}

/**
* @method openSelectMan_Mobile
* @description 手机版弹出选人，并监听回调事件
* @params:{selType：选人类型，openMode：打开方式暂时填1，bcName：数据来源bc，idField：回填id字段，titleField：回填名称字段}
*/
UCML.BusinessUnit.prototype.openSelectMan_Mobile = function (selType, openMode, bcName, idField, titleField)  {
    var ids = eval(bcName+"Base.getFieldValue('"+idField+"')");
    var url = UCMLLocalResourcePath+"selectMan_Mobile.HTML?type="+selType;

    if(selType % 2) { //奇数--1,3,5,7,9多选,需要处理idField值
        if(ids && ids != "00000000-0000-0000-0000-000000000000") {
            var tempArr = ids.split(",");
            for(var i=0; i<tempArr.length; i++) {
                tempArr[i] = tempArr[i].substr(3, 36);
            }
            ids = tempArr.join(",");
        }
    }else { //偶数--2,4,6,8,10单选,不需要处理idField值

    }
    url += "&idField="+ids;
    var top = $(document).scrollTop();
    var popupContent = $('<div id="popupSelect" class="selectMan_Mobile" style="top:'+top+'px" data-role="popup" data-overlay-theme="b"><iframe width="100%" height="800" app="'+url+'" id="selectManIframe"></iframe></div>');
    $("body").append(popupContent);
    //$("#BPO_LeaveAppZGWF_MobileForm").after(popupContent);

    //监听选人层确定按钮
    pm.bind("selectManEntry", function (data) {
        $("#popupSelect").remove();//隐藏层
        //该data是选人页面的tempData
        var name = "", id = "";
        var isFirst = false;
        for(var p in data) {
            if(isFirst) {
                name += ",";
                id += ",";
            }
            name += data[p].name;
            if(selType % 2) {
                id += "(U:"+data[p].oid+")";
            }else {
                id += data[p].oid;
            }
            isFirst = true;
        }
        eval(bcName + "Base.setFieldValue('" + idField + "',id)");
        eval(bcName + "Base.setFieldValue('" + titleField + "',name)");
    });

    //监听选人层返回按钮
    pm.bind("selectManClose", function(data) {
        $("#popupSelect").remove();//隐藏层
    });
}

/**   
* @method havaFlowAction 
* @description 判断是否有当前的流程动作
* @param {string} actionId 动作id   
*/
function havaFlowAction(actionId) {
    return this.flowActionlist[actionId] ? true : false;
}


function getFlowAction(actionId) {
    return this.flowActionlist[actionId];
}


function getFlowAciontListByType(type) {
    type = type == undefined ? 1 : type;
    var actionlist = []; //常用流程动作列表

    var TaskKind = getURLParameters('TaskKind'); //任务种类

    for (var i in this.flowActionlist) {
        var actionObj = this.flowActionlist[i];
        if (TaskKind != "0")//不能生成按钮得过滤掉
        {
            if (actionObj.name == "WF_BACK") {
                continue; //如果不是主办任务就禁掉跳转
            }
            else if (actionObj.name == "WF_NEXTSTEP" || actionObj.name == "WF_NEXTSTEP2" || actionObj.name == "WF_NEXTSTEP3" || actionObj.name == "WF_ABORT" || actionObj.name == "WF_PAUSE" || actionObj.name == "WF_RESUME") {//如果是下一步换成完成任务动作，如果已经有完成任务则取消该动作
                if (havaFlowAction("WF_FINISHTASK")) {//如果有完成任务
                    continue;
                }
                else {
                    this.flowActionlist[i].name = "WF_FINISHTASK";
                    this.flowActionlist[i].caption = "完成任务";
                }
            }
        }

        if (this.flowActionlist[i].type == type) {
            actionlist.add(this.flowActionlist[i]);
        }
    }
    return actionlist;
}


function setFlowActionButtonByVC(VC, type, callbackFn) {
    var toolbar = UCML.get("ToolBar" + VC.id);
    if (!toolbar) {

        if ($(VC.id + "ToolBar_Module").length == 0) {//ToolBar_Module假如对象不存在,则创建该对象
            var toolbarHtml = "<div id='" + VC.id + "Toolbar_Module'></div>";
            $("#" + VC.id + "_Module").prepend(toolbarHtml);
        }

        window["ToolBar" + VC.id] = new UCML.ToolBar(VC.id + "ToolBar_Module");
        toolbar = window["ToolBar" + VC.id];
		toolbar.Wrap =true;
    }
    setFlowActionButtonByToolbar(toolbar, type, callbackFn);
}

function setFlowActionButtonByToolbar(toolbar, type, callbackFn) {
    if (!toolbar) {
        return;
    }
    type = type == undefined ? 0 : type;

    if (type == 0) {//如果为默认按钮
        var actionList1 = getFlowAciontListByType(1);
        if (actionList1.length > 0) {
            for (var i = 0; i < actionList1.length; i++) {
                var actionObj = actionList1[i];
                if (actionObj.name == "WF_IDEATYPE" || actionObj.name == "WF_UPLOAD" || actionObj.name == "WF_SIGNIDEA")//不能生成按钮得过滤掉
                {
                    continue;
                }
                toolbar.addButton({
                    text: actionObj.caption,
                    tooltip: actionObj.caption,
                    action: actionObj,
                    onClick: function (el, e) {
                        flowToAction(this.action.name, callbackFn);
                    }
                });
                toolbar.addSeparator();
            }
        }

        var actionList2 = getFlowAciontListByType(2);
        if (actionList2.length > 0) {
            var menu2 = toolbar.addSplitButton({ text: "常用操作" });
            setFlowActionmMenu(menu2.menu, actionList2, callbackFn);
        }


        var actionList3 = getFlowAciontListByType(3);
        if (actionList3.length > 0) {
            var menu3 = toolbar.addSplitButton({ text: "高级操作" });
            setFlowActionmMenu(menu3.menu, actionList3, callbackFn);
        }
        return;
    }

    var actionList = getFlowAciontListByType(type);
    for (var i in actionList) {
        toolbar.addButton({
            text: actionList[i].caption,
            tooltip: actionList[i].caption,
            action: actionList[i],
            onClick: function (el, e) {
                flowToAction(this.action.name, callbackFn);
            }
        });
        toolbar.addSeparator();
    }

    function setFlowActionmMenu(menu, actionList, callbackFn) {
        for (var i = 0; i < actionList.length; i++) {

            menu.addMenuItem(actionList[i].name, actionList[i].caption, "",
                     function (menuItem, e) {
                         flowToAction(menuItem.menuId, callbackFn);
                     }
                )
            menu.addSeparator();
        }
    }
}

