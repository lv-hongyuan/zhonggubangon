﻿UCML.Combo = function (id) {
    this.addEvents("blur", "focus");
    UCML.Combo.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Combo, UCML.Input, {
    width: 'auto',
    panelWidth: 'auto',
    panelHeight: 200,
    multiple: true,
    separator: ',',
    editable: true,
    disabled: false,
    required: false,
    missingMessage: 'This field is required.',
    selectPrev: function () { },
    selectNext: function () { },
    selectCurr: function () { },
    filter: function (query) { },
    onChange: function (newValue, oldValue) { },
    hide: function () {
        UCML.Combo.superclass.hide.call(this);
        this.combo.hide();
        this.hidePanel();
    },
    show: function () {
        UCML.Combo.superclass.show.call(this);
        this.combo.show();
        this.showPanel();
    }
});

UCML.Combo.prototype.init = function () {
    UCML.Combo.superclass.init.call(this);
    //  this.el.hide();
    //  this.setDisabled(this.disabled);
    //   this.setSize();
    //  this.validate();
}

UCML.Combo.prototype.onRender = function () {
    var span = $('<span class="combo"></span>').insertAfter(this.el);
    var table = $('<table width="100%" style="width:100%; table-layout:fixed" border=0 cellSpacing=0 cellPadding=0><tr><td ></td><td  width="20"><span class="combo-arrow"></span></td></tr></table>').appendTo(span);
    var input = this.el.appendTo(table.find("td")[0]);
    input.addClass("combo-text");
    $('<input type="hidden" class="combo-value">').appendTo(span);



    var name = this.el.attr('name');
    if (name) {
        span.find('input.combo-value').attr('name', name);
        this.el.removeAttr('name').attr('comboName', name);
    }
    input.attr('autocomplete', 'off');

    if (!this.editable) {
        input.attr('readonly', this.editable);
    }

    if (!this.panel) {


        var panel = new UCML.Panel({ el: $('<div ></div>').appendTo('body'), doSize: true, closed: true,
            style: { position: 'absolute' }, width: this.panelWidth, height: this.panelHeight, isSetProperties: false,
            bodyCls: "combo-panel"
        });
        this.panel = panel;
	   //解决输入慢性能问题	
      //  $("<iframe app=\"about:blank\"  style=\"position:absolute; visibility:inherit; top:0px; left:0px; width:" + this.panelWidth + "; height:" + this.panelHeight + "; z-index:-1; filter='progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)';\"></iframe>").appendTo(this.panel.body);
        this.panel.el.bind('mousedown.combo', function (e) {
            return false;
        });
    }
    this.combo = span;

    UCML.on(this.el, "blur", function () {
        this.setValue(this.dom.value, false);
    }, this);
}

UCML.Combo.prototype.setProperties = function () {
    UCML.Combo.superclass.setProperties.call(this);
    this.panelWidth = (parseInt(this.el.attr('panelWidth')) || this.panelWidth);
    this.panelHeight = (this.el.attr('panelHeight') == 'auto' ? 'auto' : parseInt(this.el.attr('panelHeight')) || this.panelHeight);
    this.separator = (this.el.attr('separator') || this.separator);
    this.multiple = (this.el.attr('multiple') ? (this.el.attr('multiple') == 'true' || this.el.attr('multiple') == true) : this.multiple);
    this.editable = (this.el.attr('editable') ? this.el.attr('editable') == 'true' : true);
    this.disabled = (this.el.attr('disabled') ? true : this.disabled);
    this.required = (this.el.attr('required') ? (this.el.attr('required') == 'true' || this.el.attr('required') == true) : this.required);
    this.missingMessage = (this.el.attr('missingMessage') || this.missingMessage);

}

UCML.Combo.prototype.focus = function () {
    this.combo.find('input.combo-text')[0].focus();
    //  this.showPanel();
}

UCML.Combo.prototype.blur = function () {
    this.combo.find('input.combo-text')[0].blur();
    this.close();
}


UCML.Combo.prototype.destroy = function () {
    this.panel.destroy();
    this.combo.remove();
    UCML.Combo.superclass.destroy.call(this);
}

UCML.Combo.prototype.showPanel = function () {

    this.panel.el.css("z-index", UCML.Window.zIndex++);

    this.panel.open();

    var panelWidth = this.panel.getOuterWidth(true);
    var opt = this;
    (function () {
        if (opt.panel.el.is(':visible')) {
            var top = opt.combo.offset().top + opt.combo.outerHeight();
            var height = null;
            if (top + opt.panel.el.outerHeight() > $(window).height() + $(document).scrollTop()) {
                top = opt.combo.offset().top - opt.panel.el.outerHeight();
                height = $(window).height() - (opt.combo.offset().top + opt.combo.outerHeight());
            }
            if (top < $(document).scrollTop()) {
                top = opt.combo.offset().top + opt.combo.outerHeight();
            }
            var left = opt.combo.offset().left;
            if (left + panelWidth > $(window).width() + $(document).scrollLeft()) {
                left = (left + opt.combo.outerWidth()) - panelWidth;
            }

            opt.panel.el.css({
                left: left,
                top: top
            });

            if(height)
            {
                opt.panel.el.css({
                    height: height,
                    'overflow-y': 'auto',
                    'overflow-x': 'hidden'
                });
            }

            setTimeout(arguments.callee, 200);
        }

    })();
}

UCML.Combo.prototype.hidePanel = function () {
    this.panel.close();
}



UCML.Combo.prototype.setDisabled = function (disabled) {
    if (disabled) {
        this.disabled = true;
        this.el.attr('disabled', true);
        this.combo.find('.combo-value').attr('disabled', true);
        this.combo.find('.combo-text').attr('disabled', true);
    } else {
        this.disabled = false;
        this.el.removeAttr('disabled');
        this.combo.find('.combo-value').removeAttr('disabled');
        this.combo.find('.combo-text').removeAttr('disabled');
    }
}

UCML.Combo.prototype.clear = function () {
    this.combo.find('input.combo-value:gt(0)').remove();
    this.combo.find('input.combo-value').val('');
    this.combo.find('input.combo-text').val('');
}

UCML.Combo.prototype.getText = function () {
    return this.combo.find('input.combo-text').val();
}

UCML.Combo.prototype.setText = function (text) {
    this.combo.find('input.combo-text').val(text);
    this.validate(true);
}

UCML.Combo.prototype.validate = function (doit) {

    var input = this.combo.find('input.combo-text');
    // this.validatebox = new UCML.Validatebox(input);
    //  this.validatebox.init();

    //   if (doit) {
    //       this.validatebox.validate();
    //       input.trigger('mouseleave');
    //   }
}

UCML.Combo.prototype.getValues = function () {
    var values = [];
    this.combo.find('input.combo-value').each(function () {
        values.push($(this).val());
    });
    return values;
}

UCML.Combo.prototype.setValues = function (values) {
    var oldValues = this.getValues();
    this.combo.find('input.combo-value').remove();
    var name = this.el.attr('comboName');
    for (var i = 0; i < values.length; i++) {
        var input = $('<input type="hidden" class="combo-value">').appendTo(this.combo);
        if (name) input.attr('name', name);
        input.val(values[i]);
    }

    var tmp = [];
    for (var i = 0; i < oldValues.length; i++) {
        tmp[i] = oldValues[i];
    }
    var aa = [];
    for (var i = 0; i < values.length; i++) {
        for (var j = 0; j < tmp.length; j++) {
            if (values[i] == tmp[j]) {
                aa.push(values[i]);
                tmp.splice(j, 1);
                break;
            }
        }
    }

    if (aa.length != values.length || values.length != oldValues.length) {
        if (this.multiple) {
            this.onChange.call(this, values, oldValues);
        } else {
            this.onChange.call(this, values[0], oldValues[0]);
        }
    }
}

UCML.Combo.prototype.getValue = function () {
    var values = this.getValues();
    return values[0];
}

UCML.Combo.prototype.setValue = function (value, trigger) {
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    this.value = value;
    this.setText(value);
    this.setValues([value]);
}

UCML.Combo.prototype.setSize = function (width) {
    if (width) {
        this.width = width;
    }

    if (!isNaN(this.width)) {
        if ($.boxModel == true) {
            this.combo.width(this.width - (this.combo.outerWidth() - this.combo.width()));
        } else {
            this.combo.width(this.width);
        }
    }
}

UCML.Combo.prototype.setOffset = function (top, left) {
    this.combo.offset({ top: top, left: left });
}


UCML.Combo.prototype.close = function () {
    this.fireEvent("blur");
    if (this.panel) {
        this.panel.close();
    }
}

UCML.Combo.prototype.bindEvents = function () {
    UCML.Combo.superclass.bindEvents.call(this);
    var input = this.combo.find('.combo-text');
    var arrow = this.combo.find('.combo-arrow');

    //  $(document).unbind('.combo');
    this.combo.unbind('.combo');
    // this.panel.el.unbind('.combo');
    input.unbind('.combo');
    arrow.unbind('.combo');

    var opt = this;

    if (!this.disabled) {
        UCML.on(document, "mousedown.combo", this.close, this);
        //        this.panel.el.bind('mousedown.combo', function(e) {
        //            return false;
        //        });

        this.combo.bind('mousedown.combo', function (e) {
            return false;
        });

        input.bind('focus.combo', function () {

            if (opt.fireEvent("focus") === true) {
                opt.showPanel();
            }
            //              input.bind('blur.combo', function () {
            //            if (opt.fireEvent("blur") === true) {
            //                opt.hidePanel();
            //            }

        }).bind('mousedown.combo', function (e) {
            input.focus();
            //  e.stopPropagation();
        }).bind('keyup.combo', function (e) {
            switch (e.keyCode) {
                case 37: // left
                case 38: // up
                    opt.selectPrev.call(opt);
                    break;
                case 39: // right
                case 40: // down
                    opt.selectNext.call(opt);
                    break;
                case 13: // enter
                    //解决控件回车弹出窗口问题
                    //opt.selectCurr.call(opt);
                    opt.hidePanel(opt);
                    break;
                case 27: // esc
                    opt.hidePanel(opt);
                    break;
                default:
                    if (opt.editable) {
                        opt.filter.call(opt, $(this).val());
                    }
            }
            return false;
        });

        arrow.bind('click.combo', function () {
            input.focus();
        }).bind('mouseenter.combo', function () {
            $(this).addClass('combo-arrow-hover');
        }).bind('mouseleave.combo', function () {
            $(this).removeClass('combo-arrow-hover');
        });
    }
}




UCML.Combo.prototype.disable = function (jq) {
    this.setDisabled(true);
    this.bindEvents();
}

UCML.Combo.prototype.enable = function (jq) {
    this.setDisabled(false);
    this.bindEvents();
}

UCML.Combo.prototype.textbox = function () {
    return this.combo.find('input.combo-text');
}

UCML.reg("UCML.Combo", UCML.Combo);





UCML.ComboBox = function (id) {
    this.selected = [];
    UCML.ComboBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.ComboBox, UCML.Combo, {
    ctype: "UCML.ComboBox",
    autoEl: 'input',
    panelWidth: 180,
    panelHeight: 'auto',
    setProperties: function () {
        UCML.ComboBox.superclass.setProperties.call(this);
        this.codeTable = this.getAttribute("codeTable") || this.codeTable;
        this.isCodeTable = (this.el.attr('isCodeTable') ? this.getAttribute('isCodeTable')
== 'true' : false) || this.isCodeTable;
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute
("srcBCName")) || undefined;
        this.dataValueField = this.getAttribute("dataValueField") || this.dataValueField;
        this.dataTextField = this.getAttribute("dataTextField") || this.dataTextField;

    }
});

UCML.ComboBox.prototype.init = function () {
    UCML.ComboBox.superclass.init.call(this);
    UCML.on(this.dom, "change", function () { this.setValue(this.dom.value); }, this);

    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.srcDataTable.on("onLoad", this.dataBind, this);

    }
    else if (this.isCodeTable == true && !UCML.isEmpty(this.codeTable)) {
        this.bindCodeTable();
    }
}


/**
* select previous item
*/
UCML.ComboBox.prototype.selectPrev = function () {
    var panel = this.panel.body;
    var values = this.getValues();
    var item = panel.find("div.combobox-item[value='" + values.pop() + "']");
    if (item.length) {
        var prev = item.prev(':visible');
        if (prev.length) {
            item = prev;
        }
    } else {
        item = panel.find('div.combobox-item:visible:last');
    }
    var value = item.attr('value');

    this.setValues([value]);

    if (item.position().top <= 0) {
        var h = panel.scrollTop() + item.position().top;
        panel.scrollTop(h);
    } else if (item.position().top + item.outerHeight() > panel.height()) {
        var h = panel.scrollTop() + item.position().top + item.outerHeight() - panel.height();
        panel.scrollTop(h);
    }
}

UCML.ComboBox.prototype.selectNext = function () {
    var panel = this.panel.body;
    var values = this.getValues();
    var item = panel.find("div.combobox-item[value='" + values.pop() + "']");
    if (item.length) {
        var next = item.next(':visible');
        if (next.length) {
            item = next;
        }
    } else {
        item = panel.find('div.combobox-item:visible:first');
    }
    var value = item.attr('value');

    this.setValues([value]);

    if (item.position().top <= 0) {
        var h = panel.scrollTop() + item.position().top;
        panel.scrollTop(h);
    } else if (item.position().top + item.outerHeight() > panel.height()) {
        var h = panel.scrollTop() + item.position().top + item.outerHeight() - panel.height();
        panel.scrollTop(h);
    }
}

UCML.ComboBox.prototype.selectCurr = function () {
    var panel = this.panel.body;
    var item = panel.find('div.combobox-item-selected');
    this.setValues([item.attr('value')]);
    this.hidePanel();
}

UCML.ComboBox.prototype.filter = function (query) {

    this.showPanel();
    var panel = this.panel.body;
    //   this.setValues([], true, false);
    panel.find('div.combobox-item').each(function () {
        var item = $(this);
        if (item.text().indexOf(query) == 0) {
            item.show();
            if (item.text() == query) {
                item.addClass('combobox-item-selected');
            }
        } else {
            item.hide();
        }
    });
}

/**
* select the specified value
*/
UCML.ComboBox.prototype.select = function (value) {

    var opts = this;

    if (opts.multiple) {
        var values = this.getValues();
        for (var i = 0; i < values.length; i++) {
            if (values[i] == value) return;
        }
        values.push(value);

        var val = values.join(this.separator);

        this.setValue(val);
        //this.setValues(values);
    } else {
        this.setValue(value);
        //   this.setValues([value]);
        this.hidePanel();
    }

    /*
    for (var i = 0; i < data.length; i++) {
    if (data[i][opts.valueField] == value) {
    opts.onSelect.call(target, data[i]);
    return;
    }
    }
    */
}

/**
* unselect the specified value
*/
UCML.ComboBox.prototype.unselect = function (value) {
    var opts = this;
    var values = this.getValues();

    for (var i = 0; i < values.length; i++) {
        if (values[i] == value) {
            values.splice(i, 1);

            var val = values.join(this.separator);

            this.setValue(val);

            break;
        }
    }

}

UCML.ComboBox.prototype.setValues = function (values, remainText) {
    var opts = this;
    var panel = this.panel.body;

    UCML.ComboBox.superclass.setValues.call(this, values);
    panel.find('div.combobox-item-selected').removeClass('combobox-item-selected');
    var vv = [], ss = [];
    for (var i = 0; i < values.length; i++) {
        var v = values[i];

        var itemSelect = panel.find("div.combobox-item[value='" + v + "']");
        vv.push(v);
        ss.push(itemSelect.html());
        itemSelect.addClass('combobox-item-selected');
    }
    if (!remainText) {
        var text = ss.join(opts.separator);

        this.setText(text);
    }
}

/**
* set value
*/
UCML.ComboBox.prototype.setValue = function (value, trigger) {

    var opts = this;
    var v = value;

    if (opts.multiple) {
        if (v) {
            this.setValues(value.split(opts.separator));
        }
        else {
            this.setValues([]);
        }
    }
    else {
        this.setValues([v]);
    }

    if (trigger !== false) {
        this.fireEvent("setValue", v);
    }
}


UCML.ComboBox.prototype.add = function (caption, value, selected) {

    var opts = this;
    var panel = this.panel.body;
    var item = $('<div class="combobox-item"></div>').appendTo(panel);
    item.attr('value', value);
    item.html(caption);
    if (selected) {
        this.selected.push(value);

        if (opts.multiple) {
            this.setValues(this.selected);
        } else {
            if (this.selected.length) {
                this.setValues([this.selected[0]]);
            } else {
                this.setValues([]);
            }
        }
    }

    item.hover(
			function () { $(this).addClass('combobox-item-hover'); },
			function () { $(this).removeClass('combobox-item-hover'); }
		).click(function () {
		    var item = $(this);
		    if (opts.multiple) {

		        if (item.hasClass('combobox-item-selected')) {
		            opts.unselect(item.attr('value'));
		        } else {
		            opts.select(item.attr('value'));
		        }
		    } else {
		        opts.select(item.attr('value'));

		        opts.fireEvent("blur");
		    }
		});
}

/**   
* @method dataBind 
* @description 绑定控件   
*/
UCML.ComboBox.prototype.dataBind = function () {
    UCML.RadioGroup.superclass.dataBind.call(this);

    this.el.empty();
    var recordCount = this.srcDataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        this.add(this.srcDataTable.getFieldValue(this.dataTextField), this.srcDataTable.getFieldValue(this.dataValueField));
    }
    this.srcDataTable.SetIndexNoEvent(0);
}

UCML.ComboBox.prototype.bindCodeTable = function () {
    var panel = this.panel.body;
    panel.empty(); // clear old data
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    if (codeList && codeList.length > 0) {
        for (var i = 0; i < codeList.length; i++) {
            this.add(codeList[i].caption, codeList[i].value);
        }
    }
}

UCML.reg("UCML.ComboBox", UCML.ComboBox);