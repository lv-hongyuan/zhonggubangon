UCML.Pictures_Mobile = function(id) {
	this.saveFieldName;
    this.savePath = "Temp";
    this.saveToDB = true;
    this.parentVC = null;
    this.imgWidth = "100px";
    this.imgHeight = "100px";
    this.imgListSrc = {};

	UCML.Pictures_Mobile.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Pictures_Mobile, UCML.Applet, {
	buttonText: "上传图片",
    hideButton: false,

    onRender: function () {

    	UCML.Pictures_Mobile.superclass.onRender.call(this);

    	var img = $('<img style="width:'+this.imgWidth+';height:'+this.imgHeight+'"></img>');
    	this.el.append(img);
    	this.img = img;
    	if(this.parentVC) {
            var column = this.parentVC.getColumn(this.saveFieldName);
            if (column && column.allowModify == false) 
            	this.hideButton = true;
        }
        if(!this.hideButton) {

            this.showPanel();
        }

    },
    
    open: function () {

    	UCML.Pictures_Mobile.superclass.open.call(this);
    },
    bindEvents: function() {
    	UCML.Pictures_Mobile.superclass.bindEvents.call(this);

    	this.dataTable.on("onLoad", this.loadData, this);
    	this.dataTable.on("OnRecordChange", this.loadData, this);
    },
    getSrc: function () {

        return getServicePath() + "File/Images/" + this.dataTable.TableName + "_" + this.saveFieldName + "/" + UCML.UserInfo.getUserOID() + "/" + this.dataTable.getOID();
    }
});

UCML.Pictures_Mobile.prototype.loadData = function (data) {
    if (this.dataTable.getFieldValue(this.saveFieldName) != "") {
        this.img[0].src = "";
        if (this.saveToDB)
            this.img[0].src = this.getSrc() + this.dataTable.getFieldValue(this.saveFieldName);
        else
            this.img[0].src = getServicePath() + "File/Images/System/" + this.dataTable.getFieldValue(this.saveFieldName);

        //点击图片查看缩略图
        this.img.bind("click", function(){
            rnBridge.exec(function(){}, function(){}, "ExchangeService", "ShowImgDetail", [this.src]);
        });
    }
    else if (this.imgListSrc && this.imgListSrc[this.dataTable.getOID()]) {
        this.img[0].src = "";
        this.img[0].src = this.imgListSrc[this.dataTable.getOID()];
    }
    else {
        this.img[0].src = "";
    }
}

//显示图片选择渠道按钮{来自相机、来自相册}
UCML.Pictures_Mobile.prototype.showPanel = function () {
    var innerHtml = '<div class="fj_upload"><a href="#" style="width:50px;height:50px" class="fj_upload_a fj_select_lib">相册</a><a href="#" style="width:50px;height:50px" class="fj_upload_a fj_select_camera">相机</a></div><div class="upload_process_bar"><div class="upload_current_process"></div></div><div class="process_info"></div>';
    this.el.append(innerHtml);

    var opts_Mobile = this;
    this.el.find(".fj_select_camera").click(function(e) {
        openFileSelector("camera");
    });

    this.el.find(".fj_select_lib").click(function(e) {
        openFileSelector("photoLib");
    });

    //选择打开类型
    function openFileSelector(type) {
        var source = null;
        if(type == "camera")
            source = navigator.camera.PictureSourceType.CAMERA;
        else if(type == "photoLib")
            source = navigator.camera.PictureSourceType.PHOTOLIBRARY;

        //描述类型，取文件路径
        var destinationType = navigator.camera.DestinationType;

        //媒体类型，设置为ALLMEDIA即支持任意文件选择
        var mediaType = navigator.camera.MediaType.PICTURE;

        var options = {
            quality: 50,
            destinationType: destinationType.FILE_URI,//DATA_URL----"data:image/jpeg;base64," + imageData
            sourceType : source,
            mediaType : mediaType //targetWidth:100,targetHeight:100
        };

        navigator.camera.getPicture(uploadFile, uploadBroken, options);
    }

    //选择文件成功后
    function uploadFile(fileURI) {
//        window.resolveLocalFileSystemURL(fileURI,
//            function(fileEntry) {
                var options = new FileUploadOptions();
                options.fileKey = "file";
                options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
                var tempName = options.fileName.split(".");
                if(tempName.length > 1) {
                    var len = tempName.length;
                    var extension = tempName[len-1].toLowerCase();
                    if(extension != "png" && extension != "jpg" && extension != "jpeg" && extension != "gif") {
                        options.fileName = options.fileName + ".jpg";
                    }
                }else {
                    options.fileName = options.fileName + ".jpg";
                }
                //options.mimeType = "multipart/form-data";
                options.mimeType = "image/jpeg";
                options.chunkedMode = false;
                ft = new FileTransfer();
        
                opts_Mobile.img.attr("src", fileURI);

                if (opts_Mobile.syncSubmit() === false) return false; //取消上传

                var data = { fieldName: opts_Mobile.saveFieldName, saveToDB: opts_Mobile.saveToDB, tableName: opts_Mobile.dataTable.TableName, OID: opts_Mobile.dataTable.getOID(), UserOID: UCML.UserInfo.getUserOID() };
                options.params = data;

                var uploadUrl = encodeURI(getServicePath() + "UCML_JS/Plugs/Pictures/UploadFileToDB.ashx");

                ft.upload(fileURI, uploadUrl, uploadSuccess, uploadFailed, options);

                //获取上传进度
                ft.onprogress = uploadProcessing;
                //显示进度条
                //$('.upload_process_bar,.process_info').show();
                opts_Mobile.el.find('.upload_process_bar,.process_info').show();
            //},function() {
            //    alert("error");
            //});
    }
    //选择失败
    function uploadBroken(message) {
        alert('Failed because: ' + message);
    }
    //上传进度
    function uploadProcessing(progressEvent) {
        if (progressEvent.lengthComputable) {
            //已经上传
            var loaded = progressEvent.loaded;
            //文件总长度
            var total = progressEvent.total;

            //计算百分比，用于显示进度条
            var percent = parseInt((loaded / total) * 100);
            //换算成MB
            loaded = (loaded / 1024 / 1024).toFixed(2);
            total = (total / 1024 / 1024).toFixed(2);
            opts_Mobile.el.find('.process_info').html(loaded + 'M/' + total + 'M');
            opts_Mobile.el.find('.upload_current_process').css({ 'width': percent + '%' });
        }
    }
    //上传成功
    function uploadSuccess(result) {
        var $process_info = opts_Mobile.el.find('.process_info');
        $process_info.html($process_info.html()+"&nbsp;&nbsp;上传成功!");

        //隐藏进度条
        window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2500);
    }
    //上传失败
    function uploadFailed(error) {
        //e.code, e.source, e.target, e.http_status, e.body
        var $process_info = this.el.find('.process_info');
        $process_info.html($process_info.html()+"&nbsp;&nbsp;上传失败!");

        //隐藏进度条
        window.setTimeout("$('.upload_process_bar,.process_info').hide()", 2500);
    }
}

//同步提交数据
UCML.Pictures_Mobile.prototype.syncSubmit = function () {
    if (BusinessObject.BeforeSubmit === undefined || BusinessObject.BeforeSubmit() === true) {
        if (BusinessObject.CanSubmit() == false) return true;
        var changeXML = this.dataTable.getChangeXML();
        if (this.dataTable.ChangeCount() > 0) {
            ShowMask();
            ShowMessage("正在提交信息，请等待......");
            var config = { methodName: "BusinessSubmit", params: { DeltaXml: changeXML }, async: false };

            var text = BusinessObject.invoke(config);

            this.dataTable.MergeChange();
            HideMessage();
            hidenMask();
        }
    }
}

