﻿
(function ($) {
    $.fn.UCML_resizable = function (options) {
        function resize(e) {
            var resizeData = e.data;
            var options = $.data(resizeData.target, 'resizable').options;
            if (resizeData.dir.indexOf('e') != -1) {
                var width = resizeData.startWidth + e.pageX - resizeData.startX;
                width = Math.min(
							Math.max(width, options.minWidth),
							options.maxWidth
						);
                resizeData.width = width;
            }
            if (resizeData.dir.indexOf('s') != -1) {
                var height = resizeData.startHeight + e.pageY - resizeData.startY;
                height = Math.min(
						Math.max(height, options.minHeight),
						options.maxHeight
				);
                resizeData.height = height;
            }
            if (resizeData.dir.indexOf('w') != -1) {
                resizeData.width = resizeData.startWidth - e.pageX + resizeData.startX;
                if (resizeData.width >= options.minWidth && resizeData.width <= options.maxWidth) {
                    resizeData.left = resizeData.startLeft + e.pageX - resizeData.startX;
                }
            }
            if (resizeData.dir.indexOf('n') != -1) {
                resizeData.height = resizeData.startHeight - e.pageY + resizeData.startY;
                if (resizeData.height >= options.minHeight && resizeData.height <= options.maxHeight) {
                    resizeData.top = resizeData.startTop + e.pageY - resizeData.startY;
                }
            }
        }

        function applySize(e) {
            var resizeData = e.data;
            var target = resizeData.target;
            if ($.boxModel == true) {
                $(target).css({
                    width: resizeData.width - resizeData.deltaWidth,
                    height: resizeData.height - resizeData.deltaHeight,
                    left: resizeData.left,
                    top: resizeData.top
                });
            } else {
                $(target).css({
                    width: resizeData.width,
                    height: resizeData.height,
                    left: resizeData.left,
                    top: resizeData.top
                });
            }
        }

        function doDown(e) {
            $.data(e.data.target, 'resizable').options.onStartResize.call(e.data.target, e);
            return false;
        }

        function doMove(e) {
            resize(e);
            if ($.data(e.data.target, 'resizable').options.onResize.call(e.data.target, e) != false) {
                //     applySize(e)
            }
            return false;
        }

        function doUp(e) {
            resize(e, true);
            //   applySize(e);
            var doc = e.data.document || document;
            $(doc).unbind('.resizable');
            $.data(e.data.target, 'resizable').options.onStopResize.call(e.data.target, e);
            return false;
        }

        return this.each(function () {
            var opts = null;
            var state = $.data(this, 'resizable');
            if (state) {
                $(this).unbind('.resizable');
                opts = $.extend(state.options, options || {});
            } else {
                opts = $.extend({}, $.fn.UCML_resizable.defaults, options || {});
            }

            if (opts.disabled == true) {
                return;
            }

            $.data(this, 'resizable', {
                options: opts
            });

            var target = this;
            var doc = opts.document || document;
            // bind mouse event using namespace resizable
            $(this).bind('mousemove.resizable', { document: doc }, onMouseMove)
				   .bind('mousedown.resizable', { document: doc }, onMouseDown);

            function onMouseMove(e) {
                var dir = getDirection(e);
                if (dir == '') {
                    $(target).css('cursor', 'default');
                } else {
                    $(target).css('cursor', dir + '-resize');
                }
            }

            function onMouseDown(e) {
                var dir = getDirection(e);
                if (dir == '') return;

                var data = {
                    target: this,
                    dir: dir,
                    startLeft: getCssValue('left'),
                    startTop: getCssValue('top'),
                    left: getCssValue('left'),
                    top: getCssValue('top'),
                    startX: e.pageX,
                    startY: e.pageY,
                    startWidth: $(target).outerWidth(),
                    startHeight: $(target).outerHeight(),
                    width: $(target).outerWidth(),
                    height: $(target).outerHeight(),
                    deltaWidth: $(target).outerWidth() - $(target).width(),
                    deltaHeight: $(target).outerHeight() - $(target).height(),
                    document: e.data.document
                };

                var doc = e.data.document;
                $(doc).bind('mousedown.resizable', data, doDown);
                $(doc).bind('mousemove.resizable', data, doMove);
                $(doc).bind('mouseup.resizable', data, doUp);
            }

            // get the resize direction
            function getDirection(e) {
                var dir = '';
                var offset = $(target).offset();
                var width = $(target).outerWidth();
                var height = $(target).outerHeight();
                var edge = opts.edge;
                if (e.pageY > offset.top && e.pageY < offset.top + edge) {
                    dir += 'n';
                } else if (e.pageY < offset.top + height && e.pageY > offset.top + height - edge) {
                    dir += 's';
                }
                if (e.pageX > offset.left && e.pageX < offset.left + edge) {
                    dir += 'w';
                } else if (e.pageX < offset.left + width && e.pageX > offset.left + width - edge) {
                    dir += 'e';
                }

                var handles = opts.handles.split(',');
                for (var i = 0; i < handles.length; i++) {
                    var handle = handles[i].replace(/(^\s*)|(\s*$)/g, '');
                    if (handle == 'all' || handle == dir) {
                        return dir;
                    }
                }
                return '';
            }

            function getCssValue(css) {
                var val = parseInt($(target).css(css));
                if (isNaN(val)) {
                    return 0;
                } else {
                    return val;
                }
            }

        });
    };

    $.fn.UCML_resizable.defaults = {
        disabled: false,
        document: null,
        handles: 'n, e, s, w, ne, se, sw, nw, all',
        minWidth: 10,
        minHeight: 10,
        maxWidth: 10000, //$(document).width(),
        maxHeight: 10000, //$(document).height(),
        edge: 5,
        onStartResize: function (e) { },
        onResize: function (e) { },
        onStopResize: function (e) { }
    };

})(jQuery);


(function ($) {
    function drag(e) {
        var opts = $.data(e.data.target, 'draggable').options;

        var dragData = e.data;
        var left = dragData.startLeft + e.pageX - dragData.startX;
        var top = dragData.startTop + e.pageY - dragData.startY;

        if (opts.deltaX != null && opts.deltaX != undefined) {
            left = e.pageX + opts.deltaX;
        }
        if (opts.deltaY != null && opts.deltaY != undefined) {
            top = e.pageY + opts.deltaY;
        }

        var doc = e.data.document || document;

        if (e.data.parnet != doc.body) {
            if ($.boxModel == true) {
                left += $(e.data.parent).scrollLeft();
                top += $(e.data.parent).scrollTop();
            }
        }

        if (opts.axis == 'h') {
            dragData.left = left;
        } else if (opts.axis == 'v') {
            dragData.top = top;
        } else {
            dragData.left = left;
            dragData.top = top;
        }
    }

    function applyDrag(e) {
        var opts = $.data(e.data.target, 'draggable').options;
        var proxy = $.data(e.data.target, 'draggable').proxy;
        if (proxy) {
            proxy.css('cursor', opts.cursor);
        } else {
            proxy = $(e.data.target);
            $.data(e.data.target, 'draggable').handle.css('cursor', opts.cursor);
        }
        proxy.css({
            left: e.data.left,
            top: e.data.top
        });
    }

    function doDown(e) {
        var opts = $.data(e.data.target, 'draggable').options;

        var droppables = $('.droppable').filter(function () {
            return e.data.target != this;
        }).filter(function () {
            var accept = $.data(this, 'droppable').options.accept;
            if (accept) {
                return $(accept).filter(function () {
                    return this == e.data.target;
                }).length > 0;
            } else {
                return true;
            }
        });
        $.data(e.data.target, 'draggable').droppables = droppables;

        var proxy = $.data(e.data.target, 'draggable').proxy;
        if (!proxy) {
            if (opts.proxy) {
                if (opts.proxy == 'clone') {
                    proxy = $(e.data.target).clone().insertAfter(e.data.target);
                } else {
                    proxy = opts.proxy.call(e.data.target, e.data.target);
                }
                $.data(e.data.target, 'draggable').proxy = proxy;
            } else {
                proxy = $(e.data.target);
            }
        }

        proxy.css('position', 'absolute');
        drag(e);
        applyDrag(e);

        opts.onStartDrag.call(e.data.target, e);
        return false;
    }

    function doMove(e) {

        drag(e);
        if ($.data(e.data.target, 'draggable').options.onDrag.call(e.data.target, e) != false) {
            applyDrag(e);
        }

        var source = e.data.target;
        $.data(e.data.target, 'draggable').droppables.each(function () {
            var dropObj = $(this);

            var p2 = $(this).offset();
            if (e.pageX > p2.left && e.pageX < p2.left + dropObj.outerWidth()
					&& e.pageY > p2.top && e.pageY < p2.top + dropObj.outerHeight()) {
                if (!this.entered) {
                    $(this).trigger('_dragenter', [source, e]);
                    this.entered = true;
                }
                $(this).trigger('_dragover', [source, e]);
            } else {
                if (this.entered) {
                    $(this).trigger('_dragleave', [source, e]);
                    this.entered = false;
                }
            }
        });

        return false;
    }

    function doUp(e) {
        drag(e);

        var proxy = $.data(e.data.target, 'draggable').proxy;
        var opts = $.data(e.data.target, 'draggable').options;
        if (opts.revert) {
            if (checkDrop() == true) {
                removeProxy();
                $(e.data.target).css({
                    position: e.data.startPosition,
                    left: e.data.startLeft,
                    top: e.data.startTop
                });
            } else {
                if (proxy) {
                    proxy.animate({
                        left: e.data.startLeft,
                        top: e.data.startTop
                    }, function () {
                        removeProxy();
                    });
                } else {
                    $(e.data.target).animate({
                        left: e.data.startLeft,
                        top: e.data.startTop
                    }, function () {
                        $(e.data.target).css('position', e.data.startPosition);
                    });
                }
            }
        } else {
            $(e.data.target).css({
                position: 'absolute',
                left: e.data.left,
                top: e.data.top
            });
            removeProxy();
            checkDrop();
        }



        opts.onStopDrag.call(e.data.target, e);

        function removeProxy() {
            if (proxy) {
                proxy.remove();
            }
            $.data(e.data.target, 'draggable').proxy = null;
        }

        function checkDrop() {
            var dropped = false;
            $.data(e.data.target, 'draggable').droppables.each(function () {
                var dropObj = $(this);
                var p2 = $(this).offset();
                if (e.pageX > p2.left && e.pageX < p2.left + dropObj.outerWidth()
						&& e.pageY > p2.top && e.pageY < p2.top + dropObj.outerHeight()) {
                    if (opts.revert) {
                        $(e.data.target).css({
                            position: e.data.startPosition,
                            left: e.data.startLeft,
                            top: e.data.startTop
                        });
                    }
                    $(this).trigger('_drop', [e.data.target]);
                    dropped = true;
                    this.entered = false;
                }
            });
            return dropped;
        }

        var doc = e.data.document || document;

        $(doc).unbind('.draggable');
        return false;
    }

    $.fn.UCML_draggable = function (options) {
  
        if (typeof options == 'string') {
            switch (options) {
                case 'options':
                    return $.data(this[0], 'draggable').options;
                case 'proxy':
                    return $.data(this[0], 'draggable').proxy;
                case 'enable':
                    return this.each(function () {
                        $(this).UCML_draggable({ disabled: false });
                    });
                case 'disable':
                    return this.each(function () {
                        $(this).UCML_draggable({ disabled: true });
                    });
            }
        }

        return this.each(function () {
            //			$(this).css('position','absolute');

            var opts;
            var state = $.data(this, 'draggable');
            if (state) {
                state.handle.unbind('.draggable');
                opts = $.extend(state.options, options);
            } else {
                opts = $.extend({}, $.fn.UCML_draggable.defaults, options || {});
            }

            if (opts.disabled == true) {
                $(this).css('cursor', 'default');
                return;
            }

            var handle = null;
            if (typeof opts.handle == 'undefined' || opts.handle == null) {
                handle = $(this);
            } else {
                handle = (typeof opts.handle == 'string' ? $(opts.handle, this) : handle);
            }
            $.data(this, 'draggable', {
                options: opts,
                handle: handle
            });

            // bind mouse event using event namespace draggable
            handle.bind('mousedown.draggable', { target: this, document: opts.document }, onMouseDown);
            handle.bind('mousemove.draggable', { target: this, document: opts.document }, onMouseMove);

            function onMouseDown(e) {
       
                if (checkArea(e) == false) return;

                var position = $(e.data.target).position();
                var data = {
                    startPosition: $(e.data.target).css('position'),
                    startLeft: position.left,
                    startTop: position.top,
                    left: position.left,
                    top: position.top,
                    startX: e.pageX,
                    startY: e.pageY,
                    target: e.data.target,
                    parent: $(e.data.target).parent()[0],
                    document: opts.document
                };

                var doc = e.data.document || document;

                $(doc).bind('mousedown.draggable', data, doDown);
                $(doc).bind('mousemove.draggable', data, doMove);
                $(doc).bind('mouseup.draggable', data, doUp);
            }

            function onMouseMove(e) {
                if (checkArea(e)) {
                    $(this).css('cursor', opts.cursor);
                } else {
                    $(this).css('cursor', 'default');
                }
            }

            // check if the handle can be dragged
            function checkArea(e) {
                var offset = $(handle).offset();
                var width = $(handle).outerWidth();
                var height = $(handle).outerHeight();
                var t = e.pageY - offset.top;
                var r = offset.left + width - e.pageX;
                var b = offset.top + height - e.pageY;
                var l = e.pageX - offset.left;

                return Math.min(t, r, b, l) > opts.edge;
            }

        });
    };

    $.fn.UCML_draggable.defaults = {
        proxy: null, // 'clone' or a function that will create the proxy object, 
        // the function has the source parameter that indicate the source object dragged.
        revert: false,
        document: null,
        cursor: 'move',
        deltaX: null,
        deltaY: null,
        handle: null,
        disabled: false,
        edge: 0,
        axis: null, // v or h

        onStartDrag: function (e) { },
        onDrag: function (e) { },
        onStopDrag: function (e) { }
    };


})(jQuery);

(function ($) {
    function init(target) {
        $(target).addClass('droppable');
        $(target).bind('_dragenter', function (e, source, a) {
            $.data(target, 'droppable').options.onDragEnter.apply(target, [e, source, a]);
        });
        $(target).bind('_dragleave', function (e, source, a) {
            $.data(target, 'droppable').options.onDragLeave.apply(target, [e, source, a]);
        });
        $(target).bind('_dragover', function (e, source, a) {
            $.data(target, 'droppable').options.onDragOver.apply(target, [e, source, a]);
        });
        $(target).bind('_drop', function (e, source) {
            $.data(target, 'droppable').options.onDrop.apply(target, [e, source]);
        });
    }

    
    $.fn.UCML_droppable = function (options) {
        options = options || {};
        return this.each(function () {
            var state = $.data(this, 'droppable');
            if (state) {
                $.extend(state.options, options);
            } else {
                init(this);
                $.data(this, 'droppable', {
                    options: $.extend({}, $.fn.UCML_droppable.defaults, options)
                });
            }
        });
    };

    $.fn.UCML_droppable.defaults = {
        accept: null,
        onDragEnter: function (e, source) { },
        onDragOver: function (e, source) { },
        onDragLeave: function (e, source) { },
        onDrop: function (e, source) { }
    };

    /*对老版本进行兼容*/
    $.fn.resizable= $.fn.UCML_resizable;
    $.fn.draggable  = $.fn.UCML_draggable;
    $.fn.droppable = $.fn.UCML_droppable;

})(jQuery);