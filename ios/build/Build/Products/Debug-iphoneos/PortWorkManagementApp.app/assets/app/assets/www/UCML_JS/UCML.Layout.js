﻿UCML.FitLayout = function (id) {
    UCML.FitLayout.superclass.constructor.call(this, id);
}

UCML.extend(UCML.FitLayout, UCML.Container, {
    ctype: "UCML.FitLayout",
    onRender: function () {
        this.items = [];

        var cds = this.el.children();
        for (var i = 0; i < cds.length; i++) {
            var el = $(cds[i]);
            var id = el.attr("id");

            var elObj = null;
            if (id) {
                var cmObj = UCML.get(id);

                if (cmObj && cmObj.el) {
                    elObj = cmObj
                }
                else {
                    elObj = new UCML.Container({ el: el });
                }
                elObj.resizeTo = false;
                this.items.add(elObj);
            }
        }
        UCML.FitLayout.superclass.onRender.call(this);
    },
    resize: function (param) {
        this.setSize(param);
        // this.el.triggerHandler('_resize');
        // alert(this.id + "triggerHandler_resize");
    },
    setSize: function (param) {
       
        UCML.FitLayout.superclass.setSize.call(this, param, "FitLayout");
        var opts = this;

        //适应高度模式
        var sumHeight = this.el.height();
        if (sumHeight <= 0) {
            return;
        }

        for (var i = 0; i < this.items.length; i++) {
            var cmObj = this.items[i];
            var height = cmObj.getHeight();
            if (i < this.items.length - 1 && sumHeight >= height) {
                cmObj.resize(null, "FitLayout");
                sumHeight = sumHeight - cmObj.getOuterHeight(true);
            }
            else {
                cmObj.resize({ height: sumHeight, width: this.getWidth() }, "FitLayout");
            }
        }
    }
});


UCML.Layout = function (id) {
    UCML.Layout.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Layout, UCML.Container, {
    alignWidth: true, //适应宽
    alignHeight: true, //适应高
    onRender: function () {

        if (this.tagName == 'body') {
            $('html').css({
                height: '100%',
                overflow: 'hidden'
            });
            $('body').css({
                height: '100%',
                overflow: 'hidden',
                border: 'none'
            });
        }

        this.cls = "layout " + this.cls;
        this.el.css({
            margin: 0,
            padding: 0
        });
        UCML.Layout.superclass.onRender.call(this);

        $('<div class="layout-split-proxy-h"></div>').appendTo(this.el);
        $('<div class="layout-split-proxy-v"></div>').appendTo(this.el);

        var panels = {
            center: this.createPanel('center')
        };

        panels.north = this.createPanel('north');
        panels.south = this.createPanel('south');
        panels.east = this.createPanel('east');
        panels.west = this.createPanel('west');

        this.panels = panels;
    }
    , createPanel: function (dir) {
        var opts = this;
        var pp = $('>div[region=' + dir + ']', this.el);

        var toolCls = null;
        if (dir == 'north') {
            toolCls = 'layout-button-up';
        } else if (dir == 'south') {
            toolCls = 'layout-button-down';
        } else if (dir == 'east') {
            toolCls = 'layout-button-right';
        } else if (dir == 'west') {
            toolCls = 'layout-button-left';
        }

        var cls = 'layout-panel layout-panel-' + dir;
        if (pp.attr('split') == 'true') {
            cls += ' layout-split-' + dir;
        }
        pp = new UCML.Panel({
            cls: cls,
            doSize: false,
            el: pp,
            bodyCls: "layout-body",
            isSetProperties: true,
            border: (pp.attr('border') == 'false' ? false : true),
            tools: [{
                iconCls: toolCls,
                handler: function () {
                    opts.collapsePanel(dir);
                }
            }]
        });

        if (pp.el.attr('split') == 'true') {
            var panel = pp.el;

            var handles = '';
            if (dir == 'north') handles = 's';
            if (dir == 'south') handles = 'n';
            if (dir == 'east') handles = 'w';
            if (dir == 'west') handles = 'e';

            panel.resizable({
                handles: handles,
                onStartResize: function (e) {
                    resizing = true;

                    if (dir == 'north' || dir == 'south') {
                        var proxy = $('>div.layout-split-proxy-v', opts.el);
                    } else {
                        var proxy = $('>div.layout-split-proxy-h', opts.el);
                    }
                    var top = 0, left = 0, width = 0, height = 0;
                    var pos = { display: 'block' };
                    if (dir == 'north') {
                        pos.top = parseInt(panel.css('top')) + panel.outerHeight() - proxy.height();
                        pos.left = parseInt(panel.css('left'));
                        pos.width = panel.outerWidth();
                        pos.height = proxy.height();
                    } else if (dir == 'south') {
                        pos.top = parseInt(panel.css('top'));
                        pos.left = parseInt(panel.css('left'));
                        pos.width = panel.outerWidth();
                        pos.height = proxy.height();
                    } else if (dir == 'east') {
                        pos.top = parseInt(panel.css('top')) || 0;
                        pos.left = parseInt(panel.css('left')) || 0;
                        pos.width = proxy.width();
                        pos.height = panel.outerHeight();
                    } else if (dir == 'west') {
                        pos.top = parseInt(panel.css('top')) || 0;
                        pos.left = panel.outerWidth() - proxy.width();
                        pos.width = proxy.width();
                        pos.height = panel.outerHeight();
                    }
                    proxy.css(pos);

                    $('<div class="layout-mask"></div>').css({
                        left: 0,
                        top: 0,
                        width: opts.el.width(),
                        height: opts.el.height()
                    }).appendTo(opts.el);
                },
                onResize: function (e) {
                    if (dir == 'north' || dir == 'south') {
                        var proxy = $('>div.layout-split-proxy-v', opts.el);
                        proxy.css('top', e.pageY - opts.el.offset().top - proxy.height() / 2);
                    } else {
                        var proxy = $('>div.layout-split-proxy-h', opts.el);
                        proxy.css('left', e.pageX - opts.el.offset().left - proxy.width() / 2);
                    }
                    return false;
                },
                onStopResize: function (e) {

                    $('>div.layout-split-proxy-v', opts.el).css('display', 'none');
                    $('>div.layout-split-proxy-h', opts.el).css('display', 'none');

                    pp.width = e.data.width;
                    pp.height = e.data.height;
                    pp.left = e.data.left;
                    pp.top = e.data.top;
                    pp.resize();
                    opts.setSize();
                    resizing = false;
                    opts.el.find('>div.layout-mask').remove();
                }
            });
        }
        return pp;
    }
    ,
    //私有的
    isPanelVisible: function (pp) {
        if (!pp) return false;
        if (pp) {
            return pp.el.is(':visible');
        } else {
            return false;
        }
    }
    , setSize: function (param) {
        UCML.Layout.superclass.setSize.call(this, param);
        var opts = this;

        var panels = this.panels;

        var cpos = {
            top: 0,
            left: 0,
            width: this.el.width(),
            height: this.el.height()
        };

        // set north panel size
        function setNorthSize(pp) {

            if (!pp) return;
            pp.resize({
                width: opts.el.width(),
                height: pp.height,
                left: 0,
                top: 0
            });
            cpos.top += Number(pp.height);
            cpos.height -= Number(pp.height);
        }
        if (opts.isPanelVisible(panels.expandNorth)) {
            setNorthSize(panels.expandNorth);
        } else {
            setNorthSize(panels.north);
        }

        // set south panel size
        function setSouthSize(pp) {

            if (!pp) return;
            pp.resize({
                width: opts.el.width(),
                height: pp.height,
                left: 0,
                top: opts.el.height() - pp.height
            });
            cpos.height -= Number(pp.height);
        }
        if (opts.isPanelVisible(panels.expandSouth)) {
            setSouthSize(panels.expandSouth);
        } else {
            setSouthSize(panels.south);
        }

        // set east panel size
        function setEastSize(pp) {
            if (!pp) return;
            pp.resize({
                width: pp.width,
                height: cpos.height,
                left: opts.el.width() - pp.width,
                top: cpos.top
            });
            cpos.width -= Number(pp.width);
        }
        if (opts.isPanelVisible(panels.expandEast)) {
            setEastSize(panels.expandEast);
        } else {
            setEastSize(panels.east);
        }

        // set west panel size
        function setWestSize(pp) {
            if (!pp) return;
            pp.resize({
                width: pp.width,
                height: cpos.height,
                left: 0,
                top: cpos.top
            });
            cpos.left += Number(pp.width);
            cpos.width -= Number(pp.width);
        }
        if (opts.isPanelVisible(panels.expandWest)) {
            setWestSize(panels.expandWest);
        } else {
            setWestSize(panels.west);
        }

        panels.center.resize(cpos);
    },
    collapsePanel: function (region) {
        var panels = this.panels;
        var opts = this;

        function createExpandPanel(dir) {
            var icon;
            if (dir == 'east') icon = 'layout-button-left'
            else if (dir == 'west') icon = 'layout-button-right'
            else if (dir == 'north') icon = 'layout-button-down'
            else if (dir == 'south') icon = 'layout-button-up';

            var p = $('<div></div>').appendTo(opts.el);

            p = new UCML.Panel({
                cls: 'layout-expand',
                title: '&nbsp;',
                el: p,
                closed: true,
                doSize: false,
                tools: [{
                    iconCls: icon,
                    handler: function () {
                        opts.expandPanel(region);
                    }
                }]
            });
            p.el.hover(
				function () { $(this).addClass('layout-expand-over'); },
				function () { $(this).removeClass('layout-expand-over'); }
			);
            return p;
        }

        if (region == 'east') {
            if (panels.east.onBeforeCollapse.call(panels.east) == false) return;

            panels.center.resize({
                width: panels.center.width + panels.east.width - 28
            });
            panels.east.el.animate({ left: opts.el.width() }, function () {
                panels.east.close();
                panels.expandEast.open();
                panels.expandEast.resize({
                    top: panels.east.top,
                    left: opts.el.width() - 28,
                    width: 28,
                    height: panels.east.height
                });
                panels.east.onCollapse.call(panels.east);
            });
            if (!panels.expandEast) {
                panels.expandEast = createExpandPanel('east');
                panels.expandEast.el.click(function () {
                    panels.east.open();
                    panels.east.resize({ left: opts.el.width() });
                    panels.east.el.animate({
                        left: opts.el.width() - panels.east.width
                    });
                    return false;
                });
            }
        } else if (region == 'west') {

            if (panels.west.onBeforeCollapse.call(panels.west) == false) return;

            panels.center.resize({
                width: panels.center.width + panels.west.width - 28,
                left: 28
            });
            panels.west.el.animate({ left: -panels.west.width }, function () {
                panels.west.close();
                panels.expandWest.open();
                panels.expandWest.resize({
                    top: panels.west.top,
                    left: 0,
                    width: 28,
                    height: panels.west.height
                });
                panels.west.onCollapse.call(panels.west);
            });
            if (!panels.expandWest) {
                panels.expandWest = createExpandPanel('west');
                panels.expandWest.el.click(function () {
                    panels.west.open();
                    panels.west.resize({ left: -panels.west.width });
                    panels.west.el.animate({
                        left: 0
                    });
                    return false;
                });
            }
        } else if (region == 'north') {
            if (panels.north.onBeforeCollapse.call(panels.north) == false) return;

            var hh = this.el.height() - 28;
            if (opts.isPanelVisible(panels.expandSouth)) {
                hh -= panels.expandSouth.height;
            } else if (opts.isPanelVisible(panels.south)) {
                hh -= panels.south.height;
            }
            panels.center.resize({ top: 28, height: hh });
            panels.east.resize({ top: 28, height: hh });
            panels.west.resize({ top: 28, height: hh });
            if (opts.isPanelVisible(panels.expandEast)) panels.expandEast.resize({ top: 28, height: hh });
            if (opts.isPanelVisible(panels.expandWest)) panels.expandWest.resize({ top: 28, height: hh });

            panels.north.el.animate({ top: -panels.north.height }, function () {
                panels.north.close();
                panels.expandNorth.open();
                panels.expandNorth.resize({
                    top: 0,
                    left: 0,
                    width: opts.el.width(),
                    height: 28
                });
                panels.north.onCollapse.call(panels.north);
            });
            if (!panels.expandNorth) {
                panels.expandNorth = createExpandPanel('north');
                panels.expandNorth.el.click(function () {
                    panels.north.open();
                    panels.north.resize({ top: -panels.north.height });
                    panels.north.el.animate({ top: 0 });
                    return false;
                });
            }
        } else if (region == 'south') {
            if (panels.south.onBeforeCollapse.call(panels.south) == false) return;

            var hh = opts.el.height() - 28;
            if (opts.isPanelVisible(panels.expandNorth)) {
                hh -= panels.expandNorth.height;
            } else if (opts.isPanelVisible(panels.north)) {
                hh -= panels.north.height;
            }
            panels.center.resize({ height: hh });
            panels.east.resize({ height: hh });
            panels.west.resize({ height: hh });
            if (opts.isPanelVisible(panels.expandEast)) panels.expandEast.resize({ height: hh });
            if (opts.isPanelVisible(panels.expandWest)) panels.expandWest.resize({ height: hh });

            panels.south.el.animate({ top: opts.el.height() }, function () {
                panels.south.close();
                panels.expandSouth.open();
                panels.expandSouth.resize({
                    top: opts.el.height() - 28,
                    left: 0,
                    width: opts.el.width(),
                    height: 28
                });
                panels.south.onCollapse.call(panels.south);
            });
            if (!panels.expandSouth) {
                panels.expandSouth = createExpandPanel('south');
                panels.expandSouth.el.click(function () {
                    panels.south.open();
                    panels.south.resize({ top: opts.el.height() });
                    panels.south.el.animate({ top: opts.el.height() - panels.south.height });
                    return false;
                });
            }
        }
    }
    ,
    expandPanel: function (region) {
        var panels = this.panels;
        var cc = this.el;
        var opts = this;
        if (region == 'east' && panels.expandEast) {
            if (panels.east.onBeforeExpand.call(panels.east) == false) return;

            panels.expandEast.close();
            panels.east.el.stop(true, true);
            panels.east.open();
            panels.east.resize({ left: cc.width() });
            panels.east.el.animate({
                left: cc.width() - panels.east.width
            }, function () {
                opts.setSize();
                panels.east.onExpand.call(panels.east);
            });
        } else if (region == 'west' && panels.expandWest) {
            if (panels.west.onBeforeExpand.call(panels.west) == false) return;

            panels.expandWest.close();
            panels.west.el.stop(true, true);
            panels.west.open();
            panels.west.resize({ left: -panels.west.width });
            panels.west.el.animate({
                left: 0
            }, function () {
                opts.setSize();
                panels.west.onExpand.call(panels.west);
            });
        } else if (region == 'north' && panels.expandNorth) {
            if (panels.north.onBeforeExpand.call(panels.north) == false) return;

            panels.expandNorth.close();
            panels.north.el.stop(true, true);
            panels.north.open();
            panels.north.resize({ top: -panels.north.height });
            panels.north.el.animate({ top: 0 }, function () {
                opts.setSize();
                panels.north.onExpand.call(panels.north);
            });
        } else if (region == 'south' && panels.expandSouth) {
            if (panels.south.onBeforeExpand.call(panels.south) == false) return;
            panels.expandSouth.close();
            panels.south.el.stop(true, true);
            panels.south.open();
            panels.south.resize({ top: cc.height() });
            panels.south.el.animate({ top: cc.height() - panels.south.height }, function () {
                opts.setSize();
                panels.south.onExpand.call(panels.south);
            });
        }
    }
});
