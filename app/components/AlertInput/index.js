import React from 'react';
import {
  StyleSheet, View, ViewPropTypes,Modal,Text, TouchableOpacity
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';
import { TextInput } from '../InputItem/TextInput';

/**
 * Created by wanghy on 2020/9/14
 */

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  loadingContainer: {
    backgroundColor: 'white',
    borderRadius: 5,
    overflow:'hidden',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '70%',
    height:180,
  },
  title: {
    height:50,
    width:'100%',
    justifyContent:'center',
    alignItems:'center',
    borderBottomColor:'#666',
    borderBottomWidth:1
  },
  bottom: {
    flexDirection:'row',
    height:45
  },
  button:{
    width:'50%',
    justifyContent:'center',alignItems:'center',
    backgroundColor:'#DC001B'
  },
  input: {
    borderColor:'#e0e0e0',
    borderWidth:1,
    width:100,
    height:30
  },
  contentView:{
    flexDirection:'row',
    justifyContent:'space-around',
    width:'100%',
    alignItems:'center',
    paddingHorizontal:20
  }
});

class AlertInput extends React.PureComponent {
  constructor(props){
    super(props)
    this.state={
      reject:''
    }
  }
  render() {
    const {type,backgroundStyle,showAlert,clickCanel,clickConfirm,original,data} = this.props    
    return (
      <Modal
          animationType="none"
          transparent={true}
          visible={showAlert}
      >
        <View style={StyleSheet.flatten([styles.background,backgroundStyle])}>
          <View style={styles.loadingContainer}>

            <View style={styles.title}>
              <Text style={{color:'#666',fontSize:16}}>{'修改'+type}</Text>
            </View>

            <View style={styles.contentView}>
              <Text style={{color:'#666',fontSize:15}}>{original}</Text>
              <FontAwesome
                name={'angle-double-right'}
                size={25}
                style={{color: '#666'}}
              />
              <TextInput
                autoFocus
                style={styles.input}
                placeholder='请输入'
                value={this.state.reject}
                onChangeText={(text) => {
                  this.setState({ reject: text.replace(/\s/g,"")});
                }}
              />
            </View>
    
            <View style={styles.bottom}>
              <TouchableOpacity 
                style={[styles.button,{backgroundColor:'#FBB03B'}]}
                onPress={()=>{
                  this.setState({reject:'',})
                  clickCanel()
                }}
              >
                <Text style={{color:'#fff',fontSize:16}}>取消</Text>
              </TouchableOpacity>
              <TouchableOpacity 
                style={styles.button}
                onPress={()=>{clickConfirm({
                  newDate:this.state.reject,
                  type,
                  originaldata:data
                })}}
              >
                <Text style={{color:'#fff',fontSize:16}}>确定</Text>
              </TouchableOpacity>
            </View>
            
          </View>
        </View>
      </Modal>
    );
  }
}

AlertInput.propTypes = {
  backgroundStyle: ViewPropTypes.style,
  indicatorColor: PropTypes.string,
  type:PropTypes.string,
  clickCanel: PropTypes.func,
  clickConfirm: PropTypes.func,
  showAlert: PropTypes.bool,
  original:PropTypes.string,
  data:PropTypes.object
};

AlertInput.defaultProps = {
  backgroundStyle: undefined,
  indicatorColor: '#DC001B',
  type:null,
  showAlert:false,
  original:'原数据',
  data:{}
};

export default AlertInput;
