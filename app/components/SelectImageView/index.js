/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, Modal, ActivityIndicator, View, ViewPropTypes, Image } from 'react-native';
import { Icon } from 'native-base';
import _ from 'lodash';
import ImageViewer from 'react-native-image-zoom-viewer';
import FastImage from 'react-native-fast-image';
import CustomImagePicker from '../CustomImagePicker';

/**
 * Created by ocean on 2018/4/23
 */

const styles = StyleSheet.create({
  imageStyle: {
    width: '100%',
    height: '100%',
    maxWidth: 100,
    maxHeight: 100,
  },
  imageTouch: {
    width: '100%',
    height: '100%',
    maxWidth: 100,
    maxHeight: 100,
    backgroundColor: '#E0E0E0',
    marginLeft: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomColor: 'transparent',
  },
  label: {},
  text: {
    flex: 2,
  },
  item: {
    flex: 1,
    borderColor: 'transparent',
  },
  itemImage: {
    flex: 1,
    borderColor: 'transparent',
    paddingTop: 10,
    maxWidth: '100%',
    maxHeight: '100%',
  },
  container: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
  },
  deleteIcon: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: 30,
    width: 30,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const SelectImageSourcePropType = PropTypes.shape({
  uri: PropTypes.string,
});

class SelectImageView extends React.Component {
  static propTypes = {
    // title: PropTypes.string,
    onPickImages: PropTypes.func,
    onPress: PropTypes.func,
    onDelete: PropTypes.func,
    options: PropTypes.object,
    source: PropTypes.oneOfType([SelectImageSourcePropType, PropTypes.string]),
    readonly: PropTypes.bool,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    options: {},
    // title: '选择图片',
    onPickImages: undefined,
    onPress: undefined,
    onDelete: undefined,
    source: undefined,
    style: undefined,
    readonly: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      loaded: false,
      loading: false,
    };
  }

  onDelete = () => {
    if (this.props.onDelete) this.props.onDelete();
  };

  picker: CustomImagePicker;

  renderImage() {
    if (this.props.source) {
      const source = { uri: 'empty' };
      // if (_.isString(this.props.source) && this.props.source !== '') {
      //   source.uri = this.props.source;
      // } else if (_.isString(this.props.source.uri) && this.props.source.uri !== '') {
      //   source.uri = this.props.source.uri;
      // }

      //加个判断,如果图片地址存在,但是没有图片,既连接后4位是null
      // if (_.isString(this.props.source) && this.props.source !== '' && this.props.source.substring(this.props.source.length - 4) !== 'null') {
      //   source.uri = this.props.source;
      // } else if (_.isString(this.props.source.uri) && this.props.source.uri !== '' && this.props.source.uri.substring(this.props.source.uri.length - 4) !== 'null') {
      //   source.uri = this.props.source.uri;
      // } else {
      //   return (
      //     <Image
      //       source={require('../../assets/image_select.png')}
      //       style={styles.imageStyle}
      //       resizeMode="cover"
      //     />
      //   )
      // }
      const uri = this.props.source && this.props.source.uri || this.props.source;
      if (_.isString(uri) && uri.length > 0 && uri.substring(uri.length - 4) !== 'null') {
        source.uri = uri;
      } else {
        return (
          <Image
            source={require('../../assets/image_select.png')}
            style={styles.imageStyle}
            resizeMode="cover"
          />
        )
      }
      return (
        <View>
          <FastImage
            source={source}
            style={styles.imageStyle}
            resizeMode={FastImage.resizeMode.cover}
            onLoadStart={() => {
              if (!this.state.loaded) {
                this.setState({ loading: true });
              }
            }}
            onLoadEnd={() => {
              this.setState({ loading: false, loaded: true });
            }}
            onError={() => {
              this.setState({ loading: false });
            }}
          />
          {this.state.loading && (
            <View style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            >
              <ActivityIndicator />
            </View>
          )}
        </View>
      );
    }
    return (
      <Image
        source={require('../../assets/image_select.png')}
        style={styles.imageStyle}
        resizeMode="cover"
      />
    );
  }

  renderDelete() {
    if (this.props.source && this.props.onDelete) {
      return (
        <TouchableOpacity style={styles.deleteIcon} onPress={this.onDelete}>
          <Icon active style={{ color: '#ff171a', fontSize: 18 }} name="trash" />
        </TouchableOpacity>
      );
    }
    return null;
  }

  renderPicker() {
    return (
      <CustomImagePicker
        ref={(ref) => {
          this.picker = ref;
        }}
        options={this.props.options}
        onPickImages={this.props.onPickImages}
      />
    );
  }

  renderImageLoading = () => (
    <View style={{
      justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%',
    }}
    >
      <ActivityIndicator />
    </View>
  );

  renderPreView() {
    // if (this.props.source && !this.props.onPress) {
    //   if (this.props.source && !this.props.onPress && this.props.source.substring(this.props.source.length - 4) !== 'null') { //加个判断,如果图片地址存在,但是没有图片,既连接后4位是null
    const uri = this.props.source && this.props.source.uri || this.props.source;
    if (_.isString(uri) && uri.length > 0 && uri.substring(uri.length - 4) !== 'null' && !this.props.onPress) {
      return (
        <Modal visible={this.state.visible} transparent>
          <ImageViewer
            imageUrls={[{
              url: uri,
            }]}
            onClick={() => {
              this.setState({ visible: false });
            }}
            onSwipeDown={() => {
              this.setState({ visible: false });
            }}
            loadingRender={this.renderImageLoading}
          />
        </Modal>
      );
    }
    return null;
  }

  render() {
    return (
      <TouchableOpacity
        style={[styles.imageTouch, this.props.style]}
        onPress={() => {
          if (this.props.source) {
            if (this.props.onPress) {
              this.props.onPress();
            } else {
              this.setState({ visible: true });
            }
          } else if (this.props.onPickImages && this.picker) {
            this.picker.show();
          }
        }}
      >
        {this.renderImage()}
        {!this.props.readonly && this.renderDelete()}
        {!this.props.readonly && this.renderPicker()}
        {this.renderPreView()}
      </TouchableOpacity>
    );
  }
}

export default SelectImageView;
