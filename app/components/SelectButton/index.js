import React from 'react';
import {
  StyleSheet, TouchableOpacity, View, Keyboard,
} from 'react-native';
import PropTypes from 'prop-types';
import { Text } from 'native-base';
import myTheme from '../../Themes';
import Svg from '../Svg/index';

const styles = StyleSheet.create({
  button: {
    flex: 1,
    marginTop: 0,
    flexDirection: 'row',
    marginBottom: 0,
    padding: 0,
    height: '100%',
    alignItems: 'center',
  },
  text: {
    flexDirection: 'row',
    fontSize: myTheme.inputFontSize,
    color: myTheme.inputColor,
    textAlign: 'center',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  placeholder: {
    flexDirection: 'row',
    fontSize: myTheme.inputFontSize,
    color: myTheme.inputColor,
    textAlign: 'center',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  afterIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});

class SelectButton extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  renderText = () => {
    if (this.props.value && this.props.value !== '') {
      return (
        <Text style={styles.text}>
          {this.props.value}
        </Text>
      );
    }
    return (
      <Text style={styles.placeholder}>
        {this.props.readOnly ? undefined : this.props.placeholder}
      </Text>
    );
  };

  render() {
    return this.props.readOnly ? (
      <View style={styles.button}>
        {this.renderText()}
        <Svg
          icon="selectAfter"
          size={10}
          style={{
            position: 'absolute',
            bottom: 0,
            right: 0,
          }}
        />
      </View>
    ) : (
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          Keyboard.dismiss();
          if (this.props.onPress) this.props.onPress();
        }}
      >
        {
          this.renderText()
        }
        <Svg
          icon="selectAfter"
          size={10}
          color="#969696"
          style={{
            position: 'absolute',
            bottom: 0,
            right: 0,
          }}
        />
      </TouchableOpacity>
    );
  }
}

SelectButton.propTypes = {
  value: PropTypes.string,
  readOnly: PropTypes.bool,
  placeholder: PropTypes.string,
  onPress: PropTypes.func,
};
SelectButton.defaultProps = {
  value: undefined,
  readOnly: false,
  placeholder: undefined,
  onPress: undefined,
};

export default SelectButton;
