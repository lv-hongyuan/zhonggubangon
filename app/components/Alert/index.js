import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, PixelRatio, Platform } from 'react-native';
import Overlay from 'teaset/components/Overlay/Overlay';
import Svg from '../Svg';

/**
 * Created by ocean on 2018/4/8
 */

const borderWidth = 1 / PixelRatio.getPixelSizeForLayoutSize(1);

const styles = {
  container: {
    backgroundColor: '#fff',
    width: 260,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  image: {
    marginTop: 20,
  },
  title: {
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    color: '#ED1727',
    textAlign: 'left',
  },
  message: {
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    color: '#ED1727',
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 20,
    flexDirection: 'row',
    height: 50,
  },
  button: {
    flex: 1,
    height: 51,
    borderColor: '#868686',
    borderTopWidth: borderWidth,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {},
};

export const AlertViewType = {
  Info: 0,
};

const AlertList = [];

function addAlert(popView) {
  AlertList.push(popView);
}

function closeAlert(popView) {
  if (popView) {
    popView.close();
    const index = AlertList.indexOf(popView);
    if (index !== -1) {
      AlertList.splice(index, 1);
    }
  }
}

function closeAllAlert() {
  AlertList.forEach((popView) => {
    if (popView) popView.close();
  });
  AlertList.splice(0, AlertList.length);
}

class AlertView extends React.Component {
  static propTypes = {
    show: PropTypes.bool,
    title: PropTypes.string,
    message: PropTypes.string,
    component: PropTypes.func,
    showCancel: PropTypes.bool,
    cancelTitle: PropTypes.string,
    cancelAction: PropTypes.func,
    confirmTitle: PropTypes.string,
    confirmAction: PropTypes.func,
  };

  static defaultProps = {
    show: false,
    title: null,
    message: null,
    component: undefined,
    type: AlertViewType.Info,
    showCancel: false,
    cancelTitle: '取消',
    cancelAction: null,
    confirmTitle: '确认',
    confirmAction: null,
  };

  static show(option: {
    show: PropTypes.bool,
    title: PropTypes.string,
    message: PropTypes.string,
    component: PropTypes.func,
    type: PropTypes.number,
    showCancel: PropTypes.bool,
    cancelTitle: PropTypes.string,
    cancelAction: PropTypes.func,
    confirmTitle: PropTypes.string,
    confirmAction: PropTypes.func
  }) {
    const options = Object.assign({}, AlertView.defaultProps, option, { show: true });
    let popView: Overlay.PopView;
    const overlayView = (
      <Overlay.PopView
        modal
        autoKeyboardInsets={Platform.OS === 'ios'}
        ref={(ref) => {
          popView = ref;
          addAlert(popView);
        }}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <View style={styles.container}>
          <Svg style={styles.image} icon="alert_warning" size={40}/>
          {options.title ? <Text style={styles.title}>{options.title}</Text> : null}
          {options.message ? <Text style={styles.message}>{options.message}</Text> : null}
          {option.component && options.component()}
          <View style={styles.buttonContainer}>
            {options.showCancel ?
              <TouchableOpacity
                style={[styles.button, {
                  backgroundColor: 'white',
                  borderRightWidth: borderWidth,
                }]}
                onPress={() => {
                  closeAlert(popView);
                  if (options.cancelAction) options.cancelAction();
                }}
              >
                <Text style={[styles.buttonText, { color: '#535353' }]}>{options.cancelTitle}</Text>
              </TouchableOpacity> : null
            }
            <TouchableOpacity
              style={[styles.button, { backgroundColor: '#ED1727' }]}
              onPress={() => {
                if (options.confirmAction) {
                  let flag = options.confirmAction();
                  if (flag !== false) {
                    closeAlert(popView);
                  }
                } else { //没有传入 confirmAction 时，默认确认关闭
                  closeAlert(popView);
                }
              }}
            >
              <Text style={[styles.buttonText, { color: 'white' }]}>{options.confirmTitle}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Overlay.PopView>
    );
    return Overlay.show(overlayView);
  }

  static closeAll() {
    closeAllAlert();
  }

  componentDidMount() {
    if (this.props.show) {
      requestAnimationFrame(() => {
        this.overlayPopView = this.show();
      });
    }
  }

  overlayPopView: number;

  show() {
    const {
      title,
      message,
      component,
      showCancel,
      cancelTitle,
      confirmTitle,
    } = this.props;
    const overlayView = (
      <Overlay.PopView
        modal
        autoKeyboardInsets={Platform.OS === 'ios'}
        ref={(ref) => {
          this.popView = ref;
        }}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <View style={styles.container}>
          <Svg style={styles.image} icon="alert_warning" size={40}/>
          {title ? <Text style={styles.title}>{title}</Text> : null}
          {message ? <Text style={styles.message}>{message}</Text> : null}
          {component && component()}
          <View style={styles.buttonContainer}>
            {showCancel ?
              <TouchableOpacity
                style={[styles.button, {
                  backgroundColor: 'white',
                  borderRightWidth: borderWidth,
                }]}
                onPress={() => {
                  if (this.popView) this.popView.close();
                  if (this.props.cancelAction) this.props.cancelAction();
                }}
              >
                <Text style={[styles.buttonText, { color: '#535353' }]}>{cancelTitle}</Text>
              </TouchableOpacity> : null
            }
            <TouchableOpacity
              style={[styles.button, { backgroundColor: '#ED1727' }]}
              onPress={() => {
                if (this.popView) this.popView.close();
                if (this.props.confirmAction) this.props.confirmAction();
              }}
            >
              <Text style={[styles.buttonText, { color: 'white' }]}>{confirmTitle}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Overlay.PopView>
    );
    return Overlay.show(overlayView);
  }

  render() {
    return null;
  }
}

export default AlertView;
