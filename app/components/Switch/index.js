import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  View, Text, StyleSheet, TouchableHighlight, Keyboard, ViewPropTypes,
} from 'react-native';
import myTheme from '../../Themes';

const styles = StyleSheet.create({
  defaultStyles: {
    width: 80,
    height: 40,
    borderRadius: 3,
  },
  container: {
    overflow: 'hidden',
    borderWidth: myTheme.borderWidth,
    borderRadius: 3,
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  segText: {
    color: '#ffffff',
    textAlign: 'center',
    // lineHeight: 28,
    flex: 1,
  },
  segItem: {
    borderWidth: myTheme.borderWidth,
    borderRadius: 3,
    backgroundColor: '#ffffff',
    // flex: 1,
    height: '100%',
    width: 30,
  },
});

class Switch extends PureComponent {
  renderView() {
    const tintColor = this.props.value ? this.props.onColor : this.props.offColor;
    return (
      <View style={[styles.container, { backgroundColor: tintColor, borderColor: tintColor }]}>
        {this.props.value ? (
          <Text style={styles.segText}>{this.props.onTitle}</Text>
        ) : (
          <View style={[styles.segItem, { borderColor: tintColor }]} />
        )}
        {this.props.value ? (
          <View style={[styles.segItem, { borderColor: tintColor }]} />
        ) : (
          <Text style={styles.segText}>{this.props.offTitle}</Text>
        )}
      </View>
    );
  }

  render() {
    return this.props.readOnly ? (
      <View style={[styles.defaultStyles, this.props.style]}>
        {this.renderView()}
      </View>
    ) : (
      <TouchableHighlight
        onPress={() => {
          Keyboard.dismiss();
          this.props.onPress();
        }}
        style={[styles.defaultStyles, this.props.style]}
      >
        {this.renderView()}
      </TouchableHighlight>
    );
  }
}

Switch.propTypes = {
  onPress: PropTypes.func.isRequired,
  style: ViewPropTypes.style,
  value: PropTypes.bool.isRequired,
  readOnly: PropTypes.bool,
  onColor: PropTypes.string,
  offColor: PropTypes.string,
  onTitle: PropTypes.string,
  offTitle: PropTypes.string,
};

Switch.defaultProps = {
  onColor: '#DC001B',
  offColor: '#969696',
  onTitle: '是',
  offTitle: '否',
  style: undefined,
  readOnly: false,
};

export default Switch;
