
//  通知中心  网络监听 ** 推送监听 ** 桌面角标控制

import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform, NetInfo, DeviceEventEmitter, AppState,
} from 'react-native';
import JPushModule from 'jpush-react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { bindActionCreators } from 'redux';
import { ROUTE_TO_DO } from '../../apps/BusinessApproval/RouteConstant';
import { ROUTE_OA_HOME } from '../../apps/OA/RouteConstant';
import { ROUTE_LOGIN } from '../../RouteConstant';
import { makeIsLogin } from '../../containers/AppInit/selectors';
import { bindRegistrationIDPromiseCreator } from '../../containers/AppInit/actions';
import { sendRegistrationIdRoutine } from './actions';
import AppStorage from '../../utils/AppStorage';
import { READY_TO_HANDLE_LAUNCH_NOTIFICATION } from './constants';
import AlertView from '../Alert';
import { ROUTE_INSTRUCTIONS, ROUTE_BULLETIN_CONTENT,ROUTE_TASKPROCESS_DETAIL_NOTIFICATION } from '../../apps/onLand/RouteConstant';
import NavigatorService from '../../NavigatorService';

const receiveCustomMsgEvent = 'receivePushMsg';
const receiveNotificationEvent = 'receiveNotification';
const openNotificationEvent = 'openNotification';

class NotificationCenter extends React.Component {
  static setBadge(num) {
    if (Platform.OS === 'ios') {
      JPushModule.setBadge(num, () => {
      });
    }
  }

  constructor(props) {
    super(props);

    this.state = {
      resolvedLaunchAppNotification: false,
      isConnect: false,
    };
  }

  componentDidMount() {
    // 初始化推送
    if (Platform.OS === 'android') {
      JPushModule.initPush();
      JPushModule.notifyJSDidLoad((resultCode) => {
        console.log(resultCode);
      });
    } else {
      JPushModule.setupPush();
    }

    // 添加监听
    this.addListener();

    // 监听网络
    NetInfo.isConnected.addEventListener('connectionChange', this.netChange);

    JPushModule.getRegistrationID(this.onGetRegistrationId);
  }

  componentWillReceiveProps(nextProps) {
    // 登录后
    if (nextProps.isLogin && !this.props.isLogin) {
      // 获取RegistrationId
      JPushModule.getRegistrationID(this.onGetRegistrationId);
    }
  }

  componentWillUnmount() {
    this.removeListener();
    NetInfo.removeEventListener('connectionChange', this.netChange);
  }

  // 收到自定义消息
  onReceiveCustomMsgListener = (map) => {
    console.log(`extras: ${map.extras}`);
  };

  // 收到通知
  onReceiveNotification = (map) => {
    console.log('onReceiveNotification: ', map);
    const self = this;
    try {
      const extra = Platform.OS === 'ios' ? map.extras : JSON.parse(map.extras);
      const content = Platform.OS === 'ios' ? map.aps.alert : map.alertContent;
      const messageId = Platform.OS === 'ios' ? map._j_msgid : map.id;
      if (AppState.currentState === 'active' && content && content.length > 0) {
        if (extra.url) {
          DeviceEventEmitter.emit('refreshDesktopMarkState',{data:extra.type})
          console.log('提醒返回的url：' + extra.url + '提醒返回的type：' + extra.type);
          if(extra.type == 'voyageNotice' || 'notice'){
          }
          if (extra.type === 'YWDB') {
            // 清除提醒
            self.reduceBadge(messageId);
            return;
          }
          AlertView.show({
            message: content,
            showCancel: true,
            confirmTitle: '查看',
            confirmAction: () => {
              AlertView.closeAll();
              self.jumpSecondActivity(extra);
              if(Platform.OS === 'android') JPushModule.clearNotificationById(messageId);
            },
          });
        } else {
          AlertView.show({
            message: content,
            showCancel: true,
            confirmAction: () => {
              self.reduceBadge(messageId);
            },
          });
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  // 点击通知
  onReceiveOpenNotification = (map) => {
    console.log('Opening notification!', map);
    console.log(`map.extra: ${map.extras}`);
    try {
      const extra = Platform.OS === 'ios' ? map.extras : JSON.parse(map.extras);
      const messageId = Platform.OS === 'ios' ? map._j_msgid : map.id;
      if (extra.url) {
        AlertView.closeAll();
        this.jumpSecondActivity(extra);
        if(Platform.OS === 'android') JPushModule.clearNotificationById(messageId);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // 获取RegistrationId
  onGetRegistrationId = (registrationId) => {
    console.log(`Device register succeed, registrationId ${registrationId}`);
    this.props.sendRegistrationIdRoutine(registrationId);
    this.bindRegistrationID(registrationId);
  };

  resolveLaunchAppNotification = () => {
    if (this.state.resolvedLaunchAppNotification) {
      return;
    }
    this.setState({ resolvedLaunchAppNotification: true });
    if (Platform.OS === 'ios') {
      JPushModule.getLaunchAppNotification((notification) => {
        if (notification === undefined) {
          // application was launch by tap app's icon
        } else if (notification.aps === undefined) {
          // application was launch by tap local notification
        } else {
          // application was launch by tap remote notification
          this.onReceiveOpenNotification(notification);
        }
      });
    }
  };


  jumpSecondActivity = (map) => {
    if (!map.url) {
      return;
    }

    let action;
    switch (map.type) {
      case 'OA':
        action = NavigationActions.navigate({
          routeName: ROUTE_OA_HOME,
          params: {
            path: map.url,
          },
        });
        break;
      case 'PortWork':
        action = NavigationActions.navigate({
          routeName: ROUTE_INSTRUCTIONS,
          params: {
            // path: map.url,
          },
        });
        break;
      case 'notice':
        console.log('点击了查看公告详情');
        DeviceEventEmitter.emit('refreshMessageState',{id:map.id,isPush:1,title:map.title})
        action = NavigationActions.navigate({
          routeName: ROUTE_BULLETIN_CONTENT,
          params: {
            // path: map.url,
            id:map.id,
            isPush: 1,
            title:map.title
          },
        });
        // DeviceEventEmitter.emit('getPush',{ id:map.id });
        break;
      case 'voyageNotice':
        action = NavigationActions.navigate({
          routeName: ROUTE_BULLETIN_CONTENT,
          params: {
            // path: map.url,
            id:map.id,
            isPush: 1,
            title:map.title
          },
        });
        // DeviceEventEmitter.emit('getPush',{ id:map.id });
        break;

      case 'YWDB':
        action = NavigationActions.navigate({
          routeName: ROUTE_TO_DO,
          params: {
            // path: map.url,
          },
        });
        break;
        case 'taskProcessNotice':
        action = NavigationActions.navigate({
          routeName: ROUTE_TASKPROCESS_DETAIL_NOTIFICATION,
          params: {
            spoId:map.spoId,
            shipName:map.shipName,
            voyageCode:map.voyageCode
          },
        });
        break;

      default:
        return;
    }
    setTimeout(() => {
      if (this.props.isLogin) {
        NavigatorService.dispatch(action);
      } else {
        NavigatorService.dispatch(NavigationActions.navigate({
          routeName: ROUTE_LOGIN,
          params: {
            nextAction: action,
          },
        }));
      }
    }, 100);
  };

  // 绑定RegistrationID
  bindRegistrationID(registrationId) {
    if (!this.props.isLogin) {
      return;
    }
    if (!registrationId || registrationId === '' || AppStorage.registrationId === registrationId) {
      return;
    }
    this.props.bindRegistrationIDPromiseCreator(registrationId)
      .catch(() => {
      });
  }

  // 网络监控
  netChange = (isConnect) => {
    if (isConnect && !this.state.isConnect) {
      JPushModule.getRegistrationID(this.onGetRegistrationId);
    }
    this.setState({ isConnect });
  };


  reduceBadge = (id) => {
    console.log('将消除提醒', id);
    if (Platform.OS === 'ios') {
      JPushModule.getBadge((bage) => {
        JPushModule.setBadge(Math.max(0, bage - 1), () => {
        });
      });
    } else{
      if (id) JPushModule.clearNotificationById(id);
    }
  };

  addListener() {
    // 自定义消息，不弹出的消息
    JPushModule.addReceiveCustomMsgListener(this.onReceiveCustomMsgListener);

    // 收到通知
    JPushModule.addReceiveNotificationListener(this.onReceiveNotification);

    // 点击通知
    JPushModule.addReceiveOpenNotificationListener(this.onReceiveOpenNotification);

    // app启动完成，可以跳转画面
    this.deEmitter = DeviceEventEmitter.addListener(
      READY_TO_HANDLE_LAUNCH_NOTIFICATION,
      this.resolveLaunchAppNotification,
    );
  }

  removeListener() {
    // 收到自定义消息
    JPushModule.removeReceiveCustomMsgListener(receiveCustomMsgEvent);
    // 收到通知
    JPushModule.removeReceiveNotificationListener(receiveNotificationEvent);
    // 点击通知
    JPushModule.removeReceiveOpenNotificationListener(openNotificationEvent);

    // app启动完成，可以跳转画面
    this.deEmitter.remove();
  }

  render() {
    return null;
  }
}

NotificationCenter.propTypes = {
  bindRegistrationIDPromiseCreator: PropTypes.func.isRequired,
  sendRegistrationIdRoutine: PropTypes.func.isRequired,
  isLogin: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isLogin: makeIsLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      bindRegistrationIDPromiseCreator,
    }, dispatch),
    ...bindActionCreators({
      sendRegistrationIdRoutine,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationCenter);
