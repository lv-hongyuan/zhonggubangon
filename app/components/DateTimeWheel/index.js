import React from 'react';
import PropTypes from 'prop-types';
import Picker from 'react-native-wheel-picker';
import { Platform } from 'react-native';
import { Label, View } from 'native-base';
import { getDaysInOneMonth } from '../../common/Utils';
import {
  suitableDateTime,
  dateWidthSuitable,
  currentSuitableTime,
} from '../../utils/suitableDateTime';

const PickerItem = Picker.Item;

export const DATE_WHEEL_TYPE = {
  DATE_TIME: 'DATE_TIME',
  DATE: 'DATE',
  TIME: 'TIME',
  TIME_HAS_SEC: 'TIME_HAS_SEC',
};

const styles = {
  label: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    height: 200,
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  picker: {
    height: '100%',
    minWidth: 30,
  },
  item: {
    color: '#5f5f5f',
    fontSize: 20,
  },
};

function initNumArray(count = 60, sep = 1) {
  const numArr = [];
  let sepCount = 0;
  for (let i = 0; i < count; i += 1) {
    if (sepCount === 0) {
      numArr.push(i);
    }
    sepCount += 1;
    if (sepCount === sep) {
      sepCount = 0;
    }
  }
  return numArr;
}

/**
 * Created by jianzhexu on 2018/3/30
 */
class DateTimeWheel extends React.Component {
  constructor(props) {
    super(props);

    this.syncState = {
      selectYear: 0,
      selectMonth: 0,
      selectDay: 0,
      selectHour: 0,
      selectMinu: 0,
      selectSec: 0,
      years: [],
      month: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      hours: initNumArray(24),
      days: [],
      numArr: initNumArray(60, this.props.minuteSep),
    };

    const newState = this.stateFromProps(props);
    this.setSyncState(newState);
    this.state = { ...this.syncState };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.minValue !== nextProps.minValue
      || this.props.maxValue !== nextProps.maxValue
      || this.props.value !== nextProps.value) {
      const newState = this.stateFromProps(nextProps);
      this.setSyncState(newState);
      this.setState(newState);
    }
  }

  onDateTimeChange() {
    if (this.minTime || this.maxTime) {
      const selectDate = new Date();
      const {
        selectYear,
        selectMonth,
        selectDay,
        selectHour,
        selectMinu,
        selectSec,
      } = this.syncState;
      selectDate.setFullYear(selectYear, selectMonth - 1, selectDay);
      selectDate.setHours(selectHour, selectMinu, selectSec);
      const selectDateTime = selectDate.getTime();

      if (this.minTime && selectDateTime < this.minTime) {
        this.setSyncState({
          days: this.getDays(this.minYear, this.minMonth),
          selectYear: this.minYear,
          selectMonth: this.minMonth,
          selectDay: this.minDay,
          selectHour: this.minHour,
          selectMinu: this.minMinu,
          selectSec: this.minSec,
        });
      }
      if (this.maxTime && selectDateTime > this.maxTime) {
        this.setSyncState({
          days: this.getDays(this.maxYear, this.maxMonth),
          selectYear: this.maxYear,
          selectMonth: this.maxMonth,
          selectDay: this.maxDay,
          selectHour: this.maxHour,
          selectMinu: this.maxMinu,
          selectSec: this.maxSec,
        });
      }
    }

    this.setState(this.syncState);

    if (this.props.onDateTimeChange) {
      this.props.onDateTimeChange(
        this.syncState.selectYear,
        this.syncState.selectMonth,
        this.syncState.selectDay,
        this.syncState.selectHour,
        this.syncState.selectMinu,
        this.syncState.selectSec,
      );
    }
  }

  onYearSelect(index) {
    console.debug(`${index}onMonthSelect`);
    const days = this.getDays(index, this.state.selectMonth);
    let { selectDay } = this.state;
    if (selectDay > days[days.length - 1]) {
      selectDay = days[days.length - 1];
    }

    console.debug(`${index}onYearSelect`);
    this.setSyncState({
      selectYear: index,
      days,
      selectDay,
    });
    this.onDateTimeChange();
  }

  onMonthSelect(index) {
    console.debug(`${index}onMonthSelect`);
    this.setSyncState({
      selectMonth: index,
    });
    const days = this.getDays(this.state.selectYear, index);
    let { selectDay } = this.state;
    if (selectDay > days[days.length - 1]) {
      selectDay = days[days.length - 1];
    }
    this.setSyncState({
      selectMonth: index,
      days,
      selectDay,
    });
    this.onDateTimeChange();
  }

  onDaySelect(index) {
    this.setSyncState({ selectDay: index });
    this.onDateTimeChange();
  }

  onMinuSelect(index) {
    this.setSyncState({
      selectMinu: index,
    });
    this.onDateTimeChange();
  }

  onSecSelect(index) {
    this.setSyncState({
      selectSec: index,
    });
    this.onDateTimeChange();
  }

  onHourSelect(index) {
    this.setSyncState({
      selectHour: index,
    });
    this.onDateTimeChange();
  }

  get currentValue() {
    const date = new Date();
    const {
      selectYear, selectMonth, selectDay, selectHour, selectMinu, selectSec,
    } = this.syncState;
    switch (this.props.type) {
      case DATE_WHEEL_TYPE.DATE:
        date.setFullYear(selectYear, selectMonth - 1, selectDay);
        date.setHours(0, 0, 0, 0);
        break;
      case DATE_WHEEL_TYPE.DATE_TIME:
        date.setFullYear(selectYear, selectMonth - 1, selectDay);
        date.setHours(selectHour, selectMinu, 0, 0);
        break;
      case DATE_WHEEL_TYPE.TIME:
        date.setHours(selectHour, selectMinu, 0, 0);
        break;
      case DATE_WHEEL_TYPE.TIME_HAS_SEC:
        date.setHours(selectHour, selectMinu, selectSec, 0);
        break;

      default:
        break;
    }
    return date.getTime();
  }

  setSyncState(state) {
    Object.assign(this.syncState, state);
  }

  getDays(year, month) {
    const days = [];
    const date = getDaysInOneMonth(year, month);
    for (let i = 1; i <= date; i += 1) {
      days.push(i);
    }
    this.days = days;
    return days;
  }

  stateFromProps(props) {
    this.minTime = props.minValue && suitableDateTime(props.minValue, props.minuteSep);
    this.maxTime = props.maxValue && suitableDateTime(props.maxValue, props.minuteSep);

    let currentTime = Number.isInteger(props.value) ?
      suitableDateTime(props.value, props.minuteSep) :
      currentSuitableTime(props.minuteSep);

    if (this.minTime) {
      const minDate = dateWidthSuitable(this.minTime, this.props.minuteSep);
      this.minYear = minDate.getFullYear();
      this.minMonth = minDate.getMonth() + 1;
      this.minDay = minDate.getDate();
      this.minHour = minDate.getHours();
      this.minMinu = minDate.getMinutes();
      this.minSec = minDate.getSeconds();

      currentTime = Math.max(this.minTime, currentTime);
    }
    if (this.maxTime) {
      const maxDate = dateWidthSuitable(this.maxTime, this.maxTime);
      this.maxYear = maxDate.getFullYear();
      this.maxMonth = maxDate.getMonth() + 1;
      this.maxDay = maxDate.getDate();
      this.maxHour = maxDate.getHours();
      this.maxMinu = maxDate.getMinutes();
      this.maxSec = maxDate.getSeconds();

      currentTime = Math.min(this.maxTime, currentTime);
    }

    const currentDate = new Date(currentTime);

    return {
      years: this.initYears(currentDate.getFullYear()),
      days: this.getDays(currentDate.getFullYear(), currentDate.getMonth() + 1),
      selectYear: currentDate.getFullYear(),
      selectMonth: currentDate.getMonth() + 1,
      selectDay: currentDate.getDate(),
      selectHour: currentDate.getHours(),
      selectMinu: currentDate.getMinutes(),
      selectSec: currentDate.getSeconds(),
    };
  }

  minTime: Number;
  maxTime: Number;

  initYears(year) {
    const years = [];
    const { yearCount } = this.props;
    const minYear = this.minYear ? this.minYear : (year - 20);
    const maxYear = this.maxYear ? this.maxYear : ((year + yearCount) - 20);

    for (let i = minYear; i <= maxYear; i += 1) {
      years.push(i);
    }

    return years;
  }

  renderYear() {
    return this.state.years.length > 0 && (
      <Picker
        style={[styles.picker, { minWidth: 50 }]}
        selectedValue={this.state.selectYear}
        itemStyle={styles.item}
        onValueChange={index => this.onYearSelect(index)}
      >
        {this.state.years.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  renderMonth() {
    return this.state.month.length > 0 && (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectMonth}
        itemStyle={styles.item}
        onValueChange={index => this.onMonthSelect(index)}
      >
        {this.state.month.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  renderDay() {
    return this.state.days.length > 0 && (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectDay}
        itemStyle={styles.item}
        onValueChange={index => this.onDaySelect(index)}
      >
        {this.state.days.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  renderMinu() {
    return this.state.numArr.length > 0 && (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectMinu}
        itemStyle={styles.item}
        onValueChange={index => this.onMinuSelect(index)}
      >
        {this.state.numArr.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  renderHour() {
    return this.state.hours.length > 0 && (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectHour}
        itemStyle={styles.item}
        onValueChange={index => this.onHourSelect(index)}
      >
        {this.state.hours.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  renderSec() {
    return this.state.numArr.length > 0 && (
      <Picker
        style={styles.picker}
        selectedValue={this.state.selectSec}
        itemStyle={styles.item}
        onValueChange={index => this.onSecSelect(index)}
      >
        {this.state.numArr.map(value => (
          <PickerItem label={`${value}`} value={value} key={`${value}`} />
        ))}
      </Picker>
    );
  }

  render() {
    const timePadding = Platform.OS === 'ios' ? 12 : 0;
    const datePadding = Platform.OS === 'ios' ? 17 : 0;
    const showYear = this.props.type === DATE_WHEEL_TYPE.DATE || this.props.type === DATE_WHEEL_TYPE.DATE_TIME;
    const showMonth = this.props.type === DATE_WHEEL_TYPE.DATE || this.props.type === DATE_WHEEL_TYPE.DATE_TIME;
    const showDay = this.props.type === DATE_WHEEL_TYPE.DATE || this.props.type === DATE_WHEEL_TYPE.DATE_TIME;
    const showHour = this.props.type === DATE_WHEEL_TYPE.TIME || this.props.type === DATE_WHEEL_TYPE.TIME_HAS_SEC
      || this.props.type === DATE_WHEEL_TYPE.DATE_TIME;
    const showMinute = this.props.type === DATE_WHEEL_TYPE.TIME || this.props.type === DATE_WHEEL_TYPE.TIME_HAS_SEC
      || this.props.type === DATE_WHEEL_TYPE.DATE_TIME;
    const showSecond = this.props.type === DATE_WHEEL_TYPE.TIME_HAS_SEC;
    return (
      <View style={styles.container}>
        {showYear && this.renderYear()}
        {showYear && <Label style={[styles.item, { paddingTop: datePadding }]}>年</Label>}

        {showMonth && this.renderMonth()}
        {showMonth && <Label style={[styles.item, { paddingTop: datePadding }]}>月</Label>}

        {showDay && this.renderDay()}
        {showDay && <Label style={[styles.item, { paddingTop: datePadding }]}>日</Label>}

        {showHour && this.renderHour()}

        {showMinute && <Label style={[styles.item, { paddingTop: timePadding, paddingBottom: 3 }]}>:</Label>}
        {showMinute && this.renderMinu()}

        {showSecond && <Label style={[styles.item, { paddingTop: timePadding, paddingBottom: 3 }]}>:</Label>}
        {showSecond && this.renderSec()}
      </View>
    );
  }
}

DateTimeWheel.propTypes = {
  yearCount: PropTypes.number,
  minuteSep: PropTypes.number,
  type: PropTypes.string,
  value: PropTypes.number,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,
  onDateTimeChange: PropTypes.func,
};

DateTimeWheel.defaultProps = {
  yearCount: 100,
  minuteSep: 6,
  type: DATE_WHEEL_TYPE.DATE_TIME,
  value: undefined,
  minValue: undefined,
  maxValue: undefined,
  onDateTimeChange: undefined,
};

export default DateTimeWheel;
