import React from 'react';
import {
  View,
  TouchableHighlight,
  StyleSheet,
  Text,
  ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import RotateView from './RotateView';
import Svg from '../Svg/index';

const styles = StyleSheet.create({
  titleContainer: {
    // backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
});

class TouchableCell extends React.PureComponent {
  render() {
    return (
      <TouchableHighlight
        underlayColor="#f0f0f0"
        onPress={this.props.onPress}
        style={this.props.style}
      >
        <View style={[styles.titleContainer]}>
          <Text
            style={[{
              flex: 1,
              marginLeft: 30,
              color: this.props.titleColor,
            }, this.props.titleStyle]}
          >
            {this.props.title}
          </Text>
          <RotateView
            isOpen={this.props.isOpen}
            style={[{ marginRight: 8 }, this.props.rightStyle]}
          >
            <Svg icon="onLand_accessory" size={15} />
          </RotateView>
        </View>
      </TouchableHighlight>
    );
  }
}

TouchableCell.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  titleColor: PropTypes.string,
  isOpen: PropTypes.bool,
  style: ViewPropTypes.style,
  rightStyle: ViewPropTypes.style,
  titleStyle: Text.propTypes.style,
};
TouchableCell.defaultProps = {
  style: undefined,
  titleStyle: undefined,
  titleColor: undefined,
  rightStyle: undefined,
  isOpen: false,
  onPress: undefined,
};

export default TouchableCell;
