import React from 'react';
import { Animated, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

class RotateView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      rotation: new Animated.Value(this.props.isOpen ? 1 : 0),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      Animated.timing(this.state.rotation, {
        toValue: nextProps.isOpen ? 1 : 0,
        duration: 250,
      }).start();
    }
  }

  render() {
    return (
      <Animated.View
        style={[this.props.style, {
          transform: [
            {
              rotate: this.state.rotation.interpolate({
                inputRange: [0, 1],
                outputRange: ['0deg', '90deg'],
              }),
            },
          ],
        }]}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

RotateView.propTypes = {
  isOpen: PropTypes.bool,
  style: ViewPropTypes.style,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
RotateView.defaultProps = {
  isOpen: false,
  style: undefined,
  children: null,
};

export default RotateView;
