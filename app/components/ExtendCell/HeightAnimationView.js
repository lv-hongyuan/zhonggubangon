import React from 'react';
import { Animated, ViewPropTypes, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import myTheme from '../../Themes';

class HeightAnimationView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      height: 0,
      translateY: new Animated.Value(this.props.isOpen ? 1 : 0),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      Animated.timing(this.state.translateY, {
        toValue: nextProps.isOpen ? 1 : 0,
        duration: 250,
      }).start();
    }
  }

  render() {
    return (
      <Animated.View
        style={{
          overflow: 'hidden',
          height: this.state.translateY.interpolate({
            inputRange: [0, 1],
            outputRange: [0, this.state.height],
          }),
          opacity: this.state.translateY.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
          }),
        }}
      >
        <Animated.View
          onLayout={(event) => {
            const { height } = event.nativeEvent.layout;
            if (this.state.height !== height) {
              this.setState({
                height,
              });
            }
          }}
          style={StyleSheet.flatten([{
            // backgroundColor: 'white',
            borderTopWidth: myTheme.borderWidth,
            borderBottomWidth: myTheme.borderWidth,
            borderColor: '#E0E0E0',
            paddingLeft: 30,
            paddingRight: 30,
            zIndex: -1,
            transform: [
              {
                translateY: this.state.translateY.interpolate({
                  inputRange: [0, 1],
                  outputRange: [-this.state.height, 0],
                }),
              },
            ],
          }, this.props.style])}
        >
          {this.props.children}
        </Animated.View>
      </Animated.View>
    );
  }
}

HeightAnimationView.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  style: ViewPropTypes.style,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};
HeightAnimationView.defaultProps = {
  style: undefined,
  children: undefined,
};

export default HeightAnimationView;
