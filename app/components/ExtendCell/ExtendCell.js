import React from 'react';
import { View, Keyboard, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import TouchableCell from './TouchableCell';
import HeightAnimationView from './HeightAnimationView';

class ExtendCell extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: this.props.isOpen || false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ isOpen: nextProps.isOpen });
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: '#ffffff',
          ...this.props.style,
          overflow: 'hidden',
        }}
      >
        <TouchableCell
          title={this.props.title || ''}
          titleColor={this.props.titleColor}
          isOpen={this.state.isOpen}
          onPress={() => {
            if (this.props.touchable) {
              Keyboard.dismiss();
              this.setState({ isOpen: !this.state.isOpen });
              if (this.props.onPress) this.props.onPress();
            }
          }}
        />
        <HeightAnimationView isOpen={this.state.isOpen}>
          {this.props.children}
        </HeightAnimationView>
      </View>
    );
  }
}

ExtendCell.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  style: ViewPropTypes.style,
  title: PropTypes.string,
  titleColor: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  touchable: PropTypes.bool,
  onPress: PropTypes.func,
};
ExtendCell.defaultProps = {
  title: undefined,
  titleColor: '#DC001B',
  style: undefined,
  children: undefined,
  touchable: false,
  onPress: undefined,
};

export default ExtendCell;
