/*
 * https://blog.jeepeng.com/2017/03/16/react-native-webview-load-from-device-local-file-system/
 */
import { Platform } from 'react-native';
import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';

const qs = require('qs');

export default function htmlRequire(sourcePath, htmlName, params) {
  let source;
  // let queryStr;
  // const keys = Object.keys(params);
  // queryStr = keys.length === 0 ? '' : keys.reduce((pre, key) => {
  //     let value = '';
  //     if (pre.length > 1) {
  //         value += '&';
  //     }
  //     value += key;
  //     if (params[key]) {
  //         value += `=${params[key]}`;
  //     }
  //     return pre + value;
  // }, '?');
  const queryStr = qs.stringify(params);
  const _source = resolveAssetSource(sourcePath);
  if (__DEV__) {
    source = { uri: `${_source.uri}?${queryStr}` };
  } else {
    source = Platform.OS === 'ios'
      ? { uri: `${_source.uri}?${queryStr}` }
      : { uri: `file:///android_asset/${htmlName}?${queryStr}` };
  }

  console.log(source.uri);
  return source;
}
