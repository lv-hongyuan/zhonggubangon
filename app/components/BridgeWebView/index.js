import React from 'react';
import PropTypes from 'prop-types';
import {
   View, TouchableHighlight, DeviceEventEmitter,Platform,WebView
} from 'react-native';
import { Text } from 'native-base';
import Orientation from 'react-native-orientation';
import patchPostMessageJsCode from './patchPostMessageJsCode';
import { APP_LOGOUT } from '../../common/Constant';
import WebViewAndroid from 'react-native-webview-android'

const IS_IOS = Platform.OS === 'ios';
const serviceContainer = {};

class BridgeWebView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      webViewBackEnable: false,
      url: props.source && props.source.uri,
      isLoading: false,
    };
    Object.assign(serviceContainer, this.props.serviceContainer);
  }

  webView: WebView;

  loadingUrl: string;

  handleMessage = (event: any) => {
    console.log('handleMessage', event.nativeEvent.data);
    if (event.nativeEvent.data === '_logout_') {
      DeviceEventEmitter.emit(APP_LOGOUT);
      return;
    } if (event.nativeEvent.data === 'HScreen') {
      console.log('unlockAllOrientations');
      Orientation.unlockAllOrientations();
      return;
    }

    try {
      const data = JSON.parse(event.nativeEvent.data);
      this.handleJsonData(data);
    } catch (error) {
      console.log(error);
    }
  };

  handleJsonData(data) {
    console.log('handleMessage', data);

    if (data.backKey) {
      this.props.onChangeBackKey && this.props.onChangeBackKey(data.backKey);
      return;
    }

    if (!Array.isArray(data) || data.length !== 4) {
      return;
    }

    const callbackId = data[0];
    const service = data[1];
    const action = data[2];
    const actionArgs = data[3];

    const serviceImp = serviceContainer[service];
    if (!serviceImp) {
      this.webView.postMessage(
        JSON.stringify({
          callbackId,
          error: 'no service found',
        }),
      );
      return;
    }
    const actionImp = serviceImp[action];
    if (!actionImp) {
      this.webView.postMessage(
        JSON.stringify({
          callbackId,
          error: 'no action found in service',
        }),
      );
      return;
    }

    const result = actionImp.call(actionArgs);

    this.webView.postMessage(
      JSON.stringify({
        callbackId,
        result,
      }),
    );
  }

  onLoadStart (e){
    // 忽略postMessage的url请求
    if (e.indexOf('://postMessage?') !== -1) return;

    console.log('load start..', e);
    this.props.onLoadStart && this.props.onLoadStart(e);
  };

  onLoadEnd = (e) => {
    this.webView.postMessage(
      JSON.stringify({ nativeEvent: 'deviceready' }),
    );

    console.log('load end...', e.nativeEvent.url);
    this.props.onLoadEnd && this.props.onLoadEnd(e);
  };

  handleError = (error) => {
    console.log(error);
    this.webView.stopLoading();
    this.props.onLoadEnd && this.props.onLoadEnd(error);
  };

  navigateTo(url) {
    const js = `
        setTimeout(function () {
            window.stop();
            if (window.location.href === '${url}') {
                window.location.reload(true);
            } else {
                window.location.assign('${url}');
            }
        }, 100);
        `;
    this.webView.injectJavaScript(js);
  }

  onShouldStartLoadWithRequest = (e) => {
    // Implement any custom loading logic here, don't forget to return!
    // 解决WebKitErrorDomain code:101的警告
    // http://leonhwa.com/blog/0014905236320002ebb3db97fe64fb3bb6f047eafb1c5de000
    const scheme = e.url.split('://')[0];
    this.onLoadStart(e.url)
    return scheme === 'http' || scheme === 'https' || scheme === 'file';
  };

  onNavigationStateChange = (navState) => {
    // 忽略postMessage的url请求
    if (navState.url.indexOf('://postMessage?') !== -1) return;

    this.props.onUrlChange && this.props.onUrlChange(navState.url);
    if (this.loadingUrl !== navState.url) {
      this.loadingUrl = navState.url;
    } else {
      this.loadingUrl = undefined;
    }
    const isLoading = !!this.loadingUrl;
    if (isLoading) {
      console.log('lockToPortrait');
      Orientation.lockToPortrait();
    } else {
      this.props.onLoadEnd && this.props.onLoadEnd(navState);
    }
    this.setState({
      webViewBackEnable: navState.canGoBack,
      url: navState.url,
      title: navState.title,
      isLoading,
    });
    this.props.onChangeLoading && this.props.onChangeLoading(isLoading);
    this.props.onChangeCanGoBack && this.props.onChangeCanGoBack(navState.canGoBack);
    this.props.onTitleChange && this.props.onTitleChange(navState.title);
  };

  renderError = (e) => {
    this.props.onLoadEnd && this.props.onLoadEnd(e);
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>{e.message || '出错了'}</Text>

        <TouchableHighlight onPress={() => {
          this.webView.reload();
        }}
        >
          <View>
            <Text>重试</Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  };

  render() {
    if(IS_IOS){
      return(
        <WebView
        ref={webView => (this.webView = webView)}
        source={this.props.source}
        /**
         * https://facebook.github.io/react-native/docs/webview
         * Loads static HTML or a URI (with optional headers) in the WebView.
         * Note that static HTML will require setting originWhitelist to ["*"]
         */
        originWhitelist={['*']}
        /*
        * https://www.phodal.com/blog/react-native-webview-post-message-failure/
        * https://github.com/facebook/react-native/issues/10865#issuecomment-269847703
        */
        injectedJavaScript={patchPostMessageJsCode}
        onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
        onNavigationStateChange={this.onNavigationStateChange}
        onMessage={this.handleMessage}
        // onLoadStart={this.onLoadStart}
        onLoadEnd={this.onLoadEnd}
        onError={this.handleError}
        bounces={false}
        startInLoadingState={false}
        renderError={this.renderError}
      />
      )
    }else{
      return (
        <WebViewAndroid
        style={{flex:1}}
        ref="webViewAndroidSample"
          url={this.props.source.uri}
          // originWhitelist={['*']}
         javaScriptEnabled={true}
         geolocationEnabled={false}
         builtInZoomControls={false}
         injectedJavaScript={patchPostMessageJsCode}
         onShouldStartLoadWithRequest={this.onShouldStartLoadWithRequest}
         onNavigationStateChange={this.onNavigationStateChange}
         onMessage={this.onMessage}
         
        />
        
      );
    }
    
  }

  goBack() {
    // you can use this callback to control web view
    this.refs.webViewAndroidSample.goBack();
  }
  goForward() {
    this.refs.webViewAndroidSample.goForward();
  }
  reload() {
    this.refs.webViewAndroidSample.reload();
  }
  stopLoading() {
    // stops the current load
    this.refs.webViewAndroidSample.stopLoading();
  }
  postMessage(data) {
    // posts a message to web view
    this.refs.webViewAndroidSample.postMessage(data);
  }
  evaluateJavascript(data) {
    // evaluates javascript directly on the webview instance
    this.refs.webViewAndroidSample.evaluateJavascript(data);
  }
  injectJavaScript(script) {
    // executes JavaScript immediately in web view
    this.refs.webViewAndroidSample.injectJavaScript(script);
  }
  onShouldStartLoadWithRequest(event) {
    // currently only url & navigationState are returned in the event.
    console.log(event.url);
    console.log(event.navigationState);

    if (event.url === 'https://www.mywebsiteexample.com/') {
      return true;
    } else {
      return false;
    }
  }
  onNavigationStateChange(event) {
    console.log(event);

    this.setState({
      backButtonEnabled: event.canGoBack,
      forwardButtonEnabled: event.canGoForward,
      url: event.url,
      status: event.title,
      loading: event.loading
    });
  }
  onMessage(event) {
    this.setState({
      messageFromWebView: event.message
    });
  }
  javascriptToInject () {
    return `
      $(document).ready(function() {
        $('a').click(function(event) {
          if ($(this).attr('href')) {
            var href = $(this).attr('href');
            window.webView.postMessage('Link tapped: ' + href);
          }
        })
      })
    `
  }
}

BridgeWebView.propTypes = {
  serviceContainer: PropTypes.object,
  onUrlChange: PropTypes.func,
  source: WebView.propTypes.source,
  // onLoadStart: WebView.propTypes.onLoadStart,
  // onLoadEnd: WebView.propTypes.onLoadEnd,
  // onError: WebView.propTypes.onError,
  onChangeLoading: PropTypes.func,
  onTitleChange: PropTypes.func,
  onChangeCanGoBack: PropTypes.func,
  onChangeBackKey: PropTypes.func,
};

export default BridgeWebView;
