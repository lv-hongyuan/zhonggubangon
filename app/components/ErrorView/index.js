import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text, Button } from 'native-base';
import PropTypes from 'prop-types';

/**
 * Created by ocean on 2018/4/21
 */
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginBottom: 10,
  },
  message: {
    marginBottom: 10,
  },
  btnTitle: {},
});

class ErrorView extends React.Component {
  render() {
    return (
      <View style={[styles.container, { backgroundColor: '#E0E0E0' }]}>
        <View>
          <Text style={styles.title}>{this.props.title}</Text>
          {this.props.message ? (
            <Text style={styles.message}>{this.props.message}</Text>
          ) : null}
          <View>
            <Button onPress={this.props.onReload}>
              <Text style={styles.btnTitle}>{this.props.buttonTitle}</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

ErrorView.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  onReload: PropTypes.func,
  buttonTitle: PropTypes.string,
};

ErrorView.defaultProps = {
  title: '出错了！',
  buttonTitle: '重试',
  message: undefined,
  onReload: undefined,
};

export default ErrorView;
