import React from "react";
import PropTypes from 'prop-types';
import { TouchableOpacity, Keyboard, TextInput, SafeAreaView, FlatList } from 'react-native';
import { InputGroup, Label, Text, View } from 'native-base';
import commonStyles from "../../common/commonStyles";
import myTheme from "../../Themes";
import InputItem from "../InputItem";

const style = {
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
  },
};

class SelectBox extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      showList: false,
      value: '',
    };
  }

  componentDidMount() {

  }


  //列表的每一行
  renderItemView({ item, index }) {
    console.log('index' + index + '' + item);
    return (
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={() => {
//          TODO 更改
          console.log('选择', item);
          this.setState({
            showList: false,
            value: item,
          });
          this.props.onChangeText(item);

        }}
      >
        <View style={{ alignItems: 'center' }}>
          <Text>{item}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  // 去除警告
  extraUniqueKey = (item, index) => {
    return index + item;
  };

  clickInput = () => {

    this.setState({ showList: true });
  };

  render() {
    console.log(this.props.labelTitle);
    console.log('dianji ', this.state.showList, this.props.items);
    return (
      <SafeAreaView style={{ width: '100%' }}>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            autoFocus
            label={this.props.labelTitle}
            returnKeyType="search"
            value={this.state.value}
            clearButtonMode="while-editing"
            onChangeText={(text) => {
              this.setState({ value: text });
            }}
            onSubmitEditing={() => {
              Keyboard.dismiss();
            }}
            onFocus={() => {
              this.clickInput();
            }}
          />
          <FlatList
            style={{ flex: 1 }}
            data={this.props.items}
            renderItem={(item, index) => {
              this.renderItemView(item, index);
            }}
            keyExtractor={(item, index) =>
              this.extraUniqueKey(item, index)
            }
          />
        </InputGroup>
      </SafeAreaView>)
  }

}

SelectBox.propTypes = {
  labelTitle: PropTypes.string,
  items: PropTypes.array,
  value: PropTypes.string,
  readOnly: PropTypes.bool,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
};
SelectBox.defaultProps = {
  labelTitle: '',
  items: [],
  value: undefined,
  readOnly: false,
  placeholder: undefined,
  onChangeText: undefined,

};

export default SelectBox;
