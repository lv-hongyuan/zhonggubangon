import React from 'react';
import PropTypes from 'prop-types';
import Overlay from 'teaset/components/Overlay/Overlay';
import { Keyboard } from 'react-native';
import SelectButton from '../SelectButton/index';
import { formatDate, longToDate } from '../../utils/DateFormat';
import DatePullPicker from './DatePullPicker';
import { DATE_WHEEL_TYPE } from '../DateTimeWheel';

/**
 * Created by zjl on 2018/3/23
 */

class DatePullSelector extends React.PureComponent {
  constructor(props) {
    super(props);

    this.time = this.props.value;
    this.submitFlag = false;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.time = nextProps.value;
    }
  }

  onCancelSelect = () => {
    this.submitFlag = true;
    this.time = "";
    this.pullView.close();
  };

  onCancel = () => {
    this.submitFlag = false;
    this.pullView.close();
  };

  onSubmit = (time) => {
    this.submitFlag = true;
    this.time = time;
    this.pullView.close();
  };

  onDisappearCompleted = () => {
    if (this.submitFlag && this.props.onChangeValue) {
      if (this.props.onChangeValue) this.props.onChangeValue(this.time);
    }
  };

  showSelector = () => {
    Keyboard.dismiss();
    const overlayView = (
      <Overlay.PullView
        side="bottom"
        animated={false}
        modal={false}
        ref={(ref) => {
          this.pullView = ref;
        }}
        onDisappearCompleted={this.onDisappearCompleted}
      >
        <DatePullPicker
          type={this.props.type}
          value={this.props.value}
          defaultValue={this.props.defaultValue}
          minValue={this.props.minValue}
          maxValue={this.props.maxValue}
          title={this.props.title}
          onCancelSelect={this.onCancelSelect}
          onCancel={this.onCancel}
          onSubmit={this.onSubmit}
        />
      </Overlay.PullView>
    );
    Overlay.show(overlayView);
  };

  formatTime(time) {
    let formatStr = 'yyyy-MM-dd hh:mm';
    switch (this.props.type) {
      case DATE_WHEEL_TYPE.DATE:
        formatStr = 'yyyy-MM-dd';
        break;
      case DATE_WHEEL_TYPE.DATE_TIME:
        formatStr = 'yyyy-MM-dd hh:mm';
        break;
      case DATE_WHEEL_TYPE.TIME:
        formatStr = 'hh:mm';
        break;
      case DATE_WHEEL_TYPE.TIME_HAS_SEC:
        formatStr = 'hh:mm:ss';
        break;

      default:
        break;
    }
    return formatDate(longToDate(time), formatStr);
  }

  render() {
    return (
      <SelectButton
        onPress={this.showSelector}
        value={this.formatTime(this.props.value)}
        placeholder={this.props.readOnly ? ' ' : this.props.placeholder}
        readOnly={this.props.readOnly}
      />
    );
  }
}

DatePullSelector.propTypes = {
  title: PropTypes.string,
  value: PropTypes.number,
  defaultValue: PropTypes.number,
  type: PropTypes.string,
  minValue: PropTypes.number,
  maxValue: PropTypes.number,
  onChangeValue: PropTypes.func,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
};

DatePullSelector.defaultProps = {
  title: '请选择时间',
  value: undefined,
  defaultValue: undefined,
  onChangeValue: undefined,
  placeholder: '请选择时间',
  type: DATE_WHEEL_TYPE.DATE_TIME,
  readOnly: false,
  minValue: undefined,
  maxValue: undefined,
};

export default DatePullSelector;
