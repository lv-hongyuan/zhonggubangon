import React from 'react';
import {
  Dimensions, Keyboard, PixelRatio, Platform, View, Button,
} from 'react-native';
import InputAccessoryView from 'react-native/Libraries/Components/TextInput/InputAccessoryView';

/**
 * Created by ocean on 2018/5/29
 */
import Orientation from 'react-native-orientation';

export const inputAccessoryViewID = 'inputAccessoryView1';

class InputAccessory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      width: Dimensions.get('window').width,
    };
  }

  componentDidMount() {
    Orientation.addOrientationListener(this.orientationDidChange);
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this.orientationDidChange);
  }

  orientationDidChange = () => {
    this.setState({
      width: Dimensions.get('window').width,
    });
  };

  render() {
    return Platform.OS === 'ios' && (
      <InputAccessoryView nativeID={inputAccessoryViewID}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: 'white',
          height: 50,
          width: this.state.width,
          borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
          borderTopColor: '#E0E0E0',
        }}
        >
          <View />
          <Button
            onPress={() => {
              Keyboard.dismiss();
            }}
            title="完成"
          />
        </View>
      </InputAccessoryView>
    );
  }
}

export default InputAccessory;
