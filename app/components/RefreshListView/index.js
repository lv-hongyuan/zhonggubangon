//
//  Created by Liu Jinyong on 17/4/5.
//  Copyright © 2016年 Liu Jinyong. All rights reserved.
//
//  @flow
//  Github:
//  https://github.com/huanxsd/react-native-refresh-list-view

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  SwipeableFlatList,
  TouchableOpacity,
  ScrollView as nativeScrollView,
  Platform, Animated,
} from 'react-native';
import { ScrollView as mjrefreshScrollView } from 'react-native-mjrefresh';
import SkypeIndicator from 'react-native-indicators/src/components/skype-indicator/index';
import CustomRefreshControl from './CustomRefreshControl';

const ScrollView = Platform.OS === 'ios' ? mjrefreshScrollView : nativeScrollView;

export const RefreshState = {
  Idle: 0,
  HeaderRefreshing: 1,
  FooterRefreshing: 2,
  NoMoreData: 3,
  Failure: 4,
  EmptyData: 5,
};

const DEBUG = false;
const log = (text: string) => {
  if (DEBUG) console.log(text);
};

const styles = StyleSheet.create({
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 44,
  },
  footerText: {
    fontSize: 14,
    color: '#555555',
  },
});

class RefreshListView extends PureComponent {
  componentWillReceiveProps(nextProps) {
    log(`[RefreshListView]  RefreshListView componentWillReceiveProps ${nextProps.refreshState}`);
    if (this.props.refreshState !== nextProps.refreshState) {
      if (nextProps.refreshState !== RefreshState.HeaderRefreshing) {
        this.refreshControl.finishRefresh();
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    log(`[RefreshListView]  RefreshListView componentDidUpdate ${prevProps.refreshState}${prevState}`);
  }

  onHeaderRefresh = () => {
    log('[RefreshListView]  onHeaderRefresh');

    if (this.shouldStartHeaderRefreshing()) {
      log('[RefreshListView]  onHeaderRefresh');
      if (this.props.onHeaderRefresh) this.props.onHeaderRefresh(RefreshState.HeaderRefreshing);
    }
  };

  onEndReached = (info: { distanceFromEnd: number }) => {
    log(`[RefreshListView]  onEndReached   ${info.distanceFromEnd}`);

    if (this.shouldStartFooterRefreshing()) {
      log('[RefreshListView]  onFooterRefresh');
      if (this.props.onFooterRefresh) this.props.onFooterRefresh(RefreshState.FooterRefreshing);
    }
  };

  finishRefresh = () => {
    this.refreshControl.finishRefresh();
  };

  beginRefresh = () => {
    this.refreshControl.beginRefresh();
  };

  shouldStartHeaderRefreshing = () => {
    log('[RefreshListView]  shouldStartHeaderRefreshing');

    return !(this.props.refreshState === RefreshState.HeaderRefreshing ||
      this.props.refreshState === RefreshState.FooterRefreshing);
  };

  shouldStartFooterRefreshing = () => {
    log('[RefreshListView]  shouldStartFooterRefreshing');

    const { refreshState, data } = this.props;
    if (data.length === 0) {
      return false;
    }

    return (refreshState === RefreshState.Idle);
  };



  renderFooter = () => {
    let footer = null;

    const {
      footerRefreshingText,
      footerFailureText,
      footerNoMoreDataText,
      footerEmptyDataText,

      footerRefreshingComponent,
      footerFailureComponent,
      footerNoMoreDataComponent,
      footerEmptyDataComponent,
    } = this.props;

    switch (this.props.refreshState) {
      case RefreshState.Idle:
        footer = (<View style={styles.footerContainer} />);
        break;
      case RefreshState.Failure: {
        footer = footerFailureComponent || (
          <TouchableOpacity onPress={() => {
            if (this.props.data.length === 0) {
              this.refreshControl.beginRefresh();
            } else if (this.props.onFooterRefresh) {
              this.props.onFooterRefresh(RefreshState.FooterRefreshing);
            }
          }}
          >
            <View style={styles.footerContainer}>
              <Text style={styles.footerText}>{footerFailureText}</Text>
            </View>

          </TouchableOpacity>
        );
        break;
      }
      case RefreshState.EmptyData: {
        footer = footerEmptyDataComponent || (
          <TouchableOpacity onPress={() => {
            this.refreshControl.beginRefresh();
          }}
          >
            <View style={styles.footerContainer}>
              <Text style={styles.footerText}>{footerEmptyDataText}</Text>
            </View>
          </TouchableOpacity>
        );
        break;
      }
      case RefreshState.FooterRefreshing: {
        footer = footerRefreshingComponent || (
          <View style={styles.footerContainer}>
            <SkypeIndicator style={{ flex: 0 }} size={24} color="#ED1727" />
            <Text style={[styles.footerText, { marginLeft: 7 }]}>{footerRefreshingText}</Text>
          </View>
        );
        break;
      }
      case RefreshState.NoMoreData: {
        footer = footerNoMoreDataComponent || (
          <View style={styles.footerContainer}>
            <Text style={styles.footerText}>{footerNoMoreDataText}</Text>
          </View>
        );
        break;
      }
      default:
        break;
    }

    return (
      <Animated.View style={{
        transform: [{ translateX: this.props.headerTranslateX || 0 }],
        width: this.props.containerWidth || '100%',
      }}
      >
        {footer}
      </Animated.View>
    );
  };

  render() {
    log(`[RefreshListView]  render  refreshState:${this.props.refreshState}`);

    const { renderItem, ...rest } = this.props;

    return (
      <View style={{ flex: 1, overflow: 'hidden' }}>
        <SwipeableFlatList
          ref={this.props.listRef}
          onEndReached={this.onEndReached}
          ListFooterComponent={this.renderFooter}
          onEndReachedThreshold={0.1}
          renderItem={renderItem}
          {...rest}
          renderScrollComponent={props => (
            <ScrollView
              {...props}
              style={{ flex: 1 }}
              refreshControl={
                <CustomRefreshControl
                  ref={(ref) => { this.refreshControl = ref; }}
                  headerTranslateX={this.props.headerTranslateX}
                  containerWidth={this.props.containerWidth}
                  onRefresh={this.onHeaderRefresh}
                />
              }
            />
          )}
          onScrollBeginDrag={(event) => {
            log('onScrollBeginDrag');
            this.canAction = true;
            if (this.props.onScrollBeginDrag) this.props.onScrollBeginDrag(event);
          }}
          onScrollEndDrag={(event) => {
            log('onScrollEndDrag');
            this.canAction = false;
            if (this.props.onScrollEndDrag) this.props.onScrollEndDrag(event);
          }}
          onMomentumScrollBegin={(event) => {
            log('onMomentumScrollBegin');
            this.canAction = true;
            if (this.props.onMomentumScrollBegin) this.props.onMomentumScrollBegin(event);
          }}
          onMomentumScrollEnd={(event) => {
            log('onMomentumScrollEnd');
            this.canAction = false;
            if (this.props.onMomentumScrollEnd) this.props.onMomentumScrollEnd(event);
          }}
        />
      </View>
    );
  }
}

RefreshListView.defaultProps = {
  footerRefreshingText: '数据加载中…',
  footerFailureText: '点击重新加载',
  footerNoMoreDataText: '已加载全部数据',
  footerEmptyDataText: '暂时没有相关数据',
  renderQuickActions: undefined,
  maxSwipeDistance: 80,
};

RefreshListView.props = {
  ...SwipeableFlatList.props,
  refreshState: PropTypes.objectOf(Object.keys(RefreshState)),
  onHeaderRefresh: PropTypes.func,
  onFooterRefresh: PropTypes.func,
  data: PropTypes.array,

  listRef: PropTypes.func,

  footerRefreshingText: PropTypes.string,
  footerFailureText: PropTypes.string,
  footerNoMoreDataText: PropTypes.string,
  footerEmptyDataText: PropTypes.string,

  footerRefreshingComponent: PropTypes.node,
  footerFailureComponent: PropTypes.node,
  footerNoMoreDataComponent: PropTypes.node,
  footerEmptyDataComponent: PropTypes.node,

  renderItem: PropTypes.func,

  renderQuickActions: PropTypes.func,
  maxSwipeDistance: PropTypes.number,

  headerTranslateX: PropTypes.object,
  containerWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default RefreshListView;
