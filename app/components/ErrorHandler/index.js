import React from 'react';
import PropTypes from 'prop-types';
import {
  Button, StyleSheet, Text, View,
} from 'react-native';
import { connect } from 'react-redux';
import { cleanError } from './actions';
import AlertView from '../Alert';
import Loading from '../Loading';

const styles = StyleSheet.create({
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  loadingContainer: {
    backgroundColor: 'white',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    padding: 20,
  },
  errorContainer: {
    backgroundColor: 'white',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    width: 200,
  },
  buttonContainer: {
    borderTopColor: '#E0E0E0',
    borderTopWidth: 1,
    width: '100%',
  },
});

class ErrorHandler extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      const { message } = nextProps.error;
      setTimeout(() => {
        AlertView.show({ message });
      }, 0);
    }
    this.props.dispatch(cleanError());
  }

  renderChildren() {
    return React.Children.map(this.props.children, child => React.cloneElement(child, { ...this.props }));
  }

  static renderLoadingView() {
    return (
      <Loading />
    );
  }

  renderError() {
    return (
      <View style={styles.background}>
        <View style={styles.errorContainer}>
          <View>
            <Text>Error</Text>
          </View>
          <View>
            <Text>{this.props.error.message}</Text>
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="确定"
              onPress={() => {
                this.props.dispatch(cleanError());
              }}
            />
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View {...this.props}>
        {this.renderChildren()}
        {this.props.isLoading ? ErrorHandler.renderLoadingView() : null}
        {/* {this.props.error ? this.renderError() : null} */}
      </View>
    );
  }
}

ErrorHandler.propTypes = {
  // navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
};
ErrorHandler.defaultProps = {
  error: undefined,
};

const mapStateToProps = state => ({
  isLoading: state.error.isLoading,
  error: state.error.error,
});

export default connect(mapStateToProps)(ErrorHandler);
