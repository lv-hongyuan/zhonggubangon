import { ERROR_MESSAGE, CLEAN_ERROR } from './constants';

export default function (state = { success: false }, action) {
  switch (action.type) {
    case ERROR_MESSAGE:
      const error = {
        type: action.type,
        code: action.payload.code,
        message: action.payload.message,
      };
      return { ...state, error };
    case CLEAN_ERROR:
      return { ...state, error: null };
    default:
      if (!action.globalLoading) {
        return state;
      }
      console.log(action);
      if (action.type.indexOf('_RESULT') === -1) {
        return { ...state, isLoading: true };
      }
      return { ...state, isLoading: false };
  }
}
