export const ERROR_MESSAGE = 'App/ERROR_MESSAGE';
export const SUCCESS_MESSAGE = 'App/SUCCESS_MESSAGE';
export const WARN_MESSAGE = 'App/WARN_MESSAGE';
export const RESET_MESSAGE = 'App/RESET_MESSAGE';
export const CLEAN_ERROR = 'App/CLEAN_ERROR';
