import {
  ERROR_MESSAGE, WARN_MESSAGE, SUCCESS_MESSAGE, RESET_MESSAGE, CLEAN_ERROR,
} from './constants';

export function errorMessage(error) {
  return {
    type: ERROR_MESSAGE,
    payload: error,
  };
}

export function resetMessage() {
  return {
    type: RESET_MESSAGE,
  };
}

export function warnMessage(msg) {
  return {
    type: WARN_MESSAGE,
    payload: msg,
  };
}

export function successMessage(msg) {
  return {
    type: SUCCESS_MESSAGE,
    payload: msg,
  };
}

export function cleanError() {
  return {
    type: CLEAN_ERROR,
  };
}
