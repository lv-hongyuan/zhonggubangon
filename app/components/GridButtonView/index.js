import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
  Button, Text, View, Badge,
} from 'native-base';
import Svg from '../Svg/index';

const styles = StyleSheet.create({
  gridContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'flex-start',
    // borderTopWidth: 1,
    // borderLeftWidth: 1,
    borderColor: 'gray',
  },
  gridButton: {
    borderColor: 'gray',
    // borderBottomWidth: 1,
    // borderRightWidth: 1,
    borderRadius: 0,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 0,
    paddingBottom: 0,
    aspectRatio: 1,
  },
  badge: {
    position: 'absolute',
    top: 1,
    right: 1,
  },
});

class GridButtonView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      itemWidth: 0,
    };
  }

  static renderBadge(badge) {
    if (badge && badge > 0) {
      return (
        <Badge style={styles.badge}>
          <Text>{badge}</Text>
        </Badge>
      );
    }
  }

  renderButton(data) {
    const {
      icon, title, color, badge, action,
    } = data;
    return (
      <Button
        transparent
        key={`${title}`}
        onPress={action || (() => {
        })}
        style={[styles.gridButton, { width: this.state.itemWidth }]}
      >
        <View style={{ padding: 8 }}>
          <Svg icon={icon} size={60} color={color || '#DC001B'} />
          {GridButtonView.renderBadge(badge)}
        </View>
        <Text style={{ color: color || '#535353', paddingLeft: 0, paddingRight: 0 }}>
          {' '}
          {title}
          {' '}
        </Text>
      </Button>
    );
  }

  render() {
    const data = this.props.data || [];

    return (
      <View onLayout={(event) => {
        const { width, height } = Dimensions.get('window');
        const viewWidth = event.nativeEvent.layout.width;
        const itemWidth = width > height ? viewWidth / 6 : viewWidth / 3;
        if (itemWidth !== this.state.itemWidth) {
          this.setState({ itemWidth });
        }
      }}
      >
        <View style={styles.gridContainer}>
          {this.state.itemWidth > 0 ? data.map((value, index) => this.renderButton(value, index)) : null}
        </View>
      </View>
    );
  }
}

GridButtonView.propTypes = {
  data: PropTypes.array,
};

export default GridButtonView;
