import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import myTheme from '../../Themes';
import GridButtonView from '../../components/GridButtonView/index';
import { makePermissions } from '../AppInit/selectors';
import { ROUTE_BUSINESS_APPROVAL, ROUTE_OA, ROUTE_ON_LAND,ROUTE_SEA_MAP } from '../../RouteConstant';
import { makePortWorkBadge } from '../../apps/onLand/containers/PortWork/selectors';
import { AppPermission } from '../../common/Constant';
import screenHOC from '../../components/screenHOC';

@screenHOC
class Platforms extends React.PureComponent {
  onBackPressed = () => false;

  makeMenus() {
    return this.props.permissionList.map((permission) => {
      switch (permission) {
        case AppPermission.OA:
          return {
            icon: 'oa',
            title: 'OA',
            badge: 0,
            action: () => {
              this.props.navigation.navigate(ROUTE_OA);
            },
          };
        case AppPermission.Port:
          return {
            icon: 'onLand',
            title: '航线管理',
            badge: this.props.portWorkBadge,
            action: () => {
              this.props.navigation.navigate(ROUTE_ON_LAND);
            },
          };
        case AppPermission.ToDo:
          return {
            icon: 'todo',
            title: '待办业务',
            badge: this.props.portWorkBadge,
            action: () => {
              this.props.navigation.navigate(ROUTE_BUSINESS_APPROVAL);
              // this.props.navigation.navigate(ROUTE_SEA_MAP);
            },
          };
        default:
          return null;
      }
    }).filter(item => item !== null);
  }

  render() {
    const menus = this.makeMenus();
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content
          theme={myTheme}
          style={{
            flex: 1,
            minHeight: '100%',
          }}
        >
          <SafeAreaView style={{
            flex: 1,
            minHeight: '100%',
          }}
          >
            <View style={{
              backgroundColor: 'white',
              padding: 20,
            }}
            >
              <GridButtonView data={menus} />
            </View>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

Platforms.propTypes = {
  navigation: PropTypes.object.isRequired,
  permissionList: PropTypes.array,
  portWorkBadge: PropTypes.number,
};
Platforms.defaultProps = {
  permissionList: [],
  portWorkBadge: undefined,
};

Platforms.navigationOptions = {
  headerTitle: '现有功能',
  headerLeft: <HeaderButtons />,
};

const mapStateToProps = createStructuredSelector({
  permissionList: makePermissions(),
  portWorkBadge: makePortWorkBadge(),
});

export default connect(mapStateToProps)(Platforms);
