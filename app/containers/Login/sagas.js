import {
  call, put, takeLatest, select, fork,
} from 'redux-saga/effects';
import { Platform } from 'react-native';
import qs from 'qs';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { sendVerCodeRoutine, loginRoutine } from './actions';
import { errorMessage } from '../../components/ErrorHandler/actions';
import getDeviceInfo from '../../utils/getDeviceInfo';
import { saveLoginInput, saveUserInfo } from '../AppInit/sagas';
import uploadAppInfo from '../../common/uploadAppInfo';
import AppStorage from '../../utils/AppStorage';

function* sendVerificationCode(action) {
  console.log(action);
  try {
    yield put(sendVerCodeRoutine.request());
    const { userId, password, phoneNumber } = action.payload;
    const response = yield call(request, ApiFactory.sendVerificationCode({
      sysUserCode: userId,
      password,
      phoneNum: phoneNumber,
    }));
    console.log(response);
    yield put(sendVerCodeRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(sendVerCodeRoutine.failure(e));
  } finally {
    yield put(sendVerCodeRoutine.fulfill());
  }
}

export function* sendVerificationCodeSaga() {
  yield takeLatest(sendVerCodeRoutine.TRIGGER, sendVerificationCode);
}

const registrationId = state => state.notificationCenter.registrationId;

// 更新用户信息
export async function checkUserInfo() {
  try {
    const sysUser = AppStorage.sysUser;
    if (!sysUser) return;
    const response = await request(ApiFactory.userInfo(sysUser));
    await call(saveUserInfo, { payload: { ...AppStorage.userInfo, ...response } });
  } catch (e) {
    console.log(e);
  }
}

function* login(action) {
  console.log(action);
  try {
    yield put(loginRoutine.request());
    const {
      userId, password, phoneNumber, verCode,
    } = action.payload;
    yield fork(saveLoginInput, {
      username: userId,
      phone: phoneNumber,
    });
    const deviceKey = yield select(registrationId);
    const response = yield call(
      request,
      ApiFactory.login({
        sysUserCode: userId,
        password,
        phoneNum: phoneNumber,
        smsCode: verCode,
        deviceType: Platform.OS,
        deviceDetail: 'deviceDetail', // qs.stringify(getDeviceInfo()),
        deviceKey,
      }),
    );
    console.log('login', response.toString());
    yield call(saveUserInfo, { payload: response });
    // 上传app版本信息
    yield call(uploadAppInfo, response);
    yield put(loginRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield call(saveUserInfo, { payload: undefined });
    yield put(loginRoutine.failure(e));
  } finally {
    yield put(loginRoutine.fulfill());
  }
}

export function* loginSaga() {
  yield takeLatest(loginRoutine.TRIGGER, login);
}

// All sagas to be loaded
export default [sendVerificationCodeSaga, loginSaga];
