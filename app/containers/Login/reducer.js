import { sendVerCodeRoutine, loginRoutine } from './actions';

const initState = {
  loading: false,
  user: undefined,
  error: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    // 发送验证码
    case sendVerCodeRoutine.TRIGGER:
      return { ...state, loading: true };
    case sendVerCodeRoutine.SUCCESS:
      return state;
    case sendVerCodeRoutine.FAILURE:
      return { ...state, error: action.payload };
    case sendVerCodeRoutine.FULFILL:
      return { ...state, loading: false };

      // 登录
    case loginRoutine.TRIGGER:
      return { ...state, loading: true };
    case loginRoutine.SUCCESS:
      return { ...state, user: action.payload };
    case loginRoutine.FAILURE:
      return { ...state, error: action.payload };
    case loginRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
