import React from 'react';
import PropTypes from 'prop-types';
import { Platform,InteractionManager, Image, Dimensions, PixelRatio,} from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Container,
  Content,
  Form,
  View,
  Item,
  Text,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getSavedLoginInput } from '../AppInit/sagas';
import { sendVerCodePromiseCreator, loginPromiseCreator } from './actions';
import { ROUTE_ROOT } from '../../RouteConstant';
import { makeLoading } from './selectors';
import myTheme from '../../Themes';
import screenHOC from '../../components/screenHOC';
// import { getBadgeInfoRoutine } from '../AppInit/actions';
import NavigatorService from '../../NavigatorService';
import { TextInput } from '../../components/InputItem/TextInput';
import commonStyles from '../../common/commonStyles';
import { ApiConstants as AuthenticationApiConstants, EnableServerChange } from '../../common/Constant';
import { ApiConstants as PortApiConstants } from '../../apps/onLand/common/Constant';
import { ApiConstants as TodoApiConstants } from '../../apps/BusinessApproval/common/Constant';
const platform = Platform.OS;
const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'column',
    justifyContent: 'center',
    marginTop: 'auto',
    height: '100%',
    marginBottom: 'auto',
  },
  logo: {
    width: Dimensions.get('window').width - 100,
    height: (Dimensions.get('window').width - 100) * 0.37,
    marginBottom: 30,
  },
  item: {
    marginBottom: 20,
    justifyContent: 'center',
    backgroundColor: '#fff',
    height: 40,
    borderColor: '#DF001B',
  },
  input: {
    paddingLeft: 20,
    paddingRight: 20,
    fontSize: 16,
    height: 50,
    color: '#565656',
    paddingVertical: 0,
  },
  verButton: {
    height: 38,
    backgroundColor: '#EE7E0A',
  },
  loginBtn: {
    marginLeft: 50,
    marginRight: 50,
    height: 40,
    maxHeight: 40,
    backgroundColor: '#DF001B',
  },
};

@screenHOC
class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userId: '',
      password: '',
      phoneNumber: '',
      verCode: '',
      verCodeText: '获取验证码',
      isWaitCode: false,
      timerNum: 60,
      authenticationHost: AuthenticationApiConstants.host,
      portHost: PortApiConstants.host,
      toDoHost: TodoApiConstants.host,
    };

    this.nextAction = props.navigation.getParam('nextAction');
    console.log('nextAction', this.nextAction);
  }

  componentDidMount() {
    getSavedLoginInput()
      .then(({ username, phone }) => {
        this.setState({
          userId: username,
          phoneNumber: phone,
        });
      })
      .catch(() => {});
  }

  componentWillReceiveProps(nextProps) {
    const nextAction = nextProps.navigation.getParam('nextAction');
    if (nextAction) {
      this.nextAction = nextAction;
      this.props.navigation.setParams({ nextAction: undefined });
      console.log('nextAction', this.nextAction);
    }
  }

  componentWillUnmount() {
    if (this.timer) clearInterval(this.timer);
    this.timer = undefined;
  }

  onBackPressed = () => false;

  onBtnLoginButton = () => {
    this.props.loginPromiseCreator({
      userId: this.state.userId,
      password: this.state.password,
      phoneNumber: this.state.phoneNumber,
      verCode: this.state.verCode,
    })
      .then(() => {
        // this.props.dispatch(getBadgeInfoRoutine());
        console.debug('nextAction', this.nextAction);
        if (this.nextAction) {
          NavigatorService.dispatch(this.nextAction);
          this.nextAction = undefined;
          // this.props.navigation.setParams({nextAction: undefined})
        } else {
          NavigatorService.navigate(ROUTE_ROOT);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  onBtnVerButton = () => {
    this.props.sendVerCodePromiseCreator({
      userId: this.state.userId,
      password: this.state.password,
      phoneNumber: this.state.phoneNumber,
    })
      .then(() => {
        this.startCountDown();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  startCountDown() {
    this.setState({
      isWaitCode: true,
      verCodeText: '60 秒',
    }, () => {
      if (this.timer) clearInterval(this.timer);
      this.timer = setInterval(() => {
        InteractionManager.runAfterInteractions(() => {
          const currentNum = this.state.timerNum - 1;
          if (currentNum <= 0) {
            this.setState({
              timerNum: 60,
              isWaitCode: false,
              verCodeText: '重新发送',
            });
            if (this.timer) clearInterval(this.timer);
            this.timer = undefined;
          } else {
            this.setState({
              timerNum: currentNum,
              verCodeText: `${currentNum} 秒`,
            });
          }
        });
      }, 1000);
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Content
          theme={myTheme}
          style={{ flex: 1 }}
          contentContainerStyle={{
            minHeight: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Form style={{
            paddingLeft: 50,
            paddingRight: 50,
            width: '100%',
          }}
          >
            <Image
              style={styles.logo}
              resizeMode="cover"
              source={require('../../assets/logo.png')}
            />
            {EnableServerChange && (
              <View>
                <Item rounded style={[styles.item, { marginBottom: 0 }]}>
                  <TextInput
                    returnKeyType="next"
                    placeholderTextColor="#848484"
                    placeholder="认证服务器"
                    style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
                    value={this.state.authenticationHost}
                    autoCapitalize="none"
                    onChangeText={(text) => {
                      this.setState({ authenticationHost: text });
                      AuthenticationApiConstants.host = text;
                    }}
                  />
                </Item>
                <Item rounded style={[styles.item, { marginBottom: 0 }]}>
                  <TextInput
                    returnKeyType="next"
                    placeholderTextColor="#848484"
                    placeholder="现场服务器"
                    style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
                    value={this.state.portHost}
                    autoCapitalize="none"
                    onChangeText={(text) => {
                      this.setState({ portHost: text });
                      PortApiConstants.host = text;
                    }}
                  />
                </Item>
                <Item rounded style={[styles.item, { marginBottom: 0 }]}>
                  <TextInput
                    returnKeyType="next"
                    placeholderTextColor="#848484"
                    placeholder="待办服务器"
                    style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
                    value={this.state.toDoHost}
                    autoCapitalize="none"
                    onChangeText={(text) => {
                      this.setState({ toDoHost: text });
                      TodoApiConstants.host = text;
                    }}
                  />
                </Item>
              </View>
            )}
            <Item rounded style={[styles.item, { marginBottom: 0 }]}>
              <TextInput
                returnKeyType="next"
                placeholderTextColor="#848484"
                placeholder="用户名"
                style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
                value={this.state.userId}
                autoCapitalize="none"
                onChangeText={(text) => {
                  this.setState({ userId: text });
                }}
              />
            </Item>
            <View>
              <TextInput style={{
                backgroundColor: 'transparent',
                height: 20,
              }}
              />
              <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
              }}
              />
            </View>
            <Item rounded style={styles.item}>
              <TextInput
                secureTextEntry
                returnKeyType="go"
                placeholderTextColor="#848484"
                placeholder="密码"
                style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
                value={this.state.password}
                onChangeText={(text) => {
                  this.setState({ password: text });
                }}
              />
            </Item>
            <Item rounded style={styles.item}>
              <TextInput
                returnKeyType="next"
                keyboardType="numeric"
                multiline={false}
                onChangeText={(text) => {
                  this.setState({
                    phoneNumber: text,
                  });
                }}
                value={this.state.phoneNumber}
                placeholderTextColor="#848484"
                placeholder="手机号"
                style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
              />
            </Item>
            <Item rounded style={styles.item}>
              <TextInput
                keyboardType="numeric"
                returnKeyType="go"
                onChangeText={(text) => {
                  this.setState({
                    verCode: text,
                  });
                }}
                value={this.state.verCode}
                placeholderTextColor="#848484"
                placeholder="验证码"
                style={[commonStyles.input, styles.input,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}
              />
              <Button
                rounded
                disabled={this.state.isWaitCode}
                onPress={this.onBtnVerButton}
                style={styles.verButton}
              >
                <Text style={styles.timeText}>
                  {this.state.verCodeText}
                </Text>
              </Button>

            </Item>
          </Form>
          <Button
            style={styles.loginBtn}
            block
            rounded
            disabled={this.props.isLoading}
            onPress={this.onBtnLoginButton}
          >
            <Text style={{ color: 'white' }}>登录</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

Login.navigationOptions = {
  header: null,
};

Login.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  sendVerCodePromiseCreator: PropTypes.func.isRequired,
  loginPromiseCreator: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};
//全局state转局部props
const mapStateToProps = createStructuredSelector({
  isLoading: makeLoading(),
});
//全局方法转局部方法
function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      sendVerCodePromiseCreator,
      loginPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
