import { createSelector } from 'reselect';

const selectLoginDomain = () => state => state.login;

const makeLoading = () => createSelector(selectLoginDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

export { makeLoading };
