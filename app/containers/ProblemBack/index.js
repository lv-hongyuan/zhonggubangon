import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, PixelRatio ,Platform} from 'react-native';
import { connect } from 'react-redux';
import {
  Container, Content, Form, Item, Label, View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  makeSelectCreateProblemBackSuccess,
  makeSelectData,
} from './selectors';
import { createProblemBackAction } from './actions';
import screenHOC from '../../components/screenHOC';
import { TextInput } from '../../components/InputItem/TextInput';
import { backType } from './constants';
import commonStyles from '../../common/commonStyles';
import Selector from '../../components/Selector';
import SelectImageView from '../../components/SelectImageView';
const platform = Platform.OS;
const styles = StyleSheet.create({
  item: {
    borderBottomColor: 'transparent',
    // minHeight: ,
  },
});

@screenHOC
class ProblemBack extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: '意见反馈',
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="保存"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const handleSubmitFunc = navigation.getParam('submit', undefined);
            if (handleSubmitFunc) {
              requestAnimationFrame(() => {
                handleSubmitFunc();
              });
            }
          }}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      backPic: '',
      remark: '',
      backType: null,
    };
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  handleSubmit = () => {
    const param = {
      remark: this.state.remark,
      backType: this.state.backType,
      backPic: this.state.backPic,
    };
    this.setState(() => {
      this.props.dispatch(createProblemBackAction(param));
    });
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Container>
        <Content>
          <Form style={{
            backgroundColor: '#FFFFFF',
            padding: 10,
          }}
          >
            <View style={{ flex: 1, minWidth: '100%' }}>
              <Item style={commonStyles.inputItem}>
                <Label style={{ fontSize: 14, color: 'red' }}>问题类型:</Label>
                <Selector
                  value={this.state.backType}
                  items={backType}
                  getItemValue={item => item}
                  getItemText={item => item}
                  pickerTitle="请选择问题类型"
                  onSelected={(item) => {
                    this.setState({ backType: item });
                  }}
                />
              </Item>
              <Item style={styles.item}>
                <Label style={{
                  marginTop: 10,
                  fontSize: 14,
                  color: 'red',
                }}
                >
                  问题描述:
                </Label>
              </Item>
              <Item style={styles.item}>
                <TextInput
                  style={{
                    marginTop: 3,
                    width: '100%',
                    height: 150,
                    borderColor: '#E0E0E0',
                    borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
                    textAlignVertical: 'top',
                    fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()
                  }}
                  placeholder="请填写您的意见..."
                  multiline
                  value={this.state.remark}
                  onChangeText={(text) => {
                    this.setState({ remark: text });
                  }}
                />
              </Item>
              <Item style={styles.item}>
                <Label style={{
                  paddingLeft: 2,
                  fontSize: 14,
                  color: 'red',
                }}
                >
                  反馈图:
                </Label>
                <SelectImageView
                  style={{
                    marginTop: 10,
                    width: 100,
                    height: 100,
                    marginLeft: 5,
                  }}
                  title="选择图片"
                  source={this.state.backPic}
                  onPickImages={(images) => {
                    const image = images[0];
                    this.setState({
                      backPic: {
                        fileName: image.uri.split('/').reverse()[0],
                        fileSize: image.size,
                        path: image.uri,
                        uri: image.uri,
                      },
                    });
                  }}
                  onDelete={() => {
                    this.setState({
                      backPic: undefined,
                    });
                  }}
                />
              </Item>
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  success: makeSelectCreateProblemBackSuccess(),
  data: makeSelectData(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProblemBack);
