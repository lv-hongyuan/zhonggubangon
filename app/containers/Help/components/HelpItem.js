import React from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, TouchableHighlight,
} from 'react-native';
import Svg from '../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  info: {
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingTop: 5,
    paddingRight: 10,
    paddingBottom: 5,
    alignItems: 'center',
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
    flex: 1,
  },
});

class HelpItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const { wordName } = item;
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => {
          if (this.props.onPress) this.props.onPress(item);
        }}
      >
        <View style={styles.info}>
          <Text style={styles.label}>{wordName}</Text>
          <Svg icon="right_arrow" size={30} />
        </View>
      </TouchableHighlight>
    );
  }
}

HelpItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default HelpItem;
