import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'native-base';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { DeviceEventEmitter } from 'react-native';
import { ROUTE_AUTH, ROUTE_ROOT } from '../../RouteConstant';
import { makeIsLogin } from '../AppInit/selectors';
import { READY_TO_HANDLE_LAUNCH_NOTIFICATION } from '../../components/NotificationCenter/constants';

class Auth extends React.Component {
  componentDidMount() {
    if (this.props.isLogin) {
      this.props.navigation.navigate(ROUTE_ROOT);
    } else {
      this.props.navigation.navigate(ROUTE_AUTH);
    }
    DeviceEventEmitter.emit(READY_TO_HANDLE_LAUNCH_NOTIFICATION);
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }} />
    );
  }
}

Auth.navigationOptions = {
  header: null,
};

Auth.propTypes = {
  navigation: PropTypes.object.isRequired,
  isLogin: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isLogin: makeIsLogin(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
