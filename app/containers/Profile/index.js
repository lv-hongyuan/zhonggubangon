import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  PixelRatio,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import {
  Grid, Col, Row, Thumbnail, Container, Content, Label,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import Toast from 'teaset/components/Toast/Toast';
import DeviceInfo from 'react-native-device-info/deviceinfo';
import myTheme from '../../Themes';
import { ROUTE_AUTH, ROUTE_HELP, ROUTE_PROBLEM_BACK } from '../../RouteConstant';
import Svg from '../../components/Svg';
import { makeLoading } from './selectors';
import { makeUserInfo } from '../AppInit/selectors';
import { checkUpdate } from '../../common/checkUpdate';
import AlertView from '../../components/Alert';
import screenHOC from '../../components/screenHOC';
import { logoutPromiseCreator } from './actions';

const styles = StyleSheet.create({
  userAvatarContainer: {
    backgroundColor: '#dc001b',
    height: 150,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  row: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    borderColor: '#c9c9c9',
    borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
  },
  view: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  label: {
    flex: 1,
    marginStart: 10,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: '#969696',
  },
  logoutBtn: {
    marginTop: 20,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: '#535353',
  },
});

@screenHOC
class Profile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onBackPressed = () => false;

  confirmLogout() {
    AlertView.show({
      title: '确定要退出登录吗？',
      showCancel: true,
      confirmAction: () => {
        this.props.logoutPromiseCreator().then(() => {
          this.props.navigation.navigate(ROUTE_AUTH);
        }).catch(() => {
        });
      },
    });
  }

  render() {
    return (
      <Container>
        <Content theme={myTheme}>
          <SafeAreaView>
            <View style={styles.userAvatarContainer}>
              <Grid style={{ flex: 1 }}>
                <Row style={{
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                >
                  <Col style={{
                    height: '100%',
                    flex: 1,
                    justifyContent: 'center',
                  }}
                  >
                    <View style={{
                      justifyContent: 'center',
                      alignItems: 'flex-end',
                      paddingRight: 10,
                    }}
                    >
                      <Thumbnail large source={require('../../assets/default_avatar.png')} />
                    </View>
                  </Col>
                  <Col style={{
                    flex: 2,
                  }}
                  >
                    <View style={{
                      flexDirection: 'column',
                      height: '100%',
                      justifyContent: 'center',
                      alignItems: 'flex-start',
                    }}
                    >
                      <Text style={{
                        fontSize: 20,
                        fontWeight: '700',
                        color: '#FFFFFF',
                      }}
                      >
                        {this.props.userInfo.sysUser}
                      </Text>
                      <View style={{
                        marginTop: 5,
                        flexDirection: 'row',
                      }}
                      >
                        <Label style={{
                          color: '#FFFFFF',
                        }}
                        >
账号：
                        </Label>
                        <Text style={{
                          color: '#FFFFFF',
                        }}
                        >
                          {this.props.userInfo.sysUserCode}
                        </Text>
                      </View>
                    </View>
                  </Col>
                </Row>
              </Grid>
            </View>
            <Grid>
              <Row style={[styles.row, {
                marginTop: 20,
                borderTopWidth: 0,
              }]}
              >
                <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    this.props.navigation.navigate(ROUTE_HELP);
                  }}
                >
                  {/* <Svg icon='file' size={30}/> */}
                  <Label style={styles.label}>获取帮助</Label>
                  <Svg icon="right_arrow" size={30} />
                </TouchableOpacity>
              </Row>
              <Row style={[styles.row, {
                marginTop: 20,
                borderTopWidth: 0,
              }]}
              >
                <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    // Toast.message('敬请期待');
                  }}
                >
                  {/* <Svg icon='file' size={30}/> */}
                  <Label style={styles.label}>关于我们</Label>
                  <Svg icon="right_arrow" size={30} />
                </TouchableOpacity>
              </Row>
              <Row style={styles.row}>
                <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    this.props.navigation.push(ROUTE_PROBLEM_BACK);
                  }}
                >
                  {/* <Svg icon='file' size={30}/> */}
                  <Label style={styles.label}>意见反馈</Label>
                  <Svg icon="right_arrow" size={30} />
                </TouchableOpacity>
              </Row>
              <Row style={[styles.row, {
                marginTop: 20,
                borderTopWidth: 0,
              }]}
              >
                <TouchableOpacity
                  style={styles.view}
                  onPress={() => {
                    checkUpdate(true);
                  }}
                >
                  {/* <Svg icon='file' size={30}/> */}
                  <Label style={styles.label}>检查更新</Label>
                  <Label style={styles.text}>{DeviceInfo.getVersion()}</Label>
                  <Svg icon="right_arrow" size={30} />
                </TouchableOpacity>
              </Row>

              <TouchableOpacity
                style={styles.logoutBtn}
                onPress={() => {
                  this.confirmLogout();
                }}
              >
                <Label style={styles.btnText}>退出登录</Label>
              </TouchableOpacity>
            </Grid>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

Profile.navigationOptions = {
  title: '个人信息',
  headerLeft: <HeaderButtons />,
};

Profile.propTypes = {
  navigation: PropTypes.object.isRequired,
  logoutPromiseCreator: PropTypes.func.isRequired,
  userInfo: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isLoading: makeLoading(),
  userInfo: makeUserInfo(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      logoutPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
