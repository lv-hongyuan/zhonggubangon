/*
 *
 * Profile constants
 *
 */
export const LOGOUT_ACTION = 'app/Profile/LOGOUT_ACTION';
export const LOCAL_LOGOUT_ACTION = 'app/Profile/LOCAL_LOGOUT_ACTION';
