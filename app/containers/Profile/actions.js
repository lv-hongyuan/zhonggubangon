import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { LOCAL_LOGOUT_ACTION, LOGOUT_ACTION } from './constants';

export const localLogoutRoutine = createRoutine(LOCAL_LOGOUT_ACTION);
export const logoutRoutine = createRoutine(LOGOUT_ACTION);
export const logoutPromiseCreator = promisifyRoutine(logoutRoutine);
