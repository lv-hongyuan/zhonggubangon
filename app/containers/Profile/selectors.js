import { createSelector } from 'reselect';

const selectProfileDomain = () => state => state.profile;

const makeLoading = () => createSelector(selectProfileDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

export {
  makeLoading,
};
