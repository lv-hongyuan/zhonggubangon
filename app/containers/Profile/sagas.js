import { call, put, takeLatest } from 'redux-saga/effects';
import CookieManager from 'react-native-cookies';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import { logoutRoutine, localLogoutRoutine } from './actions';
import { errorMessage } from '../../components/ErrorHandler/actions';
import { saveUserInfo } from '../AppInit/sagas';

function* localLogout(action) {
  console.log(action);
  try {
    yield put(localLogoutRoutine.request());
    yield call(CookieManager.clearAll);
    yield call(saveUserInfo, { payload: undefined });
    yield put(localLogoutRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(localLogoutRoutine.failure(e));
  } finally {
    yield put(localLogoutRoutine.fulfill());
  }
}

export function* localLogoutSaga() {
  yield takeLatest(localLogoutRoutine.TRIGGER, localLogout);
}

function* logout(action) {
  console.log(action);
  try {
    yield put(logoutRoutine.request());
    const response = yield call(
      request,
      ApiFactory.logOut(),
    );
    console.log('logout', response);
    yield call(localLogout);
    yield put(logoutRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(logoutRoutine.failure(e));
  } finally {
    yield put(logoutRoutine.fulfill());
  }
}

export function* logoutSaga() {
  yield takeLatest(logoutRoutine.TRIGGER, logout);
}

// All sagas to be loaded
export default [
  localLogoutSaga,
  logoutSaga,
];
