import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  APP_INIT,
  SAVE_USER_INFO,
  BIND_REGISTRATION_ID,
  // GET_BADGE_INFO,
} from './constants';

export const appInitRoutine = createRoutine(APP_INIT);
export const appInitPromiseCreator = promisifyRoutine(appInitRoutine);

export const saveUserInfoRoutine = createRoutine(SAVE_USER_INFO);
export const saveUserInfoPromiseCreator = promisifyRoutine(saveUserInfoRoutine);

export const bindRegistrationIDRoutine = createRoutine(BIND_REGISTRATION_ID);
export const bindRegistrationIDPromiseCreator = promisifyRoutine(bindRegistrationIDRoutine);

// export const getBadgeInfoRoutine = createRoutine(GET_BADGE_INFO);
// export const getBadgeInfoPromiseCreator = promisifyRoutine(getBadgeInfoRoutine);
