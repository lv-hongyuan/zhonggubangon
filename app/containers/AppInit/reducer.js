import { appInitRoutine, bindRegistrationIDRoutine, saveUserInfoRoutine } from './actions';

const initState = {
  loading: false,
  user: undefined,
  appInit: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    // app初始化，从本地读取用户信息
    case appInitRoutine.TRIGGER:
      return { ...state, loading: true };
    case appInitRoutine.SUCCESS:
      return { ...state, user: action.payload, appInit: true };
    case appInitRoutine.FAILURE:
      return { ...state, error: action.payload, appInit: true };
    case appInitRoutine.FULFILL:
      return { ...state, loading: false };

      // 保存更新用户信息
    case saveUserInfoRoutine.TRIGGER:
      return { ...state, loading: true };
    case saveUserInfoRoutine.SUCCESS:
      return { ...state, user: action.payload };
    case saveUserInfoRoutine.FAILURE:
      return { ...state, user: undefined, error: action.payload };
    case saveUserInfoRoutine.FULFILL:
      return { ...state, loading: false };

      // 绑定推送ID
    case bindRegistrationIDRoutine.TRIGGER:
      return { ...state, loading: true };
    case bindRegistrationIDRoutine.SUCCESS:
      return { ...state, user: { ...state.user, deviceKey: action.payload } };
    case bindRegistrationIDRoutine.FAILURE:
      return { ...state, error: action.payload };
    case bindRegistrationIDRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
