/*
 *
 * Auth constants
 *
 */
export const APP_INIT = 'app/Auth/APP_INIT';
export const USER_INFO = 'app/Auth/USER_INFO';
export const SAVE_USER_INFO = 'app/Auth/SAVE_USER_INFO';
export const BIND_REGISTRATION_ID = 'app/Auth/BIND_REGISTRATION_ID';

// export const GET_BADGE_INFO = 'app/Auth/GET_BADGE_INFO';

export const USER_INFO_KEY = 'USER_INFO_KEY';

export const USER_NAME_KEY = 'USER_NAME_KEY';
export const USER_PHONE_KEY = 'USER_PHONE_KEY';
