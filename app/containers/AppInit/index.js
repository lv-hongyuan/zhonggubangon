import React from 'react';
import PropTypes from 'prop-types';
import { NetInfo, AppState } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect/es';
import { appInitRoutine } from './actions';
import { makePortWorkBadge } from '../../apps/onLand/containers/PortWork/selectors';
import NotificationCenter from '../../components/NotificationCenter';
import { checkUserInfo } from '../Login/sagas';

class AppInit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isConnect: undefined,
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    NotificationCenter.setBadge(0);
    // 监听网络
    NetInfo.isConnected.addEventListener('connectionChange', this.netChange);
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillReceiveProps(nextProps) {
    const badge = nextProps.portWorkBadge;
    NotificationCenter.setBadge(badge);
  }

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.netChange);
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  // 网络监控
  netChange = (isConnect) => {
    if (this.state.isConnect !== undefined && isConnect && !this.state.isConnect) {
      checkUserInfo().finally(() => {
        // this.props.getBadgeInfoRoutine();
      });
    }
    this.setState({ isConnect });
  };

  // app状态切换
  handleAppStateChange = (nextAppState) => {
    // App has come to the foreground!
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // this.checkAppUpdate();
      checkUserInfo().finally(() => {
        // this.props.getBadgeInfoRoutine();
      });
    }
    this.setState({ appState: nextAppState });
  };

  render() {
    return this.props.children;
  }
}

AppInit.propTypes = {
  // getBadgeInfoRoutine: PropTypes.func.isRequired,
  portWorkBadge: PropTypes.number.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

const mapStateToProps = createStructuredSelector({
  portWorkBadge: makePortWorkBadge(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators({
      appInitRoutine,
      // getBadgeInfoRoutine,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AppInit);
