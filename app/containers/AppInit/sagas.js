import { call, put, takeLatest } from 'redux-saga/effects';
import { AsyncStorage } from 'react-native';
import { USER_INFO_KEY, USER_NAME_KEY, USER_PHONE_KEY } from './constants';
import {
  appInitRoutine,
  bindRegistrationIDRoutine,
  // getBadgeInfoRoutine,
  saveUserInfoRoutine,
} from './actions';
import { errorMessage } from '../../components/ErrorHandler/actions';
import request from '../../utils/request';
import ApiFactory from '../../common/Api';
import AppStorage from '../../utils/AppStorage';
import { getPortWorkBadgeSaga } from '../../apps/onLand/Sagas';
import NotificationCenter from '../../components/NotificationCenter';
import {
  ApiConstants as AuthenticationApiConstants,
  AppPermission,
  AuthenticationHostKey, EnableServerChange,
} from '../../common/Constant';
import {
  ApiConstants as PortApiConstants,
  PortHostKey,
} from '../../apps/onLand/common/Constant';
import {
  ApiConstants as TodoApiConstants,
  BusinessApprovalHostKey,
} from '../../apps/BusinessApproval/common/Constant';

import uploadAppInfo from '../../common/uploadAppInfo';
import { checkUserInfo } from '../Login/sagas';

// app初始化
export function* appInit(action) {
  console.log(action);
  try {
    yield put(appInitRoutine.request());
    // 获取本地userInfo
    const userInfoStr = yield call(AsyncStorage.getItem, USER_INFO_KEY);
    const userInfo = userInfoStr && JSON.parse(userInfoStr);

    // 获取存储的Host
    if (EnableServerChange) {
      const authenticationHost = yield call(AsyncStorage.getItem, AuthenticationHostKey);
      if (authenticationHost) {
        AuthenticationApiConstants.host = authenticationHost;
      }
      const portHost = yield call(AsyncStorage.getItem, PortHostKey);
      if (portHost) {
        PortApiConstants.host = portHost;
      }
      const toDoHost = yield call(AsyncStorage.getItem, BusinessApprovalHostKey);
      if (toDoHost) {
        TodoApiConstants.host = toDoHost;
      }
    }

    yield put(appInitRoutine.success(userInfo));
    yield call(checkUserInfo);
    // 上传app版本信息
    yield call(uploadAppInfo, userInfo && userInfo);
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(appInitRoutine.failure(e));
  } finally {
    yield put(appInitRoutine.fulfill());
  }
}

export function* appInitSaga() {
  yield takeLatest(appInitRoutine.TRIGGER, appInit);
}

export async function saveLoginInput({ username, phone }) {
  await AsyncStorage.setItem(USER_NAME_KEY, username || '');
  await AsyncStorage.setItem(USER_PHONE_KEY, phone || '');
}

export async function getSavedLoginInput() {
  const username = await AsyncStorage.getItem(USER_NAME_KEY);
  const phone = await AsyncStorage.getItem(USER_PHONE_KEY);
  return {
    username,
    phone,
  };
}


// 保存用户数据
export function* saveUserInfo(action) {
  console.log(action);
  try {
    yield put(saveUserInfoRoutine.request());
    const userInfo = action.payload;
    if (userInfo) {
      yield call(AsyncStorage.setItem, USER_INFO_KEY, JSON.stringify(userInfo));
    } else {
      yield call(AsyncStorage.removeItem, USER_INFO_KEY);
    }
    yield put(saveUserInfoRoutine.success(userInfo));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(saveUserInfoRoutine.failure(e));
  } finally {
    yield put(saveUserInfoRoutine.fulfill());
  }
}

export function* saveUserInfoSaga() {
  yield takeLatest(saveUserInfoRoutine.TRIGGER, saveUserInfo);
}

// 绑定RegistrationID
function* bindRegistrationID(action) {
  console.log(action);
  try {
    yield put(bindRegistrationIDRoutine.request());
    const response = yield call(request, ApiFactory.bindRegistrationID(action.payload));
    console.log(response);
    const userInfo = AppStorage.userInfo && {
      ...AppStorage.userInfo,
      deviceKey: action.payload,
    };
    if (userInfo) {
      yield call(AsyncStorage.setItem, USER_INFO_KEY, JSON.stringify(userInfo));
    }
    yield put(bindRegistrationIDRoutine.success(action.payload));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(bindRegistrationIDRoutine.failure(e));
  } finally {
    yield put(bindRegistrationIDRoutine.fulfill());
  }
}

export function* bindRegistrationIDSaga() {
  yield takeLatest(bindRegistrationIDRoutine.TRIGGER, bindRegistrationID);
}

// app角标信息
// function* getBadgeInfo(action) {
//   console.log(action);
//   try {
//     yield put(getBadgeInfoRoutine.request());
//     let badge = 0;
//     if (AppStorage.permissionList.indexOf(AppPermission.Port) !== -1) {
//       badge += yield call(getPortWorkBadgeSaga);
//     }
//     NotificationCenter.setBadge(badge);
//     yield put(getBadgeInfoRoutine.success(badge));
//   } catch (e) {
//     console.log(e.message);
//     // yield put(errorMessage(e));
//     yield put(getBadgeInfoRoutine.failure(e));
//   } finally {
//     yield put(getBadgeInfoRoutine.fulfill());
//   }
// }

// export function* getBadgeInfoSaga() {
//   yield takeLatest(getBadgeInfoRoutine.TRIGGER, getBadgeInfo);
// }

// All sagas to be loaded
export default [appInitSaga, saveUserInfoSaga, bindRegistrationIDSaga];
