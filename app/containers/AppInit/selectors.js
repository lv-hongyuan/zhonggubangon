import { createSelector } from 'reselect/es';

const selectLoginDomain = () => state => state.app;

const makeAppInit = () => createSelector(selectLoginDomain(), (subState) => {
  console.debug(subState.appInit);
  return subState.appInit;
});

const makeIsLogin = () => createSelector(selectLoginDomain(), (subState) => {
  console.debug(subState.user);
  return !!subState.user;
});

const makeUserInfo = () => createSelector(selectLoginDomain(), (subState) => {
  console.debug(subState.user);
  return subState.user || {};
});

const makePermissions = () => createSelector(selectLoginDomain(), (subState) => {
  console.debug(subState.user);
  return ((subState.user || {}).functionUrl || '').split(',');
});

export {
  makeAppInit,
  makeIsLogin,
  makeUserInfo,
  makePermissions,
};
