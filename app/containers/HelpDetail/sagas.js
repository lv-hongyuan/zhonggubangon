import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../utils/request';
import ApiFactory from '../../apps/onLand/common/Api';
import { getHelpDetailRoutine } from './actions';
import { errorMessage } from '../../components/ErrorHandler/actions';

function* getHelpDetail(action) {
  console.log(action);
  try {
    yield put(getHelpDetailRoutine.request());
    const response = yield call(
      request,
      ApiFactory.getOneHandbook(action.payload),
    );
    console.log('getHelpDetail', response.toString());
    yield put(getHelpDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getHelpDetailRoutine.failure(e));
  } finally {
    yield put(getHelpDetailRoutine.fulfill());
  }
}

export function* getHelpDetailSaga() {
  yield takeLatest(getHelpDetailRoutine.TRIGGER, getHelpDetail);
}

// All sagas to be loaded
export default [
  getHelpDetailSaga,
];
