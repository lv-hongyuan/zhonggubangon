import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_HELP_DETAIL,
} from './constants';


export const getHelpDetailRoutine = createRoutine(GET_HELP_DETAIL);
export const getHelpDetailPromise = promisifyRoutine(getHelpDetailRoutine);
