import { routinePromiseWatcherSaga } from 'redux-saga-routines';
import appSagas from './containers/AppInit/sagas';
import loginSagas from './containers/Login/sagas';
import profileSagas from './containers/Profile/sagas';
import HelpSagas from './containers/Help/sagas';
import HelpDetailSagas from './containers/HelpDetail/sagas';
import problemBackSagas from './containers/ProblemBack/sagas';
import onLandSagas from './apps/onLand/Sagas';
import OASagas from './apps/OA/Sagas';
import BusinessApprovalSagas from './apps/BusinessApproval/Sagas';

export default function injectSagas(store) {
  const allSagas = [
    ...appSagas,
    ...loginSagas,
    ...profileSagas,
    ...HelpSagas,
    ...HelpDetailSagas,
    ...onLandSagas,
    ...OASagas,
    ...BusinessApprovalSagas,
    ...problemBackSagas,
    routinePromiseWatcherSaga,
  ];

  allSagas.map(store.runSaga);
}
