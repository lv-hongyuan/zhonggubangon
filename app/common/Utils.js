function formatTime(millisecond) {
  const date = new Date(millisecond);
  return `${date.getFullYear()} ${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
}

function getDaysInOneMonth(year, month) {
  month = parseInt(month, 10);
  const d = new Date(year, month, 0);
  return d.getDate();
}

export {
  formatTime,
  getDaysInOneMonth,
};
