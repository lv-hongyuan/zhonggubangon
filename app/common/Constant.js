import { AsyncStorage } from 'react-native';

export const APP_TYPE = 'port';
export const APP_NAME = 'PortWorkManagementApp';
export const AuthenticationHostKey = 'AuthenticationHostKey';

let host = 'auth.zhonggu56.com';

// let host = 'zx2.wsights.com:8980';
// let host = '192.168.100.136:8080';

const apiVersion = 'v1';

export const EnableServerChange = true;

export const ApiConstants = {
  get api() {
    return `http://${host}/app/${apiVersion}/`;
  },
  get host() {
    return host;
  },
  set host(newHost) {
    host = newHost;
    AsyncStorage.setItem(AuthenticationHostKey, newHost);
  },
};

export const APP_LOGOUT = 'APP_LOGOUT';

export const APP_NEED_UPDATE = 'APP_NEED_UPDATE';

export const AppPermission = {
  OA: '1',
  Port: '2',
  ToDo: '3',
};

export const SORT_ORDER = {
  ASC: 'ASC',
  DESC: 'DESC',
};
