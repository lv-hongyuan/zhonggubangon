import { Dimensions } from 'react-native';
import myTheme from '../Themes';

const { width, height } = Dimensions.get('window');
const isSmall = Math.min(width, height) < 375;

const commonStyles = {
  inputGroup: isSmall ? {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0,
  } : {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0,
  },
  inputItem: isSmall ? {
    width: '100%',
    height: 30,
    marginTop: 5,
    marginLeft: 0,
    marginBottom: 5,
    marginRight: 0,
    borderBottomWidth: myTheme.borderWidth,
    paddingLeft: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  } : {
    flex: 1,
    height: 30,
    borderBottomWidth: myTheme.borderWidth,
    marginTop: 5,
    marginLeft: 0,
    marginBottom: 5,
    marginRight: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  fixItem: {
    marginLeft: 10,
    marginRight: 10,
    flex: isSmall ? 0 : 1,
  },
  inputLabel: {
    fontSize: 14,
    color: '#535353',
  },
  readOnlyInputLabel: {
    fontSize: 14,
    color: '#969696',
  },
  input: {
    flex: 1,
    fontSize: 14,
    height: '100%',
    color: '#969696',
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  text: {
    flex: 1,
    fontSize: 14,
    color: '#969696',
    paddingVertical: 0,
    paddingLeft: 5,
    paddingRight: 5,
  },
  tail: {
    flex: 0,
    fontSize: 14,
    color: '#969696',
    paddingVertical: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  contentLabel: {
    fontSize: 14,
    color: '#535353',
    paddingLeft: 5,
  },
  swipeRow: {
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 0,
    paddingRight: 0,
    borderBottomWidth: 0,
  },
};

export default commonStyles;
