import DeviceInfo from 'react-native-device-info';
import React from 'react';
import { Linking, Platform, NativeModules, View, ActivityIndicator, Text, StyleSheet } from 'react-native';
import Overlay from 'teaset/components/Overlay/Overlay';
import request from '../utils/request';
import ApiFactory from '../apps/onLand/common/Api';
import AlertView from '../components/Alert';
import { APP_TYPE } from './Constant';

const styles = StyleSheet.create({
  container: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    marginTop: 10,
  },
});

let updateOverlay: Overlay.PopView;

function showUpdateDialog({
  versionCode, memo, url, mustUp,
}) {
  AlertView.show({
    title: '有新版本',
    message: `版本号：${versionCode}\n${memo}`,
    showCancel: mustUp !== 1,
    confirmAction: () => {
      const popView = (
        <Overlay.PopView
          modal
          ref={(ref) => {
            updateOverlay = ref;
          }}
          style={{ alignItems: 'center', justifyContent: 'center' }}
        >
          <View style={styles.container}>
            <ActivityIndicator />
            <Text style={styles.text}>正在更新</Text>
          </View>
        </Overlay.PopView>
      );
      Overlay.show(popView);
      if (Platform.OS === 'android') {
        NativeModules.upgrade.upgrade(url);
      } else {
        Linking.openURL(url);
      }
    },
  });
}

function needUpdate(serverVersion, localVersion) {
  const serverVersionParams = serverVersion.split('.');
  const localVersionParams = localVersion.split('.');

  for (let i = 0; i < Math.min(serverVersionParams.length, localVersionParams.length); i += 1) {
    const serverNumber = parseInt(serverVersionParams[i], 10) || 0;
    const localNumber = parseInt(localVersionParams[i], 10) || 0;

    if (serverNumber > localNumber) {
      return true;
    } else if (serverNumber < localNumber) {
      return false;
    }
  }

  return serverVersionParams.length > localVersionParams.length;
}

// function checkIosVersion() {
//   return fetch('https://itunes.apple.com/lookup?bundleId=com.guju.platform')
//   .then((response) => response.json())
//   .then((responseJson) => {
//        console.log(responseJson.results[0].version,"苹果应用商店版本");
//        console.log(responseJson.results[0].trackViewUrl,"苹果应用商店的下载地址");
//        //用户安装app的版本和应用商店app的版本比较
//       if (needUpdate(responseJson.results[0].version, DeviceInfo.getVersion())) {
//         Linking.openURL(responseJson.results[0].trackViewUrl).catch(err => console.error('跳转失败', err));
//       } else if (showAlert) {
//         AlertView.show({ title: '您使用的是最新版本' });
//       }
//   })
//   .catch((error) => {
//       console.error(error);
//   });
// }

export function checkUpdate(showAlert = false) {
  if (showAlert) {
    const popView = (
      <Overlay.PopView
        modal
        ref={(ref) => {
          updateOverlay = ref;
        }}
        style={{ alignItems: 'center', justifyContent: 'center' }}
      >
        <View style={styles.container}>
          <ActivityIndicator />
          <Text style={styles.text}>正在检查更新</Text>
        </View>
      </Overlay.PopView>
    );
    Overlay.show(popView);
  }

  if (Platform.OS === 'ios') {
    try {
      NativeModules.upgrade.upgrade('1500223106', (msg) => {
        if ('YES' == msg) {

          AlertView.show({
            title: '更新提示',
            message: '有新的版本,请前往AppStore下载',
            showCancel: false,
            confirmAction: () => {
              //跳转到APP Stroe  
              NativeModules.upgrade.openAPPStore('1500223106');
               return false
            }
          })
        } else {
          console.log('当前是最新版本')
          if (updateOverlay) updateOverlay.close();
          AlertView.show({ title: '您使用的是最新版本' });
        }
      })
    } catch (error) {
      if (updateOverlay) updateOverlay.close();
      if (showAlert) {
        AlertView.show({ message: error.message });
      }
    }
    return
  }

  return request(ApiFactory.checkUpdate({
    appKey: Platform.OS === 'ios' ? 'ios' : 'Android',
    type: APP_TYPE,
    versionCode: DeviceInfo.getVersion(),
  }))
    .then((response) => {
      if (updateOverlay) updateOverlay.close();
      const { versionCode, url } = response;
      if (versionCode && url) {
        if (needUpdate(versionCode, DeviceInfo.getVersion())) {
          showUpdateDialog(response);
        } else if (showAlert) {
          AlertView.show({ title: '您使用的是最新版本' });
        }
      }
    })
    .catch((error) => {
      if (updateOverlay) updateOverlay.close();
      if (showAlert) {
        AlertView.show({ message: error.message });
      }
    });
}
