// 上传app版本信息
import DeviceInfo from 'react-native-device-info/deviceinfo';
import { DeviceEventEmitter, Platform } from 'react-native';
import ApiFactory from '../apps/onLand/common/Api';
import request from '../utils/request';
import { AppPermission, APP_NEED_UPDATE } from './Constant';

export default async function uploadAppInfo(userInfo) {
  const userId = (userInfo || {}).sysUser;
  if (!userId) return;

  const permissions = ((userInfo || {}).functionUrl || '').split(',');
  if (permissions.indexOf(AppPermission.Port) === -1) return;

  try {
    const appInfo = {
      appKey: Platform.OS === 'ios' ? 'ios' : 'Android',
      appVersion: DeviceInfo.getVersion(),
      sysVersion: DeviceInfo.getSystemVersion(),
      phoneModel: DeviceInfo.getBrand(),
      userId,
    };
    //const response = await request(ApiFactory.uploadAppInfo(appInfo));
    // console.log(response);
  } catch (e) {
    if (`${e.code}` === '201') {
      DeviceEventEmitter.emit(APP_NEED_UPDATE);
    }
    console.log(e.message);
  }
}
