/**
 * Created by xu-jzh on 2017/7/18.
 */
import { ApiConstants } from './Constant';
import AppStorage from '../utils/AppStorage';

export const header = {
  content: {
    Accept: 'application/Json',
    'Content-Type': 'application/json',
  },
};

const ApiFactory = {
  // 发送验证码
  sendVerificationCode(param) {
    return fetch(`${ApiConstants.api}sendVerificationCode`, {
      method: 'POST',
      headers: {
        ...header.content,
      },
      body: JSON.stringify(param),
    });
  },
  // 登录
  login(account) {
    return fetch(`${ApiConstants.api}auth`, {
      method: 'POST',
      headers: {
        ...header.content,
      },
      body: JSON.stringify(account),
    });
  },
  // 登录
  userInfo(sysUser) {
    return fetch(`${ApiConstants.api}userInfo`, {
      method: 'POST',
      headers: {
        ...header.content,
      },
      body: JSON.stringify({
        sysUser,
        token: AppStorage.token,
      }),
    });
  },
  // 绑定推送id
  bindRegistrationID(deviceKey) {
    return fetch(`${ApiConstants.api}setCid`, {
      method: 'POST',
      headers: {
        ...header.content,
      },
      body: JSON.stringify({ deviceKey, token: AppStorage.token }),
    });
  },
  // 登出
  logOut() {
    return fetch(`${ApiConstants.api}logOut`, {
      method: 'POST',
      headers: {
        ...header.content,
      },
      body: JSON.stringify({
        token: AppStorage.token,
      }),
    });
  },
};

export default ApiFactory;
