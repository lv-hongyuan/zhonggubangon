import 'whatwg-fetch';
import { DeviceEventEmitter } from 'react-native';
import { APP_LOGOUT } from '../common/Constant';

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
function parseJSON(response) {
  let interfaceName="";
  if(response.url && response.url.length > 0){
    let OffLineApiUrl = response.url
    if(/([^\*\/\(\:]+)$/.test(OffLineApiUrl)){interfaceName=RegExp.$1;}
  }
  console.log('接口名：',interfaceName);
  return response.text().then((text) => {
    if (text.length > 0) {
      const json = JSON.parse(text);
      return new Promise.resolve(json);
    }
    throw new Error('数据解析错误');
  });
}

function processResult(responseJson) {
  if (Array.isArray(responseJson)) {
    return new Promise((resolve => resolve(responseJson)));
  }
  const code = `${responseJson._backcode}`;
  switch (code) {
    case '200':
      return new Promise((resolve => resolve(responseJson)));
    case '303':
    case '304':
    case '901':
      DeviceEventEmitter.emit(APP_LOGOUT);
      break;
    default:
      break;
  }

  const error = new Error(responseJson._backmes || '未知错误');
  error.code = parseInt(code);
  error.response = responseJson;
  throw error;
}

/**
 * Checks if a network request came back fine, and throws an error if not
 *
 * @param  {object} response   A response from a network request
 *
 * @return {object|undefined} Returns either the response, or throws an error
 */
function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  // const error = new Error(response.statusText);
  const error = new Error(`未知错误 ${response.status}`);
  error.response = response;
  return parseJSON(response)
    .then((responseJson) => {
      error.message = responseJson.error || `未知错误 ${response.status}`;
      error.code = responseJson.status;
      throw error;
    })
    .catch(() => {
      throw error;
    });
}

/**
 * Requests a URL, returning a promise
 *
 * @return {object} The api call
 * @param api
 */
export default function request(api) {
  return api
    .then(checkStatus)
    .then(parseJSON)
    .then(processResult)
    .catch(error=>{
      if(error.message == 'Network request failed'){
        error.message = '网络连接失败';
      }
      throw(error);
    })
}
