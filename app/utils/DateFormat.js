export function longToDate(long) {
  if (!Number.isInteger(long)) {
    return null;
  }
  try {
    const parseLong = parseInt(long, 10);
    const date = new Date();
    date.setTime(parseLong);
    return date;
  } catch (e) {
    return null;
  }
}

export function formatDate(date, format) {
  if (!date || !date.getFullYear || Number.isNaN(date.getFullYear())) {
    return null;
  }
  const o = {
    'M+': date.getMonth() + 1, // month
    'd+': date.getDate(), // day
    'h+': date.getHours(), // hour
    'm+': date.getMinutes(), // minute
    's+': date.getSeconds(), // second
    'q+': Math.floor((date.getMonth() + 3) / 3), // quarter
    S: date.getMilliseconds(), // millisecond
  };
  let result = format;
  if (/(y+)/.test(result)) {
    result = format.replace(
      RegExp.$1,
      (`${date.getFullYear()}`).substr(4 - RegExp.$1.length),
    );
  }
  Object.keys(o).forEach((k) => {
    if (new RegExp(`(${k})`).test(result)) {
      result = result.replace(
        RegExp.$1,
        RegExp.$1.length === 1 ? o[k] :
          (`00${o[k]}`).substr((`${o[k]}`).length),
      );
    }
  });
  return result;
}

export function defaultFormat(long) {
  const date = longToDate(long);
  return formatDate(date, 'yyyy-MM-dd hh:mm');
}

export function toDate(string, format) {
  const normalized = string.replace(/[^a-zA-Z0-9]/g, '-');
  const normalizedFormat = format.replace(/[^a-zA-Z0-9]/g, '-');
  const formatItems = normalizedFormat.split('-');
  const dateItems = normalized.split('-');

  const monthIndex = formatItems.indexOf('MM');
  const dayIndex = formatItems.indexOf('dd');
  const yearIndex = formatItems.indexOf('yyyy');
  const hourIndex = formatItems.indexOf('hh');
  const minutesIndex = formatItems.indexOf('mm');
  const secondsIndex = formatItems.indexOf('ss');
  const millisecondsIndex = formatItems.indexOf('SS');

  const today = new Date();

  const year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
  const month = monthIndex > -1 ? dateItems[monthIndex] - 1 : 0;
  const day = dayIndex > -1 ? dateItems[dayIndex] : 1;

  const hour = hourIndex > -1 ? dateItems[hourIndex] : 0;
  const minute = minutesIndex > -1 ? dateItems[minutesIndex] : 0;
  const second = secondsIndex > -1 ? dateItems[secondsIndex] : 0;
  const milliseconds = millisecondsIndex > -1 ? dateItems[millisecondsIndex] : 0;

  return new Date(year, month, day, hour, minute, second, milliseconds);
}
