import DeviceInfo from 'react-native-device-info';
import { Platform } from 'react-native';

export default function () {
  const info = {
    applicationName: DeviceInfo.getApplicationName(),
    brand: DeviceInfo.getBrand(),
    buildNumber: DeviceInfo.getBuildNumber(),
    bundleId: DeviceInfo.getBundleId(),
    carrier: DeviceInfo.getCarrier(),
    deviceCountry: DeviceInfo.getDeviceCountry(),
    deviceId: DeviceInfo.getDeviceId(),
    deviceLocale: DeviceInfo.getDeviceLocale(),
    deviceName: DeviceInfo.getDeviceName(),
    manufacturer: DeviceInfo.getManufacturer(),
    readableVersion: DeviceInfo.getReadableVersion(),
    systemName: DeviceInfo.getSystemName(),
    systemVersion: DeviceInfo.getSystemVersion(),
    version: DeviceInfo.getVersion(),
    isTablet: DeviceInfo.isTablet(),
  };

  if (Platform.OS === 'android') {
    info.apiLevel = DeviceInfo.getAPILevel();
  }

  return info;
}
