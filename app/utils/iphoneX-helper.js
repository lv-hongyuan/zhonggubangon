import { Dimensions, Platform } from 'react-native';

export function isIphoneX() {
  const window = Dimensions.get('window');
  return (
    Platform.OS === 'ios'
        && !Platform.isPad
        && !Platform.isTVOS
        && (window.height === 812 || window.width === 812)
  );
}

export function ifIphoneX(iphoneXStyle, regularStyle) {
  if (isIphoneX()) {
    return iphoneXStyle;
  }
  return regularStyle;
}

export const iphoneX = isIphoneX();
