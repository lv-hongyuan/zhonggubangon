import _ from 'lodash';
import memoize from 'memoize-one';
import { SORT_ORDER } from '../common/Constant';

const memoizeSort = memoize((list, sortType, sortDirection = SORT_ORDER.ASC) => {
  const sortList = _.clone(list);
  return (sortList || []).sort((itemA, itemB) => {
    const sortItemA = itemA[sortType];
    const sortItemB = itemB[sortType];

    if (sortItemA === sortItemB) {
      return 0;
    }

    if (sortDirection === SORT_ORDER.ASC) {
      if (_.isString(sortItemA) && _.isString(sortItemB)) {
        return sortItemA.localeCompare(sortItemB);
      } else if (_.isNumber(sortItemA) && _.isNumber(sortItemB)) {
        return sortItemA - sortItemB;
      }
      // 升序
      return 1;
    } else if (sortDirection === SORT_ORDER.DESC) {
      // 降序
      if (_.isString(sortItemA) && _.isString(sortItemB)) {
        return sortItemB.localeCompare(sortItemA);
      } else if (_.isNumber(sortItemA) && _.isNumber(sortItemB)) {
        return sortItemB - sortItemA;
      }
      // 升序
      return 1;
    }

    return 0;
  });
});

export default memoizeSort;
