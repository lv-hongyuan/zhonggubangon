export function suitableDateTime(time, minuteSep = 6) {
  // 忽略秒数，按分钟数取整
  const minuteTime = parseInt(time / 60000);
  const newSecTime = parseInt(minuteTime / minuteSep) * minuteSep + (minuteTime % minuteSep && minuteSep);
  return newSecTime * 60000;
}

export function currentSuitableTime(minuteSep = 6) {
  return suitableDateTime(new Date().getTime(), minuteSep);
}

export function currentSuitableDate() {
  const date = new Date();
  date.setTime(suitableDateTime(date.getTime()));
  return date;
}

export function dateWidthSuitable(time, minuteSep = 6) {
  const date = new Date();
  date.setTime(suitableDateTime(time, minuteSep));
  return date;
}

export function pureDate() {
  const date = new Date();
  date.setHours(0, 0, 0, 0);
  return date;
}
