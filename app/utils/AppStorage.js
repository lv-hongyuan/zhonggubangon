import store from '../Store';

const makeUserInfo = () => store.getState().app.user;

const AppStorage = {
  get token() {
    return (makeUserInfo() || {}).token;
  },
  get userId() {
    return (makeUserInfo() || {}).sysUserId;
  },
  get sysUser() {
    return (makeUserInfo() || {}).sysUser;
  },
  get userInfo() {
    return makeUserInfo();
  },
  get registrationId() {
    return (makeUserInfo() || {}).deviceKey;
  },
  get permissionList() {
    return ((makeUserInfo() || {}).functionUrl || '').split(',');
  },
};

export default AppStorage;
