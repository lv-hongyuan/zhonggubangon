import React from 'react';
import { AppRegistry, View, StatusBar ,Text ,TextInput} from 'react-native';
import { Provider } from 'react-redux';
import { StyleProvider } from 'native-base';
import Orientation from 'react-native-orientation';
import SplashScreen from 'rn-splash-screen';
import { APP_NAME } from './common/Constant';
import store from './Store';
import theme from './Themes';
import getTheme from '../native-base-theme/components';
import AppWithNavigationState from './AppNavigator';
import ErrorHandler from './components/ErrorHandler';
import NotificationCenter from './components/NotificationCenter';
// 引入引导页组件
import { checkUpdate } from './common/checkUpdate';
import AppInit from './containers/AppInit';
import { appInitPromiseCreator } from './containers/AppInit/actions';
import Loading from './components/Loading';
import InputAccessory from './components/InputAccessory';
//禁用字体大小跟随系统
Text.defaultProps={...(Text.defaultProps||{}),allowFontScaling:false};
TextInput.defaultProps={...(TextInput.defaultProps||{}),allowFontScaling:false};
class App extends React.Component {
  static checkAppUpdate() {
    checkUpdate();
  }

  constructor(props) {
    super(props);

    this.state = {
      appInit: false,
      Orientation:'PORTRAIT',
    };
  }

  componentDidMount() {
    // this locks the view to Portrait Mode
    appInitPromiseCreator(null, store.dispatch).finally(() => {
      this.setState({ appInit: true });
    });
    Orientation.addOrientationListener((orientation)=>{
      console.log(orientation);
      this.setState({Orientation:orientation})
    });
    Orientation.lockToPortrait();

    setTimeout(() => {
      SplashScreen.hide();
    }, 0);

    App.checkAppUpdate();
  }

  componentWillUnmount() {
    removeOrientationListener((orientation)=> {console.log(orientation)});
  }

  static renderInitApp() {
    return (
      <AppInit>
        <ErrorHandler dispatch={store.dispatch} style={{ flex: 1 }}>
          <NotificationCenter dispatch={store.dispatch} />
          <AppWithNavigationState />
        </ErrorHandler>
      </AppInit>
    );
  }

  render() {
    return (
      <StyleProvider style={getTheme(theme)}>
        <Provider store={store}>
          <View style={{ backgroundColor: '#DC001B', flex: 1 ,paddingTop:theme.platform == 'android' &&this.state.Orientation == 'PORTRAIT' ? 25 : 0}}>
            <StatusBar backgroundColor="#DC001B" barStyle="light-content" />
            {this.state.appInit ? App.renderInitApp() : <Loading />}
            <InputAccessory />
          </View>
        </Provider>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent(APP_NAME, () => App);
