import React from 'react';
import { DeviceEventEmitter } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  NavigationActions,
  createBottomTabNavigator,
  createSwitchNavigator,
} from 'react-navigation';
import { createStackNavigator, StackViewStyleInterpolator } from 'react-navigation-stack';
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from './components/Svg';
import { APP_LOGOUT, APP_NEED_UPDATE } from './common/Constant';
import {
  ROUTE_AUTH_LOADING,
  ROUTE_AUTH,
  ROUTE_LOGIN,
  ROUTE_ROOT,
  ROUTE_PLATFORM,
  ROUTE_PROFILE,
  ROUTE_PROFILE_STACK,
  ROUTE_PLATFORM_STACK,
  ROUTE_ON_LAND,
  ROUTE_BUSINESS_APPROVAL,
  ROUTE_OA,
  ROUTE_HELP,
  ROUTE_HELP_DETAIL,
  ROUTE_PROBLEM_BACK,
  ROUTE_SEA_MAP,
} from './RouteConstant';
import OaApp from './apps/OA/Navigator';
import OnLandApp from './apps/onLand/Navigator';
import BusinessApprovalApp from './apps/BusinessApproval/Navigator';
import Login from './containers/Login';
import Platforms from './containers/Platforms';
import Auth from './containers/Auth';
import Profile from './containers/Profile';
import Help from './containers/Help';
import HelpDetail from './containers/HelpDetail';
import ProblemBack from './containers/ProblemBack';
import { localLogoutRoutine } from './containers/Profile/actions';
import NavigatorService from './NavigatorService';
import { checkUpdate } from './common/checkUpdate';
import SeaMapApp from './apps/SeaMap/Navigator';

const defaultNavigationOptions = ({ navigation }) => {
  const options = {
    gesturesEnabled: false,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#DC001B',
    },
    headerBackTitle: null,
    headerTitleStyle: {
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
    },
    headerTitleAllowFontScaling:false,//禁用字体跟随系统字体大小变化
    headerLeft: (
      <HeaderButtons color="white">
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons />
    ),
  };
  if (navigation.state.routes) {
    options.header = null;
  }
  return options;
};

const AuthNavigator = createStackNavigator(
  {
    [ROUTE_LOGIN]: { screen: Login },
  },
  {
    initialRouteName: ROUTE_LOGIN,
    initialRouteKey: ROUTE_LOGIN,
    navigationOptions: {
      headerMode: 'none',
    },
  },
);

const ProfileNavigator = createStackNavigator({
  [ROUTE_PROFILE]: { screen: Profile },
  [ROUTE_HELP]: { screen: Help },
  [ROUTE_HELP_DETAIL]: { screen: HelpDetail },
  [ROUTE_PROBLEM_BACK]: { screen: ProblemBack },
}, {
  initialRouteKey: ROUTE_PROFILE,
  initialRouteName: ROUTE_PROFILE,
  navigationOptions: defaultNavigationOptions,
});

const platformsNavigator = createStackNavigator({
  [ROUTE_PLATFORM]: { screen: Platforms },
  [ROUTE_ON_LAND]: { screen: OnLandApp },
  [ROUTE_BUSINESS_APPROVAL]: { screen: BusinessApprovalApp },
  [ROUTE_OA]: { screen: OaApp },
  [ROUTE_SEA_MAP]:{screen:SeaMapApp},
}, {
  initialRouteKey: ROUTE_PLATFORM,
  initialRouteName: ROUTE_PLATFORM,
  mode: 'modal',
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
  navigationOptions: defaultNavigationOptions,
});

const HomeNavigator = createBottomTabNavigator(
  {
    [ROUTE_PLATFORM_STACK]: {
      screen: platformsNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '业务列表',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_portwork" size="24" color={tintColor} />
        ),
      }),
    },
    [ROUTE_PROFILE_STACK]: {
      screen: ProfileNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '个人信息',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_profile" size="24" color={tintColor} />
        ),
      }),
    },
  },
  {
    swipeEnabled: false,
    animationEnabled: false,
    backBehavior: 'none',
    tabBarOptions: {
      allowFontScaling:false,
      activeTintColor: '#DC001B',
      inactiveTintColor: '#838383',
      showLabel: true,
      showIcon: true,
      style: {
        backgroundColor: 'white',
      },
    },
  },
);

export const AppNavigator = createSwitchNavigator(
  {
    [ROUTE_AUTH_LOADING]: Auth,
    [ROUTE_AUTH]: AuthNavigator,
    [ROUTE_ROOT]: HomeNavigator,
  },
  {
    initialRouteName: ROUTE_AUTH_LOADING,
  },
);

class AppWithNavigationState extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  componentDidMount() {
    const self = this;
    this.logoutEmitter = DeviceEventEmitter.addListener(APP_LOGOUT, () => {
      self.props.dispatch(localLogoutRoutine());
      NavigatorService.navigate(ROUTE_LOGIN);
    });
    this.updateEmitter = DeviceEventEmitter.addListener(APP_NEED_UPDATE, () => {
      checkUpdate(true);
    });
  }

  componentWillUnmount() {
    this.logoutEmitter.remove();
    this.updateEmitter.remove();
  }

  onBackPressed = () => {
    const { dispatch } = this.props;
    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    return (
      <AppNavigator ref={(ref) => { NavigatorService.setTopLevelNavigator(ref); }} />
    );
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
