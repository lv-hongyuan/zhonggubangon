import React from 'react';
import { NavigationActions } from 'react-navigation';
import { createStackNavigator, StackViewStyleInterpolator } from 'react-navigation-stack';
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../components/Svg';

import SeaMapDetail from './containers/SeaMapDetail';
import {
    ROUTE_SEA_MAP_DETAIL,
} from './RouteConstant';

const defaultNavigationOptions = ({ navigation }) => ({
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#DC001B',
    },
    headerBackTitle: null,
    headerTitleStyle: {
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
    },
    headerTitleAllowFontScaling:false,//禁用字体跟随系统字体大小变化
    headerLeft: (
      <HeaderButtons color="white">
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons />
    ),
  });


  const HomeNavigator = createStackNavigator({
    [ROUTE_SEA_MAP_DETAIL]: { screen: SeaMapDetail },
  }, {
    initialRouteKey: ROUTE_SEA_MAP_DETAIL,
    initialRouteName: ROUTE_SEA_MAP_DETAIL,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });
  
  export default HomeNavigator;
