import React from 'react';
import PropTypes from 'prop-types';
import { Platform, StatusBar, SafeAreaView,WebView } from 'react-native';
import BridgeWebView from '../../../../components/BridgeWebView';
import Svg from '../../../../components/Svg';
import Loading from '../../../../components/Loading';
import screenHOC from '../../../../components/screenHOC';
import NavigatorService from '../../../../NavigatorService';
import myTheme from '../../../../Themes';
import htmlRequire from '../../../../components/BridgeWebView/htmlRequire';
import { createStructuredSelector } from 'reselect/es';
import { connect } from 'react-redux';
import HeaderButtons from 'react-navigation-header-buttons';
import { Container } from 'native-base';
import { ApiConstants } from '../../../onLand/common/Constant'



@screenHOC
class SeaMapDetail extends React.PureComponent {
    constructor(props) {
        super(props);
    
        this.state = {
          renderWebView: false,
          isLoading: false,
        //   source: 'http://121.46.233.83:8180/index.html#/map/appMap',
        source: 'http://'+ApiConstants+'/api/map-app.html',
        // source: 'http://172.160.30.44:8080/api/map-app.html',

        };
    }

    componentDidMount() {
        this.props.navigation.setParams({
          title: '海图系统',
        });
      }

      onLoadEnd = () => {
        this.setState({ isLoading: false });
      };
      onLoadStart = (e) => {
        this.setState({ isLoading: true });
      };

      didFocus() {
        this.setState({
            renderWebView: true,
          });
      }

      renderLoading() {
        return this.state.isLoading
          ? <Loading backgroundStyle={{ backgroundColor: 'transparent' }} indicatorColor="#3d95d5" /> : null;
      }

      render() {
        return (
          <Container theme={myTheme}>
            <StatusBar backgroundColor="#3d95d5" barStyle="light-content" />
            <SafeAreaView style={{ flex: 1 }}>
              {
                this.state.renderWebView && (
                  <WebView
                    navigation={this.props.navigation}
                    url={this.state.source}
                    onLoadStart={this.onLoadStart}
                    onLoadEnd={this.onLoadEnd}
                  />
                )
              }
            </SafeAreaView>
            {this.renderLoading()}
          </Container>
        );
      }
}

SeaMapDetail.navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', ''),
    gesturesEnabled: false,
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#3d95d5',
    },
    headerBackTitle: null,
    headerTitleStyle: {
      fontSize: 18,
      textAlign: 'center',
      flex: 1,
    },
  });
  
  SeaMapDetail.propTypes = {
    navigation: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    isLoading: PropTypes.bool,
  };
  
  const mapStateToProps = createStructuredSelector({
    // path: makePath(),
  });
  
  function mapDispatchToProps(dispatch) {
    return {
      dispatch,
    };
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(SeaMapDetail);