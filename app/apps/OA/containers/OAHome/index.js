import React from 'react';
import PropTypes from 'prop-types';
import { Platform, StatusBar, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { Container } from 'native-base';
import CookieManager from 'react-native-cookies';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
import { makePath } from './selectors';
import myTheme from '../../../../Themes';
import BridgeWebView from '../../../../components/BridgeWebView';
import htmlRequire from '../../../../components/BridgeWebView/htmlRequire';
import { oaServerURL } from '../../common/Constant';
import ExchangeService from '../../common/service';
import AppStorage from '../../../../utils/AppStorage';
import Svg from '../../../../components/Svg';
import Loading from '../../../../components/Loading';
import screenHOC from '../../../../components/screenHOC';
import NavigatorService from '../../../../NavigatorService';

const defaultSource = htmlRequire(
  require('../../../../assets/www/MobileHome.html'),
  'MobileHome.html',
  { firstPage: null },
);

function makeFullPath(path) {
  let _path = path || '';
  if (_path.indexOf('/') === 0) {
    _path = _path.slice(1, _path.length);
  }
  return `${oaServerURL}${_path}`;
}

@screenHOC
class OAHome extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      renderWebView: false,
      isLoading: false,
      source: defaultSource,
    };

    this.isHome = props.path ? props.path.indexOf('/MobileHome.html') !== -1 : true;
    this.backKey = undefined;
    this.cachePath = props.path;
    this.finishInit = false;
    this.history = [];
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onBackPressed: this.onBackPressed.bind(this),
      title: 'OA系统',
    });
    this.setState({ isLoading: true });
  }

  didFocus() {
    if (!this.finishInit) {
      this.deleteCookies().then(() => {
        this.setCookie().then(() => {
          this.finishInit = true;
          if (this.cachePath) {
            this.navigateTo(makeFullPath(this.cachePath));
            this.cachePath = undefined;
          } else {
            this.setState({
              renderWebView: true,
            });
          }
        });
      });
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.path && nextProps !== this.props.path) {
      this.props.navigation.setParams({ path: undefined });
      const path = nextProps.path;
      if (path) {
        if (this.finishInit) {
          this.navigateTo(makeFullPath(path));
        } else {
          this.cachePath = path;
        }
      }
    }
  }

  backToHome() {
    this.isHome = true;
    this.setState({
      renderWebView: false,
      source: defaultSource,
    }, () => {
      this.setState({ renderWebView: true });
    });
  }

  navigateTo(url) {
    if (url === defaultSource.uri) {
      this.backToHome();
    } else {
      this.isHome = false;
      this.setState({
        renderWebView: false,
        source: { uri: url },
      }, () => {
        this.setState({ renderWebView: true });
      });
    }
  }

  onBackPressed = () => {
    // console.log('---------------------')
    // console.log('history', this.history)
    // console.log('backKey', this.backKey)
    // console.log('isHome', this.isHome)
    // console.log('---------------------')
    if (this.isHome) {
      NavigatorService.back();
    } else if (this.backKey) {
      if (this.backKey === 'HOME_PAGE') {
        this.backToHome();
      } else {
        this.navigateTo(makeFullPath(this.backKey));
      }
    } else {
      this.history.pop();
      const preUrl = this.history.pop();
      if (preUrl) {
        this.navigateTo(preUrl);
      } else {
        this.backToHome();
      }
    }
    this.backKey = undefined;
    return true;
  };

  setCookie = () => {
    const token = AppStorage.token || '';
    const customSession = `CustomSession=${token}; path=/; HttpOnly`;
    const cookie = Platform.OS === 'ios' ? { 'Set-Cookie': customSession } : customSession;
    return CookieManager.setFromResponse(oaServerURL, cookie);
  };

  showCookies = () => {
    CookieManager.get(oaServerURL).then((res) => {
      console.log('CookieManager.get =>', res); // => 'user_session=abcdefg; path=/;'
    });
  };

  deleteCookies = () => CookieManager.clearAll();

  onTitleChange = (title) => {
    this.props.navigation.setParams({ title: (title && title.length > 0) ? title : 'OA系统' });
  };

  onUrlChange = (url) => {
    this.isHome = url.indexOf('/MobileHome.html') !== -1;
  };

  onLoadStart = (e) => {
    this.backKey = undefined;

    function purePath(url) {
      const index = url.indexOf('?');
      if (index !== -1) {
        return url.substring(0, index).toLowerCase();
      }
      return url.toLowerCase();
    }

    let index = -1;
    for (let i = 0; i < this.history.length; i += 1) {
      const hisUrl = this.history[i];
      if (purePath(hisUrl) === purePath(e)) {
        index = i;
        break;
      }
    }
    if (index !== -1) {
      this.history = this.history.slice(0, index);
    }
    this.history.push(e);
    this.setState({ isLoading: true });
  };

  onLoadEnd = () => {
    console.log('isLoading: false');
    
    this.setState({ isLoading: false });
  };

  onChangeBackKey = (backKey) => {
    console.log('backKey', backKey);
    this.backKey = backKey;
  };

  renderLoading() {
    return this.state.isLoading
      ? <Loading backgroundStyle={{ backgroundColor: 'transparent' }} indicatorColor="#3d95d5" /> : null;
  }

  render() {
    return (
      <Container theme={myTheme}>
        <StatusBar backgroundColor="#3d95d5" barStyle="light-content" />
        <SafeAreaView style={{ flex: 1 }}>
          {
            this.state.renderWebView && (
              <BridgeWebView
                navigation={this.props.navigation}
                onUrlChange={this.onUrlChange}
                serviceContainer={{ ExchangeService }}
                source={this.state.source}
                onTitleChange={this.onTitleChange}
                onLoadStart={this.onLoadStart}
                onLoadEnd={this.onLoadEnd}
                onChangeBackKey={this.onChangeBackKey}
              />
            )
          }
        </SafeAreaView>
        {this.renderLoading()}
      </Container>
    );
  }
}

OAHome.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', ''),
  gesturesEnabled: false,
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#3d95d5',
  },
  headerBackTitle: null,
  headerTitleStyle: {
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
  },
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={navigation.getParam('onBackPressed', () => {
          navigation.dispatch(NavigationActions.back());
        })}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons />
  ),
});

OAHome.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  path: makePath(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OAHome);
