import { createSelector } from 'reselect';

const selectOAHomeDomain = () => state => state.OAHome;

const makeLoading = () => createSelector(
  selectOAHomeDomain(),
  (subState) => {
    console.debug(subState.isLoading);
    return subState.isLoading;
  },
);

const makePath = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState && subState.path);
    return subState && subState.path;
  },
);

export { makeLoading, makePath };
