import OAHomeSagas from './containers/OAHome/sagas';

const allSagas = [
  ...OAHomeSagas,
];

export default allSagas;
