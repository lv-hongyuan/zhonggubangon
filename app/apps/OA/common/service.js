import { oaServerURL } from './Constant';

const ExchangeService = {

  showSettingDialog(command) {
    return 'showSettingDialog';
  },

  getLoginSystemInfo() {
    const name = '';
    const password = '';
    const rid = '';
    const screenHeight = '';
    const info = [oaServerURL, name, password, rid, screenHeight].join('@');
    return info;
  },

  getMobileHeight() {
    return 'getMobileHeight';
  },

  screenChange() {
    return 'screenChange';
  },

  getRegistrationID() {
    return '';
  },

  ShowListItem() {
    return 'ShowListItem';
  },

  ShowImgDetail() {
    return 'ShowImgDetail';
  },

  startXMPPClient() {
    return 'startXMPPClient';
  },

  closeXMPPClient() {
    return 'closeXMPPClient';
  },

  openSession(loginid, name) {
    return 'openSession';
  },

  viewXmppMessage(others, name) {
    return 'viewXmppMessage';
  },

  searchVersion() {
    return 'searchVersion';
  },

  upDateApp() {
    return 'upDateApp';
  },

};

export default ExchangeService;
