import OAHomeReducer from './containers/OAHome/reducer';

const reducers = {
  OAHome: OAHomeReducer,
};

export default reducers;
