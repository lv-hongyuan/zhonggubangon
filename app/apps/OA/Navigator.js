import React from 'react';
import { createStackNavigator, NavigationActions } from 'react-navigation';
import HeaderButtons from 'react-navigation-header-buttons';
import OAHome from './containers/OAHome';
import { ROUTE_OA_HOME } from './RouteConstant';
import Svg from '../../components/Svg';

const defaultNavigationOptions = ({ navigation }) => ({
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#DC001B',
  },
  headerBackTitle: null,
  headerTitleStyle: {
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
  },
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={() => {
          navigation.dispatch(NavigationActions.back());
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons />
  ),
});

const HomeStack = createStackNavigator({
  [ROUTE_OA_HOME]: { screen: OAHome },
}, {
  initialRouteKey: ROUTE_OA_HOME,
  initialRouteName: ROUTE_OA_HOME,
  navigationOptions: defaultNavigationOptions,
});

export default HomeStack;
