import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
  getSpecialApplicationAllPortRoutine,
  getSpecialApplicationAllAreaRoutine
} from './actions';

function* getSpecialApplicationAllPort(action) {
  console.log(action);
  const { tjSfgPq } = action.payload;
  try {
    yield put(getSpecialApplicationAllPortRoutine.request({ tjSfgPq }));
    const response = yield call(request, ApiFactory.getSpecialApplicationAllPort(tjSfgPq));
    console.log(response);
    const { port } = response;
    yield put(getSpecialApplicationAllPortRoutine.success({
      portList: port || [],
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getSpecialApplicationAllPortRoutine.failure({
      portList: [],
      error: e,
    }));
  } finally {
    yield put(getSpecialApplicationAllPortRoutine.fulfill());
  }
}

function* getSpecialApplicationAllArea(action) {
  console.log(action);
  try {
    yield put(getSpecialApplicationAllAreaRoutine.request({}));
    const response = yield call(request, ApiFactory.getSpecialApplicationAllArea());
    console.log(response);
    const { area } = response;
    console.log("area", area);
    yield put(getSpecialApplicationAllAreaRoutine.success({
      areaList: area || [],
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getSpecialApplicationAllAreaRoutine.failure({
      areaList: [],
      error: e,
    }));
  } finally {
    yield put(getSpecialApplicationAllAreaRoutine.fulfill());
  }
}

export function* getSpecialApplicationAllPortSaga() {
  // 片区
  yield takeLatest(getSpecialApplicationAllAreaRoutine.TRIGGER, getSpecialApplicationAllArea);
  // 始发港
  yield takeLatest(getSpecialApplicationAllPortRoutine.TRIGGER, getSpecialApplicationAllPort);
}

// All sagas to be loaded
export default [getSpecialApplicationAllPortSaga,];
