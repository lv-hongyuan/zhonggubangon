/*
 *
 * BusinessApproval constants
 *
 */
export const GET_SPECIAL_APPLICATION_ALL_PORT = 'onLand/BusinessApproval/GET_SPECIAL_APPLICATION_ALL_PORT';
export const GET_SPECIAL_APPLICATION_ALL_AREA = 'onLand/BusinessApproval/GET_SPECIAL_APPLICATION_ALL_AREA';
