import { getSpecialApplicationAllPortRoutine, getSpecialApplicationAllAreaRoutine } from './actions';

const defaultState = {
  portList: [],
  areaList:[],
};

export default function(state = defaultState, action) {
  switch (action.type) {
    // 获取全部始发港
    case getSpecialApplicationAllPortRoutine.REQUEST:
      return {
        ...state,
      };
    case getSpecialApplicationAllPortRoutine.SUCCESS: {
      const { portList } = action.payload;
      return { ...state, portList: portList, };
    }
    case getSpecialApplicationAllPortRoutine.FAILURE: {
      const { portList } = action.payload;
      return {
        ...state,
        portList: portList ? state.portList : [],
      };
    }
    case getSpecialApplicationAllPortRoutine.FULFILL:
      return {
        ...state,
      };


    // 获取片区
    case getSpecialApplicationAllAreaRoutine.REQUEST:
      return {
        ...state,
      };
    case getSpecialApplicationAllAreaRoutine.SUCCESS: {
      const { areaList } = action.payload;
      return { ...state, areaList: areaList, };
    }
    case getSpecialApplicationAllAreaRoutine.FAILURE: {
      const { areaList } = action.payload;
      return {
        ...state,
        areaList: areaList ? state.areaList : [],
      };
    }
    case getSpecialApplicationAllAreaRoutine.FULFILL:
      return {
        ...state,
      };

    default:
      return state;
  }
}
