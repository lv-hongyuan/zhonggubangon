import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { FlatList, Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { ROUTE_SPECIAL_APPLICATION_DETAIL, ROUTE_SPECIAL_APPLICATION_SEARCH } from '../../RouteConstant';
import { getSpecialApplicationAllPortPromise, getSpecialApplicationAllAreaPromise } from './actions';
import { makePortList, makeAreaList } from './selectors';
import Selector from '../../../../components/Selector';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from "../../../../common/commonStyles";
import InputItem from "../../../../components/InputItem";
import myTheme from "../../../../Themes";
import SelectBox from "../../../../components/SelectBox";

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

@screenHOC
class SpecialApplicationSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputTjSfgPq: '',
      inputTjSfg: '',
    }
  }

  componentDidMount() {
    let inputTjSfgPq = this.props.navigation.getParam('inputTjSfgPq');
    let inputTjSfg = this.props.navigation.getParam('inputTjSfg');
    console.log('搜索页面:', { inputTjSfgPq, inputTjSfg });

    this.searchSfgPq();
    this.searchSfg(inputTjSfgPq);
    this.setState({
      inputTjSfgPq: inputTjSfgPq,
      inputTjSfg: inputTjSfg
    });
  }

  searchSfgPq = () => {
    // 查询片区
    this.props.getSpecialApplicationAllAreaPromise({}).then(() => {
    }).catch(() => {
    });
  }

  searchSfg = (inputTjSfgPq) => {
    // 根据片区查询 始发港
    this.props.getSpecialApplicationAllPortPromise({
      tjSfgPq: inputTjSfgPq
    }).then(() => {
    }).catch(() => {
    });
  }


  onSearchSubmit = () => {
    const onBack = this.props.navigation.getParam('onBack');
    if (onBack) {
      onBack({
        inputTjSfgPq: this.state.inputTjSfgPq.indexOf("片区") === -1 && this.state.inputTjSfgPq
          ? this.state.inputTjSfgPq + '片区' : this.state.inputTjSfgPq,
        inputTjSfg: this.state.inputTjSfg,
      });
    }
    this.props.navigation.goBack();
  }


  render() {
    return (
      <View style={{ ...styles.container }}>
        <SafeAreaView style={{ width: '100%' }}>
          <View style={{ padding: 10, }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                autoFocus
                label={'请选择片区：'}
                returnKeyType="search"
                value={this.state.inputTjSfgPq}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputTjSfgPq: text });
                }}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label={'请选择始发港:'}
                returnKeyType="search"
                value={this.state.inputTjSfg}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputTjSfg: text });
                }}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                }}
              />
            </InputGroup>
          </View>
          <Button
            block
            onPress={() => {
              Keyboard.dismiss();
              this.onSearchSubmit();
            }}
            style={{
              height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
            }}
          >
            <Text style={{ color: '#ffffff' }}>搜索</Text>
          </Button>
        </SafeAreaView>
      </View>
    );
  }
}

// SpecialApplicationSearch.navigationOptions = () => ({
//   title: '特价申请',
// });
SpecialApplicationSearch.navigationOptions = ({navigation}) => ({
  title: navigation.getParam('dbName', '特价申请'),
});

SpecialApplicationSearch.propTypes = {
  navigation: PropTypes.object.isRequired,
  getSpecialApplicationAllPortPromise: PropTypes.func.isRequired,
  getSpecialApplicationAllAreaPromise: PropTypes.func.isRequired,
};
SpecialApplicationSearch.defaultProps = {
  portList: [],
  areaList: [],
};

const mapStateToProps = createStructuredSelector({
  portList: makePortList(),
  areaList: makeAreaList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSpecialApplicationAllPortPromise,
      getSpecialApplicationAllAreaPromise
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialApplicationSearch);
