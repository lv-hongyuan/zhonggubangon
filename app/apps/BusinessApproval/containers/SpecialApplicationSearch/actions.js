import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_SPECIAL_APPLICATION_ALL_PORT,
  GET_SPECIAL_APPLICATION_ALL_AREA
} from './constants';

// 查询港口
export const getSpecialApplicationAllPortRoutine = createRoutine(GET_SPECIAL_APPLICATION_ALL_PORT);
export const getSpecialApplicationAllPortPromise = promisifyRoutine(getSpecialApplicationAllPortRoutine);

// 查询片区
export const getSpecialApplicationAllAreaRoutine = createRoutine(GET_SPECIAL_APPLICATION_ALL_AREA);
export const getSpecialApplicationAllAreaPromise = promisifyRoutine(getSpecialApplicationAllAreaRoutine);
