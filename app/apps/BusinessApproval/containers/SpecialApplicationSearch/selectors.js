import { createSelector } from 'reselect/es';

const selectSpecialApplicationSearchDomain = () => state => state.specialApplicationSearch;

const makePortList = () => createSelector(selectSpecialApplicationSearchDomain(), (subState) => {
  console.debug(subState);
  return subState.portList;
});

const makeAreaList = () => createSelector(selectSpecialApplicationSearchDomain(), (subState) => {
  console.debug(subState);
  return subState.areaList;
});

export {
  makePortList,
  makeAreaList,
};
