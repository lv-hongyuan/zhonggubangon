import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  ROUTE_SPECIAL_APPLICATION_DETAIL,
  ROUTE_SPECIAL_APPLICATION_SEARCH,
  ROUTE_DEMURRAGE_BOX_DETAIL
} from '../../RouteConstant';
import { getDemurrageListPromise } from './actions';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from "../../../../common/commonStyles";
import _ from "lodash";
import InputItem from "../../../../components/InputItem";
import { NavigationActions } from "react-navigation";
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../../../components/Svg';
import DatePullSelector from '../../../../components/DatePullSelector/index';
import { preComputationDemurragePromise } from './actions';
import AlertView from "../../../../components/Alert";
import { DATE_WHEEL_TYPE } from "../../../../components/DateTimeWheel";


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
  bottomContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#ffffff',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  approveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  tips: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'transparent',
    paddingLeft: 0,
    paddingRight: 0,
    fontSize: 8
  }
});

@screenHOC
class DemurragePreComputation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // 提箱时间
      pickUpBoxDate: undefined,
      // 折扣
      discount: undefined,
      data: undefined,
//      应收
      discountDemurrage: undefined,
      currentDemurrage: undefined,
      oldCurrentDemurrage: undefined,
      applyType: undefined,
    }
  }

  componentDidMount() {
    let data = this.props.navigation.getParam("data") || {};
    let pickUpBoxDate = +new Date();
    console.log('调用接口前', data, pickUpBoxDate);
    this.setState({
      pickUpBoxDate: pickUpBoxDate,
      discount: data.operatorAuditedDiscount,
      data: data,
      discountDemurrage: data ? data.discountDemurrage : '',
      currentDemurrage: data ? data.currentDemurrage : '',
      oldCurrentDemurrage: data ? (data.currentDemurrage ? data.currentDemurrage : '--') : '--',
      applyType: data ? data.applyType : ''
    }, () => {
      console.log('调用接口', pickUpBoxDate, data.operatorAuditedDiscount);
      this.preComputation(pickUpBoxDate, data.operatorAuditedDiscount);
    });

  }

  preComputation = (pickUpBoxDate, discount) => {
    this.props.preComputationDemurragePromise({
      ids: [this.state.data.id],
      pickUpBoxDate: pickUpBoxDate,
      approveDiscount: discount,
    }).then((data) => {
      console.log('返回到页面上的信息', data);
      this.setState({
        discountDemurrage: data.discountDemurrage,
        currentDemurrage: data.currentDemurrage,
      });
    }).catch((data) => {
      console.log('失败打印', data);
    });
  }


  componentWillUnmount() {

  }


  render() {
    return (
      <View style={{ ...styles.container }}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ padding: 10, flex: 1 }}>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>提箱时间:</Label>
                <DatePullSelector
                  value={this.state.pickUpBoxDate}
                  onChangeValue={(value) => {
                    this.setState({ pickUpBoxDate: value },
                      () => {
                        this.preComputation(value, this.state.discount);
                      }
                    );
                  }}
                  type={DATE_WHEEL_TYPE.DATE}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label={'请输入审核折扣:'}
                value={this.state.discount}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ discount: text });
                }}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                }}
              />
            </InputGroup>
            <View style={styles.tips}>
              <Text style={{ fontSize: 12, color: 'red' }}>提示:折扣只能入录-200 ~ 100的整数，小于0是表示折扣天数，大于0时表示折扣百分比。例:80
                (标识折扣80%) </Text>
            </View>
            <View style={styles.group}>
              <View style={styles.item}>
                <Text style={styles.label}>应收超期费:</Text>
                <Text style={styles.info}>
                  {this.state.applyType === '二次' ?
                    this.state.oldCurrentDemurrage :
                    (this.state.currentDemurrage ? this.state.currentDemurrage : '--')}
                </Text>
              </View>
              <View style={styles.item}>
                <Text style={styles.label}>折后超期费:</Text>
                <Text style={styles.info}>
                  {this.state.discountDemurrage ? this.state.discountDemurrage : '--'}
                </Text>
              </View>
            </View>
            <Button
              block
              onPress={() => {
                Keyboard.dismiss();
                // 计算
                this.preComputation(this.state.pickUpBoxDate, this.state.discount);
              }}
              style={{
                height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#3D95D5',
              }}
            >
              <Text style={{ color: '#ffffff' }}>计算</Text>
            </Button>
          </View>
        </SafeAreaView>
      </View>

    );
  }
}

DemurragePreComputation.navigationOptions = ({ navigation }) => ({
  title: '超期费计算',
});


DemurragePreComputation.propTypes = {
  navigation: PropTypes.object.isRequired,
  preComputationDemurragePromise: PropTypes.func.isRequired,
};
DemurragePreComputation.defaultProps = {};

const mapStateToProps = createStructuredSelector({});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      preComputationDemurragePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DemurragePreComputation);
