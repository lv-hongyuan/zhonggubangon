import { createSelector } from 'reselect/es';

const selectDemurragePreComputationDomain = () => state => state.demurragePreComputation;

const makeData = () => createSelector(selectDemurragePreComputationDomain(), (subState) => {
  console.debug(subState);
  return subState.data;
});

const makeRefreshState = () => createSelector(selectDemurragePreComputationDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectDemurragePreComputationDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeData,
  makeIsLoading,
  makeRefreshState,
};
