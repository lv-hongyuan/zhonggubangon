import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { preComputationDemurrageRoutine } from './actions';

// 预算超期费折后价
function* preComputationDemurrage(action) {
  console.log('预算超期费折后价', action);
  const { pickUpBoxDate, approveDiscount, ids, } = action.payload;
  try {
    yield put(preComputationDemurrageRoutine.request());
    const response = yield call(request, ApiFactory.preComputationDemurrage(pickUpBoxDate, approveDiscount, ids));
//
    console.log('输出preComputationDemurrage response', response);
    yield put(preComputationDemurrageRoutine.success(response));
//      list: loadMore ? [] : (demurrageDTOS || []),
//      loadMore,
//      pageNum,
//      pageSize,
//    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(preComputationDemurrageRoutine.failure({
      error: e.message,
    }));
  } finally {
    yield put(preComputationDemurrageRoutine.fulfill());
  }
}

export function* preComputationDemurrageSage() {
  yield takeLatest(preComputationDemurrageRoutine.TRIGGER, preComputationDemurrage);
}

// All sagas to be loaded
export default [preComputationDemurrageSage];
