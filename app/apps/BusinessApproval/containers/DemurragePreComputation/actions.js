import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  PRE_COMPUTATION_DEMURRAGE,
} from './constants';

export const preComputationDemurrageRoutine = createRoutine(PRE_COMPUTATION_DEMURRAGE);
export const preComputationDemurragePromise = promisifyRoutine(preComputationDemurrageRoutine);
