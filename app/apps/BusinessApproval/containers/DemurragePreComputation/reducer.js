import fixIdList from '../../../../utils/fixIdList';
import { preComputationDemurrageRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  data: undefined,
  isLoading: false,
  error: undefined,
};

export default function(state = defaultState, action) {
  switch (action.type) {
    // 获取动态
    case preComputationDemurrageRoutine.REQUEST:
      console.log('发送请求preComputationDemurrageRoutine.REQUEST:', action);
      return {
        ...state,
        isLoading: true,
      };
    case preComputationDemurrageRoutine.SUCCESS: {
      const { response } = action.payload;
//      const fixList = fixIdList(list);
//      const newList = loadMore ? state.list.concat(fixList) : fixList;
//      const hasMore = fixList.length === pageSize;
//      console.log('hasMore', fixList.length, pageSize, hasMore);
//      const isEmpty = newList.length === 0;
//      if (isEmpty) {
//        refreshState = RefreshState.EmptyData;
//      } else if (!hasMore) {
//        refreshState = RefreshState.NoMoreData;
//      }
      return {
        ...state,
        data: response,
      };
    }
    case preComputationDemurrageRoutine.FAILURE: {
      const { loadMore, error } = action.payload;
      console.log('FAILURE', action.payload);


      return {
        ...state,
        data: error,
      };
    }
    case preComputationDemurrageRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
