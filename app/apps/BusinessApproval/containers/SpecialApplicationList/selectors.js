import { createSelector } from 'reselect/es';

const selectSpecialApplicationListDomain = () => state => state.specialApplicationList;

const makeList = () => createSelector(selectSpecialApplicationListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectSpecialApplicationListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectSpecialApplicationListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeList,
  makeIsLoading,
  makeRefreshState,
};
