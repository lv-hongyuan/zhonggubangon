import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_SPECIAL_APPLICATION_LIST,
} from './constants';

export const getToSpecialApplicationListRoutine = createRoutine(GET_SPECIAL_APPLICATION_LIST);
export const getSpecialApplicationListPromise = promisifyRoutine(getToSpecialApplicationListRoutine);
