import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, TouchableOpacity,
} from 'react-native';
import {
  Text, View,
} from 'native-base';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  label: {
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
  badge: {},
});

class SpecialApplicationItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const {
      id, tjSfg, tjMdg, tjXx, tjXl, tjLowJe, tjJe, tjBz, tjType, tjYstk, zt, tjDcr,tjSjhyf
    } = item;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onSelect(item);
        }}
      >
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{tjType}</Text>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>特价申请ID:</Text>
              <Text style={styles.info}>{id}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>状态:</Text>
              <Text style={styles.info}>{zt}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>始发港:</Text>
              <Text style={styles.info}>{tjSfg}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>目的港:</Text>
              <Text style={styles.info}>{tjMdg}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>箱型:</Text>
              <Text style={styles.info}>{tjXx}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>箱量:</Text>
              <Text style={styles.info}>{tjXl}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>订舱人:</Text>
              <Text style={styles.info}>{tjDcr}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>运输条款:</Text>
              <Text style={styles.info}>{tjYstk}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>限价:</Text>
              <Text style={styles.info}>{tjLowJe}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>现价:</Text>
              <Text style={styles.info}>{tjJe}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>实际海运费:</Text>
              <Text style={styles.info}>{tjSjhyf}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>申请原因:</Text>
              <Text style={styles.info}>{tjBz}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

SpecialApplicationItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default SpecialApplicationItem;
