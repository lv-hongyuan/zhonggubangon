import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getToSpecialApplicationListRoutine } from './actions';

function* getSpecialApplicationList(action) {
  console.log(action);
  const { pageSize = 10, pageNum, loadMore, tjSfgPq, tjSfg, tjType } = action.payload;
  try {
    yield put(getToSpecialApplicationListRoutine.request({ loadMore }));
    const response = yield call(request, ApiFactory.getSpecialApplicationList(pageSize, pageNum, tjSfgPq, tjSfg, tjType));
    console.log(response);
    const { specialPriceApplicationDTOs } = response;
    yield put(getToSpecialApplicationListRoutine.success({
      list: specialPriceApplicationDTOs || [],
      loadMore,
      pageNum,
      pageSize,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getToSpecialApplicationListRoutine.failure({
      loadMore,
      error: e,
    }));
  } finally {
    yield put(getToSpecialApplicationListRoutine.fulfill());
  }
}

export function* getSpecialApplicationListSaga() {
  yield takeLatest(getToSpecialApplicationListRoutine.TRIGGER, getSpecialApplicationList);
}

// All sagas to be loaded
export default [getSpecialApplicationListSaga];
