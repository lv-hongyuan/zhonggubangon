import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { ROUTE_SPECIAL_APPLICATION_DETAIL, ROUTE_SPECIAL_APPLICATION_SEARCH } from '../../RouteConstant';
import { getSpecialApplicationListPromise } from './actions';
import { makeList, makeIsLoading, makeRefreshState } from './selectors';
import SpecialApplicationItem from './components/SpecialApplicationItem';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from "../../../../common/commonStyles";
import _ from "lodash";
import InputItem from "../../../../components/InputItem";
import { TodoType } from '../../common/Constant';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

@screenHOC
class SpecialApplicationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum: 0,
      tjSfgPq: '',  // 始发港片区
      tjSfg: '',  // 始发港
      inputTjSfgPq: '',
      inputTjSfg: '',
      displayMode: true,
    }
  }

  componentDidMount() {
    this.onHeaderRefresh();
  }

  componentWillUnmount() {
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    const { tjType: title } = item;
    this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_DETAIL, {
      data: item,
      title,
      onBack: this.onHeaderRefresh,
    });
  };

  loadList(loadMore: boolean) {
    const dbCode = this.props.navigation.getParam('dbCode');
    // let tjType = dbCode == TodoType.OrderSpecialPrice ? 0 : 1;
    let tjType = 1
    if(dbCode == TodoType.OrderSpecialPrice){
      tjType = 0
    }else if(dbCode == TodoType.Overprice){
      tjType = 4
    }
    console.log('tjType:',tjType);
    this.props.getSpecialApplicationListPromise({
      loadMore,
      pageNum: loadMore ? this.state.pageNum + 1 : 1,
      tjSfgPq: this.state.tjSfgPq,
      tjSfg: this.state.tjSfg,
      tjType,
    })
      .then(({ pageNum }) => {
        this.setState({ pageNum });
      })
      .catch(() => {
      });
  }

  renderItem = ({ item }) => (
    <SpecialApplicationItem
      item={item}
      onSelect={this.onSelect}
    />
  );

  setInputValue(callBack) {
    this.setState({
      inputTjSfgPq: this.state.tjSfgPq,
      inputTjSfg: this.state.tjSfg,
    }, callBack)
  }

  setSearchValue(callBack) {
    this.setState({
      tjSfgPq: this.state.inputTjSfgPq,
      tjSfg: this.state.inputTjSfg,
    }, callBack)
  }

  //
  changeDisplayMode = () => {
    const dbName = this.props.navigation.getParam('dbName');
    let { inputTjSfgPq, inputTjSfg } = this.state;
    this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_SEARCH, {
      inputTjSfgPq: this.state.inputTjSfgPq,
      inputTjSfg: this.state.inputTjSfg,
      dbName,
      onBack: ({ inputTjSfgPq, inputTjSfg }) => {
        console.log(inputTjSfgPq, inputTjSfg)
        this.setState({
          inputTjSfgPq: inputTjSfgPq,
          inputTjSfg: inputTjSfg,
          tjSfgPq: inputTjSfgPq,
          tjSfg: inputTjSfg,
        }, () => {
          this.onHeaderRefresh();
        });
      },
    });
  };

  renderFilter = () => {
    console.log("displayMode", this.state.displayMode);
    return (
      <View style={{ ...styles.container }}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ padding: 10, flex: 1 }}>
            <TouchableOpacity
              activeOpacity={1}
              style={styles.searchInput}
              onPress={() => {
                this.setInputValue(this.changeDisplayMode());
              }}
            >
              {!_.isEmpty(this.state.tjSfgPq) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>片区:</Label>
                  <Text style={commonStyles.text}>{this.state.tjSfgPq}</Text>
                </View>
              )}
              {!_.isEmpty(this.state.tjSfg) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>始发港:</Label>
                  <Text style={commonStyles.text}>{this.state.tjSfg}</Text>
                </View>
              )}
              {/* 都为空时*/}
              {_.isEmpty(this.state.tjSfgPq) && _.isEmpty(this.state.tjSfg) && (
                <Item
                  style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                  onPress={() => {
                    this.setInputValue(this.changeDisplayMode());
                  }}
                >
                  <Text style={commonStyles.text}>点击输入要搜索的片区/始发港</Text>
                </Item>
              )}
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>);
  };

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ flex: 1 }}>
          {this.renderFilter()}
          <RefreshListView
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          />
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute', width: '100%', height: '100%', backgroundColor: '#ffffff',
            }}
            />)}
        </SafeAreaView>
      </Container>
    );
  }
}

// SpecialApplicationList.navigationOptions = () => ({
//   title: '特价申请',
// });
SpecialApplicationList.navigationOptions = ({navigation}) => ({
  title: navigation.getParam('dbName', '特价申请'),
});

SpecialApplicationList.propTypes = {
  navigation: PropTypes.object.isRequired,
  getSpecialApplicationListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
SpecialApplicationList.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeList(),
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSpecialApplicationListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialApplicationList);
