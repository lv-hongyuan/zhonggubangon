import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getSpecialApplicationDetailRoutine,
  getSpecialApplicationChangeDetailRoutine,
  rejectApplicationRoutine,
  approveApplicationRoutine,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getSpecialApplicationDetail(action) {
  console.log(action);
  try {
    yield put(getSpecialApplicationDetailRoutine.request());
    const response = yield call(request, ApiFactory.getSpecialApplicationDetail(action.payload));
    console.log(response);
    // 返回数据
    yield put(getSpecialApplicationDetailRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(getSpecialApplicationDetailRoutine.failure(error));
  } finally {
    yield put(getSpecialApplicationDetailRoutine.fulfill());
  }
}

function* getSpecialApplicationChangeDetail(action) {
  console.log(action);
  try {
    yield put(getSpecialApplicationChangeDetailRoutine.request());
    const response = yield call(request, ApiFactory.getSpecialApplicationChangeDetail(action.payload));
    console.log(response);
    // 返回数据
    yield put(getSpecialApplicationChangeDetailRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(getSpecialApplicationChangeDetailRoutine.failure(error));
  } finally {
    yield put(getSpecialApplicationChangeDetailRoutine.fulfill());
  }
}

function* rejectApplication(action) {
  console.log(action);
  try {
    yield put(rejectApplicationRoutine.request());
    const response = yield call(request, ApiFactory.rejectApplication(action.payload));
    console.log(response);
    yield put(rejectApplicationRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(rejectApplicationRoutine.failure(error));
  } finally {
    yield put(rejectApplicationRoutine.fulfill());
  }
}

function* approveApplication(action) {
  console.log(action);
  try {
    yield put(approveApplicationRoutine.request());
    const response = yield call(request, ApiFactory.approveApplication(action.payload));
    console.log(response);
    yield put(approveApplicationRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(approveApplicationRoutine.failure(error));
  } finally {
    yield put(approveApplicationRoutine.fulfill());
  }
}

export function* getSpecialApplicationDetailSaga() {
  yield takeLatest(getSpecialApplicationDetailRoutine.TRIGGER, getSpecialApplicationDetail);
}

export function* getSpecialApplicationChangeDetailSaga() {
  yield takeLatest(getSpecialApplicationChangeDetailRoutine.TRIGGER, getSpecialApplicationChangeDetail);
}

export function* rejectApplicationSaga() {
  yield takeLatest(rejectApplicationRoutine.TRIGGER, rejectApplication);
}

export function* approveApplicationSaga() {
  yield takeLatest(approveApplicationRoutine.TRIGGER, approveApplication);
}

// All sagas to be loaded
export default [
  getSpecialApplicationDetailSaga,
  getSpecialApplicationChangeDetailSaga,
  rejectApplicationSaga,
  approveApplicationSaga,
];
