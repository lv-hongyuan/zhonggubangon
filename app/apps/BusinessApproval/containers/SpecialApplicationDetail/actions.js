import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  APPROVE_APPLICATION,
  GET_SPECIAL_APPLICATION_DETAIL,
  GET_SPECIAL_APPLICATION_CHANGE_DETAIL,
  REJECT_APPLICATION,
} from './constants';

export const getSpecialApplicationDetailRoutine = createRoutine(GET_SPECIAL_APPLICATION_DETAIL);
export const getSpecialApplicationDetailPromise = promisifyRoutine(getSpecialApplicationDetailRoutine);

export const getSpecialApplicationChangeDetailRoutine = createRoutine(GET_SPECIAL_APPLICATION_CHANGE_DETAIL);
export const getSpecialApplicationChangeDetailPromise = promisifyRoutine(getSpecialApplicationChangeDetailRoutine);

export const rejectApplicationRoutine = createRoutine(REJECT_APPLICATION);
export const rejectApplicationPromise = promisifyRoutine(rejectApplicationRoutine);

export const approveApplicationRoutine = createRoutine(APPROVE_APPLICATION);
export const approveApplicationPromise = promisifyRoutine(approveApplicationRoutine);
