import shortid from 'shortid';
import { Text, View } from 'native-base';
import { StyleSheet } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 50,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  label: {
    width: 80,
    // textAlign: 'right',
    fontSize: 13,
    lineHeight: 16,
  },
  colon: {
    marginRight: 4,
    fontSize: 13,
    lineHeight: 16,
  },
  info: {
    flex: 1,
    fontSize: 13,
    lineHeight: 16,
    paddingLeft: 2,
  },
  diffValue: {
    borderWidth: myTheme.borderWidth,
    borderLeftWidth: 0,
    marginTop: -myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    backgroundColor: '#FF9E7A',
    height: '100%',
  },
  diffOldValue: {
    borderWidth: myTheme.borderWidth,
    marginTop: -myTheme.borderWidth,
    marginLeft: -myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    backgroundColor: '#FEF9CF',
    height: '100%',
  },
});

class ChangingOrderView extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        {this.props.changeFields.map(({ title, data, lowData }) => {
          const showDiff = data !== lowData;
          return (
            <View style={styles.group} key={shortid.generate()}>
              <View style={styles.item}>
                <Text style={styles.label}>{title}:</Text>
                {showDiff && (
                  <Text style={[styles.info, styles.diffOldValue]}>{lowData || ' '}</Text>
                )}
                <Text style={[styles.info, showDiff ? styles.diffValue : undefined]}>{data || ' '}</Text>
              </View>
            </View>
          );
        })}
      </View>
    );
  }
}

ChangingOrderView.propTypes = {
  // data: PropTypes.object,
  changeFields: PropTypes.array,
};

ChangingOrderView.defaultProps = {
  // data: {},
  changeFields: [],
};

export default ChangingOrderView;
