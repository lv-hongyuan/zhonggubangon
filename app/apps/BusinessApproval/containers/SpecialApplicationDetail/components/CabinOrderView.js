import _ from 'lodash';
import { Text, View } from 'native-base';
import { StyleSheet } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 50,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  label: {
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
});

class CabinOrderView extends React.PureComponent {
  render() {
    const {
      id, tjSfg, tjMdg, tjXx, tjXl, tjLowJe, tjJe, tjYstk, tjBz,
      tjZzg1, tjZzg2, tjZzg3, tjZzg4, tjZzg5, tjZzg6,
      tjCzy, tjBsc, tjHm, tjZhg, tjXhg, tjCm, tjHc, tjDcr,
      tjFhr, tjShr, tjFhrMd, tjShrMD, tjEBookId, zt, fio,tjSjhyf
    } = this.props.data;
    return (
      <View style={styles.container}>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>特价申请ID:</Text>
            <Text style={styles.info}>{id}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>状态:</Text>
            <Text style={styles.info}>{zt}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>始发港:</Text>
            <Text style={styles.info}>{tjSfg}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>目的港:</Text>
            <Text style={styles.info}>{tjMdg}</Text>
          </View>
        </View>
        <View style={styles.group}>
          {!_.isEmpty(tjZhg) && (
            <View style={styles.item}>
              <Text style={styles.label}>装货港:</Text>
              <Text style={styles.info}>{tjZhg}</Text>
            </View>
          )}
          {!_.isEmpty(tjXhg) && (
            <View style={styles.item}>
              <Text style={styles.label}>卸货港:</Text>
              <Text style={styles.info}>{tjXhg}</Text>
            </View>
          )}
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>申请人:</Text>
            <Text style={styles.info}>{tjCzy}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>申请办:</Text>
            <Text style={styles.info}>{tjBsc}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>运输条款:</Text>
            <Text style={styles.info}>{tjYstk}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>货名:</Text>
            <Text style={styles.info}>{tjHm}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>箱型:</Text>
            <Text style={styles.info}>{tjXx}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>箱量:</Text>
            <Text style={styles.info}>{tjXl}</Text>
          </View>
        </View>
        <View style={styles.group}>
          {!_.isEmpty(tjCm) && (
            <View style={styles.item}>
              <Text style={styles.label}>船名:</Text>
              <Text style={styles.info}>{tjCm}</Text>
            </View>
          )}
          {!_.isEmpty(tjHc) && (
            <View style={styles.item}>
              <Text style={styles.label}>航次:</Text>
              <Text style={styles.info}>{tjHc}</Text>
            </View>
          )}
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>限价:</Text>
            <Text style={styles.info}>{tjLowJe}</Text>
          </View>
          <View style={styles.item}>
            <Text style={styles.label}>现价:</Text>
            <Text style={styles.info}>{tjJe}</Text>
          </View>
        </View>
        <View style={styles.group}>
          {!_.isEmpty(tjZzg1) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港1:</Text>
              <Text style={styles.info}>{tjZzg1}</Text>
            </View>
          )}
          {!_.isEmpty(tjZzg2) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港2:</Text>
              <Text style={styles.info}>{tjZzg2}</Text>
            </View>
          )}
        </View>
        <View style={styles.group}>
          {!_.isEmpty(tjZzg3) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港3:</Text>
              <Text style={styles.info}>{tjZzg3}</Text>
            </View>
          )}
          {!_.isEmpty(tjZzg4) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港4:</Text>
              <Text style={styles.info}>{tjZzg4}</Text>
            </View>
          )}
        </View>
        <View style={styles.group}>
          {!_.isEmpty(tjZzg5) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港5:</Text>
              <Text style={styles.info}>{tjZzg5}</Text>
            </View>
          )}
          {!_.isEmpty(tjZzg6) && (
            <View style={styles.item}>
              <Text style={styles.label}>中装港6:</Text>
              <Text style={styles.info}>{tjZzg6}</Text>
            </View>
          )}
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>订单业务号:</Text>
            <Text style={styles.info}>{tjEBookId}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>订舱人:</Text>
            <Text style={styles.info}>{tjDcr}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>发货人:</Text>
            <Text style={styles.info}>{tjFhr}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>收货人:</Text>
            <Text style={styles.info}>{tjShr}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>发货人门点:</Text>
            <Text style={styles.info}>{tjFhrMd}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>收货人门点:</Text>
            <Text style={styles.info}>{tjShrMD}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>申请原因:</Text>
            <Text style={styles.info}>{tjBz}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>实际海运费:</Text>
            <Text style={styles.info}>{tjSjhyf}</Text>
          </View>
        </View>
        <View style={styles.group}>
          <View style={styles.item}>
            <Text style={styles.label}>预算FIO:</Text>
            <Text style={styles.info}>{fio}</Text>
          </View>
        </View>
      </View>
    );
  }
}

CabinOrderView.propTypes = {
  data: PropTypes.object,
};

CabinOrderView.defaultProps = {
  data: {},
};

export default CabinOrderView;
