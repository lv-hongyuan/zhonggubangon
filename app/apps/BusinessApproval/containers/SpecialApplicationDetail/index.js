import { Button, Container, Content, Text, View } from 'native-base';
import { PixelRatio, SafeAreaView, StyleSheet } from 'react-native';
import { Overlay } from 'teaset';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import AlertView from '../../../../components/Alert';
import { TextInput } from '../../../../components/InputItem/TextInput';
import { SpecialPriceType } from '../../common/Constant';
import {
  getSpecialApplicationDetailPromise,
  getSpecialApplicationDetailRoutine,
  getSpecialApplicationChangeDetailPromise,
  rejectApplicationPromise,
  approveApplicationPromise,
} from './actions';
import CabinOrderView from './components/CabinOrderView';
import ChangingOrderView from './components/ChangingOrderView';
import {
  makeIsLoading, makeChangeFields,
  makeSelectSpecialApplicationDetail,
} from './selectors';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 50,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  label: {
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
  bottomContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#ffffff',
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  approveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  rejectButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
});

@screenHOC
class SpecialApplicationDetail extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      rejectReason: undefined,
    };
  }

  componentDidMount() {
    const data = this.props.navigation.getParam('data');
    if (!data) {
      // this.loadDetail(data.id);
    } else {
      this.props.dispatch(getSpecialApplicationDetailRoutine.success(data));
      const { tjType, id } = data;
      if (tjType === SpecialPriceType.InternalOrderChange || tjType === SpecialPriceType.ExternalOrderChange) {
        this.loadDiff(id);
      }
    }
  }

  loadDetail = (id) => {
    this.props.getSpecialApplicationDetailPromise(id)
      .then((data) => {
        const { tjType: title } = data;
        this.props.navigation.setParams({ title });
      })
      .catch((error) => {
        AlertView.show({
          message: error.message,
          confirmAction: () => {
            this.props.navigation.goBack();
          },
        });
      });
  };

  loadDiff = (id) => {
    this.props.getSpecialApplicationChangeDetailPromise(id)
      .then(() => {
      })
      .catch((error) => {
        AlertView.show({
          message: error.message,
          confirmAction: () => {
            this.props.navigation.goBack();
          },
        });
      });
  };

  rejectApplication = () => {
    let backFlag = true;
    AlertView.show({
      message: '驳回申请',
      component: this.renderInput,
      showCancel: true,
      confirmAction: () => {
        this.props.rejectApplicationPromise({
          id: this.props.data.id,
          rejectReason: this.state.rejectReason,
        })
          .then(({ _backmes }) => {
            const alertKey = AlertView.show({
              message: _backmes || '驳回成功',
              confirmAction: () => {
                if (backFlag) {
                  backFlag = true;
                  const onBack = this.props.navigation.getParam('onBack');
                  if (onBack) onBack();
                  this.props.navigation.goBack();
                }
              },
            });
            setTimeout(() => {
              Overlay.hide(alertKey);
              if (backFlag) {
                backFlag = true;
                const onBack = this.props.navigation.getParam('onBack');
                if (onBack) onBack();
                this.props.navigation.goBack();
              }
            }, 1000);
          })
          .catch(() => {
          });
      },
    });
  };

  approveApplication = () => {
    let backFlag = true;
    AlertView.show({
      message: '确定要通过审核吗？',
      showCancel: true,
      confirmAction: () => {
        this.props.approveApplicationPromise({
          id: this.props.data.id,
        })
          .then(({ _backmes }) => {
            const alertKey = AlertView.show({
              message: _backmes || '审核成功',
              confirmAction: () => {
                if (backFlag) {
                  backFlag = true;
                  const onBack = this.props.navigation.getParam('onBack');
                  if (onBack) onBack();
                  this.props.navigation.goBack();
                }
              },
            });
            setTimeout(() => {
              Overlay.hide(alertKey);
              if (backFlag) {
                backFlag = true;
                const onBack = this.props.navigation.getParam('onBack');
                if (onBack) onBack();
                this.props.navigation.goBack();
              }
            }, 1000);
          })
          .catch(() => {
          });
      },
    });
  };

  renderInput = () => (
    <View style={{
      width: '100%',
      paddingTop: 10,
      paddingLeft: 10,
      paddingRight: 10,
    }}
    >
      <TextInput
        autoFocus
        style={{
          padding: 4,
          width: '100%',
          height: 80,
          borderColor: '#E0E0E0',
          borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
          textAlignVertical: 'top',
          borderRadius: 4,
        }}
        placeholder="请填写驳回原因"
        multiline
        value={this.state.rejectReason}
        onChangeText={(text) => {
          this.setState({ rejectReason: text });
        }}
      />
    </View>
  );

  render() {
    const { tjType } = this.props.data;
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          {tjType === SpecialPriceType.InternalOrderChange || tjType === SpecialPriceType.ExternalOrderChange ? (
            <ChangingOrderView data={this.props.data} changeFields={this.props.changeFields}/>
          ) : (
            <CabinOrderView data={this.props.data}/>
          )}
        </Content>
        <SafeAreaView style={styles.bottomContainer}>
          <View style={styles.buttonContainer}>
            <Button onPress={this.rejectApplication} style={styles.rejectButton}>
              <Text>驳回</Text>
            </Button>
            <Button onPress={this.approveApplication} style={styles.approveButton}>
              <Text>审核</Text>
            </Button>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

SpecialApplicationDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '特价申请'),
});

SpecialApplicationDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  getSpecialApplicationDetailPromise: PropTypes.func.isRequired,
  getSpecialApplicationChangeDetailPromise: PropTypes.func.isRequired,
  rejectApplicationPromise: PropTypes.func.isRequired,
  approveApplicationPromise: PropTypes.func.isRequired,
  data: PropTypes.object,
  changeFields: PropTypes.array,
};
SpecialApplicationDetail.defaultProps = {
  data: {},
  changeFields: [],
};

const mapStateToProps = createStructuredSelector({
  isLoading: makeIsLoading(),
  data: makeSelectSpecialApplicationDetail(),
  changeFields: makeChangeFields(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSpecialApplicationDetailPromise,
      getSpecialApplicationChangeDetailPromise,
      rejectApplicationPromise,
      approveApplicationPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SpecialApplicationDetail);
