import {
  rejectApplicationRoutine,
  approveApplicationRoutine,
  getSpecialApplicationDetailRoutine,
  getSpecialApplicationChangeDetailRoutine,
} from './actions';

const defaultState = {
  data: undefined,
  changeDetail: {
    fields: [],
  },
  isLoading: false,
  loadingError: undefined,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case getSpecialApplicationDetailRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case getSpecialApplicationDetailRoutine.SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    case getSpecialApplicationDetailRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getSpecialApplicationDetailRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };
    case getSpecialApplicationChangeDetailRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case getSpecialApplicationChangeDetailRoutine.SUCCESS:
      return {
        ...state,
        changeDetail: action.payload,
      };
    case getSpecialApplicationChangeDetailRoutine.FAILURE:
      return {
        ...state,
        changeDetail: defaultState.changeDetail,
        error: action.payload,
      };
    case getSpecialApplicationChangeDetailRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case rejectApplicationRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case rejectApplicationRoutine.SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    case rejectApplicationRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case rejectApplicationRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case approveApplicationRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case approveApplicationRoutine.SUCCESS:
      return {
        ...state,
        data: action.payload,
      };
    case approveApplicationRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case approveApplicationRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
}
