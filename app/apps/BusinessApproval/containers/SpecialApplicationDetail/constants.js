/*
 *
 * SpecialApplicationDetail constants
 *
 */
export const GET_SPECIAL_APPLICATION_DETAIL = 'onLand/BusinessApproval/GET_SPECIAL_APPLICATION_DETAIL';
export const GET_SPECIAL_APPLICATION_CHANGE_DETAIL = 'onLand/BusinessApproval/GET_SPECIAL_APPLICATION_CHANGE_DETAIL';

export const REJECT_APPLICATION = 'onLand/BusinessApproval/REJECT_APPLICATION';
export const APPROVE_APPLICATION = 'onLand/BusinessApproval/APPROVE_APPLICATION';
