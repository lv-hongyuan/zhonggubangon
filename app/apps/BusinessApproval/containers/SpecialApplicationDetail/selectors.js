import _ from 'lodash';
import { createSelector } from 'reselect';

const selectSpecialApplicationDetailDomain = () => state => state.specialApplicationDetail;

const makeSelectSpecialApplicationDetail = () => createSelector(
  selectSpecialApplicationDetailDomain(),
  (subState) => {
    console.log(subState);
    return subState.data;
  },
);

const makeChangeFields = () => createSelector(
  selectSpecialApplicationDetailDomain(),
  (subState) => {
    console.log(subState);
    const { fields } = (subState.changeDetail || {});
    return _.isArray(fields) ? fields : [];
  },
);

const makeIsLoading = () => createSelector(
  selectSpecialApplicationDetailDomain(),
  subState => subState.isLoading,
);

export {
  makeSelectSpecialApplicationDetail,
  makeChangeFields,
  makeIsLoading,
};
