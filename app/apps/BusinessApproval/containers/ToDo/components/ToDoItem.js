import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, TouchableOpacity,
} from 'react-native';
import {
  Text, View,
} from 'native-base';
import Svg from '../../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  badgeContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 20,
    borderRadius: 10,
    minWidth: 20,
    backgroundColor: 'red',
    paddingLeft: 5,
    paddingRight: 5,
  },
  badge: {
    color: '#FFFFFF',
    height: 20,
    lineHeight: 20,
  },
});

class ToDoItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const { dbName, dbCount } = item;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onSelect(item);
        }}
      >
        <View style={styles.container}>
          <Text style={styles.title}>{dbName}</Text>
          {(dbCount && dbCount > 0) && (
            <View style={styles.badgeContainer}>
              <Text style={styles.badge}>{dbCount}</Text>
            </View>
          )}
          <Svg icon="right_arrow" size={30} />
        </View>
      </TouchableOpacity>
    );
  }
}

ToDoItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default ToDoItem;
