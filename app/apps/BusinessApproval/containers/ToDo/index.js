import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { Container, View } from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { TodoType } from '../../common/Constant';
import { getToDoTypeListPromise } from './actions';
import { makeList, makeIsLoading, makeRefreshState } from './selectors';
import ToDoItem from './components/ToDoItem';
import {
  ROUTE_DEMURRAGE_LIST,
  ROUTE_SPECIAL_APPLICATION_LIST,
} from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';

@screenHOC
class ToDo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onSelect = ({ dbCode, dbName }) => {
    switch (dbCode) {
      case TodoType.SpecialPrice:
        this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_LIST);
        break;
      case TodoType.OverdueFee:
        this.props.navigation.navigate(ROUTE_DEMURRAGE_LIST);
        break;
      case TodoType.OrderSpecialPrice:
        this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_LIST, { dbCode: TodoType.OrderSpecialPrice, dbName: dbName });
        break;
      case TodoType.ChangeSpecialPrice:
        this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_LIST, { dbCode: TodoType.ChangeSpecialPrice, dbName: dbName });
        break;
      case TodoType.Overprice:
        this.props.navigation.navigate(ROUTE_SPECIAL_APPLICATION_LIST, { dbCode: TodoType.Overprice, dbName: dbName });
      default:
        break;
    }
  };

  didFocus() {
    this.onHeaderRefresh();
  }

  loadList() {
    this.props.getToDoTypeListPromise()
      .then(() => {
      })
      .catch(() => {
      });
  }

  renderItem = ({ item }) => {
    console.log(item);
    if (item.dbCount == 0) {
      return null
    }
    return (
      <ToDoItem
        item={item}
        onSelect={this.onSelect}
      />
    )
  };

  render() {
    console.log('待办数据:', this.props.list)
    if (this.props.list.length === 0) {
      console.log('待办无数据')
      return null;
    }
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ flex: 1 }}>
          <RefreshListView
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          />
        </SafeAreaView>
      </Container>
    );
  }
}

ToDo.navigationOptions = () => ({
  title: '待办',
});

ToDo.propTypes = {
  navigation: PropTypes.object.isRequired,
  getToDoTypeListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
ToDo.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeList(),
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getToDoTypeListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);
