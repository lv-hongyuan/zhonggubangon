import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_TO_DO_LIST,
} from './constants';

export const getToDoTypeListRoutine = createRoutine(GET_TO_DO_LIST);
export const getToDoTypeListPromise = promisifyRoutine(getToDoTypeListRoutine);
