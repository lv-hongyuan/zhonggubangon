import { createSelector } from 'reselect/es';

const selectToDoDomain = () => state => state.todo;

const makeList = () => createSelector(selectToDoDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectToDoDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectToDoDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeList,
  makeRefreshState,
  makeIsLoading,
};
