import fixIdList from '../../../../utils/fixIdList';
import { getToDoTypeListRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  list: [],
  isLoading: false,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态
    case getToDoTypeListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        refreshState: RefreshState.HeaderRefreshing,
      };
    case getToDoTypeListRoutine.SUCCESS: {
      const fixList = fixIdList(action.payload);
      const isEmpty = fixList.length === 0;
      return {
        ...state,
        list: fixList,
        error: undefined,
        refreshState: isEmpty ? RefreshState.EmptyData : RefreshState.NoMoreData,
      };
    }
    case getToDoTypeListRoutine.FAILURE:
      return {
        ...state,
        list: [],
        error: action.payload,
        refreshState: RefreshState.Failure,
      };
    case getToDoTypeListRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
