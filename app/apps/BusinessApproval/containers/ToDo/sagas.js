import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getToDoTypeListRoutine } from './actions';

function* getToDoTypeList(action) {
  console.log(action);
  try {
    yield put(getToDoTypeListRoutine.request());
    const response = yield call(request, ApiFactory.getToDoTypeList());
    console.log(response);
    yield put(getToDoTypeListRoutine.success(response.dbTotalDTOs || []));
  } catch (e) {
    console.log(e);
    yield put(errorMessage(e));
    yield put(getToDoTypeListRoutine.failure(e));
  } finally {
    yield put(getToDoTypeListRoutine.fulfill());
  }
}

export function* getToDoTypeListSaga() {
  yield takeLatest(getToDoTypeListRoutine.TRIGGER, getToDoTypeList);
}

// All sagas to be loaded
export default [getToDoTypeListSaga];
