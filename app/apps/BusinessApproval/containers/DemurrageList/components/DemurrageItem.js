import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, TouchableOpacity,
} from 'react-native';
import {
  Text, View,
} from 'native-base';
import Svg from "../../../../../components/Svg";

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    flexDirection: 'row',
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  label: {
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
  badge: {},
  boxCountViewStyle:{ 
    flexDirection: 'row',
    justifyContent:'center',
    alignContent:'center',
    backgroundColor:'red',
    marginTop: 30,
    width:30,
    height:30,
    borderRadius:15,
  },
  boxCountStyle:{
    color:'white',
    fontSize: 15,
    textAlign:'center',
    marginTop:5,
  },
});

class DemurrageItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const {
      id, waybillNum, vesselVoyage, shipper,
    } = item;
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onSelect(item);
        }}
      >

        <View style={styles.container}>
          <View style={{
            flex: 20,
          }}>
            <View style={styles.group}>
              <View style={styles.item}>
                <Text style={styles.label}>船名航次:</Text>
                <Text style={styles.info}>{vesselVoyage}</Text>
              </View>

            </View>
            <View style={styles.group}>
              <View style={styles.item}>
                <Text style={styles.label}>运单号:</Text>
                <Text style={styles.info}>{waybillNum}</Text>
              </View>
            </View>
            <View style={styles.group}>
              <View style={styles.item}>
                <Text style={styles.label}>订舱人:</Text>
                <Text style={styles.info}>{shipper}</Text>
              </View>
            </View>
          </View>
          <View style={styles.boxCountViewStyle}>
            <Text style={styles.boxCountStyle}>{item.boxCount}</Text>
          </View>
          <View style={{ flex: 1, height: '100%', paddingTop: 30, flexDirection: 'row', }}>
            <Svg icon="right_arrow" size={30}/>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

DemurrageItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default DemurrageItem;
