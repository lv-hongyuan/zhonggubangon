import fixIdList from '../../../../utils/fixIdList';
import { getDemurrageListRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  list: [],
  isLoading: false,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态
    case getDemurrageListRoutine.REQUEST:
      return {
        ...state,
        isLoading: true,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getDemurrageListRoutine.SUCCESS: {
      const { list, pageSize, loadMore } = action.payload;
      const fixList = fixIdList(list);
      // const newList = loadMore ? state.list.concat(fixList) : fixList;
      const newList = fixList;
      // const hasMore = fixList.length === pageSize;
      const hasMore = fixList.length % pageSize === 0;
      // const hasMore = true;

      console.log('stateList',state.list)
      console.log('fixList', fixIdList)
      console.log('newList', newList)
      console.log('hasMore', fixList.length, pageSize, hasMore);
      const isEmpty = newList.length === 0;
      let refreshState = RefreshState.Idle;
      if (isEmpty) {
        refreshState = RefreshState.EmptyData;
      } else if (!hasMore) {
        refreshState = RefreshState.NoMoreData;
      }
      return {
        ...state,
        list: newList,
        error: undefined,
        refreshState,
      };
    }
    case getDemurrageListRoutine.FAILURE: {
      const { loadMore, error } = action.payload;
      return {
        ...state,
        list: loadMore ? state.list : [],
        error,
        refreshState: RefreshState.Failure,
      };
    }
    case getDemurrageListRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
