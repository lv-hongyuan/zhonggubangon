import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  ROUTE_SPECIAL_APPLICATION_DETAIL,
  ROUTE_SPECIAL_APPLICATION_SEARCH,
  ROUTE_DEMURRAGE_BOX_DETAIL
} from '../../RouteConstant';
import { getDemurrageListPromise } from './actions';
import { makeList, makeIsLoading, makeRefreshState } from './selectors';
import DemurrageItem from './components/DemurrageItem';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from "../../../../common/commonStyles";
import _ from "lodash";
import InputItem from "../../../../components/InputItem";
import { NavigationActions } from "react-navigation";
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },

});

@screenHOC
class DemurrageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum: 0,
      displayMode: true,
      inputBoxNo: '',
      inputWaybillNum: '',
      inputShipper: '',
      boxNo: '',
      waybillNum: '',
      shipper: '',
    }

  }

  componentDidMount() {
    this.onHeaderRefresh();
    this.props.navigation.setParams({
      displayMode: this.state.displayMode,
      exitSearch: () => {
        this.changeDisplayMode(true);
      },

    });
  }

  componentWillUnmount() {
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  loadList(loadMore: boolean) {
    this.props.getDemurrageListPromise({
      loadMore,
      pageNum: loadMore ? this.state.pageNum + 1 : 1,
      boxNo: this.state.boxNo,
      waybillNum: this.state.waybillNum,
      shipper: this.state.shipper,
    }).then(({ pageNum }) => {
      this.setState({ pageNum });
    }).catch(() => {
    });
  }

  renderItem = ({ item }) => {

    return (<DemurrageItem
      item={item}
      onSelect={() => {
        this.props.navigation.navigate(ROUTE_DEMURRAGE_BOX_DETAIL, {
          // data: item.demurrageBoxDetailDTOS,
          // data:item.DemurrageDTOS,
          waybillNum: item.waybillNum,
          vesselVoyage: item.vesselVoyage,
          boxNo: item.boxNo,

          onBack: () => {
            console.log()
            this.onHeaderRefresh();
          },
        });
      }}
    />
    )
  }

  onSearchSubmit = () => {
    this.setState({
      displayMode: true,
      boxNo: this.state.inputBoxNo,
      shipper: this.state.inputShipper,
      waybillNum: this.state.inputWaybillNum,
    }, () => {
      this.onHeaderRefresh();
    });

  };

  changeDisplayMode = (displayMode = false) => {
    this.props.navigation.setParams({
      displayMode
    });
    if (!displayMode) {  // 当 == false   打开 搜索画面时
      this.setState({
        displayMode,
        inputBoxNo: this.state.boxNo,
        inputShipper: this.state.shipper,
        inputWaybillNum: this.state.waybillNum,
      });
    } else {
      this.setState({ displayMode, });
    }
  }

  // 搜索 头
  renderFilter = () => (
    <View style={{ ...styles.container }}>
      <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
        <View style={{ padding: 10, flex: 1 }}>
          <TouchableOpacity
            activeOpacity={1}
            style={styles.searchInput}
            onPress={() => {
              this.changeDisplayMode();
            }}
          >
            {!_.isEmpty(this.state.waybillNum) && (
              <View style={styles.searchItem}>
                <Label style={commonStyles.inputLabel}>运单号:</Label>
                <Text style={commonStyles.text}>{this.state.waybillNum}</Text>
              </View>
            )}
            {!_.isEmpty(this.state.boxNo) && (
              <View style={styles.searchItem}>
                <Label style={commonStyles.inputLabel}>箱号:</Label>
                <Text style={commonStyles.text}>{this.state.boxNo}</Text>
              </View>
            )}
            {!_.isEmpty(this.state.shipper) && (
              <View style={styles.searchItem}>
                <Label style={commonStyles.inputLabel}>订舱人:</Label>
                <Text style={commonStyles.text}>{this.state.shipper}</Text>
              </View>
            )}
            {/* 都为空时*/}
            {_.isEmpty(this.state.shipper) &&
              _.isEmpty(this.state.boxNo) &&
              _.isEmpty(this.state.waybillNum) && (
                <Item
                  style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                  onPress={() => {
                    this.changeDisplayMode();
                  }}
                >
                  <Text style={commonStyles.text}>点击输入要搜索的运单号/箱号/订舱人</Text>
                </Item>
              )}
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </View>
  );

  // 搜索画面
  searchView = () => (
    <View style={{ ...styles.container }}>
      <SafeAreaView style={{ width: '100%' }}>
        <View style={{ padding: 10, }}>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              autoFocus
              label={'请选择运单号：'}
              returnKeyType="search"
              value={this.state.inputWaybillNum}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                this.setState({ inputWaybillNum: text });
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label={'请输入箱号:'}
              returnKeyType="search"
              value={this.state.inputBoxNo}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                this.setState({ inputBoxNo: text });
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label={'请输入订舱人:'}
              returnKeyType="search"
              value={this.state.inputShipper}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                this.setState({ inputShipper: text });
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />
          </InputGroup>
        </View>
        <Button
          block
          onPress={() => {
            Keyboard.dismiss();
            this.onSearchSubmit();
          }}
          style={{
            height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
          }}
        >
          <Text style={{ color: '#ffffff' }}>搜索</Text>
        </Button>
      </SafeAreaView>
    </View>
  );

  //  // 点击返回按钮时
  //  onBackPressed = () => {
  //    console.log('onBackPressed');
  //    if (!this.state.displayMode) {
  //      this.setState({ displayMode: true });
  //      return true;
  //    }
  //  };

  render() {
    console.log('数据', this.props.list, this.state.displayMode);

    // 搜索页面
    if (!this.state.displayMode) {
      return this.searchView();
    }
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ flex: 1 }}>
          {this.renderFilter()}
          <RefreshListView
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={item => item.id + ''}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          />
        </SafeAreaView>
      </Container>
    );
  }
}

DemurrageList.navigationOptions = ({ navigation }) => ({
  title: '超期费审核',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
          <HeaderButtons.Item
            title="取消"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              const exitSearch = navigation.getParam('exitSearch');
              exitSearch();
            }}
          />
        )}
    </HeaderButtons>
  ),
});

DemurrageList.propTypes = {
  navigation: PropTypes.object.isRequired,
  getDemurrageListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
DemurrageList.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeList(),
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDemurrageListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DemurrageList);
