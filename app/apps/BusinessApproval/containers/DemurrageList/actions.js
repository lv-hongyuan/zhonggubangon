import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_DEMURRAGE_LIST,
} from './constants';

export const getDemurrageListRoutine = createRoutine(GET_DEMURRAGE_LIST);
export const getDemurrageListPromise = promisifyRoutine(getDemurrageListRoutine);
