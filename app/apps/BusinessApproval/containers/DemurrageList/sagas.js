import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDemurrageListRoutine } from './actions';

// 查询超期费代办列表
function* getDemurrageList(action) {
  console.log(action);
  const { pageSize = 10, pageNum, loadMore, boxNo, shipper, waybillNum } = action.payload;
  try {
    yield put(getDemurrageListRoutine.request({ loadMore }));
    const response = yield call(request, ApiFactory.getDemurrageList(pageSize, pageNum, boxNo, shipper, waybillNum));
    console.log(response);
    
    const { demurrageDTOS } = response;

    console.log('输出啊啊啊啊', demurrageDTOS);
    yield put(getDemurrageListRoutine.success({     
      // list: loadMore ? [] : (demurrageDTOS || []),
      list: demurrageDTOS,
      loadMore,
      pageNum,
      pageSize,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDemurrageListRoutine.failure({
      loadMore,
      error: e,
    }));
  } finally {
    yield put(getDemurrageListRoutine.fulfill());
  }
}

export function* getDemurrageListSaga() {
  yield takeLatest(getDemurrageListRoutine.TRIGGER, getDemurrageList);
}

// All sagas to be loaded
export default [getDemurrageListSaga];
