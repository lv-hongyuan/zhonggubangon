import { createSelector } from 'reselect/es';

const selectDemurrageListDomain = () => state => state.demurrageList;

const makeList = () => createSelector(selectDemurrageListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDemurrageListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectDemurrageListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeList,
  makeIsLoading,
  makeRefreshState,
};
