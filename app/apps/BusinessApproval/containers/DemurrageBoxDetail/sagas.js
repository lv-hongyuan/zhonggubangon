import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDemurrageBoxDetailRoutine, approveDemurrageRoutine } from './actions';
import { getSpecialApplicationChangeDetailRoutine } from "../SpecialApplicationDetail/actions";

// 查询该运单下含有超期费的箱信息
function* getDemurrageBoxDetail(action) {

  console.log(action);
  const { pageSize = 10, pageNum, loadMore, boxNo, vesselVoyage, waybillNum } = action.payload;
  try {
    yield put(getDemurrageBoxDetailRoutine.request({ loadMore }));
    const response = yield call(request, ApiFactory.getDemurrageListDetail(pageSize, pageNum, waybillNum,vesselVoyage, boxNo));
    console.log(response);
    const { demurrageBoxDetailDTOs } = response;

    console.log('输出啊啊啊啊>>>', demurrageBoxDetailDTOs);
    yield put(getDemurrageBoxDetailRoutine.success({
      list: demurrageBoxDetailDTOs || [],
      loadMore,
      pageNum,
      pageSize,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDemurrageBoxDetailRoutine.failure({
      loadMore,
      error: e,
    }));
  } finally {
    yield put(getDemurrageBoxDetailRoutine.fulfill());
  }

}


// 查询该运单下含有超期费的箱信息
function* approveDemurrage(action) {
  const { ids, approveDiscount, checkFlag } = action.payload;
  console.log("sages", ids, approveDiscount, checkFlag);
  try {
    yield put(approveDemurrageRoutine.request());
    const response = yield call(request, ApiFactory.approveDemurrage(ids, approveDiscount, checkFlag));
    console.log(response);
    // 返回数据
    yield put(approveDemurrageRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(approveDemurrageRoutine.failure(error.message));
  } finally {
    yield put(approveDemurrageRoutine.fulfill());
  }


}

export function* getDemurrageBoxDetailSaga() {
  yield takeLatest(getDemurrageBoxDetailRoutine.TRIGGER, getDemurrageBoxDetail);
  yield takeLatest(approveDemurrageRoutine.TRIGGER, approveDemurrage);
}

// All sagas to be loaded
export default [getDemurrageBoxDetailSaga];
