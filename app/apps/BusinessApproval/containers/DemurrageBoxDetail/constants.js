/*
 *
 * BusinessApproval constants
 *
 */
export const GET_DEMURRAGE_BOX_DETAIL = 'onLand/BusinessApproval/GET_DEMURRAGE_BOX_DETAIL';
export const APPROVE_DEMURRAGE = 'onLand/BusinessApproval/APPROVE_DEMURRAGE';
export const PRE_COMPUTATION_DEMURRAGE = 'onLand/BusinessApproval/PRE_COMPUTATION_DEMURRAGE';

