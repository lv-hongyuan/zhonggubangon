import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  APPROVE_DEMURRAGE,
  GET_DEMURRAGE_BOX_DETAIL,
  PRE_COMPUTATION_DEMURRAGE
} from './constants';

export const getDemurrageBoxDetailRoutine = createRoutine(GET_DEMURRAGE_BOX_DETAIL);
export const getDemurrageBoxDetailPromise = promisifyRoutine(getDemurrageBoxDetailRoutine);


export const approveDemurrageRoutine = createRoutine(APPROVE_DEMURRAGE);
export const approveDemurragePromise = promisifyRoutine(approveDemurrageRoutine);

export const preComputationDemurrageRoutine = createRoutine(PRE_COMPUTATION_DEMURRAGE);
export const preComputationDemurragePromise = promisifyRoutine(preComputationDemurrageRoutine);


