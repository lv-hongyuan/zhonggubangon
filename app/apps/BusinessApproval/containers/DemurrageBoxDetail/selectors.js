import { createSelector } from 'reselect/es';

const selectDemurrageBoxDetailDomain = () => state => state.demurrageBoxDetail;

const makeList = () => createSelector(selectDemurrageBoxDetailDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const approveDemurrageData = () => createSelector(selectDemurrageBoxDetailDomain(), (subState) => {
  console.debug(subState);
  return subState.data;
});

const makeRefreshState = () => createSelector(selectDemurrageBoxDetailDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectDemurrageBoxDetailDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeList,
  approveDemurrageData,
  makeIsLoading,
  makeRefreshState,
};
