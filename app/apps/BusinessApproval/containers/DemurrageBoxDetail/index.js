import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { Keyboard, SafeAreaView, StyleSheet, TouchableOpacity } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  ROUTE_DEMURRAGE_BOX_DETAIL,
  ROUTE_SPECIAL_APPLICATION_DETAIL,
  ROUTE_SPECIAL_APPLICATION_SEARCH,
  ROUTE_DEMURRAGE_PRE_COMPUTATION
} from '../../RouteConstant';
import {
  approveDemurragePromise,
  getDemurrageBoxDetailPromise,
  getDemurrageBoxDetailRoutine,
  preComputationDemurragePromise
} from './actions';
import { approveDemurrageData, makeList, makeIsLoading, makeRefreshState} from './selectors';
import DemurrageBoxItem from './components/DemurrageBoxItem';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from "../../../../common/commonStyles";
import _ from "lodash";
import InputItem from "../../../../components/InputItem";
import { NavigationActions } from "react-navigation";
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../../../components/Svg';
import { Overlay } from "teaset";
import AlertView from "../../../../components/Alert";
import DatePullSelector from '../../../../components/DatePullSelector/index';
import DemurrageDateSelect from "./components/DemurrageDateSelect";
import DemurrageInputDiscount from "./components/DemurrageInputDiscount";


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
  bottomContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#ffffff',
    flexDirection: 'row',
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
    flex: 1,
//    flexDirection: 'row',
  },
  approveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
//  rejectButton: {
//    height: '100%',
//    backgroundColor: '#DC001B',
//    borderRadius: 0,
//    flex: 1,
//    justifyContent: 'center',
//  },
  preComputationButton: {
    height: '100%',
    backgroundColor: '#3d95d5',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
});

@screenHOC
class DemurrageBoxDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pageNum: 0,
      displayMode: true,
      boxNo: '',
      inputBoxNo: '',
      checked: [],
      pickUpBoxDate: null,
      discount: '',
    }
    this.list = [];
    this.inputPickUpBoxDate = null;
    this.inputDiscount = 0;

    this.waybillNum = '';
    this.vesselVoyage = '';
  }

  componentDidMount() {
    // const data = this.props.navigation.getParam("data");
    // console.log("componentDidMount：", data);
    // if (!data) {
    // } else {
    //   this.list = data;
    //   this.props.dispatch(getDemurrageBoxDetailRoutine.success(data));
    // }

    //运单号和船名航次
    this.waybillNum = this.props.navigation.getParam("waybillNum");
    this.vesselVoyage = this.props.navigation.getParam("vesselVoyage");
    
    this.flyfishLoadData(this.waybillNum,this.vesselVoyage,false);


    this.props.navigation.setParams({
      displayMode: this.state.displayMode,
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
      checkAll: () => {
        this.checkAll();
      }
    });
  }

  componentWillUnmount() {
  }

  onHeaderRefresh = () => {
    this.flyfishLoadData(this.waybillNum,this.vesselVoyage,false);
  };

  onFooterRefresh = () => {
    this.flyfishLoadData(this.waybillNum,this.vesselVoyage,true);
  };

  flyfishLoadData(waybillNum,vesselVoyage,loadMore: boolean) {
    this.props.getDemurrageBoxDetailPromise({
      loadMore,
      pageNum: loadMore ? this.state.pageNum + 1 : 1,
      boxNo: this.state.boxNo,
      waybillNum: waybillNum,
      vesselVoyage: vesselVoyage,
    }).then(({ pageNum }) => {
      console.log(pageNum);
      this.setState({ pageNum });
    }).catch(() => {
    });
  }

  onSearchSubmit = () => {
    this.setState({
      displayMode: true,
      boxNo: this.state.inputBoxNo,
    }, () => {
      // 箱号本地查询
      console.log("this.state.boxNo", this.state.boxNo);
      if (this.state.boxNo) {
        let newList = [];
        this.list.map(item => {
          if (item.boxNo && item.boxNo.indexOf(this.state.boxNo) > -1) {
            newList.push(item);
          }
        });
        console.log("newList", newList);
        this.props.dispatch(getDemurrageBoxDetailRoutine.success(newList));
      } else {
        this.props.dispatch(getDemurrageBoxDetailRoutine.success(this.list));
      }
    });
  };
  //
  changeDisplayMode = (displayMode = false) => {
    this.props.navigation.setParams({
      displayMode
    });
    if (!displayMode) {  // 当 == false   打开 搜索画面时
      this.setState({
        displayMode,
        inputBoxNo: this.state.boxNo,
      });
    } else {
      this.setState({ displayMode, });
    }
  }
  checkAll = () => {
    // 全部取消
    if (this.props.list.length === this.state.checked.length) {
      this.setState({ checked: [] });
    } else {
      const dataList = this.props.list.map(item => item.id);
      this.setState({ checked: dataList });
    }
  };


  // 点击一个
  selectItem = (id) => {
    let datalist = this.state.checked;
    let index = datalist.indexOf(id);
    if (index > -1) {
      datalist.splice(index, 1);
      this.setState({ checked: datalist });
    } else {
      this.setState({ checked: [...this.state.checked, id] });
    }
  }

  // 提前计算超期费
  preComputation = () => {
    if (this.state.checked.length === 0) {
      AlertView.show({ message: '请选择一条记录！' });
      return;
    }
    if (this.state.checked.length !== 1) {
      AlertView.show({ message: '计算超期费只能选择一条记录' });
      return;
    }
    let data;
    this.props.list.map(item => {
      if (item.id === this.state.checked[0]) {
        data = item;
      }
    })
    this.props.navigation.navigate(ROUTE_DEMURRAGE_PRE_COMPUTATION, {
      data: data,
      onBack: () => {

      },
    });
  }

  // 审核 调到下一画面 输入审核折扣
  approveApplication = () => {
    let backFlag = true;
    if (this.state.checked.length === 0) {
      AlertView.show({ message: '请选择一条记录！' });
      return;
    }
    let dataList = [];
    this.props.list.map((item, index) => {
      this.state.checked.map(idItem => {
        if (idItem === item.id) {
          dataList.push(item);
        }
      });
    });
    let checkFlagSame = true;
    let checkFlag = dataList[0].checkFlag;
    dataList.map(item => {
      if (item.checkFlag !== checkFlag) {
        checkFlagSame = false;
      }
    })
    if (!checkFlagSame) {
      AlertView.show({
        title: '审核类型不相同，请逐条审核',
        showCancel: true,
      });
      return false;
    }

    AlertView.show({
      title: '请填写审核折扣',
      showCancel: true,
      confirmAction: () => {
        console.log("调用审核接口");
        if (!this.state.discount) { // 未填写不关闭确认框
          return false;
        }
        if (this.state.discount > 100 || this.state.discount < -200) {
          AlertView.show({ message: '折扣率不能大于100，不能小于-200' });
          return;
        }
        this.approve(this.state.discount, checkFlag);
      },
      cancelAction: () => {
        this.setState({ discount: null });
      },
      component: () => (
        <DemurrageInputDiscount
          onChange={(value) => {
            this.setState({ discount: value })
          }}
        />
      )
    });
  };

  approve = (discount, checkFlag) => {
    console.log('index调用接口数据', discount, checkFlag);
    this.props.approveDemurragePromise({
      ids: this.state.checked,
      approveDiscount: discount,
      checkFlag: checkFlag
    }).then(( data) => {
      AlertView.show({
        // message: this.props.approveDemurrageData._backmes,
        message:data._backmes,
        showCancel: false,
        confirmAction: () => {
          const onBack = this.props.navigation.getParam("onBack");
          this.props.navigation.dispatch(NavigationActions.back());
          if (onBack) {
            onBack();
          }
        }
      });
    }).catch((error) => {
      // 错误提示 不返回页面
      AlertView.show({
        message: error,
        showCancel: false,
        confirmAction: () => {

        }
      });
    });
  }

  // 搜索头
  renderFilter = () => {
    return (
      <View style={{ ...styles.container }}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ padding: 10, flex: 1 }}>
            <TouchableOpacity
              activeOpacity={1}
              style={styles.searchInput}
              onPress={() => {
                this.changeDisplayMode();
              }}
            >
              {!_.isEmpty(this.state.boxNo) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>箱号:</Label>
                  <Text style={commonStyles.text}>{this.state.boxNo}</Text>
                </View>
              )}
              {_.isEmpty(this.state.boxNo) && (
                <Item
                  style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                  onPress={() => {
                    this.changeDisplayMode();
                  }}
                >
                  <Text style={commonStyles.text}>点击输入要搜索的箱号</Text>
                </Item>
              )}
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>
    );
  }

  // 搜索画面
  searchView = () => {
    return (
      <View style={{ ...styles.container }}>
        <SafeAreaView style={{ width: '100%' }}>
          <View style={{ padding: 10, }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label={'请输入箱号:'}
                returnKeyType="search"
                value={this.state.inputBoxNo}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputBoxNo: text });
                }}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                }}
              />
            </InputGroup>
          </View>
          <Button
            block
            onPress={() => {
              Keyboard.dismiss();
              this.onSearchSubmit();
            }}
            style={{
              height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
            }}
          >
            <Text style={{ color: '#ffffff' }}>搜索</Text>
          </Button>
        </SafeAreaView>
      </View>
    );
  }

  // 渲染item
  renderItem = ({ item }) => {
//    console.log('进入renderItem', item, this.state.checked);
    let isSelect = false;
    if (this.state.checked.indexOf(item.id) > -1) {
      isSelect = true;
    }
    return (
      <DemurrageBoxItem
        item={item}
        onSelect={(id) => {
          this.selectItem(id);
        }}
        isSelect={isSelect}
      />
    )
  };

  render() {
    const list = this.props.list;
    console.log("data：", list);
    // 搜索页面
    if (!this.state.displayMode) {
      return this.searchView();
    }
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ flex: 1, }}>
          {this.renderFilter()}
          <RefreshListView
            style={{ width: '100%', }}
            data={list}
            contentContainerStyle={{ paddingBottom: 60 }}
            keyExtractor={item => item.id + ''}
            renderItem={this.renderItem}
            ListHeaderComponent={() => (<View style={{ height: 5 }}/>)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }}/>)}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
          />
          <SafeAreaView style={styles.bottomContainer}>
            <View style={styles.buttonContainer}>
              <Button onPress={() => {
                this.preComputation();
              }} style={styles.preComputationButton}>
                <Text>计算</Text>
              </Button>
            </View>
            <View style={styles.buttonContainer}>
              <Button onPress={() => {
                // 先清空之前的值
                this.setState({ discount: 0 }, () => {
                  this.approveApplication();
                })
              }} style={styles.approveButton}>
                <Text>审核</Text>
              </Button>
            </View>
          </SafeAreaView>
        </SafeAreaView>
      </Container>
    );
  }
}

DemurrageBoxDetail.navigationOptions = ({ navigation }) => ({
  title: '超期费审核详情',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white"/>}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
  headerRight: (
    <View>
      {navigation.getParam('displayMode', true) ?
        <HeaderButtons>
          <HeaderButtons.Item
            title="全选"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              const checkAll = navigation.getParam('checkAll');
              checkAll();
            }}
          />
        </HeaderButtons>
        :
        <View/>}
    </View>
  ),
});
DemurrageBoxDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
  getDemurrageBoxDetailPromise: PropTypes.func.isRequired,
  approveDemurragePromise: PropTypes.func.isRequired,
  preComputationDemurragePromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  approveDemurrageData: PropTypes.object,
};
DemurrageBoxDetail.defaultProps = {
  list: [],
  approveDemurrageData: {},
};

const mapStateToProps = createStructuredSelector({
  // list: makeList(),
  // approveDemurrageData: approveDemurrageData(),

  list: makeList(),
  isLoading: makeIsLoading(),
  refreshState: makeRefreshState(),

});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDemurrageBoxDetailPromise,
      approveDemurragePromise,
      preComputationDemurragePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DemurrageBoxDetail);
