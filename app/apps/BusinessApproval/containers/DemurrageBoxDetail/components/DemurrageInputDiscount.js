import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView, Keyboard, } from 'react-native';

import { View, InputGroup, Item, Label, } from 'native-base';
import commonStyles from "../../../../../common/commonStyles";
import DatePullSelector from '../../../../../components/DatePullSelector/index';
import InputItem from "../../../../../components/InputItem";


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  label: {
    marginRight: 5,
  },

});

class DemurrageInputDiscount extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      discount: null
    }

  }

  componentDidMount() {
  }

  render() {
    return (
      <View>
        <SafeAreaView style={{ width: '100%' }}>
          <InputGroup style={{ ...commonStyles.inputGroup, width: 150,  }}>
            <InputItem
              style={{
                alignItems: 'center',
                borderWidth: 0.5,
                borderColor: '#E0E0E0',
              }}
              label={''}
              placeholder={''}
              value={this.state.discount}
//              clearButtonMode="while-editing"
              onChangeText={(text) => {
                this.setState({ discount: text },
                  () => {
                    this.props.onChange(text);
                  });
              }}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
            />
          </InputGroup>
        </SafeAreaView>
      </View>
    );
  }
}

DemurrageInputDiscount.defaultProps = {
  onChange: () => {
  }
};
DemurrageInputDiscount.propTypes = {
  onChange: PropTypes.func.isRequired,
};


export default DemurrageInputDiscount;
