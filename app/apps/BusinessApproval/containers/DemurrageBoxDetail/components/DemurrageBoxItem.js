import React from 'react';
import PropTypes from 'prop-types';
import CheckBox from 'react-native-check-box'
import {
  StyleSheet, TouchableOpacity,
} from 'react-native';
import {
  Text, View,
} from 'native-base';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  label: {
    marginRight: 5,
  },
  info: {
    flex: 1,
  },
  badge: {},
  checkBoxItem: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
});

class DemurrageBoxItem extends React.PureComponent {
  constructor(props) {
    super(props);

  }

  componentWillMount() {
  }

  render() {
    const item = this.props.item || {};
    const {
      id, vesselVoyage, waybillNum, boxNo, shipper,
      endPort, currentDiscount, applyDiscount, operatorAuditedDiscount,
      currentDemurrage, boxType, discountDemurrage, applyType,
      checkFlag, areaMainDiscount, areaMain
    } = item;

    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onSelect(id);
        }}
      >
        <View style={styles.container}>
          <View style={styles.group}>

          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>船名航次:</Text>
              <Text style={styles.info}>{vesselVoyage}</Text>
              <CheckBox style={{ flex: 0, padding: 10, justifyContent: 'flex-end', height: 5 }}
                        onClick={() => {
                          this.props.onSelect(id);
                        }}
                        isChecked={this.props.isSelect}
                        checkBoxColor={'#DC001B'}
              />
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>运单号:</Text>
              <Text style={styles.info}>{waybillNum}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>箱号:</Text>
              <Text style={styles.info}>{boxNo}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>箱型:</Text>
              <Text style={styles.info}>{boxType}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>订舱人:</Text>
              <Text style={styles.info}>{shipper}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>目的港:</Text>
              <Text style={styles.info}>{endPort}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>当前折扣:</Text>
              <Text style={styles.info}>{currentDiscount}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>申请折扣:</Text>
              <Text style={styles.info}>{applyDiscount}</Text>
            </View>
          </View>
          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={{ ...styles.label, color: applyType === '二次' ? 'red' : 'black' }}>审核类型:</Text>
              <Text style={{ ...styles.info, color: applyType === '二次' ? 'red' : 'black' }}>{applyType}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>操作审核折扣:</Text>
              <Text style={styles.info}>{operatorAuditedDiscount}</Text>
            </View>
          </View>

          <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>应收超期费:</Text>
              <Text style={styles.info}>{currentDemurrage ? currentDemurrage : '--'}</Text>
            </View>
            <View style={styles.item}>
              <Text style={styles.label}>折后超期费:</Text>
              <Text style={styles.info}>{discountDemurrage ? discountDemurrage : '--'}</Text>
            </View>
          </View>
          {checkFlag === 2 && <View style={styles.group}>
            <View style={styles.item}>
              <Text style={styles.label}>片区审核人:</Text>
              <Text style={styles.info}>{areaMain}</Text>
            </View>

          </View>}
          {checkFlag === 2 && <View style={styles.group}>
            <View style={styles.item}>
              <Text style={{ ...styles.label }}>片区审核折扣:</Text>
              <Text style={{ ...styles.info }}>{areaMainDiscount}</Text>
            </View>
          </View>}
        </View>
      </TouchableOpacity>
    );
  }
}

DemurrageBoxItem.defaultProps = {
  item: {},
  isSelect: false,
  onSelect: () => {

  }
};
DemurrageBoxItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  isSelect: PropTypes.bool.isRequired,
};


export default DemurrageBoxItem;
