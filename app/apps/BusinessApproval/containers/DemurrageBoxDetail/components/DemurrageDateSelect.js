import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView, } from 'react-native';

import { View, InputGroup, Item, Label, } from 'native-base';
import commonStyles from "../../../../../common/commonStyles";
import DatePullSelector from '../../../../../components/DatePullSelector/index';


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
  },
  titleContainer: {
    padding: 5,
    backgroundColor: 'red',
    borderRadius: 5,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    flex: 1,
  },
  group: {
    flexDirection: 'row',
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    padding: 5,
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center',
  },
  label: {
    marginRight: 5,
  },

});

class DemurrageDateSelect extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      pickUpBoxDate: undefined
    }

  }

  componentDidMount() {
    this.setState({ pickUpBoxDate: this.props.defaultDate });
  }

  render() {
    return (
      <View>
        <SafeAreaView style={{ width: '100%' }}>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={{ ...commonStyles.inputItem, width: 130 }}>
              <DatePullSelector
                value={this.state.pickUpBoxDate}
                onChangeValue={(value) => {
                  console.log(value);
                  this.setState({ pickUpBoxDate: value }, () => {
                    this.props.onChangeValue(value);
                  });

                }}
              />
            </Item>
          </InputGroup>
        </SafeAreaView>
      </View>
    );
  }
}

DemurrageDateSelect.defaultProps = {
  defaultDate: undefined,
  onChangeValue: () => {
  }
};
DemurrageDateSelect.propTypes = {
  onChangeValue: PropTypes.func.isRequired,
  defaultDate: PropTypes.number,
};


export default DemurrageDateSelect;
