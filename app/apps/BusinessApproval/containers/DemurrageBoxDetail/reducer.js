import fixIdList from '../../../../utils/fixIdList';
import { getDemurrageBoxDetailRoutine, approveDemurrageRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  list: [],
  data: undefined,
  isLoading: false,
};

export default function(state = defaultState, action) {
  switch (action.type) {
    // 获取动态
    case getDemurrageBoxDetailRoutine.REQUEST:
      return {
        ...state,
        isLoading: true,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getDemurrageBoxDetailRoutine.SUCCESS: {
      // const list = action.payload.list;
      // return {
      //   ...state,
      //   list: list,
      // };

      const { list, pageSize, loadMore } = action.payload;
      console.log("!!!",list);
      const fixList = fixIdList(list);
      const newList = loadMore ? state.list.concat(fixList) : fixList;
      const hasMore = fixList.length === pageSize;
      
      console.log('hasMore', fixList.length, pageSize, hasMore);
      const isEmpty = newList.length === 0;
      let refreshState = RefreshState.Idle;
      if (isEmpty) {
        refreshState = RefreshState.EmptyData;
      } else if (!hasMore) {
        refreshState = RefreshState.NoMoreData;
      }
      return {
        ...state,
        list: newList,
        error: undefined,
        refreshState,
      };

    }
    case getDemurrageBoxDetailRoutine.FAILURE: {
      const { loadMore } = action.payload;
      return {
        ...state,
        list: loadMore ? state.list : [],
        error,
        list: loadMore ? state.list : [],
      };
    }
    case getDemurrageBoxDetailRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };


    // 审核
    case approveDemurrageRoutine.REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case approveDemurrageRoutine.SUCCESS: {
      console.log("reducer", action.payload, action);
      return {
        ...state,
        data: action.payload,
      };
    }
    case approveDemurrageRoutine.FAILURE: {
      console.log('审核失败', action.payload);
      console.log('审核失败1', action);
      return {
        ...state,
        error: action.payload,
      };
    }
    case approveDemurrageRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
