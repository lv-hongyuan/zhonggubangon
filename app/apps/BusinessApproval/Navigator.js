import React from 'react';
import { NavigationActions } from 'react-navigation';
import { createStackNavigator, StackViewStyleInterpolator } from 'react-navigation-stack';
import HeaderButtons from 'react-navigation-header-buttons';
import Svg from '../../components/Svg';

import ToDo from './containers/ToDo';
import SpecialApplicationList from './containers/SpecialApplicationList';
import SpecialApplicationDetail from './containers/SpecialApplicationDetail';
import SpecialApplicationSearch from './containers/SpecialApplicationSearch';
import DemurrageList from './containers/DemurrageList';
import DemurrageBoxDetail from './containers/DemurrageBoxDetail';
import DemurragePreComputation from './containers/DemurragePreComputation';

import {
  ROUTE_TO_DO,
  ROUTE_SPECIAL_APPLICATION_LIST,
  ROUTE_SPECIAL_APPLICATION_DETAIL,
  ROUTE_SPECIAL_APPLICATION_SEARCH,
  ROUTE_DEMURRAGE_LIST,
  ROUTE_DEMURRAGE_BOX_DETAIL,
  ROUTE_DEMURRAGE_PRE_COMPUTATION,
} from './RouteConstant';

const defaultNavigationOptions = ({ navigation }) => ({
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#DC001B',
  },
  headerBackTitle: null,
  headerTitleStyle: {
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
  },
  headerTitleAllowFontScaling:false,//禁用字体跟随系统字体大小变化
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={() => {
          navigation.dispatch(NavigationActions.back());
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons />
  ),
});

const HomeNavigator = createStackNavigator({
  [ROUTE_TO_DO]: { screen: ToDo },
  [ROUTE_SPECIAL_APPLICATION_LIST]: { screen: SpecialApplicationList },
  [ROUTE_SPECIAL_APPLICATION_DETAIL]: { screen: SpecialApplicationDetail },
  [ROUTE_SPECIAL_APPLICATION_SEARCH]: { screen: SpecialApplicationSearch },
  [ROUTE_DEMURRAGE_LIST]: { screen: DemurrageList },
  [ROUTE_DEMURRAGE_BOX_DETAIL]: { screen: DemurrageBoxDetail },
  [ROUTE_DEMURRAGE_PRE_COMPUTATION]: { screen: DemurragePreComputation },
}, {
  initialRouteKey: ROUTE_TO_DO,
  initialRouteName: ROUTE_TO_DO,
  transitionConfig: () => ({
    screenInterpolator: StackViewStyleInterpolator.forHorizontal,
  }),
  navigationOptions: defaultNavigationOptions,
});

export default HomeNavigator;
