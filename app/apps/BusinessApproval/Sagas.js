import ToDoSagas from './containers/ToDo/sagas';
import SpecialApplicationListSagas from './containers/SpecialApplicationList/sagas';
import SpecialApplicationDetailSagas from './containers/SpecialApplicationDetail/sagas';
import SpecialApplicationSearchSagas from './containers/SpecialApplicationSearch/sagas';
import DemurrageListSagas from './containers/DemurrageList/sagas';
import DemurrageBoxDetailSagas from './containers/DemurrageBoxDetail/sagas';
import DemurragePreComputationSagas from './containers/DemurragePreComputation/sagas';

const allSagas = [
  ...ToDoSagas,
  ...SpecialApplicationListSagas,
  ...SpecialApplicationDetailSagas,
  ...SpecialApplicationSearchSagas,
  ...DemurrageListSagas,
  ...DemurrageBoxDetailSagas,
  ...DemurragePreComputationSagas
];

export default allSagas;
