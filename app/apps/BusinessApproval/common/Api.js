/**
 * Created by xu-jzh on 2017/7/18.
 */
import { ApiConstants } from './Constant';
import AppStorage from '../../../utils/AppStorage';

export const header = {
  content: {
    Accept: 'application/Json',
    'Content-Type': 'application/json',
  },
};

const ApiFactory = {
  getToDoTypeList() {
    console.log("token:", AppStorage.token);
    return fetch(`${ApiConstants.api}dbTotal/getDbTotals.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({}),
    });
  },

  getSpecialApplicationList(pageSize = 10, pageNum = 1, tjSfgPq = '', tjSfg = '',tjType = 0) {
    return fetch(`${ApiConstants.api}specialPrice/getSpecialPrices.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({
        pageSize,
        pageNum,
        tjSfgPq,
        tjSfg,
        tjType,
      }),
    });
  },

  getSpecialApplicationDetail(id) {
    return fetch(`${ApiConstants.api}findMDynPort/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  getSpecialApplicationChangeDetail(id) {
    return fetch(`${ApiConstants.api}specialPrice/getDiffentce.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({
        id,
      }),
    });
  },

  rejectApplication({ id, rejectReason }) {
    return fetch(`${ApiConstants.api}specialPrice/refuseSpecialPrice.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({
        id,
        tjBhbz: rejectReason,
      }),
    });
  },

  approveApplication(data) {
    return fetch(`${ApiConstants.api}specialPrice/auditSpecialPrice.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 查询所有始发港（根据片区字段）
  getSpecialApplicationAllPort(tjSfgPq) {
    return fetch(`${ApiConstants.api}specialPrice/getAllPort.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({
        tjSfgPq: tjSfgPq
      }),
    });
  },
  // 查询所有片区
  getSpecialApplicationAllArea() {
    return fetch(`${ApiConstants.api}specialPrice/getAllArea.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({}),
    });
  },
  // 超期费列表
  /*
  getDemurrageList(pageSize = 10, pageNum = 1, boxNo = '', shipper = '', waybillNum = '') {
    return fetch(`${ApiConstants.api}demurrage/getDemurrageList.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({ pageSize, pageNum, boxNo, shipper, waybillNum }),
    });
  },
  */
  //超期费列表新接口
 getDemurrageList(pageSize = 10, pageNum = 1, boxNo = '', shipper = '', waybillNum = '') {
  return fetch(`${ApiConstants.api}demurrage/getDemurrageBillList.json`, {
    method: 'POST',
    headers: {
      ...header.content,
      token: AppStorage.token,
    },
    body: JSON.stringify({ pageSize, pageNum, boxNo, shipper, waybillNum }),
  });
},

  //超期费审核详情
  getDemurrageListDetail(pageSize = 10,pageNum = 1,waybillNum = '',vesselVoyage = '',boxNo = ''){
    return fetch(`${ApiConstants.api}demurrage/getDemurrageBoxDetail.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({ pageSize, pageNum, waybillNum, vesselVoyage, boxNo }),
    });
  },

  // 审核超期费
  approveDemurrage(ids = [], approveDiscount = 100, checkFlag) {
    return fetch(`${ApiConstants.api}demurrage/approveDemurrage.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({ ids, approveDiscount, checkFlag }),
    });
  },

  // 超期费计算
  preComputationDemurrage(pickUpBoxDate, approveDiscount, ids) {
    return fetch(`${ApiConstants.api}demurrage/calculate.json`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify({ pickUpBoxDate, approveDiscount, ids }),
    });
  },

};


export default ApiFactory;
