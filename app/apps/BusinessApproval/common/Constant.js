import { AsyncStorage } from 'react-native';

export const BusinessApprovalHostKey = 'BusinessApprovalHostKey';

let host = 'dingcang.zhonggu56.com';
// let host= '172.16.1.61:8110';
//let host = 'dctestapi.zhonggu56.com:7701';
//let host = 'localhost:8480';

export const ApiConstants = {
  get api() {
    const site = `http://${host}`;
    return `${site}/app/`;
  },
  get host() {
    return host;
  },
  set host(newHost) {
    host = newHost;
    AsyncStorage.setItem(BusinessApprovalHostKey, newHost);
  },
};

export const TodoType = {
  OA: 1, // OA待办
  Business: 2, // 业务待办
  Demand: 3, // 需求待办
  SpecialPrice: 4, // 特价待办
  OverdueFee: 5, // 超期费
  OrderSpecialPrice:40,  //订单特价待办
  ChangeSpecialPrice:41,  //改单特价待办
  Overprice:42,  //!议价申请待办
};

// 特价类型
export const SpecialPriceType = {
  OrderApplication: '订单申请', // 订单申请
  InternalOrderChange: '内部改单', // 内部改单
  ExternalOrderChange: '外部改单', // 外部改单
};

// 审核状态
export const ReviewState = {
  Unapproved: 0, // 未审核
  UnderReview: 1, // 审核中
  Reviewed: 2, // 已审核
  Reject: 3, // 驳回
  CustomerWithdrawal: 4, // 客户撤单
};

/**
 * @return {string}
 */
export function reviewTitleFromState(state) {
  switch (state) {
    case ReviewState.Unapproved:
      return '未审核';
    case ReviewState.UnderReview:
      return '审核中';
    case ReviewState.Reviewed:
      return '已审核';
    case ReviewState.Reject:
      return '已驳回';
    case ReviewState.CustomerWithdrawal:
      return '客户撤单';
    default:
      return '';
  }
}
