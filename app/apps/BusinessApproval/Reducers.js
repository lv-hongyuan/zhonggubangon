import ToDo from './containers/ToDo/reducer';
import SpecialApplicationList from './containers/SpecialApplicationList/reducer';
import SpecialApplicationDetail from './containers/SpecialApplicationDetail/reducer';
import SpecialApplicationSearch from './containers/SpecialApplicationSearch/reducer';
import DemurrageList from './containers/DemurrageList/reducer';
import DemurrageBoxDetail from './containers/DemurrageBoxDetail/reducer';
import DemurragePreComputation from './containers/DemurragePreComputation/reducer';

const reducers = {
  todo: ToDo,
  specialApplicationList: SpecialApplicationList,
  specialApplicationDetail: SpecialApplicationDetail,
  specialApplicationSearch: SpecialApplicationSearch,
  demurrageList: DemurrageList,
  demurrageBoxDetail: DemurrageBoxDetail,
  demurragePreComputation: DemurragePreComputation,
};

export default reducers;
