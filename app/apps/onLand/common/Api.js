/**
 * Created by xu-jzh on 2017/7/18.
 */
import { ApiConstants } from './Constant';
import AppStorage from '../../../utils/AppStorage';

export const header = {
  content: {
    Accept: 'application/Json',
    'Content-Type': 'application/json',
  },
};

const ApiFactory = {
  getUserInfo() {
    return fetch(`${ApiConstants.api}getuser`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  getWorkList() {
    return fetch(`${ApiConstants.api}getDynList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  receiveShip(id) {
    return fetch(`${ApiConstants.api}receiveShip/${id}`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  getWorkDetail(id) {
    return fetch(`${ApiConstants.api}findMDynPort/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  savePlan(data) {
    return fetch(`${ApiConstants.api}plan`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  completePlan(data) {
    return fetch(`${ApiConstants.api}planOK`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  changePlan(data) {
    return fetch(`${ApiConstants.api}planChange`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  saveBerthing(data) {
    return fetch(`${ApiConstants.api}berth`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  completeBerthing(data) {
    return fetch(`${ApiConstants.api}berthOK`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  saveOperating(data) {
    return fetch(`${ApiConstants.api}work`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  completeOperating(data) {
    return fetch(`${ApiConstants.api}workOK`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  saveUnBerthing(data) {
    return fetch(`${ApiConstants.api}leave`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  completeUnBerthing(data) {
    return fetch(`${ApiConstants.api}leaveOK`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  getInstructions(data) { 
    return fetch(`${ApiConstants.api}getNoticeList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  readInstruction(id) {
    return fetch(`${ApiConstants.api}readOne/${id}`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  readAllInstructions() {
    return fetch(`${ApiConstants.api}readAll`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  getPopDetail(popId) {
    return fetch(`${ApiConstants.api}findPopDetail/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  savePopDetail(data) {
    return fetch(`${ApiConstants.api}savePopDetail`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  completePopDetail(data) {
    return fetch(`${ApiConstants.api}finishPopDetail`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  logOut() {
    return fetch(`${ApiConstants.api}logOut`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 检查更新
  checkUpdate({ appKey, type, versionCode }) {
    return fetch(`${ApiConstants.api}getMaxOne`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
      },
      body: JSON.stringify({
        appKey,
        type,
        versionCode,
      }),
    });
  },

  // 上传app版本信息
  uploadAppInfo({
    appKey, appVersion, sysVersion, phoneModel, userId,
  }) {
    return fetch(`${ApiConstants.api}saveUserApp`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        'Content-Type': 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify({
        appKey,
        appVersion,
        sysVersion,
        phoneModel,
        userId,
      }),
    });
  },

  // 现场船期表列表
  getShippingSchedule() {
    return fetch(`${ApiConstants.api}ShippingSchedule`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 现场船期表详细信息
  getShippingScheduleDetail(popid) {
    return fetch(`${ApiConstants.api}ShippingScheduleDetail/${popid}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  //船舶动态列表
  getShipPopList(data){
    return fetch(`${ApiConstants.api}getShipPopList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //船期表列表
  getShipScheduleList(data){
    return fetch(`${ApiConstants.api}getShipScheduleList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //船期表详情
  getShipVoyageList(data){
    return fetch(`${ApiConstants.api}getShipVoyageList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },


  // 船舶详细信息
  getShipInfo(shipId) {
    return fetch(`${ApiConstants.api}ShipInfo/${shipId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  //托轮费计费
  getTugCost(data){
    return fetch(`${ApiConstants.api}getTugCost`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 靠泊效率
  getBerthEfficiency() {
    return fetch(`${ApiConstants.api}BerthEfficiency`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 载重量默认值
  getDefaultLoadValue(data) {
    return fetch(`${ApiConstants.api}getConts`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 码头列表
  getPierList(id) {
    return fetch(`${ApiConstants.api}getPierList/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 泊位列表
  getBerthList(id) {
    return fetch(`${ApiConstants.api}getBerthList/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 每日船期表
  shipDateList() {
    return fetch(`${ApiConstants.api}shipDateList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 直靠率
  // portBerthEfficiencyList(fromDateTime, toDateTime) {
  //   return fetch(`${ApiConstants.api}portBerthEfficiencyList/${fromDateTime || 0}/${toDateTime || 0}`, {
  //     method: 'GET',
  //     headers: {
  //       ...header.content,
  //       token: AppStorage.token,
  //     },
  //   });
  // },
  portBerthEfficiencyList(fromDateTime, toDateTime, feemon, week, linetype, port) {
    return fetch(`${ApiConstants.api}portBerthEfficiencyList/${fromDateTime || 0}/${toDateTime || 0}/${feemon || null}/${week || 0}/${linetype || null}/${port || null}`, {
    method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  //码头计划
  wharfPlaneList(data) {
    return fetch(`${ApiConstants.api}selectBerthPlan`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //作业进度
  taskProcessList(data) {
    return fetch(`${ApiConstants.api}selectWorkProgress`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //作业进度详情
  taskProcessDetail(data) {
    return fetch(`${ApiConstants.api}getDetailData`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  taskProcessDetailNotification(data) {
    return fetch(`${ApiConstants.api}getOnePushData`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 各片区直靠率排名
  // pqBerthEfficiencyList(fromDateTime, toDateTime) {
  //   return fetch(`${ApiConstants.api}pqBerthEfficiencyList/${fromDateTime || 0}/${toDateTime || 0}`, {
  //     method: 'GET',
  //     headers: {
  //       ...header.content,
  //       token: AppStorage.token,
  //     },
  //   });
  // },
  pqBerthEfficiencyList(fromDateTime, toDateTime, feemon, week, linetype) {
    return fetch(`${ApiConstants.api}pqBerthEfficiencyList/${fromDateTime || 0}/${toDateTime || 0}/${feemon || null}/${week || 0}/${linetype || null}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 船舶在港超时汇总
  inPortOverTimeList(fromDateTime, toDateTime) {
    return fetch(`${ApiConstants.api}inPortOverTimeList/${fromDateTime || 0}/${toDateTime || 0}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 在港停时考核
  inPortTimeExamDTOList(fromDateTime, toDateTime) {
    return fetch(`${ApiConstants.api}inPortTimeExamDTOList/${fromDateTime || 0}/${toDateTime || 0}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 靠泊计划
  berthPlanList(fromDateTime, toDateTime) {
    return fetch(`${ApiConstants.api}berthPlanList/${fromDateTime || 0}/${toDateTime || 0}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 获取公告列表
  getPublishNoticeList(data) {
    return fetch(`${ApiConstants.api}getPublishNoticeList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
    
  //获取未读消息数量
  getNotReadNoticeNum(){
    return fetch(`${ApiConstants.api}getNotReadNoticeNum`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },
  //!批量标记已读
  upDataMultiple(data){
    return fetch(`${ApiConstants.api}readNotices`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: data,
    });
  },
  // getPublishNoticeList(fromDateTime, toDateTime) {
  //   return fetch(`${ApiConstants.api}getPublishNoticeList/${fromDateTime || 0}/${toDateTime || 0}`, {
  //     method: 'GET',
  //     headers: {
  //       ...header.content,
  //       token: AppStorage.token,
  //     },
  //   });
  // },

  // 公告标记已读
  getPublishNoticeRead(data) {
    return fetch(`${ApiConstants.api}getPublishNoticeRead`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 停止提醒
  changeStopRemindState(data) {
    return fetch(`${ApiConstants.api}changeStopRemindState`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 每日实装
  getRealLoad() {
    return fetch(`${ApiConstants.api}getRealLoad`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 每日预配
  getPrematch() {
    return fetch(`${ApiConstants.api}getPrematch`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 箱号查询
  getQueryBoxNumber(data) {
    return fetch(`${ApiConstants.api}getContDetail`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //考核公示
  getEditionList(data) {
    return fetch(`${ApiConstants.api}getEditionList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  // 综合得分考核列表（港口）
  getPortAssessList(data) {
    return fetch(`${ApiConstants.api}/getPortAssessList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  // 直靠考核列表（港口）
  getBerthAssessList(data) {
    return fetch(`${ApiConstants.api}/getBerthAssessList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // APP考核列表（港口）
   getAppAssessList(data) {
    return fetch(`${ApiConstants.api}/getAppAssessList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 靠离考核列表（港口）
  getOpAssessList(data) {
    return fetch(`${ApiConstants.api}/getOpAssessList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 在港停时考核列表（港口）
  getTimeInPortAssessList(data) {
    return fetch(`${ApiConstants.api}/getTimeInPortAssessList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 直靠考核列表（片区）
  getBerthAssessPqList(data) {
    return fetch(`${ApiConstants.api}/getBerthAssessPqList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // APP考核列表（片区）
  getAppAssessPqList(data) {
    return fetch(`${ApiConstants.api}/getAppAssessPqList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 靠离考核列表（片区）
  getOpAssessPqList(data) {
    return fetch(`${ApiConstants.api}/getOpAssessPqList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 在港停时考核列表（片区）
  getTimeInPortAssessPqList(data) {
    return fetch(`${ApiConstants.api}/getTimeInPortAssessPqList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 综合得分考核列表（片区）
  getPortAssessPqList(data) {
    return fetch(`${ApiConstants.api}/getPortAssessPqList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  //! 综合得分考核列表（码头）
  getPortAssessWharfList(data) {
    return fetch(`${ApiConstants.api}/getPortAssessWharfList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  //! 直靠考核列表（码头）
  getBerthAssessWharfList(data) {
    return fetch(`${ApiConstants.api}/getBerthAssessWharfList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  //! APP考核列表（码头）
  getAppAssessWharfList(data) {
    return fetch(`${ApiConstants.api}/getAppAssessWharfList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  //! 靠离考核列表（码头）
  getOpAssessWharfList(data) {
    return fetch(`${ApiConstants.api}/getOpAssessWharfList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },
  //! 在港停时考核列表（码头）
  getTimeInPortAssessWharfList(data) {
    return fetch(`${ApiConstants.api}/getTimeInPortAssessWharfList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 用户菜单查询
  getAPPFunctionList() {
    return fetch(`${ApiConstants.api}getAPPFunctionList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // PortAttention注意事项
  getCreateAttentionList(createPopId) {
    return fetch(`${ApiConstants.api}/getCreateAttentionList/${createPopId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  deleteAttention(id) {
    return fetch(`${ApiConstants.api}deleteAttention/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  getAttentionList(attentionPopId) {
    return fetch(`${ApiConstants.api}/getAttentionList/${attentionPopId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // PortAttentionEdit港口注意事项编辑
  getNextPopSelect(popId) {
    return fetch(`${ApiConstants.api}/getNextPopSelect/${popId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  createAttention(data) {
    return fetch(`${ApiConstants.api}createAttention`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  updateAttention(data) {
    return fetch(`${ApiConstants.api}updateAttention`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // HistoryRecord历史记录
  getHistoryRecord(data) {
    return fetch(`${ApiConstants.api}getHisDynList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // EmergenciesHistory获取突发事件列表
  getEmergenciesHistory(data) {
    return fetch(`${ApiConstants.api}getHisEventList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取动态列表
  getCurrentDynList(data) {
    return fetch(`${ApiConstants.api}getCurrentDynList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 及时性扣分
  timelinessDeductionList() {
    return fetch(`${ApiConstants.api}/timelinessDeductionList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 片区及时性扣分
  timelinessDeductionPQList() {
    return fetch(`${ApiConstants.api}/timelinessDeductionPQList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 操作扣分
  operationDeductionList() {
    return fetch(`${ApiConstants.api}/operationDeductionList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 片区操作扣分
  operationDeductionPQList() {
    return fetch(`${ApiConstants.api}/operationDeductionPQList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 获取拖轮申请列表
  getTugboatApplyList(data) {
    return fetch(`${ApiConstants.api}findApplyTugs`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 更新拖轮申请审核状态
  updateApplyTugState(data) {
    return fetch(`${ApiConstants.api}updateApplyTugState`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取引水申请列表
  findApplyDivs(data) {
    return fetch(`${ApiConstants.api}findApplyDivs`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 更新引水申请审核状态
  updateApplyDivState(data) {
    return fetch(`${ApiConstants.api}updateApplyDivState`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取修船申请列表
  getShipRepairApplyList(data) {
    return fetch(`${ApiConstants.api}getShipRepairApplyList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 更新修船申请审核状态
  updateShipRepairApplyState(data) {
    return fetch(`${ApiConstants.api}updateShipRepairApplyState`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取劳务费申请列表
  findApplyLaborFee(data) {
    return fetch(`${ApiConstants.api}findApplyLaborFee`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 更新劳务费申请审核状态
  updateApplyLaborFeeState(data) {
    return fetch(`${ApiConstants.api}updateApplyLaborFeeState`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取加油申请列表
  getFindAddOilPlans(data) {
    return fetch(`${ApiConstants.api}findAddOilPlans`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 更新加油申请审核状态
  updateAddOilPlanState(data) {
    return fetch(`${ApiConstants.api}updateAddOilPlanState`, {
      method: 'PUT',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取帮助列表
  getHandbookList(data) {
    return fetch(`${ApiConstants.api}getHandbookList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取帮助详情
  getOneHandbook(id) {
    return fetch(`${ApiConstants.api}getOneHandbook/${id}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 创建意见反馈
  createCoupleBack(data) {
    return fetch(`${ApiConstants.api}createCoupleBack`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 上传文件
  uploadFile(businessType, businessId, data) {
    return fetch(`${ApiConstants.api}uploadFile/${businessType}/${businessId}`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 到港预报获得事件
  getSchedule(data) {
    return fetch(`${ApiConstants.api}scheduleList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // MyPort港口列表
  getMyPortSelect() {
    return fetch(`${ApiConstants.api}getMyPortSelect/`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 航次调度 List
  getVoyageList(data) {
    return fetch(`${ApiConstants.api}voyagelist`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 航次调度点击航线
  findAllPopsByVoy(data) {
    return fetch(`${ApiConstants.api}findAllPopsByVoy`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  //获取航线全称list
  getShipMainLineSelectList(data) {
    return fetch(`${ApiConstants.api}getShipMainLineSelectList`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 航次调度计划本期航线
  createVoyageLine(data) {
    return fetch(`${ApiConstants.api}createVoyageLine`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 航次调度计划下期航线
  createNextVoyageLine(data) {
    return fetch(`${ApiConstants.api}createNextVoyageLine`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 航次调度删除下期航线
  deleteNextVoyageLine(data) {
    return fetch(`${ApiConstants.api}deleteNextVoyageLine`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 航次调度调整航线
  changeVoyageLine(data) {
    return fetch(`${ApiConstants.api}editVoyageLineNew`, {
      method: 'POST',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 获取航线类型列表
  getLineTypeList() {
    return fetch(`${ApiConstants.api}getLineTypeSelectList`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },
  // 获取航线港口的码头列表
  getWharfList(data) {
    return fetch(`${ApiConstants.api}getWharfSelectForPortName/${data}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 获取船舶证书
  getShipCred(shipId) {
    return fetch(`${ApiConstants.api}getShipCred/${shipId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        token: AppStorage.token,
      },
    });
  },

  // 获取突发事件记录
  getEventList(popId, range) {
    return fetch(`${ApiConstants.api}getEventList/${popId}/${range}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
        token: AppStorage.token,
      },
    });
  },

  // 获取事件类型
  findPortEventTypes(range) {
    return fetch(`${ApiConstants.api}findPortEventTypes/${range}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        token: AppStorage.token,
      },
    });
  },

  // 创建突发事件
  createEvent(data) {
    return fetch(`${ApiConstants.api}createEvent`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 编辑突发事件
  updateEvent(data) {
    return fetch(`${ApiConstants.api}updateEvent`, {
      method: 'PUT',
      headers: {
        ...header.content,
        Accept: 'application/Json',
        'Content-Type': 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  // 删除突发事件
  deleteEvent(id) {
    return fetch(`${ApiConstants.api}deleteEvent/${id}`, {
      method: 'DELETE',
      headers: {
        ...header.content,
        Accept: 'application/json',
        token: AppStorage.token,
      },
    });
  },

  // 删除突发事件
  activationNextPort(data) {
    return fetch(`${ApiConstants.api}activationNextPort`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify(data),
    });
  },

  getShipDepartmentSchedule(type, shipName) {
    return fetch(`${ApiConstants.api}shipTubeDepart`, {
      method: 'POST',
      headers: {
        ...header.content,
        Accept: 'application/json',
        token: AppStorage.token,
      },
      body: JSON.stringify({
        type,
        shipName,
      }),
    });
  },

  getWharfSelect(portId) {
    return fetch(`${ApiConstants.api}getWharfSelect/${portId}`, {
      method: 'GET',
      headers: {
        ...header.content,
        Accept: 'application/json',
        token: AppStorage.token,
      },
    });
  },
};

export default ApiFactory;
