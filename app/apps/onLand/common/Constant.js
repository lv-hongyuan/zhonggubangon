import { AsyncStorage } from 'react-native';

export const PortHostKey = 'PortHostKey';

let host = 'shipapi.zhonggu56.com';
// let host = 'zx2.wsights.com:9380';
// let host = '121.46.233.83:8180';
// let host = '172.160.30.112:8080';
// let host = '172.160.30.141:8080';

const apiVersion = 'v1';

export const ApiConstants = {
  get api() {
    const site = `http://${host}/app/port/`;
    return `${site}api/${apiVersion}/`;
  },
  get host() {
    return host;
  },
  set host(newHost) {
    host = newHost;
    AsyncStorage.setItem(PortHostKey, newHost);
  },
};

export const tugMemoReasons = [{
  text: '船舶申请，船东承担费用',
  value: 1,
}, {
  text: '船舶申请，船东承担一半费用',
  value: 2,
}, {
  text: '航运部同意，公司承担费用',
  value: 3,
}, {
  text: '港口强制要求',
  value: 4,
}, {
  text: '港口VTS临时要求',
  value: 5,
}, {
  text: '其他',
  value: 6,
}];

export function validTugMemoReason(value) {
  return (tugMemoReasons.map(item => item.value).indexOf(value) !== -1) ? value : null;
}

export const pilotReasons = [{
  text: '港口强制要求',
  value: 1,
}, {
  text: '船舶第一次靠港',
  value: 2,
}, {
  text: '船长第一次靠港',
  value: 3,
}, {
  text: '航运部直接安排引水',
  value: 4,
}, {
  text: '船舶/船东申请',
  value: 5,
}, {
  text: '其他',
  value: 6,
}];

export function validPilotReason(value) {
  return (pilotReasons.map(item => item.value).indexOf(value) !== -1) ? value : null;
}

export const lossCauseReasons = [
  '卸空下线',
  '货重不准',
  '内调空箱',
  '箱子不足',
  '货源不足',
  '赶潮水甩货',
  '中间加挂',
  '船方甩货',
  '吃水受限',
  '预留舱位',
  // '二港装货',
  // '箱子或驳船未回',
];

export const waitBerthCauses = [
  '天气因素',
  '海事交管因素',
  '船舶故障',
  '船舶集中抵港',
  '前船没有作业完工',
  '泊位长度不够',
  '被其他公司船舶插队',
  '没有引航员',
  '等潮水',
];

export const directBerthCause = [
  '等泊',
  '等货',
  '等潮水',
  '港口管制',
  '天气原因',
  '其他原因',
];

export const waitGoodsCauses = [
  '等货',
];

export const EventRange = {
  SHIP_SAILING: 11, // "航行中停航原因"
  SHIP_ANCHORING: 12, // "航行锚泊漂航原因"
  SHIP_BERTHING: 13, // "船上靠泊后"
  PORT_BERTHING: 21, // "未直靠原因"
  PORT_OPERATION: 22, // "影响作业原因"
  PORT_UN_BERTHING: 23, // "影响离泊原因"
};

export const planChangeReasons = [
  '上一港未按时离泊',
  '天气因素',
  '临时交通管制',
  '船舶未按时到港',
  '码头前船未按时离泊',
  '调整计划',
];

export const DefaultFunctionList = [{
  id: 1,
  text: '港口作业',
  sort: 100,
  appfunctionUrl: 'PortWork',
  children: [],
}, {
  id: 2,
  text: '船岸交互',
  sort: 200,
  appfunctionUrl: 'ShipLand',
  children: [{
    id: 4,
    text: '航次指令',
    sort: 210,
    appfunctionUrl: 'Instructions',
  }, {
    id: 5,
    text: '船期表',
    sort: 220,
    appfunctionUrl: 'ShipTime',
  }, {
    id: 6,
    text: '箱号查询',
    sort: 230,
    appfunctionUrl: 'BoxNumberQuery',
  },{
    id: 7,
    text: '船期表',
    sort: 225,
    appfunctionUrl: 'ShipTimeNew',
  }],
  

}, {
  id: 3,
  text: '作业分析',
  sort: 300,
  appfunctionUrl: 'WorkAnalysis',
  children: [{
    id: 9,
    text: '靠泊效率',
    sort: 310,
    appfunctionUrl: 'BerthingEfficiency',
  }, {
    id: 9,
    text: '每日船期表',
    sort: 310,
    appfunctionUrl: 'DailySchedule',
  }, {
    id: 9,
    text: '直靠率',
    sort: 310,
    appfunctionUrl: 'DirectRate',
  }, {
    id: 9,
    text: '在港停时',
    sort: 310,
    appfunctionUrl: 'PortTime',
  }, {
    id: 9,
    text: '靠泊计划',
    sort: 310,
    appfunctionUrl: 'BerthingPlan',
  }, {
    id: 9,
    text: '公告栏',
    sort: 310,
    appfunctionUrl: 'BulletinBoard',
  }, {
    id: 9,
    text: 'APP考核',
    sort: 310,
    appfunctionUrl: 'Assessment',
  },{
      id: 9,
      text: '考核公示',
      sort: 310,
      appfunctionUrl: 'AssessmentPublicity',
    },{
    id: 9,
    text: '每日实装',
    sort: 310,
    appfunctionUrl: 'DailyActualLoading',
  }, {
    id: 9,
    text: '每日预配',
    sort: 310,
    appfunctionUrl: 'DailyProvisioning',
  }],
}];

export function getTugboatUseWayName(useWay) {
  switch (useWay) {
    case 10:
      return '靠泊';
    case 20:
      return '离泊';
    case 30:
      return '移泊';
    default:
      return '移泊';
  }
}

export function getTugboatApplyType(applyType) {
  switch (applyType) {
    case 1:
      return '大风影响';
    case 2:
      return '港口强制';
    case 3:
      return '潮水因素';
    case 4:
      return '码头条件';
    default:
      return '移泊';
  }
}

export const ReviewState = {
  UnderReviewed: 10,
  Approved: 20,
  Dismissed: 30,
};

export function reviewTitleFromState(state) {
  switch (state) {
    case ReviewState.UnderReviewed:
      return '审批中';
    case ReviewState.Approved:
      return '已批准';
    case ReviewState.Dismissed:
      return '已驳回';
    default:
      return '未审批';
  }
}

export const getLineType = [
  '主干线', '支线','全部'
]
export const getRegionArr = [
  '全部','东北','华北','山东','华中','华东','长江','东南','华南','西南'
]

export const ReadState = {
  unRead: 0,
  read: 1,
  allState: 2,
};

export function reviewReadFromState(state) {
  switch (state) {
    case ReadState.unRead:
      return '未读';
    case ReadState.read:
      return '已读';
    case ReadState.allState:
      return '全部';
    default:
      return '未知状态';
  }
}
