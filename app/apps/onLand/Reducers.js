import portWorkReducer from './containers/PortWork/reducer';
import portWorkDetailReducer from './containers/PortWorkDetail/reducer';
import sailNewspaperReducer from './containers/SailNewspaper/reducer';
import emergenciesReducer from './containers/Emergencies/reducer';
import emergencyEditReducer from './containers/EmergencyEdit/reducer';
import instructionsReducer from './containers/Instructions/reducer';
import berthingEfficiencyReducer from './containers/BerthingEfficiency/reducer';
import portTimeReducer from './containers/PortTime/reducer';
import portTimeRankingReducer from './containers/PortTimeRanking/reducer';
import shipTimeReducer from './containers/ShipTime/reducer';
import shipTimeNewLeftReducer from './containers/ShipTimeNewLeft/reducer';
import shipTimeNewRightReducer from './containers/ShipTimeNewRight/reducer';
import shipTimeNewDetailReducer from './containers/ShipTimeNewDetail/reducer';
import shipTimeDetailReducer from './containers/ShipTimeDetail/reducer';
import historyRecordReducer from './containers/HistoryRecord/reducer';
import historyRecordDetailReducer from './containers/HistoryRecordDetail/reducer';
import historyNewsPaperReducer from './containers/HistoryNewsPaper/reducer';
import emergenciesHistoryReducer from './containers/EmergenciesHistory/reducer';
import shipInformationReducer from './containers/ShipInformation/reducer';
import portAttentionsReducer from './containers/PortAttention/reducer';
import portAttentionEditReducer from './containers/PortAttentionEdit/reducer';
import berthingPlanReducer from './containers/BerthingPlan/reducer';
import assessmentReducer from './containers/Assessment/reducer';
import assessmentPublicityReducer from './containers/AssessmentPublicity/reducer'
import assessmentClassReducer from './containers/AssessmentClass/reducer';
import dailyScheduleReducer from './containers/DailySchedule/reducer';
import dailyActualLoadingReducer from './containers/DailyActualLoading/reducer';
import drawingOutFreeReducer from './containers/DrawingOutFee/reducer';
import dailyProvisioningReducer from './containers/DailyProvisioning/reducer';
import bulletinBoardReducer from './containers/BulletinBoard/reducer';
import bulletinContentReducer from './containers/BulletinContent/reducer';
import directRateReducer from './containers/DirectRate/reducer';
import directRateRankingReducer from './containers/DirectRateRanking/reducer';
import boxNumberQueryReducer from './containers/BoxNumberQuery/reducer';
import permissionsPromptReducer from './containers/PermissionsPrompt/reducer';
import popListReducer from './containers/PopList/reducer';
import tugboatApplyListReducer from './containers/TugboatApplyList/reducer';
import pilotApplyListReducer from './containers/PilotApplyList/reducer';
import shipRepairApplyListReducer from './containers/ShipRepairApplyList/reducer';
import addOilApplyReducer from './containers/AddOilApply/reducer';
import serviceFeeApplyListReducer from './containers/ServiceFeeApplyList/reducer';
import scheduleReducer from './containers/Schedule/reducer';
import voyageDispatchReducer from './containers/VoyageDispatch/reducer';
import voyageEditReducer from './containers/VoyageEdit/reducer';
import voyageLineEditReducer from './containers/VoyageLineEdit/reducer';
import shipCertificateReducer from './containers/ShipCertificate/reducer';
import activationNextPortReducer from './containers/ActivationNextPort/reducer';
import shipDepartmentScheduleReducer from './containers/ShipDepartmentSchedule/reducer';
import wharfPlaneRudecer from './containers/WharfPlane/reducer';
import taskProcessReducer from './containers/TaskProcess/reducer';
import workAnalysisReducer from './containers/WorkAnalysis/reducer';


const reducers = {
  portWork: portWorkReducer,
  portWorkDetail: portWorkDetailReducer,
  sailNewspaper: sailNewspaperReducer,
  emergencies: emergenciesReducer,
  instructions: instructionsReducer,
  emergencyEdit: emergencyEditReducer,
  berthingEfficiency: berthingEfficiencyReducer,
  shipTime: shipTimeReducer,
  shipTimeNewLeft:shipTimeNewLeftReducer,
  shipTimeNewRight:shipTimeNewRightReducer,
  shipTimeNewDetail:shipTimeNewDetailReducer,
  shipTimeDetail: shipTimeDetailReducer,
  historyRecord: historyRecordReducer,
  historyRecordDetail: historyRecordDetailReducer,
  historyNewsPaper: historyNewsPaperReducer,
  emergenciesHistory: emergenciesHistoryReducer,
  portTime: portTimeReducer,
  shipInformation: shipInformationReducer,
  portAttentions: portAttentionsReducer,
  portAttentionEdit: portAttentionEditReducer,
  berthingPlan: berthingPlanReducer,
  assessment: assessmentReducer,
  assessmentPublicity: assessmentPublicityReducer,
  assessmentClass: assessmentClassReducer,
  dailySchedule: dailyScheduleReducer,
  dailyActualLoading: dailyActualLoadingReducer,
  dailyProvisioning: dailyProvisioningReducer,
  drawingOutFree: drawingOutFreeReducer,
  bulletinBoard: bulletinBoardReducer,
  bulletinContent: bulletinContentReducer,
  portTimeRanking: portTimeRankingReducer,
  directRate: directRateReducer,
  directRateRanking: directRateRankingReducer,
  boxNumberQuery: boxNumberQueryReducer,
  permissionsPrompt: permissionsPromptReducer,
  popList: popListReducer,
  tugboatApplyList: tugboatApplyListReducer,
  pilotApplyList: pilotApplyListReducer,
  shipRepairApplyList: shipRepairApplyListReducer,
  addOilApply: addOilApplyReducer,
  serviceFeeApplyList: serviceFeeApplyListReducer,
  schedule: scheduleReducer,
  voyageDispatch: voyageDispatchReducer,
  voyageEdit: voyageEditReducer,
  voyageLineEdit: voyageLineEditReducer,
  shipCertificate: shipCertificateReducer,
  activationNextPort: activationNextPortReducer,
  shipDepartmentSchedule: shipDepartmentScheduleReducer,
  wharfPlane: wharfPlaneRudecer,
  taskProcess:taskProcessReducer,
  workAnalysis: workAnalysisReducer
};

export default reducers;
