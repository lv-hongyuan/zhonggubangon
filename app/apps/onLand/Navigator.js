import React from 'react';
import { AppState, NetInfo } from 'react-native';
import { createBottomTabNavigator, NavigationActions } from 'react-navigation';
import { createStackNavigator, StackViewStyleInterpolator } from 'react-navigation-stack';
import HeaderButtons from 'react-navigation-header-buttons';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import PropTypes from 'prop-types';
import Svg from '../../components/Svg';

import PortWork from './containers/PortWork';
import PortWorkDetail from './containers/PortWorkDetail';
import SailNewspaper from './containers/SailNewspaper';
import LoadingInfoEdit from './containers/LoadingInfoEdit';
import Emergencies from './containers/Emergencies';
import EmergencyEdit from './containers/EmergencyEdit';
import ShipInformation from './containers/ShipInformation';
import ShipCertificate from './containers/ShipCertificate';
import PortAttention from './containers/PortAttention';
import PortAttentionEdit from './containers/PortAttentionEdit';
import HistoryDetail from './containers/HistoryDetail';
import HistoryRecord from './containers/HistoryRecord';
import HistoryRecordDetail from './containers/HistoryRecordDetail';
import HistoryNewsPaper from './containers/HistoryNewsPaper';
import EmergenciesHistory from './containers/EmergenciesHistory';

import PopList from './containers/PopList';
import PopDetail from './containers/PopDetail';

import ShipLand from './containers/ShipLand';
import Instructions from './containers/Instructions';
import ShipTime from './containers/ShipTime';
import ShipTimeNew from './containers/ShipTimeNew';
import ShipTimeNewLeft from './containers/ShipTimeNewLeft';
import ShipTimeNewRight from './containers/ShipTimeNewRight';
import ShipTimeNewDetail from './containers/ShipTimeNewDetail';
import ShipTImeNewSearch from './containers/ShipTimeNewSearch';
import ShipTimeDetail from './containers/ShipTimeDetail';
import BoxNumberQuery from './containers/BoxNumberQuery';
import TugboatApplyList from './containers/TugboatApplyList';
import PilotApplyList from './containers/PilotApplyList';
import ShipRepairApplyList from './containers/ShipRepairApplyList';
import ServiceFeeApplyList from './containers/ServiceFeeApplyList';
import AddOilApply from './containers/AddOilApply';
import Schedule from './containers/Schedule';
import VoyageDispatch from './containers/VoyageDispatch';
import VoyageEdit from './containers/VoyageEdit';
import VoyageLineEdit from './containers/VoyageLineEdit';
import ActivationNextPort from './containers/ActivationNextPort';
import ShipDepartmentSchedule from './containers/ShipDepartmentSchedule';

import WorkAnalysis from './containers/WorkAnalysis';
import BerthingEfficiency from './containers/BerthingEfficiency';
import PortTime from './containers/PortTime';
import PortTimeRanking from './containers/PortTimeRanking';
import BerthingPlan from './containers/BerthingPlan';
import Assessment from './containers/Assessment';
import AssessmentPublicity from './containers/AssessmentPublicity';
import AssessmentClass from './containers/AssessmentClass';
import DailySchedule from './containers/DailySchedule';
import DailyActualLoading from './containers/DailyActualLoading';
import DailyProvisioning from './containers/DailyProvisioning';
import BulletinBoard from './containers/BulletinBoard';
import BulletinContent from './containers/BulletinContent';
import DirectRate from './containers/DirectRate';
import DirectRateRanking from './containers/DirectRateRanking';

import PermissionsPrompt from './containers/PermissionsPrompt';

import DrawingOutFee from "./containers/DrawingOutFee";
import WharfPlane from './containers/WharfPlane'
import TaskProcess from './containers/TaskProcess'
import TaskProcessDetailNotification from './containers/TaskProcessDetailNotification'
import SeaMap from './containers/SeaMap';

import {
  ROUTE_PORT_WORK,
  ROUTE_PORT_WORK_DETAIL,
  ROUTE_PORT_SAIL_NEWSPAPER,
  ROUTE_PORT_LOADING_INFO_EDIT,
  ROUTE_EMERGENCIES,
  ROUTE_EMERGENCY_EDIT,
  ROUTE_SHIP_LAND,
  ROUTE_INSTRUCTIONS,
  ROUTE_WORK_ANALYSIS,
  ROUTE_PORT_WORK_STACK,
  ROUTE_SHIP_LAND_STACK,
  ROUTE_WORK_ANALYSIS_STACK,
  ROUTE_SHIP_TIME_DETAIL,
  ROUTE_SHIP_TIME,
  ROUTE_SHIP_TIME_NEW,
  ROUTE_SHIP_TIME_NEW_LEFT,
  ROUTE_SHIP_TIME_NEW_DETAIL,
  ROUTE_SHIP_TIME_NEW_RIGHT,
  ROUTE_SHIP_TIME_NEW_SEARCH,
  ROUTE_EMERGENCIES_HISTORY,
  ROUTE_HISTORY_RECORD,
  ROUTE_HISTORY_RECORD_DETAIL,
  ROUTE_HISTORY_NEWSPAPER,
  ROUTE_BERTHING_EFFICIENCY,
  ROUTE_PORT_TIME,
  ROUTE_PORT_TIME_RANKING,
  ROUTE_SHIP_INFORMATION,
  ROUTE_PORT_ATTENTION,
  ROUTE_PORT_ATTENTION_EDIT,
  ROUTE_BERTHING_PLAN,
  ROUTE_ASSESSMENT,
  ROUTE_ASSESSMENT_PUBLICITY,
  ROUTE_ASSESSMENT_CLASS,
  ROUTE_DAILY_SCHEDULE,
  ROUTE_DAILY_ACTUAL_LOADING,
  ROUTE_DAILY_PROVISIONING,
  ROUTE_BULLETIN_BOARD,
  ROUTE_BULLETIN_CONTENT,
  ROUTE_DIRECT_RATE,
  ROUTE_DIRECT_RATE_RANKING,
  ROUTE_BOX_NUMBER_QUERY,
  ROUTE_PERMISSION_PROMPT,
  ROUTE_HISTORY_DETAIL,
  ROUTE_POP_LIST,
  ROUTE_POP_DETAIL,
  ROUTE_PORT_WORK_SUMMARY_STACK,
  ROUTE_PERMISSION_PROMPT_STACK,
  ROUTE_TUGBOAT_APPLY_LIST,
  ROUTE_PILOT_APPLY_LIST,
  ROUTE_SHIP_REPAIR_APPLY_LIST,
  ROUTE_SERVICE_FEE_APPLY_LIST,
  ROUTE_ADD_OIL_APPLY,
  ROUTE_SCHEDULE,
  ROUTE_VOYAGE_DISPATCH,
  ROUTE_VOYAGE_EDIT,
  ROUTE_VOYAGE_LINE_EDIT,
  ROUTE_SHIP_CERTIFICATE,
  ROUTE_ACTIVATION_NEXT_PORT,
  ROUTE_SHIP_DEPARTMENT_SCHEDULE,
  ROUTE_DRAWING_OUT_FEE,
  ROUTE_WHARF_PLANE,
  ROUTE_TASK_PROCESS,
  ROUTE_TASKPROCESS_DETAIL_NOTIFICATION,
  ROUTE_SEA_MAP,
} from './RouteConstant';
import { getFunctionListPromise } from './containers/PermissionsPrompt/actions';
import { makeFunctionList, makeLoading } from './containers/PermissionsPrompt/selectors';

const defaultNavigationOptions = ({ navigation }) => ({
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: '#DC001B',
  },
  headerBackTitle: null,
  headerTitleStyle: {
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
  },
  headerTitleAllowFontScaling: false,//禁用字体跟随系统字体大小变化
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={() => {
          navigation.dispatch(NavigationActions.back());
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons />
  ),
});

const PortWorkNavigator = createStackNavigator({
  [ROUTE_PORT_WORK]: { screen: PortWork },
  [ROUTE_PORT_WORK_DETAIL]: { screen: PortWorkDetail },
  [ROUTE_PORT_SAIL_NEWSPAPER]: { screen: SailNewspaper },
  [ROUTE_PORT_LOADING_INFO_EDIT]: { screen: LoadingInfoEdit },
  [ROUTE_EMERGENCIES]: { screen: Emergencies },
  [ROUTE_EMERGENCY_EDIT]: { screen: EmergencyEdit },
  [ROUTE_SHIP_INFORMATION]: { screen: ShipInformation },
  [ROUTE_SHIP_CERTIFICATE]: { screen: ShipCertificate },
  [ROUTE_PORT_ATTENTION]: { screen: PortAttention },
  [ROUTE_PORT_ATTENTION_EDIT]: { screen: PortAttentionEdit },
  [ROUTE_HISTORY_DETAIL]: { screen: HistoryDetail },
  [ROUTE_HISTORY_RECORD]: { screen: HistoryRecord },
  [ROUTE_HISTORY_RECORD_DETAIL]: { screen: HistoryRecordDetail },
  [ROUTE_HISTORY_NEWSPAPER]: { screen: HistoryNewsPaper },
  [ROUTE_EMERGENCIES_HISTORY]: { screen: EmergenciesHistory },
}, {
    initialRouteKey: ROUTE_PORT_WORK,
    initialRouteName: ROUTE_PORT_WORK,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });

const PortWorkSummaryStackNavigator = createStackNavigator({
  [ROUTE_POP_LIST]: { screen: PopList },
  [ROUTE_POP_DETAIL]: { screen: PopDetail },
  [ROUTE_PORT_LOADING_INFO_EDIT]: { screen: LoadingInfoEdit },
  [ROUTE_SHIP_INFORMATION]: { screen: ShipInformation },
  [ROUTE_PORT_ATTENTION]: { screen: PortAttention },
  [ROUTE_PORT_ATTENTION_EDIT]: { screen: PortAttentionEdit },
  [ROUTE_HISTORY_RECORD]: { screen: HistoryRecord },
  [ROUTE_HISTORY_RECORD_DETAIL]: { screen: HistoryRecordDetail },
  [ROUTE_HISTORY_NEWSPAPER]: { screen: HistoryNewsPaper },
  [ROUTE_EMERGENCIES_HISTORY]: { screen: EmergenciesHistory },
}, {
    initialRouteKey: ROUTE_POP_LIST,
    initialRouteName: ROUTE_POP_LIST,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });

class PortWorkSummaryNavigator extends React.Component {
  static router = PortWorkSummaryStackNavigator.router;
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  render() {
    const { navigation } = this.props;
    return <PortWorkSummaryStackNavigator navigation={navigation} screenProps={{ summary: true }} />;
  }
}

const ShipLandNavigator = createStackNavigator({
  [ROUTE_SHIP_LAND]: { screen: ShipLand },
  [ROUTE_INSTRUCTIONS]: { screen: Instructions },
  [ROUTE_SHIP_TIME]: { screen: ShipTime },
  [ROUTE_SHIP_TIME_NEW]: { screen: ShipTimeNew },
  [ROUTE_SHIP_TIME_NEW_LEFT]: { screen: ShipTimeNewLeft },
  [ROUTE_SHIP_TIME_NEW_RIGHT]: { screen: ShipTimeNewRight },
  [ROUTE_SHIP_TIME_NEW_DETAIL]: { screen: ShipTimeNewDetail },
  [ROUTE_SHIP_TIME_NEW_SEARCH]: { screen: ShipTImeNewSearch },
  [ROUTE_SHIP_TIME_DETAIL]: { screen: ShipTimeDetail },
  [ROUTE_BOX_NUMBER_QUERY]: { screen: BoxNumberQuery },
  [ROUTE_TUGBOAT_APPLY_LIST]: { screen: TugboatApplyList },
  [ROUTE_PILOT_APPLY_LIST]: { screen: PilotApplyList },
  [ROUTE_SHIP_REPAIR_APPLY_LIST]: { screen: ShipRepairApplyList },
  [ROUTE_SERVICE_FEE_APPLY_LIST]: { screen: ServiceFeeApplyList },
  [ROUTE_ADD_OIL_APPLY]: { screen: AddOilApply },
  [ROUTE_SCHEDULE]: { screen: Schedule },
  [ROUTE_VOYAGE_DISPATCH]: { screen: VoyageDispatch },
  [ROUTE_VOYAGE_EDIT]: { screen: VoyageEdit },
  [ROUTE_VOYAGE_LINE_EDIT]: { screen: VoyageLineEdit },
  [ROUTE_ACTIVATION_NEXT_PORT]: { screen: ActivationNextPort },
  [ROUTE_SHIP_DEPARTMENT_SCHEDULE]: { screen: ShipDepartmentSchedule },
  [ROUTE_SEA_MAP]: { screen: SeaMap },
}, {
    initialRouteKey: ROUTE_SHIP_LAND,
    initialRouteName: ROUTE_SHIP_LAND,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });

const WorkAnalysisNavigator = createStackNavigator({
  [ROUTE_WORK_ANALYSIS]: { screen: WorkAnalysis },
  [ROUTE_BERTHING_EFFICIENCY]: { screen: BerthingEfficiency },
  [ROUTE_PORT_TIME]: { screen: PortTime },
  [ROUTE_PORT_TIME_RANKING]: { screen: PortTimeRanking },
  [ROUTE_BERTHING_PLAN]: { screen: BerthingPlan },
  [ROUTE_ASSESSMENT]: { screen: Assessment },
  [ROUTE_ASSESSMENT_PUBLICITY]: { screen: AssessmentPublicity },
  [ROUTE_ASSESSMENT_CLASS]: { screen: AssessmentClass },
  [ROUTE_DAILY_SCHEDULE]: { screen: DailySchedule },
  [ROUTE_DAILY_ACTUAL_LOADING]: { screen: DailyActualLoading },
  [ROUTE_DAILY_PROVISIONING]: { screen: DailyProvisioning },
  [ROUTE_BULLETIN_BOARD]: { screen: BulletinBoard },
  [ROUTE_BULLETIN_CONTENT]: { screen: BulletinContent },
  [ROUTE_DIRECT_RATE]: { screen: DirectRate },
  [ROUTE_DIRECT_RATE_RANKING]: { screen: DirectRateRanking },
  [ROUTE_DRAWING_OUT_FEE]: { screen: DrawingOutFee },
  [ROUTE_WHARF_PLANE]: { screen: WharfPlane },
  [ROUTE_TASK_PROCESS]: { screen: TaskProcess },
  [ROUTE_TASKPROCESS_DETAIL_NOTIFICATION]: { screen: TaskProcessDetailNotification },
}, {
    initialRouteKey: ROUTE_WORK_ANALYSIS,
    initialRouteName: ROUTE_WORK_ANALYSIS,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });

const PermissionsPromptNavigator = createStackNavigator({
  [ROUTE_PERMISSION_PROMPT]: { screen: PermissionsPrompt },
}, {
    initialRouteKey: ROUTE_PERMISSION_PROMPT,
    initialRouteName: ROUTE_PERMISSION_PROMPT,
    transitionConfig: () => ({
      screenInterpolator: StackViewStyleInterpolator.forHorizontal,
    }),
    navigationOptions: defaultNavigationOptions,
  });

const HomeNavigator = createBottomTabNavigator(
  {
    [ROUTE_PERMISSION_PROMPT_STACK]: {
      screen: PermissionsPromptNavigator,
      navigationOptions: {
        tabBarVisible: false,
      },
    },
    [ROUTE_PORT_WORK_STACK]: {
      screen: PortWorkNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '港口作业',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_portwork" size="24" color={tintColor} />
        ),
      }),
    },
    [ROUTE_PORT_WORK_SUMMARY_STACK]: {
      screen: PortWorkSummaryNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '港口作业',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_portwork" size="24" color={tintColor} />
        ),
      }),
    },
    [ROUTE_SHIP_LAND_STACK]: {
      screen: ShipLandNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '船岸交互',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_shipLand" size="24" color={tintColor} />
        ),
      }),
    },
    [ROUTE_WORK_ANALYSIS_STACK]: {
      screen: WorkAnalysisNavigator,
      navigationOptions: ({ navigation }) => ({
        tabBarVisible: navigation.state.index === 0,
        tabBarLabel: '作业分析',
        // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ tintColor }) => (
          <Svg icon="onLand_workAnalysis" size="24" color={tintColor} />
        ),
      }),
    },
  },
  {
    initialRouteKey: ROUTE_PERMISSION_PROMPT_STACK,
    initialRouteName: ROUTE_PERMISSION_PROMPT_STACK,
    swipeEnabled: false,
    animationEnabled: false,
    backBehavior: 'none',
    tabBarOptions: {
      allowFontScaling: false,
      activeTintColor: '#DC001B',
      inactiveTintColor: '#838383',
      showLabel: true,
      showIcon: true,
      style: {
        backgroundColor: 'white',
      },
    },
  },
);

class Navigator extends React.Component {
  static router = {
    ...HomeNavigator.router,
    getStateForAction: (action, lastState) => HomeNavigator.router.getStateForAction(action, lastState)
    ,
  };
  static propTypes = {
    functionList: PropTypes.array,
    navigation: PropTypes.object.isRequired,
    getFunctionList: PropTypes.func.isRequired,
  };
  static defaultProps = {
    functionList: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    this.getFunctionList();

    // 监听网络
    NetInfo.isConnected.addEventListener('connectionChange', this.netChange);
    AppState.addEventListener('change', this.handleAppStateChange);
  }

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.netChange);
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  getFunctionList() {
    this.props.getFunctionList()
      .then(() => { })
      .catch((error) => {
        console.log(error);
      });
  }

  // 网络监控
  netChange = (isConnect) => {
    if (this.state.isConnect !== undefined && isConnect && !this.state.isConnect) {
      this.getFunctionList();
    }
    this.setState({ isConnect });
  };

  // app状态切换
  handleAppStateChange = (nextAppState) => {
    // App has come to the foreground!
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      this.getFunctionList();
    }
    this.setState({ appState: nextAppState });
  };

  render() {
    const { navigation } = this.props;
    const { routes, index } = navigation.state;
    const filteredRoutes = routes.filter(({ routeName }) => this.props.functionList.indexOf(routeName) !== -1);
    const filteredIndex = Math.max(0, filteredRoutes.findIndex(({ routeName }) => routeName === routes[index].routeName));
    if (filteredRoutes.length === 0) {
      filteredRoutes.push(routes[0]);
    }
    return (
      <HomeNavigator
        navigation={{
          ...navigation,
          state: {
            ...navigation.state,
            routes: filteredRoutes,
            index: filteredIndex,
          },
        }}
      />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  functionList: makeFunctionList(),
  isLoading: makeLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getFunctionList: getFunctionListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigator);
