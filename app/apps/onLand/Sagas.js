import { call, put, select } from 'redux-saga/effects';
import portWorkSagas from './containers/PortWork/sagas';
import portWorkDetailSagas from './containers/PortWorkDetail/sagas';
import emergenciesSagas from './containers/Emergencies/sagas';
import emergencyEditSagas from './containers/EmergencyEdit/sagas';
import instructionsSagas from './containers/Instructions/sagas';
import sailNewspaperSagas from './containers/SailNewspaper/sagas';
import berthingEfficiencySagas from './containers/BerthingEfficiency/sagas';
import portTimeSagas from './containers/PortTime/sagas';
import PortTimeRankingSagas from './containers/PortTimeRanking/sagas';
import shipTimeSagas from './containers/ShipTime/sagas';
import shipTimeDetailSagas from './containers/ShipTimeDetail/sagas';
import shipTimeNewLeftSagas from './containers/ShipTimeNewLeft/sagas';
import shipTimeNewRightSagas from './containers/ShipTimeNewRight/sagas';
import ShipTimeNewDetailSagas from './containers/ShipTimeNewDetail/sagas';
import historyRecordSagas from './containers/HistoryRecord/sagas';
import historyRecordDetailSagas from './containers/HistoryRecordDetail/sagas';
import historyNewsPaperSagas from './containers/HistoryNewsPaper/sagas';
import emergenciesHistorySagas from './containers/EmergenciesHistory/sagas';
import shipInformationSagas from './containers/ShipInformation/sagas';
import portAttentionSagas from './containers/PortAttention/sagas';
import portAttentionEditSagas from './containers/PortAttentionEdit/sagas';
import BerthingPlanSagas from './containers/BerthingPlan/sagas';
import AssessmentSagas from './containers/Assessment/sagas';
import AssessmentPublicitySagas from './containers/AssessmentPublicity/sagas';
import AssessmentClassSagas from './containers/AssessmentClass/sagas';
import DailyScheduleSagas from './containers/DailySchedule/sagas';
import DailyActualLoadingSagas from './containers/DailyActualLoading/sagas';
import DailyProvisioningSagas from './containers/DailyProvisioning/sagas';
import DrawingOutFeeSagas from "./containers/DrawingOutFee/sagas";
import BulletinBoardSagas from './containers/BulletinBoard/sagas';
import BulletinContentSagas from './containers/BulletinContent/sagas';
import DirectRateSagas from './containers/DirectRate/sagas';
import DirectRateRankingSagas from './containers/DirectRateRanking/sagas';
import BoxNumberQuerySagas from './containers/BoxNumberQuery/sagas';
import PermissionsPromptSagas from './containers/PermissionsPrompt/sagas';
import PopListSagas from './containers/PopList/sagas';
import TugboatApplyListSagas from './containers/TugboatApplyList/sagas';
import PilotApplyListSagas from './containers/PilotApplyList/sagas';
import ShipRepairApplyListSagas from './containers/ShipRepairApplyList/sagas';
import ServiceFeeApplyListSagas from './containers/ServiceFeeApplyList/sagas';
import AddOilApplySagas from './containers/AddOilApply/sagas';
import ScheduleSagas from './containers/Schedule/sagas';
import VoyageDispatchSagas from './containers/VoyageDispatch/sagas';
import VoyageEditSagas from './containers/VoyageEdit/sagas';
import VoyageLineEditSagas from './containers/VoyageLineEdit/sagas';
import ShipCertificateSagas from './containers/ShipCertificate/sagas';
import ActivationNextPortSagas from './containers/ActivationNextPort/sagas';
import ShipDepartmentScheduleSagas from './containers/ShipDepartmentSchedule/sagas';
import WharfPlaneSagas from './containers/WharfPlane/sagas';
import TaskProcessSagas from './containers/TaskProcess/sagas';
import WorkAnalysisSagas from './containers/WorkAnalysis/sagas'

import request from '../../utils/request';
import ApiFactory from './common/Api';
import { getInstructionListRoutine } from './containers/Instructions/actions';
import { makePortWorkBadge } from './containers/PortWork/selectors';

const allSagas = [
  ...portWorkSagas,
  ...portWorkDetailSagas,
  ...emergenciesSagas,
  ...emergencyEditSagas,
  ...instructionsSagas,
  ...sailNewspaperSagas,
  ...berthingEfficiencySagas,
  ...shipTimeSagas,
  ...shipTimeNewLeftSagas,
  ...shipTimeNewRightSagas,
  ...shipTimeDetailSagas,
  ...ShipTimeNewDetailSagas,
  ...historyRecordSagas,
  ...historyRecordDetailSagas,
  ...historyNewsPaperSagas,
  ...emergenciesHistorySagas,
  ...portTimeSagas,
  ...shipInformationSagas,
  ...portAttentionSagas,
  ...portAttentionEditSagas,
  ...BerthingPlanSagas,
  ...AssessmentSagas,
  ...AssessmentPublicitySagas,
  ...AssessmentClassSagas,
  ...DailyScheduleSagas,
  ...DailyActualLoadingSagas,
  ...DailyProvisioningSagas,
  ...DrawingOutFeeSagas,
  ...BulletinBoardSagas,
  ...BulletinContentSagas,
  ...PortTimeRankingSagas,
  ...DirectRateSagas,
  ...DirectRateRankingSagas,
  ...BoxNumberQuerySagas,
  ...PermissionsPromptSagas,
  ...PopListSagas,
  ...TugboatApplyListSagas,
  ...PilotApplyListSagas,
  ...ShipRepairApplyListSagas,
  ...ServiceFeeApplyListSagas,
  ...AddOilApplySagas,
  ...ScheduleSagas,
  ...VoyageDispatchSagas,
  ...VoyageEditSagas,
  ...VoyageLineEditSagas,
  ...ShipCertificateSagas,
  ...ActivationNextPortSagas,
  ...ShipDepartmentScheduleSagas,
  ...WharfPlaneSagas,
  ...TaskProcessSagas,
  ...WorkAnalysisSagas,
];

export default allSagas;

// export function* getPortWorkBadgeSaga() {
//   try {
//     const response = yield call(request, ApiFactory.getInstructions());
//     console.log('getNoticeList', response.toString());
//     yield put(getInstructionListRoutine.success(response));
//     return yield select(makePortWorkBadge());
//   } catch (e) {
//     throw e;
//   }
// }
