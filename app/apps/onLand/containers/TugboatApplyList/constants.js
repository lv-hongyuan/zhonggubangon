/*
 *
 * TugboatApplyList constants
 *
 */
export const GET_TUGBOAT_APPLY_LIST = 'onLand/TugboatApplyList/GET_TUGBOAT_APPLY_LIST';
export const REVIEW_TUGBOAT_APPLY = 'onLand/TugboatApplyList/REVIEW_TUGBOAT_APPLY';
