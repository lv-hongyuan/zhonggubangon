import { RefreshState } from '../../../../components/RefreshListView';
import { getTugboatApplyListRoutine, reviewTugboatApplyRoutine } from './actions';

const defaultState = {
  list: [],
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取申请列表
    case getTugboatApplyListRoutine.TRIGGER: {
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getTugboatApplyListRoutine.SUCCESS: {
      const { page, pageSize, list ,showAudit} = action.payload;
      console.log('、、showAudit',showAudit)
      return {
        showAudit,
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getTugboatApplyListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getTugboatApplyListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 审批申请
    case reviewTugboatApplyRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case reviewTugboatApplyRoutine.SUCCESS:
      const { id } = action.payload;
      const newList = (state.list || []).filter(item => item.id !== id);
      console.log('+++item',item)
      return { ...state, list: newList };
    case reviewTugboatApplyRoutine.FAILURE:
      return { ...state, loadingError: action.payload };
    case reviewTugboatApplyRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
