import React from 'react';
import PropTypes from 'prop-types';
import {DeviceEventEmitter, StyleSheet} from 'react-native';
import {
  Button, Label, Text, View,
} from 'native-base';
import _ from 'lodash';
import myTheme from '../../../../../Themes';
import { getTugboatApplyType, getTugboatUseWayName, ReviewState } from '../../../common/Constant';
import Svg from '../../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
  },
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  btnContainer: {
    height: 60,
    borderColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
  },
  leftButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class TugboatApplyItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        tugAudit: 1,
    };
  }
  //修改审核权限如果是0显示审批按钮其他的隐藏
  renderReviewState(item) {
      switch (item.state) {
          case ReviewState.UnderReviewed:
              if (this.state.tugAudit == 0){
              return (
                  <View style={styles.btnContainer}>
                      <Button
                          block
                          style={[styles.leftButton, { backgroundColor: '#FBB03B' }]}
                          onPress={() => {
                              this.props.onApproved(item);
                          }}
                      >
                          <Text style={styles.buttonText}>批准</Text>
                      </Button>
                      <Button
                          block
                          style={[styles.rightButton, { backgroundColor: '#DC001B' }]}
                          onPress={() => {
                              this.props.onDismissed(item);
                          }}
                      >
                          <Text style={styles.buttonText}>驳回</Text>
                      </Button>
                  </View>
              );}else{
                return null;
              }
          case ReviewState.Approved:
              return (
                  <Svg
                      icon="approved"
                      size={150}
                      style={{
                          position: 'absolute',
                          bottom: 0,
                          right: 20,
                          zIndex: 1,
                          opacity: 0.5,
                      }}
                  />
              );
          case ReviewState.Dismissed:
              return (
                  <Svg
                      icon="dismissed"
                      size={150}
                      style={{
                          position: 'absolute',
                          bottom: 0,
                          right: 20,
                          zIndex: 1,
                          opacity: 0.5,
                      }}
                  />
              );
          default:
              return null;
      }
  }

  componentDidMount(){
      this.listener = DeviceEventEmitter.addListener('addShowAudit',(param)=>{
           console.log('GetTugBoatParam',param.audit);
           this.setState({tugAudit:param.audit})
          console.log('hhhhhh',this.state.audit)
           });
  }

    componentWillUnmount(){
      this.listener.remove();
    }
    //有数据改变的时候调用此方法
    render() {
    const item = this.props.item || {};
    const {
      shipName, voyageCode, portName, tugNum, useWay, applyType, applyReasons, rejectReasons,
    } = item;
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Label style={styles.label}>船名:</Label>
          <Text style={styles.text}>{shipName}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>航次:</Label>
          <Text style={styles.text}>{voyageCode}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>发生港口:</Label>
          <Text style={styles.text}>{portName}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>使用途径:</Label>
          <Text style={styles.text}>{getTugboatUseWayName(useWay)}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>拖轮数量:</Label>
          <Text style={styles.text}>{tugNum}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>申请原因类型:</Label>
          <Text style={styles.text}>{getTugboatApplyType(applyType)}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>申请原因:</Label>
          <Text style={styles.text}>{applyReasons}</Text>
        </View>
        {!_.isEmpty(rejectReasons) && (
          <View style={styles.row}>
            <Label style={styles.label}>审批描述:</Label>
            <Text style={styles.text}>{rejectReasons}</Text>
          </View>
        )}
          {this.renderReviewState(item)}
      </View>
    );
  }
}

TugboatApplyItem.propTypes = {
  item: PropTypes.object.isRequired,
  onApproved: PropTypes.func.isRequired,
  onDismissed: PropTypes.func.isRequired,
};

export default TugboatApplyItem;
