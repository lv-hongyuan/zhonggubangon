import { createSelector } from 'reselect/es';

const selectTugboatApplyListDomain = () => state => state.tugboatApplyList;

const makeSelectEmergencies = () => createSelector(selectTugboatApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectTugboatApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectTugboatApplyListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeIsLoading };
