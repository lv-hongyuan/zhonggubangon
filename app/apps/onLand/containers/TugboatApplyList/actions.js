import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_TUGBOAT_APPLY_LIST, REVIEW_TUGBOAT_APPLY } from './constants';

export const getTugboatApplyListRoutine = createRoutine(GET_TUGBOAT_APPLY_LIST);
export const getTugboatApplyListPromise = promisifyRoutine(getTugboatApplyListRoutine);

export const reviewTugboatApplyRoutine = createRoutine(REVIEW_TUGBOAT_APPLY);
export const reviewTugboatApplyPromise = promisifyRoutine(reviewTugboatApplyRoutine);
