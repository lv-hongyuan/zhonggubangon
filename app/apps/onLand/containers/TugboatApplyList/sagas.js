import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getTugboatApplyListRoutine, reviewTugboatApplyRoutine } from './actions';

function* getTugboatApplyList(action) {
  console.log('+++拖轮开始',JSON.stringify(action));
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getTugboatApplyListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getTugboatApplyList(param));
    console.log('+++拖轮',JSON.stringify(response.showAudit));
    const showAudit = response.showAudit;
    const { totalElements, content } = (response.dtoList || {});
    yield put(getTugboatApplyListRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit,
    }));
    console.log('showAudit----',content,'121',showAudit)
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTugboatApplyListRoutine.failure(e));
  } finally {
    yield put(getTugboatApplyListRoutine.fulfill());
  }
}

export function* getTugboatApplyListSaga() {
  yield takeLatest(getTugboatApplyListRoutine.TRIGGER, getTugboatApplyList);
}

function* reviewTugboatApply(action) {
  console.log(action);
  try {
    yield put(reviewTugboatApplyRoutine.request());
    const { id, state, rejectReasons } = action.payload;
    const response = yield call(request, ApiFactory.updateApplyTugState({ id, state, rejectReasons }));
    console.log('reviewTugboatApply', response);
    yield put(reviewTugboatApplyRoutine.success({ id, state }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(reviewTugboatApplyRoutine.failure(e));
  } finally {
    yield put(reviewTugboatApplyRoutine.fulfill());
  }
}

export function* reviewTugboatApplySaga() {
  yield takeLatest(reviewTugboatApplyRoutine.TRIGGER, reviewTugboatApply);
}

// All sagas to be loaded
export default [getTugboatApplyListSaga, reviewTugboatApplySaga];
