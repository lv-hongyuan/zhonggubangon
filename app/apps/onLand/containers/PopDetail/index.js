
//航线管理=>港口作业=>点击列表后进入的上方tab页

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { createMaterialTopTabNavigator } from 'react-navigation';
import myTheme from '../../../../Themes';
import {
  ROUTE_HISTORY_RECORD_DETAIL,
  ROUTE_HISTORY_NEWSPAPER,
  ROUTE_EMERGENCIES_HISTORY,
} from '../../RouteConstant';
import { makeUnReadInstructionBadge } from '../Instructions/selectors';
import screenHOC from '../../../../components/screenHOC';
import { makeShipLandFunctionList } from '../PermissionsPrompt/selectors';
import HistoryRecordDetail from '../HistoryRecordDetail';
import HistoryNewsPaper from '../HistoryNewsPaper';
import EmergenciesHistory from '../EmergenciesHistory';

const TopTabNavigator = createMaterialTopTabNavigator({
  [ROUTE_HISTORY_RECORD_DETAIL]: { screen: HistoryRecordDetail },
  [ROUTE_HISTORY_NEWSPAPER]: { screen: HistoryNewsPaper },
  [ROUTE_EMERGENCIES_HISTORY]: { screen: EmergenciesHistory },
}, {
  backBehavior: 'none',
  tabBarOptions: {
    allowFontScaling:false,
    activeTintColor: '#ED1727',
    inactiveTintColor: '#666666',
    style: {
      backgroundColor: '#ffffff',
    },
    tabStyle: {
      backgroundColor: 'transparent',
      borderBottomColor: myTheme.borderColor,
      borderBottomWidth: myTheme.borderWidth,
    },
    indicatorStyle: {
      backgroundColor: '#ED1727',
    },
  },
});

@screenHOC
class PopDetail extends React.PureComponent {
  static router = {
    ...TopTabNavigator.router,
    getStateForAction: (action, lastState) => {
      const state = TopTabNavigator.router.getStateForAction(action, lastState);
      if (lastState === state) {
        return state;
      }
      const routes = state.routes && state.routes.map(route => ({
        ...route,
        params: { ...route.params, ...state.params, readOnly: true },
      }));
      return { ...state, routes };
    },
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TopTabNavigator navigation={this.props.navigation} screenProps={{ test: this.state.test }} />
    );
  }
}

PopDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '港口动态'),
});

PopDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  instructionsBadge: makeUnReadInstructionBadge(),
  functionList: makeShipLandFunctionList(),
});

export default connect(mapStateToProps)(PopDetail);
