import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DAILY_ACTUAL_LOADING } from './constants';

export const getDailyActualLoadingRoutine = createRoutine(GET_DAILY_ACTUAL_LOADING);
export const getDailyActualLoadingPromise = promisifyRoutine(getDailyActualLoadingRoutine);
