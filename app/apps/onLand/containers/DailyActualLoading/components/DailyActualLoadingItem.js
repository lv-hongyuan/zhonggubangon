import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class DailyActualLoadingItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pqname, gk, zwcm, allport, hc, loadteu, kaoheteu, wmzyy, manzai, fio,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pqname}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{gk}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{allport}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{zwcm}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{hc}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{loadteu}</Text>
        </View>
        <View style={[styles.col, { width: 90 }]}>
          <Text style={styles.text}>{kaoheteu}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{fio}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{manzai}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{wmzyy}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pqname}</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.text}>{gk}</Text>
            </View>
            <View style={[styles.col, { width: 140 }]}>
              <Text style={styles.text}>{allport}</Text>
            </View>
            <View style={[styles.col, { width: 140 }]}>
              <Text style={styles.text}>{zwcm}</Text>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

DailyActualLoadingItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default DailyActualLoadingItem;
