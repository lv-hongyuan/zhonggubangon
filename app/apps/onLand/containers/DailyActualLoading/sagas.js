import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDailyActualLoadingRoutine } from './actions';

function* getDailyActualLoading(action) {
  console.log(action);
  try {
    yield put(getDailyActualLoadingRoutine.request());
    const response = yield call(request, ApiFactory.getRealLoad());
    console.log(response);
    yield put(getDailyActualLoadingRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDailyActualLoadingRoutine.failure(e));
  } finally {
    yield put(getDailyActualLoadingRoutine.fulfill());
  }
}

export function* getDailyActualLoadingSaga() {
  yield takeLatest(getDailyActualLoadingRoutine.TRIGGER, getDailyActualLoading);
}

// All sagas to be loaded
export default [getDailyActualLoadingSaga];
