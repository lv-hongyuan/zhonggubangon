import { createSelector } from 'reselect/es';

const selectDailyActualLoadingDomain = () => state => state.dailyActualLoading;

const makeList = () => createSelector(selectDailyActualLoadingDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDailyActualLoadingDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
