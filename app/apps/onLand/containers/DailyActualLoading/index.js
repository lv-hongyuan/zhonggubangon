import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, StatusBar} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getDailyActualLoadingPromise } from './actions';
import { ROUTE_SHIP_TIME_DETAIL } from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import DailyActualLoadingItem from './components/DailyActualLoadingItem';
import RefreshListView from '../../../../components/RefreshListView';

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    height: 40,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

@screenHOC
class DailyActualLoading extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      containerWidth: undefined,
      recordW: 0,
      recordH: 0,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }


  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    this.props.navigation.navigate(
      ROUTE_SHIP_TIME_DETAIL,
      {
        id: this.props.navigation.getParam('id'),
        item,
      },
    );
  };

  //根View的onLayout回调函数
  onLayout = (event) => {
    //获取根View的宽高，以及左上角的坐标值
    let {x, y, width, height} = event.nativeEvent.layout;
    this.setState({
      recordW: width,
      recordH: height,
    });
  }

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    this.props.getDailyActualLoadingPromise({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    })
      .catch(() => {
      });
  }

  renderItem = ({ item }) => (
    <DailyActualLoadingItem
      item={item}
      headerTranslateX={this.translateX}
    />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <StatusBar backgroundColor="#DC001B" barStyle="light-content" hidden={true} translucent={true}/>
        <SafeAreaView style={{
          backgroundColor: '#E0E0E0',
          flex: 1,
        }}
        >
          <View
            style={{
              width: '100%',
              flex: 1,
            }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
            >
              <View style={{
                flex: 1,
                minWidth: '100%',
              }}
              >
                <View style={styles.container}>
                  <View style={styles.col}>
                    <Text style={styles.text}>片区</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>港口</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>航线</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>船名</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>航次</Text>
                  </View>
                  <View style={styles.col} onLayout={this.onLayout} >
                    <Text style={styles.text}>装载TEU</Text>
                  </View>
                  <View style={[styles.col, { width: 90 }]}>
                    <Text style={styles.text}>指标箱量</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>FIO收入（万元）</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>状态</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>原因</Text>
                  </View>
                  <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.translateX }],
                  }}
                  >
                    <View style={styles.container}>
                      <View style={[styles.col, {height: this.state.recordH}]}>
                        <Text style={styles.text}>片区</Text>
                      </View>
                      <View style={styles.col}>
                        <Text style={styles.text}>港口</Text>
                      </View>
                      <View style={[styles.col, { width: 140 }]}>
                        <Text style={styles.text}>航线</Text>
                      </View>
                      <View style={[styles.col, { width: 140 }]}>
                        <Text style={styles.text}>船名</Text>
                      </View>
                    </View>
                  </Animated.View>
                </View>
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={this.props.list || []}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  // onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

DailyActualLoading.navigationOptions = () => ({
  title: '每日实装',
});

DailyActualLoading.propTypes = {
  navigation: PropTypes.object.isRequired,
  getDailyActualLoadingPromise: PropTypes.func.isRequired,
  refreshState: PropTypes.number.isRequired,
  list: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDailyActualLoadingPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DailyActualLoading);
