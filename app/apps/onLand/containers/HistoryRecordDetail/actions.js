import {
  GET_WORK_DETAIL,
  WORK_DETAIL_RESULT,
  CLEAN,
} from './constants';

export function getDetailAction(workId) {
  return {
    type: GET_WORK_DETAIL,
    payload: workId,
  };
}

export function workDetailResultAction(workDetail, error) {
  return {
    type: WORK_DETAIL_RESULT,
    payload: {
      workDetail,
      error,
    },
  };
}

export function cleanAction() {
  return {
    type: CLEAN,
  };
}
