
// 点击下方tab页第二个港口作业的列表 + 点击下方tab页第一个港口作业的右上角历史记录
// 此页面不可编辑 
// 时间节点

import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  InputGroup,
  Container,
  Content,
  Form,
  Item,
  Label,
  Button,
  Text,
  View,
} from 'native-base';
import { BerthingState } from '../PortWorkDetail/constants';
import {
  getDetailAction,
  cleanAction,
} from './actions';
import {
  makeSelectHistoryRecordDetail,
  makeSelectHistoryRecordDetailNavBack,
  makeSelectHistoryRecordDetailLoading,
} from './selectors';
import myTheme from '../../../../Themes';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import DatePullSelector from '../../../../components/DatePullSelector/index';
import { HistoryRecordState } from '../HistoryRecord/constants';
import InputItem from '../../../../components/InputItem';
import AlertView from '../../../../components/Alert';
import commonStyles from '../../../../common/commonStyles';
import Switch from '../../../../components/Switch';
import Selector from '../../../../components/Selector';
import screenHOC from '../../../../components/screenHOC';
import { MaskType } from '../../../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  buttonContainer: {
    height: 50,
    borderColor: '#E0E0E0',
    flexDirection: 'row',
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  changePlanButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  overlay: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  doneImage: {
    position: 'absolute',
    top: 0,
    right: 40,
  },
  tipText: {
    paddingTop: 10,
    paddingLeft: 0,
    paddingBottom: 0,
    fontSize: 12,
    color: 'red',
  },
  inLineTouchCell: {
    marginLeft: -30,
    marginRight: -30,
    backgroundColor: '#ffffff',
    borderTopWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
});

@screenHOC
class HistoryRecordDetail extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      popId: this.props.navigation.getParam('popId'),
      data: {},
      openPlan: true,
      openBerthing: true,
      openOperating: true,
      openUnBerthing: true,
      isEditing: false,
      planWaitBerth: false,
    };
  }

  componentDidMount() {
    this.loadDetail();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navBack) {
      this.props.navigation.goBack();
      return;
    }
    // prop值改变时
    if (this.props.workDetail !== nextProps.workDetail) {
      console.log('this.props.workDetail', this.props.workDetail);
      console.log('nextProps.workDetail', nextProps.workDetail);
      const workDetail = nextProps.workDetail || {};

      // 首次设置数据
      if (!this.props.workDetail) {
        this.setState({
          // openPlan: workDetail.statePort >= HistoryRecordState.plan,
          // openBerthing: workDetail.statePort >= HistoryRecordState.berthing,
          // openOperating: workDetail.statePort >= HistoryRecordState.operating,
          // openUnBerthing: workDetail.statePort >= HistoryRecordState.unBerthing,
        });
      } else {
        if (this.state.isEditing) {
          this.setState({ isEditing: false });
        }
        if (this.props.workDetail.statePort !== workDetail.startWorkPort) {
          switch (workDetail.statePort) {
            case HistoryRecordState.plan:
              this.setState({ openPlan: true });
              break;
            case HistoryRecordState.berthing:
              this.setState({ openBerthing: true });
              break;
            case HistoryRecordState.operating:
              this.setState({ openOperating: true });
              break;
            case HistoryRecordState.unBerthing:
              this.setState({ openUnBerthing: true });
              break;
            default:
              break;
          }
        }
      }

      this.setWordDetail(workDetail);
    }
  }

  componentWillUnmount() {
    this.props.dispatch(cleanAction());
  }

  setWordDetail(detail) {
    const workDetail = detail || {};
    console.log('!!!+++_',JSON.stringify(workDetail));
    function zeroToNull(time) {
      return time === 0 ? null : time;
    }

    this.setState({
      data: {
        portId: workDetail.portId,
        voyageCode: workDetail.voyageCode,
        statePort: workDetail.statePort,
        estWaitBerthTime: workDetail.estWaitBerthTime,
        waitBerthCause: workDetail.waitBerthCause,
        estWaitGoodsTime: workDetail.estWaitGoodsTime,
        waitGoodsCause: workDetail.waitGoodsCause,
        wharf: workDetail.wharf,
        berth: workDetail.berth,
        voyageId: workDetail.voyageId,
        portName: workDetail.portName,
        nextPortId: workDetail.nextPortId,
        version: workDetail.version,
        directBerth: workDetail.directBerth,
        shipId: workDetail.shipId,
        id: workDetail.id,
        shipName: workDetail.shipName,
        portUserId: workDetail.portUserId,
        etaPlan: zeroToNull(workDetail.etaPlan),
        etbPlan: zeroToNull(workDetail.etbPlan),
        etdPlan: zeroToNull(workDetail.etdPlan),
        atdPort: zeroToNull(workDetail.atdPort),
        atbPort: zeroToNull(workDetail.atbPort),
        etdAfterBerth: zeroToNull(workDetail.etdAfterBerth),
        startWorkPort: zeroToNull(workDetail.startWorkPort),
        finishWorkPortMiddle: zeroToNull(workDetail.finishWorkPortMiddle),
        wharf1: workDetail.wharf1,
        workCon1: workDetail.workCon1,
        reverseCon1: workDetail.reverseCon1,
        wharf2: workDetail.wharf2,
        workCon2: workDetail.workCon2,
        reverseCon2: workDetail.reverseCon2,
        startWorkPortMiddle: zeroToNull(workDetail.startWorkPortMiddle),
        doubleHanger: workDetail.doubleHanger === '是' ? '是' : '否',
        finishWorkPort: zeroToNull(workDetail.finishWorkPort),
        ataPort: zeroToNull(workDetail.ataPort),
        directBerthPort: workDetail.directBerthPort,
        waitType: workDetail.directBerthPort === 1 ? BerthingState.directBerth : workDetail.waitType,
        weighAnchorPort: zeroToNull(workDetail.weighAnchorPort),
        planMemo: workDetail.planMemo,
        planChangeReason: workDetail.planChangeReason,
        berthOne: workDetail.berthOne,
        berthTwo: workDetail.berthTwo,
        workMemo: workDetail.workMemo,
        wharfWork: workDetail.wharfWork,
        berthWork: workDetail.berthWork,
      },
      planWaitBerth: workDetail.estWaitBerthTime > 0,
    });
  }

  setData(params, callback = () => {}) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    }, callback);
  }

  get readOnly() {
    return this.props.navigation.getParam('readOnly');
  }

  loadDetail = () => {
    this.props.dispatch(getDetailAction(this.state.popId));
  };

  //计划阶段：
  renderPlan() {
    const canEdit = this.state.isEditing;
    return (
        <View style={{
          marginTop: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}>
          <ExtendCell
              title="计划阶段"
              titleColor="#DC001B"
              isOpen={this.state.openPlan}
              onPress={() => {
                this.setState({ openPlan: !this.state.openPlan });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  预达锚地:
                </Label>
                <DatePullSelector
                    value={this.state.data.etaPlan}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  预计靠泊:
                </Label>
                <DatePullSelector
                    value={this.state.data.etbPlan}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  预计离泊:
                </Label>
                <DatePullSelector
                    value={this.state.data.etdPlan}
                    minValue={this.state.data.etbPlan}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  是否直靠:
                </Label>
                <Switch
                    value={this.state.data.directBerth === 1}
                    readOnly={!canEdit}
                    style={{
                      height: 30,
                      width: 60,
                    }}
                />
              </Item>
            </InputGroup>
            {this.state.data.directBerth !== 1 ? (
                    <InputGroup style={commonStyles.inputGroup}>
                      <Item style={commonStyles.inputItem}>
                        <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>
                          非直靠原因:
                        </Label>
                        <Selector
                            value={this.state.data.waitBerthCause}
                            pickerTitle="非直靠原因"
                            disabled={!canEdit}
                        />
                      </Item>
                    </InputGroup>)
                : null
            }
            {this.state.data.directBerth !== 1 ? (
                    <InputGroup style={commonStyles.inputGroup}>
                      <InputItem
                          label="预计时长:"
                          value={(this.state.data.estWaitBerthTime || 0) }
                          editable={canEdit}
                      />
                    </InputGroup>
                )
                : null
            }
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="预靠码头:"
                  value={this.state.data.wharf}
                  editable={canEdit}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="预靠泊位:"
                  value={this.state.data.berth}
                  editable={canEdit}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="计划备注:"
                  value={this.state.data.planMemo}
                  editable={canEdit}
              />
            </InputGroup>

            {this.state.data.planChangeReason === null ||
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="更改原因:"
                  value={this.state.data.planChangeReason}
                  editable={canEdit}
              />
            </InputGroup>}
          </ExtendCell>
        </View>
    );
  }

  //靠泊阶段：
  renderBerthing() {
    const canEdit = this.state.isEditing;
    return (
        <View style={{
          marginTop: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}>
          <ExtendCell
              title="靠泊阶段"
              titleColor="#DC001B"
              isOpen={this.state.openBerthing}
              onPress={() => {
                this.setState({ openBerthing: !this.state.openBerthing });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  到达锚地:
                </Label>
                <DatePullSelector
                    value={this.state.data.ataPort}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  是否直靠:
                </Label>
                <Selector
                    value={this.state.data.waitType}
                    pickerTitle="是否直靠"
                    disabled={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  起锚时间:
                </Label>
                <DatePullSelector
                    value={this.state.data.weighAnchorPort}
                    minValue={this.state.data.ataPort}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  实际靠泊:
                </Label>
                <DatePullSelector
                    value={this.state.data.atbPort}
                    minValue={this.state.data.ataPort}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
          </ExtendCell>
        </View>
    );
  }

  //作业阶段：
  renderOperating() {
    const canEdit = this.state.isEditing;
    return (
        <View style={{
          marginTop: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}>
          <ExtendCell
              title="作业阶段"
              titleColor="#DC001B"
              isOpen={this.state.openOperating}
              onPress={() => {
                this.setState({ openOperating: !this.state.openOperating });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>是否双挂:</Label>
                <Switch
                    value={this.state.data.doubleHanger === '是'}
                    readOnly={!canEdit}
                    style={{
                      height: 30,
                      width: 60,
                    }}
                />
              </Item>
            </InputGroup>
            {this.state.data.doubleHanger === '是' ? (
                <View>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="进口作业码头:"
                        editable={canEdit}
                        value={this.state.data.wharf1}
                        placeholder="请输入进口作业码头"
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="进口作业泊位:"
                        value={this.state.data.berthOne}
                        editable={canEdit}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPort}
                          minValue={this.state.data.atbPort}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPortMiddle}
                          minValue={this.state.data.startWorkPort}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业箱量:"
                        editable={canEdit}
                        value={this.state.data.workCon1}
                        placeholder="请输入作业箱量"
                    />
                    <InputItem
                        label="倒箱量:"
                        editable={canEdit}
                        value={this.state.data.reverseCon1}
                        placeholder="请输入倒箱量"
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="出口作业码头:"
                        editable={canEdit}
                        value={this.state.data.wharf2}
                        placeholder="请输入出口作业码头"
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="出口作业泊位:"
                        value={this.state.data.berthTwo}
                        editable={canEdit}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPortMiddle}
                          minValue={this.state.data.finishWorkPortMiddle}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPort}
                          minValue={this.state.data.startWorkPortMiddle}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业箱量:"
                        editable={canEdit}
                        value={this.state.data.workCon2}
                        placeholder="请输入作业箱量"
                    />
                    <InputItem
                        label="倒箱量:"
                        editable={canEdit}
                        value={this.state.data.reverseCon2}
                        placeholder="请输入倒箱量"
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业备注:"
                        value={this.state.data.workMemo}
                        editable={canEdit}
                    />
                  </InputGroup>
                </View>
            ) : (
                <View>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业码头:</Label>
                      <Selector
                          value={this.state.data.wharfWork}
                          disabled={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业泊位:"
                        value={this.state.data.berthWork}
                        editable={canEdit}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPort}
                          minValue={this.state.data.atbPort}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPort}
                          minValue={this.state.data.startWorkPort}
                          readOnly={!canEdit}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业备注:"
                        value={this.state.data.workMemo}
                        editable={canEdit}
                    />
                  </InputGroup>
                </View>
            )}
          </ExtendCell>
        </View>
    );
  }

  //离泊阶段：
  renderUnBerthing() {
    const canEdit = this.state.isEditing;
    return (
        <View style={{
          marginTop: 8,
          marginBottom: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}
        >
          <ExtendCell
              title="离泊阶段"
              titleColor="#DC001B"
              isOpen={this.state.openUnBerthing}
              onPress={() => {
                this.setState({ openUnBerthing: !this.state.openUnBerthing });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={[commonStyles.inputGroup, { marginBottom: 8 }]}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  实际离港:
                </Label>
                <DatePullSelector
                    value={this.state.data.atdPort}
                    minValue={this.state.data.finishWorkPort}
                    readOnly={!canEdit}
                />
              </Item>
            </InputGroup>
          </ExtendCell>
        </View>
    );
  }

  render() {
    return (
        <Container style={{ backgroundColor: '#E0E0E0' }}>
          <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
            <Form style={{ paddingBottom: this.readOnly ? 0 : 60 }}>
              {this.renderPlan()}
              {this.renderBerthing()}
              {this.renderOperating()}
              {this.renderUnBerthing()}
              <Text style={{
                textAlign: 'center',
                padding: 8,
                fontSize: 12,
                color: '#969696',
              }}
              >
                ———— 已经到底了 ————
              </Text>
            </Form>
          </Content>
        </Container>
    );
  }
}

HistoryRecordDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '信息录入'),
  tabBarLabel: '时间节点',
});

HistoryRecordDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  workDetail: PropTypes.object,
  navBack: PropTypes.bool.isRequired,
};
HistoryRecordDetail.defaultProps = {
  workDetail: undefined,
};

const mapStateToProps = createStructuredSelector({
  workDetail: makeSelectHistoryRecordDetail(),
  isLoading: makeSelectHistoryRecordDetailLoading(),
  navBack: makeSelectHistoryRecordDetailNavBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryRecordDetail);
