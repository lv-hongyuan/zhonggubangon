import { createSelector } from 'reselect';

const selectHistoryRecordDetailDomain = () => state => state.historyRecordDetail;

const makeSelectHistoryRecordDetail = () => createSelector(selectHistoryRecordDetailDomain(), (subState) => {
  console.log(subState);
  return subState.workDetail;
});

const makeSelectHistoryRecordDetailNavBack = () => createSelector(selectHistoryRecordDetailDomain(), (subState) => {
  console.log(subState);
  return subState.navBack;
});

const makeSelectHistoryRecordDetailLoading = () => createSelector(selectHistoryRecordDetailDomain(), subState => subState.isLoading);

export {
  makeSelectHistoryRecordDetail,
  makeSelectHistoryRecordDetailNavBack,
  makeSelectHistoryRecordDetailLoading,
};
