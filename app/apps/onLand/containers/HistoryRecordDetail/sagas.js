import { call, put, takeLatest } from 'redux-saga/effects';
import {
  GET_WORK_DETAIL,
} from './constants';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  workDetailResultAction,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getWorkDetail(action) {
  console.log(action);
  try {
    const response = yield call(
      request,
      ApiFactory.getWorkDetail(action.payload),
    );
    console.log('getWorkDetail', response.toString());
    yield put(workDetailResultAction(response, undefined));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(workDetailResultAction(undefined, e));
  }
}

export function* workDetailSaga() {
  yield takeLatest(GET_WORK_DETAIL, getWorkDetail);
}

// All sagas to be loaded
export default [
  workDetailSaga
];
