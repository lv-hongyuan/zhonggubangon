import {
  GET_WORK_DETAIL,
  WORK_DETAIL_RESULT,

  CHANGE_HISTORY_RECORD,
  CHANGE_HISTORY_RECORD_RESULT,

  CLEAN,
} from './constants';

const defaultState = {
  workDetail: null,
  isLoading: false,
  loadingError: null,
  navBack: false,
  pierList: [],
  berthList: [],
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case GET_WORK_DETAIL:
      return { ...state, isLoading: true };
    case WORK_DETAIL_RESULT:
      return { ...state, ...action.payload, isLoading: false };

    case CHANGE_HISTORY_RECORD:
      return { ...state, isLoading: true };
    case CHANGE_HISTORY_RECORD_RESULT:
      return {
        ...state,
        workDetail: action.payload.workDetail || state.workDetail,
        error: action.payload.error,
        isLoading: false,
      };

    case CLEAN:
      return defaultState;
    default:
      return state;
  }
}
