/*
 *
 * HistoryRecord constants
 *
 */
export const GET_WORK_DETAIL = 'onLand/HistoryRecordDetail/GET_WORK_DETAIL';
export const WORK_DETAIL_RESULT = 'onLand/HistoryRecordDetail/WORK_DETAIL_RESULT';

export const CLEAN = 'onLand/HistoryRecordDetail/CLEAN';
