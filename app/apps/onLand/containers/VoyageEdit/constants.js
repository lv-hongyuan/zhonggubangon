/*
 *
 * VoyageEdit constants
 *
 */
export const GET_VOYAGE_ALL_LIST = 'onLand/PortWork/VoyageEdit/getShipVoyageList';
export const CHANGE_VOYAGE_CODE = 'onLand/PortWork/VoyageEdit/changeVoyageCode';
export const CHANGE_VOYAGE_POP_PORT = 'onLand/PortWork/VoyageEdit/changePopPort';
export const DELETE_VOYAGE_LINE = 'onLand/PortWork/VoyageEdit/deleteNextVoyageLine';

export const ALL_DONE_COLOR = '#DC001B';
export const SHIP_DONE_COLOR = '#ffc107';
export const PORT_DONE_COLOR = '#8bc34a';
export const NONE_DONE_COLOR = '#BDBDBD';

export function stateColor({ statePort, stateShip }) {
  if (statePort > 0 && stateShip > 0) {
    return ALL_DONE_COLOR;
  }
  if (stateShip > 0) {
    return SHIP_DONE_COLOR;
  }
  if (statePort > 0) {
    return PORT_DONE_COLOR;
  }
  return NONE_DONE_COLOR;
}
