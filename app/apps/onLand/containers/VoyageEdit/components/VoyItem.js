
//! 卡片整体（包含左侧按钮）

import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { Text, View } from 'native-base';
import shortId from 'shortid';
import myTheme from '../../../../../Themes';
import PopItem from './PopItem';
import { changeVoyageCodePromise } from '../actions';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 10,
  },
  content: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    borderRadius: 5,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  circle: {
    padding: 3,
    borderRadius: 10,
    height: 20,
    minWidth: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  popContainer: {
    flexDirection: 'row',
  },
  lineContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 0,
    alignItems: 'center',
  },
  line: {
    top: 30,
    position: 'absolute',
    height: 2,
  },
  btnContainer: {
    marginTop: 5,
    padding: 5,
  },
  voyCode: {
    color: '#23527c',
  },
  delete: {
    color: 'red',
  },
});

class VoyItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      popList, title, showEdit, isLast, onPressChange, onDeleteVoyage,onPressPop,changeVoyageCode
    } = this.props;
    if (!Array.isArray(popList) || popList.length === 0) return null;

    const { voyCode, voyid, shipName ,shipMainLineName,shipMainLineId,wharfName} = popList[0];
    return (
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#FFFFFF',
          borderBottomWidth: myTheme.borderWidth,
          borderBottomColor: myTheme.borderColor,
        }}
      >
        <View style={{ width: 80, padding: 10, alignItems: 'center',justifyContent:'space-around' }}>
          <Text style={{}}>{title}</Text>
          <TouchableOpacity onPress={()=>{
            changeVoyageCode({
              voyCode,
              voyid,
              shipName,
              popList,
              shipMainLineName,
              shipMainLineId,
              wharfName
            })
          }}>
            <Text style={styles.voyCode}>{voyCode}</Text>
          </TouchableOpacity>
          {showEdit && (
            <TouchableOpacity
              style={styles.btnContainer}
              onPress={() => {
                onPressChange({
                  voyCode,
                  voyid,
                  shipName,
                  popList,
                  shipMainLineName,
                  shipMainLineId,
                  wharfName
                });
              }}
            >
              <Text style={styles.voyCode}>调整</Text>
            </TouchableOpacity>
          )}
          {isLast && (
            <TouchableOpacity
              style={styles.btnContainer}
              onPress={() => {
                onDeleteVoyage({ voyid, voyCode });
              }}
            >
              <Text style={styles.delete}>删除</Text>
            </TouchableOpacity>
          )}
        </View>
        <ScrollView style={{ flex: 1 }} horizontal showsHorizontalScrollIndicator={false}>
          <View style={styles.popContainer}>
            {popList.map((popItem, index) => (
              <PopItem
                key={shortId.generate()}
                index={index}
                isFirst={index === 0}
                isLast={index === popList.length - 1}
                item={popItem}
                onPress={() => {
                  onPressPop(popItem);
                }}
              />
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
}

VoyItem.defaultProps = {
  onDeleteVoyage: () => {},
  onPressChange: () => {},
  popList: [],
  showEdit: true,
  isLast: false,
};

VoyItem.propTypes = {
  onPressPop: PropTypes.func.isRequired,
  changeVoyageCode:PropTypes.func.isRequired,
  onDeleteVoyage: PropTypes.func,
  onPressChange: PropTypes.func,
  popList: PropTypes.array,
  showEdit: PropTypes.bool,
  isLast: PropTypes.bool,
  title: PropTypes.string.isRequired,
};

export default VoyItem;
