//! 卡片详情：  实际抵港/靠泊/离泊   
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import Svg from '../../../../../components/Svg';
import { stateColor } from '../constants';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
  },
  content: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    borderRadius: 5,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.3,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  circleContainer: {
    position: 'absolute',
    top: 10,
    left: 0,
    right: 0,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    padding: 3,
    borderRadius: 10,
    height: 20,
    minWidth: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleTitle: {
    fontSize: 12,
    color: '#FFFFFF',
  },
  lineContainer: {
    position: 'absolute',
    top: 10,
    left: 0,
    right: 0,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  line: {
    flex: 1,
    height: 2,
  },
});

class PopItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  static renderTa(eta, ata) {
    if (ata) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>实际抵港:</Text>
          {defaultFormat(ata)}
        </Text>
      );
    }
    if (eta) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>预计抵港:</Text>
          {defaultFormat(eta)}
        </Text>
      );
    }
    return null;
  }

  static renderTb(etb, atb) {
    if (atb) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>实际靠泊:</Text>
          {defaultFormat(atb)}
        </Text>
      );
    }
    if (etb) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>预计靠泊:</Text>
          {defaultFormat(etb)}
        </Text>
      );
    }
    return null;
  }

  static renderTd(etd, atd) {
    if (atd) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>实际离泊:</Text>
          {defaultFormat(atd)}
        </Text>
      );
    }
    if (etd) {
      return (
        <Text style={styles.label}>
          <Text style={styles.label}>预计离泊:</Text>
          {defaultFormat(etd)}
        </Text>
      );
    }
    return null;
  }

  render() {
    const {
      item, onPress, isFirst, isLast, index,
    } = this.props;
    const {
      portName, ata, atb, atd, eta, etb, etd, statePort, stateShip,
      wharfName
    } = item || {};
    const circleColor = stateColor({ statePort, stateShip });
    return (
      <View style={styles.container}>
          <View style={styles.content}>
            <View style={{ marginTop: 20 }}>
              <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <Text style={[styles.label, { textAlign: 'center' }]}>{portName}</Text>
               <TouchableOpacity onPress={()=>{
                 onPress(item);
               }}>
                 <Text style={{fontSize: 14,color: '#23527c',margin: 2,}}>({wharfName})</Text>
                </TouchableOpacity> 
              </View>
              
              {PopItem.renderTa(eta, ata)}
              {PopItem.renderTb(etb, atb)}
              {PopItem.renderTd(etd, atd)}
            </View>
          </View>
        <View style={styles.lineContainer} pointerEvents="none">
          <View style={[styles.line, { backgroundColor: isFirst ? 'transparent' : circleColor }]} />
          <View style={[styles.line, { backgroundColor: isLast ? 'transparent' : circleColor }]} />
        </View>
        <View style={styles.circleContainer} pointerEvents="none">
          <View style={[styles.circle, { backgroundColor: circleColor }]}>
            {statePort > 0 && stateShip > 0 ? (
              <Svg icon="ok-icon" size={10} color="#FFFFFF" />
            ) : (
              <Text style={styles.circleTitle}>{index + 1}</Text>
            )}
          </View>
        </View>
      </View>
    );
  }
}

PopItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
  isFirst: PropTypes.bool.isRequired,
  isLast: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired,
};

export default PopItem;
