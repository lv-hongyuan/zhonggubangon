
//! 航次调度详情界面

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SafeAreaView,
  StyleSheet, TouchableOpacity,
} from 'react-native';
import _ from 'lodash';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, Content, Text, View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  makeSelectRefreshState,
  makeLastPopList,
  makePopList,
  makeLatestPopId,
  makeLatestVoyId,
  makeNextVoyageList,
  makeIsLoading,
} from './selectors';
import { ROUTE_VOYAGE_LINE_EDIT } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import AlertView from '../../../../components/Alert';
import AlertInput from '../../../../components/AlertInput';
import screenHOC from '../../../../components/screenHOC';
import {
  getVoyageAllListPromise,
  changeVoyageCodePromise,
  changePopPortPromise,
  deleteNextVoyageLinePromise,
} from './actions';
// import commonStyles from '../../../../common/commonStyles';
// import InputItem from '../../../../components/InputItem';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import VoyItem from './components/VoyItem';
import { ALL_DONE_COLOR, PORT_DONE_COLOR, SHIP_DONE_COLOR } from './constants';
import { LineEditType, LineEditTypeKey } from '../VoyageLineEdit/constants';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
  tipContainer: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.3,
    zIndex: 1,
  },
  tipCircle: {
    borderRadius: 7.5,
    width: 15,
    height: 15,
    marginLeft: 2,
    marginRight: 8,
  },
  popContainer: {
    flexDirection: 'row',
  },
});

@screenHOC
class VoyageEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAlert:false
    };
  }

  componentDidMount() {
    this.loadList();
    const { navigation } = this.props;
    navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }


  //! 调整航线
  onPressChange = ({
    voyCode, voyid, shipName, popList, popId,shipMainLineName,shipMainLineId,wharfName
  }) => {
    const { navigation } = this.props;
    navigation.navigate(ROUTE_VOYAGE_LINE_EDIT, {
      voyCode,
      voyid,
      popId,
      shipName,
      popList,
      wharfName,
      shipLineName:shipMainLineName,
      shipId:shipMainLineId,
      [LineEditTypeKey]: LineEditType.EDIT,
      onBack: () => {
        this.loadList();
      },
    });
  };

  // !修改码头
  onPressPop = (data)=> {
    console.log('修改码头',data);
    this.setState({
      showAlert:true,
      type:'码头',
      original:data.wharfName,
      data
    })
    
  }
  // ! 修改航次号
  changeVoyageCode = (data) => {
    console.log('修改航次号',data);
    this.setState({
      showAlert:true,
      type:'航次号',
      original:data.voyCode,
      data
    })
  }

  // !计划下期
  onPlanNext = () => {
    const {
      navigation, latestPopId, latestVoyId, popList, nextVoyageList,lastPopList,
    } = this.props;
    let nextPop;
    let voyCode;
    let nextShipName;
    let nextShipId;
    let firstPop = '';
    var haveLetter = /[A-Z]/i;
    //有下期的情况下判断是不是空，如果不是空取nextVoyageList的值，下期是空的时候跑else，取popList的值
    if (nextVoyageList.length > 0) { // 已有下期航次的情况下
      const latestPops = nextVoyageList[nextVoyageList.length - 1];
      nextPop = latestPops[latestPops.length - 1];//最后一条下期韩次
      if(nextVoyageList.length > 1){
        firstPop = nextVoyageList[nextVoyageList.length - 2][0].voyCode
      }else{
        firstPop = popList[popList.length - 1].voyCode
      }
      let nextArrName1 = nextVoyageList[nextVoyageList.length-1];
      let nextArrName2 = nextArrName1[nextArrName1.length-1];
      let  nextArrId1 = nextVoyageList[nextVoyageList.length-1];
      let nextArrId2 = nextArrId1[nextArrId1.length-1];
      nextShipName = nextArrName2.shipMainLineName;
      nextShipId =  nextArrId2.shipMainLineId;

    } else {// 无下期航次情况下
      nextPop = popList[popList.length - 1];//本期韩次
      if(lastPopList.length > 0){
        firstPop = lastPopList[lastPopList.length -1].voyCode
      }
      nextShipName = popList[popList.length-1].shipMainLineName;
      nextShipId =  popList[popList.length-1].shipMainLineId;
    }
    if (!_.isEmpty(nextPop)) {
      let codeStr = nextPop.voyCode;
      console.log(firstPop,codeStr);
      if (!_.isEmpty(codeStr)) {
        const codeCharacter = codeStr.substring(0, codeStr.length - 1);//取下港口数字部分
        const codeLastCharacter = codeStr.substr(codeStr.length - 1, 1);//取下港口最后一位字母
        const firstNum = firstPop.substring(0, firstPop.length - 1);//取上港口数字部分
        if (codeLastCharacter == 'S') {
          if(haveLetter.test(codeCharacter)){
            voyCode = ''
          }else if(haveLetter.test(firstNum)){
            voyCode = codeCharacter + 'N';
          }else if(codeCharacter == firstNum){
            voyCode = (parseInt(codeCharacter) + 1) + 'N';
          }else{
            voyCode = codeCharacter + 'N';
          }
        } else {
          if(haveLetter.test(codeCharacter)){
            voyCode = ''
          }else if(haveLetter.test(firstNum)){
            voyCode = codeCharacter + 'S';
          }else if(codeCharacter == firstNum){
            voyCode = (parseInt(codeCharacter) + 1) + 'S';
          }else{
            voyCode = codeCharacter + 'S';
          }
        }
      } else {
        voyCode = '';
      }
    } else {
      voyCode = '';
    }

    navigation.navigate(ROUTE_VOYAGE_LINE_EDIT, {
      voyid: latestVoyId,
      popid: latestPopId,
      shipId: nextShipId,
      shipLineName: nextShipName,
      shipName: navigation.getParam('shipName'),
      popList: [nextPop],
      voyCode: voyCode,
      [LineEditTypeKey]: LineEditType.ADD_NEXT,
      onBack: () => {
        this.loadList();
      },
    });
  };

  // 删除航次
  onDeleteVoyage = ({ voyid, voyCode, isDelete = 0 }) => {
    const { deleteNextVoyageLine, showErrorMessage } = this.props;
    AlertView.show({
      message: `确定要删除${voyCode}航次吗？`,
      showCancel: true,
      confirmAction: () => {
        deleteNextVoyageLine({
          isDelete,
          voyid,
        })
          .then(() => {
            this.loadList();
          })
          .catch((error) => {
            const code = error && error.code;
            if (code === 831 || code === 832) {
              AlertView.show({
                message: error.message,
                showCancel: true,
                confirmAction: () => {
                  this.onDeleteVoyage({ voyid, voyCode, isDelete: 1 });
                },
              });
            } else {
              showErrorMessage(error);
            }
          });
      },
    });
  };

  loadList() {
    const { navigation, getVoyageAllList } = this.props;
    const voyid = navigation.getParam('voyid');
    getVoyageAllList({ voyid })
      .then(() => {
      })
      .catch(() => {});
  }

  //! 修改航次号 /修改码头号
  modification =(data)=>{
    console.log('1234',data);
    let newDate = data.newDate
    if(!newDate){
      this.setState({showAlert:false})
      AlertView.show({
        message: '请填写'+ data.type,
      });
    }else{

      //!  点击修改航次号，点击修改码头
      //!  点击修改航次号，点击修改码头
      //!  点击修改航次号，点击修改码头

    }
  }

  render() {
    const { lastPopList, popList, nextVoyageList } = this.props;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <View style={styles.tipContainer}>
          <Text>注：</Text>
          <Text>整体</Text>
          <View style={[styles.tipCircle, { backgroundColor: ALL_DONE_COLOR }]} />
          <Text>船上</Text>
          <View style={[styles.tipCircle, { backgroundColor: SHIP_DONE_COLOR }]} />
          <Text>现场</Text>
          <View style={[styles.tipCircle, { backgroundColor: PORT_DONE_COLOR }]} />
        </View>
        <Content
          style={{ flex: 1 }}
          contentContainerStyle={{ paddingBottom: 60 }}
        >
          <VoyItem
            onPressChange={this.onPressChange}
            onPressPop={this.onPressPop}
            popList={lastPopList}
            showEdit={false}
            title="上期:"
            changeVoyageCode = {this.changeVoyageCode}
          />
          <VoyItem
            onPressChange={this.onPressChange}
            onPressPop={this.onPressPop}
            changeVoyageCode = {this.changeVoyageCode}
            popList={popList}
            title="本期:"
          />
          {nextVoyageList
          && nextVoyageList.map((list, index) => {
            const title = index === 0 ? '下期:' : `下${index + 1}期:`;
            return (
              <VoyItem
                key={title}
                onPressChange={this.onPressChange}
                onPressPop={this.onPressPop}
                changeVoyageCode = {this.changeVoyageCode}
                onDeleteVoyage={this.onDeleteVoyage}
                popList={list}
                isLast={index === nextVoyageList.length - 1}
                title={title}
              />
            );
          })}
        </Content>
        <SafeAreaView style={{
          position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: '#ffffff',
        }}
        >
          <View style={{ flex: 1, backgroundColor: '#FBB03B' }}>
            <TouchableOpacity
              style={{ padding: 20 }}
              onPress={this.onPlanNext}
            >
              <Text style={{ textAlign: 'center', color: '#FFFFFF' }}>计划下期</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        <AlertInput
          type={this.state.type}  //!标题
          showAlert={this.state.showAlert}  //! 是否展示
          clickCanel={()=>{this.setState({showAlert:false})}}  //! 点击取消
          clickConfirm={this.modification}   //!  点击确定
          original={this.state.original}  //! 修改前数据
          data={this.state.data}   //! 传入的数据
        />
      </Container>
    );
  }
}

VoyageEdit.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '航次调度'),
});

VoyageEdit.defaultProps = {
  lastPopList: [],
  popList: [],
  nextVoyageList: [],
  latestPopId: undefined,
  latestVoyId: undefined,
};

VoyageEdit.propTypes = {
  //新建一个航线全称id shipMainLineId  航线全称 shipMainLineName
  shipMainLineId: PropTypes.number,
  shipMainLineName: PropTypes.string,
  navigation: PropTypes.object.isRequired,
  latestPopId: PropTypes.number,
  latestVoyId: PropTypes.number,
  lastPopList: PropTypes.array,
  popList: PropTypes.array,
  nextVoyageList: PropTypes.array,
  getVoyageAllList: PropTypes.func.isRequired,
  // changeVoyageCode: PropTypes.func.isRequired,
  // changePopPort: PropTypes.func.isRequired,
  deleteNextVoyageLine: PropTypes.func.isRequired,
  showErrorMessage: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  latestPopId: makeLatestPopId(),
  latestVoyId: makeLatestVoyId(),
  popList: makePopList(),
  lastPopList: makeLastPopList(),
  nextVoyageList: makeNextVoyageList(),
  refreshState: makeSelectRefreshState(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        showErrorMessage: errorMessage,
      },
      dispatch,
    ),
    ...bindPromiseCreators(
      {
        getVoyageAllList: getVoyageAllListPromise,
        changeVoyageCode: changeVoyageCodePromise,
        changePopPort: changePopPortPromise,
        deleteNextVoyageLine: deleteNextVoyageLinePromise,
      },
      dispatch,
    ),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VoyageEdit);
