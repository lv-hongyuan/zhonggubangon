import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CHANGE_VOYAGE_CODE, CHANGE_VOYAGE_POP_PORT, GET_VOYAGE_ALL_LIST, DELETE_VOYAGE_LINE,
} from './constants';

export const getVoyageAllListRoutine = createRoutine(GET_VOYAGE_ALL_LIST);
export const getVoyageAllListPromise = promisifyRoutine(getVoyageAllListRoutine);

export const changeVoyageCodeRoutine = createRoutine(CHANGE_VOYAGE_CODE);
export const changeVoyageCodePromise = promisifyRoutine(changeVoyageCodeRoutine);

export const changePopPortRoutine = createRoutine(CHANGE_VOYAGE_POP_PORT);
export const changePopPortPromise = promisifyRoutine(changePopPortRoutine);

export const deleteNextVoyageLineRoutine = createRoutine(DELETE_VOYAGE_LINE);
export const deleteNextVoyageLinePromise = promisifyRoutine(deleteNextVoyageLineRoutine);
