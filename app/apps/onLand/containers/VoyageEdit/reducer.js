import { getVoyageAllListRoutine, deleteNextVoyageLineRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  lastPopList: null,
  popList: null,
  nextPorts: null,
  popid: null,
  loading: false,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态（带分页）
    case getVoyageAllListRoutine.TRIGGER: {
      return {
        ...state,
        loading: true,
        refreshState: RefreshState.HeaderRefreshing,
      };
    }
    case getVoyageAllListRoutine.SUCCESS: {
      const {
        lastPopList, popList, nextPorts, popid,
      } = action.payload;
      return {
        ...state,
        lastPopList,
        popList,
        nextPorts,
        popid,
        refreshState: RefreshState.Idle,
      };
    }
    case getVoyageAllListRoutine.FAILURE: {
      return {
        ...state,
        lastPopList: null,
        popList: null,
        nextPorts: null,
        refreshState: RefreshState.Failure,
      };
    }
    case getVoyageAllListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 航次调度删除下期航线
    case deleteNextVoyageLineRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case deleteNextVoyageLineRoutine.SUCCESS: {
      const lineTypeList = action.payload;
      return { ...state, lineTypeList };
    }
    case deleteNextVoyageLineRoutine.FAILURE: {
      return { ...state };
    }
    case deleteNextVoyageLineRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
