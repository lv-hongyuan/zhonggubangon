import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
  getVoyageAllListRoutine, changeVoyageCodeRoutine, changePopPortRoutine, deleteNextVoyageLineRoutine,
} from './actions';

export function* getVoyageList(action) {
  console.log(action);
  const { voyid } = action.payload || {};
  try {
    yield put(getVoyageAllListRoutine.request());
    const response = yield call(request, ApiFactory.findAllPopsByVoy({ voyid }));
    console.log(response);
    const {
      lastPopList, popList, nextPorts, popid,
    } = response;
    yield put(
      getVoyageAllListRoutine.success({
        lastPopList,
        popList,
        nextPorts,
        popid,
      }),
    );
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getVoyageAllListRoutine.failure({ error }));
  } finally {
    yield put(getVoyageAllListRoutine.fulfill());
  }
}

export function* getVoyageListSaga() {
  yield takeLatest(getVoyageAllListRoutine.TRIGGER, getVoyageList);
}

function* changeVoyageCode(action) {
  console.log(action);
  try {
    yield put(changeVoyageCodeRoutine.request());
    const {
      newVoyageCode, oldVoyageCode, shipName, voyageId,
    } = action.payload;
    const response = yield call(
      request,
      ApiFactory.changeVoyageCode({
        newVoyageCode,
        oldVoyageCode,
        shipName,
        voyageId,
      }),
    );
    console.log('changeVoyageCode', response);
    yield put(changeVoyageCodeRoutine.success({}));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(changeVoyageCodeRoutine.failure(e));
  } finally {
    yield put(changeVoyageCodeRoutine.fulfill());
  }
}

export function* changeVoyageCodeSaga() {
  yield takeLatest(changeVoyageCodeRoutine.TRIGGER, changeVoyageCode);
}

function* changePopPort(action) {
  console.log(action);
  try {
    yield put(changePopPortRoutine.request());
    const { activePopid, newPortName, popid } = action.payload;
    const response = yield call(request, ApiFactory.changePopPort({ activePopid, newPortName, popid }));
    console.log('changePopPort', response);
    yield put(changePopPortRoutine.success({}));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(changePopPortRoutine.failure(e));
  } finally {
    yield put(changePopPortRoutine.fulfill());
  }
}

export function* changePopPortSaga() {
  yield takeLatest(changePopPortRoutine.TRIGGER, changePopPort);
}

function* deleteNextVoyageLine(action) {
  console.log(action);
  try {
    yield put(deleteNextVoyageLineRoutine.request());
    const { voyid, isDelete } = action.payload;
    const response = yield call(request, ApiFactory.deleteNextVoyageLine({ voyid, isDelete }));
    console.log('deleteNextVoyageLine', response);
    yield put(deleteNextVoyageLineRoutine.success({}));
  } catch (e) {
    console.log(e.message);
    yield put(deleteNextVoyageLineRoutine.failure(e));
  } finally {
    yield put(deleteNextVoyageLineRoutine.fulfill());
  }
}

export function* deleteNextVoyageLineSaga() {
  yield takeLatest(deleteNextVoyageLineRoutine.TRIGGER, deleteNextVoyageLine);
}

// All sagas to be loaded
export default [getVoyageListSaga, changeVoyageCodeSaga, changePopPortSaga, deleteNextVoyageLineSaga];
