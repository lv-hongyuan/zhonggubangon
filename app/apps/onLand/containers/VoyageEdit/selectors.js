import { createSelector } from 'reselect/es';

const selectVoyageEditDomain = () => state => state.voyageEdit;

const makeLastPopList = () => createSelector(selectVoyageEditDomain(), (subState) => {
  console.debug(subState);
  return subState.lastPopList;
});

const makePopList = () => createSelector(selectVoyageEditDomain(), (subState) => {
  console.debug(subState);
  return subState.popList;
});

const makeNextVoyageList = () => createSelector(selectVoyageEditDomain(), (subState) => {
  console.debug(subState);
  return subState.nextPorts;
});

const makeLatestPop = () => createSelector(makePopList(), makeNextVoyageList(), (popList, nextPorts) => {
  console.debug(popList, nextPorts);
  if (Array.isArray(nextPorts) && nextPorts.length > 0) {
    const latestVoy = nextPorts[nextPorts.length - 1];
    if (Array.isArray(latestVoy) && latestVoy.length > 0) {
      return latestVoy[latestVoy.length - 1];
    }
  }
  if (Array.isArray(popList)) {
    return popList.slice(-1)[0];
  }
  return null;
});

const makeLatestPopId = () => createSelector(makeLatestPop(), (pop) => {
  console.debug(pop);
  return pop && pop.id;
});

const makeLatestVoyId = () => createSelector(makeLatestPop(), (pop) => {
  console.debug(pop);
  return pop && pop.voyid;
});

const makeSelectRefreshState = () => createSelector(selectVoyageEditDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectVoyageEditDomain(), (subState) => {
  console.debug(subState);
  return subState.loading;
});

export {
  makeLastPopList,
  makeLatestVoyId,
  makePopList,
  makeNextVoyageList,
  makeLatestPopId,
  makeSelectRefreshState,
  makeIsLoading,
};
