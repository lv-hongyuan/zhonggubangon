import { getShipTimeNewDetailRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';
const initState = {
  list: [],
  refreshState: RefreshState.Idle,
  loading:false
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipTimeNewDetailRoutine.TRIGGER:{
      const { page } = action.payload;
      return { 
        ...state, 
        loading: true, 
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      }
    }
    case getShipTimeNewDetailRoutine.SUCCESS:{
      const { page, pageSize, list } = action.payload;
      console.log( list);
      return { 
        ...state,
        list: page == 1 ? fixIdList(list) : fixIdList(state.list.concat(list)),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      }
    }
      
    case getShipTimeNewDetailRoutine.FAILURE:{
      const { page, error } = action.payload;
      return {
        ...state,
        list: page === 1 ? [] : state.list,
        error,
        refreshState: RefreshState.Failure,
      };
    }
      
    case getShipTimeNewDetailRoutine.FULFILL:{
      return { ...state, loading: false };
    }
      

    default:
      return state;
  }
}