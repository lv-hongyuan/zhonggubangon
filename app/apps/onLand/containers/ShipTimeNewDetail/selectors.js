import { createSelector } from 'reselect';

const selectShipTimeNewDetailDomain = () => state => state.shipTimeNewDetail;
const makeList = () => createSelector(selectShipTimeNewDetailDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectShipTimeNewDetailDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

// const makeDefaultType = () => createSelector(selectShipTimeNewDetailDomain(), (subState) => {
//   console.debug(subState.defaultType);
//   return subState.defaultType
// })
export { makeList, makeRefreshState};