import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipTimeNewDetailRoutine } from './actions';

function* getShipTimeNewDetail(action) {
  const { page, pageSize, ...rest } = (action.payload || {});

  try {
    yield put(getShipTimeNewDetailRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      param.filter.filters.push({
        field: key,
        operator: 'eq',
        value,
      });
    });
    console.log('param:', param)
    const response = yield call(request, ApiFactory.getShipVoyageList(param));
    console.log(response);
    yield put(getShipTimeNewDetailRoutine.success({
      list:(response.dtoList.content || []),
      page,
      pageSize
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipTimeNewDetailRoutine.failure({page,pageSize,e}));
  } finally {
    yield put(getShipTimeNewDetailRoutine.fulfill());
  }
}

export function* getShipTimeNewDetailSaga() {
  yield takeLatest(getShipTimeNewDetailRoutine.TRIGGER, getShipTimeNewDetail);
}

export default [getShipTimeNewDetailSaga];