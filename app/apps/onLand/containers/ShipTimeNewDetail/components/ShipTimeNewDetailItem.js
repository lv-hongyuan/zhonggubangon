import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, ScrollView } from 'react-native';
import { Text, View } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		height: 90,
		paddingLeft: 15,
		marginBottom: 5,
	},
	leftView: {
		width: 55,
		flexDirection: 'column',
		height: '100%',
		marginRight: '10%',
		justifyContent: 'space-around',
		alignItems: 'flex-end',
	},
	rightView: {
		flex: 1,
		flexDirection: 'row',
		paddingTop: 1,
	},
	itemsDetail: {
		width: 65,
		height: '100%',
		flexDirection: 'column',
		justifyContent: 'space-around',
	}
});

class ShipTimeNewDetailItem extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		const {
			voyageCode, 	//航次
			popList,   		//航次名称list
			voyNum,			//排序key
		} = (this.props.item || {});

		return (
			<View style={styles.container}>
				<View style={styles.leftView}>
					<Text style={{ fontSize: 14, color: voyNum == 0 ? '#47f' : '#000' }}>{voyageCode}</Text>
					<Text>靠港</Text>
					<Text>离港</Text>
				</View>
				<ScrollView
					style={styles.rightView}
					horizontal={true}
					showsHorizontalScrollIndicator={false}
				>
					{
						popList && popList.map((list, index) => {
							let atbTextBgc = list.atb ? '#000' : 'orange';
							let atdTextBgc = list.atd ? '#000' : 'orange';

							return (
								<View style={{ flexDirection: 'row' }} key={list.portName}>
									{index == 0 ? null : <View style={{ width: 50}}>
											<View style={{
												width:30,
												height:14,
												borderBottomWidth: 1.5,
												borderBottomColor: voyNum == 0 ? '#47f' : '#000',
											}}></View>
									</View>}
									<View style={styles.itemsDetail} >
										<Text style={{ fontSize: 14, color: voyNum == 0 ? '#47f' : '#000' }}>{list.portName}</Text>
										<Text style={{ color: atbTextBgc }}>{list.atb ? list.atb : list.etb}</Text>
										<Text style={{ color: atdTextBgc }}>{list.atd ? list.atd : list.etd}</Text>
									</View>

								</View>
							)
						})
					}</ScrollView>
			</View>
		);
	}
}

ShipTimeNewDetailItem.propTypes = {
	item: PropTypes.object.isRequired,
};

export default ShipTimeNewDetailItem;
