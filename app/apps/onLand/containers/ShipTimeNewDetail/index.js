import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import screenHOC from '../../../../components/screenHOC';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { makeList, makeRefreshState } from './selectors';
import { getShipTimeNewDetailPromiseCreator } from './actions';
import RefreshListView from '../../../../components/RefreshListView';
import ShipTimeNewDetailItem from './components/ShipTimeNewDetailItem';
const styles = {
  toptitle: {
    height: 40,
    flexDirection: 'row',
  },
  headerbutton: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#aaa',
    borderBottomWidth: 2,
  },
  text: {
    color: '#000',
    fontSize: 16,
  },
  tab: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
    borderBottomColor: '#aaa',
    borderBottomWidth: 1,
    paddingLeft: 10,
  }
}
const firstPageNum = 1;
const DefaultPageSize = 10;
@screenHOC
class ShipTimeNewDetail extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      pageSize: DefaultPageSize,  //展示行数
			page: firstPageNum,         //页数
      textStyleLeft: { color: '#c00' },
      buttonStyleLeft: { borderBottomColor: '#c00' },
      textStyleRight: {},
      buttonStyleRight: {},
      type: 0,   //查询类型
    };
  }

  componentDidMount() {
		this.listView.beginRefresh();
  }

  	//加载列表
	loadList(loadMore = false, type = this.state.type, shipName) {
		const { page, pageSize } = this.state;
		this.props.getShipTimeNewDetailPromiseCreator({
			type: type ? type : 0,    //查询类型
			page: loadMore ? page : 1,
			pageSize,
			shipName,
		})
			.then(({ page}) => {
				this.setState({ page: page + 1 });
			})
			.catch(() => {
			});
	}

	// 上拉加载
	onHeaderRefresh = () => {
    let shipName = this.props.navigation.getParam('shipName')
		this.loadList(false,this.state.type,shipName);
	};

	// 下拉刷新
	onFooterRefresh = () => {
    let shipName = this.props.navigation.getParam('shipName')
		this.loadList(true,this.state.type,shipName);
  };
  
  renderItem = ({ item }) => {
		return (
			<ShipTimeNewDetailItem
				item={item}
			/>
		)
  };
  
  render() {
    const textStyle = { color: '#c00' }
    const buttonStyle = { borderBottomColor: '#c00' }
    return (
      <View style={{ flex: 1, backgroundColor: '#eee' }}>
        <View style={styles.toptitle}>
          <TouchableOpacity
            style={[styles.headerbutton, this.state.buttonStyleLeft]}
            onPress={() => {
              this.setState({
                textStyleLeft: textStyle,
                buttonStyleLeft: buttonStyle,
                textStyleRight: null,
                buttonStyleRight: null,
                type:0
              })
              let shipName = this.props.navigation.getParam('shipName')
              this.listView.beginRefresh();
              this.loadList(false,0,shipName);
              
            }}
          >
            <Text style={[styles.text, this.state.textStyleLeft]}>计划航次</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.headerbutton, this.state.buttonStyleRight]}
            onPress={() => {
              this.setState({
                textStyleLeft: null,
                buttonStyleLeft: null,
                textStyleRight: textStyle,
                buttonStyleRight: buttonStyle,
                type:1
              })
              let shipName = this.props.navigation.getParam('shipName')
              this.listView.beginRefresh();
              this.loadList(false,1,shipName);
            }}
          >
            <Text style={[styles.text, this.state.textStyleRight]}>历史航次</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tab}>
          <Text style={{ fontSize: 15, marginRight: '10%' ,marginLeft:15,fontWeight:'400',color:'#000'}}>航次号</Text>
          <Text style={{ fontSize: 15 ,fontWeight:'400',color:'#000'}}>航线</Text>
        </View>
        <View style={{ flex: 1 }}>
          <RefreshListView
            ref={(ref) => { this.listView = ref; }}
            data={this.props.list || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
          />
        </View>
      </View>
    );
  }
}

ShipTimeNewDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '船舶详情'),
});

ShipTimeNewDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
  getShipTimeNewDetailPromiseCreator: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipTimeNewDetailPromiseCreator,
    }, dispatch),
    dispatch,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ShipTimeNewDetail);
