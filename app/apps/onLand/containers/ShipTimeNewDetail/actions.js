import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TIME_NEW_DETAIL } from './constants';

export const getShipTimeNewDetailRoutine = createRoutine(GET_SHIP_TIME_NEW_DETAIL);
export const getShipTimeNewDetailPromiseCreator = promisifyRoutine(getShipTimeNewDetailRoutine);