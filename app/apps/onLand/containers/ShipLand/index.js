import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import myTheme from '../../../../Themes';
import GridButtonView from '../../../../components/GridButtonView';
import {
  ROUTE_INSTRUCTIONS,
  ROUTE_SHIP_TIME,
  ROUTE_BOX_NUMBER_QUERY,
  ROUTE_TUGBOAT_APPLY_LIST,
  ROUTE_PILOT_APPLY_LIST,
  ROUTE_SHIP_REPAIR_APPLY_LIST,
  ROUTE_SERVICE_FEE_APPLY_LIST,
  ROUTE_ADD_OIL_APPLY,
  ROUTE_SCHEDULE,
  ROUTE_VOYAGE_DISPATCH,
  ROUTE_ACTIVATION_NEXT_PORT,
  ROUTE_SHIP_DEPARTMENT_SCHEDULE,
  ROUTE_SHIP_TIME_NEW,
  ROUTE_SEA_MAP,
} from '../../RouteConstant';
import { makeUnReadInstructionBadge } from '../Instructions/selectors';
import screenHOC from '../../../../components/screenHOC';
import { makeShipLandFunctionList } from '../PermissionsPrompt/selectors';

@screenHOC
class ShipLand extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  menuList() {
    return (this.props.functionList || []).map((appfunctionUrl) => {
      switch (appfunctionUrl) {
        case 'Instructions':
          return {
            icon: 'onLand_voyage-instructions',
            title: '航次指令',
            badge: this.props.instructionsBadge,
            action: () => {
              this.props.navigation.navigate(ROUTE_INSTRUCTIONS);
            },
          };
        case 'ShipTime':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '现场船期表',
            action: () => {
              this.props.navigation.navigate(ROUTE_SHIP_TIME);
            },
          };
        case 'BoxNumberQuery':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '箱号查询',
            action: () => {
              this.props.navigation.navigate(ROUTE_BOX_NUMBER_QUERY);
            },
          };
        case 'TugboatApply':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '拖轮申请',
            action: () => {
              this.props.navigation.navigate(ROUTE_TUGBOAT_APPLY_LIST);
            },
          };
        case 'PilotApply':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '引水申请',
            action: () => {
              this.props.navigation.navigate(ROUTE_PILOT_APPLY_LIST);
            },
          };
        case 'ServiceFeeApply':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '劳务费申请',
            action: () => {
              this.props.navigation.navigate(ROUTE_SERVICE_FEE_APPLY_LIST);
            },
          };
        case 'RepairApply':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '修船申请',
            action: () => {
              this.props.navigation.navigate(ROUTE_SHIP_REPAIR_APPLY_LIST);
            },
          };
        case 'FindAddOilPlans':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '加油申请',
            action: () => {
              this.props.navigation.navigate(ROUTE_ADD_OIL_APPLY);
            },
          };
        case 'Schedule':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '到港预报',
            action: () => {
              this.props.navigation.navigate(ROUTE_SCHEDULE);
            },
          };
        case 'VoyageDispatch':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '航次调度',
            action: () => {
              this.props.navigation.navigate(ROUTE_VOYAGE_DISPATCH);
            },
          };
        case 'ActivationNextPort':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '港口动态激活',
            action: () => {
              this.props.navigation.navigate(ROUTE_ACTIVATION_NEXT_PORT);
            },
          };
        case 'ShipDepartmentSchedule':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '船管部船期表',
            action: () => {
              this.props.navigation.navigate(ROUTE_SHIP_DEPARTMENT_SCHEDULE);
            },
          };
        case 'ShipTimeNew':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '船期表',
            action: () => {
              this.props.navigation.navigate(ROUTE_SHIP_TIME_NEW);
            },
          };
        case 'SeaMap':
          return {
            icon: 'onLand_ship-ime-chart',
            title: '船舶定位',
            action: () => {
              this.props.navigation.navigate(ROUTE_SEA_MAP);
            },
          };
        default:
          return undefined;
      }
    })
      .filter(item => !!item);
  }

  render() {
    const data = this.menuList();
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content
          theme={myTheme}
          style={{
            flex: 1,
            minHeight: '100%',
          }}
        >
          <SafeAreaView style={{
            flex: 1,
            minHeight: '100%',
          }}
          >
            <View style={{
              backgroundColor: 'white',
              padding: 20,
            }}
            >
              <GridButtonView data={data} />
            </View>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

ShipLand.propTypes = {
  navigation: PropTypes.object.isRequired,
  functionList: PropTypes.array,
  instructionsBadge: PropTypes.number,
};
ShipLand.defaultProps = {
  functionList: [],
  instructionsBadge: undefined,
};

ShipLand.navigationOptions = {
  title: '船岸交互',
};

const mapStateToProps = createStructuredSelector({
  instructionsBadge: makeUnReadInstructionBadge(),
  functionList: makeShipLandFunctionList(),
});

export default connect(mapStateToProps)(ShipLand);
