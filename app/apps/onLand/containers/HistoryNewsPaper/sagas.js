import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getSailNewspaperDetailRoutine,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getPopDetail(action) {
  console.log(action);
  try {
    yield put(getSailNewspaperDetailRoutine.request());
    const response = yield call(request, ApiFactory.getPopDetail(action.payload));
    console.log('getPopDetail', response);
    yield put(getSailNewspaperDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getSailNewspaperDetailRoutine.failure(e));
  } finally {
    yield put(getSailNewspaperDetailRoutine.fulfill());
  }
}

export function* popDetailSaga() {
  yield takeLatest(getSailNewspaperDetailRoutine.TRIGGER, getPopDetail);
}

// All sagas to be loaded
export default [popDetailSaga];
