import { createSelector } from 'reselect';

const selectSailNewspaperDomain = () => state => state.historyNewsPaper;

const makeSelectSailNewspaperDetail = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.debug(subState);
  return subState.popDetail;
});

const makeSelectSailNewspaperLoading = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

const makeSelectSailNewspaperError = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.log(subState);
  return subState.error;
});

export {
  makeSelectSailNewspaperDetail,
  makeSelectSailNewspaperLoading,
  makeSelectSailNewspaperError,
};
