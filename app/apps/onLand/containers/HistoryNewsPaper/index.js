// 点击下方tab页第二个港口作业的列表 + 点击下方tab页第一个港口作业的右上角历史记录
// 此页面不可编辑 
// 开航报

import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView ,TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Text, InputGroup, Container, Content, View, Form, Item, Label, Button,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import {
  getSailNewspaperDetailPromise,
} from './actions';
import {
  makeSelectSailNewspaperDetail,
  makeSelectSailNewspaperLoading,
} from './selectors';
import myTheme from '../../../../Themes';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import InfoTable from '../SailNewspaper/components/InfoTable';
import Switch from '../../../../components/Switch';

import InputItem from '../../../../components/InputItem';
import AlertView from '../../../../components/Alert';
import Selector from '../../../../components/Selector';
import screenHOC from '../../../../components/screenHOC';
import {
  validPilotReason,
  validTugMemoReason,
} from '../../common/Constant';
import commonStyles from '../../../../common/commonStyles';
import { MaskType } from '../../../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  leaveTons: {
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    padding: 5,
    marginLeft: -5,
    marginRight: -5,
    borderRadius: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  ChangingButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  overlay: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  billing:{
    width:70,
    height:30,
    backgroundColor:'#DC001B',
    alignItems:'center',
    justifyContent:'center'
  }
});

@screenHOC
class HistoryNewsPaper extends React.PureComponent {
  static loadDetailData(popDetail) {
    const data = popDetail || {};
    return {
      e20Load: data.e20Load || 0,
      f20Load: data.f20Load || 0,
      e40Load: data.e40Load || 0,
      f40Load: data.f40Load || 0,
      e40HLoad: data.e40HLoad || 0,
      f40HLoad: data.f40HLoad || 0,
      eOtherLoad: data.eOtherLoad || 0,
      fOtherLoad: data.fOtherLoad || 0,
      e20UnLoad: data.e20UnLoad || 0,
      f20UnLoad: data.f20UnLoad || 0,
      e40UnLoad: data.e40UnLoad || 0,
      f40UnLoad: data.f40UnLoad || 0,
      e40HUnLoad: data.e40HUnLoad || 0,
      f40HUnLoad: data.f40HUnLoad || 0,
      eOtherUnLoad: data.eOtherUnLoad || 0,
      fOtherUnLoad: data.fOtherUnLoad || 0,
      e20Dx: data.e20Dx || 0,
      f20Dx: data.f20Dx || 0,
      e40Dx: data.e40Dx || 0,
      f40Dx: data.f40Dx || 0,
      e40HDx: data.e40HDx || 0,
      f40HDx: data.f40HDx || 0,
      eOtherDx: data.eOtherDx || 0,
      fOtherDx: data.fOtherDx || 0,
      e20Tx: data.e20Tx || 0,
      f20Tx: data.f20Tx || 0,
      e40Tx: data.e40Tx || 0,
      f40Tx: data.f40Tx || 0,
      e40HTx: data.e40HTx || 0,
      f40HTx: data.f40HTx || 0,
      eOtherTx: data.eOtherTx || 0,
      fOtherTx: data.fOtherTx || 0,

      id: data.id,
      pilotMemoBerth: data.pilotMemoBerth,
      state: data.state,
      cableFee: data.cableFee,
      popId: data.popId,
      memoDx: data.memoDx,
      fullLoad: data.fullLoad,
      pilotLeave: data.pilotLeave,
      tugNumBerth: data.tugNumBerth,
      pilotMemoLeave: data.pilotMemoLeave,
      otherFee: data.otherFee,
      tugFeeBerth: data.tugFeeBerth,
      pilotFeeLeave: data.pilotFeeLeave,
      pilotBerth: data.pilotBerth,
      tugMemoLeave: data.tugMemoLeave,
      waterFee: data.waterFee,
      parkingFee: data.parkingFee,
      feeMemo: data.feeMemo,
      tugMemoBerth: data.tugMemoBerth,
      tugNumLeave: data.tugNumLeave,
      version: data.version,
      expectedLoss: data.expectedLoss,
      openDoorFee: data.openDoorFee,
      pilotFeeBerth: data.pilotFeeBerth,
      memoTx: data.memoTx,
      tugFeeLeave: data.tugFeeLeave,
      agencyFee: data.agencyFee,
      lossCause: data.lossCause,
      loadTon: data.loadTon,
      unLoadTon: data.unLoadTon,

      f20: data.f20,
      e20: data.e20,
      f40: data.f40,
      e40: data.e40,
      goodsTon: data.goodsTon,

      parkTime: _.isNumber(data.parkTime) ? data.parkTime.toFixed(1) : undefined,

      draughtFront: data.draughtFront,
      draughtMid: data.draughtMid,
      draughtAfter: data.draughtAfter,
      gm: data.gm,
      draughtFull: data.draughtFull,
      clearanceFee:data.clearanceFee,//清道费
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      popId: this.props.navigation.getParam('popId'),
      data: HistoryNewsPaper.loadDetailData(this.props.popDetail),
      isChanging: false,
      openLoadingInfo: true,
      openCostInfo: true,
    };
  }

  componentDidMount() {
    this.loadDetail();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.popDetail !== nextProps.popDetail) {
      this.setState({
        data: HistoryNewsPaper.loadDetailData(nextProps.popDetail),
      });
    }
  }

  get readOnly() {
    return this.props.navigation.getParam('readOnly');
  }

  //获取列表详情数据
  loadDetail = () => {
    this.props.getSailNewspaperDetailPromise(this.state.popId)
        .then(() => {
        })
        .catch(() => {
        });
  };

  //装载信息：
  renderLoadingInformation() {
    const canEdit = this.state.isChanging;
    return (
        <ExtendCell
            title="装载信息"
            titleColor="#DC001B"
            isOpen={this.state.openLoadingInfo}
            onPress={() => {
              this.setState({ openLoadingInfo: !this.state.openLoadingInfo });
            }}
            touchable
            style={{ marginTop: 8 }}
        >
          <InfoTable
              tableData={this.state.data}
              disabled={!canEdit}
              style={{ marginLeft: -30, marginRight: -30 }}
          />
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>倒箱备注:</Label>
              <InputItem
                  value={this.state.data.memoDx}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>特殊作业备注:</Label>
              <InputItem
                  value={this.state.data.memoTx}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>装货吨:</Label>
              <InputItem
                  value={this.state.data.loadTon}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>卸货吨:</Label>
              <InputItem
                  value={this.state.data.unLoadTon}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <View style={styles.leaveTons}>
            <Text style={commonStyles.inputLabel}>船舶离港在船总货量</Text>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="F20:"
                  value={this.state.data.f20}
                  editable={canEdit}
              />
              <InputItem
                  label="E20:"
                  value={this.state.data.e20}
                  editable={canEdit}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="F40:"
                  value={this.state.data.f40}
                  editable={canEdit}
              />
              <InputItem
                  label="E40:"
                  keyboardType="numeric"
                  maskType={MaskType.INTEGER}
                  value={this.state.data.e40}
                  editable={canEdit}
                  onChangeText={(text) => {
                    this.setData({ e40: text });
                  }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="货物吨数:"
                  value={this.state.data.goodsTon}
                  editable={canEdit}
                  rightItem={<Text style={commonStyles.tail}>吨</Text>}
              />
            </InputGroup>
          </View>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>满载:</Label>
              <Switch
                  value={this.state.data.fullLoad === 1}
                  readOnly={!canEdit}
                  style={{
                    height: 30,
                    width: 60,
                  }}
              />
            </Item>
          </InputGroup>
          {this.state.data.fullLoad === 1 ? null : (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>亏吨预估:</Label>
                  <InputItem
                      value={this.state.data.expectedLoss}
                      editable={canEdit}
                  />
                </Item>
              </InputGroup>
          )}
          {this.state.data.fullLoad === 1 ? null : (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>未满原因:</Label>
                  <Selector
                      disabled={!canEdit}
                      value={this.state.data.lossCause}
                  />
                </Item>
              </InputGroup>
          )}
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>吃水前:</Label>
              <InputItem
                  value={this.state.data.draughtFront}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>吃水中:</Label>
              <InputItem
                  value={this.state.data.draughtMid}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>吃水后:</Label>
              <InputItem
                  value={this.state.data.draughtAfter}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>GM值:</Label>
              <InputItem
                  value={this.state.data.gm}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>满载吃水:</Label>
              <InputItem
                  value={this.state.data.draughtFull}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
        </ExtendCell>
    );
  }

  //费用信息：
  renderCostInformation() {
    const canEdit = this.state.isChanging;
    const portName = this.props.navigation.getParam('portName');
    return (
        <ExtendCell
            title="费用信息"
            titleColor="#DC001B"
            isOpen={this.state.openCostInfo}
            onPress={() => {
              this.setState({ openCostInfo: !this.state.openCostInfo });
            }}
            touchable
            style={{ marginTop: 8 }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>靠泊拖轮数:</Label>
              <InputItem
                  value={this.state.data.tugNumBerth}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>离泊拖轮数:</Label>
              <InputItem
                  value={this.state.data.tugNumLeave}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>拖轮费:</Label>
              <InputItem
                  value={this.state.data.tugFeeBerth}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>拖轮说明:</Label>
              <Selector
                  disabled={!canEdit}
                  value={validTugMemoReason(this.state.data.tugMemoBerth)}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>靠泊引水:</Label>
              <Switch
                  value={this.state.data.pilotBerth === 1}
                  readOnly={!canEdit}
                  style={{ height: 30, width: 60 }}
              />
            </Item>
            <View style={{ width: 20 }} />
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>离泊引水:</Label>
              <Switch
                  readOnly={!canEdit}
                  value={this.state.data.pilotLeave === 1}
                  style={{ height: 30, width: 60 }}
              />
            </Item>
          </InputGroup>
          {this.state.data.pilotBerth === 1 || this.state.data.pilotLeave === 1 ? (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>引水费:</Label>
                  <InputItem
                      editable={canEdit}
                      value={this.state.data.pilotFeeBerth}
                  />
                </Item>
              </InputGroup>
          ) : null}
          {this.state.data.pilotBerth === 1 || this.state.data.pilotLeave === 1 ? (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>引水说明:</Label>
                  <Selector
                      disabled={!canEdit}
                      value={validPilotReason(this.state.data.pilotMemoBerth)}
                  />
                </Item>
              </InputGroup>
          ) : null}
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>开关舱数:</Label>
              <InputItem
                  value={this.state.data.openDoorFee}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>解系缆:</Label>
              <InputItem
                  value={this.state.data.cableFee}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>停泊时间:</Label>
              <InputItem
                  value={this.state.data.parkTime}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>停泊费:</Label>
              <InputItem
                  value={this.state.data.parkingFee}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>代理费:</Label>
              <InputItem
                  value={this.state.data.agencyFee}
                  editable={canEdit}
              />
            </Item>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>淡水费:</Label>
              <InputItem
                  value={this.state.data.waterFee}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>其它费:</Label>
              <InputItem
                  value={this.state.data.otherFee}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
          {portName == '东江仓' ? <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="清道费:"
              value={this.state.data.clearanceFee}
              editable={canEdit}
            />
          </InputGroup> : null}
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>费用备注:</Label>
              <InputItem
                  value={this.state.data.feeMemo}
                  editable={canEdit}
              />
            </Item>
          </InputGroup>
        </ExtendCell>
    );
  }

  render() {
    return (
        <Container style={{ backgroundColor: '#E0E0E0' }}>
          <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
            <Form style={{ paddingBottom: this.readOnly ? 0 : 60 }}>
              {this.renderLoadingInformation()}
              {this.renderCostInformation()}
              <Text style={{
                textAlign: 'center', padding: 8, fontSize: 12, color: '#969696',
              }}
              >
                ———— 已经到底了 ————
              </Text>
            </Form>
          </Content>
        </Container>
    );
  }
}

HistoryNewsPaper.navigationOptions = {
  title: '开航报',
};

HistoryNewsPaper.propTypes = {
  navigation: PropTypes.object.isRequired,
  getSailNewspaperDetailPromise: PropTypes.func.isRequired,
  popDetail: PropTypes.object,
};
HistoryNewsPaper.defaultProps = {
  popDetail: {},
};

const mapStateToProps = createStructuredSelector({
  popDetail: makeSelectSailNewspaperDetail(),
  isLoading: makeSelectSailNewspaperLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSailNewspaperDetailPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryNewsPaper);
