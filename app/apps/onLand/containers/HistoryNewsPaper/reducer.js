import {
  getSailNewspaperDetailRoutine,
} from './actions';

const defaultState = {
  popDetail: undefined,
  loading: false,
  error: null,
  defaultValues: undefined,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取船报信息
    case getSailNewspaperDetailRoutine.TRIGGER:
      return { ...state, loading: true };
    case getSailNewspaperDetailRoutine.SUCCESS:
      return { ...state, popDetail: action.payload };
    case getSailNewspaperDetailRoutine.FAILURE:
      return { ...state, popDetail: undefined, error: action.payload };
    case getSailNewspaperDetailRoutine.FULFILL:
      return { ...state, loading: false };
    default:
      return state;
  }
}
