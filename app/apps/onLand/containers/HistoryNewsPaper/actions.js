import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_POP_DETAIL,
} from './constants';

export const getSailNewspaperDetailRoutine = createRoutine(GET_POP_DETAIL);
export const getSailNewspaperDetailPromise = promisifyRoutine(getSailNewspaperDetailRoutine);
