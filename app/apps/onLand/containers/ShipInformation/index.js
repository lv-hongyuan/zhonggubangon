import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import HeaderButtons from 'react-navigation-header-buttons';
import { connect } from 'react-redux';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  Container, Content, View, Item, Label, Text,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { ROUTE_SHIP_CERTIFICATE } from '../../RouteConstant';
import { getShipInformationPromise } from './actions';
import myTheme from '../../../../Themes';
import { makeShipInformation, makeIsLoading } from './selectors';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';

const styles = StyleSheet.create({
  row: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 10,
    flex: 1,
  },
  container: {
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
  },
});

/**
 * 船舶信息页面
 * Created by jianzhexu on 2018/3/29
 */
@screenHOC
class ShipInformation extends React.PureComponent {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '船舶信息'),
    headerRight: (
      <HeaderButtons>
        <HeaderButtons.Item
          title="船舶证书"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={navigation.getParam('showCertificate')}
        />
      </HeaderButtons>
    ),
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getShipInformationPromise: PropTypes.func.isRequired,
    data: PropTypes.object,
  };

  static defaultProps = {
    data: {},
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.navigation.setParams({
      showCertificate: this.showCertificate,
    });
    this.reLoadingPage();
  }

  showCertificate = () => {
    const shipId = this.props.navigation.getParam('shipId');
    this.props.navigation.navigate(ROUTE_SHIP_CERTIFICATE, { shipId });
  };

  reLoadingPage = () => {
    requestAnimationFrame(() => {
      const shipId = this.props.navigation.getParam('shipId');
      this.props.getShipInformationPromise(shipId)
        .then(() => {
        })
        .catch(() => {
        });
    });
  };

  render() {
    const {
      zwcm, ywcm, hh, imo, yx, syr, syrdzdh, jyr, jyrdzdh, length, phoneNum,
      contractWx, contractTon, zzd, teu, tgteu, edteu, gp20, gp40, zdw,
      jdw, tgdw, speed, oil, fuel, rhy,
    } = (this.props.data || {});

    return (
      <Container theme={myTheme}>
        <Content>
          <View style={styles.container}>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>中文船名:</Label>
                <Text style={commonStyles.text}>{zwcm}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>英文船名:</Label>
                <Text style={commonStyles.text}>{ywcm}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>呼号:</Label>
                <Text style={commonStyles.text}>{hh}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>IMO号:</Label>
                <Text style={commonStyles.text}>{imo}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>邮箱:</Label>
                <Text style={commonStyles.text}>{yx}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>船舶所有人:</Label>
                <Text style={commonStyles.text}>{syr}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>所有人地址电话:</Label>
                <Text style={commonStyles.text}>{syrdzdh}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>船舶经营人:</Label>
                <Text style={commonStyles.text}>{jyr}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>经营人地址电话:</Label>
                <Text style={commonStyles.text}>{jyrdzdh}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>总长度:</Label>
                <Text style={commonStyles.text}>{length}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>手机号:</Label>
                <Text style={commonStyles.text}>{phoneNum}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>合同稳性:</Label>
                <Text style={commonStyles.text}>{contractWx}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>合同载重吨:</Label>
                <Text style={commonStyles.text}>{contractTon}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>载重吨:</Label>
                <Text style={commonStyles.text}>{zzd}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>载箱teu:</Label>
                <Text style={commonStyles.text}>{teu}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>提供teu:</Label>
                <Text style={commonStyles.text}>{tgteu}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>额定teu:</Label>
                <Text style={commonStyles.text}>{edteu}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>20箱量:</Label>
                <Text style={commonStyles.text}>{gp20}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>40箱量:</Label>
                <Text style={commonStyles.text}>{gp40}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>总吨位:</Label>
                <Text style={commonStyles.text}>{zdw}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>净吨位:</Label>
                <Text style={commonStyles.text}>{jdw}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>提供吨位:</Label>
                <Text style={commonStyles.text}>{tgdw}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>航速:</Label>
                <Text style={commonStyles.text}>{speed}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>重油耗:</Label>
                <Text style={commonStyles.text}>{fuel}</Text>
              </Item>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>轻油耗:</Label>
                <Text style={commonStyles.text}>{oil}</Text>
              </Item>
            </View>
            <View style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>润滑油:</Label>
                <Text style={commonStyles.text}>{rhy}</Text>
              </Item>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeShipInformation(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipInformationPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipInformation);
