import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_SHIP_INFORMATION,
} from './constants';


export const getShipInformationRoutine = createRoutine(GET_SHIP_INFORMATION);
export const getShipInformationPromise = promisifyRoutine(getShipInformationRoutine);
