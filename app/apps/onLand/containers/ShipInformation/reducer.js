import { getShipInformationRoutine } from './actions';

const initState = {
  data: undefined,
  loading: false,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipInformationRoutine.TRIGGER:
      return { ...state, data: undefined, loading: true };
    case getShipInformationRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getShipInformationRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getShipInformationRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
