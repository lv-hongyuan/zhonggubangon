import { createSelector } from 'reselect';

const selectShipInformationDomain = () => state => state.shipInformation;

const makeShipInformation = () => createSelector(
  selectShipInformationDomain(),
  (subState) => {
    console.debug(subState.data);
    return subState.data;
  },
);

const makeIsLoading = () => createSelector(
  selectShipInformationDomain(),
  (subState) => {
    console.debug(subState.loading);
    return subState.loading;
  },
);

export {
  makeShipInformation,
  makeIsLoading,
};
