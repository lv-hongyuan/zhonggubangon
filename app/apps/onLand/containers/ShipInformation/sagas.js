import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { getShipInformationRoutine } from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getShipInformation(action) {
  console.log(action);
  try {
    yield put(getShipInformationRoutine.request());
    const response = yield call(
      request,
      ApiFactory.getShipInfo(action.payload),
    );
    console.log('getShipInfo', response.toString());
    yield put(getShipInformationRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipInformationRoutine.failure(e));
  } finally {
    yield put(getShipInformationRoutine.fulfill());
  }
}

export function* getShipInformationSaga() {
  yield takeLatest(getShipInformationRoutine.TRIGGER, getShipInformation);
}

// All sagas to be loaded
export default [
  getShipInformationSaga,
];
