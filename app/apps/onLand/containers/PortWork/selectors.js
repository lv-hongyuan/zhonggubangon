import { createSelector } from 'reselect/es';
import { selectInstructionsDomain } from '../Instructions/selectors';

const selectPortWorkDomain = () => state => state.portWork;

const makeWorkPopList = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState);
  return subState.workList;
});

const makeSelectRefreshState = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeLoading = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

const makeOperatingItemId = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState.operatingItemId);
  return subState.operatingItemId;
});

const makeOperatingShip = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState.operatingShip);
  return subState.operatingShip;
});

const makePortWorkBadge = () => createSelector([selectInstructionsDomain()], (instructions) => {
  console.debug(instructions.badge);
  return instructions.badge;
});

export {
  makeWorkPopList,
  makeLoading,
  makeOperatingItemId,
  makeSelectRefreshState,
  makeOperatingShip,
  makePortWorkBadge,
};
