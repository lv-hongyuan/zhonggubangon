import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_WORK_LIST,
  RECEIVE_SHIP,
  RECEIVE_SHIP_RESULT,
  CLEAN,
  SET_OPERATING_SHIP,
} from './constants';

export const getPopListRoutine = createRoutine(GET_WORK_LIST);
export const getPopListPromise = promisifyRoutine(getPopListRoutine);

// export const receiveShipRoutine = createRoutine(RECEIVE_SHIP)
// export const receiveShipPromise = promisifyRoutine(receiveShipRoutine)

export function receiveShipAction(id) {
  return {
    type: RECEIVE_SHIP,
    payload: id,
  };
}

export function receiveShipResultAction(workList, success) {
  return {
    type: RECEIVE_SHIP_RESULT,
    payload: {
      workList,
      success,
    },
  };
}

export function setOperatingShip(item) {
  return {
    type: SET_OPERATING_SHIP,
    payload: item,
  };
}

export function cleanAction() {
  return {
    type: CLEAN,
  };
}
