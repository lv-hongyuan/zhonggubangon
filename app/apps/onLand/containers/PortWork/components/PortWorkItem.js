import React from 'react';
import PropTypes from 'prop-types';
import { Platform,StyleSheet, PixelRatio,} from 'react-native';
import {
  Button, Spinner, Grid, Col, Badge,  View, Text,
} from 'native-base';
import _ from 'lodash';
import { portWorkStateTitle } from '../constants';
import AppStorage from '../../../../../utils/AppStorage';
import { defaultFormat } from '../../../../../utils/DateFormat';
import Svg from '../../../../../components/Svg';
import CarouselLabel from '../../../../../components/CarouselLabel';
const platform = Platform.OS;
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  info: {
    // flex: 1,
    // paddingLeft: 20,
    // paddingTop: 10,
    // paddingRight: 10,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  badge: {
    alignSelf: 'center',
    height: 18,
    minWidth: 18,
    borderRadius: 9,
    paddingTop: 0,
    paddingLeft: 0,
    paddingBottom: 0,
    paddingRight: 0,
  },
  badgeText: {
    lineHeight: 18,
  },
  btnContainer: {
    height: 60,
    // borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
  },
  leftButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  roundButton: {
    height: 40,
    flex: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 14,
    color: 'white',
  },
  infoContainer: {
    flexDirection: 'row',
    borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
  },
  infoButtonContainer: {
    borderLeftWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
    paddingLeft: 0,
    paddingRight: 0,
  },
  infoButton: {
    height: 30,
    paddingTop: 0,
    paddingBottom: 0,
    paddingLeft: 10,
    paddingRight: 20,
    flex: 1,
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
  },
  infoButtonText: {
    color: '#535353',
    fontSize: 14,
  },
});

class PortWorkItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPortAttention = () => {
    if (this.props.onPortAttention) this.props.onPortAttention(this.props.item);
  };

  showShipInfo = () => {
    if (this.props.onShowShipInfo) this.props.onShowShipInfo(this.props.item);
  };

  renderShipInformation() {
    return (
      <Button
        transparent
        style={styles.infoButton}
        onPress={this.showShipInfo}
      >
        <Text style={styles.infoButtonText}>船舶信息</Text>
        <Svg icon="infomation" size={18} color="#DF001B" />
      </Button>
    );
  }

  renderPortAttention(attendNum) {
    return attendNum ? (
      <Button
        transparent
        style={[styles.infoButton, {
          borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
          borderColor: '#E0E0E0',
        }]}
        onPress={this.onPortAttention}
      >
        <Text style={styles.infoButtonText}>注意事项</Text>
        <Badge style={styles.badge}>
          <Text style={styles.badgeText}>{attendNum}</Text>
        </Badge>
      </Button>
    ) : null;
  }

  renderButtons(item) {
    const {
      portSysUser, toDoState, toDoMemo, statePort, userName,
    } = item || {};
    if (AppStorage.sysUser !== portSysUser) {
      return (
        <View style={styles.btnContainer}>
          <Button
            style={[styles.roundButton, { backgroundColor: '#FBB03B' }]}
            disabled={this.props.operating}
            onPress={() => {
              this.props.onReceiveShip(item);
            }}
          >
            {this.props.operating ? (
              <Spinner color="#DF001B" />
            ) : (
              <Text style={styles.buttonText}>{userName || '接船'}</Text>
            )}
          </Button>
        </View>
      );
    }
    return (
      <View style={styles.btnContainer}>
        <Button
          block
          style={[styles.leftButton, {
            backgroundColor: '#FBB03B',
            overflow: 'hidden',
          }]}
          onPress={() => {
            this.props.onEdit(item);
          }}
        >
          {toDoState === 1 ? (
            <CarouselLabel
              bgViewStyle={{
                marginLeft: 20,
                marginRight: 20,
              }}
              textContainerHeight={30}
              speed={50}
              text={!_.isEmpty(toDoMemo) ? toDoMemo : '您有阶段信息需要录入，请及时录入'}
              textStyle={{
                fontSize: 13,
                color: 'white',
              }}
            />
          ) : (
            <Text style={styles.buttonText}>{portWorkStateTitle(statePort) || '---'}</Text>
          )}
        </Button>
        <Button
          block
          style={[styles.rightButton, {
            backgroundColor: '#DC001B',
            overflow: 'hidden',
          }]}
          onPress={() => {
            this.props.onSailNewspaper(item);
          }}
        >
          {toDoState === 2 ? (
            <CarouselLabel
              bgViewStyle={{
                marginLeft: 20,
                marginRight: 20,
              }}
              textContainerHeight={30}
              speed={50}
              text={!_.isEmpty(toDoMemo) ? toDoMemo : '请及时录入开航报信息'}
              textStyle={{
                fontSize: 13,
                color: 'white',
              }}
            />
          ) : (
            <Text style={styles.buttonText}>开航报</Text>
          )}
        </Button>
      </View>
    );
  }

  render() {
    const item = this.props.item || {};
    const {
      shipName, voyageCode, line, ata, eta, atb, etb, atd, etd, portNum, portName, attendNum,
    } = item;
    const ports = (line || '').split('-')
      .filter(str => str !== '' && str !== '-');
    if (ports.length === 0 && portName) {
      ports.push(portName);
    }
    return (
      <View style={styles.container}>
        <View style={styles.info}>
          <View style={{
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 10,
            paddingBottom: 5,
          }}
          >
            <Grid>
              <Col>
                <Text style={styles.label}>
                  船名：
                  {shipName || '-----------'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.label}>
                  航次：
                  {voyageCode || '-----------'}
                </Text>
              </Col>
            </Grid>
            <Text style={styles.label}>
              航线：
              {ports.map((port, index) => {
                const style = [styles.label];
                if (portNum && portNum > 1 && port === portName) {
                  style.push({ color: 'red' });
                }
                return (
                  <Text style={[style,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]} key={port}>
                    {port}
                    {index < ports.length - 1 && <Text style={[styles.label,{fontSize:platform === 'ios' ? 14 : 14 / PixelRatio.getFontScale()}]}> - </Text>}
                  </Text>
                );
              })}
            </Text>
          </View>
          <View style={styles.infoContainer}>
            <View style={{
              flex: 1,
              justifyContent: 'space-between',
              paddingLeft: 20,
              paddingTop: 5,
              paddingBottom: 5,
            }}
            >
              <Text style={styles.label}>
                {
                  ata ? `实际抵锚：${defaultFormat(ata)}` : `预计抵锚：${defaultFormat(eta) || '-----------'}`
                }
              </Text>
              <Text style={styles.label}>
                {
                  atb ? `实际停靠：${defaultFormat(atb)}` : `预计停靠：${defaultFormat(etb) || '-----------'}`
                }
              </Text>
              <Text style={styles.label}>
                {
                  atd ? `实际离港：${defaultFormat(atd)}` : `预计离港：${defaultFormat(etd) || '-----------'}`
                }
              </Text>
            </View>
            <View style={styles.infoButtonContainer}>
              {this.renderShipInformation()}
              {this.renderPortAttention(attendNum)}
            </View>
          </View>
        </View>
        {this.renderButtons(item)}
      </View>
    );
  }
}

PortWorkItem.propTypes = {
  item: PropTypes.object.isRequired,
  operating: PropTypes.bool.isRequired,
  onReceiveShip: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onSailNewspaper: PropTypes.func.isRequired,
  onShowShipInfo: PropTypes.func.isRequired,
  onPortAttention: PropTypes.func.isRequired,
};

export default PortWorkItem;
