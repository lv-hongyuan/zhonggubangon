import { call, put, takeLatest } from 'redux-saga/effects';
import { RECEIVE_SHIP } from './constants';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { receiveShipResultAction } from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getPopListRoutine } from './actions';

function* getPopList(action) {
  let shipName = action.payload.shipName
  let lineName = action.payload.lineName
  try {
    yield put(getPopListRoutine.request());
    const response = yield call(request, ApiFactory.getWorkList());
    if((shipName === '' && lineName === '') || response.dtoList.length === 0){
      yield put(getPopListRoutine.success(response.dtoList || []));
    } else {
      let list = response.dtoList
      if(shipName != '' && lineName != '' ){
        let dtoList = list.filter((obj)=>{
          return shipName === obj.shipName && lineName === obj.voyageCode
        })
        yield put(getPopListRoutine.success(dtoList || []));
      }else if(shipName != '' && lineName == ''){
        let dtoList = list.filter((obj)=>{
          return shipName === obj.shipName
        })
        yield put(getPopListRoutine.success(dtoList || []));
      }else {
        let dtoList = list.filter((obj)=>{
          return lineName === obj.voyageCode
        })
        yield put(getPopListRoutine.success(dtoList || []));
      }
    }
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPopListRoutine.failure(e));
  } finally {
    yield put(getPopListRoutine.fulfill());
  }
}

export function* getPopListSaga() {
  yield takeLatest(getPopListRoutine.TRIGGER, getPopList);
}

function* receiveShip(action) {
  console.log(action);
  try {
    const response = yield call(
      request,
      ApiFactory.receiveShip(action.payload),
    );
    console.log('receiveShip', response.toString());
    yield put(receiveShipResultAction(response.dtoList, true));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(receiveShipResultAction(undefined, false));
  }
}

export function* receiveShipSaga() {
  yield takeLatest(RECEIVE_SHIP, receiveShip);
}

// All sagas to be loaded
export default [getPopListSaga, receiveShipSaga];
