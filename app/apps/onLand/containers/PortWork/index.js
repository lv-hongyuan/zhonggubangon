import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView,StyleSheet,TouchableOpacity,Text,Modal } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import commonStyles from "../../../../common/commonStyles";
import InputItem from '../../../../components/InputItem';
import { Container, View, InputGroup } from 'native-base';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { cleanAction, receiveShipAction, setOperatingShip, getPopListPromise } from './actions';
import { makeWorkPopList, makeOperatingItemId, makeSelectRefreshState } from './selectors';
import PortWorkItem from './components/PortWorkItem';
import {
  ROUTE_PORT_WORK_DETAIL,
  ROUTE_PORT_SAIL_NEWSPAPER,
  ROUTE_SHIP_INFORMATION,
  ROUTE_PORT_ATTENTION,
  ROUTE_HISTORY_RECORD,
} from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import AlertView from '../../../../components/Alert';
import AppStorage from '../../../../utils/AppStorage';
import screenHOC from '../../../../components/screenHOC';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import Loading from '../../../../components/Loading'
import CarouselLabel from '../../../../components/CarouselLabel';

@screenHOC
class PortWork extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchView:false,
      searchShipName:'',
      searchLineName:'',
      loading:false,
      showTitle:false
    };
  }

  componentDidMount() {
    this.onFooterRefresh();
  }

  componentWillUnmount() {
    this.props.dispatch(cleanAction());
    this.timer && clearTimeout(this.timer);
  }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(this.getcondition());
  };

  //!获取筛选条件
  getcondition(){
    let condition = {}
    condition.shipName = this.state.searchShipName
    condition.lineName = this.state.searchLineName
    return condition
  }

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(this.getcondition());
  };

  onReceiveShip = (item) => {
    const { portSysUser, voyageCode } = item;
    if (!voyageCode) {
      AlertView.show({ message: '没有航次，无法接船' });
      return;
    }
    if (portSysUser && AppStorage.sysUser !== portSysUser) {
      AlertView.show({
        message: '此船是他人船只，确认要接船吗？',
        showCancel: true,
        confirmAction: () => {
          this.props.dispatch(receiveShipAction(item.id));
        },
      });
    } else {
      this.props.dispatch(receiveShipAction(item.id));
    }
  };

  onShowShipInfo = (item) => {
    this.props.navigation.navigate(ROUTE_SHIP_INFORMATION, { shipId: item.shipId });
  };

  onPortAttention = (item) => {
    this.props.navigation.navigate(ROUTE_PORT_ATTENTION, {
      popId: item.id,
      portName: item.portName,
      page: 'portWork',
    });
  };

  showDetail = (item) => {
    this.props.dispatch(setOperatingShip(item));
    this.props.navigation.navigate(ROUTE_PORT_WORK_DETAIL, {
      popId: item.id,
      shipName: item.shipName,
      portName: item.portName,
    });
  };

  showSailNewspaper = (item) => {
    this.props.navigation.navigate(ROUTE_PORT_SAIL_NEWSPAPER, {
      popId: item.id,
      shipName: item.shipName,
      voyageCode: item.voyageCode,
      portName: item.portName,
    });
  };

  didFocus = () => {
    // 每次进页面刷新列表
    this.onFooterRefresh();
  };

  loadList(condition) {
    this.props.getPopList(condition)
      .then(() => {
        this.setState({loading:false})
      })
      .catch(() => {});
  }

  renderItem = ({ item }) => (
    <PortWorkItem
      item={item}
      operating={item.id === this.props.operatingItemId}
      onEdit={this.showDetail}
      onSailNewspaper={this.showSailNewspaper}
      onReceiveShip={this.onReceiveShip}
      onShowShipInfo={this.onShowShipInfo}
      onPortAttention={this.onPortAttention}
    />
  );

  //!上方搜索区域
  renderSearchView(){
    return <Modal animationType="none" transparent={true} visible={this.state.showSearchView}>
      <View style={{flex:1,justifyContent: 'center',alignItems: 'center',backgroundColor: 'rgba(0,0,0,0.3)',}}>
        <View style={styles.searchView}>
          <InputGroup style={[commonStyles.inputGroup,{marginTop:10,marginHorizontal:20}]}>
            <InputItem
              label="船名:"
              returnKeyType="search"
              value={this.state.searchShipName}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                newText = text.replace(/\s/g,"")
                this.setState({ searchShipName: newText });
              }}
            />
          </InputGroup>
          <InputGroup style={[commonStyles.inputGroup,{marginTop:10,marginHorizontal:20}]}>
            <InputItem
              label="航次:"
              returnKeyType="search"
              value={this.state.searchLineName}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                newText = text.replace(/\s/g,"").toUpperCase()
                this.setState({ searchLineName: newText });
              }}
            />
          </InputGroup>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <TouchableOpacity 
                style={[styles.searchButton,{backgroundColor:'#FBB03B'}]}
                onPress={()=>{
                  this.setState({
                    showSearchView:false,
                  }
                )}}
              >
                <Text style={{color:'#fff',fontSize:16}}>取消</Text>
              </TouchableOpacity>
              <TouchableOpacity 
                style={[styles.searchButton]}
                onPress={()=>{
                  this.setState({loading:true,showSearchView:false})
                  this.onFooterRefresh();
                  if(this.state.searchLineName != '' || this.state.searchShipName != ''){
                    this.setState({showTitle:true})
                  }
                }}
              >
                <Text style={{color:'#fff',fontSize:16}}>确定</Text>
              </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal> 
  }

  //!上方滚动提示信息
  rederTitle(){
    if(!this.state.showTitle) return
    let shipName = this.state.searchShipName ? '船名:' + this.state.searchShipName : '';
    let lineName = this.state.searchLineName ? '   航次:' + this.state.searchLineName : '';
    let titleText = '当前筛选条件:    ' + shipName + lineName;
    return <View style={styles.title}>
      <Feather
        name={'alert-triangle'}
        size={20}
        style={{color: '#fff',marginRight:5}}
      />
      {titleText.length > 30 ? <CarouselLabel
        bgViewStyle={{
          overflow: 'hidden',
          width: '83%',
        }}
        speed={50}
        text={titleText}
        textStyle={{
          fontSize: 13,
          color: '#fff',
          width: titleText * 14
        }}
      /> : <Text style={{color:'#fff'}}>{titleText}</Text>}
      
      <TouchableOpacity 
        style={{
          width:40,height:40,position:'absolute',
          right:0,justifyContent:'center',alignItems:'center'
        }}
        onPress={()=>{
          this.setState({
            showTitle:false,
            searchLineName:'',
            searchShipName:'',
            loading:true
          })
          this.timer = setTimeout(() => {
            this.onFooterRefresh();
          }, 20);
          
        }}
      >
        <View style={{
          borderColor:'#fff',borderWidth:1,borderRadius:10,
          width:20,height:20,justifyContent:'center',alignItems:'center'
        }}>
          <Ionicons
            name={'md-close'}
            size={20}
            style={{color: '#fff',marginLeft:1,marginBottom:1}}
          />
        </View>
        
      </TouchableOpacity>
    </View>
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ flex: 1 }}>
          {this.rederTitle()}
          {this.renderSearchView()}
          {this.state.loading && <Loading/>}
          <RefreshListView
            style={{ width: '100%' }}
            data={this.props.workList || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => (<View style={{ height: 3 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 3 }} />)}
          />
          <TouchableOpacity 
            style={styles.search}
            onPress={()=>{
              this.setState({showSearchView:!this.state.showSearchView})
            }}
          >
            <Ionicons
              name={'ios-search'}
              size={40}
              style={{color: '#DC001B',marginTop:4,marginLeft:3}}
            />
          </TouchableOpacity>
          
        </SafeAreaView>
      </Container>
    );
  }
}

PortWork.navigationOptions = ({ navigation }) => ({
  title: '港口作业',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="历史记录"
        buttonStyle={{ fontSize: 14, color: '#ffffff',marginBottom:10 }}
        onPress={() => { navigation.navigate(ROUTE_HISTORY_RECORD); }}
      />
    </HeaderButtons>
  ),
});

PortWork.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  getPopList: PropTypes.func.isRequired,
  workList: PropTypes.array,
  operatingItemId: PropTypes.number,
  refreshState: PropTypes.number.isRequired,
};
PortWork.defaultProps = {
  workList: [],
  operatingItemId: undefined,
};

const mapStateToProps = createStructuredSelector({
  workList: makeWorkPopList(),
  operatingItemId: makeOperatingItemId(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPopList: getPopListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortWork);

const styles = StyleSheet.create({
  search: {
    width:50,
    height:50,
    borderColor:'#eee',
    borderWidth:1,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#fff',
    position:'absolute',
    right:10,
    bottom:100,
    borderRadius:5
  },
  searchView: {
    backgroundColor:'#fff',
    width:'80%',
    borderRadius:5,
    justifyContent:'flex-end',
    overflow:'hidden'
  },
  searchButton: {
    height:45,
    width:'50%',
    backgroundColor:'#DC001B',
    marginTop:15,
    justifyContent:'center',
    alignItems:'center',
  },
  title: {
    height:40,
    backgroundColor:'rgb(250,175,75)',
    width:'100%',
    alignItems:'center',
    paddingHorizontal:10,
    flexDirection:'row'
  }
})
