import fixIdList from '../../../../utils/fixIdList';
import {
  CLEAN,
  RECEIVE_SHIP,
  RECEIVE_SHIP_RESULT,
  SET_OPERATING_SHIP,
} from './constants';
import { getPopListRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  userInfo: undefined,
  workList: [],
  isLoading: false,
  operatingItemId: null,
  editingShip: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态
    case getPopListRoutine.TRIGGER:
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getPopListRoutine.SUCCESS:
      return {
        ...state, workList: fixIdList(action.payload), error: undefined, refreshState: RefreshState.NoMoreData,
      };
    case getPopListRoutine.FAILURE:
      return {
        ...state, workList: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPopListRoutine.FULFILL:
      return { ...state, loading: false };

    case RECEIVE_SHIP:
      return { ...state, isLoading: true, operatingItemId: action.payload };
    case RECEIVE_SHIP_RESULT:
      return {
        ...state, ...action.payload, isLoading: false, operatingItemId: null,
      };

    case SET_OPERATING_SHIP:
      return { ...state, operatingShip: action.payload };

    case CLEAN:
      return {
        ...state,
        isLoading: false,
        operatingItemId: null,
        refreshState: RefreshState.Idle,
      };
    default:
      return state;
  }
}
