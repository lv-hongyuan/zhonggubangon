/*
 *
 * PortWork constants
 *
 */
export const GET_WORK_LIST = 'onLand/PortWork/GET_WORK_LIST';
export const RECEIVE_SHIP = 'onLand/PortWork/RECEIVE_SHIP';
export const RECEIVE_SHIP_RESULT = 'onLand/PortWork/RECEIVE_SHIP_RESULT';
export const CLEAN = 'onLand/PortWork/CLEAN';
export const SET_OPERATING_SHIP = 'onLand/PortWork/SET_OPERATING_SHIP';

// 港口现场APP状态
export const PortWorkState = {
  plan: 10, // 计划
  berthing: 20, // 靠泊
  operating: 30, // 作b业
  unBerthing: 40, // 离泊
  complete: 50, // 完成
};

export function portWorkStateTitle(state) {
  let title;
  switch (state) {
    case PortWorkState.plan:
      title = '计划';
      break;
    case PortWorkState.berthing:
      title = '靠泊';
      break;
    case PortWorkState.operating:
      title = '作业';
      break;
    case PortWorkState.unBerthing:
      title = '离泊';
      break;
    case PortWorkState.complete:
      title = '完成';
      break;
    default:
      break;
  }
  return title;
}
