import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
  },
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    // fontSize: 12,
    color: '#535353',
    // margin: 2,
  },
  text: {
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
});

class EmergencyItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const item = this.props.item || {};
    return (
      <TouchableHighlight style={{ marginTop: 8 }} onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={styles.row}>
            <Text style={styles.label}>事发港口：</Text>
            <Text style={styles.text}>{item.portName || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>事件类型：</Text>
            <Text style={styles.text}>{item.typeChildText || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>事件描述：</Text>
            <Text style={styles.text}>{item.describe || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>开始时间：</Text>
            <Text style={styles.text}>{item.startTime > 0 ? defaultFormat(item.startTime) : '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>结束时间：</Text>
            <Text style={styles.text}>{item.endTime > 0 ? defaultFormat(item.endTime) : '-----------'}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

EmergencyItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};
EmergencyItem.defaultProps = {
  onSelect: undefined,
};

export default EmergencyItem;
