import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getEmergencyListRoutine, deleteEmergencyRoutine } from './actions';

export function* getEmergencies(action) {
  console.log(action);
  const { popId, range } = action.payload;
  try {
    yield put(getEmergencyListRoutine.request());
    const response = yield call(request, ApiFactory.getEventList(popId, range));
    console.log('getEventList', response);
    yield put(getEmergencyListRoutine.success({ range, list: response.dtoList }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getEmergencyListRoutine.failure({ range, error: e }));
  } finally {
    yield put(getEmergencyListRoutine.fulfill());
  }
}

export function* getEmergenciesSaga() {
  yield takeLatest(getEmergencyListRoutine.TRIGGER, getEmergencies);
}

function* deleteEmergency(action) {
  console.log(action);
  const { emergency, range } = action.payload;
  try {
    yield put(deleteEmergencyRoutine.request());
    const response = yield call(request, ApiFactory.deleteEvent(emergency.id));
    console.log('deleteEmergency', response);
    yield put(deleteEmergencyRoutine.success({ emergency, range }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    if (error.response && error.response._backcode + '' === '830') {
      yield put(deleteEmergencyRoutine.success({ emergency, range }));
    } else {
      yield put(deleteEmergencyRoutine.failure(error));
    }
  } finally {
    yield put(deleteEmergencyRoutine.fulfill());
  }
}

export function* deleteEmergencySaga() {
  yield takeLatest(deleteEmergencyRoutine.TRIGGER, deleteEmergency);
}

// All sagas to be loaded
export default [getEmergenciesSaga, deleteEmergencySaga];
