import { RefreshState } from '../../../../components/RefreshListView';
import { EventRange } from '../../common/Constant';
import { getEmergencyListRoutine, deleteEmergencyRoutine } from './actions';

const defaultState = {
  berthingEvents: [], // "未直靠原因"
  operatingEvents: [], // "影响作业原因"
  unBerthingEvents: [], // "影响离泊原因"
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

function suitableListParam(range, list) {
  const param = {};
  switch (range) {
    case EventRange.PORT_BERTHING:
      param.berthingEvents = list;
      break;
    case EventRange.PORT_OPERATION:
      param.operatingEvents = list;
      break;
    case EventRange.PORT_UN_BERTHING:
      param.unBerthingEvents = list;
      break;
    default:
      break;
  }
  return param;
}

function stateListParam(range, state) {
  switch (range) {
    case EventRange.PORT_BERTHING:
      return state.berthingEvents;
    case EventRange.PORT_OPERATION:
      return state.operatingEvents;
    case EventRange.PORT_UN_BERTHING:
      return state.unBerthingEvents;
    default:
      return [];
  }
}

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取突发事件
    case getEmergencyListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        loadingError: undefined,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getEmergencyListRoutine.SUCCESS: {
      const { range, list } = action.payload;
      return {
        ...state,
        loadingError: undefined,
        refreshState: RefreshState.NoMoreData,
        ...suitableListParam(range, list),
      };
    }
    case getEmergencyListRoutine.FAILURE: {
      const { range, error } = action.payload;
      return {
        ...state,
        refreshState: RefreshState.Failure,
        loadingError: error,
        ...suitableListParam(range, []),
      };
    }
    case getEmergencyListRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 删除突发事件
    case deleteEmergencyRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case deleteEmergencyRoutine.SUCCESS: {
      const { range, emergency } = action.payload;
      let newList = [];
      if (emergency) {
        newList = stateListParam(range, state).filter(item => item !== emergency);
      } else {
        newList = stateListParam(range, state);
      }
      return {
        ...state,
        ...suitableListParam(range, newList),
      };
    }
    case deleteEmergencyRoutine.FAILURE:
      return { ...state, loadingError: action.payload };
    case deleteEmergencyRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
