/*
 *
 * Emergencies constants
 *
 */
export const GET_EMERGENCY_LIST = 'onLand/PortWork/GET_EMERGENCY_LIST';
export const DELETE_EMERGENCY = 'onLand/PortWork/DELETE_EMERGENCY';
