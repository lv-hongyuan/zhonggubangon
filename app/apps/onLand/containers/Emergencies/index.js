import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, TouchableHighlight, Text,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { Container } from 'native-base';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getEmergencyListPromise, deleteEmergencyPromise } from './actions';
import EmergencyItem from './components/EmergencyItem';
import { ROUTE_EMERGENCY_EDIT } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';

import {
  makeIsLoading,
  makeSelectEmergencies,
  makeSelectRefreshState,
} from './selectors';
// import AlertView from '../../../../components/Alert';
import screenHOC from '../../../../components/screenHOC';
import { EventRange } from '../../common/Constant';

const styles = StyleSheet.create({
  actionsContainer: {
    flex: 1,
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionButton: {
    width: 80,
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  actionButtonText: {
    textAlign: 'center',
    color: 'white',
  },
});

@screenHOC
class Emergencies extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};

    this.props.navigation.setParams({
      addEmergency: this.addEmergency,
    });
  }

  componentDidMount() {
    this.onFooterRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    // if (item.endTime > 0) {
    //   AlertView.show({ message: '已结束的事件无法编辑' });
    // } else {
    this.props.navigation.navigate(
      ROUTE_EMERGENCY_EDIT,
      {
        id: this.props.navigation.getParam('id'),
        range: this.range,
        item,
      },
    );
    // }
  };

  onDelete = (item) => {
    // if (item.endTime > 0) {
    //   AlertView.show({ message: '已结束的事件无法删除' });
    // } else {
    this.props.deleteEmergencyPromise({
      range: this.range,
      emergency: item,
    })
      .then(() => {})
      .catch(() => {});
    // }
  };

  get range() {
    return this.props.navigation.getParam('range', EventRange.PORT_BERTHING);
  }

  willFocus = () => {
    // 每次进页面刷新列表
    this.onFooterRefresh();
  };

  loadList() {
    this.props.getEmergencyListPromise({
      popId: this.props.navigation.getParam('id'),
      range: this.range,
    })
      .then(() => {})
      .catch(() => {});
  }

  addEmergency = () => {
    this.props.navigation.navigate(
      ROUTE_EMERGENCY_EDIT,
      {
        id: this.props.navigation.getParam('id'),
        range: this.range,
      },
    );
  };

  renderItem = ({ item }) => (
    <EmergencyItem item={item} onSelect={this.onSelect} />
  );

  renderQuickActions = ({ item }) => (
    <View style={styles.actionsContainer}>
      <TouchableHighlight
        style={styles.actionButton}
        onPress={() => {
          this.onDelete(item);
        }}
      >
        <View style={styles.actionButton}>
          <Text style={styles.actionButtonText}>删除</Text>
        </View>
      </TouchableHighlight>
    </View>
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <RefreshListView
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          renderQuickActions={this.renderQuickActions}
          maxSwipeDistance={80}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
        />
      </Container>
    );
  }
}

Emergencies.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '情况汇总'),
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="创建"
        buttonStyle={{
          fontSize: 14,
          color: '#ffffff',
        }}
        onPress={navigation.getParam('addEmergency')}
      />
    </HeaderButtons>
  ),
});

Emergencies.propTypes = {
  navigation: PropTypes.object.isRequired,
  getEmergencyListPromise: PropTypes.func.isRequired,
  deleteEmergencyPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
Emergencies.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeSelectEmergencies(),
  isLoading: makeIsLoading(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getEmergencyListPromise,
      deleteEmergencyPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Emergencies);
