import { createSelector } from 'reselect/es';
import { EventRange } from '../../common/Constant';

const selectEmergenciesDomain = () => state => state.emergencies;

const makeBerthingEvents = () => createSelector(
  selectEmergenciesDomain(),
  (subState) => {
    console.debug(subState);
    return subState.berthingEvents;
  },
);

const makeBerthingEventsNum = () => createSelector(
  makeBerthingEvents(),
  events => events.length,
);

const makeUnFinishedBerthingEventsNum = () => createSelector(
  makeBerthingEvents(),
  events => events.reduce((pre, event) => (pre + (event.endTime ? 0 : 1)), 0),
);

const makeOperatingEvents = () => createSelector(
  selectEmergenciesDomain(),
  (subState) => {
    console.debug(subState);
    return subState.operatingEvents;
  },
);

const makeOperatingEventsNum = () => createSelector(
  makeOperatingEvents(),
  events => events.length,
);

const makeUnFinishedOperatingEventsNum = () => createSelector(
  makeOperatingEvents(),
  events => events.reduce((pre, event) => (pre + (event.endTime ? 0 : 1)), 0),
);

const makeUnBerthingEvents = () => createSelector(
  selectEmergenciesDomain(),
  (subState) => {
    console.debug(subState);
    return subState.unBerthingEvents;
  },
);

const makeUnBerthingEventsNum = () => createSelector(
  makeUnBerthingEvents(),
  events => events.length,
);

const makeUnFinishedUnBerthingEventsNum = () => createSelector(
  makeUnBerthingEvents(),
  events => events.reduce((pre, event) => (pre + (event.endTime ? 0 : 1)), 0),
);

const makeRange = () => createSelector(
  (state, props) => props.navigation.state.params,
  subState => subState.range,
);

const makeSelectEmergencies = () => createSelector(
  makeBerthingEvents(),
  makeOperatingEvents(),
  makeUnBerthingEvents(),
  makeRange(),
  (berthingEvents, operatingEvents, unBerthingEvents, range) => {
    switch (range) {
      case EventRange.PORT_BERTHING:
        return berthingEvents;
      case EventRange.PORT_OPERATION:
        return operatingEvents;
      case EventRange.PORT_UN_BERTHING:
        return unBerthingEvents;
      default:
        return [];
    }
  },
);

const makeSelectRefreshState = () => createSelector(selectEmergenciesDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectEmergenciesDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  makeBerthingEventsNum,
  makeUnFinishedBerthingEventsNum,
  makeOperatingEventsNum,
  makeUnFinishedOperatingEventsNum,
  makeUnBerthingEventsNum,
  makeUnFinishedUnBerthingEventsNum,
  makeSelectEmergencies,
  makeSelectRefreshState,
  makeIsLoading,
};
