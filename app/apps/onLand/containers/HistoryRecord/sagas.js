import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getHistoryRoutine } from './actions';

function* getHistoryRecord(action) {
  console.log(action);
  const { page, pageSize, ...rest } = (action.payload || {});
  try {
    yield put(getHistoryRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      if (!_.isEmpty(value)) {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getHistoryRecord(param));
    console.log(response);
    const { totalElements, content } = (response.dtoList || {});
    yield put(getHistoryRoutine.success({
      page, pageSize, list: (content || []), totalElements,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getHistoryRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getHistoryRoutine.fulfill());
  }
}

export function* getHistoryRecordSaga() {
  yield takeLatest(getHistoryRoutine.TRIGGER, getHistoryRecord);
}

// All sagas to be loaded
export default [getHistoryRecordSaga];
