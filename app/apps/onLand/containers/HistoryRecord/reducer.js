import { RefreshState } from '../../../../components/RefreshListView';
import { getHistoryRoutine } from './actions';

const defaultState = {
  list: [],
  userInfo: undefined,
  workList: [],
  success: false,
  isLoading: false,
  operatingItemId: null,
  editingShip: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case getHistoryRoutine.TRIGGER: {
      const { page } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getHistoryRoutine.SUCCESS: {
      const { page, pageSize, list } = action.payload;
      return {
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getHistoryRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getHistoryRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
