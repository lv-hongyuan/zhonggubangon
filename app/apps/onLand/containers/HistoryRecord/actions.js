import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_HISTORY_LIST } from './constants';

export const getHistoryRoutine = createRoutine(GET_HISTORY_LIST);
export const getHistoryPromise = promisifyRoutine(getHistoryRoutine);
