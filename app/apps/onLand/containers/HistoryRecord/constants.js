/*
 *
 * HistoryRecord constants
 *
 */
export const GET_HISTORY_LIST = 'onLand/HistoryRecord/GET_HISTORY_LIST';

// 港口现场APP状态
export const HistoryRecordState = {
  plan: 10, // 计划
  berthing: 20, // 靠泊
  operating: 30, // 作b业
  unBerthing: 40, // 离泊
  complete: 50, // 完成
};
