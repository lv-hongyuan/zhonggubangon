import { createSelector } from 'reselect';

const selectPortWorkDomain = () => state => state.historyRecord;

const makeSelectPortWorkList = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeLoading = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

const makeOperatingItemId = () => createSelector(selectPortWorkDomain(), (subState) => {
  console.debug(subState.operatingItemId);
  return subState.operatingItemId;
});

export {
  makeSelectPortWorkList,
  makeLoading,
  makeOperatingItemId,
  makeSelectRefreshState,
};
