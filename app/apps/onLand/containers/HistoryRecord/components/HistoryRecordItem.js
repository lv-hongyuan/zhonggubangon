import React from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, PixelRatio, TouchableOpacity,
} from 'react-native';
import { Grid, Col } from 'native-base';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  info: {
    // flex: 1,
    // paddingLeft: 20,
    // paddingTop: 10,
    // paddingRight: 10,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  infoContainer: {
    flexDirection: 'row',
    borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
  },
});

class HistoryRecordItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    return (
      <TouchableOpacity style={styles.container} onPress={this.props.onPress || undefined}>
        <View style={styles.info}>
          <View style={{
            paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 5,
          }}
          >
            <Grid>
              <Col>
                <Text style={styles.label}>
                  船名：
                  {item.shipName || '-----------'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.label}>
                  航次：
                  {item.voyageCode || '-----------'}
                </Text>
              </Col>
            </Grid>
            <Text style={styles.label}>
航线：
              {item.line}
            </Text>
          </View>
          <View style={styles.infoContainer}>
            <View style={{
              flex: 1,
              justifyContent: 'space-between',
              paddingLeft: 20,
              paddingTop: 5,
              paddingBottom: 5,
            }}
            >
              <Text style={styles.label}>
                {
                  item.ata ? `实际抵锚：${defaultFormat(item.ata)}` : `预计抵锚：${defaultFormat(item.eta) || '-----------'}`
                }
              </Text>
              <Text style={styles.label}>
                {
                  item.atb ? `实际停靠：${defaultFormat(item.atb)}` : `预计停靠：${defaultFormat(item.etb) || '-----------'}`
                }
              </Text>
              <Text style={styles.label}>
                {
                  item.atd ? `实际离港：${defaultFormat(item.atd)}` : `预计离港：${defaultFormat(item.etd) || '-----------'}`
                }
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

HistoryRecordItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default HistoryRecordItem;
