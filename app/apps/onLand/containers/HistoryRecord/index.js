import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  SafeAreaView, StyleSheet, Keyboard, TouchableOpacity,
} from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, Item, Label, View, Button, Text, InputGroup,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash';
import { getHistoryPromise } from './actions';
import { makeSelectPortWorkList, makeSelectRefreshState } from './selectors';
import HistoryRecordItem from './components/HistoryRecordItem';
import { ROUTE_HISTORY_DETAIL } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import AlertView from '../../../../components/Alert';
import screenHOC from '../../../../components/screenHOC';
import InputItem from '../../../../components/InputItem';
import commonStyles from '../../../../common/commonStyles';
import Svg from '../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class HistoryRecord extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipName: '',
      voyageCode: '',
      inputShipName: '',
      inputVoyageCode: '',
      page: firstPageNum,
      pageSize: DefaultPageSize,
      displayMode: true,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(true);
  };

  setSearchValue(callBack) {
    this.setState({
      shipName: this.state.inputShipName,
      voyageCode: this.state.inputVoyageCode,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputShipName: this.state.shipName,
      inputVoyageCode: this.state.voyageCode,
    }, callBack);
  }

  changeDisplayMode = (displayMode) => {
    this.props.navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  alert: AlertView;

  loadList(loadMore = false) {
    const {
      shipName,
      voyageCode,
      page,
      pageSize,
    } = this.state;
    this.props.getHistoryPromise({
      shipName,
      voyageCode,
      page: loadMore ? page : firstPageNum,
      pageSize,
    })
    // eslint-disable-next-line no-shadow
      .then(({ page }) => {
        this.setState({ page: page + 1 });
      })
      .catch(() => {});
  }

  renderFilter() {
    return (
      <View style={styles.container}>
        {this.state.displayMode ? (
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(this.state.shipName) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>船名:</Label>
                    <Text style={commonStyles.text}>{this.state.shipName}</Text>
                  </View>
                )}
                {!_.isEmpty(this.state.voyageCode) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>航次:</Label>
                    <Text style={commonStyles.text}>{this.state.voyageCode}</Text>
                  </View>
                )}
                {_.isEmpty(this.state.shipName) && _.isEmpty(this.state.voyageCode) && (
                  <Item
                    style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                    onPress={() => {
                      this.setInputValue();
                      this.changeDisplayMode(false);
                    }}
                  >
                    <Text style={commonStyles.text}>点击输入要搜索的船名/航次</Text>
                  </Item>
                )}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  autoFocus
                  label="船名:"
                  returnKeyType="search"
                  value={this.state.inputShipName}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputShipName: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="航次:"
                  returnKeyType="search"
                  value={this.state.inputVoyageCode}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputVoyageCode: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
            </View>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.setSearchValue();
                this.changeDisplayMode(true);
                this.setState({
                  displayMode: true,
                }, () => {
                  this.listView.beginRefresh();
                });
              }}
              block
              style={{
                height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
              }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        )}
      </View>
    );
  }

  renderItem = ({ item }) => (
    <HistoryRecordItem
      item={item}
      onPress={() => {
        this.props.navigation.navigate(ROUTE_HISTORY_DETAIL, {
          popId: item.id,
          shipName: item.shipName,
          voyageCode: item.voyageCode,
          portName: item.portName,
        });
      }}
    />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <View style={{ flex: 1, width: '100%' }}>
          <SafeAreaView style={{ flex: 1 }}>
            <RefreshListView
              ref={(ref) => { this.listView = ref; }}
              style={{ width: '100%' }}
              data={this.props.workList || []}
              keyExtractor={item => `${item.id}`}
              renderItem={this.renderItem}
              refreshState={this.props.refreshState}
              onHeaderRefresh={this.onHeaderRefresh}
              onFooterRefresh={this.onFooterRefresh}
              ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
              ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
            />
          </SafeAreaView>
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute', width: '100%', height: '100%', backgroundColor: '#ffffff',
            }}
            />
          )}
        </View>
      </Container>
    );
  }
}

HistoryRecord.navigationOptions = ({ navigation }) => ({
  title: '历史记录',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
});

HistoryRecord.propTypes = {
  navigation: PropTypes.object.isRequired,
  getHistoryPromise: PropTypes.func.isRequired,
  workList: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
HistoryRecord.defaultProps = {
  workList: [],
};

const mapStateToProps = createStructuredSelector({
  workList: makeSelectPortWorkList(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getHistoryPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryRecord);
