import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';
import Svg from '../../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  left: {
    alignItems: 'center',
    width: 50,
    marginLeft: 50,
  },
  right: {
    backgroundColor: 'white',
    margin: 5,
    padding: 5,
    borderRadius: 10,
  },
  topLine: {
    position: 'absolute',
    width: 2,
    top: 0,
    height: 30,
  },
  bottomLine: {
    position: 'absolute',
    width: 2,
    top: 30,
    bottom: 0,
  },
  checkIcon: {
    top: 20,
  },
  row: {
    flex: 1,
    padding: 5,
    flexDirection: 'row',
  },
  label: {
    // fontSize: 12,
    // color: '#535353',
    // margin: 2,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class PortItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const topLineColor = this.props.isTop ? 'transparent' : '#dc001b';
    const bottomLineColor = this.props.isBottom ? 'transparent' : '#dc001b';
    return (
      <View style={[styles.container, { marginTop: this.props.isTop ? 5 : 0 }]}>
        <View style={styles.left}>
          <View style={[styles.topLine, { backgroundColor: topLineColor }]} />
          <View style={[styles.bottomLine, { backgroundColor: bottomLineColor }]} />
          <Svg icon="check" size={20} style={styles.checkIcon} />
        </View>
        <View style={styles.right}>
          <View style={styles.row}>
            <Text style={styles.text}>{item.portName}</Text>
          </View>
          {
            item.ata > 0 ? (
              <View style={styles.row}>
                <Text style={styles.text}>实际靠泊:</Text>
                <Text style={styles.text}>{defaultFormat(item.ata)}</Text>
              </View>
            ) : (
              <View style={styles.row}>
                <Text style={styles.text}>预计靠泊:</Text>
                <Text style={styles.text}>{defaultFormat(item.eta)}</Text>
              </View>
            )
          }
          {
            item.atd > 0 ? (
              <View style={styles.row}>
                <Text style={styles.text}>实际离泊:</Text>
                <Text style={styles.text}>{defaultFormat(item.atd)}</Text>
              </View>
            ) : (
              <View style={styles.row}>
                <Text style={styles.text}>预计离泊:</Text>
                <Text style={styles.text}>{defaultFormat(item.etd)}</Text>
              </View>
            )
          }
        </View>
      </View>
    );
  }
}

PortItem.propTypes = {
  item: PropTypes.object.isRequired,
  isTop: PropTypes.bool,
  isBottom: PropTypes.bool,
};
PortItem.defaultProps = {
  isTop: false,
  isBottom: false,
};

export default PortItem;
