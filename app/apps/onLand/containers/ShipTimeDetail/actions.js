import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TIME_DETAIL } from './constants';

export const getShipTimeDetailRoutine = createRoutine(GET_SHIP_TIME_DETAIL);
export const getShipTimeDetailPromiseCreator = promisifyRoutine(getShipTimeDetailRoutine);
