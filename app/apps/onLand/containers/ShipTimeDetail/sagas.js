import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipTimeDetailRoutine } from './actions';

function* getShipTime(action) {
  console.log(action);
  try {
    yield put(getShipTimeDetailRoutine.request());
    const id = action.payload;
    const response = yield call(request, ApiFactory.getShippingScheduleDetail(id));
    console.log(response);
    yield put(getShipTimeDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipTimeDetailRoutine.failure(e));
  } finally {
    yield put(getShipTimeDetailRoutine.fulfill());
  }
}

export function* getShipTimeSaga() {
  yield takeLatest(getShipTimeDetailRoutine.TRIGGER, getShipTime);
}

// All sagas to be loaded
export default [getShipTimeSaga];
