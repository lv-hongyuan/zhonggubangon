import { getShipTimeDetailRoutine } from './actions';

const initState = {
  loading: false,
  detail: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    // 获取航次表详情
    case getShipTimeDetailRoutine.TRIGGER:
      return { ...state, detail: undefined, loading: true };
    case getShipTimeDetailRoutine.SUCCESS:
      return { ...state, detail: action.payload };
    case getShipTimeDetailRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getShipTimeDetailRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
