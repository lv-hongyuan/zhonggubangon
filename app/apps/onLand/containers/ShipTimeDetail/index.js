import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Text,
  View,
} from 'native-base';
import shortId from 'shortid';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getShipTimeDetailPromiseCreator } from './actions';
import { makeDetail, makeId, makeIsLoading } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';
import PortItem from './components/PortItem';

const styles = {
  container: {
    backgroundColor: 'white',
    padding: 5,
    paddingTop: 5,
    paddingBottom: 5,
  },
  item: {
    padding: 5,
    backgroundColor: 'white',
    minHeight: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
};

@screenHOC
class ShipTimeDetail extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.onFooterRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  willFocus() {
    this.onFooterRefresh();
  }

  loadList(loadMore) {
    this.props.getShipTimeDetailPromiseCreator(this.props.id, loadMore)
      .catch(() => {
      });
  }

  render() {
    const {
      shipName, voyageCode, line, stateMemo, portItem,
    } = (this.props.detail || {});
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={styles.container}>
            <View style={styles.item}>
              <Text style={commonStyles.inputLabel}>船名:</Text>
              <Text style={commonStyles.text}>{shipName}</Text>
            </View>
            <View style={styles.item}>
              <Text style={commonStyles.inputLabel}>航次:</Text>
              <Text style={commonStyles.text}>{voyageCode}</Text>
            </View>
            <View style={styles.item}>
              <Text style={commonStyles.inputLabel}>航线:</Text>
              <Text style={commonStyles.text}>{line}</Text>
            </View>
            <View style={styles.item}>
              <Text style={commonStyles.inputLabel}>当前动态:</Text>
              <Text style={commonStyles.text}>{stateMemo}</Text>
            </View>
          </View>
          {
            (portItem || []).map((port, index) => (
              <PortItem
                item={port}
                key={`${shortId.generate()}`}
                isTop={index === 0}
                isBottom={index === (portItem || []).length - 1}
              />
            ))
          }
        </Content>
      </Container>
    );
  }
}

ShipTimeDetail.navigationOptions = () => ({
  title: '船期表详情',
});

ShipTimeDetail.propTypes = {
  getShipTimeDetailPromiseCreator: PropTypes.func.isRequired,
  id: PropTypes.number,
  detail: PropTypes.object,
};
ShipTimeDetail.defaultProps = {
  id: undefined,
  detail: {},
};

const mapStateToProps = createStructuredSelector({
  id: makeId(),
  isLoading: makeIsLoading(),
  detail: makeDetail(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipTimeDetailPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipTimeDetail);
