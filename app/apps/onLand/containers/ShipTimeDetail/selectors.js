import { createSelector } from 'reselect';

const selectShipTimeDetailDomain = () => state => state.shipTimeDetail;

const makeId = () => createSelector(
  (state, props) => props.navigation.state.params,
  (subState) => {
    console.debug(subState.id);
    return subState.id;
  },
);

const makeDetail = () => createSelector(selectShipTimeDetailDomain(), (subState) => {
  console.debug(subState);
  return subState.detail;
});

const makeIsLoading = () => createSelector(selectShipTimeDetailDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

export { makeId, makeDetail, makeIsLoading };
