import { createSelector } from 'reselect';

const selectInstructionsDomain = () => state => state.instructions;

const makeSelectInstructions = () => createSelector(selectInstructionsDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeUnReadInstructionBadge = () => createSelector(selectInstructionsDomain(), (subState) => {
  console.debug(subState);
  return subState.badge;
});

const makeSelectRefreshState = () => createSelector(selectInstructionsDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeLoading = () => createSelector(selectInstructionsDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export {
  selectInstructionsDomain,
  makeSelectInstructions,
  makeSelectRefreshState,
  makeLoading,
  makeUnReadInstructionBadge,
};
