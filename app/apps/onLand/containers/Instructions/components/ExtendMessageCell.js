import React from 'react';
import { TouchableHighlight, StyleSheet, ViewPropTypes } from 'react-native';
import {
  View, Text, Badge, Button,
} from 'native-base';
import PropTypes from 'prop-types';
import HeightAnimationView from '../../../../../components/ExtendCell/HeightAnimationView';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  titleContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  badge: {
    alignSelf: 'center',
    marginRight: 10,
    alignItems: 'center',
  },
  readButton: {
    height: 40,
    width: 100,
    backgroundColor: '#DF001B',
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
});

class ExtendMessageCell extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: this.props.isOpen || false,
    };
  }

  onReadMessage = () => {
    if (this.props.onReadMessage) this.props.onReadMessage();
  };

  renderButton() {
    if (!this.props.hasRead) {
      return (
        <Button rounded onPress={this.onReadMessage} style={[styles.readButton]}>
          <Text>已读</Text>
        </Button>
      );
    }
    return null;
  }

  render() {
    return (
      <View
        style={{
          ...this.props.style,
          overflow: 'hidden',
        }}
      >
        <TouchableHighlight
          onPress={() => {
            this.setState({
              isOpen: !this.state.isOpen,
            });
          }}
        >
          <View style={[styles.titleContainer]}>
            <Text
              style={{
                flex: 1,
                marginLeft: 30,
                color: this.props.hasRead ? '#969696' : '#DF001B',
              }}
            >
              通知时间：
              {this.props.date}
            </Text>
            <Badge style={[styles.badge, { backgroundColor: this.props.hasRead ? '#969696' : '#DF001B' }]}>
              <Text>{this.props.hasRead ? '已读' : '未读'}</Text>
            </Badge>
          </View>
        </TouchableHighlight>
        <HeightAnimationView style={{ backgroundColor: '#ffffff' }} isOpen={this.state.isOpen}>
          {this.props.children}
          {this.renderButton()}
        </HeightAnimationView>
      </View>
    );
  }
}

ExtendMessageCell.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  style: ViewPropTypes.style,
  hasRead: PropTypes.bool.isRequired,
  date: PropTypes.string.isRequired,
  onReadMessage: PropTypes.func,
};
ExtendMessageCell.defaultProps = {
  onReadMessage: undefined,
  style: undefined,
};

export default ExtendMessageCell;
