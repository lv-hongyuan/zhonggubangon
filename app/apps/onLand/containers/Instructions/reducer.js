import _ from 'lodash';
import { RefreshState } from '../../../../components/RefreshListView';
import {
  getInstructionListRoutine,
  readOneInstructionRoutine,
  readAllInstructionRoutine,
} from './actions';


const defaultState = {
  list: [],
  success: false,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
  badge: 0,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取列表
    case getInstructionListRoutine.TRIGGER: {
      const { page } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getInstructionListRoutine.SUCCESS: {
      const { page, pageSize, list } = action.payload;
      return {
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getInstructionListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getInstructionListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 已读
    case readOneInstructionRoutine.TRIGGER:
      return { ...state, isLoading: true };
    case readOneInstructionRoutine.SUCCESS: {
      const newList = state.list.map((item) => {
        if (item.id === action.payload) {
          const newItem = _.cloneDeep(item);
          newItem.readState = 2;
          return newItem;
        }
        return item;
      });
      // 角标
      const badge = newList.reduce((pre, current) => {
        const unRead = current.readState === 0 || current.readState === 1;
        return pre + (unRead ? 1 : 0);
      }, 0);
      return { ...state, list: newList, badge };
    }
    case readOneInstructionRoutine.FAILURE:
      return { ...state, error: action.payload };
    case readOneInstructionRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 全部已读
    case readAllInstructionRoutine.TRIGGER:
      return { ...state, isLoading: true };
    case readAllInstructionRoutine.SUCCESS: {
      const newList = state.list.map((item) => {
        const newItem = _.cloneDeep(item);
        newItem.readState = 2;
        return newItem;
      });
      // 角标
      const badge = newList.reduce((pre, current) => {
        const unRead = current.readState === 0 || current.readState === 1;
        return pre + (unRead ? 1 : 0);
      }, 0);
      return { ...state, list: newList, badge };
    }
    case readAllInstructionRoutine.FAILURE:
      return { ...state, error: action.payload };
    case readAllInstructionRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
