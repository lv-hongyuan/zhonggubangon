/*
 *
 * Instructions constants
 *
 */
export const GET_INSTRUCTION_LIST = 'onLand/Instructions/GET_INSTRUCTION_LIST';
export const READ_ONE_INSTRUCTION = 'onLand/Instructions/READ_ONE_INSTRUCTION';
export const READ_ALL_INSTRUCTION = 'onLand/Instructions/READ_ALL_INSTRUCTION';
