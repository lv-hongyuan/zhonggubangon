import _ from 'lodash';
import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getInstructionListRoutine,
  readOneInstructionRoutine,
  readAllInstructionRoutine,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getInstructionList(action) {
  console.log(action);
  const { page, pageSize, ...rest } = (action.payload || {});
  try {
    yield put(getInstructionListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      if (!_.isEmpty(value)) {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getInstructions(param));
    console.log(response);
    const { totalElements, content } = (response.dtoList || {});
    yield put(getInstructionListRoutine.success({
      page, pageSize, list: (content || []), totalElements,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getInstructionListRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getInstructionListRoutine.fulfill());
  }
}

export function* getInstructionListSaga() {
  yield takeLatest(getInstructionListRoutine.TRIGGER, getInstructionList);
}

function* readOneInstruction(action) {
  console.log(action);
  try {
    yield put(readOneInstructionRoutine.request());
    const id = action.payload;
    const response = yield call(request, ApiFactory.readInstruction(id));
    console.log('readOne', response.toString());
    yield put(readOneInstructionRoutine.success(id));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(readOneInstructionRoutine.failure(e));
  } finally {
    yield put(readOneInstructionRoutine.fulfill());
  }
}

export function* readOneInstructionSaga() {
  yield takeLatest(readOneInstructionRoutine.TRIGGER, readOneInstruction);
}

function* readAllInstruction(action) {
  console.log(action);
  try {
    yield put(readAllInstructionRoutine.request());
    const response = yield call(request, ApiFactory.readAllInstructions());
    console.log('readAll', response.toString());
    yield put(readAllInstructionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(readAllInstructionRoutine.failure(e));
  } finally {
    yield put(readAllInstructionRoutine.fulfill());
  }
}

export function* readAllInstructionSaga() {
  yield takeLatest(readAllInstructionRoutine.TRIGGER, readAllInstruction);
}

// All sagas to be loaded
export default [
  getInstructionListSaga,
  readOneInstructionSaga,
  readAllInstructionSaga,
];
