
import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_INSTRUCTION_LIST,
  READ_ONE_INSTRUCTION,
  READ_ALL_INSTRUCTION,
} from './constants';


export const getInstructionListRoutine = createRoutine(
  GET_INSTRUCTION_LIST,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const getInstructionListPromise = promisifyRoutine(getInstructionListRoutine);

export const readOneInstructionRoutine = createRoutine(
  READ_ONE_INSTRUCTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const readOneInstructionPromise = promisifyRoutine(readOneInstructionRoutine);

export const readAllInstructionRoutine = createRoutine(
  READ_ALL_INSTRUCTION,
  updates => updates,
  () => ({ globalLoading: true }),
);
export const readAllInstructionPromise = promisifyRoutine(readAllInstructionRoutine);
