import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DRAWING_OUT_FEE } from './constants';

export const getDrawingOutFeeRoutine = createRoutine(GET_DRAWING_OUT_FEE);
export const getDrawingOutFeePromise = promisifyRoutine(getDrawingOutFeeRoutine);
