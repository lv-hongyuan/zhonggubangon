import { createSelector } from 'reselect/es';

const selectDrawingOutFreeDomain = () => state => state.drawingOutFree;

const makeList = () => createSelector(selectDrawingOutFreeDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDrawingOutFreeDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
