/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
    container: {
        alignContent: 'stretch',
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    col: {
        width: 80,
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
    },
    text: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        color: myTheme.inputColor,
    },
});

class DrawingOutFeeItemItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {
            zwcm, hc, zhg, khsj, xhg, cont_count_20, cont_count_40, cont_count_zl, xh_count_20, xh_count_40, xh_count_zl,
        } = this.props.item || {};
        return (
            <View style={styles.container}>
                <View style={[styles.col,
                    {
                        flexDirection: 'column',
                        width: 140,
                    }]}>
                    <View style={[styles.col, {
                        width: 140,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{zwcm}</Text>
                    </View>
                    <View style={[styles.col, {
                        width: 140,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{zwcm}</Text>
                    </View>
                </View>
                <View style={styles.col}>
                    <Text style={styles.text}>{hc}</Text>
                </View>
                <View style={[styles.col,
                    {
                        flexDirection: 'column',
                    }]}>
                    <View style={[styles.col, {
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{zhg}</Text>
                    </View>
                    <View style={[styles.col, {
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{zhg}</Text>
                    </View>
                </View>
                <View style={[styles.col,
                    {
                        flexDirection: 'column',
                        width: 80,
                    }]}>
                    <View style={[styles.col, {
                        width: 80,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{khsj}</Text>
                    </View>
                    <View style={[styles.col, {
                        width: 80,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{khsj}</Text>
                    </View>
                </View>
                <View style={[styles.col, { width: 80 }]}>
                    <Text style={styles.text}>{xhg}</Text>
                </View>
                <View style={[styles.col, { width: 80 }]}>
                    <Text style={styles.text}>{cont_count_20}</Text>
                </View>
                <View style={[styles.col,
                    {
                        flexDirection: 'column',
                        width: 80,
                    }]}>
                    <View style={[styles.col, {
                        width: 80,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{cont_count_40}</Text>
                    </View>
                    <View style={[styles.col, {
                        width: 80,
                        flex: 1,
                    }]}
                    >
                        <Text style={styles.text}>{cont_count_40}</Text>
                    </View>
                </View>
                <View style={[styles.col, { width: 80 }]}>
                    <Text style={styles.text}>{cont_count_zl}</Text>
                </View>
                <View style={[styles.col, { width: 80 }]}>
                    <Text style={styles.text}>{xh_count_20}</Text>
                </View>
                <View style={[styles.col, { width: 90 }]}>
                    <Text style={styles.text}>{xh_count_40}</Text>
                </View>
                <View style={[styles.col, { width: 90 }]}>
                    <Text style={styles.text}>{xh_count_zl}</Text>
                </View>
                <View style={[styles.col, { width: 90 }]}>
                    <Text style={styles.text}>{xh_count_zl}</Text>
                </View>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.props.headerTranslateX }],
                }}
                >
                    <View style={[styles.container, { flex: 1 }]}>
                        <View style={[styles.col,
                            {
                                flexDirection: 'column',
                                width: 140,
                            }]}>
                            <View style={[styles.col, {
                                width: 140,
                                flex: 1,
                            }]}
                            >
                                <Text style={styles.text}>{zwcm}</Text>
                            </View>
                            <View style={[styles.col, {
                                width: 140,
                                flex: 1,
                            }]}
                            >
                                <Text style={styles.text}>{zwcm}</Text>
                            </View>
                        </View>
                        <View style={styles.col}>
                            <Text style={styles.text}>{hc}</Text>
                        </View>
                        <View style={[styles.col,
                            {
                                flexDirection: 'column',
                            }]}>
                            <View style={[styles.col, {
                                flex: 1,
                            }]}
                            >
                                <Text style={styles.text}>{zhg}</Text>
                            </View>
                            <View style={[styles.col, {
                                flex: 1,
                            }]}
                            >
                                <Text style={styles.text}>{zhg}</Text>
                            </View>
                        </View>
                    </View>
                </Animated.View>
            </View>
        );
    }
}

DrawingOutFeeItemItem.propTypes = {
    item: PropTypes.object.isRequired,
    headerTranslateX: PropTypes.object.isRequired,
};

export default DrawingOutFeeItemItem;
