import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDrawingOutFeeRoutine } from './actions';

function* getDrawingOutFee(action) {
  console.log(action);
  try {
    yield put(getDrawingOutFeeRoutine.request());
    const response = yield call(request, ApiFactory.getPrematch());
    console.log(response);
    yield put(getDrawingOutFeeRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDrawingOutFeeRoutine.failure(e));
  } finally {
    yield put(getDrawingOutFeeRoutine.fulfill());
  }
}

export function* getDrawingOutFeeSaga() {
  yield takeLatest(getDrawingOutFeeRoutine.TRIGGER, getDrawingOutFee);
}

// All sagas to be loaded
export default [getDrawingOutFeeSaga];
