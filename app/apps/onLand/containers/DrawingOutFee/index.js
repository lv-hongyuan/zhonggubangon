//! ********* 此页面已弃用 **********
//! ********* 结拖引费 **********
//! ********* 此页面已弃用 **********
import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, Animated, TouchableHighlight} from 'react-native';
import { connect } from 'react-redux';
import {
  Container, Item, Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getDrawingOutFeePromise } from './actions';
import { ROUTE_SHIP_TIME_DETAIL } from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import DrawingOutFeeItemItem from './components/DrawingOutFeeItem';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from "../../../../common/commonStyles";
import DatePullSelector from "../../../../components/DatePullSelector";
import {DATE_WHEEL_TYPE} from "../../../../components/DateTimeWheel";

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    // alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    height: 40,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    backgroundColor: 'white',
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  subText: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
  },
};

@screenHOC
class DrawingOutFee extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      recordW: 0,
      recordH: 0,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    this.props.navigation.navigate(
      ROUTE_SHIP_TIME_DETAIL,
      {
        id: this.props.navigation.getParam('id'),
        item,
      },
    );
  };

  //根View的onLayout回调函数
  onLayout = (event) => {
    //获取根View的宽高，以及左上角的坐标值
    let {x, y, width, height} = event.nativeEvent.layout;
    this.setState({
      recordW: width,
      recordH: height,
    });
  }

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    this.props.getDrawingOutFeePromise({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    })
      .catch(() => {
      });
  }

  renderItem = ({ item }) => (
    <DrawingOutFeeItemItem
      item={item}
      headerTranslateX={this.translateX}
    />
  );

  renderFilter() {
    return (
        <View style={styles.container}>
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <Item style={[commonStyles.inputItem, {
              paddingLeft: 5,
              marginBottom: 0,
              flex: 1,
              width: undefined,
            }]}
            >
              <Label style={commonStyles.inputLabel}>起始时间:</Label>
              <DatePullSelector
                  value={this.state.startTime}
                  type={DATE_WHEEL_TYPE.DATE}
                  onChangeValue={(value) => {
                    this.setState({ startTime: value }, () => {
                      this.listView.beginRefresh();
                    });
                  }}
              />
            </Item>
            <Item style={[commonStyles.inputItem, {
              paddingLeft: 5,
              marginBottom: 0,
              flex: 1,
              width: undefined,
            }]}
            >
              <Label style={commonStyles.inputLabel}>截止时间:</Label>
              <DatePullSelector
                  value={this.state.endTime}
                  type={DATE_WHEEL_TYPE.DATE}
                  onChangeValue={(value) => {
                    this.setState({ endTime: value }, () => {
                      this.listView.beginRefresh();
                    });
                  }}
              />
            </Item>
          </SafeAreaView>
        </View>
    );
  }

  render() {
    return (
        <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
          {this.renderFilter()}
          <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
            <View
                style={{
                  width: '100%',
                  flex: 1,
                }}
                onLayout={(event) => {
                  const { width } = event.nativeEvent.layout;
                  this.setState({ containerWidth: width });
                }}
            >
              <Animated.ScrollView
                  onScroll={Animated.event([{
                    nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
                  }], { useNativeDriver: true })}
                  scrollEventThrottle={1}
                  horizontal
              >
                <View style={{
                  flex: 1,
                  minWidth: '100%',
                }}
                >
                  <View style={styles.container}>
                    <View style={[styles.col, { width: 140 }]}>
                      <Text style={styles.text}>时间</Text>
                    </View>
                    <View style={styles.col}>
                      <Text style={styles.text}>港口</Text>
                    </View>
                    <View style={styles.col}>
                      <Text style={styles.text}>航次</Text>
                    </View>
                    <View style={[styles.col, {
                      flexDirection: 'column',
                      width: 240,
                    }]} onLayout={this.onLayout}
                    >
                      <View style={[styles.col, {
                        width: 240,
                        flex: 1,
                      }]}
                      >
                        <Text style={styles.subText}>航次拖轮使用情况</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row',
                        flex: 1,
                      }}
                      >
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>靠/离</Text>
                        </View>
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>节省数</Text>
                        </View>
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>金额</Text>
                        </View>
                      </View>
                    </View>
                    <View style={[styles.col, {
                      flexDirection: 'column',
                      width: 240,
                    }]}
                    >
                      <View style={[styles.col, {
                        width: 240,
                        flex: 1,
                      }]}
                      >
                        <Text style={styles.subText}>航次引水使用情况</Text>
                      </View>
                      <View style={{
                        flexDirection: 'row',
                        flex: 1,
                      }}
                      >
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>靠/离</Text>
                        </View>
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>节省数</Text>
                        </View>
                        <View style={[styles.col, {
                          width: 80,
                          height: '100%',
                          borderBottomWidth: 0,
                        }]}
                        >
                          <Text style={styles.subText}>金额</Text>
                        </View>
                      </View>
                    </View>
                    <View style={[styles.col, { width: 90 }]}>
                      <Text style={styles.text}>合计</Text>
                    </View>
                    <View style={[styles.col, { width: 90 }]}>
                      <Text style={styles.text}>修正</Text>
                    </View>
                    <View style={[styles.col, { width: 90 }]}>
                      <Text style={styles.text}>备注</Text>
                    </View>
                    <Animated.View style={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      bottom: 0,
                      transform: [{ translateX: this.translateX }],
                    }}
                    >
                      <View style={styles.container}>
                        <View style={[styles.col, { width: 140 }]}>
                          <Text style={styles.text}>时间</Text>
                        </View>
                        <View style={[styles.col, {height: this.state.recordH}]}>
                          <Text style={styles.text}>港口</Text>
                        </View>
                        <View style={styles.col}>
                          <Text style={styles.text}>航次</Text>
                        </View>
                      </View>
                    </Animated.View>
                  </View>
                  <RefreshListView
                      ref={(ref) => { this.listView = ref; }}
                      headerTranslateX={this.translateX}
                      containerWidth={this.state.containerWidth}
                      data={this.props.list || []}
                      style={{ flex: 1 }}
                      keyExtractor={item => `${item.id}`}
                      renderItem={this.renderItem}
                      refreshState={this.props.refreshState}
                      onHeaderRefresh={this.onHeaderRefresh}
                      // onFooterRefresh={this.onFooterRefresh}
                  />
                </View>
              </Animated.ScrollView>
            </View>
          </SafeAreaView>
          <View style={[styles.container, {
            flexDirection: 'column',
            height: 80,
          }]}
          >
            <View style={{
              flexDirection: 'row',
              flex: 1,
              height: 40,
            }}
            >
              <View style={[styles.col, { width: 140, borderTopWidth: myTheme.borderWidth }]}>
                <Text style={styles.text}>总计</Text>
              </View>
              <View style={[styles.col, {borderTopWidth: myTheme.borderWidth}]} >
                <Text style={styles.text}>应付</Text>
              </View>
              <View style={[styles.col, { width: 160, borderTopWidth: myTheme.borderWidth}]}>
                <Text style={styles.text}>100</Text>
              </View>
              <View style={[styles.col, {borderTopWidth: myTheme.borderWidth}]}>
                <Text style={styles.text}>实付</Text>
              </View>
              <View style={[styles.col, { width: 160, borderTopWidth: myTheme.borderWidth }]}>
                <Text style={styles.text}>100</Text>
              </View>
            </View>
            <View style={[styles.col, {
              justifyContent: 'flex-end',
              width: '100%',
            }]}>
              <Text style={styles.text}>船长：丁春祥</Text>
            </View>
          </View>
        </Container>
    );
  }
}

DrawingOutFee.navigationOptions = () => ({
  title: '结拖引费',
});

DrawingOutFee.propTypes = {
  navigation: PropTypes.object.isRequired,
  getDrawingOutFeePromise: PropTypes.func.isRequired,
  refreshState: PropTypes.number.isRequired,
  list: PropTypes.array,
};

DrawingOutFee.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDrawingOutFeePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawingOutFee);
