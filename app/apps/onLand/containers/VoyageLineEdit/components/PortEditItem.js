import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  Button, Text, View, Card,
} from 'native-base';
import _ from 'lodash';
import Svg from '../../../../../components/Svg';
import { TextInput } from '../../../../../components/InputItem/TextInput';
import Selector from '../../../../../components/Selector';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 10,
    paddingLeft: 40,
    paddingRight: 40,
    width: '100%',
  },
  port: {
    textAlign: 'center',
    fontSize: 14
  },
  deleteButtonContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    width: 40,
    justifyContent: 'center',
    paddingLeft: 5,
  },
  deleteButton: {
    paddingTop: 0,
    paddingLeft: 0,
    paddingBottom: 0,
    paddingRight: 0,
    width: 20,
    height: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    elevation: 10,
  },
});

class PortEditItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      data: {
        typeChildId: 0,
        typeChildText: this.props.wharfName,
      },
    };
  }

  componentDidMount() {
    const { portName } = this.props;
    if (_.isEmpty(portName)) {
      setTimeout(() => {
        this.setState({ editMode: true });
      }, 100);
    }
  }

  //!点击选择码头事件
  onChangeTypeParent = (item) => {
    const {selectWharf,index,portName} = this.props;
    if (this.state.typeChildCode !== item.id) {
      this.setData({
        typeChildId: item.id,
        typeChildText: item.text,
      });

      //!回调事件：将要上传的数据设置为  选择码头之后的港口、id、码头数据
      selectWharf(portName,item.text,index)

    }
  };
  setData(params) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    });
  }

  //!点击展开码头列表
  showgetWharfList() {
    const { getWharfList, portName } = this.props
    if (portName.replace(/\s+/g, "").length == 0) {
      return
    }

    //!回调事件：获取到码头列表数据之后再打开选择框
    getWharfList(this.showalert)
  }

  //!打开选择框
  showalert = () => { this.selector.showPicker() }
  render() {
    const { editMode } = this.state;
    const {
      portName,         //! 港口名
      onChangePortName, //! 修改港口事件
      onDelete,         //! 删除港口事件
      onFocus,          //! 输入框聚焦
      onBlur,           //! 输入款失焦
      editable,         //! 输入款及选择框是否可互动
      wharfName,        //! 码头名
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={{
          borderRadius: 5, width: '100%', borderColor: '#ddd', height: 37,
          borderWidth: 1, flexDirection: 'row', overflow: 'hidden',
        }}
        >
          {editable && editMode ? (
            <View
              style={{
                width: '50%',
                padding: 5,
                backgroundColor: '#FFFFFF',
                borderRightWidth: 1, borderRightColor: '#ddd'
              }}
            >
              <TextInput
                //!左侧输入框
                style={{ width: '100%', padding: 0 ,backgroundColor:'#fff'}}
                autoFocus
                textAlign="center"
                value={portName}
                placeholder="请输入港口名称"
                onChangeText={(text) => {
                  this.inputValue = text;
                  const { index } = this.props;
                  if (text && text.length > 0) { onChangePortName(text, index) }
                  this.setData({
                    //! 每次修改港口名清空码头数据
                    typeChildText: '',
                  });
                }}
                onFocus={() => {
                  this.inputValue = portName;
                  onFocus();
                }}
                onBlur={() => {
                  this.setState({ editMode: false });
                  const { index } = this.props;
                  onChangePortName(this.inputValue, index);
                  onBlur();
                }}
                onSubmitEditing={() => {
                  const { index } = this.props;
                  if (this.inputValue && this.inputValue.length > 0) { onChangePortName(this.inputValue, index) }
                }}
              />
            </View>
          ) : (
              <TouchableOpacity
                //!第一行左侧框  （不可编辑
                style={{ width: '50%' }}
                onPress={() => {
                  this.setState({ editMode: true });
                }}
              >
                <View
                  style={{
                    padding: 10,
                    backgroundColor: '#FFFFFF',
                    borderRightWidth: 1, borderRightColor: '#ddd'
                  }}
                >
                  <Text style={styles.port}>{portName}</Text>
                </View>
              </TouchableOpacity>
            )}
          {editable ? (
            <View style={{
              flex: 1
            }}>
              <Selector
                //!右侧选择框
                value={this.state.data.typeChildText}       //! 码头名
                items={this.props.wharfList || []}          //! 码头列表数据
                ref={(ref) => {
                  this.selector = ref;
                }}
                placeholder="请选择..."
                onPress={() => { this.showgetWharfList() }} //!点击展开码头列表
                pickerTitle="码头列表"                       //!码头列表 表头文字
                getItemValue={item => item.text}
                getItemText={item => item.text}
                onSelected={this.onChangeTypeParent}       //!点击选择码头事件
                valueColor={'rgb(28,28,28)'}
                textAlign={'center'}
              />
            </View>
          ) : (
              <TouchableOpacity style={{ width: '50%', height: 35, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 14 }}>{wharfName}</Text>
              </TouchableOpacity>
            )}
        </View>
        {editable && (  //! 删除按钮
          <View style={styles.deleteButtonContainer}>
            <Button
              rounded
              style={styles.deleteButton}
              onPress={() => {
                const { index } = this.props;
                onDelete(index);
              }}
            >
              <Svg icon="delete" color="red" size={20} />
            </Button>
          </View>
        )}
      </View>
    );
  }
}

PortEditItem.defaultProps = {
  editable: true,
  allLineList: []
};

PortEditItem.propTypes = {
  portName: PropTypes.string.isRequired,
  onChangePortName: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  editable: PropTypes.bool,
};

export default PortEditItem;
