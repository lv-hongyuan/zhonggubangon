import { createSelector } from 'reselect/es';

const selectVoyageLineEditDomain = () => state => state.voyageLineEdit;

const makeLineList = () => createSelector(selectVoyageLineEditDomain(), (subState) => {
  console.debug(subState);
  return subState.allLineList;
});

const makeLineTypeList = () => createSelector(selectVoyageLineEditDomain(), (subState) => {
  console.debug(subState);
  return subState.lineTypeList;
});

const makeActivePopId = () => createSelector(selectVoyageLineEditDomain(), (subState) => {
  console.debug(subState);
  return subState.popid;
});

const makeIsLoading = () => createSelector(selectVoyageLineEditDomain(), (subState) => {
  console.debug(subState);
  return subState.loading;
});

const makeWharfList = () => createSelector(selectVoyageLineEditDomain(),(subState) => {
  console.debug(subState);
  return subState.wharfList;
})

export {
  makeLineTypeList,
  makeActivePopId,
  makeIsLoading,
  makeLineList,
  makeWharfList
};
