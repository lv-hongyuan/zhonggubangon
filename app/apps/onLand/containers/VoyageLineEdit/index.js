
//! 航次调度的航线调整界面

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Keyboard, StyleSheet } from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import {
  Button, Container, Content, InputGroup, Item, Label, View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import shortId from 'shortid';
import _ from 'lodash';
import {
  makeIsLoading, makeLineTypeList, makeActivePopId, makeLineList, makeWharfList
} from './selectors';
import myTheme from '../../../../Themes';
import AlertView from '../../../../components/Alert';
import screenHOC from '../../../../components/screenHOC';
import {
  getVoyageLineListPromise, createVoyageLinePromise, createNextVoyageLinePromise,
  changeVoyageLinePromise, getLineTypeListPromise, getWharfListPromise
} from './actions';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';
import Svg from '../../../../components/Svg';
import PortEditItem from './components/PortEditItem';
import Selector from '../../../../components/Selector';
import { LineEditType, LineEditTypeKey } from './constants';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getVoyageAllListPromise } from "../VoyageEdit/actions";

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  portContainer: {
    alignItems: 'center',
  },
  lineContainer: {
    alignItems: 'center',
    position: 'absolute',
    top: 10,
    left: 0,
    bottom: 10,
    right: 0,
  },
  line: {
    height: '100%',
    width: 2,
    backgroundColor: '#dddddd',
  },
  addButton: {
    paddingTop: 0,
    paddingLeft: 0,
    paddingBottom: 0,
    paddingRight: 0,
    width: 20,
    height: 20,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#00bb26',
  },
});

@screenHOC
class VoyageLineEdit extends React.Component {
  constructor(props) {
    super(props);
    const { navigation } = this.props;

    const item = props.navigation.getParam('item', { portName: (this.props.operatingShip || {}).portName });
    console.log('第一次航线全称' + navigation.getParam('shipLineName'));
    this.state = {
      shouldSelectLineType: false,
      voyCode: navigation.getParam('voyCode'),  //!航次号
      lineType: 0,
      isLineEnable: 0,
      isNextLineEnable: 0,
      ports: navigation.getParam('popList', []).map(popItem => ({  //!港口数据：id、港口名、码头名
        id: shortId.generate(),
        name: popItem.portName,   //!港口名
        wharfName:popItem.wharfName  //!码头名
      })),
      onInput: false,

      shipMainLineId: navigation.getParam('shipId'),
      shipMainLineName: navigation.getParam('shipLineName'),
      isEditing: item.id !== undefined && item.id !== null,
      data: {
        typeChildId: navigation.getParam('shipId'),
        typeChildText: navigation.getParam('shipLineName'),
      },
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({
      onBtnSave: this.onBtnSave,
      onBackPressed: this.onBackPressed,
    });
    this.props.getShipMainLineSelectList()
      .then((data) => {
        console.log('==>', data)
      })
      .catch(() => { });
  }

  onBackPressed = () => {
    const { navigation } = this.props;
    AlertView.show({
      message: '确定要退出编辑画面吗？',
      showCancel: true,
      confirmAction: () => {
        navigation.goBack();
      },
    });
    return true;
  };

  //!右上角完成按钮
  onBtnSave = () => {
    Keyboard.dismiss();
    setTimeout(() => {
      const { navigation } = this.props;
      const {
        ports, voyCode, shouldSelectLineType, lineType
      } = this.state;
      console.log(ports);
      if (_.isEmpty(voyCode)) {
        AlertView.show({ message: '请填写航次编号!' });
        return;
      }
      if (voyCode.length > 7) {
        AlertView.show({ message: '航次编号不可大于7个字符!' });
        return;
      }
      if (shouldSelectLineType && lineType === 0) {
        AlertView.show({ message: '请选择航线类型' });
        return;
      }
      if (ports.length < 2) {
        AlertView.show({ message: '最少需要两个港口' });
        return;
      }
      let wharfLength = 0
      //!检查码头是否已全部选择
      for(var i = 0;i < ports.length;i++){
        if(ports[i].wharfName && ports[i].wharfName.length > 0){
          wharfLength++
        }
      }
      if(wharfLength < ports.length){
        AlertView.show({ message: '请选择预靠码头' });
        return;
      }
      //!拼接新的航线全称与码头线
      const newLine = ports.map(port => port.name.trim()).join('-');
      const newWharfLine = ports.map(port => port.wharfName.trim()).join('-');

      switch (this.editType) {
        case LineEditType.PLAN_NEW:  
          this.createVoyageLine(newLine,newWharfLine);  //! 计划新航线
          break;
        case LineEditType.ADD_NEXT:  
          AlertView.show({
            message: '确认提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.createNextVoyageLine(newLine,newWharfLine);   //! 计划下期航线
            },
          });
          break;
        case LineEditType.EDIT: {   
          const oldLine = navigation
            .getParam('popList', [])
            .map(popItem => popItem.portName)
            .join('-');
          AlertView.show({
            message: `将航线\n${oldLine}\n修改为\n${newLine}`,
            showCancel: true,
            confirmAction: () => {
              this.changeVoyageLine(newLine,newWharfLine);    //! 航线修改
            },
          });
          break;
        }
        default:
          break;
      }
    }, 100);
  };
  //! 选择航线类型
  onPressLineType = () => {
    Keyboard.dismiss();
    const { getLineTypeList } = this.props;
    getLineTypeList()
      .then(() => {
        this.select.showPicker();
      })
      .catch(() => { });
  };

  get editType() {
    const { navigation } = this.props;
    return navigation.getParam(LineEditTypeKey, LineEditType.ADD_NEXT);
  }

  //!网络请求：启用新航线
  createVoyageLine = (newLine,newWharfLine) => {
    const { isLineEnable, lineType, voyCode } = this.state;
    const { navigation, createVoyageLine, showErrorMessage } = this.props;
    createVoyageLine({
      isLineEnable,
      lineType,
      linename: newLine,
      voyid: navigation.getParam('voyid'),
      popid: navigation.getParam('popid'),
      voyCode,
      wharfNames:newWharfLine,
      shipMainLineName: this.state.data.typeChildText,
      shipMainLineId: this.state.data.typeChildId,
    })
      .then(() => {
        navigation.getParam('onBack', () => { })();
        navigation.goBack();
      })
      .catch((error) => {
        const code = error && error.code;
        if (code === 821) {
          // 需要航线类型
          AlertView.show({
            message: '请选择航线类型',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                shouldSelectLineType: true,
              }, () => {
                this.onPressLineType();
              });
            },
          });
        } else if (code === 822) {
          // 需要确认启用航线
          AlertView.show({
            message: '是否启用当前航线',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                isLineEnable: 1,
              }, () => {
                this.createVoyageLine(newLine,newWharfLine);
              });
            },
          });
        } else {
          // 显示错误
          showErrorMessage(error);
        }
      });
  };

  //! 网络请求： 提交新的下期航线
  createNextVoyageLine = (newLine,newWharfLine) => {
    const { isLineEnable, lineType, voyCode } = this.state;
    const { navigation, createNextVoyageLine, showErrorMessage } = this.props;
    console.log(this.state.data);
    createNextVoyageLine({
      isLineEnable,
      lineType,
      linename: newLine,
      voyid: navigation.getParam('voyid'),
      popid: navigation.getParam('popid'),
      voyCode,
      shipMainLineName: this.state.data.typeChildText,
      shipMainLineId: this.state.data.typeChildId,
      wharfNames: newWharfLine
    })
      .then(() => {
        navigation.getParam('onBack', () => { })();
        navigation.goBack();
      })
      .catch((error) => {
        const code = error && error.code;
        if (code === 821) {
          // 需要航线类型
          AlertView.show({
            message: '请选择航线类型',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                shouldSelectLineType: true,
              }, () => {
                this.onPressLineType();
              });
            },
          });
        } else if (code === 822) {
          // 需要确认启用航线
          AlertView.show({
            message: '是否启用当前航线',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                isLineEnable: 1,
              }, () => {
                this.createNextVoyageLine(newLine,newWharfLine);
              });
            },
          });
        } else {
          // 显示错误
          showErrorMessage(error);
        }
      });
  };
  //! 网络请求：提交修改后的新的航线
  changeVoyageLine = (newLine,newWharfLine) => {
    const { isLineEnable, isNextLineEnable, lineType } = this.state;
    const { navigation, changeVoyageLine, showErrorMessage } = this.props;
    changeVoyageLine({
      isLineEnable,
      isNextLineEnable,
      lineType,
      linename: newLine,
      voyid: navigation.getParam('voyid'),
      shipMainLineId: this.state.data.typeChildId,
      shipMainLineName: this.state.data.typeChildText,
      wharfNames:newWharfLine
    })
      .then(() => {
        navigation.getParam('onBack', () => { })();
        navigation.goBack();
      })
      .catch((error) => {
        const code = error && error.code;
        if (code === 821) {
          // 需要航线类型
          AlertView.show({
            message: '请选择航线类型',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                shouldSelectLineType: true,
              }, () => {
                this.onPressLineType();
              });
            },
          });
        } else if (code === 822) {
          // 需要确认启用航线
          AlertView.show({
            message: '是否启用当前航线',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                isLineEnable: 1,
              }, () => {
                this.changeVoyageLine(newLine,newWharfLine);
              });
            },
          });
        } else if (code === 823) {
          // 需要确认启用下期航线
          AlertView.show({
            message: '是否启用下期航线',
            showCancel: true,
            confirmAction: () => {
              this.setState({
                isNextLineEnable: 1,
              }, () => {
                this.changeVoyageLine(newLine,newWharfLine);
              });
            },
          });
        } else {
          // 显示错误
          showErrorMessage(error);
        }
      });
  };

  //! 航线港口
  renderPorts() {
    const { ports, onInput } = this.state;
    return (ports || []).map(({ id, name,wharfName }, i) => {
      const next = ports[i + 1];
      const showAddBtn = !onInput && (next ? !_.isEmpty(name) && !_.isEmpty(next.name) : !_.isEmpty(name));
      return (
        <View style={styles.portContainer} key={id}>
          <PortEditItem
            wharfName={wharfName}
            portName={name}
            editable={i !== 0}
            onChangePortName={(portName, index) => { //! 港口名修改事件
              if (_.isEmpty(portName)) {
                ports.splice(index, 1);
              } else {
                ports.splice(index, 1, { id: ports[index].id, name: portName });
              }
              this.setState({ ports: [...ports] });
            }}
            onDelete={(index) => {
              //!删除港口事件
              ports.splice(index, 1);
              this.setState({ ports: [...ports], onInput: false });
            }}
            onFocus={() => { this.setState({ onInput: true }); }}
            onBlur={() => { this.setState({ onInput: false }); }}
            isFirst={false}
            isLast={false}
            index={i}
            getWharfList={(callback) => {
              //! 网络请求：获取码头列表事件
              this.props.getWharfList({ portName: name })
                .then(() => {
                  callback()   //! 获取码头列表完成后的回调函数：打开选择框
                })
                .catch((e) => { console.log(e); });
            }}
            wharfList={this.props.wharfList}
            selectWharf={(portName,data,index) => {
              //! 选择码头事件
              ports.splice(index, 1, { id: ports[index].id,name: portName,wharfName: data });
              this.setState({ ports: [...ports] });
            }}
          />
          <Button
            style={[styles.addButton, { opacity: showAddBtn ? 1 : 0 }]}   //! 新增港口按钮
            disabled={!showAddBtn}
            onPress={() => {
              ports.splice(i + 1, 0, { id: shortId.generate(), name: '' });
              this.setState({ ports: [...ports] });
            }}
            rounded
          >
            <Svg icon="onLand_add" size={15} />
          </Button>
        </View>
      );
    });
  }
  onChangeTypeParent = (item) => {
    console.log('122item.code||' + item.id + '   item.text' + item.text);
    if (this.state.typeChildCode !== item.id) {
      this.setData({
        typeChildId: item.id,
        typeChildText: item.text,
      });
    }
  };
  setData(params) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    });
  }
  onBtnTypeParent = () => {
    console.log('我点击了按钮');
    this.selector.showPicker();
  };

  render() {
    const {
      voyCode, lineType, shouldSelectLineType,
    } = this.state;
    const { lineTypeList } = this.props;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#FFFFFF' }}>
        <Content
          style={{ flex: 1 }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          contentContainerStyle={{ paddingBottom: 20 }}
        >
          <View style={styles.lineContainer}>
            <View style={styles.line} />
          </View>
          <View style={{ paddingLeft: 10, paddingRight: 10, backgroundColor: '#FFFFFF' }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航次编号:"
                editable={this.editType !== LineEditType.EDIT}
                value={voyCode}
                placeholder="请输入航次编号"
                onChangeText={(text) => {
                  this.setState({ voyCode: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>航线全称:</Label>
                <Selector
                  // value={this.state.data.typeChildText}
                  value={this.state.data.typeChildText}
                  items={this.props.allLineList || []}
                  disabled={this.state.isEditing}
                  ref={(ref) => {
                    this.selector = ref;
                  }}
                  placeholder="请选择..."
                  onPress={this.onBtnTypeParent}
                  pickerTitle="请选择航线全称"
                  getItemValue={item => item.text}
                  getItemText={item => item.text}
                  onSelected={this.onChangeTypeParent}
                />
              </Item>
            </InputGroup>

            {shouldSelectLineType && (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>航线类型:</Label>
                  <Selector
                    value={lineType || null}
                    items={lineTypeList}
                    getItemValue={item => item.id}
                    getItemText={item => item.text}
                    ref={(ref) => {
                      this.select = ref;
                    }}
                    onPress={this.onPressLineType}
                    pickerTitle="航线类型"
                    onSelected={(item) => {
                      this.setState({ lineType: item.id });
                    }}
                  />
                </Item>
              </InputGroup>
            )}
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>航线港口:</Label>
              </Item>
            </InputGroup>
          </View>
          {this.renderPorts()}
        </Content>
      </Container>
    );
  }
}

VoyageLineEdit.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '航线调整'),
  headerLeft: (
    <HeaderButtons color="white">
      <HeaderButtons.Item
        title=""
        buttonWrapperStyle={{ padding: 10 }}
        ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
        onPress={() => {
          const onBackPressed = navigation.getParam('onBackPressed');
          onBackPressed();
        }}
      />
    </HeaderButtons>
  ),
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="完成"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={() => {
          const onBtnSave = navigation.getParam('onBtnSave');
          onBtnSave();
        }}
      />
    </HeaderButtons>
  ),
});

VoyageLineEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
  createVoyageLine: PropTypes.func.isRequired,
  createNextVoyageLine: PropTypes.func.isRequired,
  changeVoyageLine: PropTypes.func.isRequired,
  showErrorMessage: PropTypes.func.isRequired,
  getLineTypeList: PropTypes.func.isRequired,
  lineTypeList: PropTypes.array.isRequired,

  getShipMainLineSelectList: PropTypes.func.isRequired,
  getWharfList: PropTypes.func.isRequired,
  allLineList: PropTypes.array,
  wharfList: PropTypes.array
};

const mapStateToProps = createStructuredSelector({
  latestPopId: makeActivePopId(),
  lineTypeList: makeLineTypeList(),
  isLoading: makeIsLoading(),
  allLineList: makeLineList(),
  wharfList: makeWharfList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(
      {
        showErrorMessage: errorMessage,
      },
      dispatch,
    ),
    ...bindPromiseCreators(
      {
        getShipMainLineSelectList: getVoyageLineListPromise,
        createVoyageLine: createVoyageLinePromise,
        createNextVoyageLine: createNextVoyageLinePromise,
        changeVoyageLine: changeVoyageLinePromise,
        getLineTypeList: getLineTypeListPromise,
        getWharfList: getWharfListPromise,
      },
      dispatch,
    ),
    dispatch,
  };
}
VoyageLineEdit.defaultProps = {
  allLineList: [],
  wharfList: []
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VoyageLineEdit);
