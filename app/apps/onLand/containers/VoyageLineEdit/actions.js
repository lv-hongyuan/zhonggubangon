import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CREATE_VOYAGE_LINE, CREATE_NEXT_VOYAGE_LINE, CHANGE_VOYAGE_LINE, GET_LINE_TYPE_LIST,GET_LINE_LIST,GET_WHARF_LIST
} from './constants';
import {GET_VOYAGE_ALL_LIST} from "../VoyageEdit/constants";

export const getVoyageLineListRoutine = createRoutine(GET_LINE_LIST);
export const getVoyageLineListPromise = promisifyRoutine(getVoyageLineListRoutine);

export const createVoyageLineRoutine = createRoutine(CREATE_VOYAGE_LINE);
export const createVoyageLinePromise = promisifyRoutine(createVoyageLineRoutine);

export const createNextVoyageLineRoutine = createRoutine(CREATE_NEXT_VOYAGE_LINE);
export const createNextVoyageLinePromise = promisifyRoutine(createNextVoyageLineRoutine);

export const changeVoyageLineRoutine = createRoutine(CHANGE_VOYAGE_LINE);
export const changeVoyageLinePromise = promisifyRoutine(changeVoyageLineRoutine);

export const getLineTypeListRoutine = createRoutine(GET_LINE_TYPE_LIST);
export const getLineTypeListPromise = promisifyRoutine(getLineTypeListRoutine);

export const getWharfListRoutine = createRoutine(GET_WHARF_LIST);
export const getWharfListPromise = promisifyRoutine(getWharfListRoutine);
