import {
  createVoyageLineRoutine, createNextVoyageLineRoutine, changeVoyageLineRoutine, 
  getLineTypeListRoutine,getVoyageLineListRoutine,getWharfListRoutine
} from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import {makeLineTypeList} from "./selectors";

const defaultState = {
  lastPopList: null,
  lineTypeList: [],
  popList: null,
  nextPorts: null,
  popid: null,
  loading: false,
  refreshState: RefreshState.Idle,
  allLineList: [],
  getWharfLis:[]
};

export default function (state = defaultState, action) {
  switch (action.type) {
      // 调整航线
    case changeVoyageLineRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case changeVoyageLineRoutine.SUCCESS: {
      const lineTypeList = action.payload;
      return { ...state, lineTypeList };
    }
    case changeVoyageLineRoutine.FAILURE: {
      return { ...state };
    }
    case changeVoyageLineRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 计划本期航线
    case createVoyageLineRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case createVoyageLineRoutine.SUCCESS: {
      return { ...state };
    }
    case createVoyageLineRoutine.FAILURE: {
      return { ...state };
    }
    case createVoyageLineRoutine.FULFILL: {
      return { ...state, loading: false };
    }

      // 计划下期航线
    case createNextVoyageLineRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case createNextVoyageLineRoutine.SUCCESS: {
      return { ...state };
    }
    case createNextVoyageLineRoutine.FAILURE: {
      return { ...state };
    }
    case createNextVoyageLineRoutine.FULFILL: {
      return { ...state, loading: false };
    }

      // 获取航线类型列表
    case getLineTypeListRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case getLineTypeListRoutine.SUCCESS: {
      const lineTypeList = action.payload;
      return { ...state, lineTypeList };
    }
    case getLineTypeListRoutine.FAILURE: {
      return { ...state };
    }
    case getLineTypeListRoutine.FULFILL: {
      return { ...state, loading: false };
    }
    // 获取码头列表
    case getWharfListRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case getWharfListRoutine.SUCCESS: {
      const wharfList = action.payload.dtoList;
      return { ...state, wharfList };
    }
    case getWharfListRoutine.FAILURE: {
      return { ...state };
    }
    case getWharfListRoutine.FULFILL: {
      return { ...state, loading: false };
    } 

    //获取航线全称列表
    case getVoyageLineListRoutine.TRIGGER: {
      console.log("开始获取航线全称");
      return { ...state, loading: true };
    }
    case getVoyageLineListRoutine.SUCCESS: {
      const allLineList = action.payload;
      return { ...state, allLineList };
    }
    case getVoyageLineListRoutine.FAILURE: {
      return { ...state };
    }
    case getVoyageLineListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
