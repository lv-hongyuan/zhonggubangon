import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
    getVoyageLineListRoutine,createVoyageLineRoutine, createNextVoyageLineRoutine, changeVoyageLineRoutine, getLineTypeListRoutine,
    getWharfListRoutine
} from './actions';
import {getVoyageAllListRoutine} from "../VoyageEdit/actions";

export function* getShipMainLineSelectList(){
    console.log("来带下拉列表");
    try {
        yield put(getVoyageLineListRoutine.request());
        const response = yield call(
            request,
            ApiFactory.getShipMainLineSelectList(),
        );
        console.log('getVoyageLineList+++', response);
        yield put(getVoyageLineListRoutine.success(response));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e));
        yield put(getVoyageLineListRoutine.failure(e));
    } finally {
        yield put(getVoyageLineListRoutine.fulfill());
    }
}
//!启用新航线
function* createVoyageLine(action) {
    console.log(action);
    try {
        yield put(createVoyageLineRoutine.request());
        const {
            isLineEnable,
            lineType,
            linename,
            voyid,
            popid,
            voyCode,
            wharfNames,
            shipMainLineName,
            shipMainLineId,
        } = action.payload;
        const response = yield call(
            request,
            ApiFactory.createVoyageLine({
                isLineEnable,
                lineType,
                linename,
                voyid,
                popid,
                voyCode,
                wharfNames,
                shipMainLineName,
                shipMainLineId,
            }),
        );
        console.log('createVoyageLine', response);
        yield put(createVoyageLineRoutine.success({}));
    } catch (e) {
        console.log(e.message);
        yield put(createVoyageLineRoutine.failure(e));
    } finally {
        yield put(createVoyageLineRoutine.fulfill());
    }
}

export function* createVoyageLineSaga() {
    yield takeLatest(createVoyageLineRoutine.TRIGGER, createVoyageLine);
}
//!计划下期航线
function* createNextVoyageLine(action) {
    console.log(action);
    try {
        yield put(createNextVoyageLineRoutine.request());
        const {
            isLineEnable,
            lineType,
            linename,
            voyid,
            popid,
            voyCode,
            shipMainLineName,
            shipMainLineId,
            wharfNames
        } = action.payload;
        const response = yield call(
            request,
            ApiFactory.createNextVoyageLine({
                isLineEnable,
                lineType,
                linename,
                voyid,
                popid,
                voyCode,
                shipMainLineName,
                shipMainLineId,
                wharfNames
            }),
        );
        console.log('createNextVoyageLine', response);
        yield put(createNextVoyageLineRoutine.success({}));
    } catch (e) {
        console.log(e.message);
        yield put(createNextVoyageLineRoutine.failure(e));
    } finally {
        yield put(createNextVoyageLineRoutine.fulfill());
    }
}

export function* createNextVoyageLineSaga() {
    yield takeLatest(createNextVoyageLineRoutine.TRIGGER, createNextVoyageLine);
}
//!更改航线
function* changeVoyageLine(action) {
    console.log('11111',action);
    try {
        yield put(changeVoyageLineRoutine.request());
        const {
            isLineEnable,
            isNextLineEnable,
            lineType,
            linename,
            voyid,
            shipMainLineId,
            shipMainLineName,
            wharfNames
        } = action.payload;
        const response = yield call(
            request,
            ApiFactory.changeVoyageLine({
                isLineEnable,
                isNextLineEnable,
                lineType,
                linename,
                voyid,
                shipMainLineId,
                shipMainLineName,
                wharfNames
            }),
        );
        console.log('changeVoyageLine', response);
        yield put(changeVoyageLineRoutine.success());
    } catch (e) {
        console.log(e.message);
        yield put(changeVoyageLineRoutine.failure(e));
    } finally {
        yield put(changeVoyageLineRoutine.fulfill());
    }
}

export function* changeVoyageLineSaga() {
    yield takeLatest(changeVoyageLineRoutine.TRIGGER, changeVoyageLine);
}

function* getLineTypeList(action) {
    console.log(action);
    try {
        yield put(getLineTypeListRoutine.request());
        const response = yield call(
            request,
            ApiFactory.getLineTypeList(),
        );
        console.log('getLineTypeList+++', response);
        yield put(getLineTypeListRoutine.success(response));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e));
        yield put(getLineTypeListRoutine.failure(e));
    } finally {
        yield put(getLineTypeListRoutine.fulfill());
    }
}

export function* getLineTypeListSaga() {
    yield takeLatest(getLineTypeListRoutine.TRIGGER, getLineTypeList);
}
//!获取码头列表
function* getWharfList(action) {
    console.log('action:',action.payload);
    const {portName} = action.payload
    try {
        yield put(getWharfListRoutine.request());
        const response = yield call(
            request,
            ApiFactory.getWharfList(portName),
        );
        yield put(getWharfListRoutine.success(response));
    } catch (e) {
        console.log(e.message);
        yield put(errorMessage(e));
        yield put(getWharfListRoutine.failure(e));
    } finally {
        yield put(getWharfListRoutine.fulfill());
    }
}
export function* getWharfListSaga() {
    yield takeLatest(getWharfListRoutine.TRIGGER, getWharfList);
}

export function* getVoyageLineListSaga() {
    yield takeLatest(getVoyageLineListRoutine.TRIGGER, getShipMainLineSelectList);
}

// All sagas to be loaded
export default [createVoyageLineSaga, createNextVoyageLineSaga, changeVoyageLineSaga, getLineTypeListSaga,getVoyageLineListSaga,getWharfListSaga ];
