/*
 *
 * VoyageLineEdit constants
 *
 */
export const CREATE_VOYAGE_LINE = 'onLand/PortWork/VoyageLineEdit/createVoyageLine';
export const CREATE_NEXT_VOYAGE_LINE = 'onLand/PortWork/VoyageLineEdit/createNextVoyageLine';
export const CHANGE_VOYAGE_LINE = 'onLand/PortWork/VoyageLineEdit/changeVoyageLine';
export const GET_LINE_TYPE_LIST = 'onLand/PortWork/VoyageLineEdit/getLineTypeList';
export const GET_WHARF_LIST = 'onLand/PortWork/VoyageLineEdit/getWharfList';

export const LineEditTypeKey = 'LineEditTypeKey';
export const LineEditType = {
  PLAN_NEW: 0, // 计划新航线
  ADD_NEXT: 1, // 计划下一期航线
  EDIT: 2, // 编辑已有航线
};
