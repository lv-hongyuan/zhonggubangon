import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { getShipCertificateRoutine } from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getCertificateManagement(action) {
  console.log(action);
  try {
    yield put(getShipCertificateRoutine.request());
    const shipId = action.payload;
    const response = yield call(request, ApiFactory.getShipCred(shipId));
    console.log('getShipCertificate', response);
    const { dtoList } = (response || {});
    yield put(getShipCertificateRoutine.success(dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipCertificateRoutine.failure(e));
  } finally {
    yield put(getShipCertificateRoutine.fulfill());
  }
}

export function* getCertificateManagementSaga() {
  yield takeLatest(getShipCertificateRoutine.TRIGGER, getCertificateManagement);
}

// All sagas to be loaded
export default [
  getCertificateManagementSaga,
];
