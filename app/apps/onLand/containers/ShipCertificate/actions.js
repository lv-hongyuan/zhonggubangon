import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_CERTIFICATE } from './constants';

export const getShipCertificateRoutine = createRoutine(GET_SHIP_CERTIFICATE);
export const getShipCertificatePromise = promisifyRoutine(getShipCertificateRoutine);
