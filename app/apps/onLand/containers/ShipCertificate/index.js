import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {
  Container, Content, View, InputGroup, Item, Label,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { makeData, makeIsLoading } from './selectors';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';
import { getShipCertificatePromise } from './actions';
import SelectImageView from '../../../../components/SelectImageView';

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#DF001B',
    backgroundColor: '#ffffff',
    padding: 5,
  },
  titleLabel: {
    fontSize: 16,
    lineHeight: 16,
    color: '#DF001B',
    padding: 5,
  },
  label: {
    fontSize: 14,
    lineHeight: 15,
    paddingTop: 1,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    lineHeight: 15,
    paddingTop: 1,
    color: '#535353',
  },
});

/**
 *  船舶证书页面
 * Created by jianzhexu on 2018/3/26
 */
@screenHOC
class ShipCertificate extends React.PureComponent {
  static navigationOptions = () => ({
    title: '船舶证书',
  });

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    getCertificateManagement: PropTypes.func.isRequired,
    data: PropTypes.array,
  };

  static defaultProps = {
    data: [],
  };

  static dateStr = (str) => {
    if (!str || str === '1970-01-01 08:00') {
      return '';
    }
    return str.slice(0, 10);
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.props.navigation.setParams({ submit: this.handleSubmit });
  }

  componentDidMount() {
    const shipId = this.props.navigation.getParam('shipId');
    this.props.getCertificateManagement(shipId)
      .then(() => {
      })
      .catch(() => {
      });
  }

  render() {
    return (
      <Container>
        <Content
          style={{ backgroundColor: '#FFF' }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          contentContainerStyle={{ paddingBottom: 10 }}
        >
          {(this.props.data || []).map((item) => {
            const {
              finalInspectionDay,
              certificatePic,
              issuingDay,
              effectiveDay,
              type,
            } = item;
            return (
              <View style={styles.container} key={type}>
                <Label style={styles.titleLabel}>{type}</Label>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={styles.label}>发证日期:</Label>
                    <Label style={styles.text}>{ShipCertificate.dateStr(issuingDay)}</Label>
                  </Item>
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={styles.label}>有效期至:</Label>
                    <Label style={styles.text}>{ShipCertificate.dateStr(effectiveDay)}</Label>
                  </Item>
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={styles.label}>最后年检日期:</Label>
                    <Label style={styles.text}>{ShipCertificate.dateStr(finalInspectionDay)}</Label>
                  </Item>
                </InputGroup>
                <InputGroup
                  style={[commonStyles.inputGroup, {
                    height: 'auto',
                    paddingTop: 10,
                    paddingBottom: 10,
                  }]}
                >
                  <Item style={[commonStyles.inputItem, {
                    justifyContent: 'flex-start',
                    height: 100,
                    borderBottomWidth: 0,
                  }]}
                  >
                    <Label style={styles.label}>证件图片:</Label>
                    <SelectImageView
                      source={certificatePic}
                    />
                  </Item>
                </InputGroup>
              </View>
            );
          })}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: makeData(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getCertificateManagement: getShipCertificatePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipCertificate);
