import { getShipCertificateRoutine } from './actions';

const initState = {
  data: undefined,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipCertificateRoutine.TRIGGER:
      return { ...state, data: [], loading: true };
    case getShipCertificateRoutine.SUCCESS:
      return { ...state, data: action.payload };
    case getShipCertificateRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getShipCertificateRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
