import { createSelector } from 'reselect/es';

const selectCertificateManagementDomain = () => state => state.shipCertificate;

const makeData = () => createSelector(
  selectCertificateManagementDomain(),
  (subState) => {
    console.debug(subState.data);
    return (subState.data && subState.data.length > 0) ? subState.data : [{}];
  },
);

const makeIsLoading = () => createSelector(
  selectCertificateManagementDomain(),
  (subState) => {
    console.debug(subState.loading);
    return subState.loading;
  },
);

export {
  makeData,
  makeIsLoading,
};
