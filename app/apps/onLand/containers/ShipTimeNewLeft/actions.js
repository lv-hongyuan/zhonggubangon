import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TIME_NEW_LEFT } from './constants';

export const getShipTimeNewLeftRoutine = createRoutine(GET_SHIP_TIME_NEW_LEFT);
export const getShipTimeNewLeftPromiseCreator = promisifyRoutine(getShipTimeNewLeftRoutine);
