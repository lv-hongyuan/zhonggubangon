import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipTimeNewLeftRoutine } from './actions';

function* getShipTimeNewLeft(action) {
  const { page, pageSize, ...rest} = (action.payload || {});
  
  try {
    yield put(getShipTimeNewLeftRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      param.filter.filters.push({
        field: key,
        operator: 'eq',
        value,
      });
    });
    console.log('param:',param)
    const response = yield call(request, ApiFactory.getShipPopList(param));
    console.log(response);
    yield put(getShipTimeNewLeftRoutine.success({
      list:(response.dtoList ? response.dtoList.content : []),
      page,
      pageSize,
      type:response.type,
      backmes:response._backmes,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getShipTimeNewLeftRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getShipTimeNewLeftRoutine.fulfill());
  }
}

export function* getShipTimeNewLeftSaga() {
  yield takeLatest(getShipTimeNewLeftRoutine.TRIGGER, getShipTimeNewLeft);
}

// All sagas to be loaded
export default [getShipTimeNewLeftSaga];