import { createSelector } from 'reselect';

const selectShipTimeNewLeftDomain = () => state => state.shipTimeNewLeft;
const makeList = () => createSelector(selectShipTimeNewLeftDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectShipTimeNewLeftDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

const makeDefaultType = () => createSelector(selectShipTimeNewLeftDomain(), (subState) => {
  console.debug(subState.defaultType);
  return subState.defaultType
})
export { makeList, makeRefreshState ,makeDefaultType};