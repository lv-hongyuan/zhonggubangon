import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import CarouselLabel from '../../../../../components/CarouselLabel';
const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: '#fff',
    flexDirection: 'column',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.7,
  },
  col: {
    height: 35,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 15
  },
  texttop: {
    // fontSize: 13,
    width: '45%',
  },
  textbottom: {
    // fontSize: 13,
    width: '55%',
    marginLeft: 5
  },
  title:{
    alignContent: 'stretch',
    height:40,
    justifyContent:'center',
    paddingLeft:25,
    borderBottomColor:'#e8e8e8',
    borderBottomWidth:1,
    borderTopWidth:10,
    borderTopColor:'#e8e8e8',
  }
});

class ShipTimeNewItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      lineName,  //航线
      nextInfo,  //预计
      prevInfo,  //已到达
      shipName,  //船名
      voyageCode, //航次
      isFirst,    //是否显示title
      port,       //本港
    } = (this.props.item || {});
    const type = this.props.type
    return (
      <View style={styles.container}>
        {isFirst == 1 && type == 30 ?
          <View style={styles.title}>
            <Text style={{fontSize:15,color:'rgb(60,150,200)'}}>{port}</Text>
          </View> : null}
        <View style={styles.col}>
          <Text style={styles.texttop}>{shipName + '/' + voyageCode}</Text>
          <Text style={styles.textbottom}>{prevInfo ? prevInfo : '暂无数据......'}</Text>
        </View>
        <View style={styles.col}>
          {
            lineName.length > 15 ? <CarouselLabel
              bgViewStyle={{
                overflow: 'hidden',
                width: '45%',
              }}
              textContainerHeight={30}
              speed={50}
              text={lineName ? lineName : '暂无数据......'}
              textStyle={{
                fontSize: 13,
                color: '#000',
                lineHeight: 30,
                width: lineName.length * 14
              }}
            /> :
              <Text style={styles.texttop}>{lineName}</Text>
          }
          <View style={{ flexDirection: 'row', alignItems: 'center', width: '55%' }}>
            <Text style={{  marginLeft: 5 }}>{nextInfo ? nextInfo : '暂无数据......'}</Text>
          </View>

        </View>
      </View>
    );
  }
}

ShipTimeNewItem.propTypes = {
  item: PropTypes.object.isRequired,
  type:PropTypes.number.isRequired,
};

export default ShipTimeNewItem;
