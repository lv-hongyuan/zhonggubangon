import { getShipTimeNewLeftRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';
const initState = {
  list: [],
  refreshState: RefreshState.Idle,
  defaultType:0,
  loading:false
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipTimeNewLeftRoutine.TRIGGER:{
      const { page } = action.payload;
      return { 
        ...state, 
        loading: true, 
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      }
    }
    case getShipTimeNewLeftRoutine.SUCCESS:{
      const { page, pageSize, list ,type} = action.payload;
      return { 
        ...state,
        list: page == 1 ? fixIdList(list) : fixIdList(state.list.concat(list)),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
        defaultType:type
      }
    }
      
    case getShipTimeNewLeftRoutine.FAILURE:{
      const { page, error } = action.payload;
      return {
        ...state,
        list: page === 1 ? [] : state.list,
        error,
        refreshState: RefreshState.Failure,
        defaultType:0,
      };
    }
      
    case getShipTimeNewLeftRoutine.FULFILL:{
      return { ...state, loading: false };
    }
      

    default:
      return state;
  }
}