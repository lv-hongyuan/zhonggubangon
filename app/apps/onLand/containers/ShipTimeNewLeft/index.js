import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, TouchableOpacity, DeviceEventEmitter } from 'react-native';
import { connect } from 'react-redux';
import { Text, View } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getShipTimeNewLeftPromiseCreator } from './actions';
import { makeList, makeRefreshState, makeDefaultType } from './selectors';
import screenHOC from '../../../../components/screenHOC';
import ShipTimeNewItem from './components/ShipTimeNewItem';
import RefreshListView from '../../../../components/RefreshListView';
import AlertView from '../../../../components/Alert';
const styles = {
	toptitle: {
		height: 30,
		backgroundColor: '#e8e8e8',
		flexDirection: 'row',
		
	},
	headerbutton: {
		width: '25%',
		alignItems: 'center',
		justifyContent:'center',
	},
	text: {
		color: '#999',
		fontSize: 15
	},
	container: {
		backgroundColor: 'white',
		flexDirection: 'row',
		alignContent: 'stretch',
	},
}
const firstPageNum = 1;
const DefaultPageSize = 10;
@screenHOC
class ShipTimeNewLeft extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			containerOffSet: new Animated.Value(0),
			containerWidth: undefined,
			pageSize: DefaultPageSize,  //展示行数
			page: firstPageNum,
			searchPq: '',     //查询条件：片区
			type: 0,   //查询类型
			searchShipName: '',  //查询条件：船名
			searchPortName: '',   //查询条件：港口
			text1focus: true     //进入页面时的默认类型
		};
	}

	componentDidMount() {
		this.listView.beginRefresh();

		//  监听搜索条件
		this.listener = DeviceEventEmitter.addListener('addServiceAudits', (data) => {
			this.listView.beginRefresh();
			this.setState({
				searchPq: data.searchPq,
				searchShipName: data.searchShipName,
				searchPortName: data.searchPortName,
				showPermissions:true
			})
			this.loadList(false, this.state.type, data.searchShipName, data.searchPortName, data.searchPq);
			
		});
		console.log(this.props.navigation);
	}

	//  移除监事件
	componentWillUnmount(){
		this.listener.remove();
	}


	//加载列表
	loadList(loadMore = false, type = this.props.defaultType, searchShipName, searchPortName, searchPq) {
		const { page, pageSize } = this.state;
		this.props.getShipTimeNewLeftPromiseCreator({
			type: type ? type : 0,
			page: loadMore ? page : 1,
			pageSize,
			searchShipName,
			searchPortName,
			searchPq
		})
			.then(({ page, type, backmes}) => {
				if (type == 40) {
					this.setState({ text1focus: true });
				} else if (type == 10) {
					this.setState({ text2focus: true, text1focus: false });
				} else if (type == 30) {
					this.setState({ text3focus: true, text1focus: false ,type:30});
				} else if (type == 20) {
					this.setState({ text4focus: true, text1focus: false });
				}
				if(backmes == '当前用户无此权限！' && this.state.showPermissions == false){
					AlertView.show({
						message: backmes,
						showCancel: false,
						confirmAction: () => {},
					});
				}
				this.setState({ page: page + 1 ,showPermissions:false});
			})
			.catch(() => {
			});
	}

	// 上拉加载
	onHeaderRefresh = () => {
		this.loadList(
			false,
			this.state.type,
			this.state.searchShipName,
			this.state.searchPortName,
			this.state.searchPq
		);
	};

	// 下拉刷新
	onFooterRefresh = () => {
		this.loadList(
			true,
			this.state.type,
			this.state.searchShipName,
			this.state.searchPortName,
			this.state.searchPq
		);
	};

	renderItem = ({ item }) => {
		return (
			<ShipTimeNewItem
				item={item}
				type={this.state.type}
			/>
		)
	};

	render() {
		const textStyle = { fontSize: 16, color: 'black' }
		return (
			<SafeAreaView style={{ flex: 1 }}>
				<View style={{ width: '100%', flex: 1,backgroundColor:'#e8e8e8' }}>
					<View style={[styles.toptitle,{
						marginBottom:this.state.type == 30 ? 0 : 10,
						paddingTop:this.state.type == 30 ? 5 : 10,
					}]}>
						<TouchableOpacity
							style={styles.headerbutton}
							onPress={() => {
								this.loadList(
									false,
									40,
									this.state.searchShipName,
									this.state.searchPortName,
									this.state.searchPq
								);
								this.listView.beginRefresh();
								this.setState({
									text1focus: true,
									text2focus: false,
									text3focus: false,
									text4focus: false,
									type: 40
								})
							}}
						>
							<Text style={[styles.text, this.state.text1focus ? textStyle : {}]}>所有</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.headerbutton}
							onPress={() => {
								this.loadList(
									false,
									10,
									this.state.searchShipName,
									this.state.searchPortName,
									this.state.searchPq
								);
								this.listView.beginRefresh();
								this.setState({
									text1focus: false,
									text2focus: true,
									text3focus: false,
									text4focus: false,
									type: 10
								})
							}}
						>
							<Text style={[styles.text, this.state.text2focus ? textStyle : {}]}>自有船</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.headerbutton}
							onPress={() => {
								this.loadList(
									false,
									30,
									this.state.searchShipName,
									this.state.searchPortName,
									this.state.searchPq
								);
								this.listView.beginRefresh();
								this.setState({
									text1focus: false,
									text2focus: false,
									text3focus: true,
									text4focus: false,
									type: 30
								})
							}}
						>
							<Text style={[styles.text, this.state.text3focus ? textStyle : {}]}>负责港口</Text>
						</TouchableOpacity>
						<TouchableOpacity
							style={styles.headerbutton}
							onPress={() => {
								this.loadList(
									false,
									20,
									this.state.searchShipName,
									this.state.searchPortName,
									this.state.searchPq
								);
								this.listView.beginRefresh();
								this.setState({
									text1focus: false,
									text2focus: false,
									text3focus: false,
									text4focus: true,
									type: 20
								})
							}}
						>
							<Text style={[styles.text, this.state.text4focus ? textStyle : {}]}>负责船舶</Text>
						</TouchableOpacity>
					</View>
					<RefreshListView
						ref={(ref) => { this.listView = ref; }}
						data={this.props.list || []}
						keyExtractor={item => `${item.id}`}
						renderItem={this.renderItem}
						refreshState={this.props.refreshState}
						onHeaderRefresh={this.onHeaderRefresh}
						onFooterRefresh={this.onFooterRefresh}
					/>
				</View>
			</SafeAreaView>
		);
	}
}

ShipTimeNewLeft.navigationOptions = () => ({
	gesturesEnabled: false,
	tabBarLabel: '船舶动态',
});

ShipTimeNewLeft.propTypes = {
	navigation: PropTypes.object.isRequired,
	getShipTimeNewLeftPromiseCreator: PropTypes.func.isRequired,
	list: PropTypes.array,
	refreshState: PropTypes.number.isRequired,
	defaultType: PropTypes.number,
};
ShipTimeNewLeft.defaultProps = {
	list: [],
};

const mapStateToProps = createStructuredSelector({
	refreshState: makeRefreshState(),
	list: makeList(),
	defaultType: makeDefaultType()
});

function mapDispatchToProps(dispatch) {
	return {
		...bindPromiseCreators({
			getShipTimeNewLeftPromiseCreator,
		}, dispatch),
		dispatch,
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipTimeNewLeft);