import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';
import { portWorkStateTitle } from '../../PortWork/constants';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class DailyScheduleItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pq, lineName, shipName, voyCode, thePortName, nextPortName, atd, eta, ata, eatb, etd, stateName, remark,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{lineName}</Text>
        </View>
        <View style={[styles.col, { width: 150 }]}>
          <Text style={styles.text}>{shipName + '/' + voyCode + '/' + thePortName}</Text>
        </View>
        {/*<View style={styles.col}>*/}
        {/*  <Text style={styles.text}>{voyCode}</Text>*/}
        {/*</View>*/}
        {/*<View style={styles.col}>*/}
        {/*  <Text style={styles.text}>{thePortName}</Text>*/}
        {/*</View>*/}
        <View style={styles.col}>
          <Text style={styles.text}>{nextPortName}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!atd && defaultFormat(atd)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!eta && defaultFormat(eta)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!ata && defaultFormat(ata)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!eatb && defaultFormat(eatb)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!etd && defaultFormat(etd)}</Text>
        </View>
        <View style={styles.col}>
          {/* <Text style={styles.text}>{portWorkStateTitle(parseInt(stateName, 10))}</Text> */}
          <Text style={styles.text}>{stateName}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{remark}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pq}</Text>
            </View>
            <View style={[styles.col, { width: 140 }]}>
              <Text style={styles.text}>{lineName}</Text>
            </View>
            <View style={[styles.col, { width: 150 }]}>
              <Text style={styles.text}>{shipName + '/' + voyCode + '/' + thePortName}</Text>
            </View>
            {/*<View style={styles.col}>*/}
            {/*  <Text style={styles.text}>{voyCode}</Text>*/}
            {/*</View>*/}
            {/*<View style={styles.col}>*/}
            {/*  <Text style={styles.text}>{thePortName}</Text>*/}
            {/*</View>*/}
          </View>
        </Animated.View>
      </View>
    );
  }
}

DailyScheduleItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default DailyScheduleItem;
