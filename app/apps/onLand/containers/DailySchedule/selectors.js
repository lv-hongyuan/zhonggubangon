import { createSelector } from 'reselect/es';

const selectDailyScheduleDomain = () => state => state.dailySchedule;

const makeList = () => createSelector(selectDailyScheduleDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDailyScheduleDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
