import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDailyScheduleRoutine } from './actions';

function* getDailySchedule(action) {
  console.log(action);
  try {
    yield put(getDailyScheduleRoutine.request());
    const response = yield call(request, ApiFactory.shipDateList());
    console.log(response);
    yield put(getDailyScheduleRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDailyScheduleRoutine.failure(e));
  } finally {
    yield put(getDailyScheduleRoutine.fulfill());
  }
}

export function* getDailyScheduleSaga() {
  yield takeLatest(getDailyScheduleRoutine.TRIGGER, getDailySchedule);
}

// All sagas to be loaded
export default [getDailyScheduleSaga];
