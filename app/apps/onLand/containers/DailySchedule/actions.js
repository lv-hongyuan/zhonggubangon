import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DAILY_SCHEDULE } from './constants';

export const getDailyScheduleRoutine = createRoutine(GET_DAILY_SCHEDULE);
export const getDailySchedulePromise = promisifyRoutine(getDailyScheduleRoutine);
