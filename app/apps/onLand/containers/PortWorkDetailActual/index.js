import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  InputGroup,
  Container,
  Content,
  Form,
  Item,
  Label,
  Button,
  Text,
  View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import InputItem from '../../../../components/InputItem';
import { MaskType } from '../../../../components/InputItem/TextInput';
import Selector from '../../../../components/Selector';
import Switch from '../../../../components/Switch';
import {
  makeBerthingEventsNum,
  makeUnFinishedBerthingEventsNum,
  makeOperatingEventsNum,
  makeUnFinishedOperatingEventsNum,
  makeUnBerthingEventsNum,
  makeUnFinishedUnBerthingEventsNum,
} from '../Emergencies/selectors';
import {
  getPierListPromise,
  getBerthListPromise,
  saveBerthingPromise,
  completeBerthingPromise,
  saveOperatingPromise,
  completeOperatingPromise,
  saveUnBerthingPromise,
  completeUnBerthingPromise,
} from '../PortWorkDetail/actions';
import { BerthingState } from '../PortWorkDetail/constants';
import {
  makeSelectPortWorkDetail,
  makePierList,
  makeBerthList,
} from '../PortWorkDetail/selectors';
import { ROUTE_EMERGENCIES, ROUTE_PORT_ATTENTION } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import TouchableCell from '../../../../components/ExtendCell/TouchableCell';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import DatePullSelector from '../../../../components/DatePullSelector/index';
import { PortWorkState } from '../PortWork/constants';
import AlertView from '../../../../components/Alert';
import commonStyles from '../../../../common/commonStyles';
import Svg from '../../../../components/Svg';
import { EventRange } from '../../common/Constant';
import screenHOC from '../../../../components/screenHOC';
import EventsCell from './EventsCell';

const styles = StyleSheet.create({
  buttonContainer: {
    height: 60,
    borderColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
  },
  saveButton: {
    backgroundColor: '#FBB03B',
    height: 40,
    flex: 1,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  completeButton: {
    backgroundColor: '#DC001B',
    height: 40,
    flex: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  changePlanButton: {
    backgroundColor: '#FBB03B',
    height: 40,
    flex: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  doneImage: {
    position: 'absolute',
    top: 0,
    right: 40,
    opacity: 0.2,
  },
  tipText: {
    paddingTop: 10,
    paddingLeft: 0,
    paddingBottom: 0,
    fontSize: 12,
    color: 'red',
  },
  inLineTouchCell: {
    backgroundColor: '#ffffff',
    borderTopWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
});

function compare(times) {
  let minKey;
  let minValue;
  let message = null;
  times.every(({ key, value }) => {
    if (value) {
      if (!minKey) {
        minKey = key;
        minValue = value;
      } else if (value < minValue) {
        message = `${key}不能小于${minKey}`;
        return false;
      } else {
        minKey = key;
        minValue = value;
      }
      return true;
    }
    return false;
  });
  return message;
}

@screenHOC
class PortWorkDetailActual extends React.PureComponent {
  static stateFromWordDetail(detail) {
    const workDetail = detail || {};

    function zeroToNull(time) {
      return time === 0 ? undefined : time;
    }
    return {
      data: {
        etaSource: workDetail.etaSource,
        etaSourceTime: workDetail.etaSourceTime,
        portId: workDetail.portId,
        voyageCode: workDetail.voyageCode,
        statePort: workDetail.statePort,
        estWaitBerthTime: workDetail.estWaitBerthTime,
        waitBerthCause: workDetail.waitBerthCause,
        estWaitGoodsTime: workDetail.estWaitGoodsTime,
        waitGoodsCause: workDetail.waitGoodsCause,
        wharf: workDetail.wharf,
        berth: workDetail.berth,
        berthWork: workDetail.berthWork,
        voyageId: workDetail.voyageId,
        portName: workDetail.portName,
        nextPortId: workDetail.nextPortId,
        version: workDetail.version,
        directBerth: workDetail.directBerth,
        shipId: workDetail.shipId,
        id: workDetail.id,
        shipName: workDetail.shipName,
        portUserId: workDetail.portUserId,
        etaPlan: zeroToNull(workDetail.etaPlan),
        etbPlan: zeroToNull(workDetail.etbPlan),
        etdPlan: zeroToNull(workDetail.etdPlan),
        atdPort: zeroToNull(workDetail.atdPort),
        atbPort: zeroToNull(workDetail.atbPort),
        etdAfterBerth: zeroToNull(workDetail.etdAfterBerth),
        startWorkPort: zeroToNull(workDetail.startWorkPort),
        finishWorkPortMiddle: zeroToNull(workDetail.finishWorkPortMiddle),
        wharf1: workDetail.wharf1,
        workCon1: workDetail.workCon1,
        reverseCon1: workDetail.reverseCon1,
        wharf2: workDetail.wharf2,
        workCon2: workDetail.workCon2,
        reverseCon2: workDetail.reverseCon2,
        startWorkPortMiddle: zeroToNull(workDetail.startWorkPortMiddle),
        doubleHanger: workDetail.doubleHanger === '是' ? '是' : '否',
        finishWorkPort: zeroToNull(workDetail.finishWorkPort),
        ataPort: zeroToNull(workDetail.ataPort),
        directBerthPort: workDetail.directBerthPort,
        waitType: workDetail.directBerthPort === 1 ? BerthingState.directBerth : workDetail.waitType,
        weighAnchorPort: zeroToNull(workDetail.weighAnchorPort),
        planMemo: workDetail.planMemo,
        berthOne: workDetail.berthOne,
        berthTwo: workDetail.berthTwo,
        wharfWork: workDetail.wharfWork,
        workMemo: workDetail.workMemo,
      },
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      popId: this.props.navigation.getParam('popId'),
      portName: this.props.navigation.getParam('portName'),
      data: PortWorkDetailActual.stateFromWordDetail(props.workDetail),
      openBerthing: props.workDetail && props.workDetail.statePort >= PortWorkState.berthing,
      openOperating: props.workDetail && props.workDetail.statePort >= PortWorkState.operating,
      openUnBerthing: props.workDetail && props.workDetail.statePort >= PortWorkState.unBerthing,
      changingPlan: false,
      changingBerthing: false,
      changingOperating: false,
      changingUnBerthing: false,
    };

    if (props.workDetail) {
      this.setWordDetail(props.workDetail);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.workDetail !== nextProps.workDetail) {
      console.log('this.props.workDetail', this.props.workDetail);
      console.log('nextProps.workDetail', nextProps.workDetail);
      const workDetail = nextProps.workDetail || {};

      // 首次设置数据
      if (!this.props.workDetail) {
        this.setState({
          openBerthing: workDetail.statePort >= PortWorkState.berthing,
          openOperating: workDetail.statePort >= PortWorkState.operating,
          openUnBerthing: workDetail.statePort >= PortWorkState.unBerthing,
        });
      } else {
        if (this.state.changingPlan) {
          this.setState({ changingPlan: false });
        }
        if (this.state.changingBerthing) {
          this.setState({ changingBerthing: false });
        }
        if (this.state.changingOperating) {
          this.setState({ changingOperating: false });
        }
        if (this.state.changingUnBerthing) {
          this.setState({ changingUnBerthing: false });
        }
        if (this.props.workDetail.statePort !== workDetail.startWorkPort) {
          switch (workDetail.statePort) {
            case PortWorkState.berthing:
              this.setState({ openBerthing: true });
              break;
            case PortWorkState.operating:
              this.setState({ openOperating: true });
              break;
            case PortWorkState.unBerthing:
              this.setState({ openUnBerthing: true });
              break;
            default:
              break;
          }
        }
      }

      this.setWordDetail(workDetail);
    }
  }

  onBtnPier = () => {
    if (this.props.pierList.length > 0) {
      this.selector.showPicker();
    } else {
      this.props.getPierListPromise(this.props.workDetail.portId)
          .then(() => {
            this.selector.showPicker();
          })
          .catch(() => {});
    }
  };

  onBtnPier1 = () => {
    if (this.props.pierList.length > 0) {
      this.selector1.showPicker();
    } else {
      this.props.getPierListPromise(this.props.workDetail.portId)
          .then(() => {
            this.selector1.showPicker();
          })
          .catch(() => {});
    }
  };
  onChangePier = (item) => {
    this.setData({
      wharfWork: item.text,
    });
  };
  onChangePier1 = (item) => {
    this.setData({
      wharf1: item.text,
    });
  };

  onBtnPier2 = () => {
    if (this.props.pierList.length > 0) {
      this.selector2.showPicker();
    } else {
      this.props.getPierListPromise(this.props.workDetail.portId)
          .then(() => {
            this.selector2.showPicker();
          })
          .catch(() => {});
    }
  };

  onChangePier2 = (item) => {
    this.setData({
      wharf2: item.text,
    });
  };

  setWordDetail(detail) {
    const state = PortWorkDetailActual.stateFromWordDetail(detail);
    this.setState({ ...state });
  }

  setData(params, callback = () => {}) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    }, callback);
  }

  changeBerthing = () => {
    this.validBerthingConfirm()
        .then(() => {
          AlertView.show({
            message: '确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.saveBerthingPromise({
                ...this.state.data,
                isUpdate: 1,
              })
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  changeOperating = () => {
    this.validOperatingConfirm()
        .then(() => {
          AlertView.show({
            message: '确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.saveOperatingPromise({
                ...this.state.data,
                isUpdate: 1,
              })
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  changeUnBerthing = () => {
    this.validUnBerthingSave()
        .then(() => {
          AlertView.show({
            message: '确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.saveUnBerthingPromise({
                ...this.state.data,
                isUpdate: 1,
              })
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  validBerthingSave() {
    const times = [];
    times.push( {
      key: '到达锚地时间',
      value: this.state.data.ataPort,
    });
    if (this.state.data.weighAnchorPort) {
      times.push({
        key: '起锚时间',
        value: this.state.data.weighAnchorPort,
      })
    }
    times.push({
      key: '实际靠泊时间',
      value: this.state.data.atbPort,
    });
    const message = compare(times);

    if (message) {
      return Promise.reject(new Error(message));
    }
    return Promise.resolve();
  }

  validBerthingConfirm() {
    if (!this.state.data.ataPort) {
      return Promise.reject(new Error('请填写到达锚地时间'));
    }
    // 非直靠
    if (this.state.data.directBerthPort === 0) {
      // if (!this.state.data.waitType) {
      //   return Promise.reject(new Error('请填写是否直靠'));
      // }
      // if (!this.state.data.weighAnchorPort) {
      //   return Promise.reject(new Error('请填写起锚时间'));
      // }
    }
    if (!this.state.data.atbPort) {
      return Promise.reject(new Error('请填写实际靠泊时间'));
    }
    return this.validBerthingSave();
  }

  //!靠泊阶段点击暂存
  saveBerthing = () => {
    this.validBerthingSave()
        .then(() => {
          this.props.saveBerthingPromise(this.state.data)
              .then(() => {})
              .catch(() => {});
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  completeBerthing = () => {
    this.validBerthingConfirm()
        .then(() => {
          AlertView.show({
            message: '确定后的数据将无法修改，确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.completeBerthingPromise(this.state.data)
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  validOperatingSave() {
    let times;
    if (this.state.data.doubleHanger === '是') {
      times = [
        {
          key: '实际靠泊时间',
          value: this.state.data.atbPort,
        },
        {
          key: '进口作业开始时间',
          value: this.state.data.startWorkPort,
        },
        {
          key: '进口作业结束时间',
          value: this.state.data.finishWorkPortMiddle,
        },
        {
          key: '出口作业开始时间',
          value: this.state.data.startWorkPortMiddle,
        },
        {
          key: '出口作业结束时间',
          value: this.state.data.finishWorkPort,
        },
      ];
    } else {
      times = [
        {
          key: '实际靠泊时间',
          value: this.state.data.atbPort,
        },
        {
          key: '作业开始时间',
          value: this.state.data.startWorkPort,
        },
        {
          key: '作业结束时间',
          value: this.state.data.finishWorkPort,
        },
      ];
    }

    let tempValue = null;
    let errorFlag = false;
    times.reduce((pre, current) => {
      const { value } = current;
      if (!tempValue && !value) {
        tempValue = current;
      } else if (tempValue && value) {
        errorFlag = true;
      }
      return current;
    }, null);
    if (errorFlag) {
      return Promise.reject(new Error(`请输入${tempValue.key}`));
    }
    const message = compare(times);

    if (message) {
      return Promise.reject(new Error(message));
    }
    return Promise.resolve();
  }

  validOperatingConfirm() {
    if (this.state.data.doubleHanger === '是') {
      if (_.isEmpty(this.state.data.wharf1)) {
        return Promise.reject(new Error('请填写进口作业码头'));
      }
      if (_.isEmpty(this.state.data.berthOne)) {
        return Promise.reject(new Error('请填写进口泊位'));
      }
      if (!this.state.data.startWorkPort) {
        return Promise.reject(new Error('请填写进口作业开始时间'));
      }
      if (!this.state.data.finishWorkPortMiddle) {
        return Promise.reject(new Error('请填写进口作业结束时间'));
      }
      if (_.isNil(this.state.data.workCon1) || this.state.data.workCon1 === '') {
        return Promise.reject(new Error('请填写进口作业箱量'));
      }
      if (_.isNil(this.state.data.reverseCon1)) {
        return Promise.reject(new Error('请填写进口倒箱量'));
      }
      if (_.isEmpty(this.state.data.wharf2)) {
        return Promise.reject(new Error('请填写出口作业码头'));
      }
      if (_.isEmpty(this.state.data.berthTwo)) {
        return Promise.reject(new Error('请填写出口作业泊位'));
      }
      if (!this.state.data.startWorkPortMiddle) {
        return Promise.reject(new Error('请填写出口作业开始时间'));
      }
      if (!this.state.data.finishWorkPort) {
        return Promise.reject(new Error('请填写出口作业结束时间'));
      }
      if (_.isNil(this.state.data.workCon2) || this.state.data.workCon2 === '') {
        return Promise.reject(new Error('请填写出口作业箱量'));
      }
      if (_.isNil(this.state.data.reverseCon2) || this.state.data.workCon2 === '') {
        return Promise.reject(new Error('请填写出口倒箱量'));
      }
    } else {
      if (!this.state.data.startWorkPort) {
        return Promise.reject(new Error('请填写作业开始时间'));
      }
      if (!this.state.data.finishWorkPort) {
        return Promise.reject(new Error('请填写作业结束时间'));
      }
      if (!this.state.data.wharfWork) {
        return Promise.reject(new Error('请填写作业码头'));
      }
      if (!this.state.data.berthWork) {
        return Promise.reject(new Error('请填写作业泊位'));
      }

    }
    return this.validOperatingSave();
  }

  saveOperating = () => {
    this.validOperatingSave()
        .then(() => {
          this.props.saveOperatingPromise(this.state.data)
              .then(() => {})
              .catch(() => {});
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  completeOperating = () => {
    this.validOperatingConfirm()
        .then(() => {
          AlertView.show({
            message: '确定后的数据将无法修改，确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.completeOperatingPromise(this.state.data)
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  validUnBerthingSave() {
    const times = [
      {
        key: '作业结束时间',
        value: this.state.data.finishWorkPort,
      },
      {
        key: '实际离港时间',
        value: this.state.data.atdPort,
      },
    ];
    const message = compare(times);

    if (message) {
      return Promise.reject(new Error(message));
    }
    if (!this.state.data.atdPort) {
      return Promise.reject(new Error('请填实际离港时间'));
    }
    if (this.state.data.atdPort < this.state.data.finishWorkPort) {
      return Promise.reject(new Error('实际离港时间不能小于作业结束时间'));
    }
    return Promise.resolve();
  }

  validUnBerthingConfirm() {
    if (!this.state.data.atdPort) {
      return Promise.reject(new Error('请填实际离港时间'));
    }
    if (this.state.data.atdPort < this.state.data.finishWorkPort) {
      return Promise.reject(new Error('实际离港时间不能小于作业结束时间'));
    }
    return Promise.resolve();
  }

  saveUnBerthing = () => {
    this.validUnBerthingSave()
        .then(() => {
          this.props.saveUnBerthingPromise(this.state.data)
              .then(() => {})
              .catch(() => {});
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  completeUnBerthing = () => {
    this.validUnBerthingConfirm()
        .then(() => {
          AlertView.show({
            message: '确定后的数据将无法修改，确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.completeUnBerthingPromise(this.state.data)
                  .then(() => {})
                  .catch(() => {});
            },
          });
        })
        .catch((error) => {
          AlertView.show({ message: error.message });
        });
  };

  renderBerthingChange() {
    if (this.state.changingBerthing) {
      return (
          <View style={styles.buttonContainer}>
            <Button
                onPress={() => {
                  this.setState({ changingBerthing: false });
                  this.setWordDetail(this.props.workDetail);
                }}
                style={styles.saveButton}
            >
              <Text>取消</Text>
            </Button>
            <Button onPress={this.changeBerthing} style={styles.completeButton}>
              <Text>提交变更</Text>
            </Button>
          </View>
      );
    }
    return (
        <View style={styles.buttonContainer}>
          <Button
              onPress={() => {
                this.setState({ changingBerthing: true });
              }}
              style={styles.changePlanButton}
          >
            <Text>数据变更</Text>
          </Button>
        </View>
    );
  }

  renderBerthing() {
    const canOpen = this.state.data.statePort >= PortWorkState.berthing;
    if (!canOpen) {
      return (
          <TouchableCell
              title="靠泊阶段"
              isOpen={false}
              style={{
                marginTop: 8,
                backgroundColor: '#ffffff',
              }}
          />
      );
    }
    const isBerthing = this.state.data.statePort === PortWorkState.berthing;
    const canEdit = this.state.changingBerthing || isBerthing;
    return (
        <View style={{
          marginTop: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}
        >
          {
            !canEdit ? (
                <Svg icon="done_blue" width={200} height={100} style={styles.doneImage} />
            ) : (null)
          }
          <ExtendCell
              title="靠泊阶段"
              titleColor="#DC001B"
              isOpen={this.state.openBerthing}
              onPress={() => {
                this.setState({ openBerthing: !this.state.openBerthing });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  到达锚地:
                </Label>
                <DatePullSelector
                    value={this.state.data.ataPort}
                    readOnly={!canEdit}
                    onChangeValue={(value) => {
                      this.setData({ ataPort: value });
                    }}
                />
              </Item>
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  起锚时间:
                </Label>
                <DatePullSelector
                    value={this.state.data.weighAnchorPort}
                    minValue={this.state.data.ataPort}
                    readOnly={!canEdit}
                    onChangeValue={(value) => {
                      this.setData({ weighAnchorPort: value });
                    }}
                />
              </Item>
            </InputGroup>
            <EventsCell
                title="非直靠原因"
                total={this.props.berthingEventsNum}
                unfinished={this.props.unFinishedBerthingEventsNum}
                titleStyle={{
                  marginLeft: 0,
                  color: '#535353',
                }}
                isOpen={false}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_EMERGENCIES, {
                    title: '非直靠原因',
                    id: this.props.workDetail.id,
                    range: EventRange.PORT_BERTHING,
                  });
                }}
            />
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label
                    style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
                >
                  实际靠泊:
                </Label>
                <DatePullSelector
                    value={this.state.data.atbPort}
                    minValue={this.state.data.directBerthPort === 0 ? this.state.data.weighAnchorPort : this.state.data.ataPort}
                    readOnly={!canEdit}
                    onChangeValue={(value) => {
                      this.setData({ atbPort: value });
                    }}
                />
              </Item>
            </InputGroup>
            {isBerthing ? (
                <View style={styles.buttonContainer}>
                  <Button onPress={this.saveBerthing} style={styles.saveButton}>
                    <Text>暂存</Text>
                  </Button>
                  <Button onPress={this.completeBerthing} style={styles.completeButton}>
                    <Text>提交</Text>
                  </Button>
                </View>) : this.renderBerthingChange()
            }
          </ExtendCell>
        </View>
    );
  }

  renderOperatingChange() {
    if (this.state.changingOperating) {
      return (
          <View style={styles.buttonContainer}>
            <Button
                onPress={() => {
                  this.setState({ changingOperating: false });
                  this.setWordDetail(this.props.workDetail);
                }}
                style={styles.saveButton}
            >
              <Text>取消</Text>
            </Button>
            <Button onPress={this.changeOperating} style={styles.completeButton}>
              <Text>提交变更</Text>
            </Button>
          </View>
      );
    }
    return (
        <View style={styles.buttonContainer}>
          <Button
              onPress={() => {
                this.setState({ changingOperating: true });
              }}
              style={styles.changePlanButton}
          >
            <Text>数据变更</Text>
          </Button>
        </View>
    );
  }

  renderOperating() {
    const canOpen = this.state.data.statePort >= PortWorkState.operating;
    if (!canOpen) {
      return (
          <TouchableCell
              title="作业阶段"
              isOpen={false}
              style={{
                marginTop: 8,
                backgroundColor: '#ffffff',
              }}
          />
      );
    }
    const isOperating = this.state.data.statePort === PortWorkState.operating;
    const canEdit = this.state.changingOperating || isOperating;
    return (
        <View style={{
          marginTop: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}
        >
          {
            !canEdit ? (
                <Svg icon="done_blue" width={200} height={100} style={styles.doneImage} />
            ) : (null)
          }
          <ExtendCell
              title="作业阶段"
              titleColor="#DC001B"
              isOpen={this.state.openOperating}
              onPress={() => {
                this.setState({ openOperating: !this.state.openOperating });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>是否双挂:</Label>
                <Switch
                    value={this.state.data.doubleHanger === '是'}
                    readOnly={!canEdit}
                    onPress={() => {
                      this.setData({ doubleHanger: this.state.data.doubleHanger === '是' ? '否' : '是' });
                      // this.setData({
                      //   finishWorkPortMiddle: undefined,
                      //   wharf1: undefined,
                      //   workCon1: undefined,
                      //   reverseCon1: undefined,
                      //   wharf2: undefined,
                      //   workCon2: undefined,
                      //   reverseCon2: undefined,
                      //   startWorkPortMiddle: undefined,
                      // });
                    }}
                    style={{
                      height: 30,
                      width: 60,
                    }}
                />
              </Item>
            </InputGroup>
            {this.state.data.doubleHanger === '是' ? (
                <View>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>进口作业码头:</Label>
                      <Selector
                          value={this.state.data.wharf1}
                          items={this.props.pierList}
                          disabled={!canEdit}
                          ref={(ref) => {
                            this.selector1 = ref;
                          }}
                          onPress={this.onBtnPier1}
                          pickerTitle="请选择进口作业码头"
                          getItemValue={item => item.text}
                          getItemText={item => item.text}
                          onSelected={this.onChangePier1}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="进口作业泊位:"
                        value={this.state.data.berthOne}
                        editable={canEdit}
                        onChangeText={(text) => {
                          this.setData({ berthOne: text });
                        }}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPort}
                          minValue={this.state.data.atbPort}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ startWorkPort: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPortMiddle}
                          minValue={this.state.data.startWorkPort}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ finishWorkPortMiddle: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业箱量:"
                        editable={canEdit}
                        value={this.state.data.workCon1}
                        placeholder="请输入作业箱量"
                        maskType={MaskType.INTEGER}
                        onChangeText={(text) => {
                          this.setData({ workCon1: text });
                        }}
                    />
                    <InputItem
                        label="倒箱量:"
                        editable={canEdit}
                        value={this.state.data.reverseCon1}
                        placeholder="请输入倒箱量"
                        maskType={MaskType.INTEGER}
                        onChangeText={(text) => {
                          this.setData({ reverseCon1: text });
                        }}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>出口作业码头:</Label>
                      <Selector
                          value={this.state.data.wharf2}
                          items={this.props.pierList}
                          disabled={!canEdit}
                          ref={(ref) => {
                            this.selector2 = ref;
                          }}
                          onPress={this.onBtnPier2}
                          pickerTitle="请选择出口作业码头"
                          getItemValue={item => item.text}
                          getItemText={item => item.text}
                          onSelected={this.onChangePier2}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="出口作业泊位:"
                        value={this.state.data.berthTwo}
                        editable={canEdit}
                        onChangeText={(text) => {
                          this.setData({ berthTwo: text });
                        }}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPortMiddle}
                          minValue={this.state.data.finishWorkPortMiddle}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ startWorkPortMiddle: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPort}
                          minValue={this.state.data.startWorkPortMiddle}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ finishWorkPort: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业箱量:"
                        editable={canEdit}
                        value={this.state.data.workCon2}
                        placeholder="请输入作业箱量"
                        maskType={MaskType.INTEGER}
                        onChangeText={(text) => {
                          this.setData({ workCon2: text });
                        }}
                    />
                    <InputItem
                        label="倒箱量:"
                        editable={canEdit}
                        value={this.state.data.reverseCon2}
                        placeholder="请输入倒箱量"
                        maskType={MaskType.INTEGER}
                        onChangeText={(text) => {
                          this.setData({ reverseCon2: text });
                        }}
                    />
                  </InputGroup>
                </View>
            ) : (
                <View>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业码头:</Label>
                      <Selector
                          value={this.state.data.wharfWork}
                          items={this.props.pierList}
                          disabled={!canEdit}
                          ref={(ref) => {
                            this.selector = ref;
                          }}
                          onPress={this.onBtnPier}
                          pickerTitle="请选择码头"
                          getItemValue={item => item.text}
                          getItemText={item => item.text}
                          onSelected={this.onChangePier}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="作业泊位:"
                        value={this.state.data.berthWork}
                        editable={canEdit}
                        onChangeText={(text) => {
                          this.setData({ berthWork: text });
                        }}
                    />
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业开始:</Label>
                      <DatePullSelector
                          value={this.state.data.startWorkPort}
                          minValue={this.state.data.atbPort}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ startWorkPort: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                  <InputGroup style={commonStyles.inputGroup}>
                    <Item style={commonStyles.inputItem}>
                      <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>作业结束:</Label>
                      <DatePullSelector
                          value={this.state.data.finishWorkPort}
                          minValue={this.state.data.startWorkPort}
                          readOnly={!canEdit}
                          onChangeValue={(value) => {
                            this.setData({ finishWorkPort: value });
                          }}
                      />
                    </Item>
                  </InputGroup>
                </View>
            )}
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                  label="作业备注:"
                  value={this.state.data.workMemo}
                  editable={canEdit}
                  onChangeText={(text) => {
                    this.setData({ workMemo: text });
                  }}
              />
            </InputGroup>
            <EventsCell
                title="影响作业进度原因"
                total={this.props.operatingEventsNum}
                unfinished={this.props.unFinishedOperatingEventsNum}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_EMERGENCIES, {
                    title: '影响作业进度原因',
                    id: this.props.workDetail.id,
                    range: EventRange.PORT_OPERATION,
                  });
                }}
            />
            {isOperating ? (
                <View style={styles.buttonContainer}>
                  <Button onPress={this.saveOperating} style={styles.saveButton}>
                    <Text>暂存</Text>
                  </Button>
                  <Button onPress={this.completeOperating} style={styles.completeButton}>
                    <Text>提交</Text>
                  </Button>
                </View>) : this.renderOperatingChange()
            }
          </ExtendCell>
        </View>
    );
  }

  renderUnBerthingChange() {
    if (this.state.changingUnBerthing) {
      return (
          <View style={styles.buttonContainer}>
            <Button
                onPress={() => {
                  this.setState({ changingUnBerthing: false });
                  this.setWordDetail(this.props.workDetail);
                }}
                style={styles.saveButton}
            >
              <Text>取消</Text>
            </Button>
            <Button onPress={this.changeUnBerthing} style={styles.completeButton}>
              <Text>提交变更</Text>
            </Button>
          </View>
      );
    }
    return (
        <View style={styles.buttonContainer}>
          <Button
              onPress={() => {
                this.setState({ changingUnBerthing: true });
              }}
              style={styles.changePlanButton}
          >
            <Text>数据变更</Text>
          </Button>
        </View>
    );
  }

  renderUnBerthing() {
    const canOpen = this.state.data.statePort >= PortWorkState.unBerthing;
    if (!canOpen) {
      return (
          <TouchableCell
              title="离泊阶段"
              isOpen={false}
              style={{
                marginTop: 8,
                backgroundColor: '#ffffff',
              }}
          />
      );
    }
    const isUnBerthing = this.state.data.statePort === PortWorkState.unBerthing;
    const canEdit = this.state.changingUnBerthing || isUnBerthing;
    return (
        <View style={{
          marginTop: 8,
          marginBottom: 8,
          overflow: 'hidden',
          backgroundColor: '#ffffff',
        }}
        >
          {
            !canEdit ? (
                <Svg icon="done_blue" width={200} height={100} style={styles.doneImage} />
            ) : (null)
          }
          <ExtendCell
              title="离泊阶段"
              titleColor="#DC001B"
              isOpen={this.state.openUnBerthing}
              onPress={() => {
                this.setState({ openUnBerthing: !this.state.openUnBerthing });
              }}
              touchable
              style={{ backgroundColor: 'transparent' }}
          >
            <InputGroup style={[commonStyles.inputGroup, { marginBottom: 8 }]}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>实际离港:</Label>
                <DatePullSelector
                    value={this.state.data.atdPort}
                    minValue={this.state.data.finishWorkPort}
                    readOnly={!canEdit}
                    onChangeValue={(value) => {
                      this.setData({ atdPort: value });
                    }}
                />
              </Item>
            </InputGroup>
            <EventsCell
                title="影响离泊原因"
                total={this.props.unBerthingEventsNum}
                unfinished={this.props.unFinishedUnBerthingEventsNum}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_EMERGENCIES, {
                    title: '影响离泊原因',
                    id: this.props.workDetail.id,
                    range: EventRange.PORT_UN_BERTHING,
                  });
                }}
            />
            <TouchableCell
                title="下一港注意事项"
                titleStyle={{
                  marginLeft: 0,
                  color: '#535353',
                }}
                isOpen={false}
                onPress={() => {
                  this.props.navigation.navigate(ROUTE_PORT_ATTENTION, {
                    popId: this.state.popId,
                    portName: this.state.portName,
                    page: 'portWorkDetail',
                  });
                }}
                style={[styles.inLineTouchCell, { borderTopWidth: 0 }]}
                rightStyle={{ marginRight: 0 }}
            />
            {isUnBerthing ? (
                <View style={styles.buttonContainer}>
                  <Button onPress={this.saveUnBerthing} style={styles.saveButton}>
                    <Text>暂存</Text>
                  </Button>
                  <Button onPress={this.completeUnBerthing} style={styles.completeButton}>
                    <Text>提交</Text>
                  </Button>
                </View>) : this.renderUnBerthingChange()
            }
          </ExtendCell>
        </View>
    );
  }

  render() {
    return (
        <Container style={{ backgroundColor: '#E0E0E0' }}>
          <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
            <Form>
              {this.renderBerthing()}
              {this.renderOperating()}
              {this.renderUnBerthing()}
            </Form>
          </Content>
        </Container>
    );
  }
}

PortWorkDetailActual.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '信息录入'),
  tabBarLabel: '实际（开航报）',
});

PortWorkDetailActual.propTypes = {
  navigation: PropTypes.object.isRequired,
  saveBerthingPromise: PropTypes.func.isRequired,
  completeBerthingPromise: PropTypes.func.isRequired,
  saveOperatingPromise: PropTypes.func.isRequired,
  completeOperatingPromise: PropTypes.func.isRequired,
  saveUnBerthingPromise: PropTypes.func.isRequired,
  completeUnBerthingPromise: PropTypes.func.isRequired,
  workDetail: PropTypes.object,
  berthingEventsNum: PropTypes.number,
  unFinishedBerthingEventsNum: PropTypes.number,
  operatingEventsNum: PropTypes.number,
  unFinishedOperatingEventsNum: PropTypes.number,
  unBerthingEventsNum: PropTypes.number,
  unFinishedUnBerthingEventsNum: PropTypes.number,
  getPierListPromise: PropTypes.func.isRequired,
  pierList: PropTypes.array,
};
PortWorkDetailActual.defaultProps = {
  workDetail: {},
  pierList: [],
  berthingEventsNum: 0,
  unFinishedBerthingEventsNum: 0,
  operatingEventsNum: 0,
  unFinishedOperatingEventsNum: 0,
  unBerthingEventsNum: 0,
  unFinishedUnBerthingEventsNum: 0,
};

const mapStateToProps = createStructuredSelector({
  workDetail: makeSelectPortWorkDetail(),
  pierList: makePierList(),
  berthList: makeBerthList(),
  berthingEventsNum: makeBerthingEventsNum(),
  unFinishedBerthingEventsNum: makeUnFinishedBerthingEventsNum(),
  operatingEventsNum: makeOperatingEventsNum(),
  unFinishedOperatingEventsNum: makeUnFinishedOperatingEventsNum(),
  unBerthingEventsNum: makeUnBerthingEventsNum(),
  unFinishedUnBerthingEventsNum: makeUnFinishedUnBerthingEventsNum(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPierListPromise,
      getBerthListPromise,
      saveBerthingPromise,
      completeBerthingPromise,
      saveOperatingPromise,
      completeOperatingPromise,
      saveUnBerthingPromise,
      completeUnBerthingPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortWorkDetailActual);
