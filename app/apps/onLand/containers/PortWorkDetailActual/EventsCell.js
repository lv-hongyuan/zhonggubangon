import React from 'react';
import {
  View,
  TouchableHighlight,
  StyleSheet,
  Text,
  ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import Svg from '../../../../components/Svg';
import myTheme from '../../../../Themes';

const styles = StyleSheet.create({
  titleContainer: {
    // backgroundColor: '#ffffff',
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
  infoContainer: {
    margin: 5,
    padding: 5,
    borderRadius: 5,
  },
  infoText: {
    color: '#FFFFFF',
    fontSize: 12,
  },
});

class EventsCell extends React.PureComponent {
  render() {
    return (
      <TouchableHighlight
        underlayColor="#f0f0f0"
        onPress={this.props.onPress}
        style={[{
          marginTop: 8,
          backgroundColor: '#ffffff',
          borderBottomWidth: myTheme.borderWidth,
          borderColor: myTheme.borderColor,
        }, this.props.style]}
      >
        <View style={[styles.titleContainer]}>
          <Text
            style={[{
              flex: 1,
              color: this.props.titleColor,
            }, this.props.titleStyle]}
          >
            {this.props.title}
          </Text>
          <View style={[styles.infoContainer, { backgroundColor: '#29a8df' }]}>
            <Text style={styles.infoText}>总数:{this.props.total}</Text>
          </View>
          <View style={[styles.infoContainer, { backgroundColor: '#dc001b' }]}>
            <Text style={styles.infoText}>未完成:{this.props.unfinished}</Text>
          </View>
          <Svg icon="onLand_accessory" size={15} />
        </View>
      </TouchableHighlight>
    );
  }
}

EventsCell.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  titleColor: PropTypes.string,
  style: ViewPropTypes.style,
  titleStyle: Text.propTypes.style,
  total: PropTypes.number,
  unfinished: PropTypes.number,
};
EventsCell.defaultProps = {
  style: undefined,
  titleStyle: undefined,
  titleColor: undefined,
  total: undefined,
  unfinished: undefined,
  onPress: undefined,
};

export default EventsCell;
