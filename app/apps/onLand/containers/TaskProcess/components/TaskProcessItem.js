import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated, TouchableOpacity } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
    container: {
        alignContent: 'stretch',
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    col: {
        width: 80,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
    },
    text: {
        color: myTheme.inputColor,
    },
});

class TaskProcessItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {
            shipState, shipName, portName, voyageCode, berth, preWorkNumCurrentWorkNum, currentWorkNum, workEfficiency, loadUnloadNum, hardWorkNum, atb, startWorkTime, finishWorkTime, preFinishTime, atd
        } = this.props.item || {};
        let shipStateName = ''
        let shipStateColor = '#000000'
        let detailTextColor = '#969696'
        if (shipState == '1') {
            shipStateName = '待装'
            shipStateColor = '#000000'
            detailTextColor = '#969696'
        } else if (shipState == '2') {
            shipStateName = '待卸'
            shipStateColor = '#000000'
            detailTextColor = '#969696'
        } else if (shipState == '3') {
            shipStateName = '装船'
            detailTextColor = '#1E90FF'
            shipStateColor = '#FF0000'
        } else if (shipState == '4') {
            shipStateName = '卸船'
            shipStateColor = '#FF0000'
            detailTextColor = '#1E90FF'
        } else if (shipState == '5') {
            shipStateName = '已装'
            shipStateColor = '#0000FF'
            detailTextColor = '#969696'
        } else if (shipState == '6') {
            shipStateName = '已卸'
            shipStateColor = '#0000FF'
            detailTextColor = '#969696'
        }
        return (
            <View style={styles.container}>
                <View style={styles.col}>
                    <Text style={[styles.text, { color: detailTextColor }]}>{'详情'}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text, { color: shipStateColor }]}>{shipStateName}</Text>
                </View>
                <View style={[styles.col, { width: 100 }]}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{shipName}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{portName}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{voyageCode}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{berth}</Text>
                </View>
                <View style={[styles.col,{width:90}]}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{preWorkNumCurrentWorkNum}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{currentWorkNum}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{workEfficiency}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{loadUnloadNum}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{hardWorkNum}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{atb}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{startWorkTime}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{finishWorkTime}</Text>
                </View>
                <View style={[styles.col,{width:110}]}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{preFinishTime}</Text>
                </View>
                <View style={styles.col}>
                    <Text style={[styles.text,{color:shipStateColor}]}>{atd}</Text>
                </View>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.props.headerTranslateX }],
                }}
                >
                    <View style={[styles.container, { flex: 1 }]}>
                        <TouchableOpacity
                            style={[styles.col,{alignItems:'center'}]}
                            activeOpacity={0.7}
                            onPress={() => {
                                let isClick = false
                                if (shipState == '3' || shipState == '4') {
                                    isClick = true
                                }
                                let tempJ = {}
                                tempJ.isClick = isClick
                                tempJ.shipName = shipName
                                tempJ.voyageCode = voyageCode
                                this.props.onSelect(tempJ);
                            }}
                        >
                            <Text style={[styles.text, { color: detailTextColor }]}>{'详情'}</Text>
                        </TouchableOpacity>
                        <View style={styles.col}>
                            <Text style={[styles.text, { color: shipStateColor }]}>{shipStateName}</Text>
                        </View>
                        <View style={[styles.col, { width: 100 }]}>
                            <Text style={[styles.text,{color:shipStateColor}]}>{shipName}</Text>
                        </View>
                    </View>
                </Animated.View>
            </View>
        );
    }
}

TaskProcessItem.propTypes = {
    item: PropTypes.object.isRequired,
    headerTranslateX: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
};

export default TaskProcessItem;
