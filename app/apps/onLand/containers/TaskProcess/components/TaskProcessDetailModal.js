import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, PixelRatio, Platform } from 'react-native';
import Overlay from 'teaset/components/Overlay/Overlay';
import Svg from "../../../../../components/Svg";


const borderWidth = 1 / PixelRatio.getPixelSizeForLayoutSize(1);

const styles = {
  container: {
    backgroundColor: '#fff',
    width: 500,
    borderRadius: 10,
    // justifyContent: 'center',
    // alignItems: 'center',
    overflow: 'hidden',
  },
  image: {
    marginTop: 20,
  },
  title: {
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    color: '#ED1727',
    textAlign: 'left',
    fontSize:15,
  },
  message: {
    marginTop: 10,
    paddingLeft: 10,
    paddingRight: 10,
    // color: '#ED1727',
    // textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 20,
    flexDirection: 'row',
    height: 50,
  },
  button: {
    flex: 1,
    height: 51,
    borderColor: '#868686',
    borderTopWidth: borderWidth,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {},
};

export const TaskProcessDetailModalType = {
  Info: 0,
};

const ModalAlert = [];

function addModal(popView) {
  ModalAlert.push(popView);
}

function closeModal(popView) {
  if (popView) {
    popView.close();
    const index = ModalAlert.indexOf(popView);
    if (index !== -1) {
      ModalAlert.splice(index, 1);
    }
  }
}

function closeAllModal() {
  ModalAlert.forEach((popView) => {
    if (popView) popView.close();
  });
  ModalAlert.splice(0, ModalAlert.length);
}

class TaskProcessDetailModal extends React.Component {
  static propTypes = {
    show: PropTypes.bool,
    title: PropTypes.string,
    message: PropTypes.string,
    showCancel: PropTypes.bool,
    cancelTitle: PropTypes.string,
    cancelAction: PropTypes.func,
    confirmTitle: PropTypes.string,
    confirmAction: PropTypes.func,
  };

  static defaultProps = {
    show: false,
    title: null,
    message: null,
    type: TaskProcessDetailModalType.Info,
    showCancel: false,
    cancelTitle: '取消',
    cancelAction: null,
    confirmTitle: '确认',
    confirmAction: null,
  };

  static show(option: {
    show: PropTypes.bool,
    title: PropTypes.string,
    message: PropTypes.string,
    type: PropTypes.number,
    showCancel: PropTypes.bool,
    cancelTitle: PropTypes.string,
    cancelAction: PropTypes.func,
    confirmTitle: PropTypes.string,
    confirmAction: PropTypes.func
  }) {
    const options = Object.assign({}, TaskProcessDetailModal.defaultProps, option, { show: true });
    let popView: Overlay.PopView;
    const overlayView = (
      <Overlay.PopView
        modal
        autoKeyboardInsets={Platform.OS === 'ios'}
        ref={(ref) => {
          popView = ref;
          addModal(popView);
        }}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <View style={styles.container}>
          {options.title ? <Text style={styles.title}>{options.title}</Text> : null}
          {options.message ? <Text style={styles.message}>{options.message}</Text> : null}
          <View style={styles.buttonContainer}>
            {options.showCancel ?
              <TouchableOpacity
                style={[styles.button, {
                  backgroundColor: 'white',
                  borderRightWidth: borderWidth,
                }]}
                onPress={() => {
                  closeModal(popView);
                  if (options.cancelAction) options.cancelAction();
                }}
              >
                <Text style={[styles.buttonText, { color: '#535353' }]}>{options.cancelTitle}</Text>
              </TouchableOpacity> : null
            }
            <TouchableOpacity
              style={[styles.button, { backgroundColor: '#ED1727' }]}
              onPress={() => {
                closeModal(popView);
                if (options.confirmAction) options.confirmAction();
              }}
            >
              <Text style={[styles.buttonText, { color: 'white' }]}>{options.confirmTitle}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Overlay.PopView>
    );
    return Overlay.show(overlayView);
  }

  static closeAll() {
    closeAllModal();
  }

  componentDidMount() {
    if (this.props.show) {
      requestAnimationFrame(() => {
        // this.overlayPopView = this.show();
      });
    }
  }

  overlayPopView: number;

  render() {
    return null;
  }
}

export default TaskProcessDetailModal;
