import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_TASK_PROCESS } from './constants';

export const getTaskProcessRoutine = createRoutine(GET_TASK_PROCESS);
export const getTaskProcessPromise = promisifyRoutine(getTaskProcessRoutine);