import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getTaskProcessRoutine } from './actions';

function* getDirectRate(action) {
  console.log(action);
  try {
    yield put(getTaskProcessRoutine.request());
    const response = yield call(request, ApiFactory.taskProcessList(action.payload));
    console.log('作业进度:',response);
    yield put(getTaskProcessRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTaskProcessRoutine.failure(e));
  } finally {
    yield put(getTaskProcessRoutine.fulfill());
  }
}

export function* getTaskProcessSaga() {
  yield takeLatest(getTaskProcessRoutine.TRIGGER, getDirectRate);
}

// All sagas to be loaded
export default [getTaskProcessSaga];
