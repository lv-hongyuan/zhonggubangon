import { createSelector } from 'reselect/es';

const selectTaskProcessDomain = () => state => state.taskProcess;

const makeList = () => createSelector(selectTaskProcessDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectTaskProcessDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
