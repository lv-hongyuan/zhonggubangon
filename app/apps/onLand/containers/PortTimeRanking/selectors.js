import { createSelector } from 'reselect/es';

const selectPortTimeRankingDomain = () => state => state.portTimeRanking;

const makeList = () => createSelector(selectPortTimeRankingDomain(), subState => subState.list);

const makeRefreshState = () => createSelector(selectPortTimeRankingDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export {
  makeList, makeRefreshState,
};
