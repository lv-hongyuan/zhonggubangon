import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: '25%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class PortTimeRankingItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      pq, port, inPortTime, score,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{port}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{inPortTime}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{score}</Text>
        </View>
      </View>
    );
  }
}

PortTimeRankingItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};

export default PortTimeRankingItem;
