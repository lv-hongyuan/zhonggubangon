//! *********** 此页面已弃用 ***************
//! *********** 在港停时考核 ***************
//! *********** 此页面已弃用 ***************
import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, TouchableHighlight, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Item,
  Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { SORT_ORDER } from '../../../../common/Constant';
import memoizeSort from '../../../../utils/memoizeSort';
import {
  getPortTimeRankingPromiseCreator,
} from './actions';
import {
  makeList, makeRefreshState,
} from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import PortTimeRankingItem from './components/PortTimeRankingItem';
import RefreshListView from '../../../../components/RefreshListView';
import Svg from '../../../../components/Svg';
import commonStyles from '../../../../common/commonStyles';
import DatePullSelector from '../../../../components/DatePullSelector';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';

const styles = {
  container: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    backgroundColor: 'white',
    width: '25%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: '25%',
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

@screenHOC
class PortTimeRanking extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      sortType: 'inPortTime',
      sortDirection: SORT_ORDER.ASC,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = () => {

  };

  supportedOrientations = Orientations.ALL;

  loadList() {
    this.props.getPortTimeRankingPromiseCreator({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    })
      .catch(() => {
      });
  }

  changeSort(sortType) {
    let sortDirection;
    if (sortType === this.state.sortType) {
      sortDirection = this.state.sortDirection === SORT_ORDER.ASC ? SORT_ORDER.DESC : SORT_ORDER.ASC;
    } else {
      sortDirection = SORT_ORDER.ASC;
    }
    this.setState({
      sortType,
      sortDirection,
    });
  }

  renderItem = ({ item }) => (
    <PortTimeRankingItem item={item} onSelect={this.onSelect} />
  );

  renderFilter() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{
          width: '100%',
          flexDirection: 'row',
        }}
        >
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5,
            marginBottom: 0,
            flex: 1,
            width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>起始时间:</Label>
            <DatePullSelector
              value={this.state.startTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ startTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5,
            marginBottom: 0,
            flex: 1,
            width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>截止时间:</Label>
            <DatePullSelector
              value={this.state.endTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ endTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
        </SafeAreaView>
      </View>
    );
  }

  render() {
    const sortedList = memoizeSort(this.props.list, this.state.sortType, this.state.sortDirection);
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <ScrollView
          contentContainerStyle={{ minWidth: '100%' }}
          horizontal
          contentInsetAdjustmentBehavior="scrollableAxes"
        >
          <View
            style={{
              width: '100%',
              flex: 1,
            }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <View style={styles.container}>
              <View style={styles.col}>
                <Text style={styles.text}>片区</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
              <TouchableHighlight
                style={styles.touchCol}
                onPress={() => {
                  this.changeSort('inPortTime');
                }}
              >
                <View style={styles.touchContent}>
                  <Text style={styles.text}>在港时间</Text>
                  {this.state.sortType === 'inPortTime' && (
                    <Svg
                      icon={this.state.sortDirection === SORT_ORDER.ASC ? 'icon_up' : 'icon_down'}
                      color="red"
                      size={15}
                    />
                  )}
                </View>
              </TouchableHighlight>
              <TouchableHighlight
                style={styles.touchCol}
                onPress={() => {
                  this.changeSort('score');
                }}
              >
                <View style={styles.touchContent}>
                  <Text style={styles.text}>得分</Text>
                  {this.state.sortType === 'score' && (
                    <Svg
                      icon={this.state.sortDirection === SORT_ORDER.ASC ? 'icon_up' : 'icon_down'}
                      color="red"
                      size={15}
                    />
                  )}
                </View>
              </TouchableHighlight>
            </View>
            <RefreshListView
              ref={(ref) => { this.listView = ref; }}
              headerTranslateX={this.translateX}
              containerWidth={this.state.containerWidth}
              data={sortedList}
              style={{ flex: 1 }}
              keyExtractor={item => `${item.id}`}
              renderItem={this.renderItem}
              refreshState={this.props.refreshState}
              onHeaderRefresh={this.onHeaderRefresh}
              // onFooterRefresh={this.onFooterRefresh}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

PortTimeRanking.navigationOptions = () => ({
  title: '在港停时考核',
});

PortTimeRanking.propTypes = {
  getPortTimeRankingPromiseCreator: PropTypes.func.isRequired,
  list: PropTypes.number.isRequired,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPortTimeRankingPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortTimeRanking);
