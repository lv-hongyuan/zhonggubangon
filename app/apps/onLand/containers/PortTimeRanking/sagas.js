import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getPortTimeRankingRoutine } from './actions';

function* getPortTimeRanking(action) {
  console.log(action);
  try {
    yield put(getPortTimeRankingRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.inPortTimeExamDTOList(startTime, endTime));
    console.log(response);
    yield put(getPortTimeRankingRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPortTimeRankingRoutine.failure(e));
  } finally {
    yield put(getPortTimeRankingRoutine.fulfill());
  }
}

export function* getPortTimeRankingSaga() {
  yield takeLatest(getPortTimeRankingRoutine.TRIGGER, getPortTimeRanking);
}

// All sagas to be loaded
export default [getPortTimeRankingSaga];
