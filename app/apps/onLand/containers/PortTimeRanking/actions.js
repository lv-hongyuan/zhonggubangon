import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_PORT_TIME_RANKING,
} from './constants';

export const getPortTimeRankingRoutine = createRoutine(GET_PORT_TIME_RANKING);
export const getPortTimeRankingPromiseCreator = promisifyRoutine(getPortTimeRankingRoutine);
