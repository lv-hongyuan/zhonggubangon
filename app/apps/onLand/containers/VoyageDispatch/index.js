
//!  航次调度列表页

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Keyboard, SafeAreaView, StyleSheet, TouchableOpacity,
} from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash';
import { makeSelectRefreshState, makePopList } from './selectors';
import { ROUTE_VOYAGE_EDIT, ROUTE_VOYAGE_LINE_EDIT } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import VoyageItem from './components/VoyageItem';
import { getVoyageListPromise, getToPlanPopPromise } from './actions';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';
import Svg from '../../../../components/Svg';
import { LineEditType, LineEditTypeKey } from '../VoyageLineEdit/constants';
import Selector from "../../../../components/Selector";
const getRegionArr = ['负责船舶', '全部船舶']
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

@screenHOC
class VoyageDispatch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipName: '',
      voyCode: '',
      resShips: '1',       //负责港口默认值
      toBePlan: '',
      inputShipName: '',
      inputVoyCode: '',
      inputResShips: '', //负责港口默认value
      inputToBePlan: '',
      displayMode: true,
    };
  }

  componentDidMount() {
    this.onFooterRefresh();
    this.listView.beginRefresh();
    const { navigation } = this.props;
    navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  onChangeVoyage = (item) => {
    const { navigation, getToPlanPop } = this.props;
    const {
      voyid, shipName, toBePlan, popid, voyCode,
    } = item;
    if (toBePlan === 1 || _.isEmpty(voyCode)) {
      getToPlanPop({ popid, voyid })
        .then((popList) => {
          navigation.navigate(ROUTE_VOYAGE_LINE_EDIT, {
            voyid,
            shipName,
            popid,
            popList,
            [LineEditTypeKey]: LineEditType.PLAN_NEW,
            onBack: () => {
              this.loadList();
            },
          });
        })
        .catch(() => { });
    } else {
      navigation.navigate(ROUTE_VOYAGE_EDIT, { voyid, shipName });
    }
  };

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(true);
  };

  setSearchValue(callBack) {
    const {
      inputShipName, inputVoyCode, inputResShips, inputToBePlan,
    } = this.state;
    this.setState(
      {
        shipName: inputShipName,
        voyCode: inputVoyCode,
        resShips: inputResShips == '全部船舶' ? '1' : '10',
        toBePlan: inputToBePlan,
      },
      callBack,
    );
  }

  setInputValue(callBack) {
    const {
      shipName, voyCode, resShips, toBePlan,
    } = this.state;
    this.setState(
      {
        inputShipName: shipName,
        inputVoyCode: voyCode,
        inputResShips: resShips == '1' ? '全部船舶' : '负责船舶',
        inputToBePlan: toBePlan,
      },
      callBack,
    );
  }

  didFocus = () => {
    // 每次进页面刷新列表
    this.onFooterRefresh();
  };

  changeDisplayMode = (displayMode) => {
    const { navigation } = this.props;
    navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  loadList() {
    const {
      shipName, voyCode, resShips
    } = this.state;
    const { getVoyageList } = this.props;
    getVoyageList({
      shipName,
      voyCode,
      resShips
    })
      .then(() => { })
      .catch(() => { });
  }

  renderItem = ({ item }) => <VoyageItem item={item} onPress={this.onChangeVoyage} />;

  renderFilter() {
    const {
      shipName, voyCode, inputShipName, inputVoyCode, inputResShips
    } = this.state;
    const { displayMode } = this.state;
    return (
      <View style={styles.container}>
        {displayMode ? (
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(shipName) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>船名:</Label>
                    <Text style={commonStyles.text}>{shipName}</Text>
                  </View>
                )}
                {!_.isEmpty(voyCode) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>航次:</Label>
                    <Text style={commonStyles.text}>{voyCode}</Text>
                  </View>
                )}
                {!_.isEmpty(inputResShips) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>负责船舶:</Label>
                    <Text style={commonStyles.text}>{inputResShips}</Text>
                  </View>
                )}
                {_.isEmpty(shipName) && _.isEmpty(voyCode) && _.isEmpty(inputResShips) &&
                  (
                    <Item
                      style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                      onPress={() => {
                        this.setInputValue();
                        this.changeDisplayMode(false);
                      }}
                    >
                      <Text style={commonStyles.text}>点击输入要搜索的船名/航次/负责港口</Text>
                    </Item>
                  )}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
            <SafeAreaView style={{ width: '100%' }}>
              <View style={{ padding: 10 }}>
                <InputGroup style={commonStyles.inputGroup}>
                  <InputItem
                    autoFocus
                    label="船名:"
                    returnKeyType="search"
                    value={inputShipName}
                    clearButtonMode="while-editing"
                    onChangeText={(text) => {
                      this.setState({ inputShipName: text });
                    }}
                    onSubmitEditing={() => {
                      Keyboard.dismiss();
                      this.setSearchValue();
                      this.changeDisplayMode(true);
                      this.listView.beginRefresh();
                    }}
                    onFocus={() => {
                      this.changeDisplayMode(false);
                    }}
                  />
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <InputItem
                    label="航次:"
                    returnKeyType="search"
                    value={inputVoyCode}
                    clearButtonMode="while-editing"
                    onChangeText={(text) => {
                      this.setState({ inputVoyCode: text });
                    }}
                    onSubmitEditing={() => {
                      Keyboard.dismiss();
                      this.setSearchValue();
                      this.changeDisplayMode(true);
                      this.listView.beginRefresh();
                    }}
                    onFocus={() => {
                      this.changeDisplayMode(false);
                    }}
                  />
                </InputGroup>
                <InputGroup style={commonStyles.inputGroup}>
                  <Item style={commonStyles.inputItem}>
                    <Label style={commonStyles.inputLabel}>负责船舶:</Label>
                    <Selector
                      value={inputResShips}
                      items={getRegionArr}
                      getItemValue={item => item}
                      getItemText={item => item}
                      onSelected={(item) => {
                        this.setState({ inputResShips: item });
                      }}
                      pickerType={'modal'}
                    />
                  </Item>
                </InputGroup>
              </View>
              <Button
                onPress={() => {
                  Keyboard.dismiss();
                  this.setSearchValue();
                  this.changeDisplayMode(true);
                  this.setState(
                    {
                      displayMode: true,
                    },
                    () => {
                      this.listView.beginRefresh();
                    },
                  );
                }}
                block
                style={{
                  height: 45,
                  margin: 10,
                  justifyContent: 'center',
                  backgroundColor: '#DC001B',
                }}
              >
                <Text style={{ color: '#ffffff' }}>搜索</Text>
              </Button>
            </SafeAreaView>
          )}
      </View>
    );
  }

  render() {
    const { list, refreshState } = this.props;
    const { displayMode } = this.state;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <SafeAreaView style={{ flex: 1 }}>
          <RefreshListView
            ref={(ref) => {
              this.listView = ref;
            }}
            style={{ width: '100%' }}
            data={list || []}
            keyExtractor={item => `${item.indexKey}`}
            renderItem={this.renderItem}
            refreshState={refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            // onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => <View style={{ height: 5 }} />}
            ItemSeparatorComponent={() => <View style={{ height: 5 }} />}
          />
          {!displayMode && (
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                backgroundColor: '#ffffff',
              }}
            />
          )}
        </SafeAreaView>
      </Container>
    );
  }
}

VoyageDispatch.navigationOptions = ({ navigation }) => ({
  title: '航次调度',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
          <HeaderButtons.Item
            title="取消"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={() => {
              const exitSearch = navigation.getParam('exitSearch');
              exitSearch();
            }}
          />
        )}
    </HeaderButtons>
  ),
});

VoyageDispatch.defaultProps = {
  list: [],
};

VoyageDispatch.propTypes = {
  navigation: PropTypes.object.isRequired,
  getVoyageList: PropTypes.func.isRequired,
  getToPlanPop: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  list: makePopList(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators(
      {
        getVoyageList: getVoyageListPromise,
        getToPlanPop: getToPlanPopPromise,
      },
      dispatch,
    ),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VoyageDispatch);
