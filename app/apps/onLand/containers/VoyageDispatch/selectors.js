import { createSelector } from 'reselect/es';

const selectVoyageDispatchDomain = () => state => state.voyageDispatch;

const makePopList = () => createSelector(selectVoyageDispatchDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectVoyageDispatchDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

export { makePopList, makeSelectRefreshState };
