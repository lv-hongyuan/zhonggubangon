/*
 *
 * VoyageDispatch constants
 *
 */
export const GET_VOYAGE_LIST = 'onLand/PortWork/VoyageDispatch';
export const GET_TO_PLAN_POP = 'onLand/PortWork/GET_TO_PLAN_POP';
