import shortid from 'shortid';
import { getVoyageListRoutine, getToPlanPopRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态（带分页）
    case getVoyageListRoutine.TRIGGER: {
      return {
        ...state,
        loading: true,
        refreshState: RefreshState.HeaderRefreshing,
      };
    }
    case getVoyageListRoutine.SUCCESS: {
      const { list } = action.payload;
      return {
        ...state,
        list: list.map(item => ({
          ...item,
          indexKey: shortid.generate(),
        })),
        refreshState: RefreshState.NoMoreData,
      };
    }
    case getVoyageListRoutine.FAILURE: {
      const { error } = action.payload;
      return {
        ...state,
        list: [],
        error,
        refreshState: RefreshState.Failure,
      };
    }
    case getVoyageListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 获取待计划航线pop
    case getToPlanPopRoutine.TRIGGER: {
      return { ...state, loading: true };
    }
    case getToPlanPopRoutine.SUCCESS: {
      return { ...state };
    }
    case getToPlanPopRoutine.FAILURE: {
      return { ...state };
    }
    case getToPlanPopRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
