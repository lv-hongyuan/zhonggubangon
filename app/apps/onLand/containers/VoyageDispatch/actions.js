import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_VOYAGE_LIST, GET_TO_PLAN_POP } from './constants';

export const getVoyageListRoutine = createRoutine(GET_VOYAGE_LIST);
export const getVoyageListPromise = promisifyRoutine(getVoyageListRoutine);

export const getToPlanPopRoutine = createRoutine(GET_TO_PLAN_POP);
export const getToPlanPopPromise = promisifyRoutine(getToPlanPopRoutine);
