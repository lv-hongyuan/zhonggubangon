import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, PixelRatio, TouchableHighlight } from 'react-native';
import {
  Grid, Col, Text, View,
} from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  info: {
    backgroundColor: 'white',
  },
  toBePlan: {
    fontSize: 14,
    color: '#23527c',
    margin: 2,
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  cornerContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 5,
    borderBottomLeftRadius: 5,
    borderLeftWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    backgroundColor: '#ccc',
  },
  infoContainer: {
    flexDirection: 'row',
    borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
  },
});

class VoyageItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { item, onPress } = this.props;
    const {
      linename,
      voyCode,
      shipName,
      toBePlan,
    } = item || {};
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => {
          onPress(item);
        }}
      >
        <View style={styles.info}>
          <View
            style={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 10,
              paddingBottom: 5,
            }}
          >
            <Grid>
              <Col>
                <Text style={styles.label}>
                  船名：
                  {shipName || '-----------'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.label}>
                  航次：
                  {toBePlan === 1 ? (
                    '待计划'
                  ) : (
                    voyCode || '-----------'
                  )}
                </Text>
              </Col>
            </Grid>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.label}>
                航线：
                {linename}
              </Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

VoyageItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default VoyageItem;
