import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getVoyageListRoutine, getToPlanPopRoutine } from './actions';

function* getVoyageList(action) {
  console.log(action);
  const { shipName, voyCode ,resShips} = action.payload || {};
  try {
    yield put(getVoyageListRoutine.request());
    const response = yield call(request, ApiFactory.getVoyageList({ shipName, voyCode ,resShips}));
    console.log(response);
    yield put(getVoyageListRoutine.success({ list: response }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getVoyageListRoutine.failure({ error }));
  } finally {
    yield put(getVoyageListRoutine.fulfill());
  }
}

export function* getVoyageListSaga() {
  yield takeLatest(getVoyageListRoutine.TRIGGER, getVoyageList);
}

function* getToPlanPop(action) {
  console.log(action);
  const { popid, voyid } = action.payload || {};
  try {
    yield put(getToPlanPopRoutine.request());
    const response = yield call(request, ApiFactory.findAllPopsByVoy({ popid, voyid }));
    console.log(response);
    const { popList } = response;
    yield put(getToPlanPopRoutine.success(popList));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getToPlanPopRoutine.failure({ error }));
  } finally {
    yield put(getToPlanPopRoutine.fulfill());
  }
}

export function* getToPlanPopSaga() {
  yield takeLatest(getToPlanPopRoutine.TRIGGER, getToPlanPop);
}

// All sagas to be loaded
export default [getVoyageListSaga, getToPlanPopSaga];
