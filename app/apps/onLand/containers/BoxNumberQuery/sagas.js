import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getBoxNumberListRoutine } from './actions';

function* getBoxNumberList(action) {
  console.log(action);
  try {
    yield put(getBoxNumberListRoutine.request());
    const { zwcm, hc, xh } = action.payload;
    const response = yield call(request, ApiFactory.getQueryBoxNumber({ zwcm, hc, xh }));
    console.log('getQueryBoxNumber', response);
    yield put(getBoxNumberListRoutine.success([response]));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBoxNumberListRoutine.failure(e));
  } finally {
    yield put(getBoxNumberListRoutine.fulfill());
  }
}

export function* getBoxNumberListSaga() {
  yield takeLatest(getBoxNumberListRoutine.TRIGGER, getBoxNumberList);
}

// All sagas to be loaded
export default [getBoxNumberListSaga];
