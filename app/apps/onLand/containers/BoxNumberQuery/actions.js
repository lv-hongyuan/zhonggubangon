import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_BOX_NUMBER_LIST } from './constants';

export const getBoxNumberListRoutine = createRoutine(GET_BOX_NUMBER_LIST);
export const getBoxNumberListPromise = promisifyRoutine(getBoxNumberListRoutine);
