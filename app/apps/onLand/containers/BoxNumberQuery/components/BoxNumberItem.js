import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
  },
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {},
  text: {
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
});

class BoxNumberItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) this.props.onSelect(this.props.item);
  };

  renderTitle(title) {
    const titleArray = title.split('');
    return (
      <View style={{
        flexDirection: 'row', width: 60, alignItems: 'center', justifyContent: 'space-between',
      }}
      >
        {
          titleArray.map((char, index) => {
            const str = index === titleArray.length - 1 ? `${char}:` : char;
            return <Text style={styles.label} key={`${str}`}>{str}</Text>;
          })
        }
      </View>
    );
  }

  render() {
    const item = this.props.item || {};
    return (
      <TouchableHighlight style={{ marginTop: 8 }} onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={styles.row}>
            {this.renderTitle('箱号')}
            <Text style={styles.text}>{item.xh || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('货物品名')}
            <Text style={styles.text}>{item.hm || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('重量')}
            <Text style={styles.text}>{item.zl || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('始发港')}
            <Text style={styles.text}>{item.sfg || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('目的港')}
            <Text style={styles.text}>{item.mdg || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('航次')}
            <Text style={styles.text}>{item.hc || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('中文船名')}
            <Text style={styles.text}>{item.zwcm || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('发货人')}
            <Text style={styles.text}>{item.fhr || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            {this.renderTitle('收货人')}
            <Text style={styles.text}>{item.shr || '-----------'}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

BoxNumberItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};
BoxNumberItem.defaultProps = {
  onSelect: undefined,
};
export default BoxNumberItem;
