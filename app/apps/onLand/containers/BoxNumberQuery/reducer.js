import { RefreshState } from '../../../../components/RefreshListView';
import { getBoxNumberListRoutine } from './actions';

const defaultState = {
  list: [],
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取箱号列表
    case getBoxNumberListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        loadingError: undefined,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getBoxNumberListRoutine.SUCCESS:
      // 列表处理
      const oldList = state.list;
      const receiveList = action.payload || [];
      const list = receiveList;

      // 刷新状态
      let refreshState;
      if (list.length === 0) {
        refreshState = RefreshState.EmptyData;
      } else if (oldList === list) {
        refreshState = RefreshState.NoMoreData;
      } else {
        refreshState = RefreshState.NoMoreData;
      }
      return { ...state, list, refreshState };
    case getBoxNumberListRoutine.FAILURE:
      return {
        ...state, list: [], refreshState: RefreshState.Failure, loadingError: action.payload,
      };
    case getBoxNumberListRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
