import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, SafeAreaView, Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, InputGroup, Button, Form,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getBoxNumberListPromise } from './actions';
import BoxNumberItem from './components/BoxNumberItem';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';

import {
  makeQueryLoading,
  makeSelectEmergencies,
  makeSelectRefreshState,
} from './selectors';
import Loading from '../../../../components/Loading';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';

const styles = StyleSheet.create({
  actionsContainer: {
    flex: 1,
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionButton: {
    width: 80,
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  actionButtonText: {
    textAlign: 'center',
    color: 'white',
  },
});

@screenHOC
class BoxNumberQuery extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      zwcm: undefined,
      hc: undefined,
      xh: undefined,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    console.log(item);
  };

  loadList(loadMore) {
    this.props.getBoxNumberListPromise({
      zwcm: this.state.zwcm,
      hc: this.state.hc,
      xh: this.state.xh,
      loadMore,
    })
      .then(() => {})
      .catch(() => {});
  }

  renderItem = ({ item }) => (
    <BoxNumberItem item={item} />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <View style={styles.container}>
          <SafeAreaView style={{ width: '100%', flexDirection: 'row', backgroundColor: '#ffffff' }}>
            <Form style={{
              paddingLeft: 20, paddingRight: 20, paddingBottom: 8, flex: 1,
            }}
            >
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="中文船名:"
                  value={this.state.zwcm}
                  placeholder="请输入中文船名"
                  onChangeText={(text) => {
                    this.setState({ zwcm: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="航       次:"
                  value={this.state.hc}
                  placeholder="请输入航次"
                  onChangeText={(text) => {
                    this.setState({ hc: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="箱       号:"
                  value={this.state.xh}
                  placeholder="请输入箱号"
                  onChangeText={(text) => {
                    this.setState({ xh: text });
                  }}
                />
              </InputGroup>
              <Button
                onPress={() => {
                  Keyboard.dismiss();
                  this.listView.beginRefresh();
                }}
                style={{
                  height: 45, width: '100%', justifyContent: 'center', backgroundColor: '#DC001B',
                }}
              >
                <Text style={{ color: '#ffffff' }}>搜索</Text>
              </Button>
            </Form>
          </SafeAreaView>
        </View>
        <RefreshListView
          ref={(ref) => { this.listView = ref; }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
        />
        {this.props.queryLoading ? <Loading /> : null}
      </Container>
    );
  }
}

BoxNumberQuery.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '箱号查询'),
});

BoxNumberQuery.propTypes = {
  getBoxNumberListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  queryLoading: PropTypes.bool,
  refreshState: PropTypes.number.isRequired,
};
BoxNumberQuery.defaultProps = {
  list: [],
  queryLoading: false,
};

const mapStateToProps = createStructuredSelector({
  list: makeSelectEmergencies(),
  queryLoading: makeQueryLoading(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getBoxNumberListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BoxNumberQuery);
