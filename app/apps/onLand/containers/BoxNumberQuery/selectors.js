import { createSelector } from 'reselect/es';

const selectBoxNumberQueryDomain = () => state => state.boxNumberQuery;

const makeSelectEmergencies = () => createSelector(selectBoxNumberQueryDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectBoxNumberQueryDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeQueryLoading = () => createSelector(selectBoxNumberQueryDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeQueryLoading };
