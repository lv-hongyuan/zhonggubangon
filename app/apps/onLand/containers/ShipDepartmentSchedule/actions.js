import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_SHIP_DEPARTMENT_SCHEDULE,
} from './constants';

export const getShipDepartmentScheduleRoutine = createRoutine(GET_SHIP_DEPARTMENT_SCHEDULE);
export const getShipDepartmentSchedulePromise = promisifyRoutine(getShipDepartmentScheduleRoutine);
