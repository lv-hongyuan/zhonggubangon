import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
  getShipDepartmentScheduleRoutine,
} from './actions';

function* getShipDepartmentSchedule(action) {
  console.log(action);
  try {
    yield put(getShipDepartmentScheduleRoutine.request());
    const { type, shipName } = (action.payload || {});
    const response = yield call(request, ApiFactory.getShipDepartmentSchedule(type || undefined, shipName));
    console.log(response);
    yield put(getShipDepartmentScheduleRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipDepartmentScheduleRoutine.failure(e));
  } finally {
    yield put(getShipDepartmentScheduleRoutine.fulfill());
  }
}

export function* getShipDepartmentScheduleSaga() {
  yield takeLatest(getShipDepartmentScheduleRoutine.TRIGGER, getShipDepartmentSchedule);
}

// All sagas to be loaded
export default [
  getShipDepartmentScheduleSaga,
];
