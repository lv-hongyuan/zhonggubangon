import {
  getShipDepartmentScheduleRoutine,
} from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipDepartmentScheduleRoutine.TRIGGER:
      return {
        ...state,
        loading: true,
        refreshState: RefreshState.HeaderRefreshing,
      };
    case getShipDepartmentScheduleRoutine.SUCCESS: {
      const newList = fixIdList(action.payload);
      return {
        ...state,
        list: newList,
        refreshState: newList.length === 0 ? RefreshState.EmptyData : RefreshState.NoMoreData,
      };
    }
    case getShipDepartmentScheduleRoutine.FAILURE:
      return {
        ...state,
        list: [],
        error: action.payload,
        refreshState: RefreshState.Failure,
      };
    case getShipDepartmentScheduleRoutine.FULFILL:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
