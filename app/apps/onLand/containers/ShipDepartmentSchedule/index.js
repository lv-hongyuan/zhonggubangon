import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, TouchableOpacity, Keyboard, StatusBar} from 'react-native';
import Orientation from 'react-native-orientation';
import { NavigationActions } from 'react-navigation';
import HeaderButtons from 'react-navigation-header-buttons';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
  Item,
  Label, InputGroup, Button,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import InputItem from '../../../../components/InputItem';
import Svg from '../../../../components/Svg';
import {
  getShipDepartmentSchedulePromise,
} from './actions';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from '../../../../common/commonStyles';
import ShipDepartmentScheduleItem from './components/ShipDepartmentScheduleItem';
import Selector from '../../../../components/Selector';

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
};

const SailType = {
  SAIL: {
    value: 1,
    name: '航行',
  },
  PORT: {
    value: 2,
    name: '在港',
  },
  ALL: {
    value: null,
    name: '全部',
  },
};

@screenHOC
class ShipDepartmentSchedule extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      shipName: '',
      sailType: SailType.ALL.value,
      inputShipName: '',
      inputSailType: SailType.ALL,
      containerOffSet: new Animated.Value(0),
      displayMode: true,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  setSearchValue(callBack) {
    this.setState({
      shipName: this.state.inputShipName,
      sailType: this.state.inputSailType,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputShipName: this.state.shipName,
      inputSailType: this.state.sailType,
    }, callBack);
  }

  changeDisplayMode = (displayMode) => {
    this.props.navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  willFocus = () => {
    Orientation.unlockAllOrientations();
  };

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    this.props.getShipDepartmentSchedule({
      shipName: this.state.shipName,
      type: this.state.sailType,
    })
      .catch(() => {
      });
  }

  renderItem = ({ item }) => (
    <ShipDepartmentScheduleItem
      item={item}
      headerTranslateX={this.translateX}
    />
  );


  renderFilter() {
    return (
      <View style={styles.container}>
        {this.state.displayMode ? (
          <SafeAreaView
            style={{
              width: '100%',
              flexDirection: 'row',
            }}
          >
            <View
              style={{
                padding: 10,
                flex: 1,
                borderBottomWidth: 1,
                borderColor: myTheme.borderColor,
              }}
            >
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(this.state.shipName) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>船名:</Label>
                    <Text style={commonStyles.text}>{this.state.shipName}</Text>
                  </View>
                )}
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>航行状态:</Label>
                  <Text style={commonStyles.text}>
                    {Object.values(SailType)
                      .find(({ value }) => value === this.state.sailType).name}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  autoFocus
                  label="船名:"
                  returnKeyType="search"
                  value={this.state.inputShipName}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputShipName: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>航行状态:</Label>
                  <Selector
                    value={this.state.inputSailType}
                    items={Object.values(SailType)}
                    getItemValue={item => item.value}
                    getItemText={item => item.name}
                    pickerTitle="航线类型"
                    onSelected={(item) => {
                      this.setState({ inputSailType: item.value });
                    }}
                  />
                </Item>
              </InputGroup>
            </View>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.setSearchValue();
                this.changeDisplayMode(true);
                this.setState({
                  displayMode: true,
                }, () => {
                  this.listView.beginRefresh();
                });
              }}
              block
              style={{
                height: 45,
                margin: 10,
                justifyContent: 'center',
                backgroundColor: '#DC001B',
              }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        )}
      </View>
    );
  }

  renderHeader() {
    return (
      <View style={styles.container}>
        <View style={[styles.col, { width: 120 }]}>
          <Text style={styles.text}>船名/航次</Text>
        </View>
        {/*<View style={[styles.col]}>*/}
        {/*  <Text style={styles.text}>航次</Text>*/}
        {/*</View>*/}
        <View style={[styles.col]}>
          <Text style={styles.text}>港口</Text>
        </View>
        <View style={[styles.col]}>
          <Text style={styles.text}>下一港</Text>
        </View>
        <View style={[styles.col]}>
          <Text style={styles.text}>状态</Text>
        </View>
        <View style={[styles.col, { width: 180 }]}>
          <Text style={styles.text}>时间</Text>
        </View>
        <View style={[styles.col, { flex:1 }]}>
          <Text style={styles.text}>航线</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { width: 120 }]}>
              <Text style={styles.text}>船名/航次</Text>
            </View>
            {/*<View style={[styles.col]}>*/}
            {/*  <Text style={styles.text}>航次</Text>*/}
            {/*</View>*/}
          </View>
        </Animated.View>
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <StatusBar backgroundColor="#DC001B" barStyle="light-content" hidden={true} translucent={true}/>
        {this.renderFilter()}
        <View style={{
          flex: 1,
          width: '100%',
        }}
        >
          <SafeAreaView style={{
            backgroundColor: '#E0E0E0',
            flex: 1,
          }}
          >
            <View
              style={{
                width: '100%',
                flex: 1,
              }}
              onLayout={(event) => {
                const { width } = event.nativeEvent.layout;
                this.setState({ containerWidth: width });
              }}
            >
              <Animated.ScrollView
                onScroll={Animated.event([{
                  nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
                }], { useNativeDriver: true })}
                scrollEventThrottle={1}
                horizontal
              >
                <View style={{
                  flex: 1,
                  minWidth: '100%',
                }}
                >
                  {this.renderHeader()}
                  <RefreshListView
                    ref={(ref) => { this.listView = ref; }}
                    headerTranslateX={this.translateX}
                    containerWidth={this.state.containerWidth}
                    data={this.props.list || []}
                    style={{ flex: 1 }}
                    keyExtractor={item => `${item.id}`}
                    renderItem={this.renderItem}
                    refreshState={this.props.refreshState}
                    onHeaderRefresh={this.onHeaderRefresh}
                    onFooterRefresh={this.onFooterRefresh}
                  />
                </View>
              </Animated.ScrollView>
            </View>
          </SafeAreaView>
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              backgroundColor: '#ffffff',
            }}
            />
          )}
        </View>
      </Container>
    );
  }
}

ShipDepartmentSchedule.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '船管部船期表'),
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{
            fontSize: 14,
            color: '#ffffff',
          }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
});

ShipDepartmentSchedule.propTypes = {
  navigation: PropTypes.object.isRequired,
  // list: PropTypes.number.isRequired,
  refreshState: PropTypes.number.isRequired,
  getShipDepartmentSchedule: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipDepartmentSchedule: getShipDepartmentSchedulePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipDepartmentSchedule);
