import { createSelector } from 'reselect/es';

const selectShipDepartmentScheduleDomain = () => state => state.shipDepartmentSchedule;

const makeList = () => createSelector(selectShipDepartmentScheduleDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectShipDepartmentScheduleDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
