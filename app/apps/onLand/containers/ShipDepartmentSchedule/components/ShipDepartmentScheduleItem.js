import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class ShipDepartmentScheduleItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      shipName, voyageCode, portName, nextPortName, stateMemo, time, line,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={[styles.col, { width: 120}]}>
          <Text style={styles.text}>{'占用view,不显示数据'}</Text>
        </View>
        {/* <View style={[styles.col]}>
         <Text style={styles.text}>{voyageCode}</Text>
        </View> */}
        
        <View style={[styles.col]}>
          <Text style={styles.text}>{portName}</Text>
        </View>
        <View style={[styles.col]}>
          <Text style={styles.text}>{nextPortName}</Text>
        </View>
        <View style={[styles.col]}>
          <Text style={styles.text}>{stateMemo}</Text>
        </View>
        <View style={[styles.col, { width: 180 }]}>
          <Text style={styles.text}>{time}</Text>
        </View>
        <View style={[styles.col, { flex:1 }]}>
          <Text style={styles.text}>{line}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={[styles.col, { width: 120 }]}>
              <Text style={styles.text}>{shipName+'/'+voyageCode}</Text>  
            </View>
            {/*<View style={styles.col}>*/}
            {/*  <Text style={styles.text}>{voyageCode}</Text>*/}
            {/*</View>*/}
          </View>
        </Animated.View>
      </View>
    );
  }
}

ShipDepartmentScheduleItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default ShipDepartmentScheduleItem;
