/*
 *
 * ShipRepairApplyList constants
 *
 */
export const GET_SHIP_REPAIR_APPLY_LIST = 'onLand/ShipRepairApplyList/GET_SHIP_REPAIR_APPLY_LIST';
export const REVIEW_SHIP_REPAIR_APPLY = 'onLand/ShipRepairApplyList/REVIEW_SHIP_REPAIR_APPLY';
