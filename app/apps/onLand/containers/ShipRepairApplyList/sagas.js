import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipRepairApplyListRoutine, reviewShipRepairApplyRoutine } from './actions';

function* getShipRepairApplyList(action) {
  console.log(action);
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getShipRepairApplyListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    console.log('param---',JSON.stringify(param))
    const response = yield call(request, ApiFactory.getShipRepairApplyList(param));
    console.log('+++修船',JSON.stringify(response));
    const showAudit = response.showAudit;
    const { totalElements, content } = (response.dtoList || {});
    yield put(getShipRepairApplyListRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipRepairApplyListRoutine.failure(e));
  } finally {
    yield put(getShipRepairApplyListRoutine.fulfill());
  }
}

export function* getShipRepairApplyListSaga() {
  yield takeLatest(getShipRepairApplyListRoutine.TRIGGER, getShipRepairApplyList);
}

function* reviewShipRepairApply(action) {
  console.log(action);
  try {
    yield put(reviewShipRepairApplyRoutine.request());
    const { id, state } = action.payload;
    const response = yield call(request, ApiFactory.updateShipRepairApplyState({ id, state }));
    console.log('reviewShipRepairApply', response);
    yield put(reviewShipRepairApplyRoutine.success({ id, state }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(reviewShipRepairApplyRoutine.failure(e));
  } finally {
    yield put(reviewShipRepairApplyRoutine.fulfill());
  }
}

export function* reviewShipRepairApplySaga() {
  yield takeLatest(reviewShipRepairApplyRoutine.TRIGGER, reviewShipRepairApply);
}

// All sagas to be loaded
export default [getShipRepairApplyListSaga, reviewShipRepairApplySaga];
