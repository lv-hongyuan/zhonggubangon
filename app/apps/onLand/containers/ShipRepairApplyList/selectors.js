import { createSelector } from 'reselect/es';

const selectShipRepairApplyListDomain = () => state => state.shipRepairApplyList;

const makeSelectEmergencies = () => createSelector(selectShipRepairApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectShipRepairApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectShipRepairApplyListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeIsLoading };
