import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_REPAIR_APPLY_LIST, REVIEW_SHIP_REPAIR_APPLY } from './constants';

export const getShipRepairApplyListRoutine = createRoutine(GET_SHIP_REPAIR_APPLY_LIST);
export const getShipRepairApplyListPromise = promisifyRoutine(getShipRepairApplyListRoutine);

export const reviewShipRepairApplyRoutine = createRoutine(REVIEW_SHIP_REPAIR_APPLY);
export const reviewShipRepairApplyPromise = promisifyRoutine(reviewShipRepairApplyRoutine);
