import React from 'react';
import { View, TextInput, Text, SafeAreaView, TouchableOpacity, StyleSheet, DeviceEventEmitter } from 'react-native'
import { InputGroup, Item, Label, } from 'native-base';
import commonStyles from "../../../../common/commonStyles";
import Selector from "../../../../components/Selector";
import { getRegionArr } from "../../common/Constant";
import { connect } from 'react-redux';
import screenHOC from '../../../../components/screenHOC';
import { ROUTE_SHIP_TIME_NEW } from '../../RouteConstant';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
import InputItem from '../../../../components/InputItem';
@screenHOC
class ShipTimeNewSearch extends React.PureComponent {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params
    console.log('params:', this.params);
    this.state = {
      searchShipName: this.params ? this.params.data.searchShipName : '',
      searchPortName: this.params ? this.params.data.searchPortName : '',
      searchPq: this.params ? this.params.data.searchPq : '',
    };
  }

  render() {
    console.log(this.props.navigation);
    return (
      <SafeAreaView style={{ width: '100%', backgroundColor: '#eee' }}>
        <View style={{ padding: 10 }}>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="船名:"
              returnKeyType="search"
              value={this.state.searchShipName}
              clearButtonMode="while-editing"
              onChangeText={(text) => {
                this.setState({ searchShipName: text });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="港口:"
              returnKeyType="search"
              value={this.state.searchPortName}
              clearButtonMode="always"
              onChangeText={(text) => {
                this.setState({ searchPortName: text });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>片区:</Label>
              <Selector
                value={this.state.searchPq}
                items={getRegionArr}
                getItemValue={item => item}
                getItemText={item => item}
                onSelected={(item) => {
                  this.setState({ searchPq: item == '全部' ? '' : item });
                }}
                pickerType={'modal'}
              />
            </Item>
          </InputGroup>
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            this.props.navigation.navigate(
              ROUTE_SHIP_TIME_NEW,
            );
            DeviceEventEmitter.emit('addServiceAudits', {
              searchPq: this.state.searchPq,
              searchShipName: this.state.searchShipName,
              searchPortName: this.state.searchPortName,
            });
          }}
        >
          <Text style={{ color: '#fff', fontSize: 14 }}>搜索</Text>
        </TouchableOpacity>

      </SafeAreaView>
    );
  }
}

ShipTimeNewSearch.navigationOptions = ({ navigation }) => ({
  title: '船期表',
  headerLeft: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="取消"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={() => {
          navigation.dispatch(NavigationActions.back());
        }}
      />
    </HeaderButtons>
  ),
});

export default connect()(ShipTimeNewSearch);
const styles = StyleSheet.create({
  button: {
    backgroundColor: '#d00',
    height: 45,
    marginHorizontal: 10,
    borderRadius: 5,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
})