import { getBerthingEfficiencyRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';

const initState = {
  loading: false,
  refreshState: RefreshState.Idle,
  list: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getBerthingEfficiencyRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getBerthingEfficiencyRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getBerthingEfficiencyRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getBerthingEfficiencyRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
