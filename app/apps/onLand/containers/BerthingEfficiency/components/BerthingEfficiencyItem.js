import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet ,DeviceInfo} from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
const IS_IPHONEX = DeviceInfo.isIPhoneX_deprecated ? true : false;
const styles = StyleSheet.create({
  container: {
    width: IS_IPHONEX ? '90%' : '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: '14.285%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class BerthingEfficiencyItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      portName, berthNum, waitBerthNum, waitGoodsNum, waitGoodsTime, waitBerthTime,
    } = this.props.item || {};
    const _berthNum = parseInt(berthNum, 10);
    const _waitBerthNum = parseInt(waitBerthNum, 10);
    const _waitGoodsNum = parseInt(waitGoodsNum, 10);
    const percent = _berthNum > 0 ? (_berthNum - _waitBerthNum - _waitGoodsNum) * 100 / _berthNum : Number.NaN;
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{portName}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{berthNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitBerthNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitBerthTime}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitGoodsNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitGoodsTime}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{!Number.isNaN(percent) && (`${percent.toFixed(2)}%`)}</Text>
        </View>
      </View>
    );
  }
}

BerthingEfficiencyItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};
BerthingEfficiencyItem.defaultProps = {
  onSelect: undefined,
};

export default BerthingEfficiencyItem;
