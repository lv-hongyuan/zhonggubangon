import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getBerthingEfficiencyRoutine } from './actions';

function* getBerthingEfficiency(action) {
  console.log(action);
  try {
    yield put(getBerthingEfficiencyRoutine.request());
    const response = yield call(request, ApiFactory.getBerthEfficiency());
    console.log(response);
    yield put(getBerthingEfficiencyRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthingEfficiencyRoutine.failure(e));
  } finally {
    yield put(getBerthingEfficiencyRoutine.fulfill());
  }
}

export function* getBerthingEfficiencySaga() {
  yield takeLatest(getBerthingEfficiencyRoutine.TRIGGER, getBerthingEfficiency);
}

// All sagas to be loaded
export default [getBerthingEfficiencySaga];
