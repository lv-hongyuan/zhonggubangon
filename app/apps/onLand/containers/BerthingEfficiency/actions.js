import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_BERTHING_EFFICIENCY } from './constants';

export const getBerthingEfficiencyRoutine = createRoutine(GET_BERTHING_EFFICIENCY);
export const getBerthingEfficiencyPromiseCreator = promisifyRoutine(getBerthingEfficiencyRoutine);
