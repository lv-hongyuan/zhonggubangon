import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView , DeviceInfo, StatusBar} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getBerthingEfficiencyPromiseCreator } from './actions';
import { ROUTE_SHIP_TIME_DETAIL } from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import BerthingEfficiencyItem from './components/BerthingEfficiencyItem';
import RefreshListView from '../../../../components/RefreshListView';
const IS_IPHONEX = DeviceInfo.isIPhoneX_deprecated ? true : false;
const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    width: IS_IPHONEX ? '90%' : '100%'
  },
  col: {
    width: '14.285%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
};

@screenHOC
class BerthingEfficiency extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    this.props.navigation.navigate(
      ROUTE_SHIP_TIME_DETAIL,
      {
        id: this.props.navigation.getParam('id'),
        item,
      },
    );
  };

  supportedOrientations = Orientations.LANDSCAPE;

  loadList(loadMore) {
    this.props.getBerthingEfficiencyPromiseCreator(this.props.navigation.getParam('id'), loadMore).catch(() => {
    });
  }

  renderItem = ({ item }) => (
    <BerthingEfficiencyItem item={item} onSelect={this.onSelect} />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <StatusBar backgroundColor="#DC001B" barStyle="light-content" hidden={true} translucent={true}/>
        <ScrollView
          contentContainerStyle={{ minWidth: '100%' }}
          horizontal
          contentInsetAdjustmentBehavior="scrollableAxes"
        >
          <View style={{ flex: 1, minWidth: '100%' }}>
            <View style={styles.container}>
              <View style={styles.col}>
                <Text style={styles.text}>口岸</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>挂靠次数</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>等泊次数</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>等泊时间</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>等货次数</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>等货时间</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>直靠率</Text>
              </View>
            </View>
            <RefreshListView
              ref={(ref) => { this.listView = ref; }}
              contentInsetAdjustmentBehavior="scrollableAxes"
              style={{ width: '100%' }}
              data={this.props.list || []}
              keyExtractor={item => `${item.id}`}
              renderItem={this.renderItem}
              refreshState={this.props.refreshState}
              onHeaderRefresh={this.onHeaderRefresh}
              onFooterRefresh={this.onFooterRefresh}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

BerthingEfficiency.navigationOptions = () => ({
  title: '靠泊效率',
});

BerthingEfficiency.propTypes = {
  navigation: PropTypes.object.isRequired,
  getBerthingEfficiencyPromiseCreator: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
BerthingEfficiency.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getBerthingEfficiencyPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BerthingEfficiency);
