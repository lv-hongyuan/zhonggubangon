import { createSelector } from 'reselect/es';

const selectBerthingEfficiencyDomain = () => state => state.berthingEfficiency;

const makeList = () => createSelector(selectBerthingEfficiencyDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectBerthingEfficiencyDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
