import React from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet, Image, TouchableOpacity, Modal, ActivityIndicator, DeviceEventEmitter,
} from 'react-native';
import {
  Button, Label, Text, View,
} from 'native-base';
import _ from 'lodash';
import ImageViewer from 'react-native-image-zoom-viewer';
import myTheme from '../../../../../Themes';
import { ReviewState } from '../../../common/Constant';
import Svg from '../../../../../components/Svg';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
  },
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    fontSize: 14,
    color: '#535353',
  },
  text: {
    fontSize: 14,
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
  btnContainer: {
    height: 60,
    borderColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
  },
  leftButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightButton: {
    height: 40,
    flex: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

class ServiceFeeApplyItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      showButton: false,
        tugAudit: 1,
    };
  }
    componentDidMount(){
        this.listener = DeviceEventEmitter.addListener('addServiceAudit',(param)=>{
            console.log('GetTugBoatParam',param.audit);
            this.setState({tugAudit:param.audit})
        });
    }

    componentWillUnmount(){
        this.listener.remove();
    }
  renderReviewState(item) {
      switch (item.state) {
      case ReviewState.UnderReviewed:
        // if (this.state.showButton) {
          if (this.state.tugAudit == 0){
            return (
                <View style={styles.btnContainer}>
                    <Button
                        block
                        style={[styles.leftButton, { backgroundColor: '#FBB03B' }]}
                        onPress={() => {
                            this.props.onApproved(item);
                        }}
                    >
                        <Text style={styles.buttonText}>批准</Text>
                    </Button>
                    <Button
                        block
                        style={[styles.rightButton, { backgroundColor: '#DC001B' }]}
                        onPress={() => {
                            this.props.onDismissed(item);
                        }}
                    >
                        <Text style={styles.buttonText}>驳回</Text>
                    </Button>
                </View>
            );
          }else{
            return null
          }
      case ReviewState.Approved:
        return (
          <Svg
            icon="approved"
            size={150}
            style={{
              position: 'absolute',
              bottom: 0,
              right: 20,
              zIndex: 1,
              opacity: 0.5,
            }}
          />
        );
      case ReviewState.Dismissed:
        return (
          <Svg
            icon="dismissed"
            size={150}
            style={{
              position: 'absolute',
              bottom: 0,
              right: 20,
              zIndex: 1,
              opacity: 0.5,
            }}
          />
        );
      default:
        return null;
            }
  }

  renderImageLoading = () => (
    <View style={{
      justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%',
    }}
    >
      <ActivityIndicator />
    </View>
  );

  renderPreView(url) {
    return (
      <Modal visible={this.state.visible} transparent>
        <ImageViewer
          imageUrls={[{ url }]}
          onClick={() => {
            this.setState({ visible: false });
          }}
          onSwipeDown={() => {
            this.setState({ visible: false });
          }}
          loadingRender={this.renderImageLoading}
        />
      </Modal>
    );
  }

  render() {
    const item = this.props.item || {};
    const {
      shipName, voyageCode, portName, con20, con40, longrod, stowagePic,
      shortrod, refrigerator, applyReasons, rejectReasons,
    } = item;

    let newApplyReasons = applyReasons ? applyReasons : '';

    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Label style={styles.label}>船名:</Label>
          <Text style={styles.text}>{shipName}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>航次:</Label>
          <Text style={styles.text}>{voyageCode}</Text>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>发生港口:</Label>
          <Label style={styles.text}>{portName}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>绑扎小柜:</Label>
          <Label style={styles.text}>{con20}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>绑扎大柜:</Label>
          <Label style={styles.text}>{con40}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>长杆绑扎:</Label>
          <Label style={styles.text}>{longrod}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>短杆绑扎:</Label>
          <Label style={styles.text}>{shortrod}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>冷柜数量:</Label>
          <Label style={styles.text}>{refrigerator}</Label>
        </View>
        <View style={styles.row}>
          <Label style={styles.label}>申请原因:</Label>
          <Label style={styles.text}>{applyReasons}</Label>
        </View>
        <View style={styles.row}>
            <Label style={styles.label}>冷&nbsp;&nbsp;柜&nbsp;&nbsp;图:</Label>
            <TouchableOpacity
                style={{
                    width: 100,
                    height: 100,
                    marginLeft: 20,
                    backgroundColor: '#E0E0E0',
                }}
                onPress={() => {
                    if (_.isEmpty(stowagePic)) {
                        this.setState({visible: false});
                    } else {
                        this.setState({visible: true});
                    }
                }}
            >
                <Image
                    style={{ flex: 1 }}
                    source={{ uri: newApplyReasons }}
                />
                {this.renderPreView(stowagePic)}
            </TouchableOpacity>
        </View>
        {/*{!_.isEmpty(stowagePic) && (*/}
        {/*  */}
        {/*)}*/}

        {!_.isEmpty(rejectReasons) && (
          <View style={styles.row}>
            <Label style={styles.label}>审批描述:</Label>
            <Text style={styles.text}>{rejectReasons}</Text>
          </View>
        )}
          {this.renderReviewState(item)}
      </View>
    );
  }
}

ServiceFeeApplyItem.propTypes = {
  item: PropTypes.object.isRequired,
  onApproved: PropTypes.func.isRequired,
  onDismissed: PropTypes.func.isRequired,
};

export default ServiceFeeApplyItem;
