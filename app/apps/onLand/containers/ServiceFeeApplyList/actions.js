import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SERVICE_FEE_APPLY_LIST, REVIEW_SERVICE_FEE_APPLY } from './constants';

export const getServiceFeeApplyListRoutine = createRoutine(GET_SERVICE_FEE_APPLY_LIST);
export const getServiceFeeApplyListPromise = promisifyRoutine(getServiceFeeApplyListRoutine);

export const reviewServiceFeeApplyRoutine = createRoutine(REVIEW_SERVICE_FEE_APPLY);
export const reviewServiceFeeApplyPromise = promisifyRoutine(reviewServiceFeeApplyRoutine);
