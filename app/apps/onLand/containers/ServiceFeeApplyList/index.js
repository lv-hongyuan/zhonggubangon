import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, Text, SafeAreaView, Keyboard, TouchableOpacity, DeviceEventEmitter,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Button, Container, InputGroup, Item, Label,
} from 'native-base';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash';
import { getServiceFeeApplyListPromise, reviewServiceFeeApplyPromise } from './actions';
import ServiceFeeApplyItem from './components/ServiceFeeApplyItem';
import myTheme from '../../../../Themes';
import Svg from '../../../../components/Svg';
import RefreshListView from '../../../../components/RefreshListView';

import {
  makeIsLoading,
  makeSelectEmergencies,
  makeSelectRefreshState,
} from './selectors';
import screenHOC from '../../../../components/screenHOC';
import { ReviewState, reviewTitleFromState } from '../../common/Constant';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';
import Selector from '../../../../components/Selector';
import AlertView from '../../../../components/Alert';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

const firstPageNum = 1;
const DefaultPageSize = 10;

@screenHOC
class ServiceFeeApplyList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      shipName: '',
      voyageCode: '',
      inputShipName: '',
      inputVoyageCode: '',
      reviewState: ReviewState.UnderReviewed,
      inputReviewState: ReviewState.UnderReviewed,
      page: firstPageNum,
      pageSize: DefaultPageSize,
      showFilter: false,
      rejectReasons: null,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.setShowFilter(false);
      },
    });
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onApproved = (item) => {
    AlertView.show({
      message: '确定要批准申请吗？',
      showCancel: true,
      confirmAction: () => {
        this.props.reviewShipRepairApplyPromise({
          id: item.id,
          state: ReviewState.Approved,
        })
          .then(() => {}).catch(() => {});
      },
    });
  };

  onDismissed = (item) => {
    AlertView.show({
      message: '驳回申请',
      component: () => (
        <InputGroup style={[commonStyles.inputGroup, { marginLeft: 10, marginRight: 10 }]}>
          <InputItem
            label="驳回原因:"
            value={this.state.rejectReasons}
            onChangeText={(text) => {
              this.setState({ rejectReasons: text });
            }}
          />
        </InputGroup>
      ),
      showCancel: true,
      confirmAction: () => {
        this.props.reviewShipRepairApplyPromise({
          id: item.id,
          state: ReviewState.Dismissed,
          rejectReasons: this.state.rejectReasons,
        })
          .then(() => {}).catch(() => {});
        this.setState({ rejectReasons: null });
      },
    });
  };

  setShowFilter = (showFilter) => {
    this.props.navigation.setParams({
      showFilter,
    });
    this.setState({ showFilter });
  };

  setSearchValue(callBack) {
    this.setState({
      shipName: this.state.inputShipName,
      voyageCode: this.state.inputVoyageCode,
      reviewState: this.state.inputReviewState,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputShipName: this.state.shipName,
      inputVoyageCode: this.state.voyageCode,
      inputReviewState: this.state.reviewState,
    }, callBack);
  }

  loadList(loadMore) {
    const {
      shipName,
      voyageCode,
      reviewState,
      page,
      pageSize,
    } = this.state;
    console.log('{{{{',reviewState)
    this.props.getShipRepairApplyListPromise({
      zwcm: shipName,
      voyageCode,
      state: reviewState,
      page: loadMore ? page : firstPageNum,
      pageSize,
      loadMore,
    })
    // eslint-disable-next-line no-shadow
      .then(({ page ,showAudit}) => {
        DeviceEventEmitter.emit('addServiceAudit',{ audit:showAudit });
        this.setState({ page: page + 1 });
      })
      .catch(() => {});
  }

  doSearch = () => {
    Keyboard.dismiss();
    this.setSearchValue();
    this.setShowFilter(false);
    this.listView.beginRefresh();
  };

  renderSearch() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <View style={{ padding: 10, flex: 1 }}>
            <TouchableOpacity
              activeOpacity={1}
              style={styles.searchInput}
              onPress={() => {
                this.setInputValue();
                this.setShowFilter(true);
              }}
            >
              {!_.isEmpty(this.state.shipName) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>船名:</Label>
                  <Text style={commonStyles.text}>{this.state.shipName}</Text>
                </View>
              )}
              {!_.isEmpty(this.state.voyageCode) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>航次:</Label>
                  <Text style={commonStyles.text}>{this.state.voyageCode}</Text>
                </View>
              )}
              {!_.isNil(this.state.reviewState) && (
                <View style={styles.searchItem}>
                  <Label style={commonStyles.inputLabel}>审批状态:</Label>
                  <Text style={commonStyles.text}>{reviewTitleFromState(this.state.reviewState)}</Text>
                </View>
              )}
              {/* {_.isEmpty(this.state.shipName) && _.isEmpty(this.state.voyageCode) && ( */}
              {/* <View style={[styles.searchItem, {width: '100%'}]}> */}
              {/* <Text style={commonStyles.text}>{'点击输入要搜索的船名/航次'}</Text> */}
              {/* </View> */}
              {/* )} */}
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>
    );
  }

  renderFilter() {
    return this.state.showFilter && (
      <View style={[styles.container, {
        position: 'absolute', top: 0, left: 0, bottom: 0, right: 0,
      }]}
      >
        <SafeAreaView style={{ width: '100%' }}>
          <View style={{ padding: 10 }}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                autoFocus
                label="船名:"
                returnKeyType="search"
                value={this.state.inputShipName}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputShipName: text });
                }}
                onSubmitEditing={this.doSearch}
                onFocus={() => {
                  this.setShowFilter(true);
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="航次:"
                returnKeyType="search"
                value={this.state.inputVoyageCode}
                clearButtonMode="while-editing"
                onChangeText={(text) => {
                  this.setState({ inputVoyageCode: text });
                }}
                onSubmitEditing={this.doSearch}
                onFocus={() => {
                  this.setShowFilter(true);
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>审批状态:</Label>
                <Selector
                  value={this.state.inputReviewState}
                  items={Object.values(ReviewState)}
                  getItemValue={item => item}
                  getItemText={item => reviewTitleFromState(item)}
                  onSelected={(item) => {
                    this.setState({ inputReviewState: item });
                  }}
                />
              </Item>
            </InputGroup>
          </View>
          <Button
            onPress={this.doSearch}
            block
            style={{
              height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
            }}
          >
            <Text style={{ color: '#ffffff' }}>搜索</Text>
          </Button>
        </SafeAreaView>
      </View>
    );
  }

  renderItem = ({ item }) => (
    <ServiceFeeApplyItem item={item} onApproved={this.onApproved} onDismissed={this.onDismissed} />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderSearch()}
        <RefreshListView
          ref={(ref) => { this.listView = ref; }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
          ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
          ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
        />
        {this.renderFilter()}
      </Container>
    );
  }
}

ServiceFeeApplyList.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '劳务费申请'),
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('showFilter', false) ? (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      ) : (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      )}
    </HeaderButtons>
  ),
});

ServiceFeeApplyList.propTypes = {
  navigation: PropTypes.object.isRequired,
  getShipRepairApplyListPromise: PropTypes.func.isRequired,
  reviewShipRepairApplyPromise: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  list: makeSelectEmergencies(),
  deleteLoading: makeIsLoading(),
  refreshState: makeSelectRefreshState(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipRepairApplyListPromise: getServiceFeeApplyListPromise,
      reviewShipRepairApplyPromise: reviewServiceFeeApplyPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ServiceFeeApplyList);
