import { createSelector } from 'reselect/es';

const selectServiceFeeApplyListDomain = () => state => state.serviceFeeApplyList;

const makeSelectEmergencies = () => createSelector(selectServiceFeeApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectServiceFeeApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectServiceFeeApplyListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeIsLoading };
