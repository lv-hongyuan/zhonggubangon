/*
 *
 * ServiceFeeApplyList constants
 *
 */
export const GET_SERVICE_FEE_APPLY_LIST = 'onLand/ServiceFeeApplyList/GET_SERVICE_FEE_APPLY_LIST';
export const REVIEW_SERVICE_FEE_APPLY = 'onLand/ServiceFeeApplyList/REVIEW_SERVICE_FEE_APPLY';
