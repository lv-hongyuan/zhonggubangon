import { RefreshState } from '../../../../components/RefreshListView';
import { getServiceFeeApplyListRoutine, reviewServiceFeeApplyRoutine } from './actions';

const defaultState = {
  list: [],
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取申请列表
    case getServiceFeeApplyListRoutine.TRIGGER: {
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getServiceFeeApplyListRoutine.SUCCESS: {
      const { page, pageSize, list, showAudit} = action.payload;
      return {
        showAudit,
        ...state,
        list: page === 1 ? list : state.list.concat(list),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getServiceFeeApplyListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getServiceFeeApplyListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    // 审批申请
    case reviewServiceFeeApplyRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case reviewServiceFeeApplyRoutine.SUCCESS:
      const { id } = action.payload;
      const newList = (state.list || []).filter(item => item.id !== id);
      return { ...state, list: newList };
    case reviewServiceFeeApplyRoutine.FAILURE:
      return { ...state, loadingError: action.payload };
    case reviewServiceFeeApplyRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
