import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getServiceFeeApplyListRoutine, reviewServiceFeeApplyRoutine } from './actions';

function* getServiceFeeApplyList(action) {
  console.log(action);
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getServiceFeeApplyListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.findApplyLaborFee(param));
    const showAudit = response.showAudit;
    console.log('+++劳费',JSON.stringify(response));
    const { totalElements, content } = (response.dtoList || {});
    yield put(getServiceFeeApplyListRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getServiceFeeApplyListRoutine.failure(e));
  } finally {
    yield put(getServiceFeeApplyListRoutine.fulfill());
  }
}

export function* getServiceFeeApplyListSaga() {
  yield takeLatest(getServiceFeeApplyListRoutine.TRIGGER, getServiceFeeApplyList);
}

function* reviewServiceFeeApply(action) {
  console.log(action);
  try {
    yield put(reviewServiceFeeApplyRoutine.request());
    const { id, state, rejectReasons } = action.payload;
    const response = yield call(request, ApiFactory.updateApplyLaborFeeState({ id, state, rejectReasons }));
    console.log('reviewServiceFeeApply', response);
    yield put(reviewServiceFeeApplyRoutine.success({ id, state }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(reviewServiceFeeApplyRoutine.failure(e));
  } finally {
    yield put(reviewServiceFeeApplyRoutine.fulfill());
  }
}

export function* reviewServiceFeeApplySaga() {
  yield takeLatest(reviewServiceFeeApplyRoutine.TRIGGER, reviewServiceFeeApply);
}

// All sagas to be loaded
export default [getServiceFeeApplyListSaga, reviewServiceFeeApplySaga];
