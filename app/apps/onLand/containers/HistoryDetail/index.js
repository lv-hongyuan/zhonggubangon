
// 历史记录  （下方tab页第一个港口作业  右上角历史记录）

import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { createMaterialTopTabNavigator } from 'react-navigation';
import myTheme from '../../../../Themes';
import {
  ROUTE_HISTORY_RECORD_DETAIL,
  ROUTE_HISTORY_NEWSPAPER,
  ROUTE_EMERGENCIES_HISTORY,
} from '../../RouteConstant';
import { makeUnReadInstructionBadge } from '../Instructions/selectors';
import screenHOC from '../../../../components/screenHOC';
import { makeShipLandFunctionList } from '../PermissionsPrompt/selectors';
import HistoryRecordDetail from '../HistoryRecordDetail';
import HistoryNewsPaper from '../HistoryNewsPaper';
import EmergenciesHistory from '../EmergenciesHistory';

const styles = StyleSheet.create({
  title: {
    color: '#FFFFFF',
    fontSize: 16,
    textAlign: 'center',
  },
  subTitle: {
    color: '#FFFFFF',
    fontSize: 14,
    textAlign: 'center',
  },
});

const TopTabNavigator = createMaterialTopTabNavigator({
  [ROUTE_HISTORY_RECORD_DETAIL]: { screen: HistoryRecordDetail },
  [ROUTE_HISTORY_NEWSPAPER]: { screen: HistoryNewsPaper },
  [ROUTE_EMERGENCIES_HISTORY]: { screen: EmergenciesHistory },
}, {
  backBehavior: 'none',
  tabBarOptions: {
    allowFontScaling:false,
    activeTintColor: '#ED1727',
    inactiveTintColor: '#666666',
    style: {
      backgroundColor: '#ffffff',
    },
    tabStyle: {
      backgroundColor: 'transparent',
      borderBottomColor: myTheme.borderColor,
      borderBottomWidth: myTheme.borderWidth,
    },
    indicatorStyle: {
      backgroundColor: '#ED1727',
    },
  },
});

@screenHOC
class HistoryDetail extends React.PureComponent {
  static router = {
    ...TopTabNavigator.router,
    getStateForAction: (action, lastState) => {
      const state = TopTabNavigator.router.getStateForAction(action, lastState);
      if (lastState === state) {
        return state;
      }
      const routes = state.routes && state.routes.map(route => ({
        ...route,
        params: { ...route.params, ...state.params, readOnly: true },
      }));
      return { ...state, routes };
    },
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TopTabNavigator navigation={this.props.navigation} screenProps={{ test: this.state.test }} />
    );
  }
}

HistoryDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
};

HistoryDetail.navigationOptions = ({ navigation }) => {
  const shipName = navigation.getParam('shipName');
  const voyageCode = navigation.getParam('voyageCode');
  return {
    title: shipName || '历史记录',
    headerTitle: (
      <View style={{ flex: 1 }}>
        <View style={{ alignSelf: 'center' }}>
          <Text style={styles.title}>{shipName}</Text>
          <Text style={styles.subTitle}>{voyageCode}</Text>
        </View>
      </View>
    ),
  };
};

const mapStateToProps = createStructuredSelector({
  instructionsBadge: makeUnReadInstructionBadge(),
  functionList: makeShipLandFunctionList(),
});

export default connect(mapStateToProps)(HistoryDetail);
