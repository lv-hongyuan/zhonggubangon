import { createSelector } from 'reselect/es';

const selectDirectRateDomain = () => state => state.directRate;

const makeList = () => createSelector(selectDirectRateDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDirectRateDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
