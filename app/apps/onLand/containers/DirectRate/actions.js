import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DIRECT_RATE } from './constants';

export const getDirectRateRoutine = createRoutine(GET_DIRECT_RATE);
export const getDirectRatePromise = promisifyRoutine(getDirectRateRoutine);
