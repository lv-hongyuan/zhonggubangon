//! *********** 此页面已弃用 ***************
//! *********** 直靠率 ***************
//! *********** 此页面已弃用 ***************
import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated ,DeviceInfo} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Item,
  Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { getDirectRatePromise } from './actions';
import { ROUTE_DIRECT_RATE_RANKING } from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import DirectRateItem from './components/DirectRateItem';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from '../../../../common/commonStyles';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';
import DatePullSelector from '../../../../components/DatePullSelector';
const IS_IPHONEX = DeviceInfo.isIPhoneX_deprecated ? true : false;
const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
  },
};

@screenHOC
class DirectRate extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      recordW: 0,
      recordH: 0,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  //根View的onLayout回调函数
  onLayout = (event) => {
    //获取根View的宽高，以及左上角的坐标值
    let {x, y, width, height} = event.nativeEvent.layout;
    this.setState({
      recordW: width,
      recordH: height,
    });
  }

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    this.props.getDirectRatePromise({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    }).catch(() => {
    });
  }

  renderItem = ({ item }) => (
    <DirectRateItem
      item={item}
      headerTranslateX={this.translateX}
    />
  );

  renderFilter() {
    return (
      <View style={[styles.container,{marginHorizontal: IS_IPHONEX ? 44 : 0}]}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>起始时间:</Label>
            <DatePullSelector
              value={this.state.startTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ startTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>截止时间:</Label>
            <DatePullSelector
              value={this.state.endTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ endTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
        </SafeAreaView>
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{ width: '100%', flex: 1 }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
            >
              <View style={{ flex: 1, minWidth: '100%' }}>
                <View style={styles.container}>
                  <View style={styles.col}>
                    <Text style={styles.text}>片区</Text>
                  </View>
                  <View style={[styles.col, { width: 100 }]}>
                    <Text style={styles.text}>口岸</Text>
                  </View>
                  <View style={styles.col} onLayout={this.onLayout} >
                    <Text style={styles.text}>挂靠次数</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>等泊次数</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>等泊时间</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>等货次数</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>等货时间</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>直靠率</Text>
                  </View>
                  <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.translateX }],
                  }}
                  >
                    <View style={styles.container}>
                      <View style={[styles.col, {height: this.state.recordH}]} >
                        <Text style={styles.text} >片区</Text>
                      </View>
                      <View style={[styles.col, { width: 100 }]}>
                        <Text style={styles.text}>口岸</Text>
                      </View>
                    </View>
                  </Animated.View>
                </View>
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={this.props.list}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  // onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

DirectRate.navigationOptions = ({ navigation }) => ({
  title: '直靠率',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="排名"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={() => {
          navigation.navigate(ROUTE_DIRECT_RATE_RANKING);
        }}
      />
    </HeaderButtons>
  ),
});

DirectRate.propTypes = {
  getDirectRatePromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
DirectRate.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDirectRatePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectRate);
