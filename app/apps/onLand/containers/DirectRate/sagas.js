import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDirectRateRoutine } from './actions';

function* getDirectRate(action) {
  console.log(action);
  try {
    yield put(getDirectRateRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.portBerthEfficiencyList(startTime, endTime));
    console.log(response);
    yield put(getDirectRateRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDirectRateRoutine.failure(e));
  } finally {
    yield put(getDirectRateRoutine.fulfill());
  }
}

export function* getDirectRateSaga() {
  yield takeLatest(getDirectRateRoutine.TRIGGER, getDirectRate);
}

// All sagas to be loaded
export default [getDirectRateSaga];
