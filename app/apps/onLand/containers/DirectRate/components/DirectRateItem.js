import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class DirectRateItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pq, port, berthNum, waitBerthNum, waitBerthTime, waitGoodsNum, waitGoodsTime, directBerthPercent,
    } = this.props.item || {};
    const _waitBerthTime = parseFloat(waitBerthTime) || 0;
    const _waitGoodsTime = parseFloat(waitGoodsTime) || 0;

    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={[styles.col, { width: 100 }]}>
          <Text style={styles.text}>{port}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{berthNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitBerthNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{_waitBerthTime.toFixed(1)}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{waitGoodsNum}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{_waitGoodsTime.toFixed(1)}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{directBerthPercent}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pq}</Text>
            </View>
            <View style={[styles.col, { width: 100 }]}>
              <Text style={styles.text}>{port}</Text>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

DirectRateItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default DirectRateItem;
