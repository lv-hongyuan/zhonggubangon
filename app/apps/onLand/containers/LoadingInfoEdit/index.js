import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  InputGroup, Container, Content, Form,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { createStructuredSelector } from 'reselect/es';
import commonStyles from '../../../../common/commonStyles';
import myTheme from '../../../../Themes';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import InputItem from '../../../../components/InputItem';
import stringToNumber from '../../../../utils/stringToNumber';
import { getLoadDefaultValuePromise } from '../SailNewspaper/actions';
import screenHOC from '../../../../components/screenHOC';
import { makeSelectDefaultValues } from '../SailNewspaper/selectors';
import { MaskType } from '../../../../components/InputItem/TextInput';

@screenHOC
class LoadingInfoEdit extends React.PureComponent {
  static loadTableData(tableData) {
    const data = tableData || {};
    return {
      e20Load: parseFloat(data.e20Load) || 0,
      f20Load: parseFloat(data.f20Load) || 0,
      e40Load: parseFloat(data.e40Load) || 0,
      f40Load: parseFloat(data.f40Load) || 0,
      e40HLoad: parseFloat(data.e40HLoad) || 0,
      f40HLoad: parseFloat(data.f40HLoad) || 0,
      eOtherLoad: parseFloat(data.eOtherLoad) || 0,
      fOtherLoad: parseFloat(data.fOtherLoad) || 0,
      loadTon: parseFloat(data.loadTon) || 0,

      e20UnLoad: parseFloat(data.e20UnLoad) || 0,
      f20UnLoad: parseFloat(data.f20UnLoad) || 0,
      e40UnLoad: parseFloat(data.e40UnLoad) || 0,
      f40UnLoad: parseFloat(data.f40UnLoad) || 0,
      e40HUnLoad: parseFloat(data.e40HUnLoad) || 0,
      f40HUnLoad: parseFloat(data.f40HUnLoad) || 0,
      eOtherUnLoad: parseFloat(data.eOtherUnLoad) || 0,
      fOtherUnLoad: parseFloat(data.fOtherUnLoad) || 0,
      unLoadTon: parseFloat(data.unLoadTon) || 0,

      e20Dx: parseFloat(data.e20Dx) || 0,
      f20Dx: parseFloat(data.f20Dx) || 0,
      e40Dx: parseFloat(data.e40Dx) || 0,
      f40Dx: parseFloat(data.f40Dx) || 0,
      e40HDx: parseFloat(data.e40HDx) || 0,
      f40HDx: parseFloat(data.f40HDx) || 0,
      eOtherDx: parseFloat(data.eOtherDx) || 0,
      fOtherDx: parseFloat(data.fOtherDx) || 0,
      e20Tx: parseFloat(data.e20Tx) || 0,
      f20Tx: parseFloat(data.f20Tx) || 0,
      e40Tx: parseFloat(data.e40Tx) || 0,
      f40Tx: parseFloat(data.f40Tx) || 0,
      e40HTx: parseFloat(data.e40HTx) || 0,
      f40HTx: parseFloat(data.f40HTx) || 0,
      eOtherTx: parseFloat(data.eOtherTx) || 0,
      fOtherTx: parseFloat(data.fOtherTx) || 0,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.submit });
    if (this.loadDefault) {
      this.props.getLoadDefaultValuePromise({
        popId: this.props.navigation.getParam('popId'),
        zwcm: this.props.navigation.getParam('shipName'),
        voyageCode: this.props.navigation.getParam('voyageCode'),
        portName: this.props.navigation.getParam('portName'),
      })
        .then((values) => {
          this.mergeDefaultValues(values);
        })
        .catch(() => {
          this.setState({
            data: LoadingInfoEdit.loadTableData(this.props.navigation.getParam('tableData', {})),
          });
        });
    } else {
      this.setState({
        data: LoadingInfoEdit.loadTableData(this.props.navigation.getParam('tableData', {})),
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navBack) {
      this.props.navigation.goBack();
    }
  }

  get loadDefault() {
    return this.props.navigation.getParam('loadDefault', true);
  }

  setData(params) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    });
  }

  mergeDefaultValues(values) {
    const data = LoadingInfoEdit.loadTableData(this.props.navigation.getParam('tableData', {}));
    const defaultValues = values || {};
    data.e20Load = data.e20Load || defaultValues.eload20gp || 0;
    data.f20Load = data.f20Load || defaultValues.fload20gp || 0;
    data.e40Load = data.e40Load || defaultValues.eload40gp || 0;
    data.f40Load = data.f40Load || defaultValues.fload40gp || 0;
    data.e40HLoad = data.e40HLoad || defaultValues.eload40hc || 0;
    data.f40HLoad = data.f40HLoad || defaultValues.fload40hc || 0;
    data.eOtherLoad = data.eOtherLoad || defaultValues.eloadother || 0;
    data.fOtherLoad = data.fOtherLoad || defaultValues.floadother || 0;
    data.loadTon = data.loadTon || defaultValues.loadzl || 0;

    data.e20UnLoad = data.e20UnLoad || defaultValues.eunload20gp || 0;
    data.f20UnLoad = data.f20UnLoad || defaultValues.funload20gp || 0;
    data.e40UnLoad = data.e40UnLoad || defaultValues.eunload40gp || 0;
    data.f40UnLoad = data.f40UnLoad || defaultValues.funload40gp || 0;
    data.e40HUnLoad = data.e40HUnLoad || defaultValues.eunload40hc || 0;
    data.f40HUnLoad = data.f40HUnLoad || defaultValues.funload40hc || 0;
    data.eOtherUnLoad = data.eOtherUnLoad || defaultValues.eunloadother || 0;
    data.fOtherUnLoad = data.fOtherUnLoad || defaultValues.funloadother || 0;
    data.unLoadTon = data.unLoadTon || defaultValues.unloadzl || 0;
    this.setState({
      data,
    });
  }

  submit = () => {
    const onBack = this.props.navigation.getParam('onBack', () => {
    });
    const param = {};
    Object.keys(this.state.data).forEach((key) => {
      param[key] = stringToNumber(this.state.data[key]);
    });
    onBack(param);
    this.props.navigation.goBack();
  };

  renderLoad() {
    return (
      <ExtendCell
        title="装船货量"
        titleColor="#DC001B"
        isOpen
        touchable
        style={{ marginTop: 8 }}
      >
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20GF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f20Load}
            onChangeText={(text) => {
              this.setData({ f20Load: text });
            }}
          />
          <InputItem
            label="40GF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40Load}
            onChangeText={(text) => {
              this.setData({ f40Load: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="40HF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40HLoad}
            onChangeText={(text) => {
              this.setData({ f40HLoad: text });
            }}
          />
          <InputItem
            label="20E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e20Load}
            onChangeText={(text) => {
              this.setData({ e20Load: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="40E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e40Load}
            onChangeText={(text) => {
              this.setData({ e40Load: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="装货吨:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.loadTon}
            onChangeText={(text) => {
              this.setData({ loadTon: text });
            }}
          />
        </InputGroup>
      </ExtendCell>
    );
  }

  renderUnLoad() {
    return (
      <ExtendCell
        title="卸船货量"
        titleColor="#DC001B"
        isOpen
        touchable
        style={{ marginTop: 8 }}
      >
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20GF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f20UnLoad}
            onChangeText={(text) => {
              this.setData({ f20UnLoad: text });
            }}
          />
          <InputItem
            label="40GF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40UnLoad}
            onChangeText={(text) => {
              this.setData({ f40UnLoad: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="40HF:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40HUnLoad}
            onChangeText={(text) => {
              this.setData({ f40HUnLoad: text });
            }}
          />
          <InputItem
            label="20E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e20UnLoad}
            onChangeText={(text) => {
              this.setData({ e20UnLoad: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="40E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e40UnLoad}
            onChangeText={(text) => {
              this.setData({ e40UnLoad: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="卸货吨:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.unLoadTon}
            onChangeText={(text) => {
              this.setData({ unLoadTon: text });
            }}
          />
        </InputGroup>
      </ExtendCell>
    );
  }

  renderDx() {
    return (
      <ExtendCell
        title="倒箱箱量"
        titleColor="#DC001B"
        isOpen
        touchable
        style={{ marginTop: 8 }}
      >
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20F:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f20Dx}
            onChangeText={(text) => {
              this.setData({ f20Dx: text });
            }}
          />
          <InputItem
            label="40F:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40Dx}
            onChangeText={(text) => {
              this.setData({ f40Dx: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e20Dx}
            onChangeText={(text) => {
              this.setData({ e20Dx: text });
            }}
          />
          <InputItem
            label="40E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e40Dx}
            onChangeText={(text) => {
              this.setData({ e40Dx: text });
            }}
          />
        </InputGroup>
      </ExtendCell>
    );
  }

  renderTx() {
    return (
      <ExtendCell
        title="特殊作业箱量"
        titleColor="#DC001B"
        isOpen
        touchable
        style={{ marginTop: 8, marginBottom: 10 }}
      >
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20F:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f20Tx}
            onChangeText={(text) => {
              this.setData({ f20Tx: text });
            }}
          />
          <InputItem
            label="40F:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.f40Tx}
            onChangeText={(text) => {
              this.setData({ f40Tx: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="20E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e20Tx}
            onChangeText={(text) => {
              this.setData({ e20Tx: text });
            }}
          />
          <InputItem
            label="40E:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.e40Tx}
            onChangeText={(text) => {
              this.setData({ e40Tx: text });
            }}
          />
        </InputGroup>
      </ExtendCell>
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <Form>
            {this.renderLoad()}
            {this.renderUnLoad()}
            {this.renderDx()}
            {this.renderTx()}
          </Form>
        </Content>
      </Container>
    );
  }
}

LoadingInfoEdit.navigationOptions = ({ navigation }) => ({
  title: '装载信息',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="保存"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={navigation.getParam('submit')}
      />
    </HeaderButtons>
  ),
});

LoadingInfoEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
  getLoadDefaultValuePromise: PropTypes.func.isRequired,
  navBack: PropTypes.func,
};
LoadingInfoEdit.defaultProps = {
  navBack: undefined,
};

const mapStateToProps = createStructuredSelector({
  isLoading: state => state.sailNewspaper.loading,
  defaultValues: makeSelectDefaultValues(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getLoadDefaultValuePromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingInfoEdit);
