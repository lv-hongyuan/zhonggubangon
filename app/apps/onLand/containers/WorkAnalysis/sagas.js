import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getUnReadMesRoutine } from './actions';

function* getUnReadMes() {
  try {
    yield put(getUnReadMesRoutine.request());
    console.log('开始请求未读消息数量：');
    const response = yield call(request, ApiFactory.getNotReadNoticeNum());
    console.log('未读消息：',response);
    yield put(getUnReadMesRoutine.success({
        list:(response.dtoList ? response.dtoList : [])
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getUnReadMesRoutine.failure(e));
  } finally {
    yield put(getUnReadMesRoutine.fulfill());
  }
}

export function* getUnReadMesSaga() {
  yield takeLatest(getUnReadMesRoutine.TRIGGER, getUnReadMes);
}

// All sagas to be loaded
export default [getUnReadMesSaga];
