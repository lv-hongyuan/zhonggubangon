import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_UNREADMES } from './constants';

//请求未读消息数量
export const getUnReadMesRoutine = createRoutine(GET_UNREADMES);
export const getUnReadMesPromise = promisifyRoutine(getUnReadMesRoutine);
