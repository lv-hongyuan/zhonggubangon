import { getUnReadMesRoutine } from './actions';

const initState = { unReadMes: [] };

export default function (state = initState, action) {
  switch (action.type) {
    //获取未读消息数量
    case getUnReadMesRoutine.TRIGGER:
      return { ...state, loading: false };
    case getUnReadMesRoutine.SUCCESS:
      return { ...state, unReadMes: action.payload.list };
    case getUnReadMesRoutine.FAILURE:
      return { ...state };
    case getUnReadMesRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}