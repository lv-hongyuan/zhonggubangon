import React from 'react';
import PropTypes from 'prop-types';
import { Platform, SafeAreaView, View, DeviceEventEmitter } from 'react-native';
import JPushModule from 'jpush-react-native';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import myTheme from '../../../../Themes';
import GridButtonView from '../../../../components/GridButtonView';
import {
  ROUTE_ASSESSMENT,
  ROUTE_ASSESSMENT_PUBLICITY,
  ROUTE_BERTHING_EFFICIENCY,
  ROUTE_BERTHING_PLAN,
  ROUTE_BULLETIN_BOARD,
  ROUTE_DAILY_ACTUAL_LOADING,
  ROUTE_DAILY_PROVISIONING,
  ROUTE_DAILY_SCHEDULE,
  ROUTE_DIRECT_RATE,
  ROUTE_PORT_TIME,
  ROUTE_DRAWING_OUT_FEE,
  ROUTE_WHARF_PLANE,
  ROUTE_TASK_PROCESS,
  ROUTE_TASKPROCESS_DETAIL_NOTIFICATION,
} from '../../RouteConstant';
import screenHOC from '../../../../components/screenHOC';
import { makeWorkAnalysisFunctionList } from '../PermissionsPrompt/selectors';
import { getUnReadMesPromise } from './actions';
import { makeUnReadMes } from './selectors';
import { bindPromiseCreators } from 'redux-saga-routines/es';
@screenHOC
class WorkAnalysis extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      CornerMark: false,   //公告栏角标状态
      UnReadMesage: 0,   //未读的公告消息
    };
  }

  componentDidMount() {
    this.getNewsList()
    this.listener = DeviceEventEmitter.addListener('refreshCornerMarkState', () => {
      this.getNewsList()
    });
    this.listenerDesktop = DeviceEventEmitter.addListener('refreshDesktopMarkState', (data) => {
      if (data.data == "notice") {
        this.setState({ UnReadMesage: this.state.UnReadMesage + 1 })
      }
      this.showNotice()
    })
  }

  //设置ios桌面角标数量，设置信息工具界面公告栏/航次指令角标
  showNotice() {
    console.log('未读消息数量：',this.state.UnReadMesage);
    if (this.state.UnReadMesage > 0) {
      this.setState({ CornerMark: true })
    } else {
      this.setState({ CornerMark: false })
    }
    if (Platform.OS === 'ios') {
      let bages = this.state.UnReadMesage
      JPushModule.setBadge(bages, (success) => {
        console.log('设置通知', success);
      })
    }
  }
  componentWillUnmount() {
    this.listener.remove();
  }
  //获取未读公告与航次指令数量 **
  getNewsList() {
    this.props.getUnReadMes()
      .then(() => {
        let data = this.props.unReadMes
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            if (data[i].type == 10) {
              this.setState({ UnReadMesage: data[i].noticeNum })
            }
          }
        }
        this.showNotice()
      })
      .catch(() => {
      });
  }

  menuList() {
    return (this.props.functionList || []).map((appfunctionUrl) => {
      switch (appfunctionUrl) {
        case 'BerthingEfficiency':
          return {
            icon: 'onLand_berthing-efficiency',
            title: '靠泊效率',
            action: () => {
              this.props.navigation.push(ROUTE_BERTHING_EFFICIENCY);
            },
          };
        case 'DailySchedule':
          return {
            icon: 'onLand_job-evaluation',
            title: '每日船期表',
            action: () => {
              this.props.navigation.push(ROUTE_DAILY_SCHEDULE);
            },
          };
        case 'DirectRate':
          return {
            icon: 'onLand_job-evaluation',
            title: '直靠率',
            action: () => {
              //! ***********此页面已弃用***************
              this.props.navigation.push(ROUTE_DIRECT_RATE);
            },
          };
        case 'PortTime':
          return {
            icon: 'onLand_job-evaluation',
            title: '在港停时',
            action: () => {
              //! ***********此页面已弃用***************
              this.props.navigation.push(ROUTE_PORT_TIME);
            },
          };
        case 'BerthingPlan':
          return {
            icon: 'onLand_job-evaluation',
            title: '靠泊计划',
            action: () => {
              this.props.navigation.push(ROUTE_BERTHING_PLAN);
            },
          };
        case 'BulletinBoard':
          return {
            icon: this.state.CornerMark ? 'onLand_job-evaluation1' : 'onLand_job-evaluation',
            title: '公告栏',
            action: () => {
              this.props.navigation.push(ROUTE_BULLETIN_BOARD);
            },
          };
        case 'Assessment':
          return {
            icon: 'onLand_job-evaluation',
            title: 'APP考核',
            action: () => {
              //! ***********此页面已弃用***************
              this.props.navigation.push(ROUTE_ASSESSMENT);
            },
          };
        case 'DailyActualLoading':
          return {
            icon: 'onLand_job-evaluation',
            title: '每日实装',
            action: () => {
              this.props.navigation.push(ROUTE_DAILY_ACTUAL_LOADING);
            },
          };
        case 'DailyProvisioning':
          console.log('get每日预配')
          return {
            icon: 'onLand_job-evaluation',
            title: '每日预配',
            action: () => {
              this.props.navigation.push(ROUTE_DAILY_PROVISIONING);
            },
          };
        case 'AssessmentPublicity':
          console.log('get考核公示')
          return {
            icon: 'onLand_job-evaluation',
            title: '考核公示',
            action: () => {
              this.props.navigation.push(ROUTE_ASSESSMENT_PUBLICITY);
            },
          };
        // case 'WorkEfficiency':
        //   return {
        //     icon: 'onLand_job-evaluation',
        //     title: '结拖引费',
        //     action: () => {
        //       this.props.navigation.push(ROUTE_DRAWING_OUT_FEE);
        //     },
        //   };
        case 'WharfPlane':
          return {
            icon: 'onLand_job-evaluation',
            title: '码头计划',
            action: () => {
              this.props.navigation.push(ROUTE_WHARF_PLANE);
            },
          };
        case 'TaskProcess':
          return {
            icon: 'onLand_job-evaluation',
            title: '作业进度',
            action: () => {
              this.props.navigation.push(ROUTE_TASK_PROCESS);
            },
          };
        default:
          return undefined;
      }
    }).filter(item => !!item);
  }

  render() {
    const data = this.menuList();
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} style={{ flex: 1, minHeight: '100%' }}>
          <SafeAreaView style={{ flex: 1, minHeight: '100%' }}>
            <View style={{ backgroundColor: 'white', padding: 20 }}>
              <GridButtonView data={data} />
            </View>
          </SafeAreaView>
        </Content>
      </Container>
    );
  }
}

WorkAnalysis.navigationOptions = {
  title: '作业分析',
};

WorkAnalysis.propTypes = {
  navigation: PropTypes.object.isRequired,
  functionList: PropTypes.array,
  unReadMes: PropTypes.array,
};
WorkAnalysis.defaultProps = {
  functionList: [],
  unReadMes: [],
};

const mapStateToProps = createStructuredSelector({
  functionList: makeWorkAnalysisFunctionList(),
  unReadMes: makeUnReadMes(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getUnReadMes: getUnReadMesPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkAnalysis);
