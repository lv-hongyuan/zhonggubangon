import { createSelector } from 'reselect/es';

const selectShipWorkDomain = () => state => state.workAnalysis;

const makeUnReadMes = () => createSelector(
    selectShipWorkDomain(),
    subState => subState.unReadMes || [],
);

export { makeUnReadMes }