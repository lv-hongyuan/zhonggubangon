import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipTimeRoutine } from './actions';

function* getShipTime(action) {
  // console.log(action);
  try {
    yield put(getShipTimeRoutine.request());
    const response = yield call(request, ApiFactory.getShippingSchedule());
    // console.log(response);
    yield put(getShipTimeRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getShipTimeRoutine.failure(e));
  } finally {
    yield put(getShipTimeRoutine.fulfill());
  }
}

export function* getShipTimeSaga() {
  yield takeLatest(getShipTimeRoutine.TRIGGER, getShipTime);
}

// All sagas to be loaded
export default [getShipTimeSaga];
