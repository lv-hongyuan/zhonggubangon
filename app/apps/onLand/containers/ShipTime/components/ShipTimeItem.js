import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class ShipTimeItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      shipName, voyageCode, line, eta,
    } = (this.props.item || {});
    return (
      <TouchableHighlight onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={[styles.col, { width: 120 }]}>
            <Text style={styles.text}>{shipName + '/' + voyageCode}</Text>
          </View>
          {/*<View style={[styles.col, { width: 80 }]}>*/}
          {/*  <Text style={styles.text}>{voyageCode}</Text>*/}
          {/*</View>*/}
          <View style={[styles.col, { width: 140 }]}>
            <Text style={styles.text}>{line}</Text>
          </View>
          <View style={[styles.col, { width: 140 }]}>
            <Text style={styles.text}>{defaultFormat(eta)}</Text>
          </View>
          <Animated.View style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            transform: [{ translateX: this.props.headerTranslateX }],
          }}
          >
            <View style={[styles.container, { flex: 1 }]}>
              <View style={[styles.col, { width: 120 }]}>
                <Text style={styles.text}>{shipName + '/' + voyageCode}</Text>
              </View>
              {/*<View style={[styles.col, { width: 80 }]}>*/}
              {/*  <Text style={styles.text}>{voyageCode + '/' + 'yy'}</Text>*/}
              {/*</View>*/}
            </View>
          </Animated.View>
        </View>
      </TouchableHighlight>
    );
  }
}

ShipTimeItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
  headerTranslateX: PropTypes.object.isRequired,
};
ShipTimeItem.defaultProps = {
  onSelect: undefined,
};

export default ShipTimeItem;
