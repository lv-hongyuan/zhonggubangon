import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TIME } from './constants';

export const getShipTimeRoutine = createRoutine(GET_SHIP_TIME);
export const getShipTimePromiseCreator = promisifyRoutine(getShipTimeRoutine);
