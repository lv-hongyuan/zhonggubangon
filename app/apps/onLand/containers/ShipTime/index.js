import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getShipTimePromiseCreator } from './actions';
import { ROUTE_SHIP_TIME_DETAIL } from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import ShipTimeItem from './components/ShipTimeItem';
import RefreshListView from '../../../../components/RefreshListView';

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

@screenHOC
class ShipTime extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      containerOffSet: new Animated.Value(0),
      containerWidth: undefined,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    this.props.navigation.navigate(
      ROUTE_SHIP_TIME_DETAIL,
      {
        id: item.id,
        item,
      },
    );
  };

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  loadList(loadMore) {
    this.props.getShipTimePromiseCreator(this.props.navigation.getParam('id'), loadMore).catch(() => {
    });
  }

  renderItem = ({ item }) => (
    <ShipTimeItem
      item={item}
      headerTranslateX={this.translateX}
      onSelect={this.onSelect}
    />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{ width: '100%', flex: 1 }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
            >
              <View style={{ flex: 1, minWidth: '100%' }}>
                <View style={styles.container}>
                  <View style={[styles.col, { width: 120 }]}>
                    <Text style={styles.text}>船名/航次</Text>
                  </View>
                  {/*<View style={[styles.col, { width: 80 }]}>*/}
                  {/*  <Text style={styles.text}>航次</Text>*/}
                  {/*</View>*/}
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>航线</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>到港时间</Text>
                  </View>
                  <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.translateX }],
                  }}
                  >
                    <View style={styles.container}>
                      <View style={[styles.col, { width: 120 }]}>
                        <Text style={styles.text}>船名/航次</Text>
                      </View>
                      {/*<View style={[styles.col, { width: 80 }]}>*/}
                      {/*  <Text style={styles.text}>航次</Text>*/}
                      {/*</View>*/}
                    </View>
                  </Animated.View>
                </View>
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={this.props.list || []}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  // onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

ShipTime.navigationOptions = () => ({
  title: '船期表',
});

ShipTime.propTypes = {
  navigation: PropTypes.object.isRequired,
  getShipTimePromiseCreator: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
ShipTime.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getShipTimePromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShipTime);
