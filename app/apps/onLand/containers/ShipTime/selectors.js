import { createSelector } from 'reselect';

const selectShipTimeDomain = () => state => state.shipTime;

const makeList = () => createSelector(selectShipTimeDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectShipTimeDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
