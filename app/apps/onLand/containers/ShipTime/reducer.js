import { getShipTimeRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getShipTimeRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getShipTimeRoutine.SUCCESS:
      console.log('船期表数据:',fixIdList(action.payload))
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getShipTimeRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getShipTimeRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
