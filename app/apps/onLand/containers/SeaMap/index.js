import React from 'react';
import PropTypes from 'prop-types';
import { Platform, StatusBar, SafeAreaView, WebView } from 'react-native';
import Loading from '../../../../components/Loading';
import { createStructuredSelector } from 'reselect/es';
import { connect } from 'react-redux';
import HeaderButtons from 'react-navigation-header-buttons';
import { Container } from 'native-base';
import screenHOC from '../../../../components/screenHOC';
import myTheme from '../../../../Themes';
import AppStorage from '../../../../utils/AppStorage';
import {
    ApiConstants as PortApiConstants,
    PortHostKey,
  } from '../../../../apps/onLand/common/Constant';

@screenHOC
class SeaMap extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            renderWebView: false,
            isLoading: false,
            source: `http://${PortApiConstants.host}/api/map-app.html?${AppStorage.userId}`,
        };
    }

    componentDidMount(){
        // console.log(this.state.source)
    }

    onLoadEnd = () => {
        this.setState({ isLoading: false });
    };
    onLoadStart = (e) => {
        this.setState({ isLoading: true });
    };

    didFocus() {
        this.setState({
            renderWebView: true,
        });
    }

    renderLoading() {
        return this.state.isLoading
            ? <Loading backgroundStyle={{ backgroundColor: 'transparent' }} indicatorColor="#3d95d5" /> : null;
    }

    render() {
        return (
            <Container theme={myTheme} style={{ backgroundColor: '#FFFFFF' }}>
                <SafeAreaView style={{ flex: 1 }}>
                    {
                        this.state.renderWebView && (
                            <WebView
                                navigation={this.props.navigation}
                                url={this.state.source}
                                onLoadStart={this.onLoadStart}
                                onLoadEnd={this.onLoadEnd}
                            />
                        )
                    }
                </SafeAreaView>
                {this.renderLoading()}
            </Container>
        );
    }

}

SeaMap.navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('title', '船舶定位'),
  });

const mapStateToProps = createStructuredSelector({
});

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SeaMap);