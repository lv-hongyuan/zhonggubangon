import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  InputGroup,
  Container,
  Content,
  Form,
  Item,
  Label,
  Button,
  Text,
  View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import { defaultFormat } from '../../../../utils/DateFormat';
import {
  getPierListPromise,
  getBerthListPromise,
  savePlanPromise,
  completePlanPromise,
  changePlanPromise,
} from '../PortWorkDetail/actions';
import { BerthingState, EtaSource } from '../PortWorkDetail/constants';
import {
  makeSelectPortWorkDetail,
  makePierList,
  makeBerthList,
} from '../PortWorkDetail/selectors';
import myTheme from '../../../../Themes';
import TouchableCell from '../../../../components/ExtendCell/TouchableCell';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import DatePullSelector from '../../../../components/DatePullSelector/index';
import { PortWorkState } from '../PortWork/constants';
import InputItem from '../../../../components/InputItem';
import AlertView from '../../../../components/Alert';
import commonStyles from '../../../../common/commonStyles';
import Svg from '../../../../components/Svg';
import { planChangeReasons, waitBerthCauses, waitGoodsCauses ,directBerthCause} from '../../common/Constant';
import Switch from '../../../../components/Switch';
import Selector from '../../../../components/Selector';
import screenHOC from '../../../../components/screenHOC';
import { MaskType } from '../../../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  etaSource: {
    borderWidth: myTheme.borderWidth,
    borderRadius: 4,
    padding: 2,
    marginRight: 2,
    fontSize: 10,
    textAlign: 'center',
    marginLeft: 0,
  },
  buttonContainer: {
    height: 60,
    borderColor: '#E0E0E0',
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
  },
  saveButton: {
    backgroundColor: '#FBB03B',
    height: 40,
    flex: 1,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  completeButton: {
    backgroundColor: '#DC001B',
    height: 40,
    flex: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  changePlanButton: {
    backgroundColor: '#FBB03B',
    height: 40,
    flex: 1,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  doneImage: {
    position: 'absolute',
    top: 0,
    right: 40,
    opacity: 0.2,
  },
  tipText: {
    paddingTop: 10,
    paddingLeft: 0,
    paddingBottom: 0,
    fontSize: 12,
    color: 'red',
  },
  inLineTouchCell: {
    marginLeft: -30,
    marginRight: -30,
    backgroundColor: '#ffffff',
    borderTopWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
});

@screenHOC
class PortWorkDetailPlan extends React.PureComponent {
  static stateFromWordDetail(detail) {
    const workDetail = detail || {};

    function zeroToNull(time) {
      return time === 0 ? undefined : time;
    }

    return {
      data: {
        etaSource: workDetail.etaSource,
        etaSourceTime: workDetail.etaSourceTime,
        portId: workDetail.portId,
        voyageCode: workDetail.voyageCode,
        statePort: workDetail.statePort,
        estWaitBerthTime: workDetail.estWaitBerthTime,
        waitBerthCause: workDetail.waitBerthCause,
        // estWaitGoodsTime: workDetail.estWaitGoodsTime,
        // waitGoodsCause: workDetail.waitGoodsCause,
        wharf: workDetail.wharf,
        berth: workDetail.berth,
        voyageId: workDetail.voyageId,
        portName: workDetail.portName,
        nextPortId: workDetail.nextPortId,
        version: workDetail.version,
        directBerth: workDetail.directBerth,
        shipId: workDetail.shipId,
        id: workDetail.id,
        shipName: workDetail.shipName,
        portUserId: workDetail.portUserId,
        etaPlan: zeroToNull(workDetail.etaPlan),
        etbPlan: zeroToNull(workDetail.etbPlan),
        etdPlan: zeroToNull(workDetail.etdPlan),
        atdPort: zeroToNull(workDetail.atdPort),
        atbPort: zeroToNull(workDetail.atbPort),
        etdAfterBerth: zeroToNull(workDetail.etdAfterBerth),
        startWorkPort: zeroToNull(workDetail.startWorkPort),
        finishWorkPortMiddle: zeroToNull(workDetail.finishWorkPortMiddle),
        wharf1: workDetail.wharf1,
        workCon1: workDetail.workCon1,
        reverseCon1: workDetail.reverseCon1,
        wharf2: workDetail.wharf2,
        workCon2: workDetail.workCon2,
        reverseCon2: workDetail.reverseCon2,
        startWorkPortMiddle: zeroToNull(workDetail.startWorkPortMiddle),
        doubleHanger: workDetail.doubleHanger === '是' ? '是' : '否',
        finishWorkPort: zeroToNull(workDetail.finishWorkPort),
        ataPort: zeroToNull(workDetail.ataPort),
        directBerthPort: workDetail.directBerthPort,
        weighAnchorPort: zeroToNull(workDetail.weighAnchorPort),
        waitType: workDetail.directBerthPort === 1 ? BerthingState.directBerth : workDetail.waitType,
        planMemo: workDetail.planMemo,
      },
      updateEtaHybDate: workDetail.updateEtaHybDate,
      planWaitBerth: !workDetail.estWaitGoodsTime,
    };
  }

  static timeCompare(times) {
    const keys = Object.keys(times);
    let minKey = null;
    let minValue = null;

    for (let i = 0; i < keys.length; i += 1) {
      const key = keys[i];
      const value = times[key];
      if (value) {
        if (!minKey) {
          minKey = key;
          minValue = value;
        } else if (value < minValue) {
          return `${key}不能小于${minKey}`;
        } else {
          minKey = key;
          minValue = value;
        }
      }
    }
    return null;
  }

  constructor(props) {
    super(props);

    this.state = {
      data: PortWorkDetailPlan.stateFromWordDetail(props.workDetail),
      openPlan: true,
      changingPlan: false,
      planWaitBerth: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.workDetail !== nextProps.workDetail) {
      console.log('this.props.workDetail', this.props.workDetail);
      console.log('nextProps.workDetail', nextProps.workDetail);
      const workDetail = nextProps.workDetail || {};

      // 首次设置数据
      if (!this.props.workDetail) {
        this.setState({
          openPlan: workDetail.statePort >= PortWorkState.plan,
        });
      } else {
        if (this.state.changingPlan) {
          this.setState({ changingPlan: false });
        }
        if (this.props.workDetail.statePort !== workDetail.startWorkPort) {
          switch (workDetail.statePort) {
            case PortWorkState.plan:
              this.setState({ openPlan: true });
              break;
            default:
              break;
          }
        }
      }

      this.setWordDetail(workDetail);
    }
  }

  onBtnPier = () => {
    if (this.props.pierList.length > 0) {
      this.selector.showPicker();
    } else {
      this.props.getPierListPromise(this.props.workDetail.portId)
        .then(() => {
          this.selector.showPicker();
        })
        .catch(() => {});
    }
  };

  onChangePier = (item) => {
    this.setData({
      wharf: item.text,
    });
  };

  setWordDetail(detail) {
    const state = PortWorkDetailPlan.stateFromWordDetail(detail);
    this.setState({ ...state });
  }

  setData(params, callback = () => {}) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    }, callback);
  }

  validPlanSave() {
    const times = {
      预计靠泊时间: this.state.data.etbPlan,
      预计离泊时间: this.state.data.etdPlan,
    };
    const message = PortWorkDetailPlan.timeCompare(times);

    if (message) {
      return Promise.reject(new Error(message));
    }

    if (this.state.data.directBerth !== 1) {
      if (this.state.planWaitBerth && !parseFloat(this.state.data.estWaitBerthTime)) {
        return Promise.reject(new Error('请填写预计时长'));
      }
      // if (!this.state.planWaitBerth && !parseFloat(this.state.data.estWaitGoodsTime)) {
      //   return Promise.reject(new Error('请填写等货时长'));
      // }
      if (this.state.planWaitBerth && !this.state.data.waitBerthCause) {
        return Promise.reject(new Error('请选择非直靠原因'));
      }
      // if (!this.state.planWaitBerth && !this.state.data.waitGoodsCause) {
      //   return Promise.reject(new Error('请选择等货原因'));
      // }
    }

    return Promise.resolve();
  }

  validPlanConfirm() {
    if (!this.state.data.etaPlan) {
      return Promise.reject(new Error('请填写预达锚地时间'));
    }
    if (!this.state.data.etbPlan) {
      return Promise.reject(new Error('请填写预计靠泊时间'));
    }
    if (!this.state.data.etdPlan) {
      return Promise.reject(new Error('请填写预计离泊时间'));
    }
    if (this.state.data.etdPlan < this.state.data.etbPlan) {
      return Promise.reject(new Error('预计离泊时间不能小于预计靠泊时间'));
    }
    if (this.state.data.directBerth !== 1) {
      if (this.state.planWaitBerth && !parseFloat(this.state.data.estWaitBerthTime)) {
        return Promise.reject(new Error('请填写预计时长'));
      }
      // if (!this.state.planWaitBerth && !parseFloat(this.state.data.estWaitGoodsTime)) {
      //   return Promise.reject(new Error('请填写等货时长'));
      // }
      if (this.state.planWaitBerth && !this.state.data.waitBerthCause) {
        return Promise.reject(new Error('请选择非直靠原因'));
      }
      // if (!this.state.planWaitBerth && !this.state.data.waitGoodsCause) {
      //   return Promise.reject(new Error('请选择等货原因'));
      // }
    }
    return Promise.resolve();
  }

  savePlan = () => {
    this.validPlanSave()
      .then(() => {
        this.props.savePlanPromise(this.state.data)
          .then(() => {})
          .catch(() => {});
      })
      .catch((error) => {
        AlertView.show({ message: error.message });
      });
  };

  completePlan = () => {
    this.validPlanConfirm()
      .then(() => {
        AlertView.show({
          message: '确认要提交吗？',
          showCancel: true,
          confirmAction: () => {
            this.props.completePlanPromise(this.state.data)
              .then(() => {})
              .catch(() => {});
          },
        });
      })
      .catch((error) => {
        AlertView.show({ message: error.message });
      });
  };

  changePlan = () => {
    this.validPlanConfirm()
      .then(() => {
        if (_.isEmpty(this.state.data.planChangeReason)) {
          AlertView.show({ message: '请填写更改原因' });
          return;
        }
        AlertView.show({
          message: '确认要提交吗？',
          showCancel: true,
          confirmAction: () => {
            this.props.changePlanPromise(this.state.data)
              .then(() => {})
              .catch(() => {});
          },
        });
      })
      .catch((error) => {
        AlertView.show({ message: error.message });
      });
  };

  renderPlanChange() {
    if (this.state.changingPlan) {
      return (
        <View>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>更改原因</Label>
              <Selector
                value={this.state.data.planChangeReason}
                items={planChangeReasons}
                getItemValue={item => item}
                getItemText={item => item}
                pickerTitle="更改原因"
                onSelected={(item) => {
                  this.setData({ planChangeReason: item });
                }}
              />
            </Item>
          </InputGroup>
          <View style={styles.buttonContainer}>
            <Button
              onPress={() => {
                this.setState({ changingPlan: false });
                this.setWordDetail(this.props.workDetail);
              }}
              style={styles.saveButton}
            >
              <Text>取消</Text>
            </Button>
            <Button onPress={this.changePlan} style={styles.completeButton}>
              <Text>提交变更</Text>
            </Button>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.buttonContainer}>
        <Button
          onPress={() => {
            this.setState({ changingPlan: true });
          }}
          style={styles.changePlanButton}
        >
          <Text>计划变更</Text>
        </Button>
      </View>
    );
  }

  renderEtaSource() {
    const { etaSource } = this.state.data;
    const { updateEtaHybDate } = this.state;
    switch (etaSource) {
      case EtaSource.DEPARTMENT:
        return (
          <TouchableOpacity
            onPress={() => {
              if (updateEtaHybDate) {
                AlertView.show({
                  title: '最新更新时间',
                  message: defaultFormat(updateEtaHybDate),
                });
              }
            }}
          >
            <Text style={[styles.etaSource, {
              color: '#dc001b',
              borderColor: '#dc001b',
            }]}
            >{etaSource}
            </Text>
          </TouchableOpacity>
        );
      case EtaSource.SHIP:
        return (
          <Text style={[styles.etaSource, {
            color: '#dc001b',
            borderColor: '#dc001b',
          }]}
          >{etaSource}
          </Text>);
      case EtaSource.PORT:
        return (
          <Text style={[styles.etaSource, {
            color: '#39b24a',
            borderColor: '#39b24a',
          }]}
          >{etaSource}
          </Text>);
      case EtaSource.PREDICTION:
        return (
          <Text style={[styles.etaSource, {
            color: '#29a8df',
            borderColor: '#29a8df',
          }]}
          >{etaSource}
          </Text>);
      default:
        return (
          <Text style={[styles.etaSource, {
            color: '#29a8df',
            borderColor: '#29a8df',
          }]}
          >{etaSource}
          </Text>);
    }
  }

  renderPlan() {
    const canOpen = this.state.data.statePort >= PortWorkState.plan;
    if (!canOpen) {
      return (
        <TouchableCell
          title="计划阶段:每天【09:00】前反馈给航运部最新靠泊计划"
          isOpen={false}
          style={{
            marginTop: 8,
            backgroundColor: '#ffffff',
          }}
        />
      );
    }
    const isPlaning = this.state.data.statePort === PortWorkState.plan;
    const canEdit = this.state.changingPlan || isPlaning;
    return (
      <View style={{
        marginTop: 8,
        overflow: 'hidden',
        backgroundColor: '#ffffff',
      }}
      >
        {
          !canEdit ? (
            <Svg icon="done_blue" width={200} height={100} style={styles.doneImage} />
          ) : (null)
        }
        <ExtendCell
          title="计划阶段:每天【09:00】前反馈给航运部最新靠泊计划"
          titleColor="#DC001B"
          isOpen={this.state.openPlan}
          onPress={() => {
            this.setState({ openPlan: !this.state.openPlan });
          }}
          touchable
          style={{ backgroundColor: 'transparent' }}
        >
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="预达锚地: "
              maskType={MaskType.FLOAT}
              value={defaultFormat(this.state.data.etaSourceTime)}
              editable={false}
              rightItem={this.renderEtaSource()}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label
                style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
              >
                预达锚地:
              </Label>
              <DatePullSelector
                value={this.state.data.etaPlan}
                readOnly={!canEdit}
                onChangeValue={(value) => {
                  this.setData({ etaPlan: value });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={ commonStyles.inputItem}>
              <Label
                style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
              >
                预计靠泊:
              </Label>
              <DatePullSelector
                value={this.state.data.etbPlan}
                minValue={this.state.data.etaPlan}
                readOnly={!canEdit}
                onChangeValue={((value) => {
                  this.setData({ etbPlan: value });
                })}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label
                style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
              >
                预计离泊:
              </Label>
              <DatePullSelector
                value={this.state.data.etdPlan}
                minValue={this.state.data.etbPlan}
                readOnly={!canEdit}
                onChangeValue={(value) => {
                  this.setData({ etdPlan: value });
                }}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>
              <Label
                style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}
              >
                是否直靠:
              </Label>
              <Switch
                value={this.state.data.directBerth === 1}
                readOnly={!canEdit}
                onPress={() => {
                  this.setData({ directBerth: this.state.data.directBerth === 1 ? 0 : 1 });
                }}
                style={{
                  height: 30,
                  width: 60,
                }}
              />
            </Item>
          </InputGroup>
          {/*{this.state.data.directBerth !== 1 ? (*/}
          {/*  <InputGroup style={commonStyles.inputGroup}>*/}
          {/*    {*/}
          {/*      canEdit && (*/}
          {/*        <Item style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}>*/}
          {/*          <Label style={commonStyles.inputLabel}>等泊/等货:</Label>*/}
          {/*          <Switch*/}
          {/*            value={this.state.planWaitBerth}*/}
          {/*            offColor="#DC001B"*/}
          {/*            onTitle="等泊"*/}
          {/*            offTitle="等货"*/}
          {/*            onPress={() => {*/}
          {/*              this.setState({ planWaitBerth: !this.state.planWaitBerth }, () => {*/}
          {/*                if (this.state.planWaitBerth) {*/}
          {/*                  this.setData({*/}
          {/*                    estWaitBerthTime: this.state.data.estWaitGoodsTime,*/}
          {/*                    estWaitGoodsTime: 0,*/}
          {/*                    waitGoodsCause: undefined,*/}
          {/*                  });*/}
          {/*                } else {*/}
          {/*                  this.setData({*/}
          {/*                    estWaitBerthTime: 0,*/}
          {/*                    waitBerthCause: undefined,*/}
          {/*                    estWaitGoodsTime: this.state.data.estWaitBerthTime,*/}
          {/*                  });*/}
          {/*                }*/}
          {/*              });*/}
          {/*            }}*/}
          {/*            style={{*/}
          {/*              height: 30,*/}
          {/*              width: 70,*/}
          {/*              marginRight: 10,*/}
          {/*            }}*/}
          {/*          />*/}
          {/*        </Item>*/}
          {/*      )*/}
          {/*    }*/}
          {/*    <InputItem*/}
          {/*      label={this.state.planWaitBerth ? '等泊时长:' : '等货时长:'}*/}
          {/*      maskType={MaskType.FLOAT}*/}
          {/*      value={(this.state.planWaitBerth ? (this.state.data.estWaitBerthTime || 0) : (this.state.data.estWaitGoodsTime || 0))}*/}
          {/*      editable={canEdit}*/}
          {/*      onChangeText={(text) => {*/}
          {/*        if (this.state.planWaitBerth) {*/}
          {/*          this.setData({*/}
          {/*            estWaitBerthTime: text,*/}
          {/*            estWaitGoodsTime: 0,*/}
          {/*          });*/}
          {/*        } else {*/}
          {/*          this.setData({*/}
          {/*            estWaitBerthTime: 0,*/}
          {/*            estWaitGoodsTime: text,*/}
          {/*          });*/}
          {/*        }*/}
          {/*      }}*/}
          {/*    />*/}
          {/*  </InputGroup>) : null*/}
          {/*}*/}

          {this.state.data.directBerth !== 1 ? (
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>
                    非直靠原因:
                  </Label>
                  <Selector
                      value={this.state.data.waitBerthCause}
                      items={directBerthCause}
                      getItemValue={item => item}
                      getItemText={item => item}
                      pickerTitle="非直靠原因"
                      onSelected={(item) => {
                          this.setData({
                            waitBerthCause: item,
                          });
                      }}
                      disabled={!canEdit}
                  />
                </Item>
              </InputGroup>)
              : null
          }
          {this.state.data.directBerth !== 1 ? (
                  <InputGroup style={commonStyles.inputGroup}>
                    <InputItem
                        label="预计时长:"
                        maskType={MaskType.FLOAT}
                        value={(this.state.data.estWaitBerthTime || 0) }
                        editable={canEdit}
                        keyboardType="numeric"
                        onChangeText={(text) => {
                          // if (this.state.planWaitBerth) {
                          //   this.setData({
                          //     estWaitBerthTime: text,
                          //   });
                          // } else {
                          //   this.setData({
                          //     estWaitBerthTime: 0,
                          //   });
                          // }
                          this.setData({
                            estWaitBerthTime: text,
                          });
                        }}
                    />
                  </InputGroup>
              )
              : null
          }
          {/*{this.state.data.directBerth !== 1 ? (*/}
          {/*  <InputGroup style={commonStyles.inputGroup}>*/}
          {/*    <Item style={commonStyles.inputItem}>*/}
          {/*      <Label*/}
          {/*        style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}*/}
          {/*      >*/}
          {/*        {this.state.planWaitBerth ? '等泊原因:' : '等货原因:'}*/}
          {/*      </Label>*/}
          {/*      <Selector*/}
          {/*        value={this.state.planWaitBerth ? this.state.data.waitBerthCause : this.state.data.waitGoodsCause}*/}
          {/*        items={this.state.planWaitBerth ? waitBerthCauses : waitGoodsCauses}*/}
          {/*        getItemValue={item => item}*/}
          {/*        getItemText={item => item}*/}
          {/*        pickerTitle={this.state.planWaitBerth ? '等泊原因' : '等货原因'}*/}
          {/*        onSelected={(item) => {*/}
          {/*          if (this.state.planWaitBerth) {*/}
          {/*            this.setData({*/}
          {/*              waitBerthCause: item,*/}
          {/*              waitGoodsCause: undefined,*/}
          {/*            });*/}
          {/*          } else {*/}
          {/*            this.setData({*/}
          {/*              waitGoodsCause: item,*/}
          {/*              waitBerthCause: undefined,*/}
          {/*            });*/}
          {/*          }*/}
          {/*        }}*/}
          {/*        disabled={!canEdit}*/}
          {/*      />*/}
          {/*    </Item>*/}
          {/*  </InputGroup>) : null*/}
          {/*}*/}
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={canEdit ? commonStyles.inputLabel : commonStyles.readOnlyInputLabel}>预靠码头:</Label>
              <Selector
                value={this.state.data.wharf}
                items={this.props.pierList}
                disabled={!canEdit}
                ref={(ref) => {
                  this.selector = ref;
                }}
                onPress={this.onBtnPier}
                pickerTitle="请选择码头"
                getItemValue={item => item.text}
                getItemText={item => item.text}
                onSelected={this.onChangePier}
              />
            </Item>
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="预靠泊位:"
              value={this.state.data.berth}
              editable={canEdit}
              onChangeText={(text) => {
                this.setData({ berth: text });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="计划备注:"
              value={this.state.data.planMemo}
              editable={canEdit}
              onChangeText={(text) => {
                this.setData({ planMemo: text });
              }}
            />
          </InputGroup>
          {isPlaning ? (
            <View style={styles.buttonContainer}>
              <Button onPress={this.savePlan} style={styles.saveButton}>
                <Text>暂存</Text>
              </Button>
              <Button onPress={this.completePlan} style={styles.completeButton}>
                <Text>提交</Text>
              </Button>
            </View>) : this.renderPlanChange()
          }
        </ExtendCell>
      </View>
    );
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <Form>
            {this.renderPlan()}
          </Form>
        </Content>
      </Container>
    );
  }
}

PortWorkDetailPlan.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '信息录入'),
  tabBarLabel: '计划',
});

PortWorkDetailPlan.propTypes = {
  savePlanPromise: PropTypes.func.isRequired,
  completePlanPromise: PropTypes.func.isRequired,
  changePlanPromise: PropTypes.func.isRequired,
  getPierListPromise: PropTypes.func.isRequired,
  workDetail: PropTypes.object,
  pierList: PropTypes.array,
};
PortWorkDetailPlan.defaultProps = {
  workDetail: {},
  pierList: [],
};

const mapStateToProps = createStructuredSelector({
  workDetail: makeSelectPortWorkDetail(),
  pierList: makePierList(),
  berthList: makeBerthList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPierListPromise,
      getBerthListPromise,
      savePlanPromise,
      completePlanPromise,
      changePlanPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortWorkDetailPlan);
