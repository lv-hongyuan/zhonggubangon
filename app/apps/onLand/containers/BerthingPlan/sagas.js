import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getBerthingPlanRoutine } from './actions';

function* getBerthingPlan(action) {
  console.log(action);
  try {
    yield put(getBerthingPlanRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.berthPlanList(startTime, endTime));
    console.log(response);
    yield put(getBerthingPlanRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthingPlanRoutine.failure(e));
  } finally {
    yield put(getBerthingPlanRoutine.fulfill());
  }
}

export function* getBerthingPlanSaga() {
  yield takeLatest(getBerthingPlanRoutine.TRIGGER, getBerthingPlan);
}

// All sagas to be loaded
export default [getBerthingPlanSaga];
