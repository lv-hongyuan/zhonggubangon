import { getBerthingPlanRoutine } from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getBerthingPlanRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getBerthingPlanRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getBerthingPlanRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getBerthingPlanRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
