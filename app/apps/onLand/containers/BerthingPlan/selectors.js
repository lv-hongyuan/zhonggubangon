import { createSelector } from 'reselect/es';

const selectBerthingPlanDomain = () => state => state.berthingPlan;

const makeList = () => createSelector(selectBerthingPlanDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectBerthingPlanDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
