import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_BERTHING_PLAN } from './constants';

export const getBerthingPlanRoutine = createRoutine(GET_BERTHING_PLAN);
export const getBerthingPlanPromise = promisifyRoutine(getBerthingPlanRoutine);
