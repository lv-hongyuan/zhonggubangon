import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class BerthingPlanItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pq, port, zwcm, hc, toAnchorTime, etb, etd, isDirectBerth, noDirectBerthCause, portName, berth,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{port}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{zwcm}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{hc}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!toAnchorTime && defaultFormat(toAnchorTime)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!etb && defaultFormat(etb)}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{!!etd && defaultFormat(etd)}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{isDirectBerth === 1 ? '直靠' : '非直靠'}</Text>
        </View>
        <View style={[styles.col, { width: 90 }]}>
          <Text style={styles.text}>{noDirectBerthCause}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{portName}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{berth}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pq}</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.text}>{port}</Text>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

BerthingPlanItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default BerthingPlanItem;
