import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, ActivityIndicator, Clipboard, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import {
    Container,
    Item,
    Label,
    View,
    Button,
} from 'native-base';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';
import HeaderButtons from 'react-navigation-header-buttons';
import Overlay from 'teaset/components/Overlay/Overlay';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import AlertView from "../../../../components/Alert";

const styles = {
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignContent: 'stretch',
    },
    text: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
    },
    activity: {
        color: '#FFFFFF',
        size: 'large',
    },
    detail: {
        padding: 15,
        fontSize: 15,
        margin: 5,
        borderRadius: 3,
        borderColor: '#FF4500',
        borderWidth: 2,
    },
    copyBtn: {
        marginTop: 30,
        marginLeft: 15,
        backgroundColor: '#00BFFF',
        height: 30,
        width: 130,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
    },
    copyText: {
        color: '#FFFFFF',
        height: 30,
        lineHeight: 30,
        textAlign: 'center',
        fontSize: 16,
    },
};
@screenHOC
export default class TaskProcessDetailNotification extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            detailText: '',
            copyText: '复制详情信息',
            spoId: this.props.navigation.state.params.spoId,
            voyageCode:this.props.navigation.state.params.voyageCode,
            shipName:this.props.navigation.state.params.shipName
        };
    }


    componentDidMount() {
        const popView = (
            <Overlay.PopView
                modal
                overlayOpacity={0.7}
                ref={(ref) => {
                    updateOverlay = ref;
                }}
                style={{ alignItems: 'center', justifyContent: 'center' }}
            >
                <View style={{ padding: 100, alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator style={styles.activity} />
                    <Text style={[styles.text, { marginTop: 20, color: '#FFFFFF' }]}>正在加载..</Text>
                </View>
            </Overlay.PopView>
        );
        Overlay.show(popView);
        request(ApiFactory.taskProcessDetailNotification({
            voyageCode:this.state.voyageCode,
            shipName:this.state.shipName
        }))
            .then((response) => {
                if (updateOverlay) updateOverlay.close();
                console.log('作业进度详情:', response);
                this.setState({ detailText: response.alert })
            })
            .catch((error) => {
                if (updateOverlay) updateOverlay.close();
                AlertView.show({ message: error.message });
            });
    }

    supportedOrientations = Orientations.PORTRAIT;

    renderCopyBtn() {
        return <TouchableOpacity
            style={styles.copyBtn}
            activeOpacity={0.7}
            onPress={() => {
                if (this.state.copyText == '复制详情信息') {
                    Clipboard.setString(this.state.detailText);
                    this.setState({ copyText: '已复制' })
                }
            }}
        >
            <Text style={styles.copyText}>{this.state.copyText}</Text>
        </TouchableOpacity>
    }

    render() {
        return (
            <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
                <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
                    {(this.state.detailText && this.state.detailText.length > 0)?<Text style={styles.detail}>{this.state.detailText}</Text>:null}
                    {(this.state.detailText && this.state.detailText.length > 0) ? this.renderCopyBtn() : null}
                </SafeAreaView>
            </Container>
        )
    }
}

TaskProcessDetailNotification.navigationOptions = () => ({
    title: '作业进度详情',
});

