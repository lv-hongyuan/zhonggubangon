import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { ACTIVATION_NEXT_PORT } from './constants';

export const activationNextPortRoutine = createRoutine(ACTIVATION_NEXT_PORT);
export const activationNextPortPromise = promisifyRoutine(activationNextPortRoutine);
