import { createSelector } from 'reselect/es';

const selectActivationNextPortDomain = () => state => state.activationNextPort;

const makeIsLoading = () => createSelector(selectActivationNextPortDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeIsLoading };
