import { activationNextPortRoutine } from './actions';

const defaultState = {
  loadingError: null,
  isLoading: false,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取箱号列表
    case activationNextPortRoutine.TRIGGER:
      return {
        ...state, isLoading: true, loadingError: undefined,
      };
    case activationNextPortRoutine.SUCCESS:
      return { ...state };
    case activationNextPortRoutine.FAILURE:
      return {
        ...state, loadingError: action.payload,
      };
    case activationNextPortRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
