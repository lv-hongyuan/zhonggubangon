import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { activationNextPortRoutine } from './actions';

function* activationNextPort(action) {
  console.log(action);
  try {
    yield put(activationNextPortRoutine.request());
    const { portName, shipName, voyageCode } = action.payload;
    const response = yield call(request, ApiFactory.activationNextPort({ portName, shipName, voyageCode }));
    console.log('activationNextPort', response);
    yield put(activationNextPortRoutine.success([response]));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(activationNextPortRoutine.failure(e));
  } finally {
    yield put(activationNextPortRoutine.fulfill());
  }
}

export function* activationNextPortSaga() {
  yield takeLatest(activationNextPortRoutine.TRIGGER, activationNextPort);
}

// All sagas to be loaded
export default [activationNextPortSaga];
