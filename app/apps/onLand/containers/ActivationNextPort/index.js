import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Container, Content, InputGroup, Button, Form, Text,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import AlertView from '../../../../components/Alert';
import { activationNextPortPromise } from './actions';
import myTheme from '../../../../Themes';
import {
  makeIsLoading,
} from './selectors';
import screenHOC from '../../../../components/screenHOC';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';

const styles = StyleSheet.create({
  form: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 8,
    flex: 1,
  },
  button: {
    height: 45,
    width: '100%',
    justifyContent: 'center',
    backgroundColor: '#DC001B',
  },
});

@screenHOC
class ActivationNextPort extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      portName: undefined,
      shipName: undefined,
      voyageCode: undefined,
    };
  }

  activationNextPort() {
    const { portName, shipName, voyageCode } = this.state;
    if (_.isEmpty(portName)) {
      AlertView.show({ message: '请填写港口名称' });
      return;
    }
    if (_.isEmpty(shipName)) {
      AlertView.show({ message: '请填写中文船名' });
      return;
    }
    if (_.isEmpty(voyageCode)) {
      AlertView.show({ message: '请填写进口航次号' });
      return;
    }
    this.props.activationNextPortPromise({
      portName, shipName, voyageCode,
    })
      .then(() => {
        AlertView.show({ message: '激活港口动态成功，请到港口动态列表录入数据' });
      })
      .catch(() => {});
  }

  render() {
    const { portName, shipName, voyageCode } = this.state;
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#FFFFFF' }}>
        <Content contentInsetAdjustmentBehavior="scrollableAxes">
          <Form style={styles.form}>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="港口:"
                value={portName}
                placeholder="请输入港口"
                onChangeText={(text) => {
                  this.setState({ portName: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="船舶:"
                value={shipName}
                placeholder="请输入船舶"
                onChangeText={(text) => {
                  this.setState({ shipName: text });
                }}
              />
            </InputGroup>
            <InputGroup style={commonStyles.inputGroup}>
              <InputItem
                label="进口航次:"
                value={voyageCode}
                placeholder="请输入进口航次"
                onChangeText={(text) => {
                  this.setState({ voyageCode: text });
                }}
              />
            </InputGroup>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.activationNextPort();
              }}
              style={styles.button}
            >
              <Text style={{ color: '#ffffff' }}>激活</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

ActivationNextPort.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '港口动态激活'),
});

ActivationNextPort.propTypes = {
  activationNextPortPromise: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      activationNextPortPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivationNextPort);
