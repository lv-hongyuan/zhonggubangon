import {
  CREATE_PORT_ATTENTION,
  CREATE_PORT_ATTENTION_RESULT,
  UPDATE_PORT_ATTENTION,
  UPDATE_PORT_ATTENTION_RESULT,
  CLEAN, SELECT_NEXT_PORT, SELECT_NEXT_PORT_RESULT,
} from './constants';

const defaultState = {
  isLoading: false,
  loadingError: undefined,
  navBack: false,
  portList: [],
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case SELECT_NEXT_PORT:
      return { ...state, isLoading: true, loadingError: undefined };
    case SELECT_NEXT_PORT_RESULT:
      return { ...state, ...action.payload, isLoading: false };

    case CREATE_PORT_ATTENTION:
      return { ...state, isLoading: true, loadingError: undefined };
    case CREATE_PORT_ATTENTION_RESULT:
      if (!action.payload.loadingError) {
        return { ...state, navBack: true };
      }
      return { ...state, ...action.payload, isLoading: false };

    case UPDATE_PORT_ATTENTION:
      return { ...state, isLoading: true, loadingError: undefined };
    case UPDATE_PORT_ATTENTION_RESULT:
      if (!action.payload.loadingError) {
        return { ...state, navBack: true };
      }
      return { ...state, ...action.payload, isLoading: false };

    case CLEAN:
      return defaultState;

    default:
      return state;
  }
}
