import {
  SELECT_NEXT_PORT,
  SELECT_NEXT_PORT_RESULT,
  CREATE_PORT_ATTENTION,
  CREATE_PORT_ATTENTION_RESULT,
  UPDATE_PORT_ATTENTION,
  UPDATE_PORT_ATTENTION_RESULT,
  CLEAN,
} from './constants';

export function getNextPortSelectAction(popId) {
  return {
    type: SELECT_NEXT_PORT,
    payload: { popId },
  };
}

export function getNextPortSelectResultAction(portList, loadingError) {
  return {
    type: SELECT_NEXT_PORT_RESULT,
    payload: { portList, loadingError },
  };
}

export function createPortAttentionAction(portAttention) {
  return {
    type: CREATE_PORT_ATTENTION,
    payload: portAttention,
  };
}

export function createPortAttentionResultAction(response, loadingError) {
  return {
    type: CREATE_PORT_ATTENTION_RESULT,
    payload: { response, loadingError },
  };
}

export function updatePortAttentionAction(portAttention) {
  return {
    type: UPDATE_PORT_ATTENTION,
    payload: portAttention,
  };
}

export function updatePortAttentionResultAction(response, loadingError) {
  return {
    type: UPDATE_PORT_ATTENTION_RESULT,
    payload: { response, loadingError },
  };
}

export function cleanAction() {
  return {
    type: CLEAN,
  };
}
