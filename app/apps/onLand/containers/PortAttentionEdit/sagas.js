import { call, put, takeLatest } from 'redux-saga/effects';
import { SELECT_NEXT_PORT, CREATE_PORT_ATTENTION, UPDATE_PORT_ATTENTION } from './constants';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getNextPortSelectResultAction,
  createPortAttentionResultAction,
  updatePortAttentionResultAction,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getNextPortSelect(action) {
  console.log(action);
  try {
    const response = yield call(
      request,
      ApiFactory.getNextPopSelect(action.payload.popId),
    );
    yield put(getNextPortSelectResultAction(response.dtoList, undefined));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getNextPortSelectResultAction(undefined, e));
  }
}

export function* getNextPortSelectSaga() {
  yield takeLatest(SELECT_NEXT_PORT, getNextPortSelect);
}

function* createAttention(action) {
  console.log(action);
  try {
    const response = yield call(
      request,
      ApiFactory.createAttention(action.payload),
    );
    yield put(createPortAttentionResultAction(response, undefined));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(createPortAttentionResultAction(undefined, e));
  }
}

export function* createPortAttentionSaga() {
  yield takeLatest(CREATE_PORT_ATTENTION, createAttention);
}

function* updateAttention(action) {
  console.log(action);
  try {
    const response = yield call(
      request,
      ApiFactory.updateAttention(action.payload),
    );
    console.log('updateAttention', response.toString());
    yield put(updatePortAttentionResultAction(response, undefined));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(updatePortAttentionResultAction(undefined, e));
  }
}

export function* updatePortAttentionSaga() {
  yield takeLatest(UPDATE_PORT_ATTENTION, updateAttention);
}

// All sagas to be loaded
export default [
  getNextPortSelectSaga,
  createPortAttentionSaga,
  updatePortAttentionSaga,
];
