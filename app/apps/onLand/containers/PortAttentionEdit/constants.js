/*
 *
 * PortWork constants
 *
 */

export const SELECT_NEXT_PORT = 'onLand/PortAttentionEdit/SELECT_NEXT_PORT';
export const SELECT_NEXT_PORT_RESULT = 'onLand/PortAttentionEdit/SELECT_NEXT_PORT_RESULT';

export const CREATE_PORT_ATTENTION = 'onLand/PortAttentionEdit/CREATE_PORT_ATTENTION';
export const CREATE_PORT_ATTENTION_RESULT = 'onLand/PortAttentionEdit/CREATE_PORT_ATTENTION_RESULT';

export const UPDATE_PORT_ATTENTION = 'onLand/PortAttentionEdit/UPDATE_PORT_ATTENTION';
export const UPDATE_PORT_ATTENTION_RESULT = 'onLand/PortAttentionEdit/UPDATE_PORT_ATTENTION_RESULT';

export const CLEAN = 'onLand/PortAttentionEdit/CLEAN';
