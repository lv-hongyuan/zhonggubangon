import { createSelector } from 'reselect/es';

const selectEditPortAttentionDomain = () => state => state.portAttentionEdit;

const makeSelectNextPort = () => createSelector(selectEditPortAttentionDomain(), (subState) => {
  console.log(subState);
  return subState.portList || [];
});

const makeSelectPortAttentionNavBack = () => createSelector(selectEditPortAttentionDomain(), (subState) => {
  console.log(subState);
  return subState.navBack;
});

const makeIsLoading = () => createSelector(selectEditPortAttentionDomain(), (subState) => {
  console.log(subState);
  return subState.isLoading;
});

export {
  makeSelectNextPort,
  makeSelectPortAttentionNavBack,
  makeIsLoading,
};
