import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  InputGroup, Container, Content, View, Form, Item, Label,
} from 'native-base';
import {
  makeSelectPortAttentionNavBack,
  makeSelectNextPort,
  makeIsLoading,
} from './selectors';
import {
  getNextPortSelectAction,
  createPortAttentionAction,
  updatePortAttentionAction,
  cleanAction,
} from './actions';
import myTheme from '../../../../Themes';
import Selector from '../../../../components/Selector';
import InputItem from '../../../../components/InputItem';
import commonStyles from '../../../../common/commonStyles';
import screenHOC from '../../../../components/screenHOC';

@screenHOC
class PortAttentionEdit extends React.PureComponent {
  constructor(props) {
    super(props);

    const popId = props.navigation.getParam('id');
    const createPortName = props.navigation.getParam('portName');
    const item = props.navigation.getParam('item', {});

    this.state = {
      isEditing: item.id !== undefined && item.id !== null,
      data: {
        id: item.id,
        popId,
        createPortName,
        attentionPopId: item.attentionPopId,
        attentionMemo: item.attentionMemo, // 注意事项描述
        attentionPortName: item.attentionPortName, // 港口名称
      },
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.submit });
    this.props.dispatch(getNextPortSelectAction(this.state.data.popId));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navBack) {
      this.props.navigation.goBack();
    }
  }

  componentWillUnmount() {
    this.props.dispatch(cleanAction());
  }

  onPressPortName = () => {
    if (this.props.types.length > 0) {
      this.parentSelect.showPicker();
    } else {
      this.props.dispatch(getNextPortSelectAction(this.state.data.popId));
    }
  };

  onChangePortType = (item) => {
    if (this.state.data.attentionPopId !== item.id) {
      this.setData({
        attentionPopId: item.id,
        attentionPortName: item.text,
      });
    }
  };

  setData(params) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    });
  }

  submit = () => {
    const param = {
      id: this.state.data.id,
      createPopId: this.state.data.popId,
      createPortName: this.state.data.createPortName,
      attentionPopId: this.state.data.attentionPopId,
      attentionPortName: this.state.data.attentionPortName,
      attentionMemo: this.state.data.attentionMemo,
    };
    if (!this.props.isLoading) {
      if (this.state.isEditing) {
        this.props.dispatch(updatePortAttentionAction(param));
      } else {
        this.props.dispatch(createPortAttentionAction(param));
      }
    }
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={{ backgroundColor: 'white', marginTop: 8 }}>
            <Form style={{ paddingLeft: 20, paddingRight: 20, paddingBottom: 8 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>港口名称:</Label>
                  <Selector
                    value={this.state.data.attentionPortName}
                    items={this.props.types || []}
                    ref={(ref) => {
                      this.parentSelect = ref;
                    }}
                    onPress={this.onPressPortName}
                    pickerTitle="请选择港口名称"
                    getItemValue={item => item.id}
                    getItemText={item => item.text}
                    onSelected={this.onChangePortType}
                  />
                </Item>
              </InputGroup>

              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="注意事项描述:"
                  value={this.state.data.attentionMemo}
                  onChangeText={(text) => {
                    this.setData({ attentionMemo: text });
                  }}
                />
              </InputGroup>
            </Form>
          </View>
        </Content>
      </Container>
    );
  }
}

PortAttentionEdit.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('item', undefined) === undefined ? '新建' : '编辑',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="保存"
        buttonStyle={{ fontSize: 14, color: '#ffffff' }}
        onPress={navigation.getParam('submit')}
      />
    </HeaderButtons>
  ),
});

PortAttentionEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  navBack: PropTypes.bool,
  types: PropTypes.array,
  isLoading: PropTypes.bool,
};
PortAttentionEdit.defaultProps = {
  navBack: false,
  types: [],
  isLoading: false,
};

const mapStateToProps = createStructuredSelector({
  types: makeSelectNextPort(),
  isLoading: makeIsLoading(),
  navBack: makeSelectPortAttentionNavBack(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortAttentionEdit);
