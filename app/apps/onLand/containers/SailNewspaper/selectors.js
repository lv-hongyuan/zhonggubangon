import { createSelector } from 'reselect';

const selectSailNewspaperDomain = () => state => state.sailNewspaper;

const makeSelectSailNewspaperDetail = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.debug(subState);
  return subState.popDetail;
});

const makegetTugCostNewspaperDetail = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.debug(subState);
  return subState.defaultFee;
});

const makeSelectSailNewspaperLoading = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

const makeSelectSailNewspaperError = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.log(subState);
  return subState.error;
});

const makeSelectDefaultValues = () => createSelector(selectSailNewspaperDomain(), (subState) => {
  console.log(subState);
  return subState.defaultValues;
});

export {
  makegetTugCostNewspaperDetail,
  makeSelectSailNewspaperDetail,
  makeSelectSailNewspaperLoading,
  makeSelectSailNewspaperError,
  makeSelectDefaultValues,
};
