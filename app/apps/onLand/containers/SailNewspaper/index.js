import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, SafeAreaView ,TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import {
  Text, InputGroup, Container, Content, View, Form, Item, Label, Button,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import _ from 'lodash';
import SelectImageView from '../../../../components/SelectImageView';
import {
  getSailNewspaperDetailPromise,
  saveSailNewspaperDetailPromise,
  completeSailNewspaperDetailPromise,
  getTugCostPromise,
} from './actions';
import {
  makeSelectSailNewspaperDetail,
  makeSelectSailNewspaperLoading,
  makegetTugCostNewspaperDetail,
} from './selectors';
import myTheme from '../../../../Themes';
import ExtendCell from '../../../../components/ExtendCell/ExtendCell';
import InfoTable from './components/InfoTable';
import Switch from '../../../../components/Switch';
import { ROUTE_PORT_LOADING_INFO_EDIT } from '../../RouteConstant';
import InputItem from '../../../../components/InputItem';
import stringToNumber from '../../../../utils/stringToNumber';
import AlertView from '../../../../components/Alert';
import Selector from '../../../../components/Selector';
import screenHOC from '../../../../components/screenHOC';
import {
  lossCauseReasons,
  pilotReasons,
  tugMemoReasons,
  validPilotReason,
  validTugMemoReason,
} from '../../common/Constant';
import commonStyles from '../../../../common/commonStyles';
import { MaskType } from '../../../../components/InputItem/TextInput';

const styles = StyleSheet.create({
  leaveTons: {
    borderWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    padding: 5,
    marginLeft: -5,
    marginRight: -5,
    borderRadius: 5,
  },
  draughtContainer: {
    borderWidth: myTheme.borderWidth,
    borderRadius: 3,
    flex: 1,
    padding: 5,
    borderColor: myTheme.borderColor,
    margin: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    backgroundColor: '#E0E0E0',
    height: 50,
  },
  saveButton: {
    height: '100%',
    backgroundColor: '#FBB03B',
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
  },
  completeButton: {
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    flex: 2,
    justifyContent: 'center',
  },
  overlay: {
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  draughtfullbutton:{
    height:28,
    width:'35%',
    flexDirection:'row',
    alignItems:'center',
    paddingLeft:5,
    marginBottom:2
  },
  draughtfulltext:{
    fontSize: 15,
    color: '#535353',
    marginLeft:5
  },
  draughtfull:{
    height:100,
    width:'90%',
    justifyContent:'space-between',
    flexDirection:'row',
    flexWrap:'wrap',
    padding:5
  },
  billing:{
    width:75,
    height:30,
    backgroundColor:'#DC001B',
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    top:0,
    right:0,
    borderRadius:5
  }
});

@screenHOC
class SailNewspaper extends React.PureComponent {
  static loadDetailData(popDetail) {
    const data = popDetail || {};
    return {
      e20Load: data.e20Load || 0,
      f20Load: data.f20Load || 0,
      e40Load: data.e40Load || 0,
      f40Load: data.f40Load || 0,
      e40HLoad: data.e40HLoad || 0,
      f40HLoad: data.f40HLoad || 0,
      eOtherLoad: data.eOtherLoad || 0,
      fOtherLoad: data.fOtherLoad || 0,
      e20UnLoad: data.e20UnLoad || 0,
      f20UnLoad: data.f20UnLoad || 0,
      e40UnLoad: data.e40UnLoad || 0,
      f40UnLoad: data.f40UnLoad || 0,
      e40HUnLoad: data.e40HUnLoad || 0,
      f40HUnLoad: data.f40HUnLoad || 0,
      eOtherUnLoad: data.eOtherUnLoad || 0,
      fOtherUnLoad: data.fOtherUnLoad || 0,
      e20Dx: data.e20Dx || 0,
      f20Dx: data.f20Dx || 0,
      e40Dx: data.e40Dx || 0,
      f40Dx: data.f40Dx || 0,
      e40HDx: data.e40HDx || 0,
      f40HDx: data.f40HDx || 0,
      eOtherDx: data.eOtherDx || 0,
      fOtherDx: data.fOtherDx || 0,
      e20Tx: data.e20Tx || 0,
      f20Tx: data.f20Tx || 0,
      e40Tx: data.e40Tx || 0,
      f40Tx: data.f40Tx || 0,
      e40HTx: data.e40HTx || 0,
      f40HTx: data.f40HTx || 0,
      eOtherTx: data.eOtherTx || 0,
      fOtherTx: data.fOtherTx || 0,

      id: data.id,
      pilotMemoBerth: data.pilotMemoBerth,
      state: data.state,
      cableFee: data.cableFee,
      popId: data.popId,
      memoDx: data.memoDx,
      fullLoad: data.fullLoad,
      pilotLeave: data.pilotLeave,
      tugNumBerth: data.tugNumBerth,
      pilotMemoLeave: data.pilotMemoLeave,
      otherFee: data.otherFee,                //其他费
      tugFeeBerth: data.tugFeeBerth,
      pilotFeeLeave: data.pilotFeeLeave,
      pilotBerth: data.pilotBerth,
      tugMemoLeave: data.tugMemoLeave,
      waterFee: data.waterFee,
      parkingFee: data.parkingFee,
      feeMemo: data.feeMemo,
      tugMemoBerth: data.tugMemoBerth,
      tugNumLeave: data.tugNumLeave,
      version: data.version,
      expectedLoss: data.expectedLoss,
      openDoorFee: data.openDoorFee,
      pilotFeeBerth: data.pilotFeeBerth,
      memoTx: data.memoTx,
      tugFeeLeave: data.tugFeeLeave,
      agencyFee: data.agencyFee,
      lossCause: data.lossCause,
      loadTon: data.loadTon,
      unLoadTon: data.unLoadTon,

      f20: data.f20,
      e20: data.e20,
      f40: data.f40,
      e40: data.e40,
      goodsTon: data.goodsTon,

      parkTime: _.isNumber(data.parkTime) ? data.parkTime.toFixed(1) : undefined,
      netTon: data.netTon,

      draughtFront: data.draughtFront,
      draughtMid: data.draughtMid,
      draughtAfter: data.draughtAfter,
      // 船舶吃水前图片
      draughtFrontPic: data.draughtFrontPic,
      // 船舶吃水中图片
      draughtMidPic: data.draughtMidPic,
      // 船舶吃水后图片
      draughtAfterPic: data.draughtAfterPic,
      gm: data.gm,
      draughtFull: data.draughtFull,
      // 考核吃水
      refDraught: parseFloat(data.refDraught, 10) || 0,
      summerFullLoadDrink:data.summerFullLoadDrink,   //夏季满载吃水
      summerFreshWaterDrink:data.summerFreshWaterDrink,   //夏季淡水吃水
      tropicFullLoadDrink:data.tropicFullLoadDrink,    //热带满载吃水
      tropicFreshWaterDrink:data.tropicFreshWaterDrink,  //热带淡水吃水
      zzd:data.zzd,                                      //载重吨
      clearanceFee:data.clearanceFee                    //清道费
    };
  }

  constructor(props) {
    super(props);

    const data = SailNewspaper.loadDetailData(this.props.popDetail);
    this.state = {
      popId: this.props.navigation.getParam('popId'),
      data: this.calculateParkingFee(data),
      inputEditable:true,
      buttonone:false,
      buttontwo:false,
      buttomthree:false,
      buttonfour:false,
      buttonfive:true
    };
  }

  componentDidMount() {
    this.loadDetail();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.popDetail !== nextProps.popDetail) {
      const data = SailNewspaper.loadDetailData(nextProps.popDetail);
      this.setState({ data: this.calculateParkingFee(data) });
    }
  }

  onPressInfoTable = () => {
    this.props.navigation.navigate(ROUTE_PORT_LOADING_INFO_EDIT, {
      tableData: this.state.data,
      popId: this.props.navigation.getParam('popId'),
      shipName: this.props.navigation.getParam('shipName'),
      voyageCode: this.props.navigation.getParam('voyageCode'),
      portName: this.props.navigation.getParam('portName'),
      onBack: (data) => {
        const newDetail = this.calculateParkingFee({ ...this.state.data, ...data }, true);
        this.props.saveSailNewspaperDetailPromise(newDetail)
          .then(() => {
          })
          .catch(() => {
          });
      },
    });
  };

  setData(params, callback = () => {}) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    }, callback);
  }

  calculateParkingFee = (detail, force = false) => {
    // 设置停泊费
    if (_.isNil(detail.parkingFee) || detail.parkingFee === 0 || force) {
      /**
       * | 港口     正常费率        只装/只卸（根据录入的装/卸箱量是否都为0判断）
       * | 其他     0.08*净吨*天数  0.08*净吨*天数
       * | 广州新港  0.08*净吨*天数  0.04*净吨*天数
       * | 东江仓   0.08*净吨*天数  0.04*净吨*天数
       * | 广州新港与东江仓，无论停泊多久，都按一天计算(追加)
       */
        // 港口名
      const portName = this.props.navigation.getParam('portName');
      // 净吨
      const { netTon = 0 } = detail;

      const { parkTime = 0 } = detail;
      // 天数
      const days = (portName === '广州新港' || portName === '东江仓') ? 1 : Math.ceil(parkTime / 24);

      // 是否正常/（只装/只卸）
      const {
        e20Load, f20Load, e40Load, f40Load, e40HLoad, f40HLoad, eOtherLoad, fOtherLoad,
        e20UnLoad, f20UnLoad, e40UnLoad, f40UnLoad, e40HUnLoad, f40HUnLoad, eOtherUnLoad, fOtherUnLoad,
      } = detail;

      // 判断装箱数是否大于0
      let hasLoad = [e20Load, f20Load, e40Load, f40Load, e40HLoad, f40HLoad, eOtherLoad, fOtherLoad].reduce((a, b) => (a || 0) + (b || 0), 0) > 0;
      // 判断卸箱数是否大于0
      let hasUnLoad = [e20UnLoad, f20UnLoad, e40UnLoad, f40UnLoad, e40HUnLoad, f40HUnLoad, eOtherUnLoad, fOtherUnLoad].reduce((a, b) => (a || 0) + (b || 0), 0) > 0;
      // 装卸中某一个不大于0说明是只装或者只卸操作，那么在广州新港与东江仓，费率为0.04
      let isNormal = hasLoad && hasUnLoad;

      // 费率系数
      let coefficient = (!isNormal && (portName === '广州新港' || portName === '东江仓')) ? 0.04 : 0.08;

      detail.parkingFee = coefficient * netTon * days;
    }

    return detail;
  };

  loadDetail = () => {
    this.props.getSailNewspaperDetailPromise(this.state.popId)
      .then(() => {})
      .catch(() => {});
  };

  paramFromData() {
    return {
      e20Load: this.state.data.e20Load,
      f20Load: this.state.data.f20Load,
      e40Load: this.state.data.e40Load,
      f40Load: this.state.data.f40Load,
      e40HLoad: this.state.data.e40HLoad,
      f40HLoad: this.state.data.f40HLoad,
      eOtherLoad: this.state.data.eOtherLoad,
      fOtherLoad: this.state.data.fOtherLoad,
      e20UnLoad: this.state.data.e20UnLoad,
      f20UnLoad: this.state.data.f20UnLoad,
      e40UnLoad: this.state.data.e40UnLoad,
      f40UnLoad: this.state.data.f40UnLoad,
      e40HUnLoad: this.state.data.e40HUnLoad,
      f40HUnLoad: this.state.data.f40HUnLoad,
      eOtherUnLoad: this.state.data.eOtherUnLoad,
      fOtherUnLoad: this.state.data.fOtherUnLoad,
      e20Dx: this.state.data.e20Dx,
      f20Dx: this.state.data.f20Dx,
      e40Dx: this.state.data.e40Dx,
      f40Dx: this.state.data.f40Dx,
      e40HDx: this.state.data.e40HDx,
      f40HDx: this.state.data.f40HDx,
      eOtherDx: this.state.data.eOtherDx,
      fOtherDx: this.state.data.fOtherDx,
      e20Tx: this.state.data.e20Tx,
      f20Tx: this.state.data.f20Tx,
      e40Tx: this.state.data.e40Tx,
      f40Tx: this.state.data.f40Tx,
      e40HTx: this.state.data.e40HTx,
      f40HTx: this.state.data.f40HTx,
      eOtherTx: this.state.data.eOtherTx,
      fOtherTx: this.state.data.fOtherTx,

      id: this.state.data.id,
      pilotMemoBerth: this.state.data.pilotMemoBerth,
      state: this.state.data.state,
      cableFee: stringToNumber(this.state.data.cableFee),
      popId: this.state.data.popId,
      memoDx: this.state.data.memoDx,
      fullLoad: this.state.data.fullLoad,
      pilotLeave: this.state.data.pilotLeave,
      tugNumBerth: stringToNumber(this.state.data.tugNumBerth),
      pilotMemoLeave: this.state.data.pilotMemoLeave,
      otherFee: stringToNumber(this.state.data.otherFee),
      tugFeeBerth: stringToNumber(this.state.data.tugFeeBerth),
      pilotFeeLeave: this.state.data.pilotFeeLeave,
      pilotBerth: this.state.data.pilotBerth,
      tugMemoLeave: stringToNumber(this.state.data.tugMemoLeave),
      waterFee: stringToNumber(this.state.data.waterFee),
      parkingFee: stringToNumber(this.state.data.parkingFee),
      feeMemo: this.state.data.feeMemo,
      tugMemoBerth: this.state.data.tugMemoBerth,
      tugNumLeave: stringToNumber(this.state.data.tugNumLeave),
      version: this.state.data.version,
      expectedLoss: stringToNumber(this.state.data.expectedLoss),
      openDoorFee: stringToNumber(this.state.data.openDoorFee),
      pilotFeeBerth: stringToNumber(this.state.data.pilotFeeBerth),
      memoTx: this.state.data.memoTx,
      tugFeeLeave: stringToNumber(this.state.data.tugFeeLeave),
      agencyFee: stringToNumber(this.state.data.agencyFee),
      lossCause: this.state.data.lossCause,
      loadTon: stringToNumber(this.state.data.loadTon),
      unLoadTon: stringToNumber(this.state.data.unLoadTon),

      f20: stringToNumber(this.state.data.f20),
      e20: stringToNumber(this.state.data.e20),
      f40: stringToNumber(this.state.data.f40),
      e40: stringToNumber(this.state.data.e40),
      goodsTon: stringToNumber(this.state.data.goodsTon),

      draughtFront: stringToNumber(this.state.data.draughtFront),
      draughtMid: stringToNumber(this.state.data.draughtMid),
      draughtAfter: stringToNumber(this.state.data.draughtAfter),
      draughtFrontPic: this.state.data.draughtFrontPic,
      draughtMidPic: this.state.data.draughtMidPic,
      draughtAfterPic: this.state.data.draughtAfterPic,
      gm: stringToNumber(this.state.data.gm),
      draughtFull: stringToNumber(this.state.data.draughtFull),
      clearanceFee: stringToNumber(this.state.data.clearanceFee),
      zzd:stringToNumber(this.state.data.zzd),
    };
  }

  savePopDetail = () => {
    const param = this.paramFromData();
    this.props.saveSailNewspaperDetailPromise(param)
      .then(() => {
      })
      .catch(() => {
      });
  };

  verifyParam = (param) => {
    if (param.lossCause === '卸空下线') {
      return null;
    }

    const loadInfo = {
      e20Load: param.e20Load,
      f20Load: param.f20Load,
      e40Load: param.e40Load,
      f40Load: param.f40Load,
      e40HLoad: param.e40HLoad,
      f40HLoad: param.f40HLoad,
      eOtherLoad: param.eOtherLoad,
      fOtherLoad: param.fOtherLoad,
      e20UnLoad: param.e20UnLoad,
      f20UnLoad: param.f20UnLoad,
      e40UnLoad: param.e40UnLoad,
      f40UnLoad: param.f40UnLoad,
      e40HUnLoad: param.e40HUnLoad,
      f40HUnLoad: param.f40HUnLoad,
      eOtherUnLoad: param.eOtherUnLoad,
      fOtherUnLoad: param.fOtherUnLoad,
      e20Dx: param.e20Dx,
      f20Dx: param.f20Dx,
      e40Dx: param.e40Dx,
      f40Dx: param.f40Dx,
      e40HDx: param.e40HDx,
      f40HDx: param.f40HDx,
      eOtherDx: param.eOtherDx,
      fOtherDx: param.fOtherDx,
      e20Tx: param.e20Tx,
      f20Tx: param.f20Tx,
      e40Tx: param.e40Tx,
      f40Tx: param.f40Tx,
      e40HTx: param.e40HTx,
      f40HTx: param.f40HTx,
      eOtherTx: param.eOtherTx,
      fOtherTx: param.fOtherTx,
    };
    const total = Object.values(loadInfo)
      .reduce((a, b) => a + b, 0);
    if (total === 0) {
      return '装载信息不能都为0';
    }
    if (_.isNil(param.loadTon)) {
      return '请填写装货吨';
    }
    if (_.isNil(param.unLoadTon)) {
      return '请填写卸货吨';
    }
    if (_.isNil(param.f20)) {
      return '请填写F20';
    }
    if (_.isNil(param.e20)) {
      return '请填写E20';
    }
    if (_.isNil(param.f40)) {
      return '请填写F40';
    }
    if (_.isNil(param.e40)) {
      return '请填写E40';
    }
    if (_.isNil(param.goodsTon)) {
      return '请填写货物吨数';
    }
    if (param.fullLoad !== 1) {
      if (!param.expectedLoss) {
        return '请填写亏吨预估';
      }
      if (!param.lossCause) {
        return '请填写未满原因';
      }
    }
    if (_.isNil(param.draughtFront)) {
      return '请填写吃水前';
    }
    if (_.isNil(param.draughtMid)) {
      return '请填写吃水中';
    }
    if (_.isNil(param.draughtAfter)) {
      return '请填写吃水后';
    }
    if (_.isNil(param.draughtFrontPic)) {
      return '请填写吃水前照片';
    }
    if (_.isNil(param.draughtMidPic)) {
      return '请填写吃水中照片';
    }
    if (_.isNil(param.draughtAfterPic)) {
      return '请填写吃水后照片';
    }
    if (_.isNil(param.gm)) {
      return '请填写GM值';
    }
    if (_.isNil(param.draughtFull)) {
      return '请填写满载吃水';
    }
    if (this.state.data.pilotBerth === 1 || this.state.data.pilotLeave === 1) {
      if (!param.pilotFeeBerth) {
        return '请填写引水费';
      }
    }
    if (_.isNil(param.cableFee)) {
      return '请填写解系缆';
    }
    if (_.isNil(param.parkingFee)) {
      return '请填写停泊费';
    }
    if (_.isNil(param.agencyFee)) {
      return '请填写代理费';
    }
    if (_.isNil(param.waterFee)) {
      return '请填写淡水费';
    }
    if (_.isNil(param.otherFee)) {
      return '请填写其它费';
    }
    return null;
  };

  completePopDetail = () => {
    const param = this.paramFromData();
    let message = this.verifyParam(param);
    if (message) {
      AlertView.show({ message });
      return;
    }

    if (param.f20 === 0 && param.e20 === 0 && param.f40 === 0 && param.e40 === 0 && param.goodsTon === 0) {
      AlertView.show({
        message: '离港在船货量信息未填写\n(货量信息都为0)\n确认要提交吗？',
        showCancel: true,
        confirmAction: () => {
          AlertView.show({
            message: '确定后的数据将无法修改，确认要提交吗？',
            showCancel: true,
            confirmAction: () => {
              this.props.completeSailNewspaperDetailPromise(param)
                .then(() => {
                  this.props.navigation.goBack();
                })
                .catch(() => {
                });
            },
          });
        },
      });
    } else {
      AlertView.show({
        message: '确定后的数据将无法修改，确认要提交吗？',
        showCancel: true,
        confirmAction: () => {
          this.props.completeSailNewspaperDetailPromise(param)
            .then(() => {
              this.props.navigation.goBack();
            })
            .catch(() => {
            });
        },
      });
    }
  };

  renderLoadingInformation() {
    return (
      <ExtendCell
        title="装载信息"
        titleColor="#DC001B"
        isOpen
        style={{ marginTop: 8 }}
      >
        <InfoTable
          tableData={this.state.data}
          onPress={this.onPressInfoTable}
          style={{
            marginLeft: -30,
            marginRight: -30,
          }}
        />
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="倒箱备注:"
            value={this.state.data.memoDx}
            onChangeText={(text) => {
              this.setData({ memoDx: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="特殊作业备注:"
            value={this.state.data.memoTx}
            onChangeText={(text) => {
              this.setData({ memoTx: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="装货吨:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.loadTon}
            onChangeText={(text) => {
              this.setData({ loadTon: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="卸货吨:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.unLoadTon}
            onChangeText={(text) => {
              this.setData({ unLoadTon: text });
            }}
          />
        </InputGroup>
        <View style={styles.leaveTons}>
          <Text style={commonStyles.inputLabel}>船舶离港在船总货量</Text>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              isRequired
              label="F20:"
              keyboardType="numeric"
              maskType={MaskType.INTEGER}
              value={this.state.data.f20}
              onChangeText={(text) => {
                this.setData({ f20: text });
              }}
            />
            <InputItem
              isRequired
              label="E20:"
              keyboardType="numeric"
              maskType={MaskType.INTEGER}
              value={this.state.data.e20}
              onChangeText={(text) => {
                this.setData({ e20: text });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              isRequired
              label="F40:"
              keyboardType="numeric"
              maskType={MaskType.INTEGER}
              value={this.state.data.f40}
              onChangeText={(text) => {
                this.setData({ f40: text });
              }}
            />
            <InputItem
              isRequired
              label="E40:"
              keyboardType="numeric"
              maskType={MaskType.INTEGER}
              value={this.state.data.e40}
              onChangeText={(text) => {
                this.setData({ e40: text });
              }}
            />
          </InputGroup>
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              isRequired
              label="货物吨数:"
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              value={this.state.data.goodsTon}
              onChangeText={(text) => {
                this.setData({ goodsTon: text });
              }}
              rightItem={<Text style={commonStyles.tail}>吨</Text>}
            />
          </InputGroup>
        </View>
        <InputGroup style={commonStyles.inputGroup}>
          <Item style={commonStyles.inputItem}>
            <Label style={commonStyles.inputLabel}>满载:</Label>
            <Switch
              value={this.state.data.fullLoad === 1}
              onPress={() => {
                this.setData({ fullLoad: this.state.data.fullLoad === 1 ? 0 : 1 });
              }}
              style={{
                height: 30,
                width: 60,
              }}
            />
          </Item>
        </InputGroup>
        {this.state.data.fullLoad === 1 ? null : (
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="亏吨预估:"
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              value={this.state.data.expectedLoss}
              onChangeText={(text) => {
                this.setData({ expectedLoss: text });
              }}
            />
          </InputGroup>
        )}
        {this.state.data.fullLoad === 1 ? null : (
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>未满原因:</Label>
              <Selector
                value={this.state.data.lossCause}
                items={lossCauseReasons}
                getItemValue={item => item}
                getItemText={item => item}
                ref={(ref) => {
                  this.parentSelect = ref;
                }}
                pickerTitle="未满原因"
                onSelected={(item) => {
                  this.setData({ lossCause: item });
                }}
              />
            </Item>
          </InputGroup>
        )}
        <View style={{
          flexDirection: 'row',
          marginLeft: -5,
          marginRight: -5,
        }}
        >
          <View style={styles.draughtContainer}>
            <View style={{
              height: 30,
              width: '100%',
            }}
            >
              <InputItem
                label="吃水前:"
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                value={this.state.data.draughtFront}
                onChangeText={(text) => {
                  this.setData({ draughtFront: text });
                }}
              />
            </View>
            <Label style={[commonStyles.inputLabel, { padding: 4 }]}>
              图&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片:
            </Label>
            <SelectImageView
              style={{
                // marginTop: 10,
                width: 100,
                height: 100,
                marginLeft: 0,
              }}
              title="选择照片"
              source={this.state.data.draughtFrontPic}
              onPickImages={(images) => {
                const image = images[0];
                this.setData({
                  draughtFrontPic: {
                    fileName: image.uri.split('/')
                      .reverse()[0],
                    fileSize: image.size,
                    path: image.uri,
                    uri: image.uri,
                  },
                });
              }}
              onDelete={() => {
                this.setData({
                  draughtFrontPic: undefined,
                });
              }}
            />
          </View>
          <View style={styles.draughtContainer}>
            <View style={{
              height: 30,
              width: '100%',
            }}
            >
              <InputItem
                label="吃水中:"
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                value={this.state.data.draughtMid}
                onChangeText={(text) => {
                  this.setData({ draughtMid: text });
                }}
                onBlur={() => {
                  const draughtMid = parseFloat(this.state.data.draughtMid, 10) || 0;
                  if (draughtMid < this.state.data.refDraught) {
                    AlertView.show({
                      message: '当前处于亏吨状态',
                    });
                  }
                }}
              />
            </View>
            <Label style={[commonStyles.inputLabel, { padding: 4 }]}>
              图&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片:
            </Label>
            <SelectImageView
              style={{
                // marginTop: 10,
                width: 100,
                height: 100,
                marginLeft: 0,
              }}
              title="选择照片"
              source={this.state.data.draughtMidPic}
              onPickImages={(images) => {
                const image = images[0];
                this.setData({
                  draughtMidPic: {
                    fileName: image.uri.split('/')
                      .reverse()[0],
                    fileSize: image.size,
                    path: image.uri,
                    uri: image.uri,
                  },
                });
              }}
              onDelete={() => {
                this.setData({
                  draughtMidPic: undefined,
                });
              }}
            />
          </View>
        </View>
        <View style={{
          flexDirection: 'row',
          marginLeft: -5,
          marginRight: -5,
        }}
        >
          <View style={styles.draughtContainer}>
            <View style={{
              height: 30,
              width: '100%',
            }}
            >
              <InputItem
                label="吃水后:"
                keyboardType="numeric"
                maskType={MaskType.FLOAT}
                value={this.state.data.draughtAfter}
                onChangeText={(text) => {
                  this.setData({ draughtAfter: text });
                }}
              />
            </View>
            <Label style={[commonStyles.inputLabel, { padding: 4 }]}>
              图&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;片:
            </Label>
            <SelectImageView
              style={{
                // marginTop: 10,
                width: 100,
                height: 100,
                marginLeft: 0,
              }}
              title="选择照片"
              source={this.state.data.draughtAfterPic}
              onPickImages={(images) => {
                const image = images[0];
                this.setData({
                  draughtAfterPic: {
                    fileName: image.uri.split('/')
                      .reverse()[0],
                    fileSize: image.size,
                    path: image.uri,
                    uri: image.uri,
                  },
                });
              }}
              onDelete={() => {
                this.setData({
                  draughtAfterPic: undefined,
                });
              }}
            />
          </View>
          <View style={{
            flex: 1,
            margin: 5,
            padding: 5,
          }}
          />
        </View>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="GM值:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.gm}
            onChangeText={(text) => {
              this.setData({ gm: text });
            }}
          />
          <InputItem
            label="满载吃水:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.draughtFull}
            onChangeText={(text) => {
              this.setData({ draughtFull: text });
            }}
            editable = {this.state.inputEditable ? true : false}
            placeholder = {'请输入'}
          />
        </InputGroup>
        <View style={styles.draughtfull}>
            <TouchableOpacity activeOpacity={0.8} style={styles.draughtfullbutton}
              onPress={()=>{
                this.setData({draughtFull: this.state.data.summerFullLoadDrink});
                this.setState({ 
                  inputEditable:false,
                  buttonone:true,
                  buttontwo:false,
                  buttomthree:false,
                  buttonfour:false,
                  buttonfive:false
                })
              }}
            >
              <Ionicons
                name={this.state.buttonone ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={15}
                color={'#DC001B'} >
              </Ionicons>
              <Text style={styles.draughtfulltext}>夏季海水</Text>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={styles.draughtfullbutton}
              onPress={()=>{
                this.setData({draughtFull: this.state.data.summerFreshWaterDrink});
                this.setState({ 
                  inputEditable:false,
                  buttonone:false,
                  buttontwo:true,
                  buttomthree:false,
                  buttonfour:false,
                  buttonfive:false
                })
              }}
            >
              <Ionicons
                name={this.state.buttontwo ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={15}
                color={'#DC001B'} >
              </Ionicons>
              <Text style={styles.draughtfulltext}>夏季淡水</Text>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={styles.draughtfullbutton}
              onPress={()=>{
                this.setData({draughtFull: this.state.data.tropicFullLoadDrink});
                this.setState({ 
                  inputEditable:false,
                  buttonone:false,
                  buttontwo:false,
                  buttomthree:true,
                  buttonfour:false,
                  buttonfive:false
                })
              }}
            >
              <Ionicons
                name={this.state.buttomthree ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={15}
                color={'#DC001B'} >
              </Ionicons>
              <Text style={styles.draughtfulltext}>热带海水</Text>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={styles.draughtfullbutton}
              onPress={()=>{
                this.setData({draughtFull: this.state.data.tropicFreshWaterDrink});
                this.setState({ 
                  inputEditable:false,
                  buttonone:false,
                  buttontwo:false,
                  buttomthree:false,
                  buttonfour:true,
                  buttonfive:false
                })
              }}
            >
              <Ionicons
                name={this.state.buttonfour ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={15}
                color={'#DC001B'} >
              </Ionicons>
              <Text style={styles.draughtfulltext}>热带淡水</Text>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} style={styles.draughtfullbutton}
              onPress={()=>{
                this.setData({ draughtFull: '' });
                this.setState({ 
                  inputEditable:true,
                  buttonone:false,
                  buttontwo:false,
                  buttomthree:false,
                  buttonfour:false,
                  buttonfive:true
                })
              }}
            >
              <Ionicons
                name={this.state.buttonfive ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                size={15}
                color={'#DC001B'} >
              </Ionicons>
              <Text style={styles.draughtfulltext}>其他</Text>
            </TouchableOpacity>
        </View>
      </ExtendCell>
    );
  }

  //计算托轮费
  getTugCost(){
    if(!this.state.data.tugMemoBerth){
      AlertView.show({
        message: '请选择拖轮说明',
        showCancel: false,
        confirmAction: () => {},
      });
    }else{
      const param = {
        shipName:this.props.navigation.getParam('shipName'),
        portName: this.props.navigation.getParam('portName'),
        berTugNum: stringToNumber(this.state.data.tugNumBerth),
        unberTugNum: stringToNumber(this.state.data.tugNumLeave),
      }
      this.props.getTugCostPromise(param)
        .then(() => {
          this.setData({ tugFeeBerth: this.props.defaultFee });
        })
        .catch(() => {});
    }
  }

  renderCostInformation() {
    const portName = this.props.navigation.getParam('portName');
    return (
      <ExtendCell
        title="费用信息"
        titleColor="#DC001B"
        isOpen
        style={{ marginTop: 8 }}
      >
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="靠泊拖轮数:"
            keyboardType="numeric"
            maskType={MaskType.INTEGER}
            value={this.state.data.tugNumBerth}
            onChangeText={(text) => {
              this.setData({ tugNumBerth: text });
            }}
          />
          <InputItem
            label="离泊拖轮数:"
            keyboardType="numeric"
            maskType={MaskType.INTEGER}
            value={this.state.data.tugNumLeave}
            onChangeText={(text) => {
              this.setData({ tugNumLeave: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <Item style={commonStyles.inputItem}>
            <Label style={commonStyles.inputLabel}>拖轮说明:</Label>
            <Selector
              value={validTugMemoReason(this.state.data.tugMemoBerth)}
              items={tugMemoReasons}
              ref={(ref) => {
                this.parentSelect = ref;
              }}
              pickerTitle="拖轮说明"
              onSelected={({ value }) => {
                this.setData({ tugMemoBerth: value });
              }}
            />
          </Item>
        </InputGroup>
        <InputGroup style={[commonStyles.inputGroup,{flexDirection:'row'}]}>
          <InputItem
            label="拖轮费:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.tugFeeBerth}
            onChangeText={(text) => {
              this.setData({ tugFeeBerth: text });
            }}
          />
          {this.state.data.state !== 1 ? <TouchableOpacity 
            activeOpacity={0.8} 
            style={{
              width:75,height:30, backgroundColor:'#DC001B',
              justifyContent:'center',alignItems:'center',
              position:'absolute',right:0,top:0,borderRadius:5
            }}
            onPress={()=>{this.getTugCost()}}
          >
            <Text style={{color:'#fff',fontSize:14}}>一键计费</Text>
          </TouchableOpacity> : null}
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <Item style={commonStyles.inputItem}>
            <Label style={commonStyles.inputLabel}>靠泊引水:</Label>
            <Switch
              value={this.state.data.pilotBerth === 1}
              onPress={() => {
                this.setData({ pilotBerth: this.state.data.pilotBerth === 1 ? 0 : 1 });
              }}
              style={{
                height: 30,
                width: 60,
              }}
            />
          </Item>
          <View style={{ width: 20 }} />
          <Item style={commonStyles.inputItem}>
            <Label style={commonStyles.inputLabel}>离泊引水:</Label>
            <Switch
              value={this.state.data.pilotLeave === 1}
              onPress={() => {
                this.setData({ pilotLeave: this.state.data.pilotLeave === 1 ? 0 : 1 });
              }}
              style={{
                height: 30,
                width: 60,
              }}
            />
          </Item>
        </InputGroup>
        {this.state.data.pilotBerth === 1 || this.state.data.pilotLeave === 1 ? (
          <InputGroup style={commonStyles.inputGroup}>
            <InputItem
              label="引水费:"
              keyboardType="numeric"
              maskType={MaskType.FLOAT}
              value={this.state.data.pilotFeeBerth}
              onChangeText={(text) => {
                this.setData({ pilotFeeBerth: text });
              }}
            />
          </InputGroup>
        ) : null}
        {this.state.data.pilotBerth === 1 || this.state.data.pilotLeave === 1 ? (
          <InputGroup style={commonStyles.inputGroup}>
            <Item style={commonStyles.inputItem}>
              <Label style={commonStyles.inputLabel}>引水说明:</Label>
              <Selector
                value={validPilotReason(this.state.data.pilotMemoBerth)}
                items={pilotReasons}
                ref={(ref) => {
                  this.parentSelect = ref;
                }}
                pickerTitle="引水说明"
                onSelected={({ value }) => {
                  this.setData({ pilotMemoBerth: value });
                }}
              />
            </Item>
          </InputGroup>
        ) : null}
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="开关舱数:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.openDoorFee}
            onChangeText={(text) => {
              this.setData({ openDoorFee: text });
            }}
          />
          <InputItem
            label="解系缆:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.cableFee}
            onChangeText={(text) => {
              this.setData({ cableFee: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="停泊时间:"
            keyboardType="numeric"
            editable={false}
            maskType={MaskType.FLOAT}
            value={this.state.data.parkTime}
            rightItem={<Text style={commonStyles.tail}>小时</Text>}
          />
          <InputItem
            label="停泊费:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.parkingFee}
            onChangeText={(text) => {
              this.setData({ parkingFee: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="代理费:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.agencyFee}
            onChangeText={(text) => {
              this.setData({ agencyFee: text });
            }}
          />
          <InputItem
            label="淡水费:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.waterFee}
            onChangeText={(text) => {
              this.setData({ waterFee: text });
            }}
          />
        </InputGroup>
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="其它费:"
            keyboardType="numeric"
            maskType={MaskType.FLOAT}
            value={this.state.data.otherFee}
            onChangeText={(text) => {
              this.setData({ otherFee: text });
            }}
          />
        </InputGroup>
        {portName == '东江仓' && this.state.data.state !== 1 ? <InputGroup style={[commonStyles.inputGroup,{flexDirection:'row'}]}>
          <InputItem
            label="清道费:"
            value={this.state.data.clearanceFee}
            onChangeText={(text) => {
              this.setData({ clearanceFee: text });
            }}
            editable={false}
          />
          <TouchableOpacity 
            activeOpacity={0.8} 
            style={styles.billing}
            onPress={()=>{
              this.ComputationalCost()
            }}
          > 
            <Text style={{color:'#fff',fontSize:14,}}>一键计费</Text>
          </TouchableOpacity>
        </InputGroup> : null}
        <InputGroup style={commonStyles.inputGroup}>
          <InputItem
            label="费用备注:"
            value={this.state.data.feeMemo}
            onChangeText={(text) => {
              this.setData({ feeMemo: text });
            }}
          />
        </InputGroup>
      </ExtendCell>
    );
  }

  //一键计费
  ComputationalCost(){
    let data = this.state.data
    let money = 0
    let tugNumBerth = data.tugNumBerth ? data.tugNumBerth : 0
    let tugNumLeave = data.tugNumLeave ? data.tugNumLeave : 0
    console.log(tugNumBerth,tugNumLeave,data.zzd);
    if(data.zzd >= 12500){
      if(tugNumLeave > 0 && tugNumBerth > 0) money = 3300
      else if(tugNumLeave > 0 && tugNumBerth == 0) money = 1615
      else if(tugNumLeave == 0 && tugNumBerth > 0) money = 1615
    }
    console.log('清道费：',money);
    this.setData({ clearanceFee: money});
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <Form style={{ paddingBottom: this.state.data.state !== 1 ? 60 : 0 }}>
            {this.renderLoadingInformation()}
            {this.renderCostInformation()}
            <Text style={{
              textAlign: 'center',
              padding: 8,
              fontSize: 12,
              color: '#969696',
            }}
            >
              ———— 已经到底了 ————
            </Text>
          </Form>
          {this.state.data.state !== 1 ? null : <View style={styles.overlay} />}
        </Content>
        {
          this.state.data.state !== 1 ? (
            <SafeAreaView style={{
              position: 'absolute',
              bottom: 0,
              left: 0,
              right: 0,
              backgroundColor: '#ffffff',
            }}
            >
              <View style={styles.buttonContainer}>
                <Button onPress={this.savePopDetail} style={styles.saveButton}>
                  <Text>暂存</Text>
                </Button>
                <Button onPress={this.completePopDetail} style={styles.completeButton}>
                  <Text>提交</Text>
                </Button>
              </View>
            </SafeAreaView>
          ) : null
        }
      </Container>
    );
  }
}

SailNewspaper.navigationOptions = {
  title: '开航报信息',
};

SailNewspaper.propTypes = {
  navigation: PropTypes.object.isRequired,
  getSailNewspaperDetailPromise: PropTypes.func.isRequired,
  getTugCostPromise: PropTypes.func.isRequired,
  saveSailNewspaperDetailPromise: PropTypes.func.isRequired,
  completeSailNewspaperDetailPromise: PropTypes.func.isRequired,
  popDetail: PropTypes.object,
  // eslint-disable-next-line react/no-unused-prop-types
  isLoading: PropTypes.bool,
  defaultFee: PropTypes.number
};
SailNewspaper.defaultProps = {
  popDetail: {},
  isLoading: false,
  defaultFee : 0,
};

const mapStateToProps = createStructuredSelector({
  popDetail: makeSelectSailNewspaperDetail(),
  defaultFee: makegetTugCostNewspaperDetail(),
  isLoading: makeSelectSailNewspaperLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSailNewspaperDetailPromise,
      saveSailNewspaperDetailPromise,
      completeSailNewspaperDetailPromise,
      getTugCostPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SailNewspaper);
