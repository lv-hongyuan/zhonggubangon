import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  COMPLETE_POP_DETAIL,
  GET_POP_DETAIL,
  SAVE_POP_DETAIL,
  GET_DEFAULT_VALUES,
  GET_TUG_COST,
} from './constants';

export const getSailNewspaperDetailRoutine = createRoutine(GET_POP_DETAIL);
export const getSailNewspaperDetailPromise = promisifyRoutine(getSailNewspaperDetailRoutine);

export const saveSailNewspaperDetailRoutine = createRoutine(SAVE_POP_DETAIL);
export const saveSailNewspaperDetailPromise = promisifyRoutine(saveSailNewspaperDetailRoutine);

export const completeSailNewspaperDetailRoutine = createRoutine(COMPLETE_POP_DETAIL);
export const completeSailNewspaperDetailPromise = promisifyRoutine(completeSailNewspaperDetailRoutine);

export const getLoadDefaultValueRoutine = createRoutine(GET_DEFAULT_VALUES);
export const getLoadDefaultValuePromise = promisifyRoutine(getLoadDefaultValueRoutine);

export const getTugCostRoutine = createRoutine(GET_TUG_COST);
export const getTugCostPromise = promisifyRoutine(getTugCostRoutine);
