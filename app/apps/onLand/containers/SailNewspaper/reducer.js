import {
  getSailNewspaperDetailRoutine,
  saveSailNewspaperDetailRoutine,
  completeSailNewspaperDetailRoutine,
  getLoadDefaultValueRoutine,
  getTugCostRoutine,
} from './actions';

const defaultState = {
  popDetail: undefined,
  loading: false,
  error: null,
  defaultValues: undefined,
  defaultFee: undefined,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取船报信息
    case getSailNewspaperDetailRoutine.TRIGGER:
      return { ...state, loading: true };
    case getSailNewspaperDetailRoutine.SUCCESS:
      return { ...state, popDetail: action.payload };
    case getSailNewspaperDetailRoutine.FAILURE:
      return { ...state, popDetail: undefined, error: action.payload };
    case getSailNewspaperDetailRoutine.FULFILL:
      return { ...state, loading: false };

      // 保存船报
    case saveSailNewspaperDetailRoutine.TRIGGER:
      return { ...state, loading: true };
    case saveSailNewspaperDetailRoutine.SUCCESS:
      return { ...state, popDetail: action.payload };
    case saveSailNewspaperDetailRoutine.FAILURE:
      return { ...state, error: action.payload };
    case saveSailNewspaperDetailRoutine.FULFILL:
      return { ...state, loading: false };

      // 确认船报
    case completeSailNewspaperDetailRoutine.TRIGGER:
      return { ...state, loading: true };
    case completeSailNewspaperDetailRoutine.SUCCESS:
      return { ...state, popDetail: action.payload };
    case completeSailNewspaperDetailRoutine.FAILURE:
      return { ...state, error: action.payload };
    case completeSailNewspaperDetailRoutine.FULFILL:
      return { ...state, loading: false };

      // 获取默认值
    case getLoadDefaultValueRoutine.TRIGGER:
      return { ...state, loading: true };
    case getLoadDefaultValueRoutine.SUCCESS:
      return { ...state, defaultValues: action.payload };
    case getLoadDefaultValueRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getLoadDefaultValueRoutine.FULFILL:
      return { ...state, loading: false };

    case getTugCostRoutine.TRIGGER:
      return { ...state, loading: true };
    case getTugCostRoutine.SUCCESS:
      console.log('托伦费:',action.payload);
      return { ...state, defaultFee: action.payload.cost};
    case getTugCostRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getTugCostRoutine.FULFILL:
      return { ...state, loading: false };
    default:
      return state;
  }
}
