import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getLoadDefaultValueRoutine,
  getSailNewspaperDetailRoutine,
  saveSailNewspaperDetailRoutine,
  completeSailNewspaperDetailRoutine,
  getTugCostRoutine,
} from './actions';
import uploadImage from '../../../../common/uploadImage';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getDefaultLoadValue(action) {
  console.log(action);
  try {
    yield put(getLoadDefaultValueRoutine.request());
    const response = yield call(request, ApiFactory.getDefaultLoadValue(action.payload));
    console.log(response);
    yield put(getLoadDefaultValueRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    // yield put(errorMessage(e));
    yield put(getLoadDefaultValueRoutine.failure(e));
  } finally {
    yield put(getLoadDefaultValueRoutine.fulfill());
  }
}

export function* getDefaultLoadValueSaga() {
  yield takeLatest(getLoadDefaultValueRoutine.TRIGGER, getDefaultLoadValue);
}

function* getPopDetail(action) {
  console.log(action);
  try {
    yield put(getSailNewspaperDetailRoutine.request());
    const response = yield call(request, ApiFactory.getPopDetail(action.payload));
    console.log('getPopDetail', response);
    yield put(getSailNewspaperDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getSailNewspaperDetailRoutine.failure(e));
  } finally {
    yield put(getSailNewspaperDetailRoutine.fulfill());
  }
}

export function* popDetailSaga() {
  yield takeLatest(getSailNewspaperDetailRoutine.TRIGGER, getPopDetail);
}


function* getTugCost (action) {
  console.log(action);
  try {
    yield put(getTugCostRoutine.request());
    const response = yield call(request, ApiFactory.getTugCost(action.payload));
    console.log('getTugCost', response);
    yield put(getTugCostRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTugCostRoutine.failure(e));
  } finally {
    yield put(getTugCostRoutine.fulfill());
  }
}
export function* getTugCostSaga() {
  yield takeLatest(getTugCostRoutine.TRIGGER, getTugCost);
}


function* savePopDetail(action) {
  console.log(action);
  try {
    yield put(saveSailNewspaperDetailRoutine.request());
    const data = action.payload;
    const {
      draughtFrontPic, draughtMidPic, draughtAfterPic, popId,
    } = data;
    if (draughtFrontPic) {
      if (draughtFrontPic.path) {
        const response = yield call(uploadImage, draughtFrontPic, 'draughtFrontPic', popId);
        data.draughtFrontPic = response.id;
      } else {
        data.draughtFrontPic = draughtFrontPic.substr(draughtFrontPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtMidPic) {
      if (draughtMidPic.path) {
        const response = yield call(uploadImage, draughtMidPic, 'draughtMidPic', popId);
        data.draughtMidPic = response.id;
      } else {
        data.draughtMidPic = draughtMidPic.substr(draughtMidPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtAfterPic) {
      if (draughtAfterPic.path) {
        const response = yield call(uploadImage, draughtAfterPic, 'draughtAfterPic', popId);
        data.draughtAfterPic = response.id;
      } else {
        data.draughtAfterPic = draughtAfterPic.substr(draughtAfterPic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.savePopDetail(data));
    console.log('savePopDetail', response);
    yield put(saveSailNewspaperDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(saveSailNewspaperDetailRoutine.failure(e));
  } finally {
    yield put(saveSailNewspaperDetailRoutine.fulfill());
  }
}

export function* savePopDetailSaga() {
  yield takeLatest(saveSailNewspaperDetailRoutine.TRIGGER, savePopDetail);
}

function* completePopDetail(action) {
  console.log(action);
  try {
    yield put(completeSailNewspaperDetailRoutine.request());
    const data = action.payload;
    const {
      draughtFrontPic, draughtMidPic, draughtAfterPic, popId,
    } = data;
    if (draughtFrontPic) {
      if (draughtFrontPic.path) {
        const response = yield call(uploadImage, draughtFrontPic, 'draughtFrontPic', popId);
        data.draughtFrontPic = response.id;
      } else {
        data.draughtFrontPic = draughtFrontPic.substr(draughtFrontPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtMidPic) {
      if (draughtMidPic.path) {
        const response = yield call(uploadImage, draughtMidPic, 'draughtMidPic', popId);
        data.draughtMidPic = response.id;
      } else {
        data.draughtMidPic = draughtMidPic.substr(draughtMidPic.lastIndexOf('/') + 1);
      }
    }
    if (draughtAfterPic) {
      if (draughtAfterPic.path) {
        const response = yield call(uploadImage, draughtAfterPic, 'draughtAfterPic', popId);
        data.draughtAfterPic = response.id;
      } else {
        data.draughtAfterPic = draughtAfterPic.substr(draughtAfterPic.lastIndexOf('/') + 1);
      }
    }
    const response = yield call(request, ApiFactory.completePopDetail(data));
    console.log('completePopDetail', response);
    yield put(completeSailNewspaperDetailRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(completeSailNewspaperDetailRoutine.failure(e));
  } finally {
    yield put(completeSailNewspaperDetailRoutine.fulfill());
  }
}

export function* completePopDetailSaga() {
  yield takeLatest(completeSailNewspaperDetailRoutine.TRIGGER, completePopDetail);
}

// All sagas to be loaded
export default [getDefaultLoadValueSaga,getTugCostSaga, popDetailSaga, savePopDetailSaga, completePopDetailSaga];
