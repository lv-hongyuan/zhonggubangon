/*
 *
 * SailNewspaper constants
 *
 */
export const GET_DEFAULT_VALUES = 'onLand/SailNewspaper/GET_DEFAULT_VALUES';

export const GET_POP_DETAIL = 'onLand/SailNewspaper/GET_POP_DETAIL';

export const SAVE_POP_DETAIL = 'onLand/SailNewspaper/SAVE_POP_DETAIL';

export const COMPLETE_POP_DETAIL = 'onLand/SailNewspaper/COMPLETE_POP_DETAIL';

export const GET_TUG_COST = 'onland/SailNewSpaper/GET_TUG_COST'