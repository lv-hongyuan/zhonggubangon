import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableHighlight } from 'react-native';
import {
  Table, TableWrapper, Row, Rows, Col,
} from 'react-native-table-component';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  head: { height: 40, backgroundColor: '#f1f8ff' },
  title: { flex: 1, backgroundColor: '#f6f8fa' },
  row: { height: 28 },
  text: { textAlign: 'center' },
});

class InfoTable extends React.PureComponent {
  render() {
    const tableHead = ['', '20GF', '40GF', '40HF', '20E', '40E'];
    const data = this.props.tableData || {};
    return (
      <TouchableHighlight {...this.props}>
        <Table
          borderStyle={{ borderWidth: myTheme.borderWidth, borderColor: '#E0E0E0' }}
          style={{ backgroundColor: 'white' }}
        >
          <Row
            data={tableHead}
            flexArr={[1, 1, 1, 1, 1, 1]}
            style={styles.head}
            textStyle={styles.text}
          />
          <TableWrapper style={{ flexDirection: 'row' }}>
            <Col data={['装', '卸']} style={styles.title} heightArr={[28, 28]} textStyle={styles.text} />
            <Rows
              data={[
                [data.f20Load, data.f40Load, data.f40HLoad, data.e20Load, data.e40Load],
                [data.f20UnLoad, data.f40UnLoad, data.f40HUnLoad, data.e20UnLoad, data.e40UnLoad],
              ]}
              flexArr={[1, 1, 1, 1, 1]}
              style={styles.row}
              textStyle={styles.text}
            />
          </TableWrapper>
          <TableWrapper style={{ flexDirection: 'row' }}>
            <Col
              data={['倒箱', '特殊\n作业']}
              style={styles.title}
              heightArr={[28, 40]}
              textStyle={styles.text}
            />
            <Rows
              data={[
                [data.f20Dx, data.f40Dx, data.e20Dx, data.e40Dx],
                [data.f20Tx, data.f40Tx, data.e20Tx, data.e40Tx],
              ]}
              flexArr={[1, 2, 1, 1]}
              heightArr={[28, 40]}
              textStyle={styles.text}
            />
          </TableWrapper>
        </Table>
      </TouchableHighlight>
    );
  }
}

InfoTable.propTypes = {
  tableData: PropTypes.object.isRequired,
};

export default InfoTable;
