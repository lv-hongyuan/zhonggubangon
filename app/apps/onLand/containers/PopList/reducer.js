import fixIdList from '../../../../utils/fixIdList';
import { getCurrentPopListRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';

const defaultState = {
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取动态（带分页）
    case getCurrentPopListRoutine.TRIGGER: {
      const { loadMore } = action.payload;
      return {
        ...state,
        loading: true,
        refreshState: loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    }
    case getCurrentPopListRoutine.SUCCESS: {
      const { page, pageSize, list } = action.payload;
      const fixedList = fixIdList(list);
      return {
        ...state,
        list: page === 1 ? fixedList : state.list.concat(fixedList),
        refreshState: fixedList.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle,
      };
    }
    case getCurrentPopListRoutine.FAILURE: {
      const { page, error } = action.payload;
      return {
        ...state, list: page === 1 ? [] : state.list, error, refreshState: RefreshState.Failure,
      };
    }
    case getCurrentPopListRoutine.FULFILL: {
      return { ...state, loading: false };
    }

    default:
      return state;
  }
}
