
// 港口作业 界面（下方tab页第二个）

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Keyboard, SafeAreaView, StyleSheet, TouchableOpacity,
} from 'react-native';
import { createStructuredSelector } from 'reselect/es';
import {
  Button, Container, InputGroup, Item, Label, Text, View,
} from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { NavigationActions } from 'react-navigation';
import _ from 'lodash';
import { makeSelectRefreshState, makePopList } from './selectors';
import { ROUTE_POP_DETAIL } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';
import screenHOC from '../../../../components/screenHOC';
import PortWorkSummaryItem from './components/PortWorkSummaryItem';
import {
  getCurrentPopListPromise,
} from './actions';
import commonStyles from '../../../../common/commonStyles';
import InputItem from '../../../../components/InputItem';
import Svg from '../../../../components/Svg';

const firstPageNum = 1;
const DefaultPageSize = 10;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
    borderBottomWidth: myTheme.borderWidth,
    borderBottomColor: myTheme.borderColor,
  },
  searchInput: {
    borderWidth: 1,
    borderRadius: 4,
    borderColor: myTheme.borderColor,
    minHeight: 30,
    paddingLeft: 5,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  searchItem: {
    flex: 0,
    width: '49.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0,
    minHeight: 30,
    paddingLeft: 5,
  },
});

@screenHOC
class PopList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      shipName: '',
      voyageCode: '',
      pq: '',
      inputShipName: '',
      inputVoyageCode: '',
      inputPq: '',
      page: firstPageNum,
      pageSize: DefaultPageSize,
      displayMode: true,
    };
  }

  componentDidMount() {
    this.onFooterRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.changeDisplayMode(true);
      },
    });
  }

  onHeaderRefresh = () => {
    // 开始上拉翻页
    this.loadList(false);
  };

  onFooterRefresh = () => {
    // 开始下拉刷新
    this.loadList(true);
  };

  setSearchValue(callBack) {
    this.setState({
      shipName: this.state.inputShipName,
      voyageCode: this.state.inputVoyageCode,
      pq: this.state.inputPq,
    }, callBack);
  }

  setInputValue(callBack) {
    this.setState({
      inputShipName: this.state.shipName,
      inputVoyageCode: this.state.voyageCode,
      inputPq: this.state.pq,
    }, callBack);
  }

  changeDisplayMode = (displayMode) => {
    this.props.navigation.setParams({
      displayMode,
    });
    this.setState({ displayMode });
  };

  showPopDetail = (item) => {
    this.props.navigation.navigate(ROUTE_POP_DETAIL, {
      popId: item.popid,
      shipName: item.zwcm,
      portName: item.port,
    });
  };

  // didFocus = () => {
  //   // 每次进页面刷新列表
  //   this.onFooterRefresh()
  // }

  loadList(loadMore: boolean) {
    const {
      shipName,
      voyageCode,
      pq,
      page,
      pageSize,
    } = this.state;
    this.props.getCurrentPopList({
      shipName,
      voyageCode,
      pq,
      page: loadMore ? page : firstPageNum,
      pageSize,
      loadMore,
    })
    // eslint-disable-next-line no-shadow
      .then(({ page }) => {
        this.setState({ page: page + 1 });
      })
      .catch(() => {});
  }

  renderItem = ({ item }) => (
    <PortWorkSummaryItem
      item={item}
      onPress={this.showPopDetail}
    />
  );

  renderFilter() {
    return (
      <View style={styles.container}>
        {this.state.displayMode ? (
          <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
            <View style={{ padding: 10, flex: 1 }}>
              <TouchableOpacity
                activeOpacity={1}
                style={styles.searchInput}
                onPress={() => {
                  this.setInputValue();
                  this.changeDisplayMode(false);
                }}
              >
                {!_.isEmpty(this.state.shipName) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>船名:</Label>
                    <Text style={commonStyles.text}>{this.state.shipName}</Text>
                  </View>
                )}
                {!_.isEmpty(this.state.voyageCode) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>航次:</Label>
                    <Text style={commonStyles.text}>{this.state.voyageCode}</Text>
                  </View>
                )}
                {!_.isEmpty(this.state.pq) && (
                  <View style={styles.searchItem}>
                    <Label style={commonStyles.inputLabel}>片区:</Label>
                    <Text style={commonStyles.text}>{this.state.pq}</Text>
                  </View>
                )}
                {_.isEmpty(this.state.shipName) && _.isEmpty(this.state.voyageCode) && _.isEmpty(this.state.pq) && (
                  <Item
                    style={[commonStyles.inputItem, { borderBottomWidth: 0 }]}
                    onPress={() => {
                      this.setInputValue();
                      this.changeDisplayMode(false);
                    }}
                  >
                    <Text style={commonStyles.text}>点击输入要搜索的船名/航次/片区</Text>
                  </Item>
                )}
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  autoFocus
                  label="船名:"
                  returnKeyType="search"
                  value={this.state.inputShipName}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputShipName: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="航次:"
                  returnKeyType="search"
                  value={this.state.inputVoyageCode}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputVoyageCode: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="片区:"
                  returnKeyType="search"
                  value={this.state.inputPq}
                  clearButtonMode="while-editing"
                  onChangeText={(text) => {
                    this.setState({ inputPq: text });
                  }}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                    this.setSearchValue();
                    this.changeDisplayMode(true);
                    this.listView.beginRefresh();
                  }}
                  onFocus={() => {
                    this.changeDisplayMode(false);
                  }}
                />
              </InputGroup>
            </View>
            <Button
              onPress={() => {
                Keyboard.dismiss();
                this.setSearchValue();
                this.changeDisplayMode(true);
                this.setState({
                  displayMode: true,
                }, () => {
                  this.listView.beginRefresh();
                });
              }}
              block
              style={{
                height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
              }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        )}
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <SafeAreaView style={{ flex: 1 }}>
          <RefreshListView
            ref={(ref) => { this.listView = ref; }}
            style={{ width: '100%' }}
            data={this.props.list || []}
            keyExtractor={item => `${item.id}`}
            renderItem={this.renderItem}
            refreshState={this.props.refreshState}
            onHeaderRefresh={this.onHeaderRefresh}
            onFooterRefresh={this.onFooterRefresh}
            ListHeaderComponent={() => (<View style={{ height: 5 }} />)}
            ItemSeparatorComponent={() => (<View style={{ height: 5 }} />)}
          />
          {!this.state.displayMode && (
            <View style={{
              position: 'absolute', width: '100%', height: '100%', backgroundColor: '#ffffff',
            }}
            />
          )}
        </SafeAreaView>
      </Container>
    );
  }
}

PopList.navigationOptions = ({ navigation }) => ({
  title: '港口作业',
  headerLeft: (
    <HeaderButtons>
      {navigation.getParam('displayMode', true) ? (
        <HeaderButtons.Item
          title=""
          buttonWrapperStyle={{ padding: 10 }}
          ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
          onPress={() => {
            navigation.dispatch(NavigationActions.back());
          }}
        />
      ) : (
        <HeaderButtons.Item
          title="取消"
          buttonStyle={{ fontSize: 14, color: '#ffffff' }}
          onPress={() => {
            const exitSearch = navigation.getParam('exitSearch');
            exitSearch();
          }}
        />
      )}
    </HeaderButtons>
  ),
});

PopList.propTypes = {
  navigation: PropTypes.object.isRequired,
  getCurrentPopList: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
PopList.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makePopList(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getCurrentPopList: getCurrentPopListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PopList);
