import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_CURRENT_WORK_LIST } from './constants';

export const getCurrentPopListRoutine = createRoutine(GET_CURRENT_WORK_LIST);
export const getCurrentPopListPromise = promisifyRoutine(getCurrentPopListRoutine);
