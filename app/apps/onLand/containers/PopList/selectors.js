import { createSelector } from 'reselect/es';

const selectPopListDomain = () => state => state.popList;

const makePopList = () => createSelector(selectPopListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectPopListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

export {
  makePopList,
  makeSelectRefreshState,
};
