import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getCurrentPopListRoutine } from './actions';

function* getCurrentPopList(action) {
  console.log(action);
  const { page, pageSize, ...rest } = (action.payload || {});
  try {
    yield put(getCurrentPopListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      if (!_.isEmpty(value)) {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.getCurrentDynList(param));
    console.log('1111111',response);
    const { totalElements, content } = (response.dtoList || {});
    yield put(getCurrentPopListRoutine.success({
      page, pageSize, list: (content || []), totalElements,
    }));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getCurrentPopListRoutine.failure({ page, pageSize, error }));
  } finally {
    yield put(getCurrentPopListRoutine.fulfill());
  }
}

export function* getCurrentPopListSaga() {
  yield takeLatest(getCurrentPopListRoutine.TRIGGER, getCurrentPopList);
}

// All sagas to be loaded
export default [getCurrentPopListSaga];
