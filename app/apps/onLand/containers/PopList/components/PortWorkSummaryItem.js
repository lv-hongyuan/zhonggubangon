import React from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, PixelRatio, TouchableHighlight,
} from 'react-native';
import { Grid, Col } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  info: {
    backgroundColor: 'white',
  },
  label: {
    fontSize: 14,
    color: '#535353',
    margin: 2,
  },
  cornerContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    padding: 5,
    borderBottomLeftRadius: 5,
    borderLeftWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    backgroundColor: '#ccc',
  },
  infoContainer: {
    flexDirection: 'row',
    borderTopWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#E0E0E0',
  },
});

class PortWorkSummaryItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const item = this.props.item || {};
    const {
      zwcm, hc, shipline, pq, stateMemo,
    } = item;
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => {
          this.props.onPress && this.props.onPress(item);
        }}
      >
        <View style={styles.info}>
          <View style={styles.cornerContainer}>
            <Text style={styles.label}>{pq}</Text>
          </View>
          <View style={{
            paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 5,
          }}
          >
            <Grid>
              <Col>
                <Text style={styles.label}>
                  船名：
                  {zwcm || '-----------'}
                </Text>
              </Col>
              <Col>
                <Text style={styles.label}>
                  航次：
                  {hc || '-----------'}
                </Text>
              </Col>
            </Grid>
            <Text style={styles.label}>
航线：
              {shipline}
            </Text>
          </View>
          <View style={styles.infoContainer}>
            <View style={{
              flex: 1,
              justifyContent: 'space-between',
              paddingLeft: 20,
              paddingTop: 5,
              paddingBottom: 5,
            }}
            >
              <Text style={styles.label}>{stateMemo}</Text>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

PortWorkSummaryItem.propTypes = {
  item: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default PortWorkSummaryItem;
