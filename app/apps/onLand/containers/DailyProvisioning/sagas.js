import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDailyProvisioningRoutine } from './actions';

function* getDailyProvisioning(action) {
  console.log(action);
  try {
    yield put(getDailyProvisioningRoutine.request());
    const response = yield call(request, ApiFactory.getPrematch());
    console.log(response);
    yield put(getDailyProvisioningRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDailyProvisioningRoutine.failure(e));
  } finally {
    yield put(getDailyProvisioningRoutine.fulfill());
  }
}

export function* getDailyProvisioningSaga() {
  yield takeLatest(getDailyProvisioningRoutine.TRIGGER, getDailyProvisioning);
}

// All sagas to be loaded
export default [getDailyProvisioningSaga];
