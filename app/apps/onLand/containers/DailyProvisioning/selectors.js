import { createSelector } from 'reselect/es';

const selectDailyProvisioningDomain = () => state => state.dailyProvisioning;

const makeList = () => createSelector(selectDailyProvisioningDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDailyProvisioningDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
