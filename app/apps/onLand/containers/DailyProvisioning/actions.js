import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DAILY_PROVISIONING } from './constants';

export const getDailyProvisioningRoutine = createRoutine(GET_DAILY_PROVISIONING);
export const getDailyProvisioningPromise = promisifyRoutine(getDailyProvisioningRoutine);
