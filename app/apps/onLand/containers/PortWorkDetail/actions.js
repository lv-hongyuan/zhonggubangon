import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  CHANGE_PLAN,
  COMPLETE_BERTHING,
  COMPLETE_OPERATING,
  COMPLETE_PLAN,
  COMPLETE_UN_BERTHING,
  GET_BERTH_LIST,
  GET_PIER_LIST,
  GET_WORK_DETAIL,
  SAVE_BERTHING,
  SAVE_OPERATING,
  SAVE_PLAN,
  SAVE_UN_BERTHING,
} from './constants';

export const getPierListRoutine = createRoutine(GET_PIER_LIST);
export const getPierListPromise = promisifyRoutine(getPierListRoutine);

export const getBerthListRoutine = createRoutine(GET_BERTH_LIST);
export const getBerthListPromise = promisifyRoutine(getBerthListRoutine);

export const getWorkDetailRoutine = createRoutine(GET_WORK_DETAIL);
export const getWorkDetailPromise = promisifyRoutine(getWorkDetailRoutine);

export const changePlanRoutine = createRoutine(CHANGE_PLAN);
export const changePlanPromise = promisifyRoutine(changePlanRoutine);

export const savePlanRoutine = createRoutine(SAVE_PLAN);
export const savePlanPromise = promisifyRoutine(savePlanRoutine);

export const completePlanRoutine = createRoutine(COMPLETE_PLAN);
export const completePlanPromise = promisifyRoutine(completePlanRoutine);

export const saveBerthingRoutine = createRoutine(SAVE_BERTHING);
export const saveBerthingPromise = promisifyRoutine(saveBerthingRoutine);

export const completeBerthingRoutine = createRoutine(COMPLETE_BERTHING);
export const completeBerthingPromise = promisifyRoutine(completeBerthingRoutine);

export const saveOperatingRoutine = createRoutine(SAVE_OPERATING);
export const saveOperatingPromise = promisifyRoutine(saveOperatingRoutine);

export const completeOperatingRoutine = createRoutine(COMPLETE_OPERATING);
export const completeOperatingPromise = promisifyRoutine(completeOperatingRoutine);

export const saveUnBerthingRoutine = createRoutine(SAVE_UN_BERTHING);
export const saveUnBerthingPromise = promisifyRoutine(saveUnBerthingRoutine);

export const completeUnBerthingRoutine = createRoutine(COMPLETE_UN_BERTHING);
export const completeUnBerthingPromise = promisifyRoutine(completeUnBerthingRoutine);
