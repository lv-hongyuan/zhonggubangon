import React from 'react';
import PropTypes from 'prop-types';
import { createMaterialTopTabNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { PortWorkState } from '../PortWork/constants';
import {
  getWorkDetailPromise,
  getPierListPromise,
  getBerthListPromise,
} from './actions';
import {
  makeIsLoading,
  makePierList,
  makeBerthList,
} from './selectors';
import {
  ROUTE_PORT_WORK_DETAIL_ACTUAL,
  ROUTE_PORT_WORK_DETAIL_PLAN,
} from '../../RouteConstant';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import PortWorkDetailActual from '../PortWorkDetailActual';
import PortWorkDetailPlan from '../PortWorkDetailPlan';

const TopTabNavigator = createMaterialTopTabNavigator({
  [ROUTE_PORT_WORK_DETAIL_PLAN]: { screen: PortWorkDetailPlan },
  [ROUTE_PORT_WORK_DETAIL_ACTUAL]: { screen: PortWorkDetailActual },
}, {
  backBehavior: 'none',
  tabBarOptions: {
    allowFontScaling:false,
    activeTintColor: '#ED1727',
    inactiveTintColor: '#666666',
    style: {
      backgroundColor: '#ffffff',
    },
    tabStyle: {
      backgroundColor: 'transparent',
      borderBottomColor: myTheme.borderColor,
      borderBottomWidth: myTheme.borderWidth,
    },
    indicatorStyle: {
      backgroundColor: '#ED1727',
    },
  },
});

@screenHOC
class PortWorkDetail extends React.PureComponent {
  static router = {
    ...TopTabNavigator.router,
    getStateForAction: (action, lastState) => {
      const state = TopTabNavigator.router.getStateForAction(action, lastState);
      if (lastState === state) {
        return state;
      }
      const routes = state.routes && state.routes.map(route => ({
        ...route,
        params: {
          ...route.params,
          ...state.params,
          readOnly: true,
        },
      }));
      return {
        ...state,
        routes,
      };
    },
  };

  constructor(props) {
    super(props);

    this.state = {
      popId: this.props.navigation.getParam('popId'),
    };
  }

  componentDidMount() {
    this.loadDetail();
    // this.props.getPierListPromise().then(() => {}).catch(() => {});
    // this.props.getBerthListPromise().then(() => {}).catch(() => {});
  }

  loadDetail = () => {
    this.props.getWorkDetailPromise(this.state.popId)
      .then((workDetail) => {
        this.props.getPierListPromise(workDetail.portId)
          .then(() => {});
        if (workDetail.statePort >= PortWorkState.berthing) {
          this.props.navigation.navigate(ROUTE_PORT_WORK_DETAIL_ACTUAL);
        }
      })
      .catch(() => {});
  };

  render() {
    return (
      <TopTabNavigator navigation={this.props.navigation} screenProps={{ test: this.state.test }} />
    );
  }
}

PortWorkDetail.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('shipName', '信息录入'),
});

PortWorkDetail.propTypes = {
  navigation: PropTypes.object.isRequired,
  getWorkDetailPromise: PropTypes.func.isRequired,
  getPierListPromise: PropTypes.func.isRequired,
};
PortWorkDetail.defaultProps = {};

const mapStateToProps = createStructuredSelector({
  isLoading: makeIsLoading(),
  pierList: makePierList(),
  berthList: makeBerthList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getWorkDetailPromise,
      getPierListPromise,
      getBerthListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortWorkDetail);
