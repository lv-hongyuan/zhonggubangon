import {
  getPierListRoutine,
  getBerthListRoutine,
  savePlanRoutine,
  completePlanRoutine,
  changePlanRoutine,
  saveBerthingRoutine,
  completeBerthingRoutine,
  saveOperatingRoutine,
  completeOperatingRoutine,
  saveUnBerthingRoutine,
  completeUnBerthingRoutine,
  getWorkDetailRoutine,
} from './actions';

const defaultState = {
  workDetail: null,
  isLoading: false,
  pierLoading: false,
  loadingError: null,
  navBack: false,
  pierList: [],
  berthList: [],
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取码头列表
    case getPierListRoutine.TRIGGER:
      return {
        ...state,
        pierLoading: true,
      };
    case getPierListRoutine.SUCCESS:
      return {
        ...state,
        pierList: action.payload,
      };
    case getPierListRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getPierListRoutine.FULFILL:
      return {
        ...state,
        pierLoading: false,
      };

    // 获取泊位列表
    case getBerthListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case getBerthListRoutine.SUCCESS:
      return {
        ...state,
        berthList: action.payload,
      };
    case getBerthListRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getBerthListRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case getWorkDetailRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case getWorkDetailRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case getWorkDetailRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getWorkDetailRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case savePlanRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case savePlanRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case savePlanRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case savePlanRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case completePlanRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case completePlanRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case completePlanRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case completePlanRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case changePlanRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case changePlanRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case changePlanRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case changePlanRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case saveBerthingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case saveBerthingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case saveBerthingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case saveBerthingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case completeBerthingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case completeBerthingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case completeBerthingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case completeBerthingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case saveOperatingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case saveOperatingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case saveOperatingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case saveOperatingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case completeOperatingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case completeOperatingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case completeOperatingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case completeOperatingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case saveUnBerthingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case saveUnBerthingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case saveUnBerthingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case saveUnBerthingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    case completeUnBerthingRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
      };
    case completeUnBerthingRoutine.SUCCESS:
      return {
        ...state,
        workDetail: action.payload,
      };
    case completeUnBerthingRoutine.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case completeUnBerthingRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
}
