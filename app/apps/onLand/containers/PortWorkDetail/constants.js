/*
 *
 * PortWork constants
 *
 */
export const GET_WORK_DETAIL = 'onLand/PortWork/GET_WORK_DETAIL';

export const SAVE_PLAN = 'onLand/PortWork/SAVE_PLAN';
export const COMPLETE_PLAN = 'onLand/PortWork/COMPLETE_PLAN';
export const CHANGE_PLAN = 'onLand/PortWork/CHANGE_PLAN';

export const SAVE_BERTHING = 'onLand/PortWork/SAVE_BERTHING';
export const COMPLETE_BERTHING = 'onLand/PortWork/COMPLETE_BERTHING';

export const SAVE_OPERATING = 'onLand/PortWork/SAVE_OPERATING';
export const COMPLETE_OPERATING = 'onLand/PortWork/COMPLETE_OPERATING';

export const SAVE_UN_BERTHING = 'onLand/PortWork/SAVE_UN_BERTHING';
export const COMPLETE_UN_BERTHING = 'onLand/PortWork/COMPLETE_UN_BERTHING';

export const GET_PIER_LIST = 'onLand/PortWork/GET_PIER_LIST';
export const GET_BERTH_LIST = 'onLand/PortWork/GET_BERTH_LIST';

export const EtaSource = {
  DEPARTMENT: '航运部',
  SHIP: '船上',
  PORT: '现场',
  PREDICTION: '测算',
};

export const BerthingState = {
  directBerth: '直靠',
  waitTide: '等潮水',
  waitBerth: '等泊',
  waitGoods: '等货',
  trafficControl: '交通管制',
};
