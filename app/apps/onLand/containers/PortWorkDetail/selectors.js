import { createSelector } from 'reselect';

const selectPortWorkDetailDomain = () => state => state.portWorkDetail;

const makeSelectPortWorkDetail = () => createSelector(
  selectPortWorkDetailDomain(),
  (subState) => {
    console.log(subState);
    return subState.workDetail;
  },
);

const makeIsLoading = () => createSelector(
  selectPortWorkDetailDomain(),
  subState => subState.isLoading,
);

const makePierList = () => createSelector(
  selectPortWorkDetailDomain(),
  subState => subState.pierList,
);

const makeBerthList = () => createSelector(
  selectPortWorkDetailDomain(),
  subState => subState.berthList,
);

export {
  makeSelectPortWorkDetail,
  makeIsLoading,
  makePierList,
  makeBerthList,
};
