import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { EventRange } from '../../common/Constant';
import { getEmergencies } from '../Emergencies/sagas';
import { PortWorkState } from '../PortWork/constants';
import {
  getPierListRoutine,
  getBerthListRoutine,
  getWorkDetailRoutine,
  savePlanRoutine,
  completePlanRoutine,
  changePlanRoutine,
  saveBerthingRoutine,
  completeBerthingRoutine,
  saveOperatingRoutine,
  completeOperatingRoutine,
  saveUnBerthingRoutine,
  completeUnBerthingRoutine,
} from './actions';
import { getEmergencyListRoutine } from '../Emergencies/actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';

function* getPierList(action) {
  console.log(action);
  try {
    yield put(getPierListRoutine.request());
    const response = yield call(request, ApiFactory.getWharfSelect(action.payload));
    console.log(response);
    yield put(getPierListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPierListRoutine.failure(e));
  } finally {
    yield put(getPierListRoutine.fulfill());
  }
}

export function* getPierListSaga() {
  yield takeLatest(getPierListRoutine.TRIGGER, getPierList);
}

function* getBerthList(action) {
  console.log(action);
  try {
    yield put(getBerthListRoutine.request());
    // const response = yield call(request, ApiFactory.getBerthList(action.payload))
    // const response = yield call(fakeList);
    // console.log(response);
    yield put(getBerthListRoutine.success([]));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthListRoutine.failure(e));
  } finally {
    yield put(getBerthListRoutine.fulfill());
  }
}

export function* getBerthListSaga() {
  yield takeLatest(getBerthListRoutine.TRIGGER, getBerthList);
}

function* getWorkDetail(action) {
  console.log(action);
  try {
    yield put(getWorkDetailRoutine.request());
    const response = yield call(request, ApiFactory.getWorkDetail(action.payload));
    console.log(response);

    // 清空缓存
    yield put(getEmergencyListRoutine.success({
      range: EventRange.PORT_BERTHING,
      list: [],
    }));
    yield put(getEmergencyListRoutine.success({
      range: EventRange.PORT_OPERATION,
      list: [],
    }));
    yield put(getEmergencyListRoutine.success({
      range: EventRange.PORT_UN_BERTHING,
      list: [],
    }));

    // 获取突发事件数量
    const { id, statePort } = response;
    if (statePort >= PortWorkState.berthing) {
      yield call(getEmergencies, {
        payload: {
          range: EventRange.PORT_BERTHING,
          popId: id,
        },
      });
    }
    if (statePort >= PortWorkState.operating) {
      yield call(getEmergencies, {
        payload: {
          range: EventRange.PORT_OPERATION,
          popId: id,
        },
      });
    }
    if (statePort >= PortWorkState.unBerthing) {
      yield call(getEmergencies, {
        payload: {
          range: EventRange.PORT_UN_BERTHING,
          popId: id,
        },
      });
    }

    // 返回数据
    yield put(getWorkDetailRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(getWorkDetailRoutine.failure(error));
  } finally {
    yield put(getWorkDetailRoutine.fulfill());
  }
}

function* savePlan(action) {
  console.log(action);
  try {
    yield put(savePlanRoutine.request());
    const response = yield call(request, ApiFactory.savePlan(action.payload));
    console.log(response);
    yield put(savePlanRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(savePlanRoutine.failure(error));
  } finally {
    yield put(savePlanRoutine.fulfill());
  }
}

function* completePlan(action) {
  console.log(action);
  try {
    yield put(completePlanRoutine.request());
    const response = yield call(request, ApiFactory.completePlan(action.payload));
    console.log(response);
    yield put(completePlanRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(completePlanRoutine.failure(error));
  } finally {
    yield put(completePlanRoutine.fulfill());
  }
}

function* changePlan(action) {
  console.log(action);
  try {
    yield put(changePlanRoutine.request());
    const response = yield call(request, ApiFactory.changePlan(action.payload));
    console.log(response);
    yield put(changePlanRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(changePlanRoutine.failure(error));
  } finally {
    yield put(changePlanRoutine.fulfill());
  }
}

function* saveBerthing(action) {
  console.log(action);
  try {
    yield put(saveBerthingRoutine.request());
    const { id, isUpdate, directBerthPort } = action.payload;
    // 非直靠判断突发事件数量
    // if (isUpdate === 1 && directBerthPort === 0) {
    //   const eventResponse = yield call(request, ApiFactory.getEventList(id, EventRange.PORT_BERTHING));
    //   const { dtoList } = eventResponse;
      // if (!dtoList || dtoList.length === 0) {
      //   const error = new Error('非直靠请添加非直靠原因');
      //   yield put(errorMessage(error));
      //   yield put(completeBerthingRoutine.failure(error));
      //   return;
      // }
    // }
    const response = yield call(request, ApiFactory.saveBerthing(action.payload));
    console.log(response);
    yield put(saveBerthingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    if (error.response && error.response._backcode + '' === '830') {
      yield put(saveBerthingRoutine.success(error.response));
    } else {
      yield put(saveBerthingRoutine.failure(error));
    }
  } finally {
    yield put(saveBerthingRoutine.fulfill());
  }
}

function* completeBerthing(action) {
  console.log(action);
  try {
    yield put(completeBerthingRoutine.request());
    const { id, directBerthPort } = action.payload;
    // 非直靠判断突发事件数量
    // if (directBerthPort === 0) {
    //   const eventResponse = yield call(request, ApiFactory.getEventList(id, EventRange.PORT_BERTHING));
    //   const { dtoList } = eventResponse;
      // if (!dtoList || dtoList.length === 0) {
      //   const error = new Error('非直靠请添加非直靠原因');
      //   yield put(errorMessage(error));
      //   yield put(completeBerthingRoutine.failure(error));
      //   return;
      // }
    // }
    const response = yield call(request, ApiFactory.completeBerthing(action.payload));
    console.log(response);
    yield put(completeBerthingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
      yield put(errorMessage(error));
      if (error.response && error.response._backcode + '' === '830') {
          yield put(completeBerthingRoutine.success(error.response));
      } else {
          yield put(completeBerthingRoutine.failure(error));
      }
  } finally {
    yield put(completeBerthingRoutine.fulfill());
  }
}

function* saveOperating(action) {
  console.log(action);
  try {
    yield put(saveOperatingRoutine.request());
    const response = yield call(request, ApiFactory.saveOperating(action.payload));
    console.log(response);
    yield put(saveOperatingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(saveOperatingRoutine.failure(error));
  } finally {
    yield put(saveOperatingRoutine.fulfill());
  }
}

function* completeOperating(action) {
  console.log(action);
  try {
    yield put(completeOperatingRoutine.request());
    const response = yield call(request, ApiFactory.completeOperating(action.payload));
    console.log(response);
    yield put(completeOperatingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(completeOperatingRoutine.failure(error));
  } finally {
    yield put(completeOperatingRoutine.fulfill());
  }
}

function* saveUnBerthing(action) {
  console.log(action);
  try {
    yield put(saveUnBerthingRoutine.request());
    const response = yield call(request, ApiFactory.saveUnBerthing(action.payload));
    console.log(response);
    yield put(saveUnBerthingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(saveUnBerthingRoutine.failure(error));
  } finally {
    yield put(saveUnBerthingRoutine.fulfill());
  }
}

function* completeUnBerthing(action) {
  console.log(action);
  try {
    yield put(completeUnBerthingRoutine.request());
    const response = yield call(request, ApiFactory.completeUnBerthing(action.payload));
    console.log(response);
    yield put(completeUnBerthingRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    yield put(completeUnBerthingRoutine.failure(error));
  } finally {
    yield put(completeUnBerthingRoutine.fulfill());
  }
}

export function* workDetailSaga() {
  yield takeLatest(getWorkDetailRoutine.TRIGGER, getWorkDetail);
}

export function* savePlanSaga() {
  yield takeLatest(savePlanRoutine.TRIGGER, savePlan);
}

export function* completePlanSaga() {
  yield takeLatest(completePlanRoutine.TRIGGER, completePlan);
}

export function* changePlanSaga() {
  yield takeLatest(changePlanRoutine.TRIGGER, changePlan);
}

export function* saveBerthingSaga() {
  yield takeLatest(saveBerthingRoutine.TRIGGER, saveBerthing);
}

export function* completeBerthingSaga() {
  yield takeLatest(completeBerthingRoutine.TRIGGER, completeBerthing);
}

export function* saveOperatingSaga() {
  yield takeLatest(saveOperatingRoutine.TRIGGER, saveOperating);
}

export function* completeOperatingSaga() {
  yield takeLatest(completeOperatingRoutine.TRIGGER, completeOperating);
}

export function* saveUnBerthingSaga() {
  yield takeLatest(saveUnBerthingRoutine.TRIGGER, saveUnBerthing);
}

export function* completeUnBerthingSaga() {
  yield takeLatest(completeUnBerthingRoutine.TRIGGER, completeUnBerthing);
}

// All sagas to be loaded
export default [
  getPierListSaga,
  getBerthListSaga,
  workDetailSaga,
  savePlanSaga,
  completePlanSaga,
  changePlanSaga,
  saveBerthingSaga,
  completeBerthingSaga,
  saveOperatingSaga,
  completeOperatingSaga,
  saveUnBerthingSaga,
  completeUnBerthingSaga,
];
