import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_ADD_OIL_APPLY, REVIEW_ADD_OIL_APPLY } from './constants';

export const getAddOilApplyRoutine = createRoutine(GET_ADD_OIL_APPLY);
export const getAddOilApplyPromise = promisifyRoutine(getAddOilApplyRoutine);

export const reviewAddOilApplyRoutine = createRoutine(REVIEW_ADD_OIL_APPLY);
export const reviewAddOilApplyPromise = promisifyRoutine(reviewAddOilApplyRoutine);
