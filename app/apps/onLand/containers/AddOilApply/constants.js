/*
 *
 * ShipRepairApplyList constants
 *
 */
export const GET_ADD_OIL_APPLY = 'onLand/AddOilApply/GET_ADD_OIL_APPLY';
export const REVIEW_ADD_OIL_APPLY = 'onLand/AddOilApply/REVIEW_ADD_OIL_APPLY';
