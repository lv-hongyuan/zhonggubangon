import { createSelector } from 'reselect/es';

const selectAddOilApplyDomain = () => state => state.addOilApply;

const makeSelectEmergencies = () => createSelector(selectAddOilApplyDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectAddOilApplyDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectAddOilApplyDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeIsLoading };
