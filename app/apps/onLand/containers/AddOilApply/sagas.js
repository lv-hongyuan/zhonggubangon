import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getAddOilApplyRoutine, reviewAddOilApplyRoutine } from './actions';

function* getFindAddOilPlans(action) {
  console.log(action);
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getAddOilApplyRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    console.log('param+++',JSON.stringify(param))
    const response = yield call(request, ApiFactory.getFindAddOilPlans(param));
    console.log('+++加油',JSON.stringify(response));
    const showAudit = response.showAudit;
    const { totalElements, content } = (response.dtoList || {});
    yield put(getAddOilApplyRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit,
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getAddOilApplyRoutine.failure(e));
  } finally {
    yield put(getAddOilApplyRoutine.fulfill());
  }
}

export function* getAddOilApplySaga() {
  yield takeLatest(getAddOilApplyRoutine.TRIGGER, getFindAddOilPlans);
}

function* reviewAddOilApply(action) {
  console.log(action);
  try {
    yield put(reviewAddOilApplyRoutine.request());
    const { id, state, rejectReasons } = action.payload;
    const response = yield call(request, ApiFactory.updateAddOilPlanState({ id, state, rejectReasons }));
    console.log('reviewAddOilApply', response);
    yield put(reviewAddOilApplyRoutine.success({ id, state }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(reviewAddOilApplyRoutine.failure(e));
  } finally {
    yield put(reviewAddOilApplyRoutine.fulfill());
  }
}

export function* reviewAddOilApplySaga() {
  yield takeLatest(reviewAddOilApplyRoutine.TRIGGER, reviewAddOilApply);
}

// All sagas to be loaded
export default [getAddOilApplySaga, reviewAddOilApplySaga];
