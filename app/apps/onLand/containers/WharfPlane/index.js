import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated, StatusBar} from 'react-native';
import { connect } from 'react-redux';
import {
    Container,
    Item,
    Label,
    Text,
    View,
    Button,
} from 'native-base';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from '../../../../common/commonStyles';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { getWharfPlanePromise } from './actions';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';
import DatePullSelector from '../../../../components/DatePullSelector';
import WharfPlaneItem from './components/WharfPlaneItem';
import { makeList, makeRefreshState } from './selectors';
import AlertView from "../../../../components/Alert";



const styles = {
    container: {
        backgroundColor: 'white',
        flexDirection: 'row',
        alignContent: 'stretch',
    },
    col: {
        backgroundColor: 'white',
        width: 80,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    touchCol: {
        backgroundColor: 'white',
        width: 80,
    },
    touchContent: {
        flex: 1,
        backgroundColor: 'white',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRightWidth: myTheme.borderWidth,
        borderBottomWidth: myTheme.borderWidth,
        borderColor: myTheme.borderColor,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    text: {
        color: myTheme.inputColor,
        textAlign: 'center',
        flex: 1,
    },
    selectTimeLabel: {
        fontSize: 14,
        color: '#535353',
        width: 70,
        marginLeft: 15,
    },
    selectTimeItem: {
        flex: 1,
        height: 30,
        borderBottomWidth: myTheme.borderWidth,
        marginTop: 5,
        marginLeft: 0,
        marginBottom: 0,
        marginRight: 0,
        justifyContent: 'space-between',
        alignItems: 'center',
        // alignContent: 'center',
        width: '30%',
    },
};


@screenHOC
class WharfPlane extends React.PureComponent {
    constructor(props) {
        super(props);
        let timestamp = new Date().getTime()
        this.state = {
            startTime: timestamp - 86400000 * 3,
            endTime: timestamp + 86400000 * 3,
            containerOffSet: new Animated.Value(0),
            containerWidth: undefined,
            recordW: 0,
            recordH: 0,
            btnDisable: true,//查询按钮不可点击
        };
    }

    formatDate(date) {
        let now = new Date(date);
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var date = now.getDate();
        return year + "—" + month + "-" + date;
    }

    componentDidMount() {
        this.listView.beginRefresh();
    }

    componentWillReceiveProps(nextProps) {
        if ((this.props.refreshState == 1 || this.props.refreshState == 2) && (nextProps.refreshState !== 1 && nextProps.refreshState !== 2)) {
            this.timer = setTimeout(() => {
                this.setState({ btnDisable: false })
            }, 300);

        } else {
            this.setState({ btnDisable: true })
        }
    }

    componentWillUnmount() {
        this.timer && clearTimeout(this.timer);
    }

    onHeaderRefresh = () => {
        this.loadList(false);
    };

    //根View的onLayout回调函数
    onLayout = (event) => {
        //获取根View的宽高，以及左上角的坐标值
        let { x, y, width, height } = event.nativeEvent.layout;
        this.setState({
            recordW: width,
            recordH: height,
        });
    }

    get translateX() {
        const x = 0;
        return this.state.containerOffSet.interpolate({
            inputRange: [-1, 0, x, x + 1],
            outputRange: [0, 0, 0, 1],
        });
    }

    supportedOrientations = Orientations.LANDSCAPE;

    loadList() {
        this.props.getWharfPlanePromise({
            // startTime: this.formatDate(this.state.startTime),
            // endTime: this.formatDate(this.state.endTime),
            startTime: this.state.startTime,
            endTime: this.state.endTime,
        }).catch(() => {
        });
    }

    renderItem = ({ item }) => (
        <WharfPlaneItem
            item={item}
            headerTranslateX={this.translateX}
        />
    );

    renderFilter() {
        let btnBackColor = this.state.btnDisable ? "#B5B5B5" : "#D90A25"
        return (
            <View style={{ flexDirection: 'row', backgroundColor: 'white' }}>
                <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
                    <Item style={styles.selectTimeItem}
                    >
                        <Label style={styles.selectTimeLabel}>起始时间:</Label>
                        <DatePullSelector
                            value={this.state.startTime}
                            type={DATE_WHEEL_TYPE.DATE}
                            onChangeValue={(value) => {
                                this.setState({ startTime: value });
                            }}
                        />
                    </Item>
                    <Item style={styles.selectTimeItem}
                    >
                        <Label style={styles.selectTimeLabel}>截止时间:</Label>
                        <DatePullSelector
                            value={this.state.endTime}
                            type={DATE_WHEEL_TYPE.DATE}
                            onChangeValue={(value) => {
                                this.setState({ endTime: value });
                            }}
                        />
                    </Item>
                    <Item style={[styles.selectTimeItem, { justifyContent: 'center' }]}
                    >
                        <Button
                            style={{ marginRight: 15, height: 25, backgroundColor: btnBackColor }}
                            // danger
                            disabled={this.state.btnDisable}
                            // small
                            onPress={() => {
                                if (!this.state.startTime || this.state.startTime.length == 0) {
                                    AlertView.show({ message: '请填写起始时间' })
                                    return
                                }
                                if (!this.state.endTime || this.state.endTime.length == 0) {
                                    AlertView.show({ message: '请填写截止时间' })
                                    return
                                }
                                this.setState({ btnDisable: true })
                                this.listViewRef._flatListRef.scrollToIndex({ animated: false, index: 0, viewPosition: 0 })
                                this.listView.beginRefresh()
                            }}
                        >
                            <Text style={{ height: 25, fontSize: 14, lineHeight: 25 }}>查询</Text>
                        </Button>
                    </Item>
                </SafeAreaView>
            </View>
        );
    }

    render() {
        return (
            <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
                <StatusBar backgroundColor="#DC001B" barStyle="light-content" hidden={true} translucent={true}/>
                {this.renderFilter()}
                <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
                    <View
                        style={{ width: '100%', flex: 1 }}
                        onLayout={(event) => {
                            const { width } = event.nativeEvent.layout;
                            this.setState({ containerWidth: width });
                        }}
                    >
                        <Animated.ScrollView
                            onScroll={Animated.event([{
                                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
                            }], { useNativeDriver: true })}
                            scrollEventThrottle={1}
                            horizontal
                        >
                            <View style={{ flex: 1, minWidth: '100%' }}>
                                <View style={styles.container}>
                                    <View style={styles.col}>
                                        <Text style={styles.text}>码头</Text>
                                    </View>
                                    <View style={[styles.col, { width: 100 }]}>
                                        <Text style={styles.text}>船名</Text>
                                    </View>
                                    <View style={styles.col}>
                                        <Text style={styles.text}>航次</Text>
                                    </View>
                                    <View style={styles.col}>
                                        <Text style={styles.text}>进出口</Text>
                                    </View>
                                    <View style={[styles.col, { width: 140 }]}>
                                        <Text style={styles.text}>预靠泊时间</Text>
                                    </View>
                                    <View style={[styles.col, { width: 140 }]} onLayout={this.onLayout} >
                                        <Text style={styles.text}>预离泊时间</Text>
                                    </View>
                                    <View style={[styles.col, { width: 140 }]}>
                                        <Text style={styles.text}>实际靠泊时间</Text>
                                    </View>
                                    <View style={[styles.col, { width: 140 }]}>
                                        <Text style={styles.text}>实际离泊时间</Text>
                                    </View>
                                    <View style={[styles.col, { width: 90 }]}>
                                        <Text style={styles.text}>预靠前后泊</Text>
                                    </View>
                                    <View style={[styles.col, { width: 90 }]}>
                                        <Text style={styles.text}>实靠前后泊</Text>
                                    </View>
                                    <View style={[styles.col, { width: 90 }]}>
                                        <Text style={styles.text}>预计吊桥数</Text>
                                    </View>
                                    <Animated.View style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        transform: [{ translateX: this.translateX }],
                                    }}
                                    >
                                        <View style={styles.container}>
                                            <View style={[styles.col, { height: this.state.recordH }]}>
                                                <Text style={styles.text}>码头</Text>
                                            </View>
                                            <View style={[styles.col, { width: 100 }]}>
                                                <Text style={styles.text}>船名</Text>
                                            </View>
                                        </View>
                                    </Animated.View>
                                </View>
                                <RefreshListView
                                    ref={(ref) => { this.listView = ref; }}
                                    listRef={(ref) => { this.listViewRef = ref; }}
                                    headerTranslateX={this.translateX}
                                    containerWidth={this.state.containerWidth}
                                    data={this.props.list || []}
                                    style={{ flex: 1 }}
                                    keyExtractor={item => `${item.id}`}
                                    renderItem={this.renderItem}
                                    refreshState={this.props.refreshState}
                                    onHeaderRefresh={this.onHeaderRefresh}
                                // onFooterRefresh={this.onFooterRefresh}
                                />
                            </View>
                        </Animated.ScrollView>
                    </View>
                </SafeAreaView>
            </Container>
        );
    }
}

WharfPlane.navigationOptions = () => ({
    title: '码头计划',
});

WharfPlane.propTypes = {
    navigation: PropTypes.object.isRequired,
    getWharfPlanePromise: PropTypes.func.isRequired,
    list: PropTypes.array.isRequired,
    refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
    refreshState: makeRefreshState(),
    list: makeList(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getWharfPlanePromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(WharfPlane);