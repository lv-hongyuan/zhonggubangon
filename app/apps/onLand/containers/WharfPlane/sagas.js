import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getWharfPlaneRoutine } from './actions';

function* getDirectRate(action) {
  console.log(action);
  try {
    yield put(getWharfPlaneRoutine.request());
    const response = yield call(request, ApiFactory.wharfPlaneList(action.payload));
    console.log('码头计划:',response);
    yield put(getWharfPlaneRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getWharfPlaneRoutine.failure(e));
  } finally {
    yield put(getWharfPlaneRoutine.fulfill());
  }
}

export function* getWharfPlaneSaga() {
  yield takeLatest(getWharfPlaneRoutine.TRIGGER, getDirectRate);
}

// All sagas to be loaded
export default [getWharfPlaneSaga];
