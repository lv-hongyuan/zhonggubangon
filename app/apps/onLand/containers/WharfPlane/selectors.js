import { createSelector } from 'reselect/es';

const selectWharfPlanDomain = () => state => state.wharfPlane;

const makeList = () => createSelector(selectWharfPlanDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectWharfPlanDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
