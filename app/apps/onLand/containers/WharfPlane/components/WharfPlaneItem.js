import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class WharfPlaneItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      portName, shipName, voyageCode, inOutType, etb, etd, atb, atd,preBerth, actBerth, preBridgeCrane,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{portName}</Text>
        </View>
        <View style={[styles.col, { width: 100 }]}>
          <Text style={styles.text}>{shipName}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{voyageCode}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{inOutType === 'I' ? '进口' : '出口'}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{etb}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{etd}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{atb}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{atd}</Text>
        </View>
        <View style={[styles.col,{width:90}]}>
          <Text style={styles.text}>{preBerth}</Text>
        </View>
        <View style={[styles.col,{width:90}]}>
          <Text style={styles.text}>{actBerth}</Text>
        </View>
        <View style={[styles.col,{width:90}]}>
          <Text style={styles.text}>{preBridgeCrane}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{portName}</Text>
            </View>
            <View style={[styles.col, { width: 100 }]}>
              <Text style={styles.text}>{shipName}</Text>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

WharfPlaneItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default WharfPlaneItem;
