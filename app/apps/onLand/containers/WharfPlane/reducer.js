import { getWharfPlaneRoutine } from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getWharfPlaneRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getWharfPlaneRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getWharfPlaneRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getWharfPlaneRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
