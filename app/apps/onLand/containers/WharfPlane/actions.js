import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_WHARF_PLANE } from './constants';

export const getWharfPlaneRoutine = createRoutine(GET_WHARF_PLANE);
export const getWharfPlanePromise = promisifyRoutine(getWharfPlaneRoutine);