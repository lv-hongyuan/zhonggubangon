import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getDirectRateRankingRoutine } from './actions';

function* getDirectRateRanking(action) {
  console.log(action);
  try {
    yield put(getDirectRateRankingRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.pqBerthEfficiencyList(startTime, endTime));
    console.log(response);
    yield put(getDirectRateRankingRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getDirectRateRankingRoutine.failure(e));
  } finally {
    yield put(getDirectRateRankingRoutine.fulfill());
  }
}

export function* getDirectRateRankingSaga() {
  yield takeLatest(getDirectRateRankingRoutine.TRIGGER, getDirectRateRanking);
}

// All sagas to be loaded
export default [getDirectRateRankingSaga];
