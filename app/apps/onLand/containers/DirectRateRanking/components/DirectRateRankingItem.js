import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: '33.3%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class DirectRateRankingItem extends React.PureComponent {
  render() {
    const { pq, directBerthPercent, ranking } = this.props.item || {};

    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{directBerthPercent}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{ranking}</Text>
        </View>
      </View>
    );
  }
}

DirectRateRankingItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default DirectRateRankingItem;
