import { createSelector } from 'reselect/es';

const selectDirectRateRankingDomain = () => state => state.directRateRanking;

const makeList = () => createSelector(selectDirectRateRankingDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectDirectRateRankingDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
