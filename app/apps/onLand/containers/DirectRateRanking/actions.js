import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_DIRECT_RATE_RANKING } from './constants';

export const getDirectRateRankingRoutine = createRoutine(GET_DIRECT_RATE_RANKING);
export const getDirectRateRankingPromise = promisifyRoutine(getDirectRateRankingRoutine);
