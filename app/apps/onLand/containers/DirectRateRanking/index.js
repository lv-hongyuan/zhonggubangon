//! *********** 此页面已弃用 ***************
//! *********** 直靠率排名 ***************
//! *********** 此页面已弃用 ***************
import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Item,
  Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getDirectRateRankingPromise } from './actions';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import DirectRateRankingItem from './components/DirectRateRankingItem';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from '../../../../common/commonStyles';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';
import DatePullSelector from '../../../../components/DatePullSelector';

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: '33.3%',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
  },
};

@screenHOC
class DirectRateRanking extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };
  supportedOrientations = Orientations.ALL;

  loadList() {
    this.props.getDirectRateRankingPromise({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    }).catch(() => {
    });
  }

  renderItem = ({ item }) => (
    <DirectRateRankingItem item={item} />
  );

  renderFilter() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>起始时间:</Label>
            <DatePullSelector
              value={this.state.startTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ startTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>截止时间:</Label>
            <DatePullSelector
              value={this.state.endTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ endTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
        </SafeAreaView>
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <ScrollView
          contentContainerStyle={{ minWidth: '100%' }}
          horizontal
          contentInsetAdjustmentBehavior="scrollableAxes"
        >
          <View style={{ width: '100%', flex: 1 }}>
            <View style={styles.container}>
              <View style={styles.col}>
                <Text style={styles.text}>片区</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>直靠率</Text>
              </View>
              <View style={styles.col}>
                <Text style={styles.text}>排名</Text>
              </View>
            </View>
            <RefreshListView
              ref={(ref) => { this.listView = ref; }}
              data={this.props.list}
              style={{ flex: 1 }}
              keyExtractor={item => `${item.id}`}
              renderItem={this.renderItem}
              refreshState={this.props.refreshState}
              onHeaderRefresh={this.onHeaderRefresh}
              // onFooterRefresh={this.onFooterRefresh}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

DirectRateRanking.navigationOptions = () => ({
  title: '各片区直靠率排名',
});

DirectRateRanking.propTypes = {
  getDirectRateRankingPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
DirectRateRanking.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getDirectRateRankingPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DirectRateRanking);
