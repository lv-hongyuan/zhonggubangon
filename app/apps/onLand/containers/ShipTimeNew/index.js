//!船期表
import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, StatusBar, Text, DeviceEventEmitter,Platform } from 'react-native'
// import HeaderButtons from 'react-navigation-header-buttons';
import { createMaterialTopTabNavigator } from 'react-navigation';
import myTheme from '../../../../Themes';
import {
  ROUTE_SHIP_TIME_NEW_LEFT,
  ROUTE_SHIP_TIME_NEW_RIGHT,
} from '../../RouteConstant';
import { NavigationActions } from 'react-navigation';
import screenHOC from '../../../../components/screenHOC';
import ShipTimeNewRight from '../ShipTimeNewRight';
import ShipTimeNewLeft from '../ShipTimeNewLeft';
import { ROUTE_SHIP_TIME_NEW_SEARCH } from '../../RouteConstant';
import Svg from '../../../../components/Svg';
import { iphoneX } from '../../../../utils/iphoneX-helper';
const IS_ANDROID = Platform.OS === 'ios' ? false: true;
const TopTabNavigator = createMaterialTopTabNavigator({
  [ROUTE_SHIP_TIME_NEW_LEFT]: { screen: ShipTimeNewLeft },
  [ROUTE_SHIP_TIME_NEW_RIGHT]: { screen: ShipTimeNewRight },
}, {
  swipeEnabled: false,
  backBehavior: 'none',
  tabBarOptions: {
    allowFontScaling:false,
    activeTintColor: '#ED1727',
    inactiveTintColor: '#444',
    style: {
      backgroundColor: '#fff',
    },
    tabStyle: {
      backgroundColor: 'transparent',
      borderBottomColor: myTheme.borderColor,
      borderBottomWidth: myTheme.borderWidth,
    },
    indicatorStyle: {
      backgroundColor: '#ED1727',
    },
    labelStyle: { fontSize: 16 },
  },
});

@screenHOC
class ShipTimeNew extends React.PureComponent {
  static router = {
    ...TopTabNavigator.router,
    getStateForAction: (action, lastState) => {
      const state = TopTabNavigator.router.getStateForAction(action, lastState);
      if (lastState === state) {
        return state;
      }
      const routes = state.routes && state.routes.map(route => ({
        ...route,
        params: { ...route.params, ...state.params, readOnly: true },
      }));
      return { ...state, routes };
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      searchShipName: '',
      searchPortName: '',
      searchPq: '',
    };
  }
  componentDidMount() {
    //  监听搜索条件
    this.listener = DeviceEventEmitter.addListener('addServiceAudits', (data) => {
      this.setState({
        searchPq: data.searchPq,
        searchShipName: data.searchShipName,
        searchPortName: data.searchPortName
      })
    });
  }

  //  移除监事件
  componentWillUnmount() {
    this.listener.remove();
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          hidden={false}
          barStyle={'light-content'}
          style={{ backgroundColor: '#DC001B' }}
        />
        <View style={{
          backgroundColor: '#DC001B',
          height: iphoneX ? 88 : IS_ANDROID ? 56 : 64,
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 10,
          paddingTop: iphoneX ? 44 : IS_ANDROID ? 0 :20,
          marginTop: 0,
          justifyContent: 'space-between'
        }}>
          <TouchableOpacity
            onPress={() => { this.props.navigation.dispatch(NavigationActions.back()) }}
            style={{
              width: 40,
              height: 40,
              justifyContent: 'center'
            }}>
            <Svg icon="icon_back" size="20" color="white" />
          </TouchableOpacity>
          <View>
            <Text style={{ color: '#fff', fontSize: 18, fontWeight: '700' }}>船期表</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(ROUTE_SHIP_TIME_NEW_SEARCH, { data: this.state })
            }}
            style={{
              height: 30,
              justifyContent: 'center'
            }}>
            <Text style={{ color: '#fff', fontSize: 16 }}>检索</Text>
          </TouchableOpacity>
        </View>
        <TopTabNavigator
          navigation={this.props.navigation}
        />
      </View>

    );
  }
}

ShipTimeNew.navigationOptions = () => ({
  headerLeft: (
    null
  ),
  headerStyle: {
    height: 0,
    marginTop: iphoneX ? -44 : -30,
  },
});

export default connect()(ShipTimeNew);
