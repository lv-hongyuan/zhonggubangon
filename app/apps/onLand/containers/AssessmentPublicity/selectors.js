import { createSelector } from 'reselect/es';

const selectEditionListDomain = () => state => state.assessmentPublicity;

const makeList = () => createSelector(selectEditionListDomain(), (subState) => {
  return subState.list;
});

const makeRefreshState = () => createSelector(selectEditionListDomain(), (subState) => {
  return subState.refreshState;
});

export { makeList, makeRefreshState };
