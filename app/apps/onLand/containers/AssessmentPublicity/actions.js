import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_EDITION_LIST } from './constants';

export const getEditionListRoutine = createRoutine(GET_EDITION_LIST);
export const getEditionListPromiseCreator = promisifyRoutine(getEditionListRoutine);
