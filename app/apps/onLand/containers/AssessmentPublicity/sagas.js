import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getEditionListRoutine } from './actions';

function* getEditionList(action) {
  console.log('考核公示请求开始++',action);
  try {
    yield put(getEditionListRoutine.request());
    const response = yield call(request, ApiFactory.getEditionList(action.payload));
    console.log('getKaohe+++',JSON.stringify(response));
    yield put(getEditionListRoutine.success(response));

  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getEditionListRoutine.failure(e));
  } finally {
    yield put(getEditionListRoutine.fulfill());
  }
}

export function* getEditionListSaga() {
  yield takeLatest(getEditionListRoutine.TRIGGER, getEditionList);
}

// All sagas to be loaded
export default [getEditionListSaga];
