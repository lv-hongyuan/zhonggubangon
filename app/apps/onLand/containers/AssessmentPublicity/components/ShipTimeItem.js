import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';
import { defaultFormat } from '../../../../../utils/DateFormat';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
  },
});

class ShipTimeItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      feemon, businessWeek, lineType,
    } = (this.props.item || {});
    return (
      <TouchableHighlight onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={[styles.col, { width: 120 }]}>
            <Text style={styles.text}>{feemon}</Text>
          </View>
          {/*<View style={[styles.col, { width: 80 }]}>*/}
          {/*  <Text style={styles.text}>{voyageCode}</Text>*/}
          {/*</View>*/}
          <View style={[styles.col, { width: 100 }]}>
            <Text style={styles.text}>{businessWeek}</Text>
          </View>
          <View style={[styles.col, { flex:1 }]}>
            <Text style={styles.text}>{lineType}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

ShipTimeItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
  headerTranslateX: PropTypes.object.isRequired,
};
ShipTimeItem.defaultProps = {
  onSelect: undefined,
};

export default ShipTimeItem;
