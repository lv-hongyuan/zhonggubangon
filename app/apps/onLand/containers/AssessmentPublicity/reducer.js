import { getEditionListRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getEditionListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getEditionListRoutine.SUCCESS:
      console.log('船期表数据:',fixIdList(action.payload))
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getEditionListRoutine.FAILURE:
      console.log('船期表数据失败:',action.payload)
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getEditionListRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
