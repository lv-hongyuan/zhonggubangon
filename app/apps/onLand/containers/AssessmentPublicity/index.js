import React from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, Animated, Keyboard} from 'react-native';
import { connect } from 'react-redux';
import {
  Button,
  Container, InputGroup, Item, Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getEditionListPromiseCreator } from './actions';
import {ROUTE_PORT_TIME_RANKING, ROUTE_SHIP_TIME_DETAIL,ROUTE_ASSESSMENT_CLASS} from '../../RouteConstant';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import ShipTimeItem from './components/ShipTimeItem';
import RefreshListView from '../../../../components/RefreshListView';
import HeaderButtons from "react-navigation-header-buttons";
import commonStyles from "../../../../common/commonStyles";
import InputItem from "../../../../components/InputItem";
import Selector from "../../../../components/Selector";
import {ReviewState, reviewTitleFromState,getLineType} from "../../common/Constant";
import Svg from "../../../../components/Svg";
import {NavigationActions} from "react-navigation";

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

@screenHOC
class AssessmentPublicity extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      containerOffSet: new Animated.Value(0),
      containerWidth: undefined,
      feemon:'',
      businessWeek:'',
      lineType:'',
      inputFeemon:'',
      inputBusinessWeek:'',
      inputLintype:'',
      showFilter: false,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      exitSearch: () => {
        this.setShowFilter(false);
      },
      setShowFilter: this.setShowFilter,
      setInputValue: this.setInputValue,
    });
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    console.log('+++点我了',JSON.stringify(item));
    this.props.navigation.navigate(
        ROUTE_ASSESSMENT_CLASS,
      {
        id: item.id,
        item,
        lineType:item.lineType,
        feemon:item.feemon,
        businessWeek:item.businessWeek,
        issueSequence:item.issueSequence,
      },
    );
  };

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  loadList(loadMore) {
    console.log('1111111++',this.state.inputFeemon,'!',this.state.inputBusinessWeek,'+', this.state.inputLintype)
    this.props.getEditionList({feemon:this.state.inputFeemon,
      businessWeek: this.state.inputBusinessWeek,
      lineType: this.state.inputLintype,}, loadMore).catch(() => {
    });
  }

  renderItem = ({ item }) => (
    <ShipTimeItem
      item={item}
      headerTranslateX={this.translateX}
      onSelect={this.onSelect}
    />
  );
  renderFilter() {
    return this.state.showFilter && (
        <View style={[styles.container, {
          position: 'absolute', top: 0, left: 0, bottom: 0, right: 0,
        }]}
        >
          <SafeAreaView style={{ width: '100%' }}>
            <View style={{ padding: 10 }}>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    autoFocus
                    label="运费月:"
                    returnKeyType="search"
                    value={this.state.inputFeemon}
                    clearButtonMode="while-editing"
                    keyboardType="numbers-and-punctuation"
                    onChangeText={(text) => {
                      this.setState({ inputFeemon: text });
                    }}
                    onSubmitEditing={this.doSearch}
                    onFocus={() => {
                      this.setShowFilter(true);
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                    label="业务周:"
                    returnKeyType="search"
                    value={this.state.inputBusinessWeek}
                    clearButtonMode="while-editing"
                    keyboardType="numeric"
                    onChangeText={(text) => {
                      this.setState({ inputBusinessWeek: text });
                    }}
                    onSubmitEditing={this.doSearch}
                    onFocus={() => {
                      this.setShowFilter(true);
                    }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>航线类型:</Label>
                  <Selector
                      value={this.state.inputLintype}
                      items={getLineType}
                      getItemValue={item => item}
                      getItemText={item => item}
                      onSelected={(item) => {
                        this.setState({ inputLintype: item });
                      }}
                  />
                </Item>
              </InputGroup>
            </View>
            <Button
                onPress={this.doSearch}
                block
                style={{
                  height: 45, margin: 10, justifyContent: 'center', backgroundColor: '#DC001B',
                }}
            >
              <Text style={{ color: '#ffffff' }}>搜索</Text>
            </Button>
          </SafeAreaView>
        </View>
    );
  }
  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{ width: '100%', flex: 1 }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <View style={{ flex: 1, minWidth: '100%' }}>
              <View style={styles.container}>
                <View style={[styles.col, { width: 120 }]}>
                  <Text style={styles.text}>运费月</Text>
                </View>
                {/*<View style={[styles.col, { width: 80 }]}>*/}
                {/*  <Text style={styles.text}>航次</Text>*/}
                {/*</View>*/}
                <View style={[styles.col, { width: 100 }]}>
                  <Text style={styles.text}>业务周</Text>
                </View>
                <View style={[styles.col, { flex:1 }]}>
                  <Text style={styles.text}>航线类型</Text>
                </View>
              </View>
              <RefreshListView
                ref={(ref) => { this.listView = ref; }}
                headerTranslateX={this.translateX}
                containerWidth={this.state.containerWidth}
                data={this.props.list || []}
                style={{ flex: 1 }}
                keyExtractor={item => `${item.id}`}
                renderItem={this.renderItem}
                refreshState={this.props.refreshState}
                onHeaderRefresh={this.onHeaderRefresh}
                // onFooterRefresh={this.onFooterRefresh}
              />
            </View>
          </View>
        </SafeAreaView>
        {this.renderFilter()}
      </Container>
    );
  }

  setInputValue = (setInputValue) =>{
    console.log('setInputValue',setInputValue);
    this.setState({
      inputFeemon: this.state.feemon,
      inputBusinessWeek: this.state.businessWeek,
      inputLintype: this.state.lineType,
    });
  }
  //show出来搜索页面
  setShowFilter = (showFilter) => {
    this.props.navigation.setParams({
      showFilter,
    });
    this.setState({ showFilter });
  };

  doSearch = () => {
    Keyboard.dismiss();
    this.setSearchValue();
    this.setShowFilter(false);
    this.listView.beginRefresh();
  };

  setSearchValue(callBack) {
    this.setState({
      feemon: this.state.inputFeemon,
      businessWeek: this.state.inputBusinessWeek,
      lineType: this.state.inputLintype,
    }, callBack);
  }
}


AssessmentPublicity.navigationOptions = ({ navigation }) => ({
  title: '考核公示',
  headerRight: (
      <HeaderButtons>
      {navigation.getParam('showFilter', false) ? (
       null ) : (<HeaderButtons.Item
              title="检索"
              buttonStyle={{
                fontSize: 14,
                color: '#ffffff',
              }}
              onPress={()=>{
                const setInputValue =  navigation.getParam('setInputValue');
                const setShowFilter =  navigation.getParam('setShowFilter');
                setShowFilter(true);
              }
              }
          />
      )}
      </HeaderButtons>
  ),headerLeft: (
      <HeaderButtons>
        {navigation.getParam('showFilter', false) ? (
            <HeaderButtons.Item
                title="取消"
                buttonStyle={{ fontSize: 14, color: '#ffffff' }}
                onPress={() => {
                  const exitSearch = navigation.getParam('exitSearch');
                  exitSearch();
                }}
            />
        ) : (
            <HeaderButtons.Item
                title=""
                buttonWrapperStyle={{ padding: 10 }}
                ButtonElement={<Svg icon="icon_back" size="20" color="white" />}
                onPress={() => {
                  navigation.dispatch(NavigationActions.back());
                }}
            />
        )}
      </HeaderButtons>
  ),
});

AssessmentPublicity.propTypes = {
  navigation: PropTypes.object.isRequired,
  getEditionList: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
};
AssessmentPublicity.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getEditionList:getEditionListPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentPublicity);
