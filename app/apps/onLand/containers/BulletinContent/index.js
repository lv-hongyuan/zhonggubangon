
//  公告栏详情  /  航次指令详情

import React from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet, View, Dimensions, DeviceEventEmitter,Text,
} from 'react-native';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect/es';
import {
    Container, Content, Form, 
} from 'native-base';
import {bindPromiseCreators} from 'redux-saga-routines/es';
import {getPublishNoticeReadPromise,getChangeStopRemindPromise} from './actions';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import {defaultFormat} from "../../../../utils/DateFormat";
import {ROUTE_ASSESSMENT_CLASS, ROUTE_EMERGENCY_EDIT} from "../../RouteConstant";
import {makeList, makeRefreshState, makeIsLoading} from "../BulletinContent/selectors";
import HeaderButtons from "react-navigation-header-buttons";

let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    form: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 8,
        paddingTop: 20,
        flex: 1,
    },
    contentView: {
        width: deviceWidth - 40,
        textAlign: 'center',
    },
    contentText: {
        fontSize: 16,
        color: '#535353',
        lineHeight: 22,
    },
    khText: {
        fontSize: 16,
        color: '#EB7C3B',
        lineHeight: 22,
    },
    dateView: {
        marginTop: 7,
        // width: deviceWidth-30,
    },
    publisherText: {
        textAlign: 'right',
        paddingRight: 20,
        fontSize: 16,
        color: '#535353',
        width: deviceWidth - 20,
    },
    dateText: {
        textAlign: 'right',
        paddingRight: 20,
        fontSize: 16,
        color: '#535353',
        width: deviceWidth - 20,
    }
});

@screenHOC
class BulletinContent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params || {};
        console.log('this.params:',this.params);
        //isPush 0:不是来自推送的消息 1:推送过来的消息
        this.state = {
            detailContent: '',
            publishDate: '',
            publisher: '',
            lineType: '',
            feemon: '',
            businessWeek: '',
            issueSequence: '',
            type: '',
            headContent: '',
            footContent: '',
            isContent: false,
            isPush: this.params.isPush,
            remindState:0,
        }
        this.props.navigation.setParams({
            changeStop: this.changeStop,
        });
    }

    componentDidMount() {
        this.loadData(this.params.id)
        this.listener = DeviceEventEmitter.addListener('refreshMessageState',(params)=>{
            this.loadData(params.id)
        });
    }
    componentWillUnmount() {
        this.listener.remove();
    }
    //如果富文本可以点击调用此方法
    pushPage() {
        this.props.navigation.navigate(
            ROUTE_ASSESSMENT_CLASS,
            {
                lineType: this.state.lineType,
                feemon: this.state.feemon,
                businessWeek: this.state.businessWeek,
                issueSequence: this.state.issueSequence,
            },
        );
    }

    loadData(id) {
        if(this.params.item && this.params.item.state == 0){
            DeviceEventEmitter.emit('getPush',{id,state:this.state.isPush})
        }
        this.props.getPublishNoticeRead({
            id: id,
            isPush: this.state.isPush,
        })
            .then(() => {
                if(this.params.isPush == 1 || this.params.item.state == 0){
                    //如果是推送消息或者未读信息，刷新角标状态和列表数据
                    DeviceEventEmitter.emit('refreshCornerMarkState')
                }
                this.props.navigation.setParams({
                    remindState:this.props.workDetail.remindState,
                    type:this.props.workDetail.type
                })
                if (this.props.workDetail != null) {
                    this.setState({
                        detailContent: this.props.workDetail.content,
                        publishDate: this.props.workDetail.publishDate,
                        publisher: this.props.workDetail.publisher,
                        lineType: this.props.workDetail.lineType,
                        feemon: this.props.workDetail.feemon,
                        businessWeek: this.props.workDetail.businessWeek,
                        issueSequence: this.props.workDetail.issueSequence,
                        type: this.props.workDetail.type,
                        title: this.props.workDetail.title,
                        remindState:this.props.workDetail.remindState,
                    })
                }
            })
            .catch(() => {
            });
    }

    changeStop = () => {
        this.props.getChangeStopRemind({
            id: this.params.id,
        }).then(() => {
            console.log('fasdaddaa')
            this.props.navigation.setParams({
                remindState: 1,
            });
            DeviceEventEmitter.emit('getPush')
        }).catch(() => {
            console.log('fffff')
            });
    };

    render() {
        const {detailContent, headContent, footContent, publishDate, publisher, feemon, businessWeek, type, isContent} = this.state;
        return (
            <Container theme={myTheme} style={{backgroundColor: '#FFFFFF'}}>
                <Content contentInsetAdjustmentBehavior="scrollableAxes">
                    {type != 30 ? <Form style={styles.form}>
                        <Text style={styles.contentText} selectable={true}>
                            {detailContent}
                        </Text>
                        <View style={styles.dateView}>
                            <Text style={styles.publisherText} selectable={true}>
                                {publisher}
                            </Text>
                        </View>
                        <View style={styles.dateView}>
                            <Text style={styles.dateText} selectable={true}>
                                {!!publishDate && defaultFormat(publishDate)}
                            </Text>
                        </View>
                    </Form> : <Form style={styles.form}>
                        <View style={styles.contentView}>
                            {businessWeek == 0 ? <Text style={styles.contentText}>
                                {feemon + '考核已发布,请在'}<Text style={styles.khText} onPress={() => {
                                this.pushPage()
                            }}>【考核公示】</Text><Text style={styles.contentText}>查询!</Text>
                            </Text> : <Text style={styles.contentText}>
                                {feemon + '业务周' + businessWeek + '考核已发布,请在'}<Text style={styles.khText} onPress={() => {
                                this.pushPage()
                            }}>【考核公示】</Text><Text style={styles.contentText}>查询!</Text>
                            </Text>}
                        </View>
                        <View style={styles.dateView}>
                            <Text style={styles.publisherText} selectable={true}>
                                {publisher}
                            </Text>
                        </View>
                        <View style={styles.dateView}>
                            <Text style={styles.dateText} selectable={true}>
                                {!!publishDate && defaultFormat(publishDate)}
                            </Text>
                        </View>
                    </Form>}

                </Content>
            </Container>
        );
    }
}

BulletinContent.navigationOptions = ({navigation}) => ({
    title: navigation.getParam('title'),
    headerRight: (
        <HeaderButtons>
        {(navigation.getParam('remindState') == 0 && navigation.getParam('type')== 70) ?
                <HeaderButtons.Item
                    title="不再提醒"
                    buttonStyle={{
                        fontSize: 14,
                        color: '#ffffff',
                    }}
                    onPress={navigation.getParam('changeStop')}
                />
             : null}
        </HeaderButtons>

    ),
    //如果是正常BulletinBoard页面过来的正常传值如果是推送过来的就请求结束赋值然后通过getParam传递过来
    // title: ((navigation.getParam('titleName'))!= null ? (navigation.getParam('titleName')) : (navigation.getParam('titleParam'))),
// title:(navigation.getParam('titleParam'))
});

BulletinContent.propTypes = {
    getPublishNoticeRead: PropTypes.func.isRequired,
    getChangeStopRemind: PropTypes.func.isRequired,
    workDetail: PropTypes.object,
};
BulletinContent.defaultProps = {
    workDetail: {},
};
const mapStateToProps = createStructuredSelector({
    isLoading: makeIsLoading(),
    refreshState: makeRefreshState(),
    workDetail: makeList(),
});

function mapDispatchToProps(dispatch) {
    return {
        ...bindPromiseCreators({
            getPublishNoticeRead: getPublishNoticeReadPromise,
            getChangeStopRemind: getChangeStopRemindPromise,
        }, dispatch),
        dispatch,
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(BulletinContent);
