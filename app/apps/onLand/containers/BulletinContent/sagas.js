import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {getPublishNoticeReadRoutine,getChangeStopRemindRoutine} from "./actions";

function* getPublishNoticeRead(action) {
  console.log(action);
  try {
    yield put(getPublishNoticeReadRoutine.request());
    console.log('yidu//////',JSON.stringify(action.payload))
    const response = yield call(request, ApiFactory.getPublishNoticeRead(action.payload));
    console.log('得到数据+++',JSON.stringify(response));
    yield put(getPublishNoticeReadRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPublishNoticeReadRoutine.failure(e));
  } finally {
    yield put(getPublishNoticeReadRoutine.fulfill());
  }
}

export function* getPublishNoticeReadSaga() {
  yield takeLatest(getPublishNoticeReadRoutine.TRIGGER, getPublishNoticeRead);
}

function* changeStopRemindState(action) {
  console.log(action);
  try {
    yield put(getChangeStopRemindRoutine.request());
    const response = yield call(request, ApiFactory.changeStopRemindState(action.payload));
    console.log('aaaa+++',JSON.stringify(response));
    yield put(getChangeStopRemindRoutine.success());
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getChangeStopRemindRoutine.failure(e));
  } finally {
    yield put(getChangeStopRemindRoutine.fulfill());
  }
}

export function* changeStopRemindStateSaga() {
  yield takeLatest(getChangeStopRemindRoutine.TRIGGER, changeStopRemindState);
}

// All sagas to be loaded
export default [getPublishNoticeReadSaga,changeStopRemindStateSaga];
