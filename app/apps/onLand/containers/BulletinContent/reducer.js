import { bulletinContentRoutine } from './actions';
import {getPublishNoticeReadRoutine,getChangeStopRemindRoutine} from "./actions";
import {RefreshState} from "../../../../components/RefreshListView";

const defaultState = {
  loadingError: null,
  isLoading: false,
  workDetail: null,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case getPublishNoticeReadRoutine.TRIGGER:
      return { ...state, isLoading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPublishNoticeReadRoutine.SUCCESS:
      console.log('$$$$',action.payload);
      return { ...state, workDetail: action.payload, refreshState: RefreshState.NoMoreData };
    case getPublishNoticeReadRoutine.FAILURE:
      return {
        ...state, workDetail: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPublishNoticeReadRoutine.FULFILL:
      return { ...state, isLoading: false };

    case getChangeStopRemindRoutine.TRIGGER:
      return { ...state, isLoading: true };
    case getChangeStopRemindRoutine.SUCCESS:
      return { ...state };
    case getChangeStopRemindRoutine.FAILURE:
      return {
        ...state, error: action.payload
      };
    case getChangeStopRemindRoutine.FULFILL:
      return { ...state, isLoading: false };


    default:
      return state;
  }
}
