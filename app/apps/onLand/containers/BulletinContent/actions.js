import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_PUBLICH_NOTICE_READ,STOP_REMIND } from './constants';

export const getPublishNoticeReadRoutine =createRoutine(GET_PUBLICH_NOTICE_READ,
        updates => updates,
    () => ({ globalLoading: true })
);
export const getPublishNoticeReadPromise = promisifyRoutine(getPublishNoticeReadRoutine)

export const getChangeStopRemindRoutine =createRoutine(STOP_REMIND,
    updates => updates,
    () => ({ globalLoading: true })
);
export const getChangeStopRemindPromise = promisifyRoutine(getChangeStopRemindRoutine)
