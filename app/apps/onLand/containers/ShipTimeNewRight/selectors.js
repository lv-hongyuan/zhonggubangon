import { createSelector } from 'reselect';

const selectShipTimeNewRightDomain = () => state => state.shipTimeNewRight;
const makeList = () => createSelector(selectShipTimeNewRightDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectShipTimeNewRightDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

const makeDefaultType = () => createSelector(selectShipTimeNewRightDomain(), (subState) => {
  console.debug(subState.defaultType);
  return subState.defaultType
})

export { makeList, makeRefreshState,makeDefaultType };