import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import CarouselLabel from '../../../../../components/CarouselLabel';
const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: '#fff',
    flexDirection: 'column',
    borderBottomColor: '#ccc',
    borderBottomWidth: 0.7,
    
  },
  col: {
    height: 35,
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10
  },
  texttop: {
    width: '40%',
  },
  textbottom: {
    marginRight: 5
  },
  title:{
    alignContent: 'stretch',
    height:40,
    justifyContent:'center',
    paddingLeft:25,
    borderBottomColor:'#e8e8e8',
    borderBottomWidth:1,
    borderTopWidth:10,
    borderTopColor:'#e8e8e8',
  }
});

class ShipTimeNewItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const {
      shipName,       //  船名 --> 海蓝中谷
      voyageCode,     //  航次 --> 1911s
      lineName,       //  航线 --> 上海-【广州】
      prevAt,         //  上港实际时间 --> 0600/06
      prevEt,         //  上港预计时间 --> 0100/06
      nextEt,         //  下港预计时间 --> 0100/06
      nextAt,         //  下港实际时间 --> 0100/06
      nextPortName,   //  下港名称 --> 上海
      prevPortName,   //  上港名称 --> 广州
      nextState,      //  下港状态 --> 抵港
      prevState,      //  上港状态 --> 离港
      isFirst,    //是否显示title
      port,       //本港
    } = (this.props.item || {});
    let preTextBgc = prevAt ? '#000' : 'orange';
    let nextTextBgc = nextAt ? '#000' : 'orange';
    const type = this.props.type
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.onSelect(this.props.item);
        }}
      >
        <View style={styles.container}>
          {isFirst == 1 && type == 30 ?
            <View style={styles.title}>
              <Text style={{fontSize:15,color:'rgb(60,150,200)'}}>{port}</Text>
            </View> : null}
          <View style={styles.col}>
            <Text style={styles.texttop}>{shipName + '/' + voyageCode}</Text>
            <Text style={{ marginLeft: 20, width: 75, }}>{prevPortName}</Text>
            <Text style={styles.textbottom}>{prevState + ':'}</Text>
            <Text style={{ color: preTextBgc }}>{prevAt ? prevAt : prevEt}</Text>
          </View>
          <View style={styles.col}>
            {
              lineName.length > 12 ? <CarouselLabel
                bgViewStyle={{
                  overflow: 'hidden',
                  width: '40%',
                }}
                textContainerHeight={30}
                speed={50}
                text={lineName ? lineName : '暂无数据......'}
                textStyle={{
                  fontSize: 13,
                  color: '#000',
                  lineHeight: 30,
                  width: lineName.length * 15
                }}
              /> :
                <Text style={styles.texttop}>{lineName}</Text>
            }
            <Text style={{ marginLeft: 20, width: 75 }}>{nextPortName}</Text>
            <Text style={styles.textbottom}>{nextState + ':'}</Text>
            <Text style={{ color: nextTextBgc }}>{nextAt ? nextAt : nextEt}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

ShipTimeNewItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
  type:PropTypes.number.isRequired,
};
ShipTimeNewItem.defaultProps = {
  onSelect: undefined,
};

export default ShipTimeNewItem;
