import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getShipTimeNewRightRoutine } from './actions';

function* getShipTimeNewRight(action) {
  const { page, pageSize, ...rest} = (action.payload || {});
  try {
    yield put(getShipTimeNewRightRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).forEach((key) => {
      const value = rest[key];
      param.filter.filters.push({
        field: key,
        operator: 'eq',
        value,
      });
    });
    console.log('param:',param)
    const response = yield call(request, ApiFactory.getShipScheduleList(param));
    console.log(response);
    yield put(getShipTimeNewRightRoutine.success({
      list:response.dtoList ? response.dtoList.content : [],
      page,
      pageSize,
      type:response.type,
      backmes:response._backmes
    }));
  } catch (error) {
    yield put(errorMessage(error));
    yield put(getShipTimeNewRightRoutine.failure({
      page,
      pageSize, 
      error
    }));
  } finally {
    yield put(getShipTimeNewRightRoutine.fulfill());
  }
}

export function* getShipTimeNewRightSaga() {
  yield takeLatest(getShipTimeNewRightRoutine.TRIGGER, getShipTimeNewRight);
}
export default [getShipTimeNewRightSaga];