import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SHIP_TIME_NEW_RIGHT } from './constants';

export const getShipTimeNewRightRoutine = createRoutine(GET_SHIP_TIME_NEW_RIGHT);
export const getShipTimeNewRightPromiseCreator = promisifyRoutine(getShipTimeNewRightRoutine);