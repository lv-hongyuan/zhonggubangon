import { createSelector } from 'reselect/es';

const selectBulletinBoardDomain = () => state => state.bulletinBoard;

const makeList = () => createSelector(selectBulletinBoardDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});
const makeselectArr = () => createSelector(selectBulletinBoardDomain(), (subState) => {
  console.debug(subState);
  return subState.selectArr;
});

const makeRefreshState = () => createSelector(selectBulletinBoardDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

const makeLoading = () => createSelector(selectBulletinBoardDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeList, makeRefreshState, makeLoading,makeselectArr };
