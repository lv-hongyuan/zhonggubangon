import { createRoutine, promisifyRoutine} from 'redux-saga-routines/es';
import { GET_ANNOUNCEMENT_LIST, GET_PUBLISH_NOTICE_READ_ID,GETMUITIPLE, CLEANSELECTARR,UPDATEMULTIPLE,SELECT_DATA_ALL } from './constants';

export const getAnnouncementListRoutine = createRoutine(GET_ANNOUNCEMENT_LIST);
export const getAnnouncementListPromise = promisifyRoutine(getAnnouncementListRoutine);

export const getPublishNoticeReadRoutine =createRoutine(GET_PUBLISH_NOTICE_READ_ID);
export const getPublishNoticeReadPromise = promisifyRoutine(getPublishNoticeReadRoutine)

export const getMultipleRoutine =createRoutine(GETMUITIPLE);
export const getMultiplePromise = promisifyRoutine(getMultipleRoutine);

export const cleanSelectArrRoutine = createRoutine(CLEANSELECTARR);
export const cleanSelectArrPromise = promisifyRoutine(cleanSelectArrRoutine);

export const upDataMultipleRoutine = createRoutine(UPDATEMULTIPLE);
export const upDataMultiplePromise = promisifyRoutine(upDataMultipleRoutine);