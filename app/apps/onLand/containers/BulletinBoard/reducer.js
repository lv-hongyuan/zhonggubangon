import { getAnnouncementListRoutine, getPublishNoticeReadRoutine, getMultipleRoutine, cleanSelectArrRoutine, upDataMultipleRoutine} from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';
import _ from "lodash";

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
  success: false,
  isLoading: false,
  selectArr:[]
};

export default function (state = initState, action) {
  switch (action.type) {
    case getAnnouncementListRoutine.TRIGGER:{
      const { page } = action.payload
      return {
        ...state,
        loading:true,
        refreshState: page > 1 ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing
      }
    }
    case getAnnouncementListRoutine.SUCCESS:{
      const { page,pageSize, list} = action.payload
      return {
        ...state,
        list: page == 1 ? fixIdList(list) : fixIdList(state.list.concat(list)),
        refreshState: list.length < pageSize ? RefreshState.NoMoreData : RefreshState.Idle
      }
    }
    case getAnnouncementListRoutine.FAILURE:{
      const { page,error} = action.payload;
      return{
        ...state,
        list: page == 1 ? [] : state.list,
        refreshState:RefreshState.Failure
      }
    }
    case getAnnouncementListRoutine.FULFILL:
      return { ...state, loading: false };

    case getPublishNoticeReadRoutine.TRIGGER:
      return { ...state, loading: true };
    case getPublishNoticeReadRoutine.SUCCESS:
      const newList = state.list.map((item) => {
        if (item.id === action.payload) {
          const newItem = _.cloneDeep(item);
          newItem.state = 1;
          return newItem;
        }
        return item;
      });
      // const badge = newList.reduce((pre, current) => {
      //   const unRead = current.state === 0 || current.state === 1;
      //   return pre + (unRead ? 1 : 0);
      // }, 0);
      return { ...state, list: newList };

      // return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPublishNoticeReadRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload
      };
    case getPublishNoticeReadRoutine.FULFILL:
      return { ...state, loading: false };

    case getMultipleRoutine.SUCCESS:{
      return {
        ...state, 
        list: action.payload.list,
        selectArr:action.payload.selectArr
      }
    }

    case cleanSelectArrRoutine.SUCCESS:{
      return {
        ...state,
        selectArr:action.payload.selectArr,
        list:action.payload.list
      }
    }

    case upDataMultipleRoutine.TRIGGER:
      return {...state,loading: true}

    case upDataMultipleRoutine.SUCCESS:
      return {...state}

    case upDataMultipleRoutine.FAILURE:
      return{...state,error:action.payload}
    
    case upDataMultipleRoutine.FULFILL:
      return {...state, loading:false}
    default:
      return state;
  }
}
