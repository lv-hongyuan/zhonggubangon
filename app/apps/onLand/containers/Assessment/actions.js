import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_TIMELINESS_DEDUCTION,
  GET_PQ_TIMELINESS_DEDUCTION,
  GET_OPERATION_DEDUCTION,
  GET_PQ_OPERATION_DEDUCTION,
} from './constants';

export const getTimelinessDeductionRoutine = createRoutine(GET_TIMELINESS_DEDUCTION);
export const getTimelinessDeductionPromise = promisifyRoutine(getTimelinessDeductionRoutine);

export const getPqTimelinessDeductionRoutine = createRoutine(GET_PQ_TIMELINESS_DEDUCTION);
export const getPqTimelinessDeductionPromise = promisifyRoutine(getPqTimelinessDeductionRoutine);

export const getOperationDeductionRoutine = createRoutine(GET_OPERATION_DEDUCTION);
export const getOperationDeductionPromise = promisifyRoutine(getOperationDeductionRoutine);

export const getPqOperationDeductionRoutine = createRoutine(GET_PQ_OPERATION_DEDUCTION);
export const getPqOperationDeductionPromise = promisifyRoutine(getPqOperationDeductionRoutine);
