import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
  getTimelinessDeductionRoutine,
  getPqTimelinessDeductionRoutine,
  getOperationDeductionRoutine,
  getPqOperationDeductionRoutine,
} from './actions';

function* getTimelinessDeduction(action) {
  console.log(action);
  try {
    yield put(getTimelinessDeductionRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.timelinessDeductionList({ startTime, endTime }));
    console.log(response);
    yield put(getTimelinessDeductionRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTimelinessDeductionRoutine.failure(e));
  } finally {
    yield put(getTimelinessDeductionRoutine.fulfill());
  }
}

export function* getTimelinessDeductionSaga() {
  yield takeLatest(getTimelinessDeductionRoutine.TRIGGER, getTimelinessDeduction);
}

function* getPqTimelinessDeduction(action) {
  console.log(action);
  try {
    yield put(getPqTimelinessDeductionRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.timelinessDeductionPQList({ startTime, endTime }));
    console.log(response);
    yield put(getPqTimelinessDeductionRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPqTimelinessDeductionRoutine.failure(e));
  } finally {
    yield put(getPqTimelinessDeductionRoutine.fulfill());
  }
}

export function* getPqTimelinessDeductionSaga() {
  yield takeLatest(getPqTimelinessDeductionRoutine.TRIGGER, getPqTimelinessDeduction);
}

function* getOperationDeduction(action) {
  console.log(action);
  try {
    yield put(getOperationDeductionRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.operationDeductionList({ startTime, endTime }));
    console.log(response);
    yield put(getOperationDeductionRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getOperationDeductionRoutine.failure(e));
  } finally {
    yield put(getOperationDeductionRoutine.fulfill());
  }
}

export function* getOperationDeductionSaga() {
  yield takeLatest(getOperationDeductionRoutine.TRIGGER, getOperationDeduction);
}

function* getPqOperationDeduction(action) {
  console.log(action);
  try {
    yield put(getPqOperationDeductionRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.operationDeductionPQList({ startTime, endTime }));
    console.log(response);
    yield put(getPqOperationDeductionRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPqOperationDeductionRoutine.failure(e));
  } finally {
    yield put(getPqOperationDeductionRoutine.fulfill());
  }
}

export function* getPqOperationDeductionSaga() {
  yield takeLatest(getPqOperationDeductionRoutine.TRIGGER, getPqOperationDeduction);
}

// All sagas to be loaded
export default [
  getTimelinessDeductionSaga,
  getPqTimelinessDeductionSaga,
  getOperationDeductionSaga,
  getPqOperationDeductionSaga,
];
