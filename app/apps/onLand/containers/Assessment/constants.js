/*
 *
 * Assessment constants
 *
 */
export const GET_TIMELINESS_DEDUCTION = 'app/Assessment/GET_TIMELINESS_DEDUCTION';
export const GET_PQ_TIMELINESS_DEDUCTION = 'app/Assessment/GET_PQ_TIMELINESS_DEDUCTION';
export const GET_OPERATION_DEDUCTION = 'app/Assessment/GET_OPERATION_DEDUCTION';
export const GET_PQ_OPERATION_DEDUCTION = 'app/Assessment/GET_PQ_OPERATION_DEDUCTION';
