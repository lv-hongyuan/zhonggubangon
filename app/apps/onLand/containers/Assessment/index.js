//! ***********此页面已弃用***************
//! ***********此页面已弃用***************
//! ***********此页面已弃用***************
import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
  Item,
  Label,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  getTimelinessDeductionPromise,
  getPqTimelinessDeductionPromise,
  getOperationDeductionPromise,
  getPqOperationDeductionPromise,
} from './actions';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import OperationDeductionItem from './components/OperationDeductionItem';
import RefreshListView from '../../../../components/RefreshListView';
import commonStyles from '../../../../common/commonStyles';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';
import DatePullSelector from '../../../../components/DatePullSelector';
import TimelinessDeductionItem from './components/TimelinessDeductionItem';
import Selector from '../../../../components/Selector';

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

const ShowType = {
  TIME: 0,
  PQ_TIME: 1,
  OPERATION: 2,
  PQ_OPERATION: 3,
};

function showTypeTitle(showType) {
  switch (showType) {
    case ShowType.TIME:
      return '及时性考核';
    case ShowType.PQ_TIME:
      return '各片区及时性考核';
    case ShowType.OPERATION:
      return '操作性考核';
    case ShowType.PQ_OPERATION:
      return '各片区操作性考核';
    default:
      return '';
  }
}

@screenHOC
class Assessment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      showType: ShowType.TIME,
      recordW: 0,
      recordH: 0,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      showType: this.state.showType,
      onChangeShowType: this.onChangeShowType,
    });
  }

  onChangeShowType = (showType) => {
    this.props.navigation.setParams({ showType });
    this.setState({ showType }, () => {
      this.listView.beginRefresh();
    });
  };

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  //根View的onLayout回调函数
  onLayout = (event) => {
    //获取根View的宽高，以及左上角的坐标值
    let {x, y, width, height} = event.nativeEvent.layout;
    this.setState({
      recordW: width,
      recordH: height,
    });
  }

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    let getListFuc;
    switch (this.state.showType) {
      case ShowType.TIME:
        getListFuc = this.props.getTimelinessDeduction;
        break;
      case ShowType.PQ_TIME:
        getListFuc = this.props.getPqTimelinessDeduction;
        break;
      case ShowType.OPERATION:
        getListFuc = this.props.getOperationDeduction;
        break;
      case ShowType.PQ_OPERATION:
        getListFuc = this.props.getPqOperationDeduction;
        break;
      default:
        getListFuc = this.props.getTimelinessDeduction;
        break;
    }
    getListFuc({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    }).catch(() => {
    });
  }

  renderItem = ({ item }) => {
    switch (this.state.showType) {
      case ShowType.TIME:
        return (
          <TimelinessDeductionItem
            item={item}
            showPort
            headerTranslateX={this.translateX}
          />
        );
      case ShowType.PQ_TIME:
        return (
          <TimelinessDeductionItem
            item={item}
            showPort={false}
            headerTranslateX={this.translateX}
          />
        );
      case ShowType.OPERATION:
        return (
          <OperationDeductionItem
            item={item}
            showPort
            headerTranslateX={this.translateX}
          />
        );
      case ShowType.PQ_OPERATION:
        return (
          <OperationDeductionItem
            item={item}
            showPort={false}
            headerTranslateX={this.translateX}
          />
        );
      default:
        return '';
    }
  };

  renderFilter() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>起始时间:</Label>
            <DatePullSelector
              value={this.state.startTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ startTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5, marginBottom: 0, flex: 1, width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>截止时间:</Label>
            <DatePullSelector
              value={this.state.endTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ endTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
        </SafeAreaView>
      </View>
    );
  }

  renderHeader() {
    switch (this.state.showType) {
      case ShowType.TIME:
        return this.renderTimelinessHeader(true);
      case ShowType.PQ_TIME:
        return this.renderTimelinessHeader();
      case ShowType.OPERATION:
        return this.renderOperationHeader(true);
      case ShowType.PQ_OPERATION:
        return this.renderOperationHeader();
      default:
        return '';
    }
  }

  renderTimelinessHeader(showPort = false) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort && (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        )}
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>挂靠次数A</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>挂靠次数B</Text>
        </View>
        <View style={[styles.col, { width: 140 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>不及时次数</Text>
        </View>
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>得分</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, {height: this.state.recordH}]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort && (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  renderOperationHeader(showPort = false) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort && (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        )}
        <View style={[styles.col, { width: 140 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>挂靠次数A</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>挂靠次数B</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>靠泊得分</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>离泊得分</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>总分</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, {height: this.state.recordH}]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort && (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {/* {this.renderFilter()} */}
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{ width: '100%', flex: 1 }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
            >
              <View style={{ flex: 1, minWidth: '100%' }}>
                {this.renderHeader()}
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={this.props.list || []}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

Assessment.navigationOptions = ({ navigation }) => ({
  headerTitle: (
    <View style={{ flex: 1 }}>
      <View style={{ alignSelf: 'center' }}>
        <Selector
          style={{ backgroundColor: 'transparent', borderWidth: 0 }}
          valueStyle={{
            textAlign: 'left', fontSize: 18, paddingLeft: 0, color: '#FFFFFF',
          }}
          iconTintColor="#FFFFFF"
          icon={undefined}
          value={navigation.getParam('showType')}
          items={Object.values(ShowType)}
          getItemValue={item => item}
          getItemText={item => showTypeTitle(item)}
          onSelected={(item) => {
            const onChangeShowType = navigation.getParam('onChangeShowType');
            if (onChangeShowType) onChangeShowType(item);
          }}
        />
      </View>
    </View>
  ),
  // title: navigation.getParam('title', 'APP考核'),
});

Assessment.propTypes = {
  navigation: PropTypes.object.isRequired,
  list: PropTypes.number.isRequired,
  refreshState: PropTypes.number.isRequired,
  getTimelinessDeduction: PropTypes.func.isRequired,
  getPqTimelinessDeduction: PropTypes.func.isRequired,
  getOperationDeduction: PropTypes.func.isRequired,
  getPqOperationDeduction: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getTimelinessDeduction: getTimelinessDeductionPromise,
      getPqTimelinessDeduction: getPqTimelinessDeductionPromise,
      getOperationDeduction: getOperationDeductionPromise,
      getPqOperationDeduction: getPqOperationDeductionPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Assessment);
