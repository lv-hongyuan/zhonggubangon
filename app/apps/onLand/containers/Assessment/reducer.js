import {
  getTimelinessDeductionRoutine,
  getPqTimelinessDeductionRoutine,
  getOperationDeductionRoutine,
  getPqOperationDeductionRoutine,
} from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getTimelinessDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getTimelinessDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getTimelinessDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getTimelinessDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getPqTimelinessDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPqTimelinessDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPqTimelinessDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPqTimelinessDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getOperationDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getOperationDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getOperationDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getOperationDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getPqOperationDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPqOperationDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPqOperationDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPqOperationDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
