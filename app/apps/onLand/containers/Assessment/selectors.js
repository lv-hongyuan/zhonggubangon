import { createSelector } from 'reselect/es';

const selectAssessmentDomain = () => state => state.assessment;

const makeList = () => createSelector(selectAssessmentDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectAssessmentDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
