import {
  getBerthAssessListDeductionRoutine,
  getAppAssessListDeductionRoutine,
  getOpAssessListDeductionRoutine,
  getPortAssessListDeductionRoutine,
  getTimeInPortAssessListDeductionRoutine,
  getBerthAssessPqListDeductionRoutine,
  getAppAssessPqListDeductionRoutine,
  getOpAssessPqListDeductionRoutine,
  getTimeInPortAssessPqListDeductionRoutine,
  getPortAssessPqListDeductionRoutine,
  getPortAssessWharfListRoutine,
  getBerthAssessWharfListRoutine,
  getAppAssessWharfListRoutine,
  getOpAssessWharfListRoutine,
  getTimeInPortAssessWharfListRoutine,
} from './actions';
import fixIdList from '../../../../utils/fixIdList';
import { RefreshState } from '../../../../components/RefreshListView';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
};

export default function (state = initState, action) {
  switch (action.type) {
    case getPortAssessListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPortAssessListDeductionRoutine.SUCCESS:
      console.log('综合++',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPortAssessListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPortAssessListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getBerthAssessListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getBerthAssessListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getBerthAssessListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getBerthAssessListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getAppAssessListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getAppAssessListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getAppAssessListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getAppAssessListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getOpAssessListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getOpAssessListDeductionRoutine.SUCCESS:
      console.log('得到靠离数据了+++',fixIdList(action.payload))
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getOpAssessListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getOpAssessListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getTimeInPortAssessListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getTimeInPortAssessListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getTimeInPortAssessListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getTimeInPortAssessListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getBerthAssessPqListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getBerthAssessPqListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getBerthAssessPqListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getBerthAssessPqListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getAppAssessPqListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getAppAssessPqListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getAppAssessPqListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getAppAssessPqListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getOpAssessPqListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getOpAssessPqListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getOpAssessPqListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getOpAssessPqListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getTimeInPortAssessPqListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getTimeInPortAssessPqListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getTimeInPortAssessPqListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getTimeInPortAssessPqListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getPortAssessPqListDeductionRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPortAssessPqListDeductionRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPortAssessPqListDeductionRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPortAssessPqListDeductionRoutine.FULFILL:
      return { ...state, loading: false };

    case getPortAssessWharfListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getPortAssessWharfListRoutine.SUCCESS:
      console.log('综合++码头：',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getPortAssessWharfListRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getPortAssessWharfListRoutine.FULFILL:
      return { ...state, loading: false };
    
    case getBerthAssessWharfListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getBerthAssessWharfListRoutine.SUCCESS:
      console.log('直靠++码头',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getBerthAssessWharfListRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getBerthAssessWharfListRoutine.FULFILL:
      return { ...state, loading: false };

    case getAppAssessWharfListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getAppAssessWharfListRoutine.SUCCESS:
      console.log('APP考核++码头：',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getAppAssessWharfListRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getAppAssessWharfListRoutine.FULFILL:
      return { ...state, loading: false };

    case getOpAssessWharfListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getOpAssessWharfListRoutine.SUCCESS:
      console.log('靠离++码头',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getOpAssessWharfListRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getOpAssessWharfListRoutine.FULFILL:
      return { ...state, loading: false };

    case getTimeInPortAssessWharfListRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getTimeInPortAssessWharfListRoutine.SUCCESS:
      console.log('在港停时++码头',action.payload,'!!!',RefreshState.NoMoreData)
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getTimeInPortAssessWharfListRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getTimeInPortAssessWharfListRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
