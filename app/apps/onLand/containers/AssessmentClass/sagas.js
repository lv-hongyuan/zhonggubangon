import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import {
  getBerthAssessListDeductionRoutine,
  getAppAssessListDeductionRoutine,
  getOpAssessListDeductionRoutine,
  getPortAssessListDeductionRoutine,
  getTimeInPortAssessListDeductionRoutine,
  getBerthAssessPqListDeductionRoutine,
  getAppAssessPqListDeductionRoutine,
  getOpAssessPqListDeductionRoutine,
  getTimeInPortAssessPqListDeductionRoutine,
  getPortAssessPqListDeductionRoutine,
  getPortAssessWharfListRoutine,
  getBerthAssessWharfListRoutine,
  getAppAssessWharfListRoutine,
  getOpAssessWharfListRoutine,
  getTimeInPortAssessWharfListRoutine,
} from './actions';

//港口
function* getPortAssessList(action) {
  console.log(action);
  try {
    yield put(getPortAssessListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getPortAssessList(action.payload));
    console.log('综合考核港口++',JSON.stringify(response));
    yield put(getPortAssessListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPortAssessListDeductionRoutine.failure(e));
  } finally {
    yield put(getPortAssessListDeductionRoutine.fulfill());
  }
}

export function* getPortAssessListDeductionSaga() {
  yield takeLatest(getPortAssessListDeductionRoutine.TRIGGER, getPortAssessList);
}

function* getBerthAssessList(action) {
  console.log(action);
  try {
    yield put(getBerthAssessListDeductionRoutine.request());
    console.log('getBerth++',JSON.stringify(action));
    const response = yield call(request, ApiFactory.getBerthAssessList(action.payload));
    yield put(getBerthAssessListDeductionRoutine.success(response));
    console.log('2response+++++',JSON.stringify(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthAssessListDeductionRoutine.failure(e));
    console.log('3response+++++',e);
  } finally {
    yield put(getBerthAssessListDeductionRoutine.fulfill());
  }
}

export function* getBerthAssessListDeductionSaga() {
  yield takeLatest(getBerthAssessListDeductionRoutine.TRIGGER, getBerthAssessList);
}

function* getAppAssessList(action) {
  console.log(action);
  try {
    yield put(getAppAssessListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getAppAssessList(action.payload));
    console.log('APP考核!!!',JSON.stringify(response));
    yield put(getAppAssessListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getAppAssessListDeductionRoutine.failure(e));
  } finally {
    yield put(getAppAssessListDeductionRoutine.fulfill());
  }
}

export function* getAppAssessListDeductionSaga() {
  yield takeLatest(getAppAssessListDeductionRoutine.TRIGGER, getAppAssessList);
}

function* getOpAssessList(action) {
  console.log(action);
  try {
    yield put(getOpAssessListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getOpAssessList(action.payload));
    console.log('靠离+++',JSON.stringify(response));
    yield put(getOpAssessListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getOpAssessListDeductionRoutine.failure(e));
  } finally {
    yield put(getOpAssessListDeductionRoutine.fulfill());
  }
}

export function* getOpAssessListDeductionSaga() {
  yield takeLatest(getOpAssessListDeductionRoutine.TRIGGER, getOpAssessList);
}

function* getTimeInPortAssessList(action) {
  console.log(action);
  try {
    yield put(getTimeInPortAssessListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getTimeInPortAssessList(action.payload));
    console.log('靠离考核!!!',JSON.stringify(response));
    yield put(getTimeInPortAssessListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTimeInPortAssessListDeductionRoutine.failure(e));
  } finally {
    yield put(getTimeInPortAssessListDeductionRoutine.fulfill());
  }
}

export function* getTimeInPortAssessListDeductionSaga() {
  yield takeLatest(getTimeInPortAssessListDeductionRoutine.TRIGGER, getTimeInPortAssessList);
}

//片区
function* getBerthAssessPqList(action) {
  console.log(action);
  try {
    yield put(getBerthAssessPqListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getBerthAssessPqList(action.payload));
    console.log('片区Berth!!!',JSON.stringify(response));
    yield put(getBerthAssessPqListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthAssessPqListDeductionRoutine.failure(e));
  } finally {
    yield put(getBerthAssessPqListDeductionRoutine.fulfill());
  }
}

export function* getBerthAssessPqListDeductionSaga() {
  yield takeLatest(getBerthAssessPqListDeductionRoutine.TRIGGER, getBerthAssessPqList);
}

function* getAppAssessPqList(action) {
  console.log(action);
  try {
    yield put(getAppAssessPqListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getAppAssessPqList(action.payload));
    console.log(response);
    yield put(getAppAssessPqListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getAppAssessPqListDeductionRoutine.failure(e));
  } finally {
    yield put(getAppAssessPqListDeductionRoutine.fulfill());
  }
}

export function* getAppAssessPqListDeductionSaga() {
  yield takeLatest(getAppAssessPqListDeductionRoutine.TRIGGER, getAppAssessPqList);
}

function* getOpAssessPqList(action) {
  console.log(action);
  try {
    yield put(getOpAssessPqListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getOpAssessPqList(action.payload));
    console.log('片区靠离++',JSON.stringify(response));
    yield put(getOpAssessPqListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getOpAssessPqListDeductionRoutine.failure(e));
  } finally {
    yield put(getOpAssessPqListDeductionRoutine.fulfill());
  }
}

export function* getOpAssessPqListDeductionSaga() {
  yield takeLatest(getOpAssessPqListDeductionRoutine.TRIGGER, getOpAssessPqList);
}

function* getTimeInPortAssessPqList(action) {
  console.log(action);
  try {
    yield put(getTimeInPortAssessPqListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getTimeInPortAssessPqList(action.payload));
    console.log('在港停时片区++',JSON.stringify(response));
    yield put(getTimeInPortAssessPqListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTimeInPortAssessPqListDeductionRoutine.failure(e));
  } finally {
    yield put(getTimeInPortAssessPqListDeductionRoutine.fulfill());
  }
}

export function* getTimeInPortAssessPqListDeductionSaga() {
  yield takeLatest(getTimeInPortAssessPqListDeductionRoutine.TRIGGER, getTimeInPortAssessPqList);
}

function* getPortAssessPqList(action) {
  console.log(action);
  try {
    yield put(getPortAssessPqListDeductionRoutine.request());
    const response = yield call(request, ApiFactory.getPortAssessPqList(action.payload));
    console.log(response);
    yield put(getPortAssessPqListDeductionRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPortAssessPqListDeductionRoutine.failure(e));
  } finally {
    yield put(getPortAssessPqListDeductionRoutine.fulfill());
  }
}

export function* getPortAssessPqListDeductionSaga() {
  yield takeLatest(getPortAssessPqListDeductionRoutine.TRIGGER, getPortAssessPqList);
}


//! ***************码头综合得分列表***************
function* getPortAssessWharfList(action) {
  console.log(action);
  try {
    yield put(getPortAssessWharfListRoutine.request());
    const response = yield call(request, ApiFactory.getPortAssessWharfList(action.payload));
    console.log(response);
    yield put(getPortAssessWharfListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPortAssessWharfListRoutine.failure(e));
  } finally {
    yield put(getPortAssessWharfListRoutine.fulfill());
  }
}
export function* getPortAssessWharfListSaga() {
  yield takeLatest(getPortAssessWharfListRoutine.TRIGGER, getPortAssessWharfList);
}


//! ***************码头直靠考核列表***************
function* getBerthAssessWharfList(action) {
  console.log(action);
  try {
    yield put(getBerthAssessWharfListRoutine.request());
    const response = yield call(request, ApiFactory.getBerthAssessWharfList(action.payload));
    console.log(response);
    yield put(getBerthAssessWharfListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getBerthAssessWharfListRoutine.failure(e));
  } finally {
    yield put(getBerthAssessWharfListRoutine.fulfill());
  }
}
export function* getBerthAssessWharfListSaga() {
  yield takeLatest(getBerthAssessWharfListRoutine.TRIGGER, getBerthAssessWharfList);
} 


//! ***************码头APP考核列表***************
function* getAppAssessWharfList(action) {
  console.log(action);
  try {
    yield put(getAppAssessWharfListRoutine.request());
    const response = yield call(request, ApiFactory.getAppAssessWharfList(action.payload));
    console.log(response);
    yield put(getAppAssessWharfListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getAppAssessWharfListRoutine.failure(e));
  } finally {
    yield put(getAppAssessWharfListRoutine.fulfill());
  }
}
export function* getAppAssessWharfListSaga() {
  yield takeLatest(getAppAssessWharfListRoutine.TRIGGER, getAppAssessWharfList);
} 


//! ***************码头靠离考核列表***************
function* getOpAssessWharfList(action) {
  console.log(action);
  try {
    yield put(getOpAssessWharfListRoutine.request());
    const response = yield call(request, ApiFactory.getOpAssessWharfList(action.payload));
    console.log(response);
    yield put(getOpAssessWharfListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getOpAssessWharfListRoutine.failure(e));
  } finally {
    yield put(getOpAssessWharfListRoutine.fulfill());
  }
}
export function* getOpAssessWharfListSaga() {
  yield takeLatest(getOpAssessWharfListRoutine.TRIGGER, getOpAssessWharfList);
} 


//! ***************码头在港停时考核列表***************
function* getTimeInPortAssessWharfList(action) {
  console.log(action);
  try {
    yield put(getTimeInPortAssessWharfListRoutine.request());
    const response = yield call(request, ApiFactory.getTimeInPortAssessWharfList(action.payload));
    console.log(response);
    yield put(getTimeInPortAssessWharfListRoutine.success(response));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getTimeInPortAssessWharfListRoutine.failure(e));
  } finally {
    yield put(getTimeInPortAssessWharfListRoutine.fulfill());
  }
}
export function* getTimeInPortAssessWharfListSaga() {
  yield takeLatest(getTimeInPortAssessWharfListRoutine.TRIGGER, getTimeInPortAssessWharfList);
} 


// All sagas to be loaded
export default [
  getPortAssessListDeductionSaga,
  getBerthAssessListDeductionSaga,
  getAppAssessListDeductionSaga,
  getOpAssessListDeductionSaga,
  getTimeInPortAssessListDeductionSaga,
  getBerthAssessPqListDeductionSaga,
  getAppAssessPqListDeductionSaga,
  getOpAssessPqListDeductionSaga,
  getTimeInPortAssessPqListDeductionSaga,
  getPortAssessPqListDeductionSaga,
  getPortAssessWharfListSaga,
  getBerthAssessWharfListSaga,
  getAppAssessWharfListSaga,
  getOpAssessWharfListSaga,
  getTimeInPortAssessWharfListSaga,
];
