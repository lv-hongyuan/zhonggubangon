import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Animated,StatusBar } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import {
  getBerthAssessListDeductionPromise,
  getAppAssessListDeductionPromise,
  getOpAssessListDeductionPromise,
  getPortAssessListDeductionPromise,
  getTimeInPortAssessListDeductionPromise,
  getBerthAssessPqListDeductionPromise,
  getAppAssessPqListDeductionPromise,
  getOpAssessPqListDeductionPromise,
  getTimeInPortAssessPqListDeductionPromise,
  getPortAssessPqListDeductionPromise,
  getPortAssessWharfListPromise,
  getBerthAssessWharfListPromise,
  getAppAssessWharfListPromise,
  getOpAssessWharfListPromise,
  getTimeInPortAssessWharfListPromise,
} from './actions';
import { makeList, makeRefreshState } from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import OperationDeductionItem from './components/OperationDeductionItem';
import RefreshListView from '../../../../components/RefreshListView';
import TimelinessDeductionItem from './components/TimelinessDeductionItem';
import Selector from '../../../../components/Selector';
import PortAssessDeductionItem from './components/PortAssessDeductionItem';
import AppAssessDeductionItem from './components/AppAssessDeductionItem';
import TimeInPortAssessDeductionItem from './components/TimeInPortAssessDeductionItem'

const styles = {
  container: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

const ShowType = {
  ZH_OPERATION: 0,
  TIME: 1,
  PQ_TIME: 2,
  KL_OPERATION: 3,
  PQ_OPERATION: 4,
};
const RightShowType = {
  AREA_TYPE: 0, //!片区
  POET_TPYE: 1, //! 港口
  WHARF_TYPE: 2,  //! 码头
}

//! 选择列表后获取title文字
function showTypeTitle(showType) {
  switch (showType) {
    case ShowType.ZH_OPERATION:
      return '综合得分考核列表';
    case ShowType.TIME:
      return '直靠考核列表';
    case ShowType.PQ_TIME:
      return 'APP考核列表';
    case ShowType.KL_OPERATION:
      return '靠离考核列表';
    case ShowType.PQ_OPERATION:
      return '在港停时考核列表';
    default:
      return '';
  }
}

//! 选择右侧列表后获取title文字
function rightShowTypeTitle(showType) {
  switch (showType) {
    case RightShowType.AREA_TYPE:
      return '片区';
    case RightShowType.POET_TPYE:
      return '港口';
    case RightShowType.WHARF_TYPE:
      return '码头';
    default:
      return '';
  }
}

@screenHOC
class AssessmentClass extends React.PureComponent {
  constructor(props) {
    super(props);
    const assessmentList = this.props.navigation.state.params || {};
    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      showType: ShowType.ZH_OPERATION,          //!   默认标题选修框为  综合得分考核列表
      rightShowType: RightShowType.POET_TPYE,    //! 默认右侧选项框值为  港口
      recordW: 0,
      recordH: 0,
      lineType: assessmentList.lineType,
      feemon: assessmentList.feemon,
      businessWeek: assessmentList.businessWeek,
      issueSequence: assessmentList.issueSequence,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
    this.props.navigation.setParams({
      showType: this.state.showType,
      rightShowType: this.state.rightShowType,
      onChangeShowType: this.onChangeShowType,
      onChangeRightShowType: this.onChangeRightShowType,
    });
  }

  //! 切换title事件
  onChangeShowType = (showType) => {
    if(this._list){
      //!切换title后列表位置重置到初始位置，防止安卓端在切换title后界面出现未及时刷新的问题
      this._list._component.scrollTo({ x: 0, y: 0,animated: true })
    }
    this.props.navigation.setParams({ showType });
    this.setState({ showType }, () => {
      this.listView.beginRefresh();
    });
  };
  //! 切换右侧选择框事件
  onChangeRightShowType = (rightShowType) => {
    if(this._list){
      //!切换右侧选项后列表位置重置到初始位置，防止安卓端在切换右侧选项后界面出现未及时刷新的问题
      this._list._component.scrollTo({ x: 0, y: 0,animated: true })
    }
    this.props.navigation.setParams({ rightShowType });
    this.setState({ rightShowType }, () => {
      this.listView.beginRefresh();
    });
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  //根View的onLayout回调函数
  onLayout = (event) => {
    //获取根View的宽高，以及左上角的坐标值
    let { x, y, width, height } = event.nativeEvent.layout;
    this.setState({
      recordW: width,
      recordH: height,
    });
  }

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  //!加载数据
  loadList() {
    let { rightShowType } = this.state
    let getListFuc;
    switch (this.state.showType) {
      case ShowType.ZH_OPERATION: {
        //! 综合得分考核列表 数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getPortAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getPortAssessLis    
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getPortAssessWharfList 
        }
      }
        break;
      case ShowType.TIME: {
        //! 直靠考核列表 数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getBerthAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getBerthAssessList   
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getBerthAssessWharfList
        }
      }
        break;
      case ShowType.PQ_TIME: {
        //! APP考核列表 数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getAppAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getAppAssessList
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getAppAssessWharfList   
        }
      }
        break;
      case ShowType.KL_OPERATION: {
        //! 靠离考核列表 数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getOpAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getOpAssessList   
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getOpAssessWharfList    
        }
      }
        break;
      case ShowType.PQ_OPERATION: {
        //! 在港停时考核列表 数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getTimeInPortAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getTimeInPortAssessList    
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getTimeInPortAssessWharfList     
        }
      }
        break;
      default: {
        //! 默认为综合得分考核列表数据 :
        if (rightShowType == RightShowType.AREA_TYPE) {
          getListFuc = this.props.getPortAssessPqList
        } else if (rightShowType == RightShowType.POET_TPYE) {
          getListFuc = this.props.getPortAssessLis   
        } else if (rightShowType == RightShowType.WHARF_TYPE){
          getListFuc = this.props.getPortAssessWharfList    
        }
      }
        break;
    }
    getListFuc({
      lineType: this.state.lineType,
      feemon: this.state.feemon,
      businessWeek: this.state.businessWeek,
      issueSequence: this.state.issueSequence,

    }).catch(() => {
    });
  }

  renderItem = ({ item }) => {
    let { rightShowType } = this.state 
    switch (this.state.showType) {
      case ShowType.ZH_OPERATION:{
        return <PortAssessDeductionItem item={item} showPort={rightShowType} headerTranslateX={this.translateX}/>
      }
      case ShowType.TIME:{
        return <TimelinessDeductionItem item={item} showPort={rightShowType} headerTranslateX={this.translateX}/>
      }
      case ShowType.PQ_TIME:{
        return <AppAssessDeductionItem item={item} showPort={rightShowType} headerTranslateX={this.translateX}/>
      }
      case ShowType.KL_OPERATION:{
        return <OperationDeductionItem item={item} showPort={rightShowType} headerTranslateX={this.translateX}/>
      }
      case ShowType.PQ_OPERATION:{
        return <TimeInPortAssessDeductionItem item={item} showPort={rightShowType} headerTranslateX={this.translateX}/>
      }
      default:
        return '';
    }
  };

  //加载不同的页面
  renderHeader() {
    let { rightShowType } = this.state 
    switch (this.state.showType) {
      case ShowType.ZH_OPERATION:{
        return this.renderPortAssessHeader(rightShowType)
      }
      case ShowType.TIME:{
        return this.renderTimelinessHeader(rightShowType)
      }
      case ShowType.PQ_TIME:{
        return this.renderAppAssessHeader(rightShowType)
      }
      case ShowType.KL_OPERATION:{
        return this.renderOpAssessLHeader(rightShowType)
      }
      case ShowType.PQ_OPERATION:{
        return this.renderTimeInPortAssessHeader(rightShowType)
      }
      default:
        return '';
    }
  }
  //综合得分考核
  renderPortAssessHeader(showPort) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        ) : (showPort == 2 &&
          <View style={styles.col}>
            <Text style={styles.text}>码头</Text>
          </View>
        )}
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>直靠率得分</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>在港停时得分</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>APP靠泊得分</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>APP离泊得分</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>APP数据录入</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>实际航次数</Text>
        </View>
        <View style={[styles.col, { width: 115 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>参与考核航次数</Text>
        </View>
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>总分</Text>
        </View>

        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { height: this.state.recordH }]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            ):(showPort == 2 &&
              <View style={styles.col}>
                <Text style={styles.text}>码头</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  //直靠考核
  renderTimelinessHeader(showPort) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        ) : (showPort == 2 &&
          <View style={styles.col}>
            <Text style={styles.text}>码头</Text>
          </View>
        )}
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>总挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>考核挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>考核直靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>考核直靠率</Text>
        </View>
        {showPort == 2 && (<View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>直靠指标</Text>
        </View>)}
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>考核得分</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { height: this.state.recordH }]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            ):(showPort == 2 &&
              <View style={styles.col}>
                <Text style={styles.text}>码头</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  //APP考核
  renderAppAssessHeader(showPort) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        ) : (showPort == 2 &&
          <View style={styles.col}>
            <Text style={styles.text}>码头</Text>
          </View>
        )}
        <View style={[styles.col, { width: 120 }]}>
          <Text style={styles.text}>总挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 120 }]}>
          <Text style={styles.text}>考核挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 120 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>不及时次数</Text>
        </View>
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>得分</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { height: this.state.recordH }]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            ) : (showPort == 2 &&
              <View style={styles.col}>
                <Text style={styles.text}>码头</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  //靠离考核
  renderOpAssessLHeader(showPort) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        ) : (showPort == 2 &&
          <View style={styles.col}>
            <Text style={styles.text}>码头</Text>
          </View>
        )}
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>总挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>考核挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>靠泊得分</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>离泊得分</Text>
        </View>
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>总分</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { height: this.state.recordH }]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            ) : (showPort == 2 &&
              <View style={styles.col}>
                <Text style={styles.text}>码头</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  //在港停时考核
  renderTimeInPortAssessHeader(showPort) {
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>片区</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>港口</Text>
          </View>
        ) : (showPort == 2 &&
          <View style={styles.col}>
            <Text style={styles.text}>码头</Text>
          </View>
        )}
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>总挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>实际在港停时</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>考核挂靠次数</Text>
        </View>
        <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
          <Text style={styles.text}>考核在港停时</Text>
        </View>
        {showPort == 0 ? null : (
          <View style={[styles.col, { width: 105 }]} onLayout={this.onLayout} >
            <Text style={styles.text}>平均在港停时</Text>
          </View>
        )}
        {showPort == 0 ? null : (
          <View style={[styles.col, { width: 80 }]} onLayout={this.onLayout} >
            <Text style={styles.text}>平均指标</Text>
          </View>
        )}
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>考核得分</Text>
        </View>

        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.translateX }],
        }}
        >
          <View style={styles.container}>
            <View style={[styles.col, { height: this.state.recordH }]} >
              <Text style={styles.text}>片区</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>港口</Text>
              </View>
            ) : (showPort == 2 &&
              <View style={styles.col}>
                <Text style={styles.text}>码头</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <StatusBar backgroundColor="#DC001B" barStyle="light-content" hidden={true} translucent={true}/>
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{ width: '100%', flex: 1 }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
              ref={ref => (this._list = ref)}
            >
              <View style={{ flex: 1, minWidth: '100%' }}>
                {this.renderHeader()}
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={this.props.list || []}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

AssessmentClass.navigationOptions = ({ navigation }) => ({
  headerTitle: (
    <View style={{ flex: 1 }}>
      <View style={{ alignSelf: 'center' }}>
        <Selector
          style={{ backgroundColor: 'transparent', borderWidth: 0 }}
          valueStyle={{
            textAlign: 'left', fontSize: 18, paddingLeft: 0, color: '#FFFFFF',
          }}
          iconTintColor="#FFFFFF"
          icon={undefined}
          value={navigation.getParam('showType')}
          items={Object.values(ShowType)}
          getItemValue={item => item}
          getItemText={item => showTypeTitle(item)}
          onSelected={(item) => {
            const onChangeShowType = navigation.getParam('onChangeShowType');
            if (onChangeShowType) onChangeShowType(item);
          }}
        />
      </View>
    </View>
  ),
  headerRight: (
    <View style={{ flex: 1, marginRight: 10 }}>
      <View style={{ alignSelf: 'center' }}>
        <Selector
          style={{ backgroundColor: 'transparent', borderWidth: 0 }}
          valueStyle={{
            textAlign: 'left', fontSize: 15, paddingLeft: 0, color: '#FFFFFF',
          }}
          iconTintColor="#FFFFFF"
          icon={undefined}
          value={navigation.getParam('rightShowType')}
          items={Object.values(RightShowType)}
          getItemValue={item => item}
          getItemText={item => rightShowTypeTitle(item)}
          onSelected={(item) => {
            const onChangeRightShowType = navigation.getParam('onChangeRightShowType');
            if (onChangeRightShowType) onChangeRightShowType(item);
          }}
        />
      </View>
    </View>
  ),
});

AssessmentClass.propTypes = {
  pqItem: PropTypes.number.isRequired,
  navigation: PropTypes.object.isRequired,
  list: PropTypes.number.isRequired,
  refreshState: PropTypes.number.isRequired,
  getBerthAssessList: PropTypes.func.isRequired,
  getAppAssessList: PropTypes.func.isRequired,
  getOpAssessList: PropTypes.func.isRequired,
  getPortAssessLis: PropTypes.func.isRequired,
  getTimeInPortAssessList: PropTypes.func.isRequired,
  getBerthAssessPqList: PropTypes.func.isRequired,
  getAppAssessPqList: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getBerthAssessList: getBerthAssessListDeductionPromise,
      getAppAssessList: getAppAssessListDeductionPromise,
      getOpAssessList: getOpAssessListDeductionPromise,
      getPortAssessLis: getPortAssessListDeductionPromise,
      getTimeInPortAssessList: getTimeInPortAssessListDeductionPromise,
      getBerthAssessPqList: getBerthAssessPqListDeductionPromise,
      getAppAssessPqList: getAppAssessPqListDeductionPromise,
      getOpAssessPqList: getOpAssessPqListDeductionPromise,
      getTimeInPortAssessPqList: getTimeInPortAssessPqListDeductionPromise,
      getPortAssessPqList: getPortAssessPqListDeductionPromise,
      getPortAssessWharfList: getPortAssessWharfListPromise,
      getBerthAssessWharfList: getBerthAssessWharfListPromise,
      getAppAssessWharfList: getAppAssessWharfListPromise,
      getOpAssessWharfList: getOpAssessWharfListPromise,
      getTimeInPortAssessWharfList: getTimeInPortAssessWharfListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssessmentClass);
