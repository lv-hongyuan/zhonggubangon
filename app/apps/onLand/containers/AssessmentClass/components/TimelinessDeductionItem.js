
//! *********直靠考核列表********

import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    alignContent: 'stretch',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class TimelinessDeductionItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pq, port, berthNum, examBerthNum, examDirectBerthNum, examDirectRate, directRatio, score,wharf
    } = this.props.item || {};
    const { showPort } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        {showPort == 1 ? (
          <View style={styles.col}>
            <Text style={styles.text}>{port}</Text>
          </View>
        ) : showPort == 2 && (
          <View style={styles.col}>
            <Text style={styles.text}>{wharf}</Text>
          </View>
        )}
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>{berthNum}</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>{examBerthNum}</Text>
        </View>
        <View style={[styles.col, { width: 105 }]}>
          <Text style={styles.text}>{examDirectBerthNum}</Text>
        </View>
          <View style={[styles.col, { width: 105 }]}>
              <Text style={styles.text}>{examDirectRate}</Text>
          </View>
          {showPort == 2 && <View style={[styles.col, { width: 105 }]}>
              <Text style={styles.text}>{directRatio}</Text>
          </View>}
        <View style={[styles.col, { flex: 1 }]}>
          <Text style={styles.text}>{score}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pq}</Text>
            </View>
            {showPort == 1 ? (
              <View style={styles.col}>
                <Text style={styles.text}>{port}</Text>
              </View>
            ) : showPort == 2 && (
              <View style={styles.col}>
                <Text style={styles.text}>{wharf}</Text>
              </View>
            )}
          </View>
        </Animated.View>
      </View>
    );
  }
}

TimelinessDeductionItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
  showPort: PropTypes.number.isRequired,
};

export default TimelinessDeductionItem;
