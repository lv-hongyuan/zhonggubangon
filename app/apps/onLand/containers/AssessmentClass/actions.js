import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  GET_BERTH_ASSESSLIST,
  GET_APP_ASSESS_LIST,
  GET_OP_ASSESS_LIST,
  GET_PORT_ASSESS_LIST,
  GET_TIME_IN_PORT_ASSESS,
  GET_BERTH_ASSESS_PQ_LIST,
  GET_APP_ASSESS_PQ_LIST,
  GET_OP_ASSESS_PQ_LIST,
  GET_TIME_IN_PORT_ASSESS_PQ_LIST,
  GET_PORT_ASSESS_PQ_LIST,
  GET_PORT_ASSESS_WHARF_LIST,
  GET_BERTH_ASSESS_WHARF_LIST,
  GET_APP_ASSESS_WHARF_LIST,
  GET_OP_ASSESS_WHARF_LIST,
  GET_TIME_IN_PORT_ASSESS_WHARF_LIST,
} from './constants';

export const getBerthAssessListDeductionRoutine = createRoutine(GET_BERTH_ASSESSLIST);
export const getBerthAssessListDeductionPromise = promisifyRoutine(getBerthAssessListDeductionRoutine);

export const getAppAssessListDeductionRoutine = createRoutine(GET_APP_ASSESS_LIST);
export const getAppAssessListDeductionPromise = promisifyRoutine(getAppAssessListDeductionRoutine);

export const getOpAssessListDeductionRoutine = createRoutine(GET_OP_ASSESS_LIST);
export const getOpAssessListDeductionPromise = promisifyRoutine(getOpAssessListDeductionRoutine);

export const getPortAssessListDeductionRoutine = createRoutine(GET_PORT_ASSESS_LIST);
export const getPortAssessListDeductionPromise = promisifyRoutine(getPortAssessListDeductionRoutine);

export const getTimeInPortAssessListDeductionRoutine = createRoutine(GET_TIME_IN_PORT_ASSESS);
export const getTimeInPortAssessListDeductionPromise = promisifyRoutine(getTimeInPortAssessListDeductionRoutine);

export const getBerthAssessPqListDeductionRoutine = createRoutine(GET_BERTH_ASSESS_PQ_LIST);
export const getBerthAssessPqListDeductionPromise = promisifyRoutine(getBerthAssessPqListDeductionRoutine);

export const getAppAssessPqListDeductionRoutine = createRoutine(GET_APP_ASSESS_PQ_LIST);
export const getAppAssessPqListDeductionPromise = promisifyRoutine(getAppAssessPqListDeductionRoutine);

export const getOpAssessPqListDeductionRoutine = createRoutine(GET_OP_ASSESS_PQ_LIST);
export const getOpAssessPqListDeductionPromise = promisifyRoutine(getOpAssessPqListDeductionRoutine);

export const getTimeInPortAssessPqListDeductionRoutine = createRoutine(GET_TIME_IN_PORT_ASSESS_PQ_LIST);
export const getTimeInPortAssessPqListDeductionPromise = promisifyRoutine(getTimeInPortAssessPqListDeductionRoutine);

export const getPortAssessPqListDeductionRoutine = createRoutine(GET_PORT_ASSESS_PQ_LIST);
export const getPortAssessPqListDeductionPromise = promisifyRoutine(getPortAssessPqListDeductionRoutine);

export const getPortAssessWharfListRoutine = createRoutine(GET_PORT_ASSESS_WHARF_LIST);
export const getPortAssessWharfListPromise = promisifyRoutine(getPortAssessWharfListRoutine);

export const getBerthAssessWharfListRoutine = createRoutine(GET_BERTH_ASSESS_WHARF_LIST);
export const getBerthAssessWharfListPromise = promisifyRoutine(getBerthAssessWharfListRoutine);

export const getAppAssessWharfListRoutine = createRoutine(GET_APP_ASSESS_WHARF_LIST);
export const getAppAssessWharfListPromise = promisifyRoutine(getAppAssessWharfListRoutine);

export const getOpAssessWharfListRoutine = createRoutine(GET_OP_ASSESS_WHARF_LIST);
export const getOpAssessWharfListPromise = promisifyRoutine(getOpAssessWharfListRoutine);

export const getTimeInPortAssessWharfListRoutine = createRoutine(GET_TIME_IN_PORT_ASSESS_WHARF_LIST);
export const getTimeInPortAssessWharfListPromise = promisifyRoutine(getTimeInPortAssessWharfListRoutine);
