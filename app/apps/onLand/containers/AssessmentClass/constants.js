/*
 *
 * Assessment constants
 *
 */
export const GET_BERTH_ASSESSLIST = 'app/AssessmentClass/GET_BERTH_ASSESSLIST';
export const GET_APP_ASSESS_LIST = 'app/AssessmentClass/GET_APP_ASSESS_LIST';
export const GET_OP_ASSESS_LIST = 'app/AssessmentClass/GET_OP_ASSESS_LIST';
export const GET_PORT_ASSESS_LIST = 'app/AssessmentClass/GET_PORT_ASSESS_LIST';
export const GET_TIME_IN_PORT_ASSESS = 'app/AssessmentClass/GET_TIME_IN_PORT_ASSESS';
export const GET_BERTH_ASSESS_PQ_LIST = 'app/AssessmentClass/GET_BERTH_ASSESS_PQ_LIST';
export const GET_APP_ASSESS_PQ_LIST = 'app/AssessmentClass/GET_APP_ASSESS_PQ_LIST';
export const GET_OP_ASSESS_PQ_LIST = 'app/AssessmentClass/GET_OP_ASSESS_PQ_LIST';
export const GET_TIME_IN_PORT_ASSESS_PQ_LIST = 'app/AssessmentClass/GET_TIME_IN_PORT_ASSESS_PQ_LIST';
export const GET_PORT_ASSESS_PQ_LIST = 'app/AssessmentClass/GET_PORT_ASSESS_PQ_LIST';
export const GET_PORT_ASSESS_WHARF_LIST = 'app/AssessmentClass/GET_PORT_ASSESS_WHARF_LIST';
export const GET_BERTH_ASSESS_WHARF_LIST = 'app/AssessmentClass/GET_BERTH_ASSESS_WHARF_LIST';
export const GET_APP_ASSESS_WHARF_LIST = 'app/AssessmentClass/GET_APP_ASSESS_WHARF_LIST';
export const GET_OP_ASSESS_WHARF_LIST = 'app/AssessmentClass/GET_OP_ASSESS_WHARF_LIST';
export const GET_TIME_IN_PORT_ASSESS_WHARF_LIST = 'app/AssessmentClass/GET_TIME_IN_PORT_ASSESS_WHARF_LIST';
