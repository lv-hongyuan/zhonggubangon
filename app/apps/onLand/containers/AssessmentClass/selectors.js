import { createSelector } from 'reselect/es';

const selectAssessmentClassDomain = () => state => state.assessmentClass;

const makeList = () => createSelector(selectAssessmentClassDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeRefreshState = () => createSelector(selectAssessmentClassDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export { makeList, makeRefreshState };
