import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getEmergencyListRoutine, deleteEmergencyRoutine } from './actions';

function* getEmergencies(action) {
  console.log(action);
  try {
    yield put(getEmergencyListRoutine.request());
    const { id } = action.payload;
    const response = yield call(request, ApiFactory.getEmergenciesHistory({ id }));
    console.log('getEmergenciesHistory', response);
    yield put(getEmergencyListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getEmergencyListRoutine.failure(e));
  } finally {
    yield put(getEmergencyListRoutine.fulfill());
  }
}

export function* getEmergenciesSaga() {
  yield takeLatest(getEmergencyListRoutine.TRIGGER, getEmergencies);
}

function* deleteEmergency(action) {
  console.log(action);
  try {
    yield put(deleteEmergencyRoutine.request());
    const response = yield call(request, ApiFactory.deleteEvent(action.payload.id));
    console.log('deleteEmergency', response);
    yield put(deleteEmergencyRoutine.success(action.payload));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    if (error.response && error.response._backcode + '' === '830') {
      yield put(deleteEmergencyRoutine.success(error.response));
    } else {
      yield put(deleteEmergencyRoutine.failure(error));
    }
  } finally {
    yield put(deleteEmergencyRoutine.fulfill());
  }
}

export function* deleteEmergencySaga() {
  yield takeLatest(deleteEmergencyRoutine.TRIGGER, deleteEmergency);
}

// All sagas to be loaded
export default [getEmergenciesSaga, deleteEmergencySaga];
