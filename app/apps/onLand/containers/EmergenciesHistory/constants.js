/*
 *
 * EmergenciesHistory constants
 *
 */
export const GET_EMERGENCY_LIST = 'onLand/HistoryDetail/GET_EMERGENCY_LIST';
export const DELETE_EMERGENCY = 'onLand/HistoryDetail/DELETE_EMERGENCY';
