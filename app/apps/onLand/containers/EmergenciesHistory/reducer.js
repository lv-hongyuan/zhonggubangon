import { RefreshState } from '../../../../components/RefreshListView';
import { getEmergencyListRoutine, deleteEmergencyRoutine } from './actions';

const defaultState = {
  list: [],
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取突发事件
    case getEmergencyListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        loadingError: undefined,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getEmergencyListRoutine.SUCCESS: {
      // 列表处理
      const oldList = state.list;
      const { loadingError } = action.payload;
      const receiveList = action.payload || [];
      // let list = oldList.concat(receiveList);
      const list = receiveList;

      // 刷新状态
      let refreshState;
      if (list.length === 0) {
        refreshState = RefreshState.EmptyData;
      } else if (oldList === list) {
        refreshState = RefreshState.NoMoreData;
      } else {
        refreshState = RefreshState.NoMoreData;
      }
      return {
        ...state,
        list,
        loadingError,
        refreshState,
      };
    }
    case getEmergencyListRoutine.FAILURE:
      return {
        ...state,
        refreshState: RefreshState.Failure,
        loadingError: action.payload,
      };
    case getEmergencyListRoutine.FULFILL:
      return {
        ...state,
        isLoading: false,
      };

    // 删除突发事件
    case deleteEmergencyRoutine.TRIGGER:
      return {
        ...state,
        queryLoading: true,
        loadingError: undefined,
      };
    case deleteEmergencyRoutine.SUCCESS: {
      const emergency = action.payload;
      let newList = [];
      if (emergency) {
        newList = state.list.filter(item => item !== emergency);
      } else {
        newList = state.list;
      }
      return {
        ...state,
        list: newList,
        queryLoading: false,
      };
    }
    case deleteEmergencyRoutine.FAILURE:
      return {
        ...state,
        loadingError: action.payload,
      };
    case deleteEmergencyRoutine.FULFILL:
      return {
        ...state,
        queryLoading: false,
      };

    default:
      return state;
  }
}
