import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { Container } from 'native-base';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getEmergencyListPromise, deleteEmergencyPromise } from './actions';
import EmergencyItem from '../Emergencies/components/EmergencyItem';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';

import {
  makeDeleteLoading,
  makeSelectEmergencies,
  makeSelectRefreshState,
} from './selectors';
import Loading from '../../../../components/Loading';
import screenHOC from '../../../../components/screenHOC';
import { EventRange } from '../../common/Constant';

@screenHOC
class EmergenciesHistory extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.onFooterRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  get range() {
    return this.props.navigation.getParam('range', EventRange.PORT_BERTHING);
  }

  didFocus = () => {
    // 每次进页面刷新列表
    this.onFooterRefresh();
  };

  loadList(loadMore) {
    this.props.getEmergencyListPromise({
      id: this.props.navigation.getParam('popId'),
      range: this.range,
      loadMore,
    })
      .then(() => {})
      .catch(() => {});
  }

  renderItem = ({ item }) => (
    <EmergencyItem item={item} />
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <RefreshListView
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}
          maxSwipeDistance={80}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
        />
        {this.props.deleteLoading ? <Loading /> : null}
      </Container>
    );
  }
}

EmergenciesHistory.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '情况汇总'),
});

EmergenciesHistory.propTypes = {
  navigation: PropTypes.object.isRequired,
  getEmergencyListPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  refreshState: PropTypes.number.isRequired,
  deleteLoading: PropTypes.bool,
};
EmergenciesHistory.defaultProps = {
  list: [],
  deleteLoading: false,
};

const mapStateToProps = createStructuredSelector({
  list: makeSelectEmergencies(),
  deleteLoading: makeDeleteLoading(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getEmergencyListPromise,
      deleteEmergencyPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmergenciesHistory);
