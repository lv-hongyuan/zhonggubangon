import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_EMERGENCY_LIST, DELETE_EMERGENCY } from './constants';

export const getEmergencyListRoutine = createRoutine(GET_EMERGENCY_LIST);
export const getEmergencyListPromise = promisifyRoutine(getEmergencyListRoutine);

export const deleteEmergencyRoutine = createRoutine(DELETE_EMERGENCY);
export const deleteEmergencyPromise = promisifyRoutine(deleteEmergencyRoutine);
