import { createSelector } from 'reselect/es';

const selectEmergenciesDomain = () => state => state.emergenciesHistory;

const makeSelectEmergencies = () => createSelector(selectEmergenciesDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectEmergenciesDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeDeleteLoading = () => createSelector(selectEmergenciesDomain(), (subState) => {
  console.debug(subState.queryLoading);
  return subState.queryLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeDeleteLoading };
