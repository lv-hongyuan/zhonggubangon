import { getFunctionListRoutine } from './actions';
// import { DefaultFunctionList } from '../../common/Constant'

const defaultState = {
  permission: '',
  functionList: [],
  isLoading: false,
  error: undefined,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    case getFunctionListRoutine.TRIGGER:
      return { ...state, error: undefined, isLoading: true };
    case getFunctionListRoutine.SUCCESS:
      return { ...state, functionList: action.payload, error: undefined };
    case getFunctionListRoutine.FAILURE:
      return { ...state, error: action.payload };
    case getFunctionListRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
