import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { getFunctionListRoutine } from './actions';

function* getFunctionList(action) {
  console.log(action);
  try {
    yield put(getFunctionListRoutine.request());
    const response = yield call(request, ApiFactory.getAPPFunctionList());
    console.log(response);
    yield put(getFunctionListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(getFunctionListRoutine.failure(e));
  } finally {
    yield put(getFunctionListRoutine.fulfill());
  }
}

export function* getFunctionListSaga() {
  yield takeLatest(getFunctionListRoutine.TRIGGER, getFunctionList);
}

// All sagas to be loaded
export default [getFunctionListSaga];
