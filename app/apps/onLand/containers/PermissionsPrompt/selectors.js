import { createSelector } from 'reselect/es';
import {
  ROUTE_PORT_WORK_STACK,
  ROUTE_PORT_WORK_SUMMARY_STACK,
  ROUTE_SHIP_LAND_STACK,
  ROUTE_WORK_ANALYSIS_STACK,
} from '../../RouteConstant';

const selectPermissionsPromptDomain = () => state => state.permissionsPrompt;

const makeFunctionList = () => createSelector(selectPermissionsPromptDomain(), (subState) => {
  console.debug(subState.functionList);
  return (subState.functionList || []).map(({ appfunctionUrl }) => {
    switch (appfunctionUrl) {
      case 'PortWork':
        return ROUTE_PORT_WORK_STACK;
      case 'SummaryPortWork':
        return ROUTE_PORT_WORK_SUMMARY_STACK;
      case 'ShipLand':
        return ROUTE_SHIP_LAND_STACK;
      case 'WorkAnalysis':
        return ROUTE_WORK_ANALYSIS_STACK;
      default:
        return undefined;
    }
  });
});

const makeShipLandFunctionList = () => createSelector(selectPermissionsPromptDomain(), (subState) => {
  console.debug(subState.functionList);
  const shipLand = (subState.functionList || []).filter(({ appfunctionUrl }) => appfunctionUrl === 'ShipLand')[0] || {};
  return (shipLand.children || []).map(({ appfunctionUrl }) => appfunctionUrl);
});

const makeWorkAnalysisFunctionList = () => createSelector(selectPermissionsPromptDomain(), (subState) => {
  console.debug(subState.functionList);
  const shipLand = (subState.functionList || []).filter(({ appfunctionUrl }) => appfunctionUrl === 'WorkAnalysis')[0] || {};
  return (shipLand.children || []).map(({ appfunctionUrl }) => appfunctionUrl);
});

const makeLoading = () => createSelector(selectPermissionsPromptDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

const makeLoadingError = () => createSelector(selectPermissionsPromptDomain(), (subState) => {
  console.debug(subState.error);
  return subState.error;
});

export {
  makeFunctionList,
  makeShipLandFunctionList,
  makeWorkAnalysisFunctionList,
  makeLoading,
  makeLoadingError,
};
