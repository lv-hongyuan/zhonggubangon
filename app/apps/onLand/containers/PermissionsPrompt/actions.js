import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_FUNCTION_LIST } from './constants';

export const getFunctionListRoutine = createRoutine(GET_FUNCTION_LIST);
export const getFunctionListPromise = promisifyRoutine(getFunctionListRoutine);
