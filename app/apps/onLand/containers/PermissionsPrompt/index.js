import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Container, View, Text, Button, Spinner,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { NavigationActions } from 'react-navigation';
import myTheme from '../../../../Themes';
import screenHOC from '../../../../components/screenHOC';
import { makeLoading, makeLoadingError } from './selectors';
import Svg from '../../../../components/Svg';

@screenHOC
class PermissionsPrompt extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.props.loading ? (
          <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
            <Spinner color="#DF001B" />
            <Text style={{ margin: 20 }}>载入中</Text>
            <Button
              style={{ alignSelf: 'center' }}
              onPress={() => { this.props.navigation.dispatch(NavigationActions.back()); }}
            >
              <Text>取消</Text>
            </Button>
          </View>
        ) : (
          <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
            <Svg icon="alert_warning" size={40} />
            <Text style={{ margin: 20 }}>{this.props.error ? this.props.error.message : '请联系管理员添加权限'}</Text>
            <Button
              style={{ alignSelf: 'center' }}
              onPress={() => { this.props.navigation.dispatch(NavigationActions.back()); }}
            >
              <Text>确定</Text>
            </Button>
          </View>
        )}
      </Container>
    );
  }
}

PermissionsPrompt.propTypes = {
  navigation: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.object,
};
PermissionsPrompt.defaultProps = {
  error: undefined,
};

PermissionsPrompt.navigationOptions = {
  title: '港口作业',
};

const mapStateToProps = createStructuredSelector({
  loading: makeLoading(),
  error: makeLoadingError(),
});

export default connect(mapStateToProps)(PermissionsPrompt);
