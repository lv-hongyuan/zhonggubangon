import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_PORT_TIME } from './constants';

export const getPortTimeRoutine = createRoutine(GET_PORT_TIME);
export const getPortTimePromiseCreator = promisifyRoutine(getPortTimeRoutine);
