import { createSelector } from 'reselect/es';

const selectPortTimeDomain = () => state => state.portTime;

const makeList = () => createSelector(selectPortTimeDomain(), subState => subState.list);

const makeRefreshState = () => createSelector(selectPortTimeDomain(), (subState) => {
  console.debug(subState.refreshState);
  return subState.refreshState;
});

export {
  makeList, makeRefreshState,
};
