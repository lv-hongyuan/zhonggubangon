import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
  },
  text: {
    color: myTheme.inputColor,
  },
});

class PortTimeItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      pq, port, voyCode, lineName, shipStateName, costTime, costTimeSum,
    } = this.props.item || {};
    return (
      <View style={styles.container}>
        <View style={styles.col}>
          <Text style={styles.text}>{pq}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{port}</Text>
        </View>
        <View style={[styles.col, { width: 160 }]}>
          <Text style={styles.text}>{voyCode}</Text>
        </View>
        <View style={[styles.col, { width: 140 }]}>
          <Text style={styles.text}>{lineName}</Text>
        </View>
        <View style={[styles.col, { width: 120 }]}>
          <Text style={styles.text}>{shipStateName}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{costTime}</Text>
        </View>
        <View style={styles.col}>
          <Text style={styles.text}>{costTimeSum}</Text>
        </View>
        <Animated.View style={{
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
          transform: [{ translateX: this.props.headerTranslateX }],
        }}
        >
          <View style={[styles.container, { flex: 1 }]}>
            <View style={styles.col}>
              <Text style={styles.text}>{pq}</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.text}>{port}</Text>
            </View>
            <View style={[styles.col, { width: 160 }]}>
              <Text style={styles.text}>{voyCode}</Text>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

PortTimeItem.propTypes = {
  item: PropTypes.object.isRequired,
  headerTranslateX: PropTypes.object.isRequired,
};

export default PortTimeItem;
