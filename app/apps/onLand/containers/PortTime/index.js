//! *********** 此页面已弃用 ***************
//! *********** 在港停时 ***************
//! *********** 此页面已弃用 ***************
import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, SafeAreaView, Animated ,DeviceInfo} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Item,
  Label,
  Text,
  View,
} from 'native-base';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import HeaderButtons from 'react-navigation-header-buttons';
import { SORT_ORDER } from '../../../../common/Constant';
import memoizeSort from '../../../../utils/memoizeSort';
import { getPortTimePromiseCreator } from './actions';
import { ROUTE_PORT_TIME_RANKING } from '../../RouteConstant';
import {
  makeList, makeRefreshState,
} from './selectors';
import myTheme from '../../../../Themes';
import screenHOC, { Orientations } from '../../../../components/screenHOC';
import PortTimeItem from './components/PortTimeItem';
import RefreshListView from '../../../../components/RefreshListView';
import Svg from '../../../../components/Svg';
import commonStyles from '../../../../common/commonStyles';
import DatePullSelector from '../../../../components/DatePullSelector';
import { DATE_WHEEL_TYPE } from '../../../../components/DateTimeWheel';
const IS_IPHONEX = DeviceInfo.isIPhoneX_deprecated ? true : false;
const styles = {
  container: {
    width: IS_IPHONEX ? null :'100%',
    backgroundColor: 'white',
    flexDirection: 'row',
    marginHorizontal: IS_IPHONEX ? 44 : 0,
  },
  container2:{
    width: '100%',
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  col: {
    backgroundColor: 'white',
    width: 80,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  touchCol: {
    backgroundColor: 'white',
    width: 80,
  },
  touchContent: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderRightWidth: myTheme.borderWidth,
    borderBottomWidth: myTheme.borderWidth,
    borderColor: myTheme.borderColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    color: myTheme.inputColor,
    textAlign: 'center',
    flex: 1,
  },
};

@screenHOC
class PortTime extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      startTime: null,
      endTime: null,
      containerOffSet: new Animated.Value(0),
      sortType: 'voyCode',
      sortDirection: SORT_ORDER.ASC,
    };
  }

  componentDidMount() {
    this.listView.beginRefresh();
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = () => {

  };

  get translateX() {
    const x = 0;
    return this.state.containerOffSet.interpolate({
      inputRange: [-1, 0, x, x + 1],
      outputRange: [0, 0, 0, 1],
    });
  }

  supportedOrientations = Orientations.LANDSCAPE;

  loadList() {
    this.props.getPortTimePromiseCreator({
      startTime: this.state.startTime,
      endTime: this.state.endTime,
    })
      .catch(() => {
      });
  }

  changeSort(sortType) {
    let sortDirection;
    if (sortType === this.state.sortType) {
      sortDirection = this.state.sortDirection === SORT_ORDER.ASC ? SORT_ORDER.DESC : SORT_ORDER.ASC;
    } else {
      sortDirection = SORT_ORDER.ASC;
    }
    this.setState({
      sortType,
      sortDirection,
    });
  }

  renderItem = ({ item }) => (
    <PortTimeItem
      item={item}
      headerTranslateX={this.translateX}
    />
  );

  renderFilter() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={{ width: '100%', flexDirection: 'row' }}>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5,
            marginBottom: 0,
            flex: 1,
            width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>起始时间:</Label>
            <DatePullSelector
              value={this.state.startTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ startTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
          <Item style={[commonStyles.inputItem, {
            paddingLeft: 5,
            marginBottom: 0,
            flex: 1,
            width: undefined,
          }]}
          >
            <Label style={commonStyles.inputLabel}>截止时间:</Label>
            <DatePullSelector
              value={this.state.endTime}
              type={DATE_WHEEL_TYPE.DATE}
              onChangeValue={(value) => {
                this.setState({ endTime: value }, () => {
                  this.listView.beginRefresh();
                });
              }}
            />
          </Item>
        </SafeAreaView>
      </View>
    );
  }

  render() {
    const sortedList = memoizeSort(this.props.list, this.state.sortType, this.state.sortDirection);
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        {this.renderFilter()}
        <SafeAreaView style={{ backgroundColor: '#E0E0E0', flex: 1 }}>
          <View
            style={{
              width: '100%',
              flex: 1,
            }}
            onLayout={(event) => {
              const { width } = event.nativeEvent.layout;
              this.setState({ containerWidth: width });
            }}
          >
            <Animated.ScrollView
              onScroll={Animated.event([{
                nativeEvent: { contentOffset: { x: this.state.containerOffSet } },
              }], { useNativeDriver: true })}
              scrollEventThrottle={1}
              horizontal
            >
              <View style={{ flex: 1, minWidth: '100%' }}>
                <View style={styles.container2}>
                  <View style={styles.col}>
                    <Text style={styles.text}>片区</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>口岸</Text>
                  </View>
                  <View style={[styles.touchCol, { width: 160 }]}>
                    <Text style={styles.text}>船名/航次</Text>
                  </View>
                  <View style={[styles.col, { width: 140 }]}>
                    <Text style={styles.text}>航线</Text>
                  </View>
                  <View style={[styles.col, { width: 120 }]}>
                    <Text style={styles.text}>动态名称</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>耗时</Text>
                  </View>
                  <View style={styles.col}>
                    <Text style={styles.text}>总耗时</Text>
                  </View>
                  <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    bottom: 0,
                    transform: [{ translateX: this.translateX }],
                  }}
                  >
                    <View style={styles.container2}>
                      <View style={styles.col}>
                        <Text style={styles.text}>片区</Text>
                      </View>
                      <TouchableHighlight
                        style={styles.touchCol}
                        onPress={() => {
                          requestAnimationFrame(() => {
                            this.changeSort('port');
                          });
                        }}
                      >
                        <View style={styles.touchContent}>
                          <Text style={styles.text}>口岸</Text>
                          {this.state.sortType === 'port' && (
                            <Svg
                              icon={this.state.sortDirection === SORT_ORDER.ASC ? 'icon_up' : 'icon_down'}
                              color="red"
                              size={15}
                            />
                          )}
                        </View>
                      </TouchableHighlight>
                      <TouchableHighlight
                        style={[styles.touchCol, { width: 160 }]}
                        onPress={() => {
                          requestAnimationFrame(() => {
                            this.changeSort('voyCode');
                          });
                        }}
                      >
                        <View style={styles.touchContent}>
                          <Text style={styles.text}>船名/航次</Text>
                          {this.state.sortType === 'voyCode' && (
                            <Svg
                              icon={this.state.sortDirection === SORT_ORDER.ASC ? 'icon_up' : 'icon_down'}
                              color="red"
                              size={15}
                            />
                          )}
                        </View>
                      </TouchableHighlight>
                    </View>
                  </Animated.View>
                </View>
                <RefreshListView
                  ref={(ref) => { this.listView = ref; }}
                  headerTranslateX={this.translateX}
                  containerWidth={this.state.containerWidth}
                  data={sortedList}
                  style={{ flex: 1 }}
                  keyExtractor={item => `${item.id}`}
                  renderItem={this.renderItem}
                  refreshState={this.props.refreshState}
                  onHeaderRefresh={this.onHeaderRefresh}
                  // onFooterRefresh={this.onFooterRefresh}
                />
              </View>
            </Animated.ScrollView>
          </View>
        </SafeAreaView>
      </Container>
    );
  }
}

PortTime.navigationOptions = ({ navigation }) => ({
  title: '在港停时',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="考核"
        buttonStyle={{
          fontSize: 14,
          color: '#ffffff',
        }}
        onPress={() => {
          navigation.navigate(ROUTE_PORT_TIME_RANKING);
        }}
      />
    </HeaderButtons>
  ),
});

PortTime.propTypes = {
  getPortTimePromiseCreator: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  refreshState: PropTypes.number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  refreshState: makeRefreshState(),
  list: makeList(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getPortTimePromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortTime);
