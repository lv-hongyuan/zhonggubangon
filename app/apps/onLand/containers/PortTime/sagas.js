import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getPortTimeRoutine } from './actions';

function* getPortTime(action) {
  console.log(action);
  try {
    yield put(getPortTimeRoutine.request());
    const { startTime, endTime } = (action.payload || {});
    const response = yield call(request, ApiFactory.inPortOverTimeList(startTime, endTime));
    console.log(response);
    yield put(getPortTimeRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPortTimeRoutine.failure(e));
  } finally {
    yield put(getPortTimeRoutine.fulfill());
  }
}

export function* getPortTimeSaga() {
  yield takeLatest(getPortTimeRoutine.TRIGGER, getPortTime);
}

// All sagas to be loaded
export default [getPortTimeSaga];
