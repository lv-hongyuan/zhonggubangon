import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight, StyleSheet } from 'react-native';
import { Text, View } from 'native-base';
import myTheme from '../../../../../Themes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 5,
    paddingBottom: 5,
  },
  row: {
    flex: 1,
    paddingLeft: 30,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 30,
    flexDirection: 'row',
  },
  label: {
    // fontSize: 12,
    // color: '#535353',
    // margin: 2,
  },
  text: {
    color: myTheme.inputColor,
    marginLeft: 20,
    flex: 1,
  },
});

class PortAttentionItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  onPressEvent = () => {
    if (this.props.onSelect) {
      this.props.onSelect(this.props.item);
    }
  };

  render() {
    const item = this.props.item || {};
    return (
      <TouchableHighlight style={{ marginTop: 8 }} onPress={this.onPressEvent}>
        <View style={styles.container}>
          <View style={styles.row}>
            <Text style={styles.label}>港口名称：</Text>
            <Text style={styles.text}>{item.attentionPortName || '-----------'}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>注意事项描述：</Text>
            <Text style={styles.text}>{item.attentionMemo || '-----------'}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

PortAttentionItem.propTypes = {
  item: PropTypes.object.isRequired,
  onSelect: PropTypes.func,
};

export default PortAttentionItem;
