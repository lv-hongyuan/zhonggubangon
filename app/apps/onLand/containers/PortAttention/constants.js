/*
 *
 * PortAttention constants
 *
 */
export const GET_PORT_ATTENTION_LIST = 'onLand/PortAttention/GET_PORT_ATTENTION_LIST';
export const DELETE_PORT_ATTENTION = 'onLand/PortAttention/DELETE_PORT_ATTENTION';
export const SELECT_ATTENTION_LIST = 'onLand/PortAttention/SELECT_ATTENTION_LIST';
