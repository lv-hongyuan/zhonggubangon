import { createSelector } from 'reselect/es';

const selectPortAttentionsDomain = () => state => state.portAttentions;

const makeSelectPortAttentions = () => createSelector(selectPortAttentionsDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectPortAttentionsDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeDeleteLoading = () => createSelector(selectPortAttentionsDomain(), (subState) => {
  console.debug(subState.queryLoading);
  return subState.queryLoading;
});

export { makeSelectPortAttentions, makeSelectRefreshState, makeDeleteLoading };
