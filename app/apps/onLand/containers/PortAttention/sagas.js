import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getCreateAttentionListRoutine, deleteAttentionRoutine, getAttentionListRoutine } from './actions';

function* getCreateAttentionList(action) {
  console.log(action);
  try {
    yield put(getCreateAttentionListRoutine.request());
    const response = yield call(request, ApiFactory.getCreateAttentionList(action.payload.createPopId));
    yield put(getCreateAttentionListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getCreateAttentionListRoutine.failure(e));
  } finally {
    yield put(getCreateAttentionListRoutine.fulfill());
  }
}

export function* getCreateAttentionListSaga() {
  yield takeLatest(getCreateAttentionListRoutine.TRIGGER, getCreateAttentionList);
}

function* deleteAttention(action) {
  console.log(action);
  try {
    yield put(deleteAttentionRoutine.request());
    const response = yield call(request, ApiFactory.deleteAttention(action.payload.id));
    console.log('deleteAttention', response);
    yield put(deleteAttentionRoutine.success(action.payload));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(deleteAttentionRoutine.failure(e));
  } finally {
    yield put(deleteAttentionRoutine.fulfill());
  }
}

export function* deleteAttentionSaga() {
  yield takeLatest(deleteAttentionRoutine.TRIGGER, deleteAttention);
}

function* getAttentionList(action) {
  console.log(action);
  try {
    yield put(getAttentionListRoutine.request());
    const response = yield call(request, ApiFactory.getAttentionList(action.payload.attentionPopId));
    yield put(getAttentionListRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getAttentionListRoutine.failure(e));
  } finally {
    yield put(getAttentionListRoutine.fulfill());
  }
}

export function* getAttentionListSaga() {
  yield takeLatest(getAttentionListRoutine.TRIGGER, getAttentionList);
}

// All sagas to be loaded
export default [getCreateAttentionListSaga, deleteAttentionSaga, getAttentionListSaga];
