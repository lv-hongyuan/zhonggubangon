import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet, View, TouchableHighlight, Text,
} from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { Container } from 'native-base';
import HeaderButtons from 'react-navigation-header-buttons';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { getCreateAttentionListPromise, deleteAttentionPromise, getAttentionListPromise } from './actions';
import PortAttentionItem from './components/PortAttentionItem';
import { ROUTE_PORT_ATTENTION_EDIT } from '../../RouteConstant';
import myTheme from '../../../../Themes';
import RefreshListView from '../../../../components/RefreshListView';

import {
  makeDeleteLoading,
  makeSelectPortAttentions,
  makeSelectRefreshState,
} from './selectors';
import Loading from '../../../../components/Loading';
import screenHOC from '../../../../components/screenHOC';

const styles = StyleSheet.create({
  actionsContainer: {
    flex: 1,
    marginTop: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionButton: {
    width: 80,
    height: '100%',
    backgroundColor: '#DC001B',
    borderRadius: 0,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  actionButtonText: {
    textAlign: 'center',
    color: 'white',
  },
});

/*
 * 港口注意事项
 */
@screenHOC
class PortAttention extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};

    this.props.navigation.setParams({
      addPortAttention: this.addPortAttention,
    });
  }

  onHeaderRefresh = () => {
    this.loadList(false);
  };

  onFooterRefresh = () => {
    this.loadList(true);
  };

  onSelect = (item) => {
    if (this.canEdit) {
      this.props.navigation.navigate(
        ROUTE_PORT_ATTENTION_EDIT,
        {
          id: this.props.navigation.getParam('popId'),
          item,
        },
      );
    }
  };

  onDelete = (item) => {
    this.props.deleteAttentionPromise(item)
      .then(() => {})
      .catch(() => {});
  };

  get canEdit() {
    return this.props.navigation.getParam('page') === 'portWorkDetail';
  }

  didFocus() {
    this.listView.beginRefresh();
  }

  addPortAttention = () => {
    this.props.navigation.navigate(
      ROUTE_PORT_ATTENTION_EDIT,
      {
        id: this.props.navigation.getParam('popId'),
        portName: this.props.navigation.getParam('portName'),
        attentionPopId: this.props.navigation.getParam('attentionPopId'),
      },
    );
  };

  loadList(loadMore) {
    if (this.canEdit) {
      this.props.getCreateAttentionListPromise({
        createPopId: this.props.navigation.getParam('popId'),
        loadMore,
      })
        .then(() => {})
        .catch(() => {});
    } else {
      this.props.getAttentionListPromise({
        attentionPopId: this.props.navigation.getParam('popId'),
        loadMore,
      })
        .then(() => {})
        .catch(() => {});
    }
  }

  renderItem = ({ item }) => (
    <PortAttentionItem
      item={item}
      onSelect={this.onSelect}
    />// 编辑
  );

  renderQuickActions = ({ item }) => (
    <View style={styles.actionsContainer}>
      <TouchableHighlight
        style={styles.actionButton}
        onPress={() => {
          this.onDelete(item);
        }}
      >
        <View style={styles.actionButton}>
          <Text style={styles.actionButtonText}>删除</Text>
        </View>
      </TouchableHighlight>
    </View>
  );

  render() {
    return (
      <Container theme={myTheme} style={{ backgroundColor: '#E0E0E0' }}>
        <RefreshListView
          ref={(ref) => { this.listView = ref; }}
          contentInsetAdjustmentBehavior="scrollableAxes"
          style={{ width: '100%' }}
          data={this.props.list || []}
          keyExtractor={item => `${item.id}`}
          renderItem={this.renderItem}// 编辑方法显示表单
          renderQuickActions={this.canEdit ? this.renderQuickActions : undefined}// 删除方法
          maxSwipeDistance={80}
          refreshState={this.props.refreshState}
          onHeaderRefresh={this.onHeaderRefresh}
          onFooterRefresh={this.onFooterRefresh}
        />
        {this.props.deleteLoading ? <Loading /> : null}
      </Container>
    );
  }
}

PortAttention.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('title', '港口注意事项'),
  headerRight: (
    <HeaderButtons>
      {navigation.getParam('page') === 'portWorkDetail'
        ? (
          <HeaderButtons.Item
            title="创建"
            buttonStyle={{ fontSize: 14, color: '#ffffff' }}
            onPress={navigation.getParam('addPortAttention')}
          />
        ) : null
      }
    </HeaderButtons>
  ),
});

PortAttention.propTypes = {
  navigation: PropTypes.object.isRequired,
  getCreateAttentionListPromise: PropTypes.func.isRequired,
  getAttentionListPromise: PropTypes.func.isRequired,
  deleteAttentionPromise: PropTypes.func.isRequired,
  list: PropTypes.array,
  deleteLoading: PropTypes.bool.isRequired,
  refreshState: PropTypes.number.isRequired,
};
PortAttention.defaultProps = {
  list: [],
};

const mapStateToProps = createStructuredSelector({
  list: makeSelectPortAttentions(),
  deleteLoading: makeDeleteLoading(),
  refreshState: makeSelectRefreshState(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getCreateAttentionListPromise,
      deleteAttentionPromise,
      getAttentionListPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PortAttention);
