import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_PORT_ATTENTION_LIST, DELETE_PORT_ATTENTION, SELECT_ATTENTION_LIST } from './constants';

export const getCreateAttentionListRoutine = createRoutine(GET_PORT_ATTENTION_LIST);
export const getCreateAttentionListPromise = promisifyRoutine(getCreateAttentionListRoutine);

export const deleteAttentionRoutine = createRoutine(DELETE_PORT_ATTENTION);
export const deleteAttentionPromise = promisifyRoutine(deleteAttentionRoutine);

export const getAttentionListRoutine = createRoutine(SELECT_ATTENTION_LIST);
export const getAttentionListPromise = promisifyRoutine(getAttentionListRoutine);
