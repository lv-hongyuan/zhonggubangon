import { RefreshState } from '../../../../components/RefreshListView';
import { getCreateAttentionListRoutine, deleteAttentionRoutine, getAttentionListRoutine } from './actions';

const defaultState = {
  list: [],
  loadingError: null,
  isLoading: false,
  operatingItemId: null,
  refreshState: RefreshState.Idle,
};

export default function (state = defaultState, action) {
  switch (action.type) {
    // getCreateAttentionListRoutine
    case getCreateAttentionListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        loadingError: undefined,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getCreateAttentionListRoutine.SUCCESS: {
      // 列表处理
      const oldList = state.list;
      const loadingError = action.payload.loadingError;
      const receiveList = action.payload || [];
      // let list = oldList.concat(receiveList);
      const list = receiveList;

      // 刷新状态
      let refreshState;
      if (list.length === 0) {
        refreshState = RefreshState.EmptyData;
      } else if (oldList === list) {
        refreshState = RefreshState.NoMoreData;
      } else {
        refreshState = RefreshState.NoMoreData;
      }
      return {
        ...state, list, loadingError, refreshState,
      };
    }
    case getCreateAttentionListRoutine.FAILURE:
      return {
        ...state, list: [], refreshState: RefreshState.Failure, loadingError: action.payload,
      };
    case getCreateAttentionListRoutine.FULFILL:
      return { ...state, isLoading: false };

    // getAttentionListRoutine
    case getAttentionListRoutine.TRIGGER:
      return {
        ...state,
        isLoading: true,
        loadingError: undefined,
        refreshState: action.payload.loadMore ? RefreshState.FooterRefreshing : RefreshState.HeaderRefreshing,
      };
    case getAttentionListRoutine.SUCCESS:
      // 列表处理
      const oldList = state.list;
      const loadingError = action.payload.loadingError;
      const receiveList = action.payload || [];
      // let list = oldList.concat(receiveList);
      const list = receiveList;

      // 刷新状态
      let refreshState;
      if (list.length === 0) {
        refreshState = RefreshState.EmptyData;
      } else if (oldList === list) {
        refreshState = RefreshState.NoMoreData;
      } else {
        refreshState = RefreshState.NoMoreData;
      }
      return {
        ...state, list, loadingError, refreshState,
      };
    case getAttentionListRoutine.FAILURE:
      return {
        ...state, list: [], refreshState: RefreshState.Failure, loadingError: action.payload,
      };
    case getAttentionListRoutine.FULFILL:
      return { ...state, isLoading: false };


    // 删除突发事件deleteAttentionRoutine
    case deleteAttentionRoutine.TRIGGER:
      return { ...state, queryLoading: true, loadingError: undefined };
    case deleteAttentionRoutine.SUCCESS:
      const portAttention = action.payload;
      let newList = [];
      if (portAttention) {
        newList = state.list.filter(item => item !== portAttention);
      } else {
        newList = state.list;
      }
      return { ...state, list: newList, queryLoading: false };
    case deleteAttentionRoutine.FAILURE:
      return { ...state, loadingError: action.payload };
    case deleteAttentionRoutine.FULFILL:
      return { ...state, queryLoading: false };

    default:
      return state;
  }
}
