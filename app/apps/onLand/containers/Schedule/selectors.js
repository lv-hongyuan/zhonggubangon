import { createSelector } from 'reselect';

const selectScheduleDomain = () => state => state.schedule;

const makeList = () => createSelector(selectScheduleDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeIsLoading = () => createSelector(selectScheduleDomain(), (subState) => {
  console.debug(subState.loading);
  return subState.loading;
});

const makeMyPortList = () => createSelector(selectScheduleDomain(), (subState) => {
  console.debug(subState);
  return subState.myPortList;
});

export { makeList, makeIsLoading, makeMyPortList };
