import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';
import {
  InputGroup, Container, Form, Item, Label,
} from 'native-base';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { Agenda, LocaleConfig } from 'react-native-calendars';
import _ from 'lodash';
import { getSchedulePromiseCreator, getMyPortSelectPromiseCreator } from './actions';
import { makeList, makeMyPortList, makeIsLoading } from './selectors';
import screenHOC from '../../../../components/screenHOC';
import Selector from '../../../../components/Selector';
import commonStyles from '../../../../common/commonStyles';
import { defaultFormat, formatDate } from '../../../../utils/DateFormat';

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 0,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
    minHeight: 30,
  },
  day: {
    flex: 0,
    width: 50,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
    minHeight: 30,
  },
  mouth: {
    color: '#fbb03a',
    fontSize: 16,
    textAlign: 'center',
  },
  emptyDate: {
    flex: 0,
    paddingTop: 9,
  },
  text: {
    paddingTop: 20,
  },
});

@screenHOC
class Schedule extends React.Component {
  constructor(props) {
    super(props);

    const date = new Date();
    const day = formatDate(date, 'yyyy-MM-dd');
    this.state = {
      items: { [day]: [] },
      portId: null,
      day,
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  componentDidMount() {
    const { getMyPortSelect } = this.props;
    getMyPortSelect().then(() => {
      const { myPortList } = this.props;
      const { portId } = this.state;
      if (_.isNil(portId) && myPortList.length > 0) {
        const { id } = myPortList[0];
        this.setState({
          portId: id,
        }, () => {
          this.loadList();
        });
      }
    }).catch(() => {});
  }

  onPressPortName = () => {
    const { getMyPortSelect, myPortList } = this.props;
    if (myPortList.length > 0) {
      this.select.showPicker();
    } else {
      getMyPortSelect().then(() => {}).catch(() => {});
    }
  };

  onChangePortType = (port) => {
    if (this.state.portId !== port.id) {
      this.setState({
        portId: port.id,
      }, () => {
        this.loadList();
      });
    }
  };

  onDayPress(day) {
    const key = day.dateString;
    const oldItems = this.state.items;
    if (!oldItems[key]) {
      oldItems[key] = [];
      this.setState({
        items: oldItems,
        day: key,
      });
    }
  }

  loadList() {
    this.props.getSchedule(this.state.portId).then(() => {
      const items = {};
      const date = new Date();
      for (let i = 0; i < 7; i += 1) {
        const newDate = formatDate(new Date(date.getTime() + (86400000 * i)), 'yyyy-MM-dd');
        items[newDate] = [];
      }
      const key = this.state.day;
      this.props.list.forEach((item) => {
        const {
          etb, portName, shipName, voyageCode,
        } = item;
        const e = defaultFormat(etb).substring(0, 10);
        const array = items[e];
        if (array) {
          array.push({
            etb, portName, shipName, voyageCode,
          });
        } else {
          items[e] = [{
            etb, portName, shipName, voyageCode,
          }];
        }
      });
      if (!items[key]) {
        items[key] = [];
      }
      this.setState({
        items,
      });
    }).catch(() => {});
  }

  rowHasChanged = (r1, r2) => r1.name !== r2.name;

  renderEmptyDate = () => (
    <View style={styles.emptyDate}>
      <Text style={styles.text}>无到港预报！</Text>
    </View>
  );

  renderDay = (day) => {
    if (day) {
      return (
        <View style={styles.day}>
          <Text style={styles.mouth}>{day.day}</Text>
        </View>
      );
    }
    return (
      <View style={styles.day}>
        <Text />
      </View>
    );
  };

  renderItem = (item) => {
    const etb = defaultFormat(item.etb);
    return (
      <View style={styles.item}>
        <Text>
          {etb}
          ,&nbsp;&nbsp;
          {item.portName}
          ,&nbsp;&nbsp;
          {item.voyageCode}
          ,&nbsp;&nbsp;
          {item.shipName}
        </Text>
      </View>
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <View style={{ backgroundColor: 'white' }}>
          <Form style={{ paddingLeft: 32, paddingRight: 33 }}>
            <InputGroup style={commonStyles.inputGroup}>
              <Item style={commonStyles.inputItem}>
                <Label style={commonStyles.inputLabel}>港口名称:</Label>
                <Selector
                  value={this.state.portId}
                  items={this.props.myPortList || []}
                  ref={(ref) => {
                    this.select = ref;
                  }}
                  pickerTitle="请选择港口名称"
                  getItemValue={item => item.id}
                  getItemText={item => item.text}
                  onPress={this.onPressPortName}
                  onSelected={this.onChangePortType}
                />
              </Item>
            </InputGroup>
          </Form>
        </View>

        <Agenda
          LocaleConfig="fr"
          firstDay={new Date().getDay()}
          items={this.state.items}
          onDayPress={this.onDayPress}
          renderItem={this.renderItem}
          renderDay={this.renderDay}
          renderEmptyDate={this.renderEmptyDate}
          rowHasChanged={this.rowHasChanged}
          hideKnob
          theme={{
            selectedDayBackgroundColor: '#fbb03a',
            dotColor: '#fbb03a',
            agendaDayTextColor: 'gray',
            agendaDayNumColor: 'gray',
            agendaKnobColor: '#fbb03a',
            agendaTodayColor: '#fbb03a',
            todayTextColor: '#fbb03a',
          }}
        />
      </Container>
    );
  }
}

LocaleConfig.locales.fr = {
  monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  monthNamesShort: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
  dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
  dayNamesShort: ['日', '一', '二', '三', '四', '五', '六'],
};
LocaleConfig.defaultLocale = 'fr';

Schedule.navigationOptions = () => ({
  title: '到港预报',
});

Schedule.propTypes = {
  getMyPortSelect: PropTypes.func.isRequired,
  getSchedule: PropTypes.func.isRequired,
  myPortList: PropTypes.array.isRequired,
  list: PropTypes.array.isRequired,
};

const mapStateToProps = createStructuredSelector({
  list: makeList(),
  myPortList: makeMyPortList(),
  isLoading: makeIsLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      getSchedule: getSchedulePromiseCreator,
      getMyPortSelect: getMyPortSelectPromiseCreator,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
