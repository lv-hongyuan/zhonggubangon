import { getScheduleRoutine, getMyPortSelectRoutine } from './actions';
import { RefreshState } from '../../../../components/RefreshListView';
import fixIdList from '../../../../utils/fixIdList';

const initState = {
  loading: false,
  list: [],
  refreshState: RefreshState.Idle,
  myPortList: [],
};

export default function (state = initState, action) {
  switch (action.type) {
    case getScheduleRoutine.TRIGGER:
      return { ...state, loading: true, refreshState: RefreshState.HeaderRefreshing };
    case getScheduleRoutine.SUCCESS:
      return { ...state, list: fixIdList(action.payload), refreshState: RefreshState.NoMoreData };
    case getScheduleRoutine.FAILURE:
      return {
        ...state, list: [], error: action.payload, refreshState: RefreshState.Failure,
      };
    case getScheduleRoutine.FULFILL:
      return { ...state, loading: false };

    case getMyPortSelectRoutine.TRIGGER:
      return { ...state, loading: true };
    case getMyPortSelectRoutine.SUCCESS:
      return { ...state, myPortList: fixIdList(action.payload) };
    case getMyPortSelectRoutine.FAILURE:
      return { ...state, myPortList: [], error: action.payload };
    case getMyPortSelectRoutine.FULFILL:
      return { ...state, loading: false };

    default:
      return state;
  }
}
