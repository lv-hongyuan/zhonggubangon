import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_SCHEDULE, SELECT_MY_PORT } from './constants';

export const getScheduleRoutine = createRoutine(GET_SCHEDULE);
export const getSchedulePromiseCreator = promisifyRoutine(getScheduleRoutine);

export const getMyPortSelectRoutine = createRoutine(SELECT_MY_PORT);
export const getMyPortSelectPromiseCreator = promisifyRoutine(getMyPortSelectRoutine);
