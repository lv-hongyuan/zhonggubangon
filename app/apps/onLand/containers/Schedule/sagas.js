import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getScheduleRoutine, getMyPortSelectRoutine } from './actions';

function* getSchedule(action) {
  console.log(action);
  try {
    yield put(getScheduleRoutine.request());
    const param = { portId: action.payload };
    const response = yield call(request, ApiFactory.getSchedule(param));
    console.log('getSchedule', response);
    yield put(getScheduleRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getScheduleRoutine.failure(e));
  } finally {
    yield put(getScheduleRoutine.fulfill());
  }
}

export function* getScheduleSaga() {
  yield takeLatest(getScheduleRoutine.TRIGGER, getSchedule);
}

function* getMyPortSelect(action) {
  console.log(action);
  try {
    yield put(getMyPortSelectRoutine.request());
    const response = yield call(
      request,
      ApiFactory.getMyPortSelect(),
    );
    yield put(getMyPortSelectRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getMyPortSelectRoutine.failure(e));
  } finally {
    yield put(getMyPortSelectRoutine.fulfill());
  }
}

export function* getMyPortSelectSaga() {
  yield takeLatest(getMyPortSelectRoutine.TRIGGER, getMyPortSelect);
}

// All sagas to be loaded
export default [getScheduleSaga, getMyPortSelectSaga];
