/*
 *
 * Schedule constants
 *
 */
export const GET_SCHEDULE = 'app/Schedule/GET_SCHEDULE';
export const SELECT_MY_PORT = 'app/Schedule/SELECT_MY_PORT';
