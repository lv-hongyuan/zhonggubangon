import { createSelector } from 'reselect/es';

const selectPilotApplyListDomain = () => state => state.pilotApplyList;

const makeSelectEmergencies = () => createSelector(selectPilotApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.list;
});

const makeSelectRefreshState = () => createSelector(selectPilotApplyListDomain(), (subState) => {
  console.debug(subState);
  return subState.refreshState;
});

const makeIsLoading = () => createSelector(selectPilotApplyListDomain(), (subState) => {
  console.debug(subState.isLoading);
  return subState.isLoading;
});

export { makeSelectEmergencies, makeSelectRefreshState, makeIsLoading };
