import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
import { getPilotApplyListRoutine, reviewPilotApplyRoutine } from './actions';

function* getPilotApplyList(action) {
  console.log(action);
  const {
    page, pageSize, loadMore, ...rest
  } = (action.payload || {});
  try {
    yield put(getPilotApplyListRoutine.request());
    const param = {
      filter: {
        logic: 'and',
        filters: [],
      },
      page,
      pageSize,
    };
    Object.keys(rest).map((key) => {
      const value = rest[key];
      if (value !== null && value !== undefined && value !== '') {
        param.filter.filters.push({
          field: key,
          operator: 'eq',
          value,
        });
      }
    });
    const response = yield call(request, ApiFactory.findApplyDivs(param));
    console.log('+++==',JSON.stringify(response));
    const showAudit = response.showAudit;
    const { totalElements, content } = (response.dtoList || {});
    yield put(getPilotApplyListRoutine.success({
      loadMore, page, pageSize, list: (content || []), totalElements,showAudit
    }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getPilotApplyListRoutine.failure(e));
  } finally {
    yield put(getPilotApplyListRoutine.fulfill());
  }
}

export function* getPilotApplyListSaga() {
  yield takeLatest(getPilotApplyListRoutine.TRIGGER, getPilotApplyList);
}

function* reviewPilotApply(action) {
  console.log(action);
  try {
    yield put(reviewPilotApplyRoutine.request());
    const { id, state, rejectReasons } = action.payload;
    const response = yield call(request, ApiFactory.updateApplyDivState({ id, state, rejectReasons }));
    console.log('reviewPilotApply', response);
    yield put(reviewPilotApplyRoutine.success({ id, state }));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(reviewPilotApplyRoutine.failure(e));
  } finally {
    yield put(reviewPilotApplyRoutine.fulfill());
  }
}

export function* reviewPilotApplySaga() {
  yield takeLatest(reviewPilotApplyRoutine.TRIGGER, reviewPilotApply);
}

// All sagas to be loaded
export default [getPilotApplyListSaga, reviewPilotApplySaga];
