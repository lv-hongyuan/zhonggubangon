import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import { GET_PILOT_APPLY_LIST, REVIEW_PILOT_APPLY } from './constants';

export const getPilotApplyListRoutine = createRoutine(GET_PILOT_APPLY_LIST);
export const getPilotApplyListPromise = promisifyRoutine(getPilotApplyListRoutine);

export const reviewPilotApplyRoutine = createRoutine(REVIEW_PILOT_APPLY);
export const reviewPilotApplyPromise = promisifyRoutine(reviewPilotApplyRoutine);
