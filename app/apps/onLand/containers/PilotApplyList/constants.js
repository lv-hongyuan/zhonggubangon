/*
 *
 * PilotApplyList constants
 *
 */
export const GET_PILOT_APPLY_LIST = 'onLand/PilotApplyList/GET_PILOT_APPLY_LIST';
export const REVIEW_PILOT_APPLY = 'onLand/PilotApplyList/REVIEW_PILOT_APPLY';
