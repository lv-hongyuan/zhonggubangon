import { createRoutine, promisifyRoutine } from 'redux-saga-routines/es';
import {
  EMERGENCY_LIST,
  EMERGENCY_TYPES,
  CREATE_EMERGENCY,
  UPDATE_EMERGENCY,
} from './constants';

export const getEmergencyListRoutine = createRoutine(EMERGENCY_LIST);
//需要使用返回结果的时候使用
// export const getEmergencyListPromise = promisifyRoutine(getEmergencyListRoutine);

export const getEmergencyTypesRoutine = createRoutine(EMERGENCY_TYPES);
//需要使用返回结果的时候使用
export const getEmergencyTypesPromise = promisifyRoutine(getEmergencyTypesRoutine);

export const createEmergencyRoutine = createRoutine(CREATE_EMERGENCY);
export const createEmergencyPromise = promisifyRoutine(createEmergencyRoutine);

export const updateEmergencyRoutine = createRoutine(UPDATE_EMERGENCY);
export const updateEmergencyPromise = promisifyRoutine(updateEmergencyRoutine);
