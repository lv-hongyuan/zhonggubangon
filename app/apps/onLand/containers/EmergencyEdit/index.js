import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect/es';
import HeaderButtons from 'react-navigation-header-buttons';
import {
  InputGroup, Container, Content, View, Form, Item, Label,
} from 'native-base';
import _ from 'lodash';
import { bindPromiseCreators } from 'redux-saga-routines/es';
import { MaskType } from '../../../../components/InputItem/TextInput';
import {
  // getemeraencyListPromise,
  getEmergencyTypesPromise,
  createEmergencyPromise,
  updateEmergencyPromise,
} from './actions';
import {
  makeSelectCreateEmergency,
  makeSelectEmergencyTypes,
  makeSelectEmergencyLoading,
} from './selectors';
import myTheme from '../../../../Themes';
import DatePullSelector from '../../../../components/DatePullSelector/index';
import Selector from '../../../../components/Selector';
import InputItem from '../../../../components/InputItem';
import AlertView from '../../../../components/Alert';
import { makeOperatingShip } from '../PortWork/selectors';
import commonStyles from '../../../../common/commonStyles';
import screenHOC from '../../../../components/screenHOC';
import { EventRange } from '../../common/Constant';

@screenHOC
class EmergencyEdit extends React.PureComponent {
  constructor(props) {
    super(props);

    const popId = props.navigation.getParam('id');
    const item = props.navigation.getParam('item', { portName: (this.props.operatingShip || {}).portName });
    this.state = {
      isEditing: item.id !== undefined && item.id !== null,
      data: {
        id: item.id,
        startTime: item.startTime === 0 ? null : item.startTime, // 开始时间
        endTime: item.endTime === 0 ? null : item.endTime, // 结束时间
        typeChildCode: item.typeChildCode, // 事件子类型
        typeChildText: item.typeChildText, // 事件子类型
        range: this.range,
        describe: item.describe, // 事件描述
        popId,
        portName: item.portName,
        workNum: item.workNum,
        daoNum: item.daoNum,
      },
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({ submit: this.submit });
    if (!this.state.isEditing) {
      this.props.getEmergencyTypesPromise(this.range)
        .then(() => {})
        .catch(() => {});
    }
  }

  onBtnTypeParent = () => {
    if (this.props.types.length > 0) {
      this.selector.showPicker();
    } else {
      this.props.getEmergencyTypesPromise(this.range)
        .then(() => {
          this.selector.showPicker();
        })
        .catch(() => {});
    }
  };

  onChangeTypeParent = (item) => {
    if (this.state.data.typeChildCode !== item.code) {
      let { workNum, daoNum } = this.state.data;
      const shouldEmpty = item.text !== '双挂作业';
      if (shouldEmpty) {
        workNum = undefined;
        daoNum = undefined;
      }
      this.setData({
        typeChildCode: item.code,
        typeChildText: item.text,
        workNum,
        daoNum,
      });
    }
  };

  setData(params) {
    this.setState({
      data: Object.assign({}, this.state.data, params),
    });
  }

  get range() {
    return this.props.navigation.getParam('range', EventRange.PORT_BERTHING);
  }

  submit = () => {
    const param = {
      id: this.state.data.id,
      startTime: this.state.data.startTime,
      endTime: this.state.data.endTime,
      workNum: this.state.data.workNum,
      daoNum: this.state.data.daoNum,
      typeChildCode: this.state.data.typeChildCode,
      typeChildText: this.state.data.typeChildText,
      describe: this.state.data.describe,
      popId: this.state.data.popId,
      portName: this.state.data.portName,
      range: this.range,
    };
    if (!this.props.isLoading) {
      if (!this.state.data.startTime) {
        AlertView.show({ message: '请填写开始时间' });
        return;
      }
      if (this.state.data.endTime && this.state.data.endTime < this.state.data.startTime) {
        AlertView.show({ message: '结束时间不能小于开始时间' });
        return;
      }
      if (!this.state.data.typeChildText) {
        AlertView.show({ message: '请填写事件类型' });
        return;
      }
      if (this.state.data.typeChildText === '其他原因' && _.isEmpty(this.state.data.describe)) {
        AlertView.show({ message: '请填写事件描述' });
        return;
      }
      if (this.state.data.typeChildText === '双挂作业') {
        if (_.isNil(this.state.data.workNum) || this.state.data.workNum === '') {
          AlertView.show({ message: '请填写作业箱量' });
          return;
        }
        if (_.isNil(this.state.data.daoNum) || this.state.data.daoNum === '') {
          AlertView.show({ message: '请填写捣箱量' });
          return;
        }
      }
      if (this.state.isEditing) {
        this.props.updateEmergencyPromise(param)
          .then(() => {
            this.props.navigation.goBack();
          })
          .catch(() => {});
      } else {
        //这相当于发请求
        this.props.createEmergencyPromise(param)
          .then(() => {
            this.props.navigation.goBack();
          })
          .catch(() => {});
      }
    }
  };

  render() {
    const showNum = this.state.data.typeChildText === '双挂作业';
    return (
      <Container style={{ backgroundColor: '#E0E0E0' }}>
        <Content theme={myTheme} contentInsetAdjustmentBehavior="scrollableAxes">
          <View style={{
            backgroundColor: 'white',
            marginTop: 8,
          }}
          >
            <Form style={{
              paddingLeft: 20,
              paddingRight: 20,
              paddingBottom: 8,
            }}
            >
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="事发港口:"
                  value={this.state.data.portName}
                  placeholder=""
                  editable={false}
                  onChangeText={(text) => {
                    this.setData({ portName: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>事件类型:</Label>
                  <Selector
                    value={this.state.data.typeChildText}//默认的text
                    items={this.props.types || []}//显示的传过来的数组或字典
                    disabled={this.state.isEditing}//是否可编辑的状态
                    ref={(ref) => {
                      this.selector = ref;
                    }}
                    onPress={this.onBtnTypeParent}
                    pickerTitle="请选择事件类型"
                    getItemValue={item => item.text}
                    getItemText={item => item.text}
                    onSelected={this.onChangeTypeParent}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <InputItem
                  label="事件描述:"
                  value={this.state.data.describe}
                  onChangeText={(text) => {
                    this.setData({ describe: text });
                  }}
                />
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>开始时间:</Label>
                  <DatePullSelector
                    value={this.state.data.startTime}
                    onChangeValue={(value) => {
                      this.setData({ startTime: value });
                    }}
                  />
                </Item>
              </InputGroup>
              <InputGroup style={commonStyles.inputGroup}>
                <Item style={commonStyles.inputItem}>
                  <Label style={commonStyles.inputLabel}>结束时间:</Label>
                  <DatePullSelector
                    value={this.state.data.endTime}
                    minValue={this.state.data.startTime}
                    onChangeValue={(value) => {
                      this.setData({ endTime: value });
                    }}
                  />
                </Item>
              </InputGroup>
              {showNum && (
                <InputGroup style={commonStyles.inputGroup}>
                  <InputItem
                    label="作业箱量:"
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    value={this.state.data.workNum}
                    onChangeText={(text) => {
                      this.setData({ workNum: text });
                    }}
                  />
                </InputGroup>
              )}
              {showNum && (
                <InputGroup style={commonStyles.inputGroup}>
                  <InputItem
                    label="捣箱量:"
                    keyboardType="numeric"
                    maskType={MaskType.FLOAT}
                    value={this.state.data.daoNum}
                    onChangeText={(text) => {
                      this.setData({ daoNum: text });
                    }}
                  />
                </InputGroup>
              )}
            </Form>
          </View>
        </Content>
      </Container>
    );
  }
}

EmergencyEdit.navigationOptions = ({ navigation }) => ({
  title: navigation.getParam('item', undefined) === undefined ? '新建' : '编辑',
  headerRight: (
    <HeaderButtons>
      <HeaderButtons.Item
        title="保存"
        buttonStyle={{
          fontSize: 14,
          color: '#ffffff',
        }}
        onPress={navigation.getParam('submit')}
      />
    </HeaderButtons>
  ),
});

EmergencyEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
  // getemeraencyListPromise: PropTypes.func.isRequired,
  getEmergencyTypesPromise: PropTypes.func.isRequired,
  createEmergencyPromise: PropTypes.func.isRequired,
  updateEmergencyPromise: PropTypes.func.isRequired,
  types: PropTypes.array,
  isLoading: PropTypes.bool,
  operatingShip: PropTypes.object,
};
EmergencyEdit.defaultProps = {
  types: [],
  isLoading: false,
  operatingShip: undefined,
};

const mapStateToProps = createStructuredSelector({
  emergency: makeSelectCreateEmergency(),
  types: makeSelectEmergencyTypes(),
  isLoading: makeSelectEmergencyLoading(),
  operatingShip: makeOperatingShip(),
});

function mapDispatchToProps(dispatch) {
  return {
    ...bindPromiseCreators({
      // getemeraencyListPromise,
      getEmergencyTypesPromise,
      createEmergencyPromise,
      updateEmergencyPromise,
    }, dispatch),
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmergencyEdit);
