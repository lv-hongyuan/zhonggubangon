/*
 *
 * PortWork constants
 *
 */
export const EMERGENCY_TYPES = 'onLand/EmergencyEdit/EMERGENCY_TYPES';

export const CREATE_EMERGENCY = 'onLand/EmergencyEdit/CREATE_EMERGENCY';

export const UPDATE_EMERGENCY = 'onLand/EmergencyEdit/UPDATE_EMERGENCY';

export const CLEAN = 'onLand/EmergencyEdit/CLEAN';
