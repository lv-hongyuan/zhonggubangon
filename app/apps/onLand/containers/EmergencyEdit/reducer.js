import {
  getEmergencyTypesRoutine,
  createEmergencyRoutine,
  updateEmergencyRoutine,
} from './actions';
//1.加一条数据的话在这声明一下
const defaultState = {
  emergency: undefined,
  isLoading: false,
  loadingError: undefined,
  navBack: false,
  types: [],
  subTypes: [],
};
//2.在这返回一个switch case 来处理action case
export default function (state = defaultState, action) {
  switch (action.type) {
    // 获取突发事件类型
    case getEmergencyTypesRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case getEmergencyTypesRoutine.SUCCESS:
      return { ...state, types: action.payload };
    case getEmergencyTypesRoutine.FAILURE:
      return { ...state, types: [], loadingError: action.payload };
    case getEmergencyTypesRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 创建突发事件
    case createEmergencyRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case createEmergencyRoutine.SUCCESS:
      return { ...state };
    case createEmergencyRoutine.FAILURE:
      return { ...state };
    case createEmergencyRoutine.FULFILL:
      return { ...state, isLoading: false };

    // 更新突发事件
    case updateEmergencyRoutine.TRIGGER:
      return { ...state, isLoading: true, loadingError: undefined };
    case updateEmergencyRoutine.SUCCESS:
      return { ...state };
    case updateEmergencyRoutine.FAILURE:
      return { ...state };
    case updateEmergencyRoutine.FULFILL:
      return { ...state, isLoading: false };

    default:
      return state;
  }
}
