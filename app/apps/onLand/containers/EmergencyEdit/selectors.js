import { createSelector } from 'reselect/es';
//1.相当于在全局的state 中找到当前画面的state 在当前文件的reducter.js文件中找到
const selectEditEmergencyDomain = () => state => state.emergencyEdit;

const makeSelectEmergencyTypes = () => createSelector(selectEditEmergencyDomain(), (subState) => {
  console.log(subState);
  return subState.types || [];
});

const makeSelectCreateEmergency = () => createSelector(selectEditEmergencyDomain(), (subState) => {
  console.debug(subState);
  return subState.response;
});
//2.selectEditEmergencyDomain()限定state的范围，然后subState => subState.isLoading从小的substate找到想要的值
const makeSelectEmergencyLoading = () => createSelector(selectEditEmergencyDomain(), subState => subState.isLoading);

export {
  makeSelectEmergencyTypes,
  makeSelectCreateEmergency,
  makeSelectEmergencyLoading,
};
