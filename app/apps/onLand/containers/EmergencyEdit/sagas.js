import { call, put, takeLatest } from 'redux-saga/effects';
import request from '../../../../utils/request';
import ApiFactory from '../../common/Api';
import {
  getEmergencyTypesRoutine,
  createEmergencyRoutine,
  updateEmergencyRoutine,
} from './actions';
import { errorMessage } from '../../../../components/ErrorHandler/actions';
//发送网络请求
function* getEmergencyTypes(action) {
  console.log(action);
  try {
    yield put(getEmergencyTypesRoutine.request());
    const range = action.payload;
    const response = yield call(
      request,
      ApiFactory.findPortEventTypes(range),
    );
    console.log('getEmergencyTypes', response.toString());
    yield put(getEmergencyTypesRoutine.success(response.dtoList));
  } catch (e) {
    console.log(e.message);
    yield put(errorMessage(e));
    yield put(getEmergencyTypesRoutine.failure(e));
  } finally {
    yield put(getEmergencyTypesRoutine.fulfill());
  }
}
//1.绑定action TRIGGER  getEmergencyTypes绑定上边的方法发送请求
export function* getEmergencyTypesSaga() {
  yield takeLatest(getEmergencyTypesRoutine.TRIGGER, getEmergencyTypes);
}

function* createEmergency(action) {
  console.log(action);
  try {
    yield put(createEmergencyRoutine.request());
    const response = yield call(
      request,
      ApiFactory.createEvent(action.payload),
    );
    console.log('createEmergency', response.toString());
    yield put(createEmergencyRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    if (error.response && error.response._backcode + '' === '830') {
      yield put(createEmergencyRoutine.success(error.response));
    } else {
      yield put(createEmergencyRoutine.failure(error));
    }
  } finally {
    yield put(createEmergencyRoutine.fulfill());
  }
}

export function* createEmergencySaga() {
  yield takeLatest(createEmergencyRoutine.TRIGGER, createEmergency);
}

function* updateEmergency(action) {
  console.log(action);
  try {
    yield put(updateEmergencyRoutine.request());
    const response = yield call(
      request,
      ApiFactory.updateEvent(action.payload),
    );
    console.log('updateEmergency', response.toString());
    yield put(updateEmergencyRoutine.success(response));
  } catch (error) {
    console.log(error.message);
    yield put(errorMessage(error));
    if (error.response && error.response._backcode + '' === '830') {
      yield put(updateEmergencyRoutine.success(error.response));
    } else {
      yield put(updateEmergencyRoutine.failure(error));
    }
  } finally {
    yield put(updateEmergencyRoutine.fulfill());
  }
}

export function* updateEmergencySaga() {
  yield takeLatest(updateEmergencyRoutine.TRIGGER, updateEmergency);
}

// All sagas to be loaded
//2.把sagas导出去
export default [
  getEmergencyTypesSaga,
  createEmergencySaga,
  updateEmergencySaga,
];
