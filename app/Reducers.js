import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';
import { AppNavigator } from './AppNavigator';

import ErrorHandlerReducer from './components/ErrorHandler/reducer';

import appReducer from './containers/AppInit/reducer';
import notificationCenterReducer from './components/NotificationCenter/reducer';
import loginReducer from './containers/Login/reducer';
import profileReducer from './containers/Profile/reducer';
import helpReducer from './containers/Help/reducer';
import helpDetailReducer from './containers/HelpDetail/reducer';
import ProblemBackReducer from './containers/ProblemBack/reducer';
import onLandReducers from './apps/onLand/Reducers';
import OAReducers from './apps/OA/Reducers';
import BusinessApprovalReducers from './apps/BusinessApproval/Reducers';

const initState = AppNavigator.router.getStateForAction(NavigationActions.init());
const navReducer = (state = initState, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state);
  return newState || state;
};

const AppReducer = combineReducers({
  nav: navReducer,
  notificationCenter: notificationCenterReducer,
  app: appReducer,
  login: loginReducer,
  profile: profileReducer,
  help: helpReducer,
  helpDetail: helpDetailReducer,
  error: ErrorHandlerReducer,
  problemBack: ProblemBackReducer,
  ...onLandReducers,
  ...OAReducers,
  ...BusinessApprovalReducers,
});

export default AppReducer;
