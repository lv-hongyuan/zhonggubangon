﻿UCML.Menu = function (id) {
    this.zIndex = 110000;
    this.left = 0;
    this.top = 0;
    this.onShow = function () { };
    this.onHide = function () { };

    this.items = [];

    this.menus = [];
    this.addEvents('menuItemClick');
    UCML.Menu.superclass.constructor.call(this, id);
}

UCML.extend(UCML.Menu, UCML.Commponent, {
});

UCML.Menu.prototype.init = function () {
    UCML.Menu.superclass.init.call(this);

    this.el.appendTo('body');
    this.el.addClass('menu'); // the top menu

    var menuList = window["menuList"] || [];

    menuList.add(this);
    window["menuList"] = menuList;
    //  this.addMenuItem('aa', 'aa');

    //  this.addSeparator('bb');


    this.el.css({
        left: this.left,
        top: this.top
    });
    this.el.hide();

    return;
    this.adjust(this.el);

    var opts = this;
    for (var i = 0; i < this.menus.length; i++) {
        var menu = this.menus[i];
        this.wrapMenu(menu);
        menu.find('>div.menu-item').each(function () {
            opts.bindMenuItemEvent($(this));
        });

        menu.find('div.menu-item').click(function () {
            // only the sub menu clicked can hide all menus
            if (!this.submenu) {
                opts.hideAll();
            }
            return false;
        });
    }




}


UCML.Menu.prototype.adjust = function (menu) {
    this.menus.push(menu);
    var opts = this;
    menu.find('>div').each(function () {
        var item = $(this);
        var submenu = item.find('>div');

        if (submenu.length) {

            submenu.insertAfter(opts.el);
            item[0].submenu = submenu;
            opts.adjust(submenu);
        }
    });
}

UCML.Menu.prototype.bindMenuItemEvent = function (item) {
    var opts = this;
    item.bind("click", function (e) {
        var menuId = item.attr("menuId");
        var menuItem = opts.getMenuItemById(menuId);
        opts.fireEvent('menuItemClick', $(this).attr("menuId"), menuItem);
        if (menuItem && menuItem.hander) {
            menuItem.hander.call(opts, menuItem, e);
        }
    });
    item.hover(
				function () {
				    // hide other menu
				    item.siblings().each(function () {
				        if (this.submenu) {
				            opts.hideMenu(this.submenu);
				        }
				        $(this).removeClass('menu-active');
				    });

				    // show this menu
				    item.addClass('menu-active');
				    var submenu = item[0].submenu;
				    if (submenu) {
				        var left = item.offset().left + item.outerWidth() - 2;
				        if (left + submenu.outerWidth() > $(window).width()) {
				            left = item.offset().left - submenu.outerWidth() + 2;
				        }
				        opts.showMenu(submenu, {
				            left: left,
				            top: item.offset().top - 3
				        });
				    }
				},
				function (e) {
				    item.removeClass('menu-active');
				    var submenu = item[0].submenu;
				    if (submenu) {
				        if (e.pageX >= parseInt(submenu.css('left'))) {
				            item.addClass('menu-active');
				        } else {
				            opts.hideMenu(submenu);
				        }

				    } else {
				        item.removeClass('menu-active');
				    }

				}
			);
}

UCML.Menu.prototype.wrapMenu = function (menu) {
    menu.addClass('menu').find('>div').each(function () {
        var item = $(this);
        if (item.hasClass('menu-sep')) {
            item.html('&nbsp;');
        } else {
            var text = item.addClass('menu-item').html();
            item.empty().append($('<div class="menu-text"></div>').html(text));
            var icon = item.attr('icon');
            if (icon) {
                $('<div class="menu-icon"></div>').addClass(icon).appendTo(item);
            }
            if (item[0].submenu) {
                $('<div class="menu-rightarrow"></div>').appendTo(item); // has sub menu
            }

            if ($.boxModel == true) {
                var height = item.height();
                item.height(height - (item.outerHeight() - item.height()));
            }
        }
    });
    menu.hide();
}

UCML.Menu.prototype.onDocClick = function (e) {

    this.hideAll();
    return false;
}

UCML.Menu.prototype.hideAll = function () {
    this.hideMenu(this.el);
    //  $(document).unbind('.menu');
    UCML.un(document, ".menu", this);
    this.onHide.call(this);
    return false;
}

UCML.Menu.prototype.showTopMenu = function (pos) {
    var opts = this;
    if (pos) {
        opts.left = pos.left;
        opts.top = pos.top;
    }
    this.showMenu(this.el, { left: opts.left, top: opts.top }, function () {
        //  $(document).bind('click.menu', this, opts.onDocClick);
        UCML.on(document, "click.menu", opts.onDocClick, opts);
        UCML.on(document, "contextmenu.menu", opts.onDocClick, opts);
        opts.onShow.call(opts);
    });
}

UCML.Menu.prototype.addSeparator = function (id) {
    var item = $("<div menuId=" + (id || UCML.id()) + " class=menu-sep  >&nbsp;</div>");
    this.el.append(item);
}

UCML.Menu.prototype.getMenuItem = function (index) {
    return this.items[index];
}

UCML.Menu.prototype.getMenuItemById = function (menuId) {
    var i = this.getMenuItemIndex(menuId);
    return this.items[i];
}

UCML.Menu.prototype.getMenuItemIndex = function (menuId) {
    for (var i = 0; i < this.items.length; i++) {
        if (menuId == this.items[i].menuId) {
            return i;
        }
    }
    return -1;
}

UCML.Menu.prototype.addMenuItem = function (menuId, text, iconcls, hander) {
    var menuItem = { menuId: menuId || UCML.id(), text: text || "", iconcls: iconcls || "", hander: hander };
    this.items.add(menuItem);
    var item = $("<div menuId=" + menuItem.menuId + " class=menu-item  ><div class=\"menu-text\">" + menuItem.text + "</div></div>");

    //   if (iconcls) {
    $('<div class="menu-icon"></div>').addClass(iconcls).appendTo(item);
    //   }
    if (item.submenu) {
        $('<div class="menu-rightarrow"></div>').appendTo(item); // has sub menu
    }
    menuItem.el = item;
    this.el.append(item);
    //  this.wrapMenu(menu);
    this.bindMenuItemEvent(item);
}



UCML.Menu.prototype.setCheckedMenuItem = function (menuId, iconcls) {
    iconcls = iconcls || "icon-ok";
    for (var i = 0; i < this.items.length; i++) {
        this.items[i].el.find(".menu-icon").removeClass(this.items[i].iconcls);
        if (this.items[i].menuId == menuId) {
            this.items[i].el.find(".menu-icon").addClass(iconcls);
            this.items[i].iconcls = iconcls;
        }
    }
}

UCML.Menu.prototype.showMenu = function (menu, pos, callback) {
    if (!menu) return;

    var opts = this;

    if (pos) {
        menu.css(pos);
    }
    //  menu.show();
    //   return;
    menu.show(1, function () {
        if (!menu[0].shadow) {
            menu[0].shadow = $('<div class="menu-shadow"></div>').insertAfter(menu);
        }
        menu[0].shadow.css({
            display: 'block',
            zIndex: opts.zIndex++,
            left: menu.css('left'),
            top: menu.css('top'),
            width: menu.outerWidth(),
            height: menu.outerHeight()
        });
        menu.css('z-index', opts.zIndex++);

        if (callback) {
            callback();
        }
    });
}

UCML.Menu.prototype.hideMenu = function (menu) {
    if (!menu) return;
    var opts = this;
    hideit(menu);
    menu.find('div.menu-item').each(function () {
        if (this.submenu) {
            opts.hideMenu(this.submenu);
        }
        $(this).removeClass('menu-active');
    });

    function hideit(m) {
        if (m[0].shadow) {
            m[0].shadow.hide();
        }
        m.hide();
    }
}

UCML.Menu.prototype.show = function (param) {
    this.showTopMenu(param);
}
UCML.Menu.prototype.hide = function () {
    this.hideAll();
}

UCML.Menu.hideMenuAll = function () {
    var menuList = window["menuList"] || [];
    for (var i = 0; i < menuList.length; i++) {
        menuList[i].hide();
    }
}


UCML.reg("UCML.Menu", UCML.Menu);