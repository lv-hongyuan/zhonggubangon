﻿
/**
* @class UCML.CheckBoxGroup
* @extends UCML.InputList
* @description 多选按钮列表控件
* @param {String} id 容器控件id
*/
UCML.CheckBoxGroup = function (id) {
    this.columns = 1;
    this.addEvents("itemchange", "blur");
    UCML.CheckBoxGroup.superclass.constructor.call(this, id);
    //显示列数
}

UCML.extend(UCML.CheckBoxGroup, UCML.InputList, {
    ctype: "UCML.CheckBoxGroup",
    onRender: function () {
        $('<table border="0" cellspacing="0" cellpadding="0"><tbody></tbody></table>').appendTo(this.el);
    }, setProperties: function () { 
        UCML.CheckBoxGroup.superclass.setProperties.call(this);
        this.columns = this.getAttribute("repeatColumns") || this.columns;
    }
});

UCML.CheckBoxGroup.prototype.init = function () {
    UCML.CheckBoxGroup.superclass.init.call(this);
};

UCML.CheckBoxGroup.prototype.renderedValue = function (val) {
    if (!UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.dataBind();
    }
    this.setValue(val);
    return this.getText();
}

/**   
* @method setValue 
* @description 设置控件值      
*/
UCML.CheckBoxGroup.prototype.setValue = function (value, trigger) {
    value = value || "";
    if (trigger !== false) {
        this.fireEvent("setValue", value);
    }
    var opt = this;
    var spv = value.split(';');

    this.el.find(":checkbox").each(function () {
        this.checked = false;
        $(this).attr("checked", false);
    });

    for (var i = 0; i < spv.length; i++) {
        this.el.find(":checkbox").each(function () {
            if (spv[i] == this.value) {
                this.checked = true;
            }
            $(this).attr("checked", this.checked);
        });
    }
    this.value = value;
    this.text = this.getText();
}
/**   
* @method change 
* @description 数据发生变化时     
*/
UCML.CheckBoxGroup.prototype.change = function (val, text) {
    this.value = this.getValue();
    this.text = this.getText();
    this.setValue(this.value);
}


/**   
* @method setColumns 
* @description 设置显示列数      
*/
UCML.CheckBoxGroup.prototype.setColumns = function (columns) {
    this.columns = columns;
}

/**   
* @description 启用控件      
*/
UCML.CheckBoxGroup.prototype.enable = function (ch) {
    this.setEnabledEdit(true);
}

/**   
* @description 禁用控件      
*/
UCML.CheckBoxGroup.prototype.disable = function (ch) {
    this.setEnabledEdit(false);
}


UCML.CheckBoxGroup.prototype.setEnabledEdit = function (value) {
    this.disabled = value ? false : true;
    if (this.disabled) {
        this.el.find("input").attr("disabled", "true");
    }
    else {
        this.el.find("input").removeAttr("disabled");
    }
}

/**   
* @method add 
* @description 添加一项    
*/
UCML.CheckBoxGroup.prototype.add = function (text, value, checked) {
    text = text || "";
    if (value) {
        var table = this.el.children("table");
        var length = table.find("td").length;
        var cid = this.id + "CheckList_" + length;

        if (length % this.columns == 0) {
            $('<tr></tr>').appendTo($('tbody', table));
        }
        var td = $('<td></td>');
        var input = $('<input id="' + cid + '" name="' + this.id + '" type="checkbox" value="' + value + '"   text="' + text + '" ' + (this.disabled ? "disabled" : "") + '  ' + (checked ? "checked=true" : "") + '>');
        var label = $('<label for="' + cid + '">' + text + '</label>');
        input.appendTo(td);
        label.appendTo(td);
        td.appendTo($(table[0].rows[table[0].rows.length - 1]));

        var opt = this;
        input.bind("change", function (e) {
            var el = $(this);
            var text = el.attr("text");
            el.attr("checked", this.checked);
            opt.change(this.value, text);
            opt.fireEvent("valuechange", opt.value, opt.text);
            opt.fireEvent("itemchange", this.value, text, this.checked);
        });
    }
}

/**   
* @method dataBind 
* @description 绑定控件   
*/
UCML.CheckBoxGroup.prototype.dataBind = function () {
    UCML.CheckBoxGroup.superclass.dataBind.call(this);
    this.el.children("table").children("tbody").empty();
    var recordCount = this.srcDataTable.getRecordCount();
    for (var i = 0; i < recordCount; i++) {
        this.srcDataTable.SetIndexNoEvent(i);
        this.add(this.srcDataTable.getFieldValue(this.dataTextField), this.srcDataTable.getFieldValue(this.dataValueField));
    }
    this.srcDataTable.SetIndexNoEvent(0);

    this.fireEvent("databind", this, this.input);
}

/**   
* @method getValue 
* @description 返回控件值      
* @return {Object} 
*/
UCML.CheckBoxGroup.prototype.getValue = function () {
    this.value = "";
    var opt = this;
    this.el.find("input:checked").each(function () {
        opt.value = opt.value + this.value + ";";
    });

    if (this.value.lastIndexOf(';') == this.value.length - 1) {
        this.value = this.value.substring(0, this.value.length - 1);
    }
    return this.value;
}

UCML.CheckBoxGroup.prototype.getText = function () {
    this.text = "";
    var opt = this;
    this.el.find("input:checked").each(function () {
        opt.text = opt.text + $(this).attr("text") + ";";
    });

    if (this.text.lastIndexOf(';') == this.text.length - 1) {
        this.text = this.text.substring(0, this.text.length - 1);
    }
    return this.text;
}


UCML.CheckBoxGroup.prototype.bindCodeTable = function () {
    UCML.CheckBoxGroup.superclass.bindCodeTable.call(this);
    this.el.children("table").children("tbody").empty();
    var codeList = BusinessObject.GetCodeValue(this.codeTable);
    if (codeList && codeList.length > 0) {
        for (var i = 0; i < codeList.length; i++) {
            this.add(codeList[i].caption, codeList[i].value);
        }
    }
}

UCML.CheckBoxGroup.prototype.bindCustomData = function (texts, values) {
    UCML.CheckBoxGroup.superclass.bindCustomData.call(this);
    this.el.children("table").children("tbody").empty();
    var valueList = values.split(";");
    var textList = texts.split(";");
    if (valueList && textList && valueList.length > 0) {
        for (var i = 0; i < valueList.length; i++) {
            this.add(textList[i], valueList[i]);
        }
    }
}

UCML.reg("UCML.CheckBoxGroup", UCML.CheckBoxGroup);