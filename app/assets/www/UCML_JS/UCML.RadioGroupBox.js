﻿UCML.RadioGroupBox = function (id) {
    /**   
    * @property dataValueField 
    * @description 值绑定字段
    * @type String
    */
    this.dataValueField;

    /**   
    * @property dataTextField 
    * @description 文本绑定字段
    * @type String
    */
    this.dataTextField;

    /**   
    * @property private dataValueField 
    * @description 数据源BC
    * @type UCML.DataTable
    */
    this.srcDataTable;

    this.codeTable;

    this.isCodeTable = false;

    this.Values;
    this.Captions;
    this.columns = 1;
    UCML.RadioGroupBox.superclass.constructor.call(this, id);
}

UCML.extend(UCML.RadioGroupBox, UCML.Combo, {
    ctype: "UCML.RadioGroupBox",
    autoEl: 'input',
    panelWidth: 180,
    panelHeight: 'auto',
    setProperties: function () {
        UCML.Combo.superclass.setProperties.call(this);
        this.codeTable = this.getAttribute("codeTable") || this.codeTable;
        this.isCodeTable = (this.el.attr('isCodeTable') ? this.getAttribute('isCodeTable')
== 'true' : false) || this.isCodeTable;
        this.srcDataTable = BusinessObject.getDataTableByBCName(this.getAttribute
("srcBCName")) || undefined;
        this.dataValueField = this.getAttribute("dataValueField") || this.dataValueField;
        this.dataTextField = this.getAttribute("dataTextField") || this.dataTextField;
        this.columns = this.getAttribute("columns") || this.columns;
        this.Values = this.getAttribute("Values") || this.Values;
        this.Captions = this.getAttribute("Captions") || this.Captions;
    },
    onRender: function () {
        this.el.attr('readonly', "true");
        UCML.RadioGroupBox.superclass.onRender.call(this);
        UCML.un(this.el, "blur");

        if (!this.RadioGroup) {
            this.RadioGroup = new UCML.RadioGroup({ columns: this.columns, isSetProperties: false,
                codeTable: this.codeTable, isCodeTable: this.isCodeTable, dataTable: this.dataTable
        , srcDataTable: this.srcDataTable, dataValueField: this.dataValueField, dataTextField: this.dataTextField
        , fieldName: this.fieldName
        , rendeTo: this.panel.body
        , Captions: this.Captions
        , Values: this.Values,BusinessObject:this.BusinessObject
            });
            this.RadioGroup.on("valuechange", this.change, this);
        }
        UCML.on(this.el, "blur", function () {
            this.setValue(this.value, false);
         //   this.hidePanel();  解决IE8下的问题
        }, this);
    },
    bindEvents: function () {
        UCML.RadioGroupBox.superclass.bindEvents.call(this);
        this.RadioGroup.on("dataBind", function () {

        }, this);
    },
    hidePanel: function () {
        UCML.RadioGroupBox.superclass.hidePanel.call(this);
    },
    change: function (val, text) {
        this.value = val;
        this.text = text;
        this.setValue(this.value);
        this.setText(this.text);
    },
    setValue: function (value, trigger) {
        if (trigger !== false) {
            this.fireEvent("setValue", value);
        }
        this.value = value;
        if (this.RadioGroup) {
            this.RadioGroup.setValue(this.value);
            this.setText(this.RadioGroup.getText());
        }
        this.setValues([value]);
    }
});

/**
* @method renderedValue
* @description 重写该函数，通过value显示text
*/
UCML.RadioGroupBox.prototype.renderedValue = function (val) {
    if (this.RadioGroup && !UCML.isEmpty(this.srcDataTable) && !UCML.isEmpty(this.dataValueField) && !
UCML.isEmpty(this.dataTextField)) {
        this.RadioGroup.dataBind();
    }
    this.setValue(val);
    return this.getText();
}

/**
* @method bindCodeTable
* @description 绑定代码表
*/
UCML.RadioGroupBox.prototype.bindCodeTable = function () { 
    
}
UCML.reg("UCML.RadioGroupBox", UCML.RadioGroupBox);