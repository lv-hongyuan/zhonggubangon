﻿
/**
* @class UCML.NumericEdit
* @extends UCML.Input
* @description 多功能文本框
* @param {String} id 容器控件id
*/
UCML.NumericEdit = function (id) {
    //UCML标准接口
    //属性
    this.LowerButtonDisabledImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/LD.gif";
    this.LowerButtonHoverImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/LH.gif";
    this.LowerButtonImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/LB.gif";
    this.LowerButtonPressedImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/LP.gif";
    this.LowerButtonTooltip = "";
    this.UpperButtonDisabledImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/UD.gif";
    this.UpperButtonHoverImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/UH.gif";
    this.UpperButtonImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/UB.gif";
    this.UpperButtonPressedImageUrl = UCMLLocalResourcePath + "Images/UCMLInput/UP.gif";
    this.UpperButtonTooltip = "";

    this.addEvents("blur", "focus");
    UCML.NumericEdit.superclass.constructor.call(this, id);
}

UCML.extend(UCML.NumericEdit, UCML.Input, {
    ctype: "UCML.NumericEdit",

    /**   
    * @property InputID 
    * @description InputID
    * @type String
    */
    InputID: "",

    /**   
    * @property EditMode 
    * @description 数字＼Mask日期等
    * @type Int
    * @defualt 4
    */
    EditMode: 4,

    /**   
    * @property MaxValue 
    * @description 最大值
    * @type Int
    * @defualt 0
    */
    MaxValue: 0,

    /**   
    * @property MinValue 
    * @description 最小值
    * @type Int
    * @defualt 0
    */
    MinValue: 0,

    /**   
    * @property MinDecimalPlaces 
    * @description 小数位
    * @type Int
    * @defualt 0
    */
    MinDecimalPlaces: 0,

    /**   
    * @property MaxLength 
    * @description 小数位
    * @type Int
    * @defualt 0
    */
    MaxLength: 0, //最大长度

    /**   
    * @property DataMode 
    * @description 小数位
    * @type Int
    * @defualt 1
    */
    DataMode: 1,

    /**   
    * @property ReadOnly 
    * @description 是否只读
    * @type Boolean
    * @defualt false
    */
    ReadOnly: false,

    /**   
    * @property HorizontalAlign 
    * @description 垂直布局方式
    * @type String
    * @defualt right
    */
    HorizontalAlign: "right",

    /**   
    * @property Delta 
    * @description Spin步距
    * @type Int
    * @defualt 1
    */
    Delta: 1,

    /**   
    * @property SpinButtonDisplay 
    * @description 快速按钮模式 None OnRight OnLef
    * @type String
    * @defualt OnRight
    */
    SpinButtonDisplay: "OnRight",

    /**   
    * @property InputMask 
    * @description 录入格式码 EditMode:1时
    * @type String
    * @defualt 
    */
    InputMask: "",

    /**   
    * @property DisplayModeFormat 
    * @description 日期的显示格式
    * @type String
    * @defualt D
    */
    DisplayModeFormat: "D",

    /**   
    * @property EditModeFormat 
    * @description 日期的编辑格式
    * @type String
    * @defualt D
    */
    EditModeFormat: "D"


});

UCML.NumericEdit.prototype.setProperties = function () {

    UCML.NumericEdit.superclass.setProperties.call(this);
    this.EditMode = this.el.attr('EditMode') || this.EditMode;
    this.MaxValue = this.el.attr('MaxValue') || this.MaxValue;
    this.MinValue = this.el.attr('MinValue') || this.MinValue;
    this.MinDecimalPlaces = (parseInt(this.el.attr('MinDecimalPlaces')) || this.MinDecimalPlaces);
    this.MaxLength = (parseInt(this.el.attr('MaxLength')) || this.MaxLength);
    this.DataMode = this.el.attr('DataMode') || this.DataMode;
    this.ReadOnly = (this.el.attr('ReadOnly'));
    this.HorizontalAlign = this.el.attr('HorizontalAlign') || this.HorizontalAlign;
    this.Delta = this.el.attr('Delta') || this.Delta;
    this.SpinButtonDisplay = this.el.attr('SpinButtonDisplay') || this.SpinButtonDisplay;
    this.InputMask = this.el.attr('InputMask') || this.InputMask;
    this.DisplayModeFormat = this.el.attr('DisplayModeFormat') || this.DisplayModeFormat;
    this.EditModeFormat = this.el.attr('EditModeFormat') || this.EditModeFormat;
}
/**   
* @method Init 
* @description 初始界面   
*/
UCML.NumericEdit.prototype.onRender = function () {

    UCML.NumericEdit.superclass.onRender.call(this);


    this.el.addClass("numericedit");
    //------ 初始界面 ---------
    var oTable = window.document.createElement("TABLE");
    oTable.className = "numericedit-table";
    oTable.cellSpacing = 0;
    oTable.cellPadding = 0;
    //    oTable.style.width = "100%";
    //    oTable.style.tableLayout = "fixed";
    this.InputID = this.dom.id + "_Input";
    oTable.id = "igtxt" + this.InputID;

    var oTR = oTable.insertRow(-1);
    if (this.EditMode == 5) {

        if (this.SpinButtonDisplay == "OnRight") {
            var td = oTR.insertCell(-1);
            td.vAlgign = "middle";
            this.createInput(td);
            td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);
        }
        else if (this.SpinButtonDisplay == "OnLeft") {
            var td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);
            td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        else {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        this.dom.appendChild(oTable);

        if (this.MaxValue != 0) {
            igedit_init(this.InputID, 5, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
				this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
				this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
				",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
				["", "..", ",", "-", "￥", "", "$n", "$-n", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 1, this.MinValue, 1, this.MaxValue, "Indigo", "", 3]);
        }
        else {
            igedit_init(this.InputID, 5, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
					this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
					this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
					",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
					["", "..", ",", "-", "￥", "", "$n", "$-n", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 0, 0, "Indigo", "", 3]);
        }
    }
    else if (this.EditMode == 6) {

        if (this.SpinButtonDisplay == "OnRight") {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
            td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);

        }
        else if (this.SpinButtonDisplay == "OnLeft") {
            var td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);
            td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        else {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        this.dom.appendChild(oTable);

        if (this.MaxValue != 0) {
            igedit_init(this.InputID, 6, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
				this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
				this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
				",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
				["", "..", ",", "-", "%", "", "n$", "-n$", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 1, this.MinValue, 1, this.MaxValue, "Indigo", "", 3]);
        }
        else {
            igedit_init(this.InputID, 6, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
					this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
					this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
					",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
					["", "..", ",", "-", "%", "", "n$", "-n$", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 0, 0, "Indigo", "", 3]);
        }
    }
    //日期格式
    else if (this.EditMode == 2) {
        if (this.SpinButtonDisplay == "OnRight") {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
            td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);

        }
        else if (this.SpinButtonDisplay == "OnLeft") {
            var td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);
            td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        else {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        this.dom.appendChild(oTable);

        var __DisplayModeFormat = "03年04月08日";
        var __EditModeFormat = "03年04月08日";
        if (this.DisplayModeFormat == "D") __DisplayModeFormat = "03年04月08日";
        else if (this.DisplayModeFormat == "F") __DisplayModeFormat = "03年04月08日 12:17:19";
        else if (this.DisplayModeFormat == "T") __DisplayModeFormat = "12:17:19";
        else if (this.DisplayModeFormat == "M") __DisplayModeFormat = "03年04月";
        else if (this.DisplayModeFormat == "Y") __DisplayModeFormat = "03年";

        if (this.EditModeFormat == "D") __EditModeFormat = "03年04月08日";
        else if (this.EditModeFormat == "F") __EditModeFormat = "03年04月08日 12:17:19";
        else if (this.EditModeFormat == "T") __EditModeFormat = "12:17:19";
        else if (this.EditModeFormat == "M") __EditModeFormat = "03年04月";
        else if (this.EditModeFormat == "Y") __EditModeFormat = "03年";

        var d = new Date();
        var dt = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + '-' + d.getHours() + '-' + d.getMinutes() + '-' + d.getMilliseconds();

        igedit_init(this.InputID, 2, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
				this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
				this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
				",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
				[dt + "-0,,", __DisplayModeFormat, "_  03", __EditModeFormat, "0", 29, 0]);
    }
    else if (this.EditMode == 1) {
        var td = oTR.insertCell(-1);
        td.vAlgign = "top";
        this.createInput(td);
        this.dom.appendChild(oTable);

        igedit_init(this.InputID, 1, this.InputID + ",,1,,,,0,1,1,,,0,1,,-1,", ["", this.InputMask, "_  10"]);
    }
    else if (this.EditMode == 0) {
        var td = oTR.insertCell(-1);
        td.vAlgign = "top";
        this.createInput(td);
        this.dom.appendChild(oTable);

        igedit_init(this.InputID, 0, this.InputID + ",,1,,,," + this.MaxLength + ",1,1,,1,2,1,,-1,", [""]);

    }
    else if (this.EditMode == 4) {
        if (this.SpinButtonDisplay == "OnRight") {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
            td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);

        }
        else if (this.SpinButtonDisplay == "OnLeft") {
            var td = oTR.insertCell(-1);
            td.width = 15;
            this.createSpinButton(td);
            td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        else {
            var td = oTR.insertCell(-1);
            td.vAlgign = "top";
            this.createInput(td);
        }
        this.dom.appendChild(oTable);

        if (this.MaxValue != 0) {
            igedit_init(this.InputID, 4, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
				this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
				this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
				",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
				["", "..", ",", "-", "", "", "n", "-n", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 1, this.MinValue, 1, this.MaxValue, "Indigo", "", 3]);
        }
        else {
            //            igedit_init(this.InputID, 4, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
            //					this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
            //					this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
            //					",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
            //					["", "..", ",", "-", "", "", "n", "-n", this.DataMode, this.MinDecimalPlaces, 0, 0, 0, "Indigo", "", 3]);

            igedit_init(this.InputID, 4, this.InputID + ", ,,,,," + this.MaxLength + "," + this.Delta + ",1,,,1,0,1,7,buttontext,,,Gray," + this.UpperButtonImageUrl + "," +
					this.UpperButtonHoverImageUrl + "," + this.UpperButtonPressedImageUrl + "," + this.UpperButtonDisabledImageUrl + "," +
					this.LowerButtonImageUrl + "," + this.LowerButtonHoverImageUrl + "," + this.LowerButtonPressedImageUrl + "," + this.LowerButtonDisabledImageUrl +
					",6,,100,,numericedit0,numericedit1,numericedit2,numericedit3,1",
					["", "..", ",", "-", "", "", "n", "-n", this.DataMode, this.MinDecimalPlaces, this.MinDecimalPlaces, 0, 0, "Indigo", "", 3]);
        }
    }
    var o = igedit_all[this.InputID];
    o.elem.sender = this;
    o.addEventListener("valuechange", this.onChange);
}

UCML.NumericEdit.prototype.bindEvents = function () {
    UCML.NumericEdit.superclass.bindEvents.call(this);
    //  var opts = this;
    var o = igedit_all[this.InputID];
    o.elem.sender = this;
    o.addEventListener("blur", function (e) { e.elem.sender.fireEvent("blur"); });
}

UCML.NumericEdit.prototype.focus = function () {
    igedit_all[this.InputID].focus();
}


/**   
* @method setValue 
* @description 设置控件值 
* @param {String} value 值    ZZZZZZZZZZzzzZ
*/
UCML.NumericEdit.prototype.setValue = function (value) {
    this.value = value;
    var o = igedit_all[this.InputID];
    if (o) {
        o.setValue(value);
    }
}

/**   
* @method leadingZero 
* @description 空缺补0
* @param {String} value 值    
*/
UCML.NumericEdit.prototype.leadingZero = function (value) {
    if (value < 10)
        return '0' + value.toString()
    else
        return value.toString()
}

/**   
* @method getValue 
* @description 返回控件值      
* @return {Object} 
*/
UCML.NumericEdit.prototype.getValue = function () {
    var o = igedit_all[this.InputID];
    if (this.EditMode == 1) {
        var value = o.getValue();
        return value;
    }
    else if (this.EditMode == 4) {
        return o.getNumber(this.InputID);
    }
    else if (this.EditMode == 2) {
        var d = o.getValue();
        if (d == null) d = new Date();
        var day = this.leadingZero(d.getDate())
        var month = this.leadingZero(d.getMonth() + 1)
        var year = d.getFullYear()
        var hours = this.leadingZero(d.getHours())
        var minutes = this.leadingZero(d.getMinutes())
        var seconds = this.leadingZero(d.getSeconds())
        var value = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
        return value;
    }
    else {
        var value = o.getValue();
        return value;
    }
}

UCML.NumericEdit.prototype.renderedValue = function (val) {
    var o = igedit_all[this.InputID];
    return o.getRenderedValue(val);
}

/**   
* @method getText 
* @description 获得文本值      
* @return {Object} 
*/
UCML.NumericEdit.prototype.getText = function () {
    var o = igedit_all[this.InputID];
    if (this.EditMode == 4) {
        var value = o.getNumber(this.InputID);
        return o.getRenderedValue(value);
    }
    else {
        var value = o.getValue();
        return o.getRenderedValue(value);
    }
}

/**   
* @method createSpinButton 
* @description 创建分割按钮
* @param {Element} TD 单元格    
*/
UCML.NumericEdit.prototype.createSpinButton = function (td) {
    td.innerHTML = '<TABLE cellSpacing=0 cellPadding=0 width=15 border=0 style="text-align:center;height:100%;vertical-align:bottom" > ' +
    '<TBODY> ' +
    '<TR>' +
    '<TD class=ucml-numericedit-td vAlign=top ><IMG  id=' + this.InputID + '_b1 ' +
    ' app="' + this.UpperButtonImageUrl + '"  border=0> </TD></TR>' +
    '<TR>' +
    '<TD class=ucml-numericedit-td  vAlign=top ><IMG  id=' + this.InputID + '_b2 ' +
    ' 	app="' + this.LowerButtonImageUrl + '" ' +
    ' border=0> ' +
    '</TD></TR></TBODY></TABLE>	';
}

/**   
* @method createInput 
* @description 创建文本框
* @param {Element} TD 单元格    
*/
UCML.NumericEdit.prototype.createInput = function (td) {
    this.input = window.document.createElement("input");
    this.input.className = "numericedit-text";
    this.input.style.textAlign = this.HorizontalAlign;
    this.input.id = this.InputID + "_t";
    //  this.input.style.width = "100%";
    this.input.readOnly = this.ReadOnly;
    td.appendChild(this.input);
}

/**   
* @method onChange 
* @description 数据变化时   
*/
UCML.NumericEdit.prototype.onChange = function (e, v) {

    e.elem.sender.value = e.elem.sender.getValue();
    e.elem.sender.fireEvent("setValue", e.elem.sender.value);
    //    if (e.elem.sender.dataTable) {
    //        e.elem.sender.dataTable.setFieldValue(e.elem.sender.fieldName, e.elem.sender.value);
    //    }
}


/**   
* @method getEditMode 
* @description  获得编辑模式   
*/
UCML.NumericEdit.prototype.getEditMode = function () {
    return this.EditMode;
}

/**   
* @method setEditMode 
* @description 设置编辑模式  
* @param {String} ) value 值     
*/
UCML.NumericEdit.prototype.setEditMode = function (value) {
    this.EditMode = value;
}

/**   
* @method getEditMode 
* @description  获得录入格式   
*/
UCML.NumericEdit.prototype.getInputMask = function () {
    return this.InputMask;
}

/**   
* @method setInputMask 
* @description 设置录入格式   
* @param {String} ) value 值     
*/
UCML.NumericEdit.prototype.setInputMask = function (value) {
    this.InputMask = value;
    var o = igedit_all[this.InputID];
    if (o != null) o.setInputMask(value);
}

/**   
* @method getMaxValue 
* @description 获得最大值   
*/
UCML.NumericEdit.prototype.getMaxValue = function () {
    return this.MaxValue;
}

/**   
* @method setMaxValue 
* @description 设置最大值 
* @param {Int} ) value 值     
*/
UCML.NumericEdit.prototype.setMaxValue = function (value) {
    this.MaxValue = value;
}

/**   
* @method getMinValue 
* @description 获得最小值   
*/
UCML.NumericEdit.prototype.getMinValue = function () {
    return this.MinValue;
}

/**   
* @method setMinValue 
* @description 设置最小值 
* @param {Int} ) value 值     
*/
UCML.NumericEdit.prototype.setMinValue = function (value) {
    this.MinValue = value;
}

/**   
* @method getMaxLength 
* @description 获得最大长度   
*/
UCML.NumericEdit.prototype.getMaxLength = function () {
    return this.MaxLength;
}

/**   
* @method setMaxLength 
* @description 设置最大长度 
* @param {Int} ) value 值     
*/
UCML.NumericEdit.prototype.setMaxLength = function (value) {
    this.MaxLength = value;
}

/**   
* @method getDataMode 
* @description 获得最大长度   
*/
UCML.NumericEdit.prototype.getDataMode = function () {
    return this.DataMode;
}

/**   
* @method setDataMode 
* @description 设置数据类型 
* @param {String} ) value 值     
*/
UCML.NumericEdit.prototype.setDataMode = function (value) {
    this.DataMode = value;
}

/**   
* @method getMinDecimalPlaces 
* @description 获得小数位 
*/
UCML.NumericEdit.prototype.getMinDecimalPlaces = function () {
    return this.MinDecimalPlaces;
}

/**   
* @method setMinDecimalPlaces 
* @description 设置小数位 
* @param {String} ) value 值     
*/
UCML.NumericEdit.prototype.setMinDecimalPlaces = function (value) {
    this.MinDecimalPlaces = value;
}

/**   
* @method getReadOnly 
* @description 获得只读状态 
*/
UCML.NumericEdit.prototype.getReadOnly = function () {
    return this.ReadOnly;
}

/**   
* @method setReadOnly 
* @description 设置只读状态 
* @param {Boolean} ) value 值     
*/
UCML.NumericEdit.prototype.setReadOnly = function (value) {
    this.ReadOnly = value;
    var o = igedit_all[this.InputID];
    if (this.ReadOnly == "false" || this.ReadOnly == false) {
        o.setReadOnly(false);
        this.input.disabled = false;
    }
    else {
        o.setReadOnly(true);
        this.input.disabled = true;
    }
}

/**   
* @method getEnabledEdit 
* @description 获得启用状态 
*/
UCML.NumericEdit.prototype.getEnabledEdit = function (value) {
    return this.EnabledEdit;
}

/**   
* @method setEnabledEdit 
* @description 设置启用状态 
* @param {Boolean} ) value 值     
*/
UCML.NumericEdit.prototype.setEnabledEdit = function (value) {
    this.EnabledEdit = value;
    var o = igedit_all[this.InputID];
    if (this.EnabledEdit == "false" || this.EnabledEdit == false) {
        this.input.style.disabled = true;
        o.setEnabled(false);
    }
    else {
        o.setEnabled(true); 
    }
}

/**   
* @description 启用控件      
*/
UCML.NumericEdit.prototype.enable = function (ch) {
    this.setEnabledEdit(true);
}

/**   
* @description 禁用控件      
*/
UCML.NumericEdit.prototype.disable = function (ch) {
    this.setEnabledEdit(false);
}


UCML.NumericEdit.prototype.getHorizontalAlign = function () {
    return this.HorizontalAlign;
}

UCML.NumericEdit.prototype.setHorizontalAlign = function (value) {
    this.HorizontalAlign = value;
}

//Spin步距
UCML.NumericEdit.prototype.getDelta = function () {
    return this.Delta;
}

UCML.NumericEdit.prototype.setDelta = function (value) {
    this.Delta = value;
}

//快速按钮模式 None OnRight OnLeft
UCML.NumericEdit.prototype.getSpinButtonDisplay = function () {
    return this.SpinButtonDisplay;
}

UCML.NumericEdit.prototype.setSpinButtonDisplay = function (value) {
    this.SpinButtonDisplay = value;
}


UCML.NumericEdit.prototype.getUpperButtonTooltip = function () {
    return this.UpperButtonTooltip;
}

UCML.NumericEdit.prototype.setUpperButtonTooltip = function (value) {
    this.UpperButtonTooltip = value;
}

UCML.NumericEdit.prototype.getUpperButtonPressedImageUrl = function () {
    return this.UpperButtonPressedImageUrl;
}

UCML.NumericEdit.prototype.setUpperButtonPressedImageUrl = function (value) {
    this.UpperButtonPressedImageUrl = value;
}
UCML.NumericEdit.prototype.getUpperButtonImageUrl = function () {
    return this.UpperButtonImageUrl;
}

UCML.NumericEdit.prototype.setUpperButtonImageUrl = function (value) {
    this.UpperButtonImageUrl = value;
}


UCML.NumericEdit.prototype.getUpperButtonHoverImageUrl = function () {
    return this.UpperButtonHoverImageUrl;
}

UCML.NumericEdit.prototype.setUpperButtonHoverImageUrl = function (value) {
    this.UpperButtonHoverImageUrl = value;
}
UCML.NumericEdit.prototype.getUpperButtonDisabledImageUrl = function () {
    return this.UpperButtonDisabledImageUrl;
}

UCML.NumericEdit.prototype.setUpperButtonDisabledImageUrl = function (value) {
    this.UpperButtonDisabledImageUrl = value;
}
UCML.NumericEdit.prototype.getLowerButtonTooltip = function () {
    return this.LowerButtonTooltip;
}

UCML.NumericEdit.prototype.setLowerButtonTooltip = function (value) {
    this.LowerButtonTooltip = value;
}
//
UCML.NumericEdit.prototype.getLowerButtonPressedImageUrl = function () {
    return this.LowerButtonPressedImageUrl;
}

UCML.NumericEdit.prototype.setLowerButtonPressedImageUrl = function (value) {
    this.LowerButtonPressedImageUrl = value;
}

UCML.NumericEdit.prototype.getLowerButtonImageUrl = function () {
    return this.LowerButtonImageUrl;
}

UCML.NumericEdit.prototype.setLowerButtonImageUrl = function (value) {
    this.LowerButtonImageUrl = value;
}
UCML.NumericEdit.prototype.getLowerButtonHoverImageUrl = function () {
    return this.LowerButtonHoverImageUrl;
}

UCML.NumericEdit.prototype.setLowerButtonHoverImageUrl = function (value) {
    this.LowerButtonHoverImageUrl = value;
}

UCML.NumericEdit.prototype.getLowerButtonDisabledImageUrl = function () {
    return this.LowerButtonDisabledImageUrl;
}

UCML.NumericEdit.prototype.setLowerButtonDisabledImageUrl = function (value) {
    this.LowerButtonDisabledImageUrl = value;
}

//<!-- 日期的显示格式 -->
UCML.NumericEdit.prototype.getDisplayModeFormat = function () {
    return this.DisplayModeFormat;
}

UCML.NumericEdit.prototype.setDisplayModeFormat = function (value) {
    this.DisplayModeFormat = value;
}


//<!-- 日期的显示格式 -->
UCML.NumericEdit.prototype.getEditModeFormat = function () {
    return this.EditModeFormat;
}

UCML.NumericEdit.prototype.setEditModeFormat = function (value) {
    this.EditModeFormat = value;
}

UCML.reg("UCML.NumericEdit", UCML.NumericEdit);