﻿UCML.UserInfo ={};

/**    
* @method public getUserOID 
* @description 得到登陆者用户OID
* @return {Object}
*/
UCML.UserInfo.getUserOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.userOID = BusinessData.UCMLUserSystemInfo[0][0] || "00000000-0000-0000-0000-000000000000"; //用户OID
    }
    return UCML.UserInfo.userOID || "00000000-0000-0000-0000-000000000000";
}

/**    
* @method public getPersonOID 
* @description 得到登陆者人员信息OID
* @return {Object}
*/
UCML.UserInfo.getPersonOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.personOID = BusinessData.UCMLUserSystemInfo[0][1] || "00000000-0000-0000-0000-000000000000"; //人员信息OID
    }
    return UCML.UserInfo.personOID || "00000000-0000-0000-0000-000000000000";
}

UCML.UserInfo.getUserId = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.userId = BusinessData.UCMLUserSystemInfo[0][2] || ""; //用户id
    }
    return UCML.UserInfo.userId || "";
}

/**    
* @method public getMasterPostOID 
* @description 得到登陆者主岗位OID
* @return {Object}
*/
UCML.UserInfo.getMasterPostOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.masterPostOID = BusinessData.UCMLUserSystemInfo[0][3] || "00000000-0000-0000-0000-000000000000"; //人员主岗位OID
    }
    return UCML.UserInfo.masterPostOID || "00000000-0000-0000-0000-000000000000";
}

/**    
* @method public getPostPrimaryOID 
* @description 得到登陆者主岗位负责人OID
* @return {Object}
*/
UCML.UserInfo.getPostPrimaryOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.postPrimaryOID = BusinessData.UCMLUserSystemInfo[0][4] || "00000000-0000-0000-0000-000000000000";
    }
    return UCML.UserInfo.postPrimaryOID || "00000000-0000-0000-0000-000000000000";
}

/**    
* @method public getPostnName 
* @description 得到登陆者部门OID
* @return {Object}
*/
UCML.UserInfo.getDivisionOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.divisionOID = BusinessData.UCMLUserSystemInfo[0][5] || "00000000-0000-0000-0000-000000000000"; //部门OID
    }
    return UCML.UserInfo.divisionOID || "00000000-0000-0000-0000-000000000000";
}

/**    
* @method public getOrgOID 
* @description 得到登陆者组织OID
* @return {Object}
*/
UCML.UserInfo.getOrgOID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.orgOID = BusinessData.UCMLUserSystemInfo[0][6] || "00000000-0000-0000-0000-000000000000"; //组织OID
    }
    return UCML.UserInfo.orgOID || "00000000-0000-0000-0000-000000000000";
}

UCML.UserInfo.getPostnName = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.postnName = BusinessData.UCMLUserSystemInfo[0][7] || ""; //人员主岗位名称
    }
    return UCML.UserInfo.postnName || "";
}

UCML.UserInfo.getPersonName = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.personName = BusinessData.UCMLUserSystemInfo[0][8] || ""; //人员名称
    }
    return UCML.UserInfo.personName || "";
}

UCML.UserInfo.getPersonName = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.personName = BusinessData.UCMLUserSystemInfo[0][8] || ""; //人员名称
    }
    return UCML.UserInfo.personName || "";
}

UCML.UserInfo.getLoginState = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.loginState = BusinessData.UCMLUserSystemInfo[0][9];
    }
    return UCML.UserInfo.loginState || "";
}

UCML.UserInfo.getLoginTime = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.loginTime = BusinessData.UCMLUserSystemInfo[0][10];
    }
    return UCML.UserInfo.loginTime || "";
}

UCML.UserInfo.getSSID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.SSID = BusinessData.UCMLUserSystemInfo[0][11];
    }
    return UCML.UserInfo.SSID || "";
}

UCML.UserInfo.getOrgNO = function () {//公司编号
var config ={methodName:"GetOrgNoOrDivisionNo", params:{infotype:1}, async:false};
UCML.UserInfo.orgNO=invoke(config);
    return UCML.UserInfo.orgNO || "";
}

UCML.UserInfo.getDepartNO = function () {//部门编号
    var config ={methodName:"GetOrgNoOrDivisionNo", params:{infotype:0}, async:false};
    UCML.UserInfo.departNO= invoke(config);
    return UCML.UserInfo.departNO || "";
}

UCML.UserInfo.getPostnID = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.postnID = BusinessData.UCMLUserSystemInfo[0][14]; //岗位编号
    }
    return UCML.UserInfo.postnID || "";
}
UCML.UserInfo.getSystemtime = function () {//获取服务器时间
    var config ={methodName:"GetSystemDateTime", params:{}, async:false};
    return invoke(config);
}
UCML.UserInfo.getOrgName = function () {
    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.orgName= BusinessData.UCMLUserSystemInfo[0][12]; //组织名称
    }
return UCML.UserInfo.orgName || "";
}
UCML.UserInfo.getDivisionName = function () {

    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.divisionName= BusinessData.UCMLUserSystemInfo[0][13]; //部门名称
    }
return UCML.UserInfo.divisionName || "";
}
UCML.UserInfo.getRESPOID = function () {

    if (BusinessData.UCMLUserSystemInfo != null && BusinessData.UCMLUserSystemInfo.length > 0) {
        UCML.UserInfo.respOID= BusinessData.UCMLUserSystemInfo[0][21]; //责任列表
    }
return UCML.UserInfo.respOID || "";
}