/*!
* UCML.GridSearch 
* Date: 2013-01-13 
* author sunyi
*  
*  JS版grid查询对话面板
*        
*        2013-01-13:加入 所有的不等于，不包含 情况加入 is null处理
*        2013-01-13:加入 全局变量dbType 方便处理oralce数据库时间类型查询语句生成
*        2013-01-13:加入 处理可子表查询情况处理
*        2013-01-13:加入 方法getCondiSQL 可获取查询设置的SQL语句
*        2013-03-12:解决  连接虚字段查询问题
*		  2013-04-12:解决 某些场景中增加，删除按钮不出来
*
*
*/

/**
* @class UCML.GridSearch
* @extends UCML.Window
* @description 窗口查询框
* @param {String} id 容器控件id
*/
UCML.GridSearch = function (id) {
    this.id = id + "_Search";
    this.renderTo = id;
    //this.rowHeight = 20;
    //this.width = 600;
    this.controls = new UCML.Common.HasTable();
    UCML.GridSearch.superclass.constructor.call(this, id);
}

UCML.extend(UCML.GridSearch, UCML.Window, {
    // 是否能被拖动  
    draggable: true,
    //是否能改变窗体大小
    resizable: false,
    //是否有阴影，有些皮肤无此效果
    showShadow: true,
    //是否遮盖（模态） 
    modal: true,
    //是否有外框,部分皮肤不生效
    border: false,
    // 是否有关闭按钮
    closable: true,
    //窗体标题
    title: "查询",
    maximizable: false,
    collapsible: false,
    //窗体高度    
    height: 400,
    // 窗体宽度
    width: 640,
    left: 100,
    top: 100,
    //要查询的VC对象
    applet: null,
    //列宽设置器
    widthCell: [],
    //数据库种类 0:mssql,1:oralce,多种数据库可扩展
    dbType: 0,
    //操作消息提示面板          
    searchMsgPanel: $('<DIV style="BORDER-RIGHT: #0066ff 1px solid; BORDER-TOP: #0066ff 1px solid; Z-INDEX: 103; LEFT: 201px; VISIBILITY: hidden; BORDER-LEFT: #0066ff 1px solid; WIDTH: 272px; BORDER-BOTTOM: #0066ff 1px solid; POSITION: absolute; TOP: 229px; HEIGHT: 27px; BACKGROUND-COLOR: #ffffcc; TEXT-ALIGN: center" ms_positioning="FlowLayout" id="searchMsgPanel"></DIV>'),
    init: function () {
        this.rendeTo = window.document.body;
        UCML.GridSearch.superclass.init.call(this);
    },
    onRender: function () {
        UCML.GridSearch.superclass.onRender.call(this);
        if (this.widthCell.length == 0) {
            /*this.widthCell[0] = (this.width-14) * 0.08 + "px";
            this.widthCell[1] = (this.width-14) * 0.22 + "px";
            this.widthCell[2] = (this.width-14) * 0.12 + "px";
            this.widthCell[3] = (this.width-14) * 0.22 + "px";
            this.widthCell[4] = (this.width-14) * 0.08 + "px";
            this.widthCell[5] = (this.width-14) * 0.12 + "px";
            this.widthCell[6] = (this.width-14) * 0.08 + "px";
            this.widthCell[7] = (this.width - 14) * 0.08 + "px";*/

            this.widthCell[0] = "8%";
            this.widthCell[1] = "22%";
            this.widthCell[2] = "12%";
            this.widthCell[3] = "22%";
            this.widthCell[4] = "8%";
            this.widthCell[5] = "12%";
            this.widthCell[6] = "8%";
            this.widthCell[7] = "8%";
            this.widthCell[8] = "100%";

        }
        this.setApplet(this.applet);
        this.createCondiPanel();
        this.createHead();
        this.createTable();
        this.createRow(this.searchTable, 0);
        this.body.append(this.searchMsgPanel);
        this.loadCondi();
    },
    //提示信息
    ShowMessage: function (str) {
        this.searchMsgPanel.css("visibility", "visible");
        this.searchMsgPanel.html(str);

    },
    //关闭提示
    HideMessage: function () {

        this.searchMsgPanel.css("visibility", "hidden");
    },
    //查询方案dropdownlist对象
    condiDropDownList: null,
    //查询方案名称textbox对象
    condiTitleBox: null,
    //存放查询方案的数据对象
    condiData: [],
    //读方案是否分用户
    condiIsUser: false,
    //列头表
    headerTableStr: '<table   border="0" cellspacing="0" cellpadding="0" style="{tstyle}" class="datagrid-header-table" ><tbody>{rows}</tbody></table>',
    //列头行
    headerRowStr: '<tr style="{style}">{cells}</tr>',
    //列头单元格
    headerCellStr: '<td style="{style}" class="datagrid-header-cell"><div class="datagrid-cell" ><span>{value}</span></div></td>'


});

/**   
* @method setApplet 
* @description 设置VC组件
* @param {UCML.Applet} applet VC组件   
*/
UCML.GridSearch.prototype.setApplet = function (applet) {
    this.dataTable = applet.dataTable;
    this.columns = applet.columns;
    this.businessObject = applet.BusinessObject;
    this.applet = applet;
}

/**   
* @method private createHead 
* @description 创建查询按钮区    
*/
UCML.GridSearch.prototype.createCondiPanel = function () {
    var condiPanel = $('<div id="'+this.applet.id+'_condiPanel" class="toolbar"></div>');
    this.body.append(condiPanel);
    var toolBarsearchMo = new UCML.ToolBar(this.applet.id+"_condiPanel");
    toolBarsearchMo.addButton({ id: "btnMakeCondi",
        text: "执行查询",
        tooltip: "执行查询",
        scope: this,
        onClick: function (el, e) {
            this.scope.makeCondi();
        }
    });
    toolBarsearchMo.addButton({ id: "btnClearCondi",
        text: "清除条件",
        tooltip: "清除条件",
        scope: this,
        onClick: function (el, e) {
            this.scope.clearCondi();
        }
    });
    toolBarsearchMo.addButton({ id: "btnSaveCondi",
        text: "保存方案",
        tooltip: "保存方案",
        scope: this,
        onClick: function (el, e) {
            this.scope.saveCondi();
        }
    });
    /*初始时loadData，该按钮没有意义
    toolBarsearchMo.addButton({ id: "btnLoadCondi",
    text: "读取方案",
    tooltip: "读取方案",
    scope: this,
    onClick: function (el, e) {
    this.scope.loadCondi();
    }
    });
    */
    toolBarsearchMo.addButton({ id: "btnClose",
        text: "关闭",
        tooltip: "关闭",
        scope: this,
        onClick: function (el, e) {
            //debugger;
            this.scope.close();
        }
        //onClick: this.butClose
    });
    var searchtb = $('<div id="'+this.applet.id+'_searchMo"></div>');
    this.body.append(searchtb);
    var toolBarsearchMo1 = new UCML.ToolBar(this.applet.id+"_searchMo");
    toolBarsearchMo1.addLabel({ id: "BtnLabel",
        text: "查询方案",
        tooltip: "",
        onClick: function (el, e) {

        }
    });
    this.condiTitleBox = toolBarsearchMo1.addTextBox({ id: "txtTitle" });
    toolBarsearchMo1.addSeparator({ id: "sepLi" });
    var ddlCondi = toolBarsearchMo1.addDropDownList({ id: "ddlCondi",
        text: "",
        tooltip: "",
        onClick: function (el, e) {

        }
    });
    ddlCondi.add("请选择方案...", "");
    this.condiDropDownList = ddlCondi;
    toolBarsearchMo1.addButton({ id: "btnDelCondi",
        text: "删除方案",
        tooltip: "删除方案",
        scope: this,
        onClick: function (el, e) {
            this.scope.delCondi();
        }
    });
    UCML.on(ddlCondi, "change", this.ddlCondiChange, this, ddlCondi);


}



/**   
* @method  createHead 
* @description 创建查询表头    
*/
UCML.GridSearch.prototype.createHead = function () {

    var headStrtpl = new UCML.Template(this.headerTableStr);
    var headerRowStrtpl = new UCML.Template(this.headerRowStr);
    var thtpl = new UCML.Template(this.headerCellStr);
    var cellsStr = [];
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[0] + ";height:20px;text-align:center", value: "左括号" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[1] + ";height:20px;text-align:center", value: "字段名称" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[2] + ";height:20px;text-align:center", value: "操作符" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[3] + ";height:20px;text-align:center", value: "取值" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[4] + ";height:20px;text-align:center", value: "右括号" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[5] + ";height:20px;text-align:center", value: "连接符" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[6] + ";height:20px;text-align:center;", value: "操作" }));
    cellsStr.add(thtpl.apply({ style: "width:" + this.widthCell[7] + ";height:20px;text-align:center", value: "操作" }));
    var headtplstr = headStrtpl.apply({ rows: headerRowStrtpl.apply({
        style: "height:20px",
        cells: cellsStr.join('')
    }),
        tstyle: "width:100%"
    });
    this.body.append(headtplstr);

}

/**   
* @method  createTable 
* @description 创建查询表格   
*/
UCML.GridSearch.prototype.createTable = function () {
    this.searchTable = $('<table id="searchTable" style="width:100%;"  cellSpacing=0 cellPadding=0   class="datagrid-body-table" />');
    this.body.append(this.searchTable);
}


/**   
* @method createRow 
* @description 创建查询行
* @param  {object} table 表格对象
* @param  {int}    rowIndex 第几行 
* @param  {object} rowData 插入的数据 
* @return {object}  创建的新row对象      
*/
UCML.GridSearch.prototype.createRow = function (table, rowIndex, rowData) {
    if (rowIndex == null || rowIndex == "") {
        rowIndex = 0;
    }
    var column = this.columns[0];
    var tr = $('<tr/>');
    table.append(tr);
    //一列
    var leftTd = $('<td style="width:' + this.widthCell[0] + ';height:20px" class="UCML-Grid-AltRow" ></td>');
    var leftInput = $('<input type="text"  style="height:20px;width:100%;border:1px;border-bottom-style:solid;border-top-style:hidden;border-left-style:hidden;border-right-style:hidden;padding:0px 0px 0px 0px;text-align:center;"  />')

    if (rowData && rowData.left && rowData.left != "null") {
        leftInput.val(rowData.left);
    }
    //控制只能输入"("
    leftInput.keyup(function () {
        var tmptxt = $(this).val();
        $(this).val(tmptxt.replace(/[^(]/g, ''));
    }).bind("paste", function () {
        var tmptxt = $(this).val();
        $(this).val(tmptxt.replace(/[^(]/g, ''));
    }).css("ime-mode", "disabled");
    leftTd.append(leftInput);
    tr.append(leftTd);
    //二列
    if (column.fieldKind == 3) {//2014-11-19 刘浩瑞 用于修改高级查询第一列为操作 查询异常
        for (var z = 0; z < this.columns.length; z++) {
            var co = this.columns[z];
            if (co.fieldKind != 3) {
                column = this.columns[z];
                break;
            }
        }
    }
    var filedTd = $('<td style="width:' + this.widthCell[1] + ';height:20px" className="UCML-Grid-AltRow"></td>');
    filedTd.attr("fieldName", column.fieldName);
    var columnTmp = this.createFieldList(filedTd, column, rowData);
    if (columnTmp) column = columnTmp;
    tr.append(filedTd);

    //三列
    var condTd = $('<td style="width:' + this.widthCell[2] + ';height:20px" className="UCML-Grid-AltRow"></td>');
    this.createLogicSymbol(condTd, column, rowData);
    tr.append(condTd);

    //四列
    var valueTd = $('<td style="width:' + this.widthCell[3] + ';height:20px" className="UCML-Grid-AltRow" noWrap=true></td>')
    //debugger;
    var obj = this.createControl(valueTd, column, rowData);
    this.controls.add(rowIndex, obj);
    tr.append(valueTd);


    //五列
    var rightTd = $('<td style="width:' + this.widthCell[4] + ';height:20px" class="UCML-Grid-AltRow" ></td>');
    var rightInput = $('<input type="text"  style="height:20px;width:100%;border:1px;border-bottom-style:solid;border-top-style:hidden;border-left-style:hidden;border-right-style:hidden;padding:0px 0px 0px 0px;text-align:center;"  />')

    if (rowData && rowData.right && rowData.right != "null") {
        rightInput.val(rowData.right);
    }
    //控制只能输入")"
    rightInput.keyup(function () {
        var tmptxt = $(this).val();
        $(this).val(tmptxt.replace(/[^)]/g, ''));
    }).bind("paste", function () {
        var tmptxt = $(this).val();
        $(this).val(tmptxt.replace(/[^)]/g, ''));
    }).css("ime-mode", "disabled");
    rightTd.append(rightInput);
    tr.append(rightTd);
    //tr.append($('<td style="width:' + this.widthCell[4] + ';height:20px" className="UCML-Grid-AltRow" ><input type="text" style="height:20px;width:100%;border:1px;border-bottom-style:solid;border-top-style:hidden;border-left-style:hidden;border-right-style:hidden;padding:0px 0px 0px 0px;text-align:center;" /></td>'));

    //六列
    var andorTd = $('<td style="width:' + this.widthCell[5] + ';height:20px" className="UCML-Grid-AltRow"></td>')
    var andorSelect = $('<select style="width:' + this.widthCell[8] + ';height:20px" />');
    andorSelect.append('<option value= "AND"  >AND</option>');
    andorSelect.append('<option value= "OR"  >OR</option>');
    andorSelect.css({ "height": "20px", "width": "100%", "border": "1px", "border-bottom-style": "solid", "border-top-style": "hidden", "border-left-style": "hidden", "border-right-style": "hidden", "padding": "0px 0px 0px 0px" });
    if (rowData && rowData.andor) {
        andorSelect.val(rowData.andor);
    }
    andorTd.append(andorSelect);
    tr.append(andorTd);

    //七列
    var addTd = $('<td style="width:' + this.widthCell[6] + ';height:20px;text-align:center;" className="UCML-Grid-AltRow"></td>');
    var theA = $('<a href = "javascript:void(0)" >增加</a>');
    UCML.on(theA, "click", this.addRowHandle, this, addTd);
    addTd.append(theA);
    tr.append(addTd);
    //八列
    var delTd = $('<td style="width:' + this.widthCell[7] + ';height:20px;text-align:center;" className="UCML-Grid-AltRow"></td>');
    var theA = $('<a href = "javascript:void(0)" >删除</a>');
    UCML.on(theA, "click", this.delRowHandle, this, delTd);
    delTd.append(theA);
    tr.append(delTd);
    return tr;

}

/**   
* @method createRow 
* @description 创建列名下拉列表，纯虚字段，SQL语句列不作查询列,(image,html,blob字段未处理)
* @param {object} cell 创建到的单元格
* @param {object} column 默认选中的列信息 
* @param {object} rowData 插入的数据 
* @return {object} 默认选中的列信息        
*/
UCML.GridSearch.prototype.createFieldList = function (cell, column, rowData) {
    if (!cell.jquery) cell = $(cell);

    var fieldSelect = $('<select style="width:' + this.widthCell[8] + ';heigth:20px" />');
    for (var i = 0; i < this.columns.length; i++) {
        var fieldName = this.columns[i].fieldName;
        var bcColumn = this.dataTable.getColumn(fieldName);
        if (bcColumn.fieldKind == 3) continue;
        if (rowData && this.columns[i].fieldName == rowData.name) {
            fieldSelect.append('<option value=' + this.columns[i].fieldName + ' selected = true >' + this.columns[i].caption + '</option>');
            cell.attr("fieldName", this.columns[i].fieldName);
            column = this.columns[i];
        }
        else
            fieldSelect.append('<option value=' + this.columns[i].fieldName + '>' + this.columns[i].caption + '</option>');
    }
    fieldSelect.css({ "height": "20px", "width": "100%", "border": "1px", "border-bottom-style": "solid", "border-top-style": "hidden", "border-left-style": "hidden", "border-right-style": "hidden", "padding": "0px 0px 0px 0px" });
    cell.append(fieldSelect);
    UCML.on(fieldSelect, "change", this.onFieldChange, this, fieldSelect);
    return column;
}


/**   
* @method createLogicSymbol 
* @description 创建逻辑比较符号,非字符类型不出like选项，代码表字段和Boolean型字段只有等于和不等于
* @param {object} cell 创建到的单元格
* @param {object} column 字段的列信息 
* @param {object} rowData 插入的数据             
*/
UCML.GridSearch.prototype.createLogicSymbol = function (cell, columnInfo, rowData) {
    if (!cell.jquery) cell = $(cell);
    var fieldName = columnInfo.fieldName;
    var bcColumn = this.dataTable.getColumn(fieldName);
    var fieldType = bcColumn.fieldType;
    var condSelect = $('<select style="width:' + this.widthCell[8] + ';heigth:20px" />')
    condSelect.append('<option value= "="  >等于</option>');
    condSelect.append('<option value= "<>"  >不等于</option>');
    if (!bcColumn.isCodeTable && fieldType != "Boolean") {
        condSelect.append('<option value= "<"  >小于</option>');
        condSelect.append('<option value= ">"  >大于</option>');
        condSelect.append('<option value= "<="  >小于等于</option>');
        condSelect.append('<option value= ">="  >大于等于</option>');

        if (fieldType == "Text" || fieldType == "VarChar" || fieldType == "Char") {
            condSelect.append('<option value= "Like"  >包含</option>');
            condSelect.append('<option value= "Not Like"  >不包含</option>');
        }
    }
    if (rowData && rowData.logic) {
        condSelect.val(rowData.logic);
    }
    condSelect.css({ "height": "20px", "width": "100%", "border": "1px", "border-bottom-style": "solid", "border-top-style": "hidden", "border-left-style": "hidden", "border-right-style": "hidden", "padding": "0px 0px 0px 0px" });
    cell.append(condSelect);

}


/**   
* @method createControl 
* @description 根据字段类型创建字段录入控件 
* @param {object} cell 创建到的单元格
* @param {object} columnInfo 字段的列信息 
* @param {object} rowData 插入的数据    
* @return {object}  创建的控件对象       
*/
UCML.GridSearch.prototype.createControl = function (cell, columnInfo, rowData) {
    //debugger;
    if (!cell.jquery) cell = $(cell);
    var obj = new Object();
    var fieldName = columnInfo.fieldName;
    var vvalue = "";
    if (rowData && rowData.fieldValue) {
        vvalue = rowData.fieldValue;
    }
    var config = { column: columnInfo, bcColumn: this.dataTable.getColumn(fieldName), value: vvalue, fieldName: fieldName };

    var col = config.column;
    var fieldName = config.fieldName;
    var value = config.value;
    var bcColumn = config.bcColumn;
    var ctype = col.EditType || col.editContrl;

    var obj = new Object();

    if (bcColumn.isCodeTable) {
        ctype = "UCML.DropDownList";
        obj.isCodeTable = true;
        obj.isSetProperties = true;
        obj.itemFirstText = "请选择..";
        obj.codeTable = bcColumn.codeTable;
    }
    obj.dataTable = this.dataTable;
    obj.fieldName = fieldName;
    obj.value = value;
    obj.applet = this;
    obj.bcColumn = bcColumn;
    if (!ctype) {
        switch (bcColumn.fieldType) {
            case "Date":
                ctype = "UCML.Datebox";
                break;
            case "DateTime":
                ctype = "UCML.DateTimebox";
                break;
            case "Time":
                ctype = "UCML.Timebox";
                break;
            case "Boolean":
                ctype = "UCML.CheckBox";
                break;
        }
    }
    else {
        switch (ctype) {
            case "INPUT":
                ctype = "UCML.TextBox";
                break;
            case "UCML.CheckBox":
                ctype = "UCML.CheckBox";
                break;
            case "DATETIME":
            case "DATEEDIT":
                ctype = "UCML.DateTimeBox";
                break;
            case "DATE":
                ctype = "UCML.Datebox";
                break;
            case "TIME":
                ctype = "UCML.Timebox";
                break;
            case "TEXTAREA":
                ctype = "UCML.TextArea";
                break;
            case "UCML.Html":
                ctype = "UCML.TextBox";
                break;
        }
    }
    obj.ctype = ctype || "UCML.TextBox";
    obj.ctype = (obj.ctype == "UCML.DateTimeBox" || obj.ctype == "UCML.Timebox") ? "UCML.Datebox" : obj.ctype;

    if (ctype == "UCML.CheckBox") {
        var condSelect = $('<select />');
        condSelect.append('<option value="">未设置</option>');
        condSelect.append('<option value="1">是</option>');
        condSelect.append('<option value="0">否</option>');
        condSelect.css({ "height": "20px", "width": "100%", "border": "1px", "border-bottom-style": "solid", "border-top-style": "hidden", "border-left-style": "hidden", "border-right-style": "hidden", "padding": "0px 0px 0px 0px" });
        cell.append(condSelect);
        condSelect.val(obj.value);
        obj.control = condSelect[0];
        return obj;
    }
    obj.rendeTo = cell;
    // debugger;
    var dataCell = UCML.create(obj, obj.ctype);
    dataCell.setWidth("100%");
    dataCell.setCssValue({ "height": "20px", "width": "100%", "border": "1px", "border-bottom-style": "solid", "border-top-style": "hidden", "border-left-style": "hidden", "border-right-style": "hidden", "padding": "0px 0px 0px 0px" });
    dataCell.setValue(obj.value);

    //防止sql注入验证
    UCML.on(dataCell, "blur", function(el, e) {
        this.fireEvent("blur", e);
        var target = UCML.isElement(e) ? e : e.target;
        var $target = $(target);

        var reg = /select|update|delete|exec|count|'|"|=|;|>|<|%/i;
        if(reg.test($target.val())) {
            alert("参数非法、请勿输入特殊字符和SQL关键字！");
            $target.focus();
        }
    }, this);

    /*if (dataCell.showPanel) {
    dataCell.showPanel();
    }*/
    obj.control = dataCell;
    return obj;


}
/**   
* @method delCondi 
* @description 删除存档信息                      
*/
UCML.GridSearch.prototype.delCondi = function () {
    var oid = this.condiDropDownList.value;
    if (oid == "") return;
    for (var i = 0; i < this.condiData.length; i++) {
        if (this.condiData[i].OID == oid) {
            this.condiData.remove(this.condiData[i]);
            this.ShowMessage("正在删除方案，请等待......");
            this.businessObject.deleteQueryCondi(oid, this.deleteQueryCondiSuccessCall, this.deleteQueryCondiFailureCall, this);
            break;
        }
    }
    this.condiDropDownList.clear();
    this.condiDropDownList.add("请选择方案...", "", true);
    this.condiTitleBox.setValue("");
    for (var i = 0; i < this.condiData.length; i++) {
        this.condiData[i].OID;
        this.condiData[i].name;
        this.condiDropDownList.add(this.condiData[i].name, this.condiData[i].OID);
    }
}
/**   
* @method deleteQueryCondiSuccessCall 
* @description 从服务器删除查询存档后，成功返回结果处理函数
*              处理过程为：填充查询方案列表                    
*/
UCML.GridSearch.prototype.deleteQueryCondiSuccessCall = function (result, text, methodName) {

    this.HideMessage();
}
/**   
* @method deleteQueryCondiFailureCall 
* @description 从服务器删除存档后，失败返回结果处理函数                    
*/
UCML.GridSearch.prototype.deleteQueryCondiFailureCall = function (result, text, methodName) {
    this.HideMessage();
    alert(text);
}

/**   
* @method setCondi 
* @description 根据存档创建列 
* @param {Array} rowdata 存档信息              
*/
UCML.GridSearch.prototype.setCondi = function (rowdata) {
    if (rowdata && rowdata.length != 0) {
        while (this.searchTable[0].rows.length > 0) {
            this.searchTable[0].deleteRow(this.searchTable[0].rows.length - 1);
        }
        this.controls.clear();
        for (var i = 0; i < rowdata.length; i++) {
            this.createRow(this.searchTable, i, rowdata[i]);
        }
    }
}
/**   
* @method loadCondi 
* @description 从服务器读取查询存档                    
*/
UCML.GridSearch.prototype.loadCondi = function () {
    this.ShowMessage("正在读取方案，请等待......");
    this.businessObject.loadQueryCondi(this.businessObject.BPOName, this.applet.id, this.condiIsUser, this.loadQueryCondiSuccessCall, this.loadQueryCondiFailureCall, this);

}
/**   
* @method loadCondi 
* @description 查询方案下拉列表变化时处理函数                    
*/
UCML.GridSearch.prototype.ddlCondiChange = function (e, el) {
    if (e.value == "") {
        this.condiTitleBox.setValue("");
        return;
    }
    for (var i = 0; i < this.condiData.length; i++) {
        if (this.condiData[i].OID == e.value) {
            this.condiTitleBox.setValue(this.condiData[i].name);
            this.setCondi(this.condiData[i].value);
            break;
        }
    }

}
/**   
* @method loadQueryCondiSuccessCall 
* @description 从服务器读取查询存档后，成功返回结果处理函数
*              处理过程为：填充查询方案列表                    
*/
UCML.GridSearch.prototype.loadQueryCondiSuccessCall = function (result, text, methodName) {

    this.condiData = eval(text);
    this.condiDropDownList.clear();
    this.condiDropDownList.add("请选择方案...", "", true);
    this.condiTitleBox.setValue("");
    for (var i = 0; i < this.condiData.length; i++) {
        this.condiData[i].OID;
        this.condiData[i].name;
        this.condiDropDownList.add(this.condiData[i].name, this.condiData[i].OID);
    }
    this.HideMessage();
}
/**   
* @method loadQueryCondiFailureCall 
* @description 从服务器读取查询存档后，失败返回结果处理函数                    
*/
UCML.GridSearch.prototype.loadQueryCondiFailureCall = function (result, text, methodName) {
    this.HideMessage();
    alert(text);
}
/**   
* @method saveCondi 
* @description 将新的查询方案保存到服务器                    
*/
UCML.GridSearch.prototype.saveCondi = function () {
    if (this.condiTitleBox.value == "") {
        alert("请输入方案名");
        this.condiTitleBox.focus();
        return;
    }

    var titleValue = this.condiTitleBox.value;
    if (titleValue == this.condiDropDownList.getText()) {
        this.delCondi();
    }

    var jsonArr = [];
    var searchTable = this.searchTable[0];
    for (var i = 0; i < searchTable.rows.length; i++) {

        var dataItem = {};

        //左侧括号
        var leftTd = searchTable.rows[i].cells[0];
        var leftInput = leftTd.children[0];
        var leftValue = leftInput.value;
        dataItem.left = leftInput.value;

        //字段值
        var filedTd = searchTable.rows[i].cells[1];
        var fieldName = filedTd.getAttribute("fieldName");
        var fieldValue = fieldName || fieldName.value;
        dataItem.name = fieldValue;

        //查询条件
        var condTd = searchTable.rows[i].cells[2];
        var condiControl = condTd.children[0];
        var condValue = condiControl.value;
        dataItem.logic = condValue;

        //查询值
        var valueTd = searchTable.rows[i].cells[3];
        var item = this.controls.get(i);
        dataItem.fieldValue = item.control.value;


        //右侧括号
        var rightTd = searchTable.rows[i].cells[4];
        var rightInput = rightTd.children[0];
        var rightValue = rightInput.value;
        dataItem.right = rightValue;

        //连接符
        var andorTd = searchTable.rows[i].cells[5];
        var andorSelect = andorTd.children[0];
        var andorValue = andorSelect.value;
        dataItem.andor = andorValue;

        dataItem.index = i;
        jsonArr.push(dataItem);
    }
    var encoded = $.toJSON(jsonArr);
    this.ShowMessage("正在保存方案，请等待......");
    this.businessObject.saveQueryCondi(this.businessObject.BPOName, this.applet.id, titleValue, encoded, this.saveQueryCondiSuccessCall, this.saveQueryCondiFailureCall, this);
}
/**   
* @method saveQueryCondiSuccessCall 
* @description 将新的查询方案保存到服务器后，成功返回处理函数                    
*/
UCML.GridSearch.prototype.saveQueryCondiSuccessCall = function (result, text, methodName) {
    // debugger;

    var a = eval(text);
    for (var i = 0; i < a.length; i++) {
        this.condiData.add(a[i]);
        this.condiDropDownList.add(a[i].name, a[i].OID, true);
        this.condiTitleBox.setValue(a[i].name);

    }
    this.loadCondi();
    this.HideMessage();
}
/**   
* @method saveQueryCondiSuccessCall 
* @description 将新的查询方案保存到服务器后，失败返回处理函数                    
*/
UCML.GridSearch.prototype.saveQueryCondiFailureCall = function (result, text, methodName) {

    this.HideMessage();
    alert(text);
}

/**   
* @method getCondi 
* @description 获取查询的SQL语句   
*/
UCML.GridSearch.prototype.getCondiSQL = function () {
    var searchTable = this.searchTable[0];

    var GridSearchLCondi = "";



    for (var i = 0; i < searchTable.rows.length; i++) {

        var dataItem = new Object();

        //左侧括号
        var leftTd = searchTable.rows[i].cells[0];
        var leftInput = leftTd.children[0];
        var leftValue = leftInput.value;
        dataItem.leftValue = leftInput.value;

        //字段值
        var filedTd = searchTable.rows[i].cells[1];
        var fieldName = filedTd.getAttribute("fieldName");
        var fieldValue = fieldName || fieldName.value;
        dataItem.fieldValue = fieldValue;

        //查询条件
        var condTd = searchTable.rows[i].cells[2];
        var condiControl = condTd.children[0];
        var condValue = condiControl.value;
        dataItem.condValue = condValue;

        //查询值
        var valueTd = searchTable.rows[i].cells[3];
        var valueList;

        //右侧括号
        var rightTd = searchTable.rows[i].cells[4];
        var rightInput = rightTd.children[0];
        var rightValue = rightInput.value;
        dataItem.rightValue = rightValue;

        //连接符
        var andorTd = searchTable.rows[i].cells[5];
        var andorSelect = andorTd.children[0];
        var andorValue = andorSelect.value;
        dataItem.andorValue = andorValue;

        var item = this.controls.get(i);
        columnInfo = item.bcColumn;


        if (columnInfo.fieldKind == 0) {
            fieldValue = this.dataTable.TableName + "." + columnInfo.fieldName;
        }
        else if (columnInfo.fieldKind == 2) {
            fieldValue = "al" + columnInfo.foreignKeyField + "." + columnInfo.lookupResultField;
        }
        else if (columnInfo.fieldKind == 10) {
            fieldValue = "al" + columnInfo.foreignKeyField + "." + columnInfo.fieldName;
        }
        else if (columnInfo.fieldKind == 5 && columnInfo.QueryRefColumn) {//SQL语句列
            fieldValue = columnInfo.QueryRefColumn;
        }

        valueList = item.control.value;

        dataItem.value = valueList;


        if (condValue == 'Like' || condValue == 'Not Like') {
            valueList = "%" + valueList + "%";
        }
        //无查询条件，无查询值，空处理
        if (condValue == '' || condValue.lenth == 0 || valueList == '' || valueList.lenth == 0) {
            //不处理此情况
        }
        else {
            GridSearchLCondi = GridSearchLCondi + leftValue;
            var tmp = this.getSql(fieldValue, condValue, valueList, columnInfo.fieldType);
            if (tmp.err) {
                alert(tmp.errmeg);
                return;
            }
            GridSearchLCondi = GridSearchLCondi + tmp.sql + rightValue; //加上右括号
            if (i != (searchTable.rows.length - 1))
                GridSearchLCondi = GridSearchLCondi + " " + andorValue + " "; //加上连接符
        }

    }

    GridSearchLCondi = GridSearchLCondi.replace(/\s*$/, "");
    if (GridSearchLCondi.substr(GridSearchLCondi.length - 3) == "AND") GridSearchLCondi = GridSearchLCondi.substring(0, GridSearchLCondi.length - 3);
    if (GridSearchLCondi.substr(GridSearchLCondi.length - 2) == "OR") GridSearchLCondi = GridSearchLCondi.substring(0, GridSearchLCondi.length - 2);
    return GridSearchLCondi;

}

/**   
* @method makeCondi 
* @description 执行查询   
*/
UCML.GridSearch.prototype.makeCondi = function () {
    //debugger;
    var fieldListx = "";
    var valueListx = "";
    var condiIndentListx = "";
    var GridSearchLCondi = this.getCondiSQL();
    if (GridSearchLCondi != null) {
        this.dataTable.SetCondiList(fieldListx, valueListx, condiIndentListx, GridSearchLCondi, 0);
        this.dataTable.getData(0, this.dataTable.getDefaultPageCount());
    }

    //this.businessObject.condiQuery(fieldList, valueList, condiIndentList, GridSearchLCondi, 0);

}

/**   
* @method getBoolSql 
* @description bool型SQL语句特殊处理，私有方法
*  分析情况1 当用户选择【"=" 是】时，返回 字段=1 
*          2 当用户选择【"<>" 是】时，返回 字段<>1 or 字段 is null
*          3 当用户选择【"=" 否】时，返回 字段=0  or 字段 is null
*          4 当用户选择【"<>" 否】时，返回 字段<>0
*   
*/
UCML.GridSearch.prototype.getBoolSql = function (field, cond, valueList) {

    // 情况 1和情况4
    if ((cond == "=" && valueList == "1") || (cond == "<>" && valueList == "0")) {
        var temp = field + " " + cond + " " + valueList;
        return temp;
    }
    //情况 2，和情况3
    if ((cond == "=" && valueList == "0") || (cond == "<>" && valueList == "1")) {
        var temp = " ( " + field + " " + cond + " " + valueList + " or " + field + " is null ) ";
        return temp;
    }
}

/**   
* @method getDateSql 
* @description 日期型SQL语句特殊处理，私有方法
*         ORACLE的时间比较特殊，需要做转换，全局定义数据库种类处理
*   
*/
UCML.GridSearch.prototype.getDateSql = function (field, cond, valueList) {
    //sqlserver处理模式
    if (this.dbType == 0) {
        var tempsql = "";
        tempsql = tempsql + field; //加上字段
        tempsql = tempsql + " "
        tempsql = tempsql + cond; //加上条件
        tempsql = tempsql + " '" + valueList + "' ";
        tempsql = this.getUnequalSql(tempsql, field, cond);
        return tempsql;
    }
    //oralce处理模式
    if (this.dbType == 1) {
        var tempsql = "";
        tempsql = tempsql + "to_char(" + field + ", 'yyyy-mm-dd')"; //加上字段
        tempsql = tempsql + " "
        tempsql = tempsql + cond; //加上条件
        tempsql = tempsql + " '" + valueList + "' ";
        tempsql = this.getUnequalSql(tempsql, field, cond);
        return tempsql;
    }

}

/**   
* @method getSql 
* @description 根据字段类型，比较符号，处理对应的SQL语句
*   
*/
UCML.GridSearch.prototype.getSql = function (field, cond, valueList, type) {
    var reto = new Object();
    reto.errmeg = "未成功查询";
    reto.err = true;
    reto.sql = "";

    if (type == "Boolean") {
        var temp = this.getBoolSql(field, cond, valueList);
        if (temp) {
            reto.sql = temp;
            reto.err = false;
            return reto;
        }
    }
    //数字类型处理
    if ((type == "Money") || (type == "Float") || (type == "Short") || (type == "Int") || (type == "Double") || (type == "Long") || (type == "Numberic")) {
        if (isNaN(valueList) == false) {
            var tempsql = "";
            tempsql = tempsql + field; //加上字段
            tempsql = tempsql + " "
            tempsql = tempsql + cond; //加上条件
            tempsql = tempsql + " " + valueList;
            tempsql = this.getUnequalSql(tempsql, field, cond);
            reto.sql = tempsql;
            reto.err = false;
            return reto;
        }
        else {
            reto.errmeg = '输入的值 ' + valueList + ' 请输入数字格式条件';
            return reto;
        }
    }
    else {
        if (type == "Date" || type == "DateTime" || type == "Time") {
            var temp = this.getDateSql(field, cond, valueList);
            if (temp) {
                reto.sql = temp;
                reto.err = false;
                return reto;
            }
        }
        else {
            var tempsql = "";
            tempsql = tempsql + field; //加上字段
            tempsql = tempsql + " "
            tempsql = tempsql + cond; //加上条件
            tempsql = tempsql + " '" + valueList + "' ";
            tempsql = this.getUnequalSql(tempsql, field, cond);
            reto.sql = tempsql;
            reto.err = false;
            return reto;
        }
    }
}

/**   
* @method getUnequalSql 
* @description 不等于，不包含，加入is null处理情况   
*/
UCML.GridSearch.prototype.getUnequalSql = function (sql, field, cond) {
    if (cond == "<>" || cond == "Not Like") {
        return " ( " + sql + " or " + field + " is null )";
    }
    else
        return sql;

}





/**   
* @method clearCondi 
* @description 清空查询条件   
*/
UCML.GridSearch.prototype.clearCondi = function () {
    while (this.searchTable[0].rows.length > 0) {
        this.searchTable[0].deleteRow(this.searchTable[0].rows.length - 1);
    }
    this.controls.clear();
    var tr = this.createRow(this.searchTable, 0);

    //UCML.Cookie.clear(this.id);
}




/**   
* @method  onFieldChange 
* @description 查询列字段名选择变化事件处理函数
*              处理过程为：重新创建逻辑比较符列和录入控件列   
*/
UCML.GridSearch.prototype.onFieldChange = function (e, el) {

    var select = e[0];
    var column = this.applet.getColumn(e.val());
    var td = select.parentNode;
    while (td.tagName != "TD") td = td.parentNode;
    if (td == null) return;
    td.setAttribute("fieldName", e.val());
    var tr = td.parentNode;
    tr.cells[3].innerHTML = "";
    tr.cells[2].innerHTML = "";
    this.createLogicSymbol(tr.cells[2], column);
    var obj = this.createControl(tr.cells[3], column);
    obj.columnInfo = column;
    this.controls.add(td.parentNode.rowIndex, obj);
}

/**   
* @method private addRowHandle 
* @description  添加一行   
*/
UCML.GridSearch.prototype.addRowHandle = function (e, el) {

    var td = e[0];
    while (td.tagName != "TD") {
        td = td.parentNode;
    }
    if (td == null) {
        return;
    }
    var tr = this.createRow(this.searchTable, td.parentNode.parentNode.rows.length);
}

/**   
* @method private addRowHandle 
* @description  删除一行   
*/
UCML.GridSearch.prototype.delRowHandle = function (e, el) {
    var td = e[0];
    while (td.tagName != "TD") {
        td = td.parentNode;
    }
    if (td == null) {
        return;
    }
    if (this.searchTable[0].rows.length == 1) {
        alert("已到了最后一行");
        return;
    }
    this.controls.removeKey(td.parentNode.rowIndex);
    this.searchTable[0].deleteRow(td.parentNode.rowIndex);
    for (var i = 0; i < this.controls.keys.length; i++) {
        this.controls.keys[i] = i;
    }
}
