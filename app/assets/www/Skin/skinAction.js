/*
   皮肤切换方法的实现与换肤通知
*/

//加载皮肤资源信息
function loadSkin(skinName,screenName,callBack){
	if(skinName!=null){
		$.get(
			localResourcePath+'changeUcmlSkin.ashx',
			{SkinName:skinName,ScreenName:screenName},
			function(data) {
				var skin=$.parseJSON(data);
				callBack(skin);
			},
			"text"
		);
   }
}

//加载皮肤资源，设置框架皮肤，接着通知页面内容和页面模板换肤
function setFrameSkin(skinName,screenName) {
	loadSkin(skinName,screenName,function(skin){
		if(skin){
			var frame=skin.frame;
			if(frame.styles){
				$.each(frame.styles,function(index,item){
					$("link#"+item.id).attr("href",localResourcePath+item.src);
				});
			}
			if(frame.scripts){
				$.each(frame.scripts,function(index,item){
					$("script#"+item.id).attr("src",localResourcePath+item.src);
				});
			}
			onSetSkin(skinName,skin.page);
		}
	});
}

//设置页面内容皮肤
function setPageSkin(pageSkin) {
        if(pageSkin){
			if(pageSkin.styles){
				$.each(pageSkin.styles,function(index,item){
					$("link#"+item.id).attr("href",UCMLLocalResourcePath+item.src);
				});
			}
			if(pageSkin.scripts){
				$.each(pageSkin.scripts,function(index,item){
				});
			}
		}
		
	$("iframe").each(function(){
		if(this.contentWindow.setPageSkin&&typeof this.contentWindow.setPageSkin=="function"){
			this.contentWindow.setPageSkin(pageSkin);
		}
	});
}

//通知页面内容和页面模板换肤
function onSetSkin(skinName,pageSkin) {
    var fnSetSkin = window.document.getElementById("mainIframe").contentWindow.setPageSkin;
	var fnSetTemplateSkin = window.document.getElementById("mainIframe").contentWindow.setTemplateSkin;
    if (fnSetSkin) {
        fnSetSkin(pageSkin);
    }
	if(fnSetTemplateSkin){
		fnSetTemplateSkin(skinName);
	}
}
